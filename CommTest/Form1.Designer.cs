﻿namespace CommTest
{
    partial class UOServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShutdownServer = new System.Windows.Forms.Button();
            this.btnStartServer = new System.Windows.Forms.Button();
            this.btnSendCommand = new System.Windows.Forms.Button();
            this.btnRemoveClinets = new System.Windows.Forms.Button();
            this.btnSendXML = new System.Windows.Forms.Button();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.txtClientInteractionLog = new System.Windows.Forms.TextBox();
            this.txtInternalServerLog = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetHCAIStuff = new System.Windows.Forms.Button();
            this.btnChangeOCFStatus = new System.Windows.Forms.Button();
            this.btnSubmitToHCAI = new System.Windows.Forms.Button();
            this.btnWithdraw = new System.Windows.Forms.Button();
            this.btnGetInsurers = new System.Windows.Forms.Button();
            this.btnDisconnectAll = new System.Windows.Forms.Button();
            this.cmdSendAck = new System.Windows.Forms.Button();
            this.btnDataExtract = new System.Windows.Forms.Button();
            this.btnCHeckPending = new System.Windows.Forms.Button();
            this.btnFacilityInfo = new System.Windows.Forms.Button();
            this.CheckExtract = new System.Windows.Forms.Button();
            this.btnToggleTriggers = new System.Windows.Forms.Button();
            this.btnChangeConfigFile = new System.Windows.Forms.Button();
            this.btnValidateHc = new System.Windows.Forms.Button();
            this.btnEncryptKey = new System.Windows.Forms.Button();
            this.txtPlainKey = new System.Windows.Forms.TextBox();
            this.txtEncryptedKey = new System.Windows.Forms.TextBox();
            this.btnDecryptKey = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnShutdownServer
            // 
            this.btnShutdownServer.Location = new System.Drawing.Point(207, 49);
            this.btnShutdownServer.Name = "btnShutdownServer";
            this.btnShutdownServer.Size = new System.Drawing.Size(75, 23);
            this.btnShutdownServer.TabIndex = 0;
            this.btnShutdownServer.Text = "Shutdown";
            this.btnShutdownServer.UseVisualStyleBackColor = true;
            this.btnShutdownServer.Click += new System.EventHandler(this.btnShutdownServer_Click);
            // 
            // btnStartServer
            // 
            this.btnStartServer.Location = new System.Drawing.Point(15, 50);
            this.btnStartServer.Name = "btnStartServer";
            this.btnStartServer.Size = new System.Drawing.Size(75, 23);
            this.btnStartServer.TabIndex = 1;
            this.btnStartServer.Text = "Start";
            this.btnStartServer.UseVisualStyleBackColor = true;
            this.btnStartServer.Click += new System.EventHandler(this.btnStartServer_Click);
            // 
            // btnSendCommand
            // 
            this.btnSendCommand.Location = new System.Drawing.Point(110, 49);
            this.btnSendCommand.Name = "btnSendCommand";
            this.btnSendCommand.Size = new System.Drawing.Size(75, 23);
            this.btnSendCommand.TabIndex = 2;
            this.btnSendCommand.Text = "Send";
            this.btnSendCommand.UseVisualStyleBackColor = true;
            this.btnSendCommand.Click += new System.EventHandler(this.btnSendCommand_Click);
            // 
            // btnRemoveClinets
            // 
            this.btnRemoveClinets.Location = new System.Drawing.Point(15, 106);
            this.btnRemoveClinets.Name = "btnRemoveClinets";
            this.btnRemoveClinets.Size = new System.Drawing.Size(92, 23);
            this.btnRemoveClinets.TabIndex = 3;
            this.btnRemoveClinets.Text = "Remove Clients";
            this.btnRemoveClinets.UseVisualStyleBackColor = true;
            this.btnRemoveClinets.Click += new System.EventHandler(this.btnRemoveClinets_Click);
            // 
            // btnSendXML
            // 
            this.btnSendXML.Location = new System.Drawing.Point(209, 106);
            this.btnSendXML.Name = "btnSendXML";
            this.btnSendXML.Size = new System.Drawing.Size(107, 23);
            this.btnSendXML.TabIndex = 4;
            this.btnSendXML.Text = "Send XML";
            this.btnSendXML.UseVisualStyleBackColor = true;
            this.btnSendXML.Click += new System.EventHandler(this.btnSendXML_Click);
            // 
            // txtServerName
            // 
            this.txtServerName.Enabled = false;
            this.txtServerName.Location = new System.Drawing.Point(12, 12);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(267, 20);
            this.txtServerName.TabIndex = 5;
            this.txtServerName.Text = "localhost\\ANXSQLSERVER";
            // 
            // txtClientInteractionLog
            // 
            this.txtClientInteractionLog.Location = new System.Drawing.Point(322, 24);
            this.txtClientInteractionLog.Multiline = true;
            this.txtClientInteractionLog.Name = "txtClientInteractionLog";
            this.txtClientInteractionLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtClientInteractionLog.Size = new System.Drawing.Size(473, 391);
            this.txtClientInteractionLog.TabIndex = 6;
            // 
            // txtInternalServerLog
            // 
            this.txtInternalServerLog.Location = new System.Drawing.Point(12, 161);
            this.txtInternalServerLog.Multiline = true;
            this.txtInternalServerLog.Name = "txtInternalServerLog";
            this.txtInternalServerLog.Size = new System.Drawing.Size(270, 254);
            this.txtInternalServerLog.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(322, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Client Server Interaction";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Internal Server Commands";
            // 
            // btnGetHCAIStuff
            // 
            this.btnGetHCAIStuff.Location = new System.Drawing.Point(17, 531);
            this.btnGetHCAIStuff.Name = "btnGetHCAIStuff";
            this.btnGetHCAIStuff.Size = new System.Drawing.Size(145, 23);
            this.btnGetHCAIStuff.TabIndex = 10;
            this.btnGetHCAIStuff.Text = "Get Pending OCFs";
            this.btnGetHCAIStuff.UseVisualStyleBackColor = true;
            this.btnGetHCAIStuff.Click += new System.EventHandler(this.btnGetHCAIStuff_Click);
            // 
            // btnChangeOCFStatus
            // 
            this.btnChangeOCFStatus.Location = new System.Drawing.Point(17, 487);
            this.btnChangeOCFStatus.Name = "btnChangeOCFStatus";
            this.btnChangeOCFStatus.Size = new System.Drawing.Size(145, 23);
            this.btnChangeOCFStatus.TabIndex = 11;
            this.btnChangeOCFStatus.Text = "Get Adjuster Response";
            this.btnChangeOCFStatus.UseVisualStyleBackColor = true;
            this.btnChangeOCFStatus.Click += new System.EventHandler(this.btnChangeOCFStatus_Click);
            // 
            // btnSubmitToHCAI
            // 
            this.btnSubmitToHCAI.Location = new System.Drawing.Point(17, 447);
            this.btnSubmitToHCAI.Name = "btnSubmitToHCAI";
            this.btnSubmitToHCAI.Size = new System.Drawing.Size(145, 23);
            this.btnSubmitToHCAI.TabIndex = 12;
            this.btnSubmitToHCAI.Text = "Submit Cmd To HCAI";
            this.btnSubmitToHCAI.UseVisualStyleBackColor = true;
            this.btnSubmitToHCAI.Click += new System.EventHandler(this.btnSubmitToHCAI_Click);
            // 
            // btnWithdraw
            // 
            this.btnWithdraw.Location = new System.Drawing.Point(207, 447);
            this.btnWithdraw.Name = "btnWithdraw";
            this.btnWithdraw.Size = new System.Drawing.Size(145, 23);
            this.btnWithdraw.TabIndex = 13;
            this.btnWithdraw.Text = "Withdraw";
            this.btnWithdraw.UseVisualStyleBackColor = true;
            this.btnWithdraw.Click += new System.EventHandler(this.btnWithdraw_Click);
            // 
            // btnGetInsurers
            // 
            this.btnGetInsurers.Location = new System.Drawing.Point(207, 487);
            this.btnGetInsurers.Name = "btnGetInsurers";
            this.btnGetInsurers.Size = new System.Drawing.Size(145, 23);
            this.btnGetInsurers.TabIndex = 14;
            this.btnGetInsurers.Text = "Get Insurers";
            this.btnGetInsurers.UseVisualStyleBackColor = true;
            this.btnGetInsurers.Click += new System.EventHandler(this.btnGetInsurers_Click);
            // 
            // btnDisconnectAll
            // 
            this.btnDisconnectAll.Location = new System.Drawing.Point(207, 531);
            this.btnDisconnectAll.Name = "btnDisconnectAll";
            this.btnDisconnectAll.Size = new System.Drawing.Size(145, 23);
            this.btnDisconnectAll.TabIndex = 15;
            this.btnDisconnectAll.Text = "Disconnect Clients";
            this.btnDisconnectAll.UseVisualStyleBackColor = true;
            this.btnDisconnectAll.Click += new System.EventHandler(this.btnDisconnectAll_Click);
            // 
            // cmdSendAck
            // 
            this.cmdSendAck.Location = new System.Drawing.Point(376, 527);
            this.cmdSendAck.Name = "cmdSendAck";
            this.cmdSendAck.Size = new System.Drawing.Size(90, 27);
            this.cmdSendAck.TabIndex = 16;
            this.cmdSendAck.Text = "send ack";
            this.cmdSendAck.UseVisualStyleBackColor = true;
            this.cmdSendAck.Click += new System.EventHandler(this.cmdSendAck_Click);
            // 
            // btnDataExtract
            // 
            this.btnDataExtract.Location = new System.Drawing.Point(376, 487);
            this.btnDataExtract.Name = "btnDataExtract";
            this.btnDataExtract.Size = new System.Drawing.Size(90, 23);
            this.btnDataExtract.TabIndex = 17;
            this.btnDataExtract.Text = "Data Extract";
            this.btnDataExtract.UseVisualStyleBackColor = true;
            this.btnDataExtract.Click += new System.EventHandler(this.btnDataExtract_Click);
            // 
            // btnCHeckPending
            // 
            this.btnCHeckPending.Location = new System.Drawing.Point(110, 106);
            this.btnCHeckPending.Name = "btnCHeckPending";
            this.btnCHeckPending.Size = new System.Drawing.Size(93, 23);
            this.btnCHeckPending.TabIndex = 18;
            this.btnCHeckPending.Text = "Check Pending";
            this.btnCHeckPending.UseVisualStyleBackColor = true;
            this.btnCHeckPending.Click += new System.EventHandler(this.btnCHeckPending_Click);
            // 
            // btnFacilityInfo
            // 
            this.btnFacilityInfo.Location = new System.Drawing.Point(376, 446);
            this.btnFacilityInfo.Name = "btnFacilityInfo";
            this.btnFacilityInfo.Size = new System.Drawing.Size(90, 23);
            this.btnFacilityInfo.TabIndex = 19;
            this.btnFacilityInfo.Text = "Get Facility Info";
            this.btnFacilityInfo.UseVisualStyleBackColor = true;
            this.btnFacilityInfo.Click += new System.EventHandler(this.btnFacilityInfo_Click);
            // 
            // CheckExtract
            // 
            this.CheckExtract.Location = new System.Drawing.Point(484, 446);
            this.CheckExtract.Name = "CheckExtract";
            this.CheckExtract.Size = new System.Drawing.Size(89, 23);
            this.CheckExtract.TabIndex = 20;
            this.CheckExtract.Text = "Check Extract";
            this.CheckExtract.UseVisualStyleBackColor = true;
            this.CheckExtract.Click += new System.EventHandler(this.CheckExtract_Click);
            // 
            // btnToggleTriggers
            // 
            this.btnToggleTriggers.Location = new System.Drawing.Point(484, 530);
            this.btnToggleTriggers.Name = "btnToggleTriggers";
            this.btnToggleTriggers.Size = new System.Drawing.Size(89, 23);
            this.btnToggleTriggers.TabIndex = 21;
            this.btnToggleTriggers.Text = "Toggle Triggers";
            this.btnToggleTriggers.UseVisualStyleBackColor = true;
            this.btnToggleTriggers.Click += new System.EventHandler(this.btnToggleTriggers_Click);
            // 
            // btnChangeConfigFile
            // 
            this.btnChangeConfigFile.Location = new System.Drawing.Point(484, 486);
            this.btnChangeConfigFile.Name = "btnChangeConfigFile";
            this.btnChangeConfigFile.Size = new System.Drawing.Size(126, 23);
            this.btnChangeConfigFile.TabIndex = 22;
            this.btnChangeConfigFile.Text = "Change Config FIle";
            this.btnChangeConfigFile.UseVisualStyleBackColor = true;
            this.btnChangeConfigFile.Click += new System.EventHandler(this.btnChangeConfigFile_Click);
            // 
            // btnValidateHc
            // 
            this.btnValidateHc.Location = new System.Drawing.Point(580, 445);
            this.btnValidateHc.Name = "btnValidateHc";
            this.btnValidateHc.Size = new System.Drawing.Size(75, 23);
            this.btnValidateHc.TabIndex = 23;
            this.btnValidateHc.Text = "Validate HC";
            this.btnValidateHc.UseVisualStyleBackColor = true;
            this.btnValidateHc.Click += new System.EventHandler(this.btnValidateHc_Click);
            // 
            // btnEncryptKey
            // 
            this.btnEncryptKey.Location = new System.Drawing.Point(17, 561);
            this.btnEncryptKey.Name = "btnEncryptKey";
            this.btnEncryptKey.Size = new System.Drawing.Size(75, 23);
            this.btnEncryptKey.TabIndex = 24;
            this.btnEncryptKey.Text = "Encrypt Key";
            this.btnEncryptKey.UseVisualStyleBackColor = true;
            this.btnEncryptKey.Click += new System.EventHandler(this.btnEncryptKey_Click);
            // 
            // txtPlainKey
            // 
            this.txtPlainKey.Location = new System.Drawing.Point(110, 563);
            this.txtPlainKey.Name = "txtPlainKey";
            this.txtPlainKey.Size = new System.Drawing.Size(206, 20);
            this.txtPlainKey.TabIndex = 25;
            // 
            // txtEncryptedKey
            // 
            this.txtEncryptedKey.Location = new System.Drawing.Point(513, 563);
            this.txtEncryptedKey.Name = "txtEncryptedKey";
            this.txtEncryptedKey.Size = new System.Drawing.Size(281, 20);
            this.txtEncryptedKey.TabIndex = 26;
            // 
            // btnDecryptKey
            // 
            this.btnDecryptKey.Location = new System.Drawing.Point(390, 560);
            this.btnDecryptKey.Name = "btnDecryptKey";
            this.btnDecryptKey.Size = new System.Drawing.Size(75, 23);
            this.btnDecryptKey.TabIndex = 27;
            this.btnDecryptKey.Text = "Decrypt Key";
            this.btnDecryptKey.UseVisualStyleBackColor = true;
            this.btnDecryptKey.Click += new System.EventHandler(this.btnDecryptKey_Click);
            // 
            // UOServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 598);
            this.Controls.Add(this.btnDecryptKey);
            this.Controls.Add(this.txtEncryptedKey);
            this.Controls.Add(this.txtPlainKey);
            this.Controls.Add(this.btnEncryptKey);
            this.Controls.Add(this.btnValidateHc);
            this.Controls.Add(this.btnChangeConfigFile);
            this.Controls.Add(this.btnToggleTriggers);
            this.Controls.Add(this.CheckExtract);
            this.Controls.Add(this.btnFacilityInfo);
            this.Controls.Add(this.btnCHeckPending);
            this.Controls.Add(this.btnDataExtract);
            this.Controls.Add(this.cmdSendAck);
            this.Controls.Add(this.btnDisconnectAll);
            this.Controls.Add(this.btnGetInsurers);
            this.Controls.Add(this.btnWithdraw);
            this.Controls.Add(this.btnSubmitToHCAI);
            this.Controls.Add(this.btnChangeOCFStatus);
            this.Controls.Add(this.btnGetHCAIStuff);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtInternalServerLog);
            this.Controls.Add(this.txtClientInteractionLog);
            this.Controls.Add(this.txtServerName);
            this.Controls.Add(this.btnSendXML);
            this.Controls.Add(this.btnRemoveClinets);
            this.Controls.Add(this.btnSendCommand);
            this.Controls.Add(this.btnStartServer);
            this.Controls.Add(this.btnShutdownServer);
            this.Name = "UOServer";
            this.Text = "UO SERVER";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShutdownServer;
        private System.Windows.Forms.Button btnStartServer;
        private System.Windows.Forms.Button btnSendCommand;
        private System.Windows.Forms.Button btnRemoveClinets;
        private System.Windows.Forms.Button btnSendXML;
        private System.Windows.Forms.TextBox txtServerName;
        private System.Windows.Forms.TextBox txtClientInteractionLog;
        private System.Windows.Forms.TextBox txtInternalServerLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetHCAIStuff;
        private System.Windows.Forms.Button btnChangeOCFStatus;
        private System.Windows.Forms.Button btnSubmitToHCAI;
        private System.Windows.Forms.Button btnWithdraw;
        private System.Windows.Forms.Button btnGetInsurers;
        private System.Windows.Forms.Button btnDisconnectAll;
        private System.Windows.Forms.Button cmdSendAck;
        private System.Windows.Forms.Button btnDataExtract;
        private System.Windows.Forms.Button btnCHeckPending;
        private System.Windows.Forms.Button btnFacilityInfo;
        private System.Windows.Forms.Button CheckExtract;
        private System.Windows.Forms.Button btnToggleTriggers;
        private System.Windows.Forms.Button btnChangeConfigFile;
        private System.Windows.Forms.Button btnValidateHc;
        private System.Windows.Forms.Button btnEncryptKey;
        private System.Windows.Forms.TextBox txtPlainKey;
        private System.Windows.Forms.TextBox txtEncryptedKey;
        private System.Windows.Forms.Button btnDecryptKey;
    }
}

