﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using EdtClient;
using UOServerLib;

namespace CommTest
{
    public partial class UOServer : Form
    {
        private readonly UOServiceLib _uoService;

        private string SQLServerName { get; set; }
        private string DatabaseName { get; set; }

        private string HcaiId { get; set; }
        private string HcaiLogin { get; set; }
        private string HcaiPassword { get; set; }

        public UOServer()
        {
            InitializeComponent();
            //Testing();
            _uoService = new UOServiceLib(null);
        }

        
        private void Testing()
        {
            HCAITableInfo tbl = new HCAITableInfo();

            HCAITableLineItem tmp = new HCAITableLineItem();
            tmp.InvoiceVersion = "aaaaaa";
            tmp.OCFType = "OCF18";
            tmp.OCFID = 4;
            tmp.OCFStatus = 0;
            tmp.StatusTime = "10:20:15 pm";
            tmp.StatusDate = "08/20/2010";
            tbl.AddLineItemToMap(tmp);

            tmp = new HCAITableLineItem();
            tmp.InvoiceVersion = "bbbbbb";
            tmp.OCFType = "OCF22";
            tmp.OCFID = 2;
            tmp.OCFStatus = 0;
            tmp.StatusTime = "08/25/2010 10:20:15 pm";
            tmp.StatusDate = "08/20/2011 10:20:15 pm";
            tbl.AddLineItemToMap(tmp);

            tmp = new HCAITableLineItem();
            tmp.InvoiceVersion = "cccccc";
            tmp.OCFType = "OCF21C";
            tmp.OCFID = 4;
            tmp.OCFStatus = 0;
            tbl.AddLineItemToMap(tmp);

            tmp = new HCAITableLineItem();
            tmp.InvoiceVersion = "ddddddd";
            tmp.OCFType = "OCF22";
            tmp.OCFID = 6;
            tmp.OCFStatus = 0;
            tbl.AddLineItemToMap(tmp);

            tmp = new HCAITableLineItem();
            tmp.InvoiceVersion = "eeeee";
            tmp.OCFType = "OCF18";
            tmp.OCFID = 5;
            tmp.OCFStatus = 0;
            tbl.AddLineItemToMap(tmp);

            tmp = new HCAITableLineItem();
            tmp.InvoiceVersion = "eeeee";
            tmp.OCFType = "OCF21B";
            tmp.OCFID = 1;
            tmp.OCFStatus = 0;
            tbl.AddLineItemToMap(tmp);

            for (int i = 0; i < tbl.GetNumberOfRowsInStatus(0); i++)
            {
                HCAITableLineItem item = tbl.GetLineItemByStatus(0, i);

                Console.WriteLine("type = " + item.OCFType + "; id = " + item.OCFID + ":  " + item.InvoiceVersion);
            }

            XmlDocument doc = tbl.CreateXmlDocument();

            doc.DocumentElement.InnerText = "My Test";

            doc.Save("test.xml");
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            //mUOService.Start();
            SQLServerName = txtServerName.Text = ConfigurationManager.AppSettings[nameof(SQLServerName)];
            DatabaseName = ConfigurationManager.AppSettings[nameof(DatabaseName)];
            HcaiId = ConfigurationManager.AppSettings[nameof(HcaiId)];
            HcaiLogin = ConfigurationManager.AppSettings[nameof(HcaiLogin)];
            HcaiPassword = ConfigurationManager.AppSettings[nameof(HcaiPassword)];
        }     

 
        private void btnShutdownServer_Click(object sender, EventArgs e)
        {
            _uoService.Stop();
            btnStartServer.Enabled = true;
            this.txtInternalServerLog.Text = this.txtInternalServerLog.Text + "Server Stoped" + "\r\n"; 
        }

        private void btnStartServer_Click(object sender, EventArgs e)
        {
            _uoService.Start();
            btnStartServer.Enabled = false;

            this.txtInternalServerLog.Text = this.txtInternalServerLog.Text + "Server Started"  + "\r\n";
        }

        private void btnSendCommand_Click(object sender, EventArgs e)
        {
            //mClientsMut.WaitOne();
            //foreach (object cl in mClients)
            //{
            //    ClientConnection c = (ClientConnection)cl;
            //    UOMessage msg = c.GetCommand();
            //    //ClientConnection client = (ClientConnection)cl;
            //    //client.SendStringToClient("Server Command");

            //}
            //mClientsMut.ReleaseMutex();
        }

        private void btnRemoveClinets_Click(object sender, EventArgs e)
        {
            _uoService.RemoveDeadClients();
        }

        private void btnLoadDBs_Click(object sender, EventArgs e)
        {

        }

        private void btnSendXML_Click(object sender, EventArgs e)
        {
            //ArrayList cls = mUOService.GetClients();

            //foreach (ClientConnection cl in cls)
            //{
            //    cl.SendHcaiTable();
            //}
            //mUOService.ReleaseClients();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _uoService.Stop();
        }

        private void btnSubmitToHCAI_Click(object sender, EventArgs e)
        {
            //submit ocf18
            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
            hcai.submitOCF18("11408");
        }

        private void btnChangeOCFStatus_Click(object sender, EventArgs e)
        {
            //mUOService.CheckAdjusterResponse(null);

            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);

            //hcai.get

            var res = hcai.getAdjusterResponse();
            //mUOService.CheckPending(null);
        }

        private void btnGetHCAIStuff_Click(object sender, EventArgs e)
        {
            //var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
            //// hcai.GetActivityList_ToFindPendingOCFS(); //one call get it all

            //SqlCommand mCommand =  hcai.GetSqlDBConnection();
            //SqlDataReader reader = ExecQuery(mCommand, "select MIN(StatusDate) as MinDate from HCAIStatusLog " +
            //                                           "where IsCurrentStatus = 1 AND (OCFStatus = 3 or OCFStatus = 6 or OCFStatus = 11 or OCFStatus = 10 or OCFStatus = 8 or OCFStatus = 7)");
            //try
            //{
            //    if (reader.Read())
            //    {
            //        DateTime FDate = new DateTime();
            //        DateTime TDate = new DateTime();

            //        FDate = (DateTime)reader["MinDate"];
            //        TDate = FDate;

            //        bool ExitLoop = false;
            //        if (FDate <= DateTime.Now)
            //        {
            //            while (ExitLoop != true)
            //            {
            //                if (FDate != TDate)
            //                {
            //                    FDate = TDate.AddDays(1);
            //                }
            //                TDate = FDate.AddDays(7);
            //                if (TDate >= DateTime.Now)
            //                {
            //                    TDate = DateTime.Now;
            //                    ExitLoop = true;
            //                }
            //                hcai.GetActivityList_ToFindPendingOCFS_ByPeriods(FDate, TDate);
            //            }
            //        }    
            //    }
            //}
            //catch
            //{ 
            //    //what?
            //}
            //reader.Close();
            //reader = null;           
        }

        private void btnWithdraw_Click(object sender, EventArgs e)
        {
            //var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
            //withdraw request
            // hcai.VoidDocumentRequest("279", "OCF18");
        }

        private void btnGetInsurers_Click(object sender, EventArgs e)
        {
            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
            var ls = (InsurerList)hcai.getInsurerList();
        }

        private void ExtractDocument()
        {
            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
        }

        public void OnNewMessageEvent(string msg)
        {
            txtClientInteractionLog.Text += "\r\n" + msg + "\r\n";
        }

        private void btnDisconnectAll_Click(object sender, EventArgs e)
        {
            //ArrayList cls = mUOService.GetClients();

            //foreach (ClientConnection cl in cls)
            //{
            //    cl.CloseConnections();
            //}

            //mUOService.ReleaseClients();
        }

        private void cmdSendAck_Click(object sender, EventArgs e)
        {
            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
            hcai.sendAdjusterResponseAck("18120300077");
        }

        private void btnDataExtract_Click(object sender, EventArgs e)
        {
            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId);
            var doc = hcai.ExtractDocument("22030701115", "FORM1");

            var t = new OCFMapReturnType();
            t = hcai.UpdateDocumentInDb(doc);
        }

        private void btnCHeckPending_Click(object sender, EventArgs e)
        {
            _uoService.CheckPending(1);
        }

        private void btnFacilityInfo_Click(object sender, EventArgs e)
        {
            var hcai = new HCAIModule(SQLServerName, DatabaseName, HcaiLogin, HcaiPassword, HcaiId); //need server, db, etc.

            var val = hcai.getFacilityList();
            var ls = val as FacilityList;
        }

        private void CheckExtract_Click(object sender, EventArgs e)
        {
            new Thread(_uoService.DocumentCheckAndFix).Start(1);
        }

        private void btnToggleTriggers_Click(object sender, EventArgs e)
        {
            //mUOService.ToggleEmailNotificationTriggers("UniversalCPR", @"QUADCORE\SQLEXPRESS", false);
        }

        private void btnChangeConfigFile_Click(object sender, EventArgs e)
        {
            //ModifyConfigFile();
            var list = new List<DateTime>
            {
                new DateTime(2014, 5, 11),
                new DateTime(2014, 5, 12),
                new DateTime(2014, 5, 13),
                new DateTime(2014, 5, 14),
                new DateTime(2014, 5, 15),
                new DateTime(2014, 5, 18),
                new DateTime(2014, 5, 19),
                new DateTime(2014, 5, 25),
                new DateTime(2014, 5, 27),
                new DateTime(2014, 5, 30),
            };

            var list2 = CombineConsecutiveDates(list);
        }

        public static bool ModifyConfigFile()
        {
            var newAdjusterResponseInterval = 3600000;  //one hour
            var cofigFilename = @"uoserver_config.xml";
            if (!File.Exists(cofigFilename))
            {
                Console.WriteLine("Config file '{0}' not found.", Path.Combine(Environment.CurrentDirectory, cofigFilename));
                return false;
            }
            var doc = new XmlDocument();
            try
            {
                doc.Load(cofigFilename);
                var el = (XmlElement)doc.DocumentElement.FirstChild;

                el.SetAttribute("AdjusterResponseInterval", newAdjusterResponseInterval.ToString());

                doc.Save(cofigFilename);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to modify config file: " + ex.Message);
                return false;
            }

            return true;
        }

        private List<ActivityInterval> CombineConsecutiveDates(List<DateTime> listOfDates)
        {
            var counter = 1;
            var result = new List<ActivityInterval>();
            var startDate = new DateTime();
            if (listOfDates.Count > 0)
            {
                var tmp = listOfDates[0];
                startDate = new DateTime(tmp.Year, tmp.Month, tmp.Day);
            }

            if (listOfDates.Count == 1)
            {
                var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(1) };
                result.Add(interval);
                return result;
            }

            for (int c = 1; c < listOfDates.Count; c++)
            {
                var tmp = listOfDates[c];
                tmp = new DateTime(tmp.Year, tmp.Month, tmp.Day);

                
                var dateDiff = tmp - startDate;
                if (dateDiff.TotalDays > counter)
                {
                    var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(counter) };
                    result.Add(interval);

                    counter = 0;
                    startDate = tmp;
                }
                else if(counter > 3)
                {
                    var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(counter - 1) };
                    result.Add(interval);

                    counter = 0;
                    startDate = tmp;
                }
                counter++;

                if(c == listOfDates.Count - 1)
                {
                    var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(counter) };
                    result.Add(interval);

                    counter = 0;
                    startDate = tmp;
                }
            }

            return result;
        }

        private void btnValidateHc_Click(object sender, EventArgs e)
        {
            try
            {
                var request = new HcvRequest
                {
                    HealthNumber = "1286844022",
                    VersionCode = "YX"
                };
                var res = _uoService.ValidateHealthCard(request);
                var x = new XmlSerializer(res.GetType());
                var str = "";
                using (var writer = new StringWriter())
                {
                    x.Serialize(writer, res);
                    str = writer.ToString();
                }

                txtClientInteractionLog.AppendText(str + "\n");

                HcvResult newRes;
                using (var reader = new StringReader(str))
                {
                    newRes = x.Deserialize(reader) as HcvResult;
                }

                txtInternalServerLog.AppendText(newRes.ErrorMessage + "\n");
            }
            catch (Exception ex)
            {
                txtInternalServerLog.AppendText(ex.Message + "\n");
            }
        }

        private void btnEncryptKey_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPlainKey.Text))
                return;

            var encryptedKey = StringCipher.Encrypt(txtPlainKey.Text);
            txtEncryptedKey.Text = encryptedKey;
        }

        private void btnDecryptKey_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEncryptedKey.Text))
                return;

            var plainKey = StringCipher.Decrypt(txtEncryptedKey.Text);
            txtPlainKey.Text = plainKey;
        }
    }

    internal class ActivityInterval
    {
        internal DateTime StartDate { get; set; }
        internal DateTime EndDate { get; set; }
    }
}
