﻿using System.Reflection;

// General Information about an assembly is controlled through the following set of attributes.
// Change these attribute values to modify the information associated with an assembly.
[assembly: AssemblyTitle("UOServerLib")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Antibex")]
[assembly: AssemblyProduct("UOServerLib")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]

// We want this assembly to have a seperate logging repository to the 
// rest of the application. We will configure this repository seperatly.
[assembly: log4net.Config.Repository("UOServerLib")]

// Configure logging for this assembly using the specific file
[assembly: log4net.Config.XmlConfigurator(Watch = true, ConfigFile = "logging.log4net")]