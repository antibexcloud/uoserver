using System;
using System.Collections;
using System.Linq;
using PMSToolkit;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using PMSToolkit.DataObjects;

namespace UOServerLib
{
    public class OCFMapReturnType
    {
        public string HCAI_Document_Number { get; set; }
        public long OCFID { get; set; }
        public string OCFType { get; set; }
        public HcaiStatus New_HCAI_Status { get; set; }
        public float ATotal { get; set; }
        public string StatusDate { get; set; }
        public string StatusTime { get; set; }
        public string MinSentDate { get; set; }
        public bool AdjusterResponseStuck { get; set; }
        public bool ForceAdjusterAck { get; set; }
    }

    public class OCF9LineItems
    {
        public string GSCode { get; set; }
        public double GSAmountClaimed { get; set; }
        public double GSServicessAmountPayable { get; set; }
        public string GSReasonCode { get; set; }
        public string GSReasonCodeDesc { get; set; }
        public string GSReasonCodeDescOther { get; set; }
        public double GSInterestPayable { get; set; }
        public int GSReferenceNumber { get; set; }
        public string GSDescription { get; set; }
    }

    /// <summary>
    /// Summary description for HCAIModule.
    /// </summary>
    public class HCAIModule
    {
        //private SqlConnection mDBConnection;
        private Toolkit pmsTK = new Toolkit();
        //private double mGST = 5;
        //private double mPST = 8;
        //private double mHST = 13;
        private static string mAdjResponseDocNumber = "";
        private static int mAdjResponseDocNumberCounter = 0;
        public int mAdjResponseDocNumberMax { set; get; }

        #region HelperFunctions

        private string FormatProvince(string Province)
        {
            if (Province.Length < 2)
            {
                return Province;
            }
            else
            {
                return MyConvert.MyLeft(Province, 2).ToUpper();
            }
        }

        private string FormatPostalCode(string PostalCode)
        {
            try
            {
                string tPostalCode = PostalCode.Trim();
                tPostalCode = tPostalCode.Replace(" ", "");
                tPostalCode = tPostalCode.ToUpper();

                if (tPostalCode.Length == 5)
                {
                    if (IsNumeric(tPostalCode) == true)
                    {
                        return tPostalCode.ToString();
                    }
                }


                if (tPostalCode.Length >= 6)
                {
                    char[] arr;
                    arr = tPostalCode.ToCharArray(0, tPostalCode.Length);

                    if ((((int)arr[0]) >= 65 && ((int)arr[0]) <= 90) &&
                        (IsNumeric(arr[1].ToString()) == true) &&
                        (((int)arr[2]) >= 65 && ((int)arr[2]) <= 90) &&
                        (IsNumeric(arr[3].ToString()) == true) && (IsNumeric(arr[5].ToString()) == true) &&
                        (((int)arr[4]) >= 65 && ((int)arr[4]) <= 90))
                    {
                        return (arr[0].ToString() + arr[1].ToString() + arr[2].ToString() + " " + arr[3].ToString() + arr[4].ToString() + arr[5].ToString()).ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }

        private string CheckNullString(string s)
        {
            if (s == null)
                return "";
            return s;
        }

        private int ConvertFlagToInt(string m)
        {
            if (string.IsNullOrEmpty(m))
                return 0;

            if (m.StartsWith("y") || m.StartsWith("Y") || m.StartsWith("1"))
                return 1;
            else
                return 0;
        }

        private string Flag(string m)
        {
            if (string.IsNullOrEmpty(m))
                return "";

            if (m.StartsWith("y") || m.StartsWith("Y") || m.StartsWith("1"))
                return "Yes";
            else
                return "No";
        }

        private string FlagNA(string m)
        {
            if (m == null || m == "")
                return "";

            if (m.StartsWith("y") || m.StartsWith("Y") || m.StartsWith("1"))
                return "Yes";
            else if (m.Equals("n") || m.Equals("N") || m.Equals("NO") || m.Equals("No") || m.Equals("no") || m.StartsWith("0"))
                return "No";
            else
                return "NotApplicable";
        }

        private string FlagUNK(string m)
        {
            if (m == null || m == "")
                return "";

            if (m.StartsWith("y") || m.StartsWith("Y"))
                return "Yes";
            else if (m.Equals("n") || m.Equals("N") || m.Equals("NO") || m.Equals("No") || m.Equals("no"))
                return "No";
            else
                return "Unknown";
        }

        private string EmploymentFlag(string m)
        {
            if (m == null || m == "")
                return "";

            if (m.StartsWith("y") || m.StartsWith("Y"))
                return "Yes";
            else if (m.Equals("n") || m.Equals("N") || m.Equals("NO") || m.Equals("No") || m.Equals("no"))
                return "No";
            //else if(m.LastIndexOf("Employed") != -1)
            else if (m.Equals("NOT") || m.Equals("Not") || m.Equals("not"))
                return "NotEmployed";
            else
                return "Unknown";
        }

        private string Gender(string m)
        {
            if (m == null || m == "")
                return "";

            if (m.StartsWith("m") || m.StartsWith("M"))
                return "M";
            else
                return "F";
        }

        private string Phone(string m)
        {
            if (m == null || m == "")
                return "";
            string tmp;
            tmp = m.Replace("(", "");
            tmp = tmp.Replace(")", "");
            tmp = tmp.Replace(" ", "");
            tmp = tmp.Replace("-", "");
            tmp = tmp.Replace(".", "");

            if (tmp.Length > 10)
            {
                //check for leading 1
                if (tmp.Substring(0, 1).ToString() == "1")
                {
                    tmp = tmp.Substring(1, tmp.Length - 1).ToString(); //remove leading 1
                }
            }
            return tmp;
        }

        private string FlagTax(string m)
        {
            if (m == null || m == "")
            {
                return "No";
            }
            else
            {
                int tax = 0;
                try
                {
                    tax = Convert.ToInt32(m.ToString());
                    if (tax > 0)
                        return "Yes";
                    else
                        return "No";
                }
                catch //(FormatException e)
                {
                    return "No";
                }
            }
        }

        #endregion

        private string mHCAILogin;
        private string mHCAIPassword;
        private string mHCAIID;
        private MessageType mType = new MessageType();

        public string DB_CONNECTION_STRING;

        //private SqlCommand mCommand;
        private string mDBName;
        private string mSqlServerName;

        //VERSION_2.0_beta
        private readonly string OCFVersion = "3";
        //end
        #region HELP FUNCTIONS
        private string CreateErrorEmailBody(string action, string hcaiN, string errormsg, string hcaierrormsg)
        {
            string ret = "";
            ret += "HCAI ID: " + mHCAIID + "\r\n";
            ret += "HCAI Login: " + mHCAILogin + "\r\n";
            ret += "HCAI Password: " + mHCAIPassword + "\r\n";
            ret += "Action Performed: " + action + "\r\n";
            ret += "OCF HCAI Number: " + hcaiN + "\r\n";
            ret += "Error Message: " + errormsg + "\r\n";
            ret += "HCAI Error Message: " + hcaierrormsg + "\r\n";
            return ret;
        }

        private SqlDataReader ExecQuery(SqlCommand mCommand, string StrSql)
        {
            SqlDataReader reader = null;
            try
            {
                mCommand.CommandText = StrSql;
                reader = mCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                Debugger.LogError($"{nameof(ExecQuery)}: {ex.Message}");
            }
            return reader;
        }

        private string GetOCFSqlQuery(string OCFType, string OCFID)
        {
            string str_sql;
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")

                str_sql = "select I.InvoiceID, I.CaseID, I.InvoiceType, I.InvoiceDate, I.InvoiceTerms, I.InvoiceGST, I.InvoicePST, I.InvoiceTotal, " +
                    "I.InvoiceComments, I.Interest, I.Version, I.InvoiceStatus, I.InvoiceSentDate, I.InvoiceEditDate, I.InvoiceAccessDate, I.MVATotal, I.ToPrint, " +
                    "I.InvoiceReplyDate, I.RepliedAs, I.ApprovedAmt as InvoiceApprovedAmt, I.InvoiceID_FC_Ref, I.BatchNo, I.TrackingID, I.CategoryID, I.chkShowProviders, " +
                    "I.ReportID, I.DefaultInvoiceTo, I.HCAIVersion, " +
                    "M.InvoiceVersion, M.FirstInvoice, M.LastInvoice, M.PlanType, M.PlanDate, M.PlanNumber, M.ApprovedAmt, M.PrevBilled, M.ReportID as MReportID, " +
                    "M.PriorBalance, M.PaymentReceived, M.OverdueAmount, " +
                    "M.AProviderName, M.AProviderID, M.AType, M.ARegulated, M.ANumber, M.AHourRate, M.AOccupation, " +
                    "M.BProviderName, M.BProviderID, M.BType, M.BRegulated, M.BNumber, M.BHourRate, M.BOccupation, " +
                    "M.CProviderName, M.CProviderID, M.CType, M.CRegulated, M.CNumber, M.CHourRate, M.COccupation, " +
                    "M.DProviderName, M.DProviderID, M.DType, M.DRegulated, M.DNumber, M.DHourRate, M.DOccupation, " +
                    "M.EProviderName, M.EProviderID, M.EType, M.ERegulated, M.ENumber, M.EHourRate, M.EOccupation, " +
                    "M.FProviderName, M.FProviderID, M.FType, M.FRegulated, M.FNumber, M.FHourRate, M.FOccupation, " +
                    "M.OHIP, M.EHC1And2, M.CIFirstName, M.CILastName, M.CIMiddleName, M.CIDOB, M.CIGender, M.CIAddress, M.CICity, M.CIProvince, M.CIPostalCode, M.CITel, M.CIExt, M.CIClaimNo, M.CIPolicyNo, M.CIDOL, " +
                    "M.IIName, M.IIAddress, M.IICity, M.IIProvince, M.IIPostalCode, M.IIAdjFirstName, M.IIAdjLastName, M.IIAdjTel, M.IIAdjExt, M.IIAdjFax, M.IIPHolderSame, M.IIPHolderLastName, M.IIPHolderFirstName, " +
                    "M.qEHC, M.qOHIP, M.EHC1, M.EHC1PlanMember, M.EHC1PolicyNo, M.EHC1IDCertNo, M.EHC2, M.EHC2PlanMember, M.EHC2PolicyNo, M.EHC2IDCertNo, M.SignedBy, M.iDate, M.Comments, M.Facility, " +
                    "M.FIPayeeFirstName, M.FIPayeeLastName, M.FIPayeeNumber, M.FIAddress, M.FICity, M.FIProvince, M.FIPostalCode, M.FITel, M.FIExt, M.FIFax, M.FIAISINo, M.FIPayTo, M.FINoConflicts, M.FIDaclare, M.FIDaclareText, M.FIEmail, " +
                    "M.OHIPChiro, M.OHIPPhysio, M.OHIPMassage, M.OHIPX, M.EHC1Chiro, M.EHC1Physio, M.EHC1Massage, M.EHC1X, M.EHC2Chiro, M.EHC2Physio, M.EHC2Massage, M.EHC2X, M.XService, M.PrintInjuries, M.PrintProviders, M.staff_id, M.chkAttachments, " +
                    "M.AdditionalComments, M.AProviderHCAINo, M.AProviderHCAINo, M.BProviderHCAINo, M.CProviderHCAINo, M.DProviderHCAINo, M.EProviderHCAINo, M.FProviderHCAINo, M.HCAIDocumentNumber, " +
                    "M.MVAInsurerHCAIID, M.MVABranchHCAIID, M.MOHTotalApprovedLineCost, M.MOHTotalApprovedReasonCode, M.MOHTotalApprovedReasonCodeDesc, M.MOHTotalApprovedReasonCodeDescOther, " +
                    "M.EHC1TotalApprovedLineCost, M.EHC1TotalApprovedReasonCode, M.EHC1TotalApprovedReasonCodeDesc, M.EHC1TotalApprovedReasonCodeDescOther, " +
                    "M.EHC2TotalApprovedLineCost, M.EHC2TotalApprovedReasonCode, M.EHC2TotalApprovedReasonCodeDesc, M.EHC2TotalApprovedReasonCodeDescOther, " +
                    "M.InsurerTotalMOHApproved, M.InsurerTotalEHC12Approved, M.AutoInsurerTotalApproved, M.GSTApprovedLineCost, M.GSTApprovedReasonCode, M.GSTApprovedReasonCodeDesc, M.GSTApprovedReasonCodeDescOther, " +
                    "M.PSTApprovedLineCost, M.PSTApprovedReasonCode, M.PSTApprovedReasonCodeDesc, M.PSTApprovedReasonCodeDescOther, M.SubtotalGSApproved, M.TotalInteresedApproved, M.SubTotalOtherGS, M.RefOfHCAI_PlanNumber, " +
                    "M.IsAmountRefused, M.OHIPChiro_Debit, M.OHIPPhysio_Debit, M.OHIPMassage_Debit, M.OHIPX_Debit, M.EHC1Chiro_Debit, M.EHC1Physio_Debit, M.EHC1Massage_Debit, M.EHC1X_Debit, M.EHC2Chiro_Debit, M.EHC2Physio_Debit, M.EHC2Massage_Debit, M.EHC2X_Debit, " +
                    "M.MOHTotalApprovedLineCost_Debit, M.MOHTotalApprovedReasonCode_Debit, M.MOHTotalApprovedReasonCodeDesc_Debit, M.MOHTotalApprovedReasonCodeDescOther_Debit, M.EHC1TotalApprovedLineCost_Debit, M.EHC1TotalApprovedReasonCode_Debit, M.EHC1TotalApprovedReasonCodeDesc_Debit, " +
                    "M.EHC1TotalApprovedReasonCodeDescOther_Debit, M.EHC2TotalApprovedLineCost_Debit, M.EHC2TotalApprovedReasonCode_Debit, M.EHC2TotalApprovedReasonCodeDesc_Debit, M.EHC2TotalApprovedReasonCodeDescOther_Debit, M.FacilityIsPayee, " +
                    "(select max(H.HCAIDocNumber) from HCAIStatusLog H where H.IsCurrentStatus = 1 AND H.ReportID = M.ReportID AND H.ReportID > 0)  as pHCAIDocNumber " +
                    "from InvoceGeneralInfo I inner join MVAInvoice M on I.InvoiceID = M.InvoiceID where I.InvoiceID = " + OCFID;
            else
                str_sql = "select * from " + MyConvert.MyLeft(OCFType, 5) + " where ReportID = " + OCFID;

            return str_sql;
            //string str_sql;
            //if (MyLeft(OCFType, 5) == "OCF21")
            //    str_sql = "select * from InvoceGeneralInfo I inner join MVAInvoice M on I.InvoiceID = M.InvoiceID where I.InvoiceID = " + OCFID;
            //else
            //    str_sql = "select * from " + MyLeft(OCFType, 5) + " where ReportID = " + OCFID;

            //return str_sql;
        }


        private string GetClientIDByOCFID(SqlCommand mCommand, string OCFType, string OCFID)
        {
            SqlDataReader reader;
            string str_sql;
            string ClientID = "";

            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                str_sql = "select DISTINCT C.client_id from Client C inner join (Client_Case CC inner join InvoceGeneralInfo P on CC.CaseID = P.CaseID) on CC.client_id = C.client_id where P.InvoiceID = " + OCFID;
            else
                str_sql = "select DISTINCT C.client_id from Client C inner join (Client_Case CC inner join PaperWork P on CC.CaseID = P.CaseID) on CC.client_id = C.client_id where P.ReportID = " + OCFID;

            try
            {
                mCommand.CommandText = str_sql;
                reader = mCommand.ExecuteReader();
                if (reader.Read() == true)
                {
                    ClientID = MyReaderFldToString(reader, "client_id");
                }
                else
                {
                    //error
                }
                reader.Close();
                //reader = null;
            }
            catch //(Exception ex)
            {
                //WRITE ERROR LOG
                //MessageBox.Show(ex.Message, "Universal Office Text Editor - Error");
            }
            //reader.Close();
            reader = null;

            return ClientID;
        }



        //private string GetMyTimeFormat(DateTime tTime)
        //{
        //    string tHour = "";
        //    string tMin = "";
        //    string tExt = "";

        //    if (tTime.Hour == 12 || tTime.Hour == 0)
        //        tHour = (12).ToString();
        //    else
        //        tHour = (tTime.Hour%12).ToString();

        //    tMin = tTime.Minute.ToString();

        //    if (tTime.Hour < 12)
        //        tExt = "AM";
        //    else
        //        tExt = "PM";

        //    return tHour + ":" + tMin + ": " + tExt;        
        //}

        private string GetHCAIProviderOccupation(SqlCommand mCommand, string Occupation)
        {
            SqlDataReader reader;
            string HCAIProviderCode = "";
            try
            {
                mCommand.CommandText = "select * from StaffTypes where StaffType = 1 AND Occupation = '" + Occupation.Replace("'", "''") + "'";
                reader = mCommand.ExecuteReader();
                if (reader.Read() == true)
                {
                    HCAIProviderCode = MyReaderFldToString(reader, "MVACode");
                    //reader.Close();
                }
                else
                {
                    //error
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Debugger.LogError($"{nameof(GetHCAIProviderOccupation)}: {ex.Message}");
            }
            reader = null;

            return HCAIProviderCode;
        }

        private string ReturnConflictsOpt(string c)
        {
            if (c == null || c == "")
                return "";

            if (c.StartsWith("y") || c.StartsWith("Y") || c.StartsWith("0"))
                return "Yes";
            else
                return "No";
        }

        #endregion

        public string BusinessName { get; set; }
        public string BusinessPhone { get; set; }
        public string WebID { get; set; }

        private double GetGovTax(SqlDataReader reader, string fldName)
        {
            try
            {
                if (reader.Read())
                {
                    return MyConvert.ConvertStrToDouble(reader[fldName].ToString());
                }

                return 0;
            }
            catch
            {
                return 0;
            }

        }

        public HCAIModule(string ServerName, string DBName, string HCAILogin, string HCAIPassword, string HCAIID)
        {
            DB_CONNECTION_STRING = @"Database=" + DBName + ";User Id=uouser; password=passc0de;Server=" + ServerName;

            mHCAILogin = HCAILogin; // "antibexpms";
            mHCAIPassword = HCAIPassword; // "DLJYKPVG";
            mHCAIID = HCAIID;

            mDBName = DBName;
            mSqlServerName = ServerName;

            OCFVersion = "3";

            Debugger.WriteToFile("OCF Version is set to " + OCFVersion, 10);
            //            mAdjResponseDocNumberCounter = 0;
            //            mAdjResponseDocNumber = "";
            mAdjResponseDocNumberMax = 1000;

#if LOG_ADRES
            File.AppendAllText("C:\\Program Files\\Antibex\\Universal CPR\\AdResponses.log", "NEW SESSION");
#endif

#if DEBUG_FILE_WRITE
			StreamWriter mFd;
			mFd = File.CreateText("c:\\testinghcai.txt");
			mFd.Flush();
			mFd.Close();
#endif
            //WriteLine("HCAI...............");
        }

        public void Finilize()
        {
            //			mFd.WriteLine("Destructor was called");

        }

        private void DebugWrite(string s)
        {
#if DEBUG_FILE_WRITE
			StreamWriter mFd;
			mFd = File.AppendText("c:\\testinghcai.txt");
			mFd.WriteLine(s);
			mFd.Flush();
			mFd.Close();
#endif
        }

        public void setIdLoginPass(string id, string login, string pass)
        {
            mHCAILogin = login;
            mHCAIPassword = pass;
        }

        private string KeyFormat(string s)
        {
            return s.Replace("/", " ");
        }

        public bool IsCredentialsValid()
        {
            return !string.IsNullOrEmpty(mHCAILogin) && !string.IsNullOrEmpty(mHCAIPassword);
        }

        private void GetErrorsAndPopulateErrorList(IError err, ErrorMessage e, LineItemType type)
        {
            var headLi = err.getLineItemErrorList(type);

            //OCF18 Session Header errors:
            foreach (var lie in headLi.Cast<ILineItemError>())
            {
                var tmp = KeyFormat(lie.getKey()) + "; " + lie.getDescription();
                e.Error_ErrorList = tmp;
                e.Error_CompleteErrorDesc += tmp + "\r\n";
            }
        }

        private void PopulateErrorMessages(ErrorMessage e)
        {
            e.Error_CompleteErrorDesc = "";
            var err = pmsTK.getError();

            var tmp = err.getErrorType() + ":?:" + err.getErrorDescription();
            e.Error_HCAI_ErrorType = tmp;

            //if (err.getErrorType() == "109")
            //    e.Error_CompleteErrorDesc = "Authentication Error: Check login and password\r\n";

            var errList = err.getErrorList();

            //Common Errors
            foreach (var t in errList)
            {
                var li = (PMSToolkit.ToolkitError.ErrorDescription)t;
                tmp = KeyFormat(li.getKey());
                tmp = tmp + "; " + li.getDescription();
                e.Error_ErrorList = tmp;
                e.Error_CompleteErrorDesc += tmp + "\r\n";
            }

            //Injury Code Errors
            var injL = err.getLineItemErrorList(LineItemType.InjuryLineItem);
            foreach (var inj in injL.Cast<ILineItemError>())
            {
                tmp = KeyFormat(inj.getKey());
                tmp = tmp + ";" + inj.getDescription();
                e.Error_ErrorList = tmp;
                e.Error_CompleteErrorDesc += tmp + "\r\n";
            }

            //OCF18 errors
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS18SessionHeaderLineItem);
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS18SessionLineItem);
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS18NonSessionLineItem);
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS18LineItem);

            //OCF22 errors
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS22LineItem);

            //OCF23 errors
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS23PAFLineItem);
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS23OtherGSLineItem);

            //OCF21C
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS21COtherReimbursableLineItem);
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS21CPAFReimbursableLineItem);
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS21CRenderedGSLineItem);

            //OCF21B
            GetErrorsAndPopulateErrorList(err, e, LineItemType.GS21BLineItem);

            //Form1
            GetErrorsAndPopulateErrorList(err, e, LineItemType.AACNServicesLineItem);
        }

        private void ParseError(ErrorMessage e)
        {
#if DEBUG_FILE_WRITE
			StreamWriter fd = File.CreateText("ErrorLog.txt"); //AppendText("ErrorLog.txt");

			IError err = pmsTK.getError();
			string errDescr = err.getErrorDescription();
			System.Console.Write(errDescr);
			string errType = err.getErrorType();
			int errCode = err.getValidationErrorCode();
			IList errList = err.getErrorList();

			fd.Write("\r\nNew Entry : ");
			fd.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());

			fd.WriteLine("Error code: " + errCode);
			fd.WriteLine("Error type: " + errType);
			fd.WriteLine("Error Description: " + errDescr);
			fd.WriteLine("");

			Queue mq = new Queue();

			string s = e.Error_ErrorList;

			mq.Enqueue(s);

			while( s != ":?:")
			{
				fd.WriteLine(s);
				fd.WriteLine("---------------");
				fd.WriteLine("");
				s = e.Error_ErrorList;
				mq.Enqueue(s);
			}

			s = (string)mq.Dequeue();
			while( s != ":?:")
			{
				e.Error_ErrorList = s;
				s = (string)mq.Dequeue();
			}
/*

			if(errList.Count > 0)
				fd.WriteLine(" Common Errors:");

			for( int i = 0; i < errList.Count; i++)
			{
				li = (PMSToolkit.ToolkitError.ErrorDescription)errList[i];
				fd.WriteLine(KeyFormat(li.getKey()));
				fd.WriteLine(li.getDescription());
				fd.WriteLine("---------------");
			}

			//Injury LineItem
			IList injL = err.getLineItemErrorList(PMSToolkit.LineItemType.InjuryLineItem);
			PMSToolkit.ILineItemError inj;
			if(injL.Count > 0)
				fd.WriteLine(" Injury Code errors:");

			for( int i = 0; i < injL.Count; i++)
			{
				inj = (PMSToolkit.ILineItemError)injL[i];
				fd.WriteLine(KeyFormat(inj.getKey()));
				fd.WriteLine(inj.getDescription());
				fd.WriteLine(inj.getLineItemIndex());
				fd.WriteLine("**************");
			}

			//OCF18
			IList headLi = err.getLineItemErrorList(PMSToolkit.LineItemType.GS18SessionHeaderLineItem);
			PMSToolkit.ILineItemError lie;
			if(headLi.Count > 0)
				fd.WriteLine(" OCF18 Session Header errors:");

			for( int i = 0; i < headLi.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)headLi[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}

			
			
			IList lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS18SessionLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF18 Session Line Item Errors:");
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}


			//OCF22
			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS22LineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF22 Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}


			//OCF23
			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS23PAFLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF23 PAF Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}

			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS23OtherGSLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF23 OtherGS Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}

			//OCF21C
			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS21COtherReimbursableLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF21C OtherReimbursable Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}

			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS21CPAFReimbursableLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF21C PAFReimbursable Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}

			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS21CRenderedGSLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF21C RenderedGS Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}

			//OCF21B
			lI = err.getLineItemErrorList(PMSToolkit.LineItemType.GS21BLineItem);
			if(lI.Count > 0)
				fd.WriteLine(" OCF21B Line Item Errors:");
			
			for( int i = 0; i < lI.Count; i++)
			{
				lie = (PMSToolkit.ILineItemError)lI[i];
				fd.WriteLine(KeyFormat(lie.getKey()));
				fd.WriteLine(lie.getDescription());
				fd.WriteLine(lie.getLineItemIndex());
				fd.WriteLine("**************");
			}
*/

			fd.Close();
#endif
        }


        public IDataType getInsurerList()
        {
            ErrorMessage e = new ErrorMessage();
            e.MessageType = mType.Error;
            e.Error_ErrorType = mType.RequestInsurerList;
            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. getInsurerList aborted.");
                e.Error_CompleteErrorDesc = "Hcai credentials are empty.";
                e.MessageDate = DateTime.Now.ToString("MM/dd/yyyy");
                e.MessageTime = DateTime.Now.ToString("hh:mm:ss tt");
                return e;
            }

            InsurerList il;

            IList insList;
            IList branchList;
            IDataItem branch;
            IDataItem insurer;
            var insKeys = new InsurerLineItemKeys();
            var branchKey = new BranchLineItemKeys();

            IDataItem insurers = pmsTK.insurerList(mHCAILogin, mHCAIPassword);

            DateTime dt = DateTime.Now;
            e.MessageDate = dt.ToString("MM/dd/yyyy");
            e.MessageTime = dt.ToString("hh:mm:ss tt");

            if (insurers == null)
            {
                var err = pmsTK.error;
                string err_msg = string.Format("{0} {1}", err.getErrorDescription(), err.getErrorType());
                PopulateErrorMessages(e);

                ParseError(e);
                Debugger.WriteToFile("HCAI returned empty insurer list. " + e.Error_ErrorList, 50);
                Debugger.WriteToFile("LOGIN: " + mHCAILogin + " pass: " + mHCAIPassword + " : " + err_msg, 50);

                return e;
            }
            else
            {
                insList = insurers.getList(PMSToolkit.LineItemType.InsurerLineItem);
                if (insList == null)
                {
                    PopulateErrorMessages(e);

                    ParseError(e);

                    Debugger.WriteToFile("Insurer list is empty. " + e.Error_ErrorList, 50);
                    return e;
                }
                else
                {
                    il = new InsurerList(insList.Count);
                    il.MessageDate = dt.ToString("MM/dd/yyyy");
                    il.MessageTime = dt.ToString("hh:mm:ss tt");
                    il.MessageType = mType.ReceivedInsurerList;

                    for (int i = 0; i < insList.Count; i++)
                    {
                        Insurer ins = new Insurer();
                        insurer = (IDataItem)insList[i];
                        ins.Insurer_ID = insurer.getValue(insKeys.Insurer_ID);
                        ins.Insurer_Name = insurer.getValue(insKeys.Insurer_Name);
                        ins.Insurer_Status = insurer.getValue(insKeys.Insurer_Status);

                        branchList = insurer.getList(PMSToolkit.LineItemType.BranchLineItem);


                        Debugger.WriteToFile("HCAI Insurer_Name " + ins.Insurer_Name, 50);

                        if (branchList == null)
                        {
                            PopulateErrorMessages(e);

                            ParseError(e);
                            return e;
                        }
                        else
                        {
                            for (int j = 0; j < branchList.Count; j++)
                            {
                                var ib = new InsurerBranch();
                                branch = (IDataItem)branchList[j];
                                ib.Branch_ID = branch.getValue(branchKey.Branch_ID);
                                ib.Branch_Name = branch.getValue(branchKey.Branch_Name);
                                ib.Branch_Status = branch.getValue(branchKey.Branch_Status);

                                ins.Branches = ib;
                            }
                        }

                        il.SetInsurer(ins, i);
                    }
                }
            }
            return il;
        }


        public IDataType getFacilityList()
        {
            var e = new ErrorMessage
            {
                MessageType = mType.Error,
                Error_ErrorType = mType.RequestFacilityInfo
            };

            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. getFacilityList aborted.");
                e.Error_CompleteErrorDesc = "Hcai credentials are empty.";
                e.MessageDate = DateTime.Now.ToString("MM/dd/yyyy");
                e.MessageTime = DateTime.Now.ToString("hh:mm:ss tt");
                return e;
            }

            FacilityList fl;

            var providerKeys = new PMSToolkit.DataObjects.ProviderLineItemKeys();
            var regKeys = new PMSToolkit.DataObjects.RegistrationLineItemKeys();
            var facilityKeys = new PMSToolkit.DataObjects.FacilityKeys();


            var facility = pmsTK.facilityInfo(mHCAILogin, mHCAIPassword);

            var dt = DateTime.Now;
            e.MessageDate = dt.ToString("MM/dd/yyyy");
            e.MessageTime = dt.ToString("hh:mm:ss tt");

            if (facility == null)
            {
                PopulateErrorMessages(e);

                ParseError(e);
                Debugger.WriteToFile("HCAI returned empty facility list. " + e.Error_ErrorList, 50);
                return e;
            }
            else
            {
                var facList = facility.getList(PMSToolkit.LineItemType.FacilityLineItem);
                if (facList == null)
                {
                    PopulateErrorMessages(e);

                    ParseError(e);
                    return e;
                }

                if (facList.Count == 0)
                {
                    facList = facility.getList(PMSToolkit.LineItemType.FacilityLineItem);
                }

                if (facList == null)
                {
                    PopulateErrorMessages(e);

                    ParseError(e);
                    return e;
                }

                fl = new FacilityList(facList.Count)
                         {
                             MessageDate = dt.ToString("MM/dd/yyyy"),
                             MessageTime = dt.ToString("hh:mm:ss tt"),
                             MessageType = mType.ReceivedFacilityInfo
                         };
                for (var k = 0; k < facList.Count; k++)
                {
                    var f = new Facility();
                    var fcl = (IDataItem)facList[k];

                    f.HCAI_Facility_Registry_ID = fcl.getValue(facilityKeys.HCAI_Facility_Registry_ID);
                    f.Facility_Name = fcl.getValue(facilityKeys.Facility_Name);
                    f.Billing_Address_Line_1 = fcl.getValue(facilityKeys.Billing_Address_Line_1);
                    f.Billing_Address_Line_2 = fcl.getValue(facilityKeys.Billing_Address_Line_2);
                    f.Billing_City = fcl.getValue(facilityKeys.Billing_City);
                    f.Billing_Province = fcl.getValue(facilityKeys.Billing_Province);
                    f.Billing_Postal_Code = fcl.getValue(facilityKeys.Billing_Postal_Code);

                    f.SameServiceAddress = fcl.getValue(facilityKeys.SameServiceAddress);
                    if (f.SameServiceAddress.ToLower().Contains("no"))
                    {
                        f.Service_Address_Line_1 = fcl.getValue(facilityKeys.Service_Address_Line_1);
                        f.Service_Address_Line_2 = fcl.getValue(facilityKeys.Service_Address_Line_2);
                        f.Service_City = fcl.getValue(facilityKeys.Service_City);
                        f.Service_Province = fcl.getValue(facilityKeys.Service_Province);
                        f.Service_Postal_Code = fcl.getValue(facilityKeys.Service_Postal_Code);
                    }

                    f.AISI_Facility_Number = fcl.getValue(facilityKeys.AISI_Facility_Number);
                    f.Telephone_Number = fcl.getValue(facilityKeys.Telephone_Number);
                    f.Fax_Number = fcl.getValue(facilityKeys.Fax_Number);

                    f.Authorizing_Officer_First_Name = fcl.getValue(facilityKeys.Authorizing_Officer_First_Name);
                    f.Authorizing_Officer_Last_Name = fcl.getValue(facilityKeys.Authorizing_Officer_Last_Name);
                    f.Authorizing_Officer_Title = fcl.getValue(facilityKeys.Authorizing_Officer_Title);
                    f.Lock_Payable_Flag = fcl.getValue(facilityKeys.Lock_Payable_Flag);


                    var licenseList = fcl.getList(PMSToolkit.LineItemType.FacilityLicenseLineItem);
                    if (licenseList == null)
                    {
                        PopulateErrorMessages(e);
                        ParseError(e);
                        return e;
                    }
                    var licenseKeys = new PMSToolkit.DataObjects.FacilityLicenseLineItemKeys();

                    for (int i = 0; i < licenseList.Count; i++)
                    {
                        var lsItem = new FacilityLicense();
                        var license = (IDataItem)licenseList[i];
                        lsItem.License_Number = license.getValue(licenseKeys.License_Number);
                        lsItem.License_Status = license.getValue(licenseKeys.License_Status);
                        lsItem.License_End_Date = license.getValue(licenseKeys.License_End_Date);
                        f.Licenses = lsItem;
                    }


                    var providerList = fcl.getList(PMSToolkit.LineItemType.ProviderLineItem);
                    if (providerList == null)
                    {
                        PopulateErrorMessages(e);

                        ParseError(e);
                        return e;
                    }

                    for (int i = 0; i < providerList.Count; i++)
                    {
                        var pr = new ProviderList();
                        var provider = (IDataItem)providerList[i];
                        pr.Provider_First_Name = provider.getValue(providerKeys.Provider_First_Name);
                        pr.Provider_Last_Name = provider.getValue(providerKeys.Provider_Last_Name);
                        pr.HCAI_Provider_Registry_ID = provider.getValue(providerKeys.HCAI_Provider_Registry_ID);

                        //VERSION_2.0_beta
                        pr.Provider_Status = provider.getValue(providerKeys.Provider_Status);
                        pr.Provider_Start_Date = provider.getValue(providerKeys.Provider_Start_Date);
                        pr.Provider_End_Date = provider.getValue(providerKeys.Provider_End_Date);
                        //end

                        // Console.WriteLine(pr.Provider_Last_Name + ", " + pr.Provider_First_Name + " No: " + pr.HCAI_Provider_Registry_ID);  

                        var providerRegList = provider.getList(PMSToolkit.LineItemType.RegistrationLineItem);
                        if (providerRegList == null)
                        {
                            PopulateErrorMessages(e);

                            ParseError(e);
                            return e;
                        }
                        else
                        {
                            for (int j = 0; j < providerRegList.Count; j++)
                            {
                                ProviderRegistration prr = new ProviderRegistration();
                                var provReg = (IDataItem)providerRegList[j];
                                prr.Profession = provReg.getValue(regKeys.Profession);
                                prr.Registration_Number = provReg.getValue(regKeys.Registration_Number);

                                pr.ProviderReg = prr;
                            }
                        }

                        f.Providers = pr;
                    }
                    fl.SetFacility(f, k);
                }
            }
            return fl;
        }

        #region MY STRING TO NUMBER, DATE/TIME CONVESIONS
        private int ConvertStrToInt(string tStr)
        {
            if (IsNumeric(tStr) == true)
            {
                try
                {
                    string t = tStr.Replace("$", "").Replace(" ", "");
                    return (int)Convert.ToInt32(t);
                }
                catch
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        private long ConvertStrToLong(string tStr)
        {
            if (IsNumeric(tStr) == true)
            {
                try
                {
                    string t = tStr.Replace("$", "").Replace(" ", "");
                    return (long)Convert.ToInt64(t);
                }
                catch
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        private double ConvertStrToDouble(string tStr)
        {
            if (IsNumeric(tStr) == true)
            {
                try
                {
                    string t = tStr.Replace("$", "").Replace(" ", "");
                    return Convert.ToDouble(t);
                }
                catch
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }


        private string ConvertToHCAIDateFormat(string tDate)
        {
            if (tDate != null && tDate != "")
            {
                try
                {
                    DateTime dDate = DateTime.Parse(tDate, System.Globalization.CultureInfo.InvariantCulture);
                    return dDate.ToString("yyyyMMdd");
                }
                catch
                {
                    return "";
                }
            }
            else
            {
                return "";
            }

        }

        private string MyReplace(string str)
        {
            if (str == null)
            {
                return "";
            }
            else
            {
                return str.Replace("'", "''");
            }

        }

        private string MyReaderFldToString(SqlDataReader reader, string fldName)
        {
            try
            {
                if (reader[fldName] != null)
                {
                    var res = reader[fldName].ToString();
                    if (res.Any(c => c < 32 && c != 0x0A && c != 0x0D && c != 0x09))
                    {
                        var tmp = Regex.Replace(res, @"[^\u0000-\u007F]", String.Empty);
                        res = new string(res.Where(c => c >= 32 || c == 0x0A && c == 0x0D && c == 0x09).ToArray());
                    }
                    return res.Trim();
                }
                else
                    return "";
            }
            catch (Exception e)
            {
                Debugger.LogError("completing ocf fields " + fldName + " failed. " + e.Message, e);
                return "";
            }
        }

        private string MyReaderFldToDateString(SqlDataReader reader, string fldName)
        {
            try
            {
                DateTime t;

                if (reader[fldName] != null && reader[fldName].ToString() != "")
                {
                    t = (DateTime)reader[fldName];
                    //Console.WriteLine(t.ToString("yyyyMMdd"));
                    return t.ToString("yyyyMMdd");
                }
                else
                {
                    // Console.WriteLine(reader[fldName].ToString());
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }



        private string MyReaderFldToDateStringMMDDYYYY(SqlDataReader reader, string fldName)
        {
            try
            {
                DateTime t;

                if (reader[fldName] != null && reader[fldName].ToString() != "")
                {
                    t = (DateTime)reader[fldName];
                    return t.ToString("MM/dd/yyyy");
                }
                else
                {
                    // Console.WriteLine(reader[fldName].ToString());
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }

        #endregion

        #region UPDATE OCF DOCUMENT STATUS

        private bool ExecQueryForWrite(SqlCommand mCommand, string StrSql)
        {

            bool IsSucc = false;
            try
            {
                mCommand.CommandText = StrSql;
                mCommand.ExecuteNonQuery();

                IsSucc = true;
            }
            catch (Exception ex)
            {
                Debugger.LogError("Query exe error: sql = '" + StrSql + "'. " + ex.Message, ex);
                IsSucc = false;
            }
            return IsSucc;
        }


        public bool RunSqlTransaction(string StrSql)
        {
            bool IsSucc = false;
            SqlConnection myConnection = new SqlConnection(DB_CONNECTION_STRING);
            myConnection.Open();

            SqlCommand myCommand = myConnection.CreateCommand();

            SqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted, "SampleTransaction");
            // Must assign transaction object and connection to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            try
            {
                myCommand.CommandText = StrSql;
                myCommand.ExecuteNonQuery();
                myTrans.Commit();
                //Console.WriteLine("record is written to database.");
                IsSucc = true;
            }
            catch (Exception e)
            {
                Debugger.LogError("Sql transaction " + StrSql + "failed. " + e.Message, e);
                try
                {
                    myTrans.Rollback("SampleTransaction");
                }
                catch (SqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        Debugger.LogError("Sql rollback transaction " + StrSql + "failed. " + e.Message, e);
                        Debugger.LogError("An exception of type " + ex.GetType() + " was encountered while attempting to roll back the transaction.");
                    }
                }

                Debugger.LogError("An exception of type " + e.GetType() +
                                  $" was encountered while inserting the data.{Environment.NewLine}" +
                                  $"record was written to database.");
            }
            finally
            {
                myConnection.Close();
            }
            return IsSucc;
        }


        private int ConvertHCAIStatusToPaperworkStatus(int Status)
        {

            if (Status == 3 || Status == 6 || Status == 11 || Status == 10 || Status == 8 || Status == 7)
                return 1;
            else if (Status == 12 || Status == 13 || Status == 14 || Status == 15)
                return 2;
            else //if (Status == 0 || Status == 1 || Status == 2 || Status == 4 || Status == 5 || Status == 9)
                return 0;
        }

        private string ConvertHCAIDocStatusToSring(int Status)
        {
            if (Status == 0 || Status == 1 || Status == 2 || Status == 4 || Status == 5 || Status == 9)
                return "Created";
            else if (Status == 3 || Status == 6 || Status == 11 || Status == 10 || Status == 8 || Status == 7)
                return "Sent";
            else if (Status == 12)
                return "Not Approved";
            else if (Status == 13)
                return "Partially Approved";
            else if (Status == 14)
                return "Approved";
            else if (Status == 15)
                return "Deemed Approved";
            else
                return "";
        }

        private int GetCurrentOCFStatus(string OCFType, SqlCommand mCommand, long OCFID)
        {
            string str_sql = "";
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                str_sql = "select InvoiceStatus as oStatus from InvoceGeneralInfo where InvoiceID = " + OCFID;
            }
            else
            {
                str_sql = "select ReportStatus as oStatus from PaperWork where ReportID = " + OCFID;
            }
            try
            {
                int oStatus = -1;
                SqlDataReader reader = ExecQuery(mCommand, str_sql);
                if (reader.Read() == true)
                {
                    oStatus = ConvertStrToInt(MyReaderFldToString(reader, "oStatus"));
                }
                reader.Close();
                reader = null;
                return oStatus;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public bool ResetOCFToNewHCAIStatus(SqlCommand mCommand, long OCFID, int NewStatus, string OCFType, string HCAIDocumentNumber)
        {
            bool IsResetSucc = false;
            string str_sql = "";
            int nStatusOrder = 0;

            //get last hcai ocf log status order                            
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //str_sql = "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + OCFID;
                str_sql = "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)";
            }
            else
            {
                //str_sql = "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + ConvertStrToLong(OCFID.ToString());                
                str_sql = "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)";

            }

            
            try
            {
                SqlDataReader reader = ExecQuery(mCommand, str_sql);
                if (reader.Read() == true)
                {
                    //Console.WriteLine(MyReaderFldToString(reader, "StatusOrder"));
                    nStatusOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder"));
                }
                nStatusOrder++;
                reader.Close();
                reader = null;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error(ex.Message, ex);
                //WRITE ERROR LOG
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID, ex.Message);
                nStatusOrder = 0;
            }
        

            //reset current status         
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                str_sql = "UPDATE HCAIStatusLog SET IsCurrentStatus = 0 WHERE InvoiceID = " + OCFID;
            else
                str_sql = "UPDATE HCAIStatusLog SET IsCurrentStatus = 0 WHERE ReportID = " + OCFID;

            ExecQueryForWrite(mCommand, str_sql);
            //RunSqlTransaction(str_sql);

            //***************************************************************************                
            //add new CURRENT hcai ocf status to log
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                str_sql = "INSERT INTO HCAIStatusLog (StatusOrder, OCFType, StatusDate, IsCurrentStatus, OCFStatus, InvoiceID, StatusTime, HCAIDocNumber) " +
                "VALUES (" + nStatusOrder + ",'" + MyConvert.MyLeft(OCFType, 6) + "','" + DateTime.Today.ToString("yyyy/MM/dd") + "',1," + NewStatus + "," + OCFID + ", '" +
                DateTime.Now.ToString("hh:mm:ss tt") + "','" + HCAIDocumentNumber.Replace("'", "''").ToString() + "')";
            }
            else
            {
                str_sql = "INSERT INTO HCAIStatusLog (StatusOrder, OCFType, StatusDate, IsCurrentStatus, OCFStatus, ReportID, StatusTime, HCAIDocNumber) " +
                "VALUES (" + nStatusOrder + ",'" + MyConvert.MyLeft(OCFType, 5) + "','" + DateTime.Today.ToString("yyyy/MM/dd") + "',1," + NewStatus + "," + OCFID + ", '" +
                DateTime.Now.ToString("hh:mm:ss tt") + "','" + HCAIDocumentNumber.Replace("'", "''").ToString() + "')";
            }

            if (ExecQueryForWrite(mCommand, str_sql) == true)
            //if (RunSqlTransaction(str_sql) == true)
            {
                if (MyConvert.MyLeft(OCFType, 5) != "OCF21")
                {
                    //update all tracking info
                    ResetOCFLinkedFieldsToNewStatus(mCommand, OCFID, NewStatus, DateTime.Now);
                }


                // TO BE TESTED -- senario when user manually sets submitted document to hcai complient status -- looses docuemnt status
                //update paperwork/invoice general
                //int pStatus = ConvertHCAIStatusToPaperworkStatus(NewStatus);
                //int oStatus = GetCurrentOCFStatus(OCFType, mCommand, OCFID);
                //if (pStatus == 1 && oStatus == 0) // if current status in created and new status in sent then update
                //{
                //    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                //    {
                //        ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET ReplyDate = NULL, InvoiceSentDate = '" + DateTime.Today.ToString("yyyy/MM/dd") + "', " +
                //                                    "InvoiceStatus = 1 where InvoiceID = " + OCFID);
                //    }
                //    else
                //    {
                //        DateTime DueDate = new DateTime();
                //        DueDate = GetOCFDueDate(mCommand, OCFType, DateTime.Today, DateTime.Now);
                //        //plans
                //        ExecQueryForWrite(mCommand, "UPDATE PaperWork SET SentDate = '" + DateTime.Today.ToString("yyyy/MM/dd") + "', " +
                //        "ReportStatus = 1, RepliedAs = '', ApprovedAmount = '0.00', " +
                //        "DueDate = '" + DueDate.ToString("yyyy/MM/dd") + "', DueTime = " + GetDueTimeForDB(DateTime.Now.ToString("hh:mm:ss tt")) + ", " +
                //        "ReplyDate = NULL where ReportID = '" + OCFID + "'");
                //    }
                //}
            }


            //update paperwork

            return IsResetSucc;
        }



        private bool IsTrackingExistsWithSameCredentials(SqlCommand mCommand, double TrackingID, double FieldID, DateTime TrackingDate, double CategoryID, string CaseID, int NewStatus)
        {
            bool IsExists = false;
            SqlDataReader reader = null;

            reader = ExecQuery(mCommand, "SELECT * FROM CaseTracking WHERE CaseID = '" + CaseID.Replace("'", "''") + "' AND TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND FieldID = " + FieldID + " AND TrackingDate = '" + TrackingDate.ToString("yyyy/MM/dd") + "'");
            if (reader.Read() == true)
            {
                string LinkToField = MyReaderFldToString(reader, "LinkToField");

                if (NewStatus == 0 || NewStatus == 1 || NewStatus == 2 || NewStatus == 4 || NewStatus == 5 || NewStatus == 9) //created
                {
                    if (LinkToField == "Created")
                    {
                        IsExists = true;
                    }

                }
                else if (NewStatus == 3 || NewStatus == 6 || NewStatus == 7 || NewStatus == 8 || NewStatus == 10 || NewStatus == 11) //sent
                {
                    if (LinkToField == "Sent")
                    {
                        IsExists = true;
                    }

                }
                else if (NewStatus == 14)  //approved
                {
                    if (LinkToField == "Approved")
                    {
                        IsExists = true;
                    }
                }
                else if (NewStatus == 12)   //not approved              
                {
                    if (LinkToField == "Not Approved")
                    {
                        IsExists = true;
                    }
                }
                else if (NewStatus == 13) //partially approved
                {
                    if (LinkToField == "Partially Approved")
                    {
                        IsExists = true;
                    }
                }
                else if (NewStatus == 15) //deemed approved
                {
                    if (LinkToField == "Deemed Approved")
                    {
                        IsExists = true;
                    }
                }

            }
            else
            {
                reader.Close();
            }

            reader = null;

            return IsExists;
        }

        private bool ResetOCFLinkedFieldsToNewStatus(SqlCommand mCommand, long OCFID, int NewStatus, DateTime StatusDate)
        {
            //note: for plan only  
            bool ResetSucc = true;
            SqlDataReader reader = null;

            reader = ExecQuery(mCommand, "select * from Paperwork where ReportID = " + OCFID);
            if (reader.Read() == true)
            {
                if (MyReaderFldToString(reader, "TrackingID") != "" && MyReaderFldToString(reader, "CategoryID") != "")
                {
                    string CaseID;
                    double TrackingID = 0;
                    double CategoryID = 0;
                    double FieldID = 0;
                    int FieldOrder;

                    //get case id  by plan id
                    CaseID = MyReaderFldToString(reader, "CaseID");

                    TrackingID = ConvertStrToDouble(MyReaderFldToString(reader, "TrackingID"));
                    CategoryID = ConvertStrToDouble(MyReaderFldToString(reader, "CategoryID"));

                    reader.Close();
                    reader = null;

                    SqlDataReader fld_reader = null;
                    fld_reader = ExecQuery(mCommand, "select * from TTField where TrackingID = " + TrackingID + " AND LinkToField = '" + ConvertHCAIDocStatusToSring(NewStatus) + "'");

                    try
                    {
                        if (fld_reader.Read() == true)
                        {
                            FieldID = ConvertStrToDouble(MyReaderFldToString(fld_reader, "FieldID"));
                            FieldOrder = ConvertStrToInt(MyReaderFldToString(fld_reader, "FieldOrder"));
                        }
                        else
                        {
                            FieldID = 0;
                            FieldOrder = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        UOServiceLib.Log.Error(ex.Message, ex);
                        //WRITE ERROR LOG
                        //DebugWrite(ex.Message.ToString());
                    }
                    fld_reader.Close();
                    fld_reader = null;
                    //tCommand = null;
                    //created: 0, 1, 2, 4, 5, 9
                    //sent: 3, 6, 11
                    //not approved: 12
                    //partially approved: 13
                    //approved: 14
                    //deemed approved: 15
                    //Delete Case Tracking Fields By Linked Status (TO AVOID DUPLICATS)


                    // NEEDS TO BE TESTED
                    //if (IsTrackingExistsWithSameCredentials(mCommand, TrackingID, FieldID, StatusDate, CategoryID, CaseID, NewStatus) == true)
                    //{
                    //    //EXIST HERE -- record in case tracking with same credentials already exists
                    //    return ResetSucc;
                    //}

                    string str_sql = "";

                    if (NewStatus == 0 || NewStatus == 1 || NewStatus == 2 || NewStatus == 4 || NewStatus == 5 || NewStatus == 9) //created
                    {
                        str_sql = "DELETE FROM CaseTracking WHERE TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                     "AND (LinkToField  = 'Sent' or LinkToField = 'Approved' or LinkToField = 'Not Approved' or LinkToField = 'Partially Approved' or LinkToField = 'Deemed Approved')";

                    }
                    else if (NewStatus == 3 || NewStatus == 6 || NewStatus == 7 || NewStatus == 8 || NewStatus == 10 || NewStatus == 11) //sent
                    {
                        str_sql = "DELETE FROM CaseTracking WHERE TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                    "AND (LinkToField = 'Approved' or LinkToField = 'Not Approved' or LinkToField = 'Partially Approved' or LinkToField = 'Deemed Approved')";

                    }
                    else if (NewStatus == 14)  //approved
                    {
                        str_sql = "DELETE FROM CaseTracking WHERE TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                    "AND (LinkToField = 'Not Approved' or LinkToField = 'Partially Approved' or LinkToField = 'Deemed Approved')";
                    }
                    else if (NewStatus == 12)   //not approved              
                    {
                        str_sql = "DELETE FROM CaseTracking WHERE TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                    "AND (LinkToField = 'Approved' or LinkToField = 'Partially Approved' or LinkToField = 'Deemed Approved')";
                    }
                    else if (NewStatus == 13) //partially approved
                    {
                        str_sql = "DELETE FROM CaseTracking WHERE TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                    "AND (LinkToField = 'Approved' or LinkToField = 'Not Approved' or LinkToField = 'Deemed Approved')";
                    }
                    else if (NewStatus == 15) //deemed approved
                    {
                        str_sql = "DELETE FROM CaseTracking WHERE TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                    "AND (LinkToField = 'Approved' or LinkToField = 'Not Approved' or LinkToField = 'Partially Approved')";
                    }

                    if (str_sql != "")
                    {
                        //RunSqlTransaction(str_sql);
                        ExecQueryForWrite(mCommand, str_sql);
                    }

                    //SaveCaseTrackingStatus
                    //old tracking date = today
                    //SaveCaseTrackingStatus(mCommand, TrackingID, FieldID, DateTime.Today, CategoryID, CaseID);
                    SaveCaseTrackingStatus(mCommand, TrackingID, FieldID, StatusDate, CategoryID, CaseID);
                }
                else
                {
                    //WRITE ERROR LOG
                    reader.Close();
                    reader = null;
                }
            }
            else
            {
                UOServiceLib.Log.WarnFormat("Sql reader could not read OCF Doc {0}.", OCFID);
                //WRITE ERROR LOG
                reader.Close();
                reader = null;
            }
            return ResetSucc;
        }




        private bool SaveCaseTrackingStatus(SqlCommand mCommand, double TrackingID, double FieldID, DateTime TrackingDate, double CategoryID, string CaseID)
        {
            bool Succ = false;
            SqlDataReader reader = null;

            reader = ExecQuery(mCommand, "select * from TTField where FieldID = " + FieldID);
            if (reader.Read() == true)
            {
                //double CaseTrackingID = 0;
                int AddToReminder = ConvertStrToInt(MyReaderFldToString(reader, "AddToReminder"));
                //TrackingData.TrackingID = TrackingID
                int DisableFromAlert = 0;
                long FieldColor = ConvertStrToLong(MyReaderFldToString(reader, "FieldColor"));
                string FieldName = MyReaderFldToString(reader, "FieldName");
                int FieldStatus = ConvertStrToInt(MyReaderFldToString(reader, "FieldStatus"));
                int IsBusinessDays = ConvertStrToInt(MyReaderFldToString(reader, "IsBusinessDays"));
                string LinkTo = MyReaderFldToString(reader, "LinkTo");
                string LinkToField = MyReaderFldToString(reader, "LinkToField");
                int FieldOrder = ConvertStrToInt(MyReaderFldToString(reader, "FieldOrder"));
                double ReminderID = 0;

                if (MyReaderFldToString(reader, "ReminderID") != "")
                {
                    ReminderID = ConvertStrToDouble(MyReaderFldToString(reader, "ReminderID"));
                }
                int RemindIn = ConvertStrToInt(MyReaderFldToString(reader, "RemindIn"));
                string Language = "";
                int TranslationReq = 0;
                int TransportationReq = 0;

                // get resource id
                string resource_id = MyReaderFldToString(reader, "resource_id");

                reader.Close();
                reader = null;

                // IMPLEMENT TRY CATCH HERE 
                var queryString = "select * from CaseTracking CT " +
                                     "where CT.TrackingID = " + TrackingID + " AND CT.CategoryID = " + CategoryID +
                                     " AND CT.FieldOrder < " + FieldOrder + " AND CT.CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                     "AND  CT.FieldOrder = (select MAX(CT2.FieldOrder) from CaseTracking CT2 where CT2.CaseID = '" + CaseID.Replace("'", "''") + "' AND CT.TrackingID = CT2.TrackingID AND CT.CategoryID = CT2.CategoryID)";

                try
                {
                    if (FieldOrder > 1)
                    {
                        SqlDataReader res_reader = null;
                        res_reader = ExecQuery(mCommand, queryString);

                        if (MyReaderFldToString(res_reader, "resource_id") != "")
                        {
                            resource_id = MyReaderFldToString(res_reader, "resource_id");
                        }
                        res_reader.Close();
                        res_reader = null;
                    }
                }
                catch (Exception e)
                {
                    Debugger.WriteToFile($"query: {queryString};;; Exception: {e.Message}", 10);
                }
                /////////////////////////////////////////////////////



                //******************** GET Language, Translation, Transportation
                //get last field info to forward it 

                SqlDataReader lf_reader = null;
                queryString = "select C.TranslationReq, C.TransportationReq, C.Language from CaseTracking C " +
                                                 "where C.CategoryID =  " + CategoryID + " AND C.TrackingID = " + TrackingID + " AND C.CaseID = '" + CaseID.Replace("'", "''") + "' " +
                                                 " AND C.FieldOrder > 0 AND C.FieldOrder = (select max(C2.FieldOrder) from CaseTracking C2 where C2.CategoryID =  " + CategoryID + " AND C2.TrackingID = " + TrackingID + " AND C2.CaseID = '" + CaseID.Replace("'", "''") + "' AND C2.FieldOrder > 0)";
                try
                {
                    lf_reader = ExecQuery(mCommand, queryString);
                    if (lf_reader.Read() == true)
                    {
                        if (lf_reader["Language"] != null || lf_reader["TransportationReq"] != null || lf_reader["TranslationReq"] != null)
                        {
                            Language = MyReaderFldToString(lf_reader, "Language");
                            TranslationReq = ConvertStrToInt(MyReaderFldToString(lf_reader, "TranslationReq"));
                            TransportationReq = ConvertStrToInt(MyReaderFldToString(lf_reader, "TransportationReq"));
                        }
                        else
                        {
                            SqlDataReader pi_reader = null;
                            pi_reader = ExecQuery(mCommand, "SELECT DISTINCT C.TransportReq, C.TranslationReq, C.Language " +
                            "FROM Client C inner join Client_Case CC on C.client_id = CC.client_id WHERE CaseID = '" + CaseID.Replace("'", "''") + "'");
                            if (pi_reader.Read() == true)
                            {
                                Language = MyReaderFldToString(pi_reader, "Language");
                                TranslationReq = ConvertStrToInt(MyReaderFldToString(pi_reader, "TranslationReq"));
                                TransportationReq = ConvertStrToInt(MyReaderFldToString(pi_reader, "TransportationReq"));

                                pi_reader.Close();
                            }
                            else
                            {
                                Language = "";
                                TranslationReq = 0;
                                TransportationReq = 0;
                            }
                            pi_reader = null;
                        }
                    }
                    else
                    {
                        Language = "";
                        TranslationReq = 0;
                        TransportationReq = 0;
                    }
                    lf_reader.Close();
                    lf_reader = null;
                }
                catch (Exception e)
                {
                    Debugger.WriteToFile($"query2: {queryString};;; Exception: {e.Message}", 10);
                }
                
                //*****************************************************************************
                // close reader
                //reader.Close();
                //reader = null;
                //*************** Add Tracking Case Data

                // delete case tracking if exists

                ExecQueryForWrite(mCommand, "DELETE FROM CaseTracking WHERE CaseID = '" + CaseID.Replace("'", "''") + "' AND TrackingID = " + TrackingID + " AND CategoryID = " + CategoryID + " AND FieldID = " + FieldID);

                // add tracking case 
                if (ExecQueryForWrite(mCommand, "INSERT INTO CaseTracking (CaseID, TrackingID, FieldID, CategoryID, FieldStatus, FieldName, FieldOrder, FieldColor, LinkTo, LinkToField, AddToReminder, RemindIn, IsBusinessDays, ReminderID, TrackingDate, DisableFromAlert, TransportationReq, TranslationReq, resource_id, Language) " +
                "VALUES ('" + CaseID.Replace("'", "''") + "','" + TrackingID + "','" + FieldID + "'," + CategoryID + "," + FieldStatus + "," +
                "'" + FieldName.Replace("'", "''") + "','" + FieldOrder + "','" + FieldColor + "','" + LinkTo.Replace("'", "''") + "','" + LinkToField.Replace("'", "''") + "'," +
                AddToReminder + "," + RemindIn + "," + IsBusinessDays + "," + ReminderID + ",'" + TrackingDate.ToString("yyyy/MM/dd") + "'," + DisableFromAlert + "," +
                TransportationReq + "," + TranslationReq + ",'" + resource_id.Replace("'", "''") + "','" + Language.Replace("'", "''") + "')") == true)
                {
                    Succ = true;
                }
                //*********************************************************************************
            }
            else
            {
                reader.Close();
            }

            reader = null;

            return Succ;
        }

        private int GetLastHCAILogOCFStatusOrder(SqlCommand mCommand, long OCFID, string OCFType)
        {
            int nStatusOrder = 1;

            //get last hcai ocf log status order
            SqlDataReader reader = null;
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + OCFID);

                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");
            }
            else
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + OCFID);
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");
            }

            if (reader.Read() == true)
            {
                nStatusOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder"));
            }
            else
            {
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: GetLastHCAILogOCFStatusOrder, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);
                nStatusOrder = 0;
            }
            nStatusOrder++;
            reader.Close();
            reader = null;
            return nStatusOrder;
        }

        //private string ConvertDateMDYtoYMD(string tDate)
        //{ 
        //    DateTime t = new DateTime(Convert.ToInt16(tDate.Substring(0, 2)), Convert.ToInt16(tDate.Substring(3, 2)), Convert.ToInt16(tDate.Substring(6, 2)));
        //    return t.ToString("yyyy/MM/dd");
        //}

        private bool ResetOCFHCAIStatusAndRecordNewForHCAIResponses(SqlCommand mCommand, long OCFID, int NewOCFStatus, int NewStatusOrder, string OCFType, DateTime StatusDate, string StatusTime, string ErrMsg, string HCAIDocNumber)
        {

            // get status order
            int StatusOrder = (int)GetLastHCAILogOCFStatusOrder(mCommand, OCFID, OCFType) + 1;
            //************************************************************************************//
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //Reset HCAI OCF Current status
                ExecQueryForWrite(mCommand, "UPDATE HCAIStatusLog SET IsCurrentStatus = 0 WHERE InvoiceID = " + OCFID);
                
                //Add New HCAI OCF Status For HCAI Response
                return ExecQueryForWrite(mCommand, "INSERT INTO HCAIStatusLog (StatusOrder, OCFType, StatusDate, IsCurrentStatus, OCFStatus, InvoiceID, StatusTime, ErrorMsg, HCAIDocNumber) " +
                "VALUES (" + StatusOrder + ",'" + MyConvert.MyLeft(OCFType, 6) + "','" + StatusDate.ToString("yyyy/MM/dd") + "',1," + NewOCFStatus + "," + OCFID + ", '" + StatusTime + "'," +
                "'" + MyReplace(ErrMsg) + "','" + MyReplace(HCAIDocNumber) + "')");

            }
            else
            {
                //Reset HCAI OCF Current status
                ExecQueryForWrite(mCommand, "UPDATE HCAIStatusLog SET IsCurrentStatus = 0 WHERE ReportID = " + OCFID);
                
                //Add New HCAI OCF Status For HCAI Response
                if (ExecQueryForWrite(mCommand, "INSERT INTO HCAIStatusLog (StatusOrder, OCFType, StatusDate, IsCurrentStatus, OCFStatus, ReportID, StatusTime, ErrorMsg, HCAIDocNumber) " +
                "VALUES (" + StatusOrder + ",'" + MyConvert.MyLeft(OCFType, 5) + "','" + StatusDate.ToString("yyyy/MM/dd") + "',1," + NewOCFStatus + "," + OCFID + ", '" + StatusTime + "'," +
                "'" + MyReplace(ErrMsg) + "','" + MyReplace(HCAIDocNumber) + "')") == true)
                
                {
                    return ResetOCFLinkedFieldsToNewStatus(mCommand, OCFID, NewOCFStatus, StatusDate);
                }
                else
                {
                    return false;
                }
            }
        }

        private void ReceiveHCAIError(SqlCommand mCommand, long OCFID, string OCFType, string OCFMsgError, int OCFErrorStatus, DateTime rDate, string rTime, string HCAIDocNumber)
        {

            //int StatusOrder = StatusOrder = GetLastHCAILogOCFStatusOrder(mCommand, OCFID, OCFType);

            SqlDataReader reader = null;
            //get current ocf status
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + OCFID);
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");
            }
            else
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + OCFID);
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");
            }

            //
            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                reader = null;

                //1. Machine Error
                if (OCFErrorStatus == 1 || OCFErrorStatus == 2 || OCFErrorStatus == 3)
                {
                    if (OCFErrorStatus == 1 || OCFErrorStatus == 2)
                    {
                        // reset ocf status to created
                        if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                        {
                            ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET InvoiceStatus = 0, InvoiceSentDate = NULL where InvoiceID = " + OCFID);
                            //RunSqlTransaction("UPDATE InvoceGeneralInfo SET InvoiceStatus = 0, InvoiceSentDate = NULL where InvoiceID = " + OCFID);
                        }
                        else
                        {
                            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET ReportStatus = 0, RepliedAs = '', ApprovedAmount = '0.00', " +
                                             "ReplyDate = NULL, DueDate = NULL, DueTime = NULL, SentDate = NULL where ReportID = " + OCFID);
                            //RunSqlTransaction("UPDATE PaperWork SET ReportStatus = 0, RepliedAs = '', ApprovedAmount = '0.00', " +
                            //                 "ReplyDate = NULL, DueDate = NULL, DueTime = NULL, SentDate = NULL where ReportID = " + OCFID);

                        }
                        //*************
                    }
                    int NewOCFStatus;
                    if (OCFErrorStatus == 1)
                        NewOCFStatus = 4;
                    else if (OCFErrorStatus == 2)
                        NewOCFStatus = 5;
                    else //3
                        NewOCFStatus = 8;

                    //update ocf status
                    if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, OCFID, NewOCFStatus, NewOrder, OCFType, rDate, rTime, OCFMsgError, HCAIDocNumber) == false)
                    {
                        //WRITE ERROR LOG
                    }
                }
            }
            else
            {
                string body = "ReceiveHCAIError, hcai current/max record was not found \nOCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID;
                UOServiceLib.Log.Error(body);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: ReceiveHCAIError, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);

                //error
                reader.Close();
                reader = null;
            }

        }

        private void ReceiveHCAISuccessfullyDelivered(SqlCommand mCommand, long OCFID, string OCFType, DateTime rDate, string rTime, string HCAIDocNumber)
        {

            Debugger.WriteToFile(string.Format("HCAI Document successfully submitted: HCAI#: {0}; type{1}", HCAIDocNumber, OCFType), 10);

            SqlDataReader reader = null;
            //get current ocf status
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //old way
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");
            }
            else
            {
                //old way
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");
            }
            try
            {

                if (reader.Read() == true)
                {
                    int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                    reader.Close();
                    reader = null;

                    //Debugger.WriteToFile("in read", true);

                    if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, OCFID, 6, NewOrder, OCFType, rDate, rTime, "", HCAIDocNumber) == true)
                    {
                        if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                        {
                            //Debugger.WriteToFile("updating OCF21 to sent", true);
                            ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET InvoiceSentDate = '" + rDate.ToString("yyyy/MM/dd") + "', " +
                                                        "InvoiceStatus = 1 where InvoiceID = " + OCFID);
                        }
                        else
                        {
                            DateTime DueDate = new DateTime();
                            DueDate = GetOCFDueDate(mCommand, OCFType, rDate, rTime);

                            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET SentDate = '" + rDate.ToString("yyyy/MM/dd") + "', " +
                            "ReportStatus = 1, RepliedAs = '', ApprovedAmount = '0.00', " +
                            "DueDate = '" + DueDate.ToString("yyyy/MM/dd") + "', DueTime = " + GetDueTimeForDB(rTime.ToString()) + ", " +
                            "ReplyDate = NULL where ReportID = '" + OCFID + "'");
                            
                        }
                    }
                    else
                    {
                        //WRITE ERROR LOG
                        UOServiceLib.Log.Error("ReceiveHCAISuccessfullyDelivered, hcai current/max record was not found \n" +
                                "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);
                        Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                                "in function: ReceiveHCAISuccessfullyDelivered, hcai current/max record was not found \n" +
                                "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);

                    }

                    //Set HCAI OCF Number to general ocf table
                    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    {
                        ExecQueryForWrite(mCommand, "UPDATE MVAInvoice SET HCAIDocumentNumber = '" + HCAIDocNumber + "' WHERE InvoiceID = " + OCFID);
                        //RunSqlTransaction("UPDATE MVAInvoice SET HCAIDocumentNumber = '" + HCAIDocNumber + "' WHERE InvoiceID = " + OCFID);
                    }
                    else
                    {
                        ExecQueryForWrite(mCommand, "UPDATE PaperWork SET HCAIDocumentNumber = '" + HCAIDocNumber + "' WHERE ReportID = " + OCFID);
                        //RunSqlTransaction("UPDATE PaperWork SET HCAIDocumentNumber = '" + HCAIDocNumber + "' WHERE ReportID = " + OCFID);
                    }

                }
                else
                {
                    reader.Close();
                    reader = null;
                }

            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error(ex.Message, ex);
                //Debugger.WriteToFile("ReceiveHCAISuccessfullyDelivered EXEPTION == " + ex.Message, true);
            }


        }


        private DateTime GetOCFDueDate(SqlCommand mCommand, string OCFType, DateTime FromDate, string rTime)
        {
            DateTime tDate = FromDate;
            //check if extra date is needed ==> If CDate(rTime) > CDate("4:45:00 PM") Then
            if (rTime.Substring(9, 2) == "PM")
            {
                if ((ConvertStrToInt((rTime.Substring(0, 2))) > 4))
                {
                    //ADD EXTRA DAY
                    tDate = tDate.AddDays(1);
                }
                else if ((ConvertStrToInt((rTime.Substring(0, 2))) == 4))
                {
                    if ((ConvertStrToInt((rTime.Substring(3, 2))) > 45))
                    {
                        //ADD EXTRA DAY
                        tDate = tDate.AddDays(1);
                    }
                }
            }

            //get preferences for days for OCF18/22/23
            SqlDataReader reader = null;
            reader = ExecQuery(mCommand, "select NROCF18, NROCF22, NROCF23, NRForm1 from Preference");

            if (reader.Read() == true)
            {
                int NROCF18, NROCF22, NROCF23, NRForm1;

                if (MyReaderFldToString(reader, "NROCF18") != "")
                    NROCF18 = ConvertStrToInt(MyReaderFldToString(reader, "NROCF18"));
                else
                    NROCF18 = 0;

                if (MyReaderFldToString(reader, "NROCF22") != "")
                    NROCF22 = ConvertStrToInt(MyReaderFldToString(reader, "NROCF22"));
                else
                    NROCF22 = 0;

                if (MyReaderFldToString(reader, "NROCF23") != "")
                    NROCF23 = ConvertStrToInt(MyReaderFldToString(reader, "NROCF23"));
                else
                    NROCF23 = 0;

                if (MyReaderFldToString(reader, "NRForm1") != "")
                    NRForm1 = ConvertStrToInt(MyReaderFldToString(reader, "NRForm1"));
                else
                    NRForm1 = 0;

                reader.Close();
                reader = null;

                //get due date
                if (MyConvert.MyLeft(OCFType, 5) == "OCF18")
                    return AddWorkDays(mCommand, FromDate, NROCF18, true);
                else if (MyConvert.MyLeft(OCFType, 5) == "OCF22")
                    return AddWorkDays(mCommand, FromDate, NROCF22, true);
                else if (MyConvert.MyLeft(OCFType, 5) == "OCF23")
                    return AddWorkDays(mCommand, FromDate, NROCF23, true);
                else if (MyConvert.MyLeft(OCFType, 5) == "Form1")
                    return AddWorkDays(mCommand, FromDate, NRForm1, true);
                else
                    return DateTime.Now;
            }
            else
            {
                reader.Close();
                reader = null;
                return DateTime.Now;
            }

        }

        public DateTime AddWorkDays(SqlCommand mCommand, DateTime D, int w, bool InludeHolidays)
        {
            int i;
            DateTime dd;

            try
            {
                if (InludeHolidays == true)
                {
                    SqlDataReader reader = null;
                    reader = ExecQuery(mCommand, "SELECT * FROM CalendarTable CT " +
                             "WHERE  CT.WeekDay <> 's' AND CT.IsHoliday = 0 AND " +
                             "CT.WDay = ((SELECT MAX(CT2.WDay) FROM CalendarTable CT2 WHERE  CT2.Date = '" + D.ToString("yyyy/MM/dd") + "') + " + w + ")");

                    dd = new DateTime();

                    if (reader.Read() == true)
                    {
                        if (MyReaderFldToString(reader, "Date") != "")
                            dd = (DateTime)reader["Date"];
                        else
                            dd = DateTime.Now;
                    }
                    else
                    {

                        dd = D;

                        i = 0;
                        while (i != w)
                        {
                            dd = dd.AddDays(1);
                            if (dd.DayOfWeek != DayOfWeek.Sunday && dd.DayOfWeek != DayOfWeek.Saturday)
                            {
                                i++;
                            }
                        }
                    }
                    reader.Close();
                    reader = null;
                }
                else
                {

                    dd = D;
                    //for (int i = 0; i <= w; i++)
                    i = 0;
                    while (i != w)
                    {
                        dd = dd.AddDays(1);
                        if (dd.DayOfWeek != DayOfWeek.Sunday && dd.DayOfWeek != DayOfWeek.Saturday)
                        {
                            i++;
                        }
                    }
                }

            }
            catch
            {
                dd = D;
                //for (int i = 0; i <= w; i++)
                i = 0;
                while (i != w)
                {
                    dd = dd.AddDays(1);
                    if (dd.DayOfWeek != DayOfWeek.Sunday && dd.DayOfWeek != DayOfWeek.Saturday)
                    {
                        i++;
                    }
                }
            }
            return dd;
        }


        private void ReceiveHCAIWithdrawResult(SqlCommand mCommand, long OCFID, string OCFType, DateTime rDate, string rTime, int isGranted, string HCAIDocNumber)
        {
            SqlDataReader reader = null;
            //get current ocf status
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + OCFID);
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");
            }
            else
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + OCFID);
                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");
            }

            //
            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                reader = null;

                int NewOCFStatus;

                if (isGranted == 9)
                {
                    // reset ocf status to created
                    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    {
                        ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET InvoiceStatus = 0, InvoiceSentDate = NULL where InvoiceID = " + OCFID);
                    }
                    else
                    {
                        ExecQueryForWrite(mCommand, "UPDATE PaperWork SET ReportStatus = 0, RepliedAs = '', ApprovedAmount = '0.00', " +
                                         "ReplyDate = NULL, DueDate = NULL, DueTime = NULL, SentDate = NULL where ReportID = " + OCFID);
                    }

                    NewOCFStatus = 9;
                }
                else //if (isGranted == 10)
                {
                    NewOCFStatus = isGranted;
                }

                if (!ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, OCFID, NewOCFStatus, NewOrder, OCFType, rDate, rTime, "", HCAIDocNumber))
                {
                    //WRITE ERROR LOG
                    UOServiceLib.Log.ErrorFormat("ResetOCFHCAIStatusAndRecordNewForHCAIResponses failed. OCF Doc {0}<{1}>, HCAI doc# {2}", OCFType, OCFID, HCAIDocNumber);
                }
            }
            else
            {
                UOServiceLib.Log.Error("ReceiveHCAIWithdrawResult, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: ReceiveHCAIWithdrawResult, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);

                reader.Close();
                reader = null;
            }
        }
        #endregion

        #region ADJUSTER RESPONSE FUNCTIONS

        private string ConvertHCAIDateFormatToDBFormat(string hDate)
        {
            try
            {
                if (hDate == null || hDate == "")
                    return "NULL";
                else

                    //MessageBox.Show(hDate.Substring(0, 4).ToString());
                    //MessageBox.Show(hDate.Substring(5, 2).ToString());
                    //MessageBox.Show(hDate.Substring(8, 2).ToString());

                    return ("'" + hDate.Substring(0, 4).ToString() + "/" + hDate.Substring(5, 2).ToString() + "/" + hDate.Substring(8, 2).ToString() + "'").ToString();
            }
            catch (FormatException e)
            {
                //WRITE ERROR LOG
                UOServiceLib.Log.Error("Date conversion failed for string " + hDate + e.Message, e);
                return "NULL";
            }
        }

        private string ConvertHCAIGenderToDBFormat(string g)
        {
            if (g.ToString() == null || g.ToString() == "")
                return "";
            else if (g.ToString() == "M")
                return "Male";
            else
                return "Female";
        }


        /// <summary>
        /// OCF9 FUNCTIONS 
        /// </summary>

        private void LoadParentPAFLineItems(SqlCommand mCommand, ref List<OCF9LineItems> OCF9Items, long OCFID, bool isApproved, int NoOfItems)
        {
            SqlDataReader reader = null;
            reader = ExecQuery(mCommand, "select * from OCF23 where ReportID = " + OCFID);


            OCF9Items = new List<OCF9LineItems>();

            if (reader.Read() == true)
            {

                for (int i = 1; i <= 2; i++)
                {
                    OCF9LineItems OCF9Item = new OCF9LineItems();

                    if (MyReaderFldToString(reader, "PAFItem" + i + "ID") != "")
                    {
                        if (i == 1)
                        {
                            OCF9Item.GSCode = MyReaderFldToString(reader, "PAFItem" + i + "ID"); // "WAD1OR2"; //MyReaderFldToString(reader, "PAFItem" & i & "ID"]
                        }
                        else if (i == 2)
                        {
                            if ((string.IsNullOrEmpty(MyReaderFldToString(reader, "PAFItem" + i + "Description"))) == true)
                                OCF9Item.GSCode = "";
                            else
                                OCF9Item.GSCode = MyReaderFldToString(reader, "PAFItem" + i + "Description");
                        }

                        if ((string.IsNullOrEmpty(MyReaderFldToString(reader, "PAFItem" + i + "EstimatedFee"))) == true)
                            OCF9Item.GSAmountClaimed = 0; //claimed amount
                        else
                            OCF9Item.GSAmountClaimed = ConvertStrToDouble(MyReaderFldToString(reader, "PAFItem" + i + "EstimatedFee")); //claimed amount

                        if (isApproved == true && OCF9Item.GSAmountClaimed > 0)
                            OCF9Item.GSServicessAmountPayable = OCF9Item.GSAmountClaimed; //approved amt
                        else
                            OCF9Item.GSServicessAmountPayable = 0;

                        OCF9Item.GSInterestPayable = 0;
                        OCF9Item.GSReasonCode = "";
                        OCF9Item.GSReasonCodeDesc = "";
                        OCF9Item.GSReasonCodeDescOther = "";
                        OCF9Item.GSReferenceNumber = i;
                    }
                    else
                    {
                        OCF9Item.GSCode = "";
                        OCF9Item.GSAmountClaimed = 0; //claimed amount
                        if (isApproved == true)
                            OCF9Item.GSServicessAmountPayable = 0;
                        else
                            OCF9Item.GSServicessAmountPayable = 0;

                        OCF9Item.GSInterestPayable = 0;
                        OCF9Item.GSReasonCode = "";
                        OCF9Item.GSReasonCodeDesc = "";
                        OCF9Item.GSReasonCodeDescOther = "";
                        OCF9Item.GSReferenceNumber = i;
                    }
                    NoOfItems++;
                    OCF9Items.Add(OCF9Item);
                }
            }

            reader.Close();
            reader = null;
        }

        //load parent line items
        private double CalculatePrecentAmount(double Precent, double ItemAmount)
        {
            double x = 0;
            x = (double)(ItemAmount * Precent / 100.0);
            x = (double)Math.Round(x, 2, MidpointRounding.ToEven);
            return x;
        }
        //private double GetTax(string Tax, string Amt, string TaxPrecent)
        //{
        //    if (Tax.ToLower() == "yes")
        //    {
        //        if (IsNumeric(TaxPrecent) == true && IsNumeric(Amt) == true)
        //            return CalculatePrecentAmount(ConvertStrToDouble(TaxPrecent), ConvertStrToDouble(Amt));
        //        else
        //            return 0;
        //    }
        //    else
        //    {
        //        return 0;            
        //    }
        // }
        private string GetTax(string Tax, string fldName)
        {
            if (Tax.ToLower() == "yes")
            {
                return fldName;
            }
            else
            {
                return "0";
            }
        }

        private double CalulateProposedAmt(SqlDataReader reader, int ItemVCForShow)
        {
            if (ItemVCForShow != 1)
            {
                if (MyReaderFldToString(reader, "ItemAmount") != "")
                {
                    double GST = 0;
                    double PST = 0;
                    if (MyReaderFldToString(reader, "GST") != "")
                    {
                        GST = CalculatePrecentAmount(ConvertStrToDouble(MyReaderFldToString(reader, "GST")), ConvertStrToDouble(MyReaderFldToString(reader, "ItemAmount")));
                    }
                    if (MyReaderFldToString(reader, "PST") != "")
                    {
                        PST = CalculatePrecentAmount(ConvertStrToDouble(MyReaderFldToString(reader, "PST")), ConvertStrToDouble(MyReaderFldToString(reader, "ItemAmount")));
                    }
                    return ((double)Math.Round(((double)(ConvertStrToDouble(MyReaderFldToString(reader, "ItemAmount")) + GST + PST)), 2, MidpointRounding.ToEven));
                }

            }
            return 0;
        }

        private double CalulateApprovedAmt(SqlDataReader reader)
        {
            if (MyReaderFldToString(reader, "aItemAmount") != "")
            {
                double GST = 0;
                double PST = 0;

                if (MyReaderFldToString(reader, "aGST") != "")
                {
                    GST = ConvertStrToDouble(MyReaderFldToString(reader, "aGST"));
                }
                if (MyReaderFldToString(reader, "aPST") != "")
                {
                    PST = ConvertStrToDouble(MyReaderFldToString(reader, "aPST"));
                }
                return (double)((ConvertStrToDouble(MyReaderFldToString(reader, "aItemAmount"))) + GST + PST);
            }
            return 0;
        }

        private void LoadParentLineItems(SqlCommand mCommand, ref List<OCF9LineItems> OCF9Items, string OCFType, long OCFID)
        {

            SqlDataReader reader = null;
            //reader = ExecQuery(mCommand, "select * from OCF23 where ReportID = " + OCFID);

            if (MyConvert.MyLeft(OCFType, 5) != "OCF23")
                OCF9Items = new List<OCF9LineItems>();

            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                reader = ExecQuery(mCommand, "select * from InvoiceItemDetails where InvoiceID = " + OCFID + " order by PMSGSKey, ItemOrder");
            else
                reader = ExecQuery(mCommand, "select * from OCFGoodsAndServices where ReportID = " + OCFID + " order by ItemOrder");

            while (reader.Read())
            {
                OCF9LineItems OCF9Item = new OCF9LineItems();

                if ((string.IsNullOrEmpty(MyReaderFldToString(reader, "ItemDescription"))) == true)
                    OCF9Item.GSDescription = "";
                else
                    OCF9Item.GSDescription = MyReaderFldToString(reader, "ItemDescription");

                OCF9Item.GSCode = MyReaderFldToString(reader, "ItemServiceCode");

                if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    OCF9Item.GSAmountClaimed = CalulateProposedAmt(reader, ConvertStrToInt(MyReaderFldToString(reader, "ItemVCForShow"))); //claimed amount
                else
                    OCF9Item.GSAmountClaimed = CalulateProposedAmt(reader, 0); //claimed amount


                OCF9Item.GSServicessAmountPayable = CalulateApprovedAmt(reader); //approved amt


                OCF9Item.GSInterestPayable = 0;
                if (MyReaderFldToString(reader, "aAdjusterReasonCode") != "")
                    OCF9Item.GSReasonCode = MyReaderFldToString(reader, "aAdjusterReasonCode");

                if (MyReaderFldToString(reader, "aAdjusterReasonCodeDesc") != "")
                    OCF9Item.GSReasonCodeDesc = MyReaderFldToString(reader, "aAdjusterReasonCodeDesc");

                if (MyReaderFldToString(reader, "aAdjusterReasonOtherCodeDesc") != "")
                    OCF9Item.GSReasonCodeDescOther = MyReaderFldToString(reader, "aAdjusterReasonOtherCodeDesc");

                if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    OCF9Item.GSReferenceNumber = ConvertStrToInt(MyReaderFldToString(reader, "PMSGSKey"));
                else
                    OCF9Item.GSReferenceNumber = ConvertStrToInt(MyReaderFldToString(reader, "ItemOrder"));


                OCF9Items.Add(OCF9Item);
            }
            reader.Close();
            reader = null;

        }

        private bool IsNumeric(string n)
        {
            if (n == null || n == "")
            {
                return false;
            }
            else
            {
                try
                {
                    double y;
                    string t = n.Replace("$", "").Replace(" ", "");
                    y = Convert.ToDouble(t);  //Convert.ToDouble(n.ToString());
                    return true;
                }
                catch (Exception ex)
                {
                    UOServiceLib.Log.Error("Failed to convert " + n + "to int. " + ex.Message, ex);
                    return false;
                }
            }
        }

        private int FindArrayIndexByRefNumber(List<OCF9LineItems> OCF9Items, int fRefNumber)
        {
            int i = 0;

            foreach (OCF9LineItems OCF9Item in OCF9Items)
            {
                if (OCF9Item.GSReferenceNumber == fRefNumber)
                {
                    return i;
                }
                i++;
            }

            return -1;
        }

        private void ReceiveAndSaveOCF9(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, string OCFVersion, bool isOCFApproved)
        {
            //check if hcai doc number not empy


            if (Reply.Parent_Document_Number == null || Reply.Parent_Document_Number == "")
            {
                return;
            }

            //check if hcai doc number exists in uo database
            SqlDataReader reader = null;
            long OCFID = ConvertStrToLong(Reply.PMS_Document_Key);
            string ClaimNo = "";
            string PolicyNo = "";
            DateTime DOA = new DateTime();
            DateTime DOB = new DateTime();
            int IsInvoice;

            if (OCFID == 0)
            {
                if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                {
                    IsInvoice = 1;
                    reader = ExecQuery(mCommand, "select * from MVAInvoice where HCAIDocumentNumber = '" + Reply.HCAI_Document_Number + "'");
                }
                else
                {
                    IsInvoice = 0;
                    reader = ExecQuery(mCommand, "select * from PaperWork where HCAIDocumentNumber = '" + Reply.HCAI_Document_Number + "'");
                }

                if (reader.Read() == true)
                {
                    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                        OCFID = ConvertStrToLong(MyReaderFldToString(reader, "InvoiceID"));
                    else
                        OCFID = ConvertStrToLong(MyReaderFldToString(reader, "ReportID"));
                }
                reader.Close();
            }
            else
            {
                if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                {
                    IsInvoice = 1;
                }
                else
                {
                    IsInvoice = 0;
                }
            }

            //if (reader.Read() == true)
            if (OCFID > 0)
            {
                //set ocf 9 parent fields
                if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    reader = ExecQuery(mCommand, "select * from MVAInvoice where InvoiceID = " + OCFID);
                else
                    reader = ExecQuery(mCommand, "select * from " + OCFType + " where ReportID = " + OCFID);


                if (reader.Read() == true)
                {
                    ClaimNo = MyReaderFldToString(reader, "CIClaimNo");
                    PolicyNo = MyReaderFldToString(reader, "CIPolicyNo");
                    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    {
                        if (MyReaderFldToString(reader, "CIDOL") != "")
                            DOA = (DateTime)reader["CIDOL"];
                    }
                    else
                    {
                        if (MyReaderFldToString(reader, "CIDOA") != "")
                            DOA = (DateTime)reader["CIDOA"];
                    }

                    if (MyReaderFldToString(reader, "CIDOB") != "")
                        DOB = (DateTime)reader["CIDOB"];


                }
                reader.Close();
                reader = null;

                //delete just incase if exists
                ExecQueryForWrite(mCommand, "DELETE FROM OCF9 WHERE PerantOCFID = " + ConvertStrToDouble(Reply.HCAI_Document_Number));

                //1. CREATE NEW OCF9 GENERAL DATA
                ExecQueryForWrite(mCommand, "INSERT INTO OCF9 (PerantOCFID, isInvoice, PerantHCAIDocNumber, PolicyNumber, ClaimNumber, DOA, DateRevised, " +
                "ApplicantFirstName, ApplicantLastName, ApplicantMiddleName, ApplicantDOB, ApplicantGender, ApplicantHomeTelephone, ApplicantTelephoneNumber, ApplicantTelephoneNumberExt, " +
                "ApplicantStreetAddress1, ApplicantStreetAddress2, ApplicantCity, ApplicantProvince, ApplicantPostalCode, AdditionalComments, AttachmentsBeingSent, OCFVersion, IBCInsurerID, IBCBranchID) " +
                "VALUES (" + ConvertStrToDouble(Reply.HCAI_Document_Number) + "," + IsInvoice + ",'" + MyReplace(Reply.HCAI_Document_Number) + "'," +
                "'" + MyReplace(PolicyNo) + "','" + MyReplace(ClaimNo) + "','" + DOA.ToString("yyyy/MM/dd") + "'," +
                ConvertHCAIDateFormatToDBFormat(Reply.OCF9_Header_DateRevised) + "," +
                "'" + MyReplace(Reply.Claimant_First_Name) + "','" + MyReplace(Reply.Claimant_Last_Name) + "','" + MyReplace(Reply.Claimant_Middle_Name) + "'," +
                "'" + DOB.ToString("yyyy/MM/dd") + "'," +
                "'" + ConvertHCAIGenderToDBFormat(Reply.Claimant_Gender) + "','" + MyReplace(Reply.Claimant_Telephone) + "','',''," +
                "'" + MyReplace(Reply.Claimant_Address1) + "','" + MyReplace(Reply.Claimant_Address2) + "','" + MyReplace(Reply.Claimant_City) + "'," +
                "'" + MyReplace(Reply.Claimant_Province) + "','" + MyReplace(Reply.Claimant_Postal_Code) + "','" + MyReplace(Reply.OCF9_AdditionalComments) + "'," +
                "'" + MyReplace(Reply.OCF9_AttachmentsBeingSent) + "','" + MyReplace(Reply.OCF9_OCFVersion) + "','" + MyReplace(Reply.Insurer_IBCInsurerID) + "','" + MyReplace(Reply.Insurer_IBCBranchID) + "')");

                //2. CREATE NEW OCF9 LINE ITEMS DATA
                int NoOfWADItems = 0;
                List<OCF9LineItems> OCF9Items = new List<OCF9LineItems>();
                OCF9LineItems OCF9Cls = new OCF9LineItems();

                if (MyConvert.MyLeft(OCFType, 5) == "OCF23")
                    LoadParentPAFLineItems(mCommand, ref OCF9Items, OCFID, isOCFApproved, NoOfWADItems);

                //Load Parent Line Items
                LoadParentLineItems(mCommand, ref OCF9Items, OCFType, OCFID);

                //4. get ocf9 line items details and merge them with perant doc
                int f;
                int tRefNo = 0;
                //int XRayIndex = 0;
                //int OCF21C_Counter = 0;
                //AdjusterResponseOCF9LineItem OCF9LineItems = Reply.OCF9_LineItem;

                string[] toSplit;

                //instead of while do this
                foreach (var OCF9LineItems in Reply.OCF9_LineItem2)
                //while (OCF9LineItems != null)
                {
                    toSplit = OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber.Split(';');

                    if (toSplit.Length > 1)
                    {
                        if (MyConvert.IsNumeric(toSplit[0]) == true)
                        {
                            if ((MyConvert.MyLeft(OCFType, 5) + OCFVersion) == "OCF21C") //OCF21C ITEMS 
                            {
                                if (toSplit[1] == "GSR")
                                {
                                    tRefNo = ConvertStrToInt(toSplit[0]);
                                }
                                else if (toSplit[1] == "MIG")
                                {
                                    tRefNo = 299 + ConvertStrToInt(toSplit[0]);
                                }
                                else if (toSplit[1] == "OGS")
                                {
                                    tRefNo = 199 + ConvertStrToInt(toSplit[0]);
                                }
                            }
                            else if (MyConvert.MyLeft(OCFType, 5) == "OCF23")  //OCF23 ITEMS 
                            {
                                if (toSplit[1] == "OGS")
                                {
                                    tRefNo = 29 + ConvertStrToInt(toSplit[0]);
                                }
                                else
                                {
                                    //paf and xray items
                                    tRefNo = ConvertStrToInt(toSplit[0]); //if 1 or 2 then its paf items
                                    if (tRefNo > 2)
                                    {
                                        if (tRefNo == 3)
                                            tRefNo = 20;
                                        else if (tRefNo == 4)
                                            tRefNo = 21;
                                        else if (tRefNo == 5)
                                            tRefNo = 22;
                                        else if (tRefNo == 6)
                                            tRefNo = 23;
                                    }
                                }
                            }
                            else  //OCF18 OCF22 OCF21B ITEMS 
                            {
                                tRefNo = ConvertStrToInt(toSplit[0]);
                            }

                            f = FindArrayIndexByRefNumber(OCF9Items, tRefNo);
                            if (f >= 0)
                            {
                                if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable) == true)
                                    OCF9Items[f].GSInterestPayable = ConvertStrToDouble(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable);
                            }
                        }
                    }
                    //'get another item in que                                            
                    //OCF9LineItems = Reply.OCF9_LineItem;
                }

                //while (OCF9LineItems != null)
                //{
                //    //find and update by line item                    
                //    if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber) == true && (MyConvert.MyLeft(OCFType, 5) + OCFVersion != "OCF21C"))
                //    {
                //        if (ConvertStrToInt(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber) < 30 && OCFType == "OCF23")
                //            tRefNo = ConvertStrToInt(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber) + 29;
                //        else
                //            tRefNo = ConvertStrToInt(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber);

                //        f = FindArrayIndexByRefNumber(OCF9Items, tRefNo);
                //        if (f >= 0)
                //        {
                //            if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable) == true)
                //            {
                //                OCF9Items[f].GSInterestPayable = ConvertStrToDouble(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable);
                //            }
                //        }
                //    }
                //    else if ((MyConvert.MyLeft(OCFType, 5) + OCFVersion) == "OCF21C")
                //    {
                //        if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber) == false)
                //        {
                //            //paf wad items only (blocks section)                            
                //            if (OCF21C_Counter < 300)
                //                OCF21C_Counter = 300;

                //            f = FindArrayIndexByRefNumber(OCF9Items, OCF21C_Counter);
                //            if (f >= 0)
                //            {
                //                if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable) == true)
                //                    OCF9Items[f].GSInterestPayable = ConvertStrToDouble(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable);
                //            }
                //            OCF21C_Counter++;
                //        }
                //        else
                //        {

                //            tRefNo = ConvertStrToInt(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber);

                //            if ((OCF21C_Counter >= tRefNo && OCF21C_Counter != 200) || OCF21C_Counter == 200)
                //            {
                //                //new section started
                //                OCF21C_Counter = 200;
                //                f = FindArrayIndexByRefNumber(OCF9Items, (int)(OCF21C_Counter + (tRefNo - 1)));
                //            }
                //            else
                //            {
                //                OCF21C_Counter = tRefNo;
                //                f = FindArrayIndexByRefNumber(OCF9Items, tRefNo);
                //            }

                //            if (f >= 0)
                //            {
                //                if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable) == true)
                //                    OCF9Items[f].GSInterestPayable = ConvertStrToDouble(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable);
                //            }
                //        }
                //    }
                //    else if (MyConvert.MyLeft(OCFType, 5) == "OCF23" && IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_ReferenceNumber) == false)
                //    {
                //        if (tRefNo == NoOfWADItems)
                //            XRayIndex = 20;

                //        if (tRefNo < 6)
                //        {
                //            if (tRefNo >= NoOfWADItems)
                //            {
                //                f = FindArrayIndexByRefNumber(OCF9Items, XRayIndex);
                //                if (f >= 0)
                //                {
                //                    if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable) == true)
                //                        OCF9Items[f].GSInterestPayable = ConvertStrToDouble(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable);
                //                }
                //                XRayIndex++;
                //            }
                //            else
                //            {
                //                if (IsNumeric(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable) == true)
                //                    OCF9Items[tRefNo].GSInterestPayable = ConvertStrToDouble(OCF9LineItems.OCF9_GoodsAndServices_Items_Item_InterestPayable);
                //            }
                //        }

                //        tRefNo++;
                //    }
                //    //'get another item in que                                            
                //    OCF9LineItems = Reply.OCF9_LineItem;
                //}

                //save ocf9lineitems
                //1. just in case delete ===> TO BE REMOVED
                ExecQueryForWrite(mCommand, "DELETE FROM OCF9LineItems WHERE PerantOCFID = " + ConvertStrToDouble(Reply.HCAI_Document_Number));

                //save ocf9 line items on by one
                string str_sql = "";

                foreach (OCF9LineItems OCF9Item in OCF9Items)
                {
                    //save ocf9 line items on by one                   
                    str_sql = str_sql + "INSERT INTO OCF9LineItems (PerantOCFID, GSCode, GSAmountClaimed, GSServicessAmountPayable, GSInterestPayable, " +
                    "GSReasonCode, GSReasonCodeDesc, GSReasonCodeDescOther, GSReferenceNumber, GSDescription) " +
                    " VALUES ('" + ConvertStrToDouble(Reply.HCAI_Document_Number) + "'," +
                    "'" + MyReplace(OCF9Item.GSCode) + "', " + OCF9Item.GSAmountClaimed.ToString() + "," +
                    "" + OCF9Item.GSServicessAmountPayable.ToString() + "," + OCF9Item.GSInterestPayable.ToString() + "," +
                    "'" + MyReplace(OCF9Item.GSReasonCode) + "', '" + MyReplace(OCF9Item.GSReasonCodeDesc) + "','" + MyReplace(OCF9Item.GSReasonCodeDescOther) + "'," +
                    "" + OCF9Item.GSReferenceNumber.ToString() + ",'" + MyReplace(OCF9Item.GSDescription) + "') ";
                }
                //one time exec
                if (str_sql != "")
                    RunSqlTransaction(str_sql);

            }
            else
            {
                // update log file there is no document WHY? ERROR
                //exit procedure 
                reader.Close();
                reader = null;
                return;
            }
        }

        // UPDATE OCF18/22/23/21B/21C ON ADJUSTER REPLY



        private string GetDocumentStatus(SqlCommand mCommand, double aTotalAmt, long OCFID, string OCFType)
        {
            SqlDataReader reader = null;
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                reader = ExecQuery(mCommand, "select (MVATotal) as Total from InvoceGeneralInfo where InvoiceID = " + OCFID);
            else
                reader = ExecQuery(mCommand, "select Total from " + MyConvert.MyLeft(OCFType, 5) + " where ReportID = " + OCFID);

            if (reader.Read() == true)
            {
                if (aTotalAmt == 0)
                {
                    reader.Close();
                    reader = null;
                    return "Not Approved";
                }
                else if (aTotalAmt == ConvertStrToDouble(MyReaderFldToString(reader, "Total")))
                {
                    reader.Close();
                    reader = null;
                    return "Approved";
                }
                else
                {
                    reader.Close();
                    reader = null;
                    return "Partially Approved";
                }


            }
            else
            {
                reader.Close();
                reader = null;
                return "";
            }
        }



        private string GetMinSentDate(SqlCommand mCommand, long OCFID, string OCFType)
        {
            string tMinSentDate = "";
            string StrSql = "";

            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                StrSql = "SELECT DISTINCT H.InvoiceID, " +
                  "(SELECT MIN(HD.StatusDate) FROM HCAIStatusLog HD " +
                  "Where H.OCFStatus > 11 And H.InvoiceID = HD.InvoiceID " +
                  "AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate " +
                  "FROM HCAIStatusLog H " +
                  "Where H.IsCurrentStatus = 1 And H.Archived = 0 And H.OCFStatus <> 1 And H.InvoiceID = " + OCFID;
            }
            else
            {
                StrSql = "SELECT DISTINCT H.InvoiceID, " +
                  "(SELECT MIN(HD.StatusDate) FROM HCAIStatusLog HD " +
                  "Where H.OCFStatus > 11 And H.InvoiceID = HD.InvoiceID " +
                  "AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate " +
                  "FROM HCAIStatusLog H " +
                  "Where H.IsCurrentStatus = 1 And H.Archived = 0 And H.OCFStatus <> 1 And H.InvoiceID = " + OCFID;
            }
            try
            {
                SqlDataReader reader = ExecQuery(mCommand, StrSql);

                if (reader.Read() == true)
                {
                    tMinSentDate = MyReaderFldToDateStringMMDDYYYY(reader, "MinSentDate");

                }
                //else
                //{
                //    Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                //            " in function: GetMinSentDate, IN ELSE \n" +
                //            "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID;
                //    //WRITE ERROR LOG
                //    reader.Close();
                //}
                reader.Close();
                reader = null;
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error("GetMinSentDate, IN CATCH \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID + e.Message, e);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        " in function: GetMinSentDate, IN CATCH \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);


            }
            return tMinSentDate;

        }

        private HcaiStatus GetHCAIStatus(string docStatus)
        {
            if (docStatus.ToLower() == "approved" || docStatus.ToLower() == "responded")
                return HcaiStatus.Approved;
            else if (docStatus.ToLower() == "partially approved")
                return HcaiStatus.PartiallyApproved;
            else //denied    docStatus.ToLower() == "declined"            
                return HcaiStatus.NotApproved;
        }

        // THIS FUNCTION NEEDS TO BE TESTED !!!!!
        private long GetUODocumentID_ByHCAINumber(SqlCommand mCommand, string OCFType, string HCAIDocNumber, long PMSKey)
        {

            long docId = PMSKey;
            string strSql = "";
            try
            {
                if (OCFType == "")
                {
                    strSql = "select MAX(InvoiceID) as ID, MAX(ReportID) as ID2 from HCAIStatusLog where HCAIDocNumber = '" + HCAIDocNumber + "'";
                }
                else
                {
                    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    {
                        strSql = "select MAX(InvoiceID) as ID from HCAIStatusLog where HCAIDocNumber = '" + HCAIDocNumber + "'";
                    }
                    else
                    {
                        strSql = "select MAX(ReportID) as ID from HCAIStatusLog where HCAIDocNumber = '" + HCAIDocNumber + "'";
                    }
                }
                SqlDataReader reader = ExecQuery(mCommand, strSql);
                if (reader.Read() == true)
                {
                    docId = ConvertStrToLong(MyReaderFldToString(reader, "ID"));
                    if (docId <= 0)
                    {
                        if (OCFType == "")
                        {
                            docId = ConvertStrToLong(MyReaderFldToString(reader, "ID2"));
                            if (docId <= 0)
                            {
                                docId = 0; //PMSKey; //not sure --> if not found return 0???
                            }
                        }
                        else
                        {
                            docId = 0; // PMSKey;    
                        }
                    }
                }

                reader.Close();
                reader = null;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error(ex.Message, ex);
                //return docId;
            }


            return docId;

        }

        private OCFMapReturnType UpdateOCF18Reply(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, DateTime rDate, string rTime)
        {
            string AdjTime;
            int NewStatus;
            string aStatus;
            OCFMapReturnType Succ = new OCFMapReturnType();
            long docId = ConvertStrToLong(Reply.PMS_Document_Key);
            DateTime AdjReplyDate = MyConvert.MyHcaiDateConvert(Reply.Adjuster_Response_Date);
            AdjTime = AdjReplyDate.ToShortTimeString();
            

            if (Config.HcaiBehavior == Config.HcaiStatusBehavior.DocumentStatusBased)
            {
                if (Reply.Document_Status.ToLower() == "approved" || Reply.Document_Status.ToLower() == "responded")
                    aStatus = "Approved";
                else if (Reply.Document_Status.ToLower() == "partially approved")
                    aStatus = "Partially Approved";
                else //if (Reply.Document_Status == "declined)
                    aStatus = "Not Approved";

                NewStatus = (int)GetHCAIStatus(aStatus);


            }
            else //Config.HcaiStatusBehavior.LineItemBased
            {
                aStatus = GetDocumentStatus(mCommand,
                                            ConvertStrToDouble(Reply.OCF18_InsurerTotals_AutoInsurerTotal_Approved),
                                            docId, OCFType);

                if (aStatus == "Approved")
                    NewStatus = 14;
                else if (aStatus == "Partially Approved")
                    NewStatus = 13;
                else //denied                
                    NewStatus = 12;
            }

            //CHECK IF THIS STATUS ALREADY IN THE SYSTEM            
            // string AdjTime2 = Reply.Adjuster_Response_Date.Substring(11, 8).ToString();
            //AdjTime2 = AdjTime2.Substring(0, 2) + ":" + AdjTime2.Substring(3, 2) + ":00"; // +" " + rTime.Substring(9, 2);
            

            ////////////////////////////////////////////////////////////////////////////////////////////
            if (IsItemAlreadyInCurrentStatus(mCommand, OCFType, docId,
                NewStatus, AdjReplyDate.ToLongTimeString(), AdjReplyDate, Reply.HCAI_Document_Number.ToString()) == true)
            {
                Succ.ForceAdjusterAck = true;
            }

            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET ReplyDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', " +
            "ReportStatus = 2, RepliedAs = '" + aStatus.Replace("'", "''") + "'," +
            "ApprovedAmount = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_AutoInsurerTotal_Approved) + " where ReportID = " + docId);

            AdjusterResponseOCF18NonSessionLineItem OCF18Item;
            OCF18Item = Reply.OCF18_NonSession_LineItem;
            string str_sql = "";

            while (OCF18Item != null)
            {

                str_sql = str_sql + "UPDATE OCFGoodsAndServices SET aItemRate = " + ConvertStrToDouble(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost) + "," +
                          "aItemQty = " + ConvertStrToInt(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count) + ", " +
                          "aItemAmount = " + ConvertStrToDouble(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost) + "," +
                          "aGST = " + GetTax(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST, "GST") + ", " +
                          "aPST = " + GetTax(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST, "PST") + ", " +
                          "aAdjusterReasonCode = '" + MyReplace(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode) + "', " +
                          "aAdjusterReasonCodeDesc = '" + MyReplace(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc) + "', " +
                          "aAdjusterReasonOtherCodeDesc = '" + MyReplace(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc) + "' " +
                          "WHERE ReportID = " + docId + " AND ItemOrder = " + ConvertStrToInt(OCF18Item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey) + " ";

                OCF18Item = Reply.OCF18_NonSession_LineItem;
            }

            if (str_sql != "")
                RunSqlTransaction(str_sql);

            //todo - Ver3.13 Need to update the query to include: OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation

            //update general ocf approved amounts and adjuster reasons
            ExecQueryForWrite(mCommand, "UPDATE OCF18 SET ASubtotal = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_SubTotal_Approved) + ", " +
            "AOHIPTotal = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_MOH_Approved_LineCost) + ", " +
            "AEHC12Total = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost) + "," +
            "AGST = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_GST_Approved_LineCost) + ", " +
            "APST = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_PST_Approved_LineCost) + ", " +
            "ATotal = " + ConvertStrToDouble(Reply.OCF18_InsurerTotals_AutoInsurerTotal_Approved) + ", " +
            "MOHTotalApprovedReasonCode = '" + MyReplace(Reply.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode) + "'," +
            "MOHTotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc) + "'," +
            "MOHTotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "AEHC12TotalApprovedReasonCode = '" + MyReplace(Reply.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "AEHC12TotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "AEHC12TotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "AGSTApprovedReasonCode = '" + MyReplace(Reply.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "AGSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "AGSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "APSTApprovedReasonCode = '" + MyReplace(Reply.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "APSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "APSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "ATotalCount = " + ConvertStrToInt(Reply.OCF18_InsurerTotals_TotalCount_Approved) + ", " +
            "SigningAdjuster_Date = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', SigningAdjuster_FirstName = '" + MyReplace(Reply.SigningAdjuster_FirstName) + "', SigningAdjuster_LastName = '" + MyReplace(Reply.SigningAdjuster_LastName) + "', " +
            "aAdjusterResponseExplanation = '" + MyReplace(Reply.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation) + "', " +
            "NameOfAdjusterOnPDF = '" + MyReplace(Reply.NameOfAdjusterOnPDF) + "' " +
            "where ReportID = " + docId);

            



            //Update HCAILOG Table
            //SqlDataReader reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + docId);

            SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
            "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
            "from HCAIStatusLog H " +
            "where  H.ReportID =  " + docId + " AND " +
            "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");

            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, docId, NewStatus, NewOrder, OCFType, AdjReplyDate, AdjTime, "", Reply.HCAI_Document_Number) == false)
                {
                    //WRITE ERROR LOG
                }
            }
            else
            {
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: UpdateOCF18Reply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);

                //WRITE ERROR LOG
                reader.Close();
            }

            reader = null;

            //RECIEVE OCF9
            if (NewStatus == 14 || NewStatus == 13)
            {
                //approved or partially approved
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", true);
            }
            else
            {
                //denied
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", false);
            }

            //update succ return
            //update succ return
            Succ.OCFType = OCFType;
            Succ.HCAI_Document_Number = Reply.HCAI_Document_Number;
            Succ.OCFID = docId;
            Succ.New_HCAI_Status = (HcaiStatus)NewStatus;
            Succ.StatusDate = AdjReplyDate.ToString(HCAITableInfo.DateFormat);
            Succ.StatusTime = AdjTime;
            Succ.ATotal = MyConvert.ConvertStrToFloat(Reply.OCF18_InsurerTotals_AutoInsurerTotal_Approved);
            Succ.MinSentDate = GetMinSentDate(mCommand, docId, OCFType);

            return Succ;
        }


        private OCFMapReturnType UpdateOCF22Reply(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, DateTime rDate, string rTime)
        {
            string AdjDate;
            string AdjTime;
            int NewStatus;
            string aStatus;
            OCFMapReturnType Succ = new OCFMapReturnType();


            long docId = ConvertStrToLong(Reply.PMS_Document_Key);

            //AdjDate = Reply.Adjuster_Response_Date.Substring(0, 4).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(5, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(8, 2).ToString();
            //AdjTime = rTime.Substring(0, 2) + ":" + rTime.Substring(3, 2) + " " + rTime.Substring(9, 2);


            DateTime AdjReplyDate = new DateTime();

            //AdjTime = MyConvert.GetMyTimeFormat(Reply.Adjuster_Response_Date.Substring(11, 8).ToString());
            //AdjDate = Reply.Adjuster_Response_Date.Substring(5, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(8, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(0, 4).ToString();


            if (MyConvert.IsNumeric(Reply.Adjuster_Response_Date.Substring(4, 1).ToString()) == false)
            {
                //old format by specs
                AdjTime = MyConvert.GetMyTimeFormat(Reply.Adjuster_Response_Date.Substring(11, 8).ToString());
                AdjDate = Reply.Adjuster_Response_Date.Substring(5, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(8, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(0, 4).ToString();

            }
            else
            {
                //new unknown format -- not in specs
                AdjTime = MyConvert.GetMyTimeFormatV2(Reply.Adjuster_Response_Date.Substring(8, 4).ToString());
                AdjDate = Reply.Adjuster_Response_Date.Substring(4, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(6, 2).ToString() + "/" + Reply.Adjuster_Response_Date.Substring(0, 4).ToString();

            }


            AdjReplyDate = DateTime.ParseExact(AdjDate, "MM/dd/yyyy", new CultureInfo("en-US"));

            if (Config.HcaiBehavior == Config.HcaiStatusBehavior.DocumentStatusBased)
            {
                //aStatus = Reply.Document_Status;
                if (Reply.Document_Status.ToLower() == "approved" || Reply.Document_Status.ToLower() == "responded")
                    aStatus = "Approved";
                else if (Reply.Document_Status.ToLower() == "partially approved")
                    aStatus = "Partially Approved";
                else //if (Reply.Document_Status == "declined)
                    aStatus = "Not Approved";

                NewStatus = (int)GetHCAIStatus(aStatus);
            }
            else //Config.HcaiStatusBehavior.LineItemBased
            {

                aStatus = GetDocumentStatus(mCommand,
                                            ConvertStrToDouble(Reply.OCF22_InsurerTotals_AutoInsurerTotal_Approved),
                                            docId, OCFType);

                if (aStatus == "Approved")
                    NewStatus = 14;
                else if (aStatus == "Partially Approved")
                    NewStatus = 13;
                else //denied                
                    NewStatus = 12;
            }

            //CHECK IF THIS STATUS ALREADY IN THE SYSTEM
            //string AdjTime2 = Reply.Adjuster_Response_Date.Substring(11, 8).ToString();
            //AdjTime2 = AdjTime2.Substring(0, 2) + ":" + AdjTime2.Substring(3, 2) + ":00"; // +" " + rTime.Substring(9, 2);
            ////////////////////////////////////////////////////////////////////////////////////////////
            if (IsItemAlreadyInCurrentStatus(mCommand, OCFType, docId,
                NewStatus, AdjReplyDate.ToLongTimeString(), AdjReplyDate, Reply.HCAI_Document_Number.ToString()) == true)
            {
                Succ.ForceAdjusterAck = true;
            }


            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET ReplyDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', " +
            "ReportStatus = 2, RepliedAs = '" + aStatus.Replace("'", "''") + "'," +
            "ApprovedAmount = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_AutoInsurerTotal_Approved) + " where ReportID = " + docId);

            //UPDATE OCF22 LINE ITEMS
            AdjusterResponseOCF22LineItem OCF22Item;
            OCF22Item = Reply.OCF22_LineItem;
            string str_sql = "";

            while (OCF22Item != null)
            {

                str_sql = str_sql + "UPDATE OCFGoodsAndServices SET aItemRate = " + ConvertStrToDouble(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost.ToString()) + "/ItemQty, " +
                "aItemQty = ItemQty, " +
                "aItemAmount = " + ConvertStrToDouble(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost) + "," +
                "aGST = " + GetTax(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST, "GST") + ", " +
                "aPST = " + GetTax(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST, "PST") + ", " +
                "aAdjusterReasonCode = '" + MyReplace(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup) + "', " +
                "aAdjusterReasonCodeDesc = '" + MyReplace(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc) + "', " +
                "aAdjusterReasonOtherCodeDesc = '" + MyReplace(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc) + "' " +
                "WHERE ReportID = " + docId + " AND ItemOrder = " + ConvertStrToInt(OCF22Item.OCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey) + " ";

                OCF22Item = Reply.OCF22_LineItem;
            }
            if (str_sql != "")
                RunSqlTransaction(str_sql);

            //UPDATE OCF22 GENERAL INFO           

            // try
            // {

            ExecQueryForWrite(mCommand, "UPDATE OCF22 SET ASubtotal = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved) + ", " +
            "AOHIPTotal = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_MOH_Approved_LineCost) + ", " +
            "AEHC12Total = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_OtherInsurers_Approved_LineCost) + ", " +
            "AGST = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_GST_Approved_LineCost) + ", " +
            "APST = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_PST_Approved_LineCost) + ", " +
            "ATotal = " + ConvertStrToDouble(Reply.OCF22_InsurerTotals_AutoInsurerTotal_Approved) + ", " +
            "MOHTotalApprovedReasonCode = '" + MyReplace(Reply.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "MOHTotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "MOHTotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "AEHC12TotalApprovedReasonCode = '" + MyReplace(Reply.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "AEHC12TotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "AEHC12TotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "AGSTApprovedReasonCode = '" + MyReplace(Reply.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "AGSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "AGSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "APSTApprovedReasonCode = '" + MyReplace(Reply.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "APSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "APSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "' " +
            "where ReportID = " + docId);

            //}
            //catch (FormatException e)
            //{
            //    MessageBox.Show(e.Message.ToString());
            //}


            //Update HCAILOG Table
            //SqlDataReader reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + docId);

            SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
            "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
            "from HCAIStatusLog H " +
            "where  H.ReportID =  " + docId + " AND " +
            "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");

            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, docId, NewStatus, NewOrder, OCFType, AdjReplyDate, AdjTime, "", Reply.HCAI_Document_Number) == false)
                {
                    //WRITE ERROR LOG
                }
            }
            else
            {
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: UpdateOCF22Reply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);

                //WRITE ERROR LOG
                reader.Close();
            }
            reader = null;

            //RECIEVE OCF9
            if (NewStatus == 14 || NewStatus == 13)
            {
                //approved or partially approved
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", true);
            }
            else
            {
                //denied
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", false);
            }

            //update succ return
            Succ.OCFType = OCFType;
            Succ.HCAI_Document_Number = Reply.HCAI_Document_Number;
            Succ.OCFID = docId;
            Succ.New_HCAI_Status = (HcaiStatus)NewStatus;

            Succ.StatusDate = AdjReplyDate.ToString(HCAITableInfo.DateFormat);
            Succ.StatusTime = AdjTime;
            Succ.ATotal = MyConvert.ConvertStrToFloat(Reply.OCF22_InsurerTotals_AutoInsurerTotal_Approved);
            Succ.MinSentDate = GetMinSentDate(mCommand, docId, OCFType);
            return Succ;
        }

        private OCFMapReturnType UpdateOCF23Reply(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, DateTime rDate, string rTime)
        {
            string AdjTime;
            int NewStatus;
            string aStatus;
            OCFMapReturnType Succ = new OCFMapReturnType();

            long docId = ConvertStrToLong(Reply.PMS_Document_Key);

            DateTime AdjReplyDate = MyConvert.MyHcaiDateConvert(Reply.Adjuster_Response_Date);
            AdjTime = AdjReplyDate.ToShortTimeString();

            if (Config.HcaiBehavior == Config.HcaiStatusBehavior.DocumentStatusBased)
            {
                //aStatus = Reply.Document_Status;
                if (Reply.Document_Status.ToLower() == "approved" || Reply.Document_Status.ToLower() == "responded")
                    aStatus = "Approved";
                else if (Reply.Document_Status.ToLower() == "partially approved")
                    aStatus = "Partially Approved";
                else //if (Reply.Document_Status == "declined)
                    aStatus = "Not Approved";

                NewStatus = (int)GetHCAIStatus(aStatus);
            }
            else //Config.HcaiStatusBehavior.LineItemBased
            {

                aStatus = GetDocumentStatus(mCommand,
                                            ConvertStrToDouble(Reply.OCF23_InsurerTotals_AutoInsurerTotal_Approved),
                                            docId, OCFType);

                if (aStatus == "Approved")
                    NewStatus = 14;
                else if (aStatus == "Partially Approved")
                    NewStatus = 13;
                else //denied                
                    NewStatus = 12;
            }
            //CHECK IF THIS STATUS ALREADY IN THE SYSTEM
            //string AdjTime2 = Reply.Adjuster_Response_Date.Substring(11, 8).ToString();
           // AdjTime2 = AdjTime2.Substring(0, 2) + ":" + AdjTime2.Substring(3, 2) + ":00"; // +" " + rTime.Substring(9, 2);
            ////////////////////////////////////////////////////////////////////////////////////////////
            if (IsItemAlreadyInCurrentStatus(mCommand, OCFType, docId,
                NewStatus, AdjReplyDate.ToLongTimeString(), AdjReplyDate, Reply.HCAI_Document_Number.ToString()) == true)
            {
                Succ.ForceAdjusterAck = true;
            }

            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET ReplyDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', " +
            "ReportStatus = 2, RepliedAs = '" + aStatus.Replace("'", "''") + "'," +
            "ApprovedAmount = " + ConvertStrToDouble(Reply.OCF23_InsurerTotals_AutoInsurerTotal_Approved) + " where ReportID = " + docId);
            
            //UPDATE OCF23 LINE ITEMS
            AdjusterResponseOCF23OtherGSLineItem OCF23Item;
            OCF23Item = Reply.OCF23_OtherGS_LineItem;
            string str_sql = "";

            while (OCF23Item != null)
            {
                str_sql = str_sql + "UPDATE OCFGoodsAndServices SET aItemRate = " + ConvertStrToDouble(OCF23Item.OCF23_OtherGoodsandServices_Items_Approved_LineCost) + "/ItemQty, " +
                "aItemQty = ItemQty, " +
                "aItemAmount = " + ConvertStrToDouble(OCF23Item.OCF23_OtherGoodsandServices_Items_Approved_LineCost) + "," +
                "aAdjusterReasonCode = '" + MyReplace(OCF23Item.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode) + "', " +
                "aAdjusterReasonCodeDesc = '" + MyReplace(OCF23Item.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
                "aAdjusterReasonOtherCodeDesc = '" + MyReplace(OCF23Item.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "' " +
                "WHERE ReportID = " + docId + " AND ItemOrder = " + ConvertStrToInt(OCF23Item.OCF23_OtherGoodsandServices_Items_Item_PMSGSKey) + " ";

                OCF23Item = Reply.OCF23_OtherGS_LineItem;
            }
            if (str_sql != "")
                RunSqlTransaction(str_sql);


            //UPDATE OCF23 GENERAL INFO
            ExecQueryForWrite(mCommand, "UPDATE OCF23 SET " +
            "aSubtotalSG = " + ConvertStrToDouble(Reply.OCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved) + ", " +
            "aTotal = " + ConvertStrToDouble(Reply.OCF23_InsurerTotals_AutoInsurerTotal_Approved) + ", " +
            "ASubtotalPreApproved = " + ConvertStrToDouble(Reply.OCF23_InsurerTotals_SubTotalPreApproved_Approved) + ", " +
            "SigningAdjuster_Date = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', SigningAdjuster_FirstName = '" + MyReplace(Reply.SigningAdjuster_FirstName) + "', SigningAdjuster_LastName = '" + MyReplace(Reply.SigningAdjuster_LastName) + "', " +
            "OCF23_OtherGoodsAndServices_AdjusterResponseExplanation = '" + MyReplace(Reply.OCF23_OtherGoodsAndServices_AdjusterResponseExplanation) + "', " +
            "NameOfAdjusterOnPDF = '" + MyReplace(Reply.NameOfAdjusterOnPDF) + "' " +
            "where ReportID = " + docId);


            //Update HCAILOG Table
            //SqlDataReader reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + docId);

            SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
            "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
            "from HCAIStatusLog H " +
            "where  H.ReportID =  " + docId + " AND " +
            "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");

            if (reader.Read())
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                if (!ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, docId, NewStatus, NewOrder, OCFType, AdjReplyDate, AdjTime, "", Reply.HCAI_Document_Number))
                {
                    //WRITE ERROR LOG
                    UOServiceLib.Log.ErrorFormat("ResetOCFHCAIStatusAndRecordNewForHCAIResponses failed for OCF Doc {0} HCAI# {1}", OCFType, Reply.HCAI_Document_Number);
                }
            }
            else
            {
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: UpdateOCF23Reply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);

                UOServiceLib.Log.ErrorFormat("UpdateOCF23Reply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);
                //WRITE ERROR LOG
                reader.Close();
            }
            reader = null;

            //RECIEVE OCF9
            if (NewStatus == 14 || NewStatus == 13)
            {
                //approved or partially approved
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", true);
            }
            else
            {
                //denied
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", false);
            }
            //update succ return
            Succ.OCFType = OCFType;
            Succ.HCAI_Document_Number = Reply.HCAI_Document_Number;
            Succ.OCFID = docId;
            Succ.New_HCAI_Status = (HcaiStatus)NewStatus;

            Succ.StatusDate = AdjReplyDate.ToString(HCAITableInfo.DateFormat);
            Succ.StatusTime = AdjTime;
            Succ.ATotal = MyConvert.ConvertStrToFloat(Reply.OCF23_InsurerTotals_AutoInsurerTotal_Approved);
            Succ.MinSentDate = GetMinSentDate(mCommand, docId, OCFType);
            return Succ;
        }

        private string ConvertHcaiDateTimeToStr(string dt)
        {
            if(dt == null)
            {
                return "NULL";
            }
            else if (dt.Trim() == "")
            {
                return "NULL";
            }
            else
            {
                string dtTime = dt.Substring(11, 8).ToString();
                dtTime = dtTime.Substring(0, 2) + ":" + dtTime.Substring(3, 2) + ":00";
                
                //string dtDate = MyConvert.MyHcaiDateConvert(dt).ToString("yyyy/MM/dd");

                string dtDate = dt.Substring(0, 10).ToString();
                dtDate = MyConvert.MyHcaiDateConvert(dtDate).ToString("yyyy/MM/dd");
                return "'" + dtDate + " " + dtTime + "'";
            }

        }

        private OCFMapReturnType UpdateForm1Reply(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, DateTime rDate, string rTime)
        {
            string AdjTime;
            int NewStatus;
            string aStatus;
            OCFMapReturnType Succ = new OCFMapReturnType();
            long docId = ConvertStrToLong(Reply.PMS_Document_Key);
            DateTime AdjReplyDate = MyConvert.MyHcaiDateConvert(Reply.Adjuster_Response_Date);
            AdjTime = AdjReplyDate.ToShortTimeString();

            if (Config.HcaiBehavior == Config.HcaiStatusBehavior.DocumentStatusBased)
            {
                //aStatus = Reply.Document_Status;
                if (Reply.Document_Status.ToLower() == "approved" || Reply.Document_Status.ToLower() == "responded")
                    aStatus = "Approved";
                else if (Reply.Document_Status.ToLower() == "partially approved")
                    aStatus = "Partially Approved";
                else //if (Reply.Document_Status == "declined)
                    aStatus = "Not Approved";
                
            }
            else //Config.HcaiStatusBehavior.LineItemBased
            {
                //should not happen
                aStatus = "Not Approved";

            }
            NewStatus = (int)GetHCAIStatus(aStatus);

            //CHECK IF THIS STATUS ALREADY IN THE SYSTEM
            //string AdjTime2 = Reply.Adjuster_Response_Date.Substring(11, 8).ToString();
            //AdjTime2 = AdjTime2.Substring(0, 2) + ":" + AdjTime2.Substring(3, 2) + ":00"; // +" " + rTime.Substring(9, 2);
            ////////////////////////////////////////////////////////////////////////////////////////////
            if (IsItemAlreadyInCurrentStatus(mCommand, OCFType, docId, NewStatus, AdjReplyDate.ToLongTimeString(), AdjReplyDate, Reply.HCAI_Document_Number.ToString()))
            {
                Succ.ForceAdjusterAck = true;
            }

            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET ReplyDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', " +
            "ReportStatus = 2, RepliedAs = '" + aStatus.Replace("'", "''") + "', " +
            "HCAIDocumentNumber = '" + Reply.HCAI_Document_Number.Replace("'", "''") + "' " +
            " where ReportID = " + docId);

            string sqlStr = "UPDATE FORM1 SET AR_SubmissionTime = " + ConvertHcaiDateTimeToStr(Reply.Submission_Date) + ", " +
            "AR_InDispute = '" + Reply.In_Dispute.Replace("'", "''") + "', " +
            "AR_AttachmentsReceivedDate = " + ConvertHcaiDateTimeToStr(Reply.Form1_AttachmentsReceivedDate) + ", " +
            "AR_SigningAdjusterName_FirstName = '" + Reply.SigningAdjuster_FirstName.Replace("'", "''") + "', " +
            "AR_SigningAdjusterName_LastName = '" + Reply.SigningAdjuster_LastName.Replace("'", "''") + "', " +
            "AR_AdjusterResponseTime = " + ConvertHcaiDateTimeToStr(Reply.Form1_AdjusterResponseTime) + ", " +
            "AR_ArchivalStatus = '" + Reply.Form1_ArchivalStatus.Replace("'", "''") + "', " +
            "AR_Claimant_Name_FirstName = '" + Reply.Claimant_First_Name.Replace("'", "''") + "', " +
            "AR_Claimant_Name_MiddleName = '" + Reply.Claimant_Middle_Name.Replace("'", "''") + "', " +
            "AR_Claimant_Name_LastName = '" + Reply.Claimant_Last_Name.Replace("'", "''") + "', " +
            "AR_Claimant_DateOfBirth = '" + MyConvert.MyHcaiDateConvert(Reply.Claimant_Date_Of_Birth).ToString("yyyy/MM/dd") + "', " +
            "AR_Claimant_Gender = '" + Reply.Claimant_Gender.Replace("'", "''") + "', " +
            "AR_Claimant_Extension = '" + Reply.Claimant_Extension.Replace("'", "''") + "', " +
            "AR_Claimant_Address_StreetAddress1 = '" + Reply.Claimant_Address1.Replace("'", "''") + "', " +
            "AR_Claimant_Address_StreetAddress2 = '" + Reply.Claimant_Address2.Replace("'", "''") + "', " +
            "AR_Claimant_Address_City = '" + Reply.Claimant_City.Replace("'", "''") + "', " +
            "AR_Claimant_Address_Province = '" + Reply.Claimant_Province.Replace("'", "''") + "', " +
            "AR_Claimant_Address_PostalCode = '" + Reply.Claimant_Postal_Code.Replace("'", "''") + "', " +
            "AR_EOB_EOBAdditionalComments = '" + Reply.Form1_EOB_EOBAdditionalComments.Replace("'", "''") + "', " +
            "AR_Costs_Approved_CalculatedBenefit = '" + Reply.Form1_Costs_Approved_CalculatedBenefit.Replace("'", "''") + "', " +
            "AR_Costs_Approved_Benefit = '" + Reply.Form1_Costs_Approved_Benefit.Replace("'", "''") + "', " +
            "AR_Costs_Approved_ReasonCode = '" + Reply.Form1_Costs_Approved_ReasonCode.Replace("'", "''") + "', " +
            "AR_Costs_Approved_ReasonDescription = '" + Reply.Form1_Costs_Approved_ReasonDescription.Replace("'", "''") + "', " +
            "AR_Costs_Approved_AdjusterResponseExplanation = '" + Reply.Form1_Costs_Approved_AdjusterResponseExplanation.Replace("'", "''") + "', " +
            "NameOfAdjusterOnPDF = '" + Reply.NameOfAdjusterOnPDF.Replace("'", "''") + "' ";

            //update adjuster form1 part4 items information
            int Count = 1;
            foreach (var item in Reply.Form1ApprovedCostLineItem)
            {
                if (Count <= 3)
                {
                    sqlStr = sqlStr + ", Part4_AR_Approved_Part_WeeklyHours" + Count + " = '" + item.Costs_Approved_Part_WeeklyHours + "', " +
                    "Part4_AR_Approved_Part_MonthlyHours" + Count + " = '" + item.Costs_Approved_Part_MonthlyHours + "', " +
                    "Part4_AR_Approved_Part_HourlyRate" + Count + " = '" + item.Costs_Approved_Part_HourlyRate + "', " +
                    "Part4_AR_Approved_Part_Benefit" + Count + " = '" + item.Costs_Approved_Part_Benefit + "', " +
                    "Part4_AR_Approved_Part_ReasonCode" + Count + " = '" + item.Costs_Approved_Part_ReasonCode.Replace("'", "''") + "', " +
                    "Part4_AR_Approved_Part_ReasonDescription" + Count + " = '" + item.Costs_Approved_Part_ReasonDescription.Replace("'", "''") + "' ";
                }
                Count=Count + 1;
            }
            sqlStr = sqlStr + " where ReportID = " + docId;
            ExecQueryForWrite(mCommand, sqlStr);


            //upadte adjuster form1 service items
            sqlStr = "";
            //Count = 1;

            foreach (var item in Reply.Form1AcsLineItem)
            {
               // var code = item.AttendantCareServices_Item_Approved_ReasonCode;
                
                sqlStr = sqlStr + "UPDATE FORM1_Parts123 SET " +
                "AttendantCareServices_Item_Approved_Minutes = '" + item.AttendantCareServices_Item_Approved_Minutes + "', "  +
                "AttendantCareServices_Item_Approved_TimesPerWeek = '" + item.AttendantCareServices_Item_Approved_TimesPerWeek + "', " + 
                "AttendantCareServices_Item_Approved_TotalMinutes = '" + item.AttendantCareServices_Item_Approved_TotalMinutes + "', " +
                "AttendantCareServices_Item_Approved_ReasonCode = '" + item.AttendantCareServices_Item_Approved_ReasonCode.Replace("'", "''") + "', " +
                "AttendantCareServices_Item_Approved_ReasonDescription = '" + item.AttendantCareServices_Item_Approved_ReasonDescription.Replace("'", "''") + "', " +
                "AttendantCareServices_Item_Approved_IsItemDeclined = '" + ConvertFlagToInt(item.AttendantCareServices_Item_Approved_IsItemDeclined) + "' " +
                "where ReportID = '" + docId + "' AND ACSItemID = '" + item.AttendantCareServices_Item_ACSItemID + "' ";
                
            }
            
            if (sqlStr != "")
            {
                RunSqlTransaction(sqlStr);
            }

            ///////////////////////////////////////////////////////////////////////////////////////
            //Update HCAILOG Table
            SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
            "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
            "from HCAIStatusLog H " +
            "where  H.ReportID =  " + docId + " AND " +
            "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");

            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, docId, NewStatus, NewOrder, OCFType, AdjReplyDate, AdjTime, "", Reply.HCAI_Document_Number) == false)
                {
                    //WRITE ERROR LOG
                }
            }
            else
            {
                //Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                //        "in function: UpdateFORM1Reply, hcai current/max record was not found \n" +
                //        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);

                //WRITE ERROR LOG
                reader.Close();
            }

            reader = null;

            ////RECIEVE OCF9
            //if (NewStatus == 14 || NewStatus == 13)
            //{
            //    //approved or partially approved
            //    ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", true);
            //}
            //else
            //{
            //    //denied
            //    ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "", false);
            //}
            //update succ return
            Succ.OCFType = OCFType;
            Succ.HCAI_Document_Number = Reply.HCAI_Document_Number;
            Succ.OCFID = docId;
            Succ.New_HCAI_Status = (HcaiStatus)NewStatus;
            Succ.StatusDate = AdjReplyDate.ToString(HCAITableInfo.DateFormat);
            Succ.StatusTime = AdjTime;
            Succ.ATotal = MyConvert.ConvertStrToFloat(Reply.OCF18_InsurerTotals_AutoInsurerTotal_Approved);
            Succ.MinSentDate = GetMinSentDate(mCommand, docId, OCFType);
            
            return Succ;
        }

        private string ConvertTimeFomatHHMMSS(string tTime)
        {
            DateTime y;
            if (DateTime.TryParse(tTime, out y))
            {
                return y.ToString("hh:mm:ss");
            }
            else
            {
                return tTime;
            }
        }


        private bool IsItemAlreadyInCurrentStatus(SqlCommand mCommand, string OCFType, long OCFID, int hStatus, string hAdjTime, DateTime hAdjReplyDate, string HCAI_Document_Number)
        {
            bool IsExists = false;
            string StrSql = "";

            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //StrSql = "select * from HCAIStatusLog " +
                //        "where IsCurrentStatus = 1 AND OCFStatus = " + hStatus + " AND StatusDate = '" + hAdjReplyDate.ToString("yyyy/MM/dd") + "' " +
                //        "AND InvoiceID = " + OCFID + " " +
                //        "AND LTRIM(RTRIM(STUFF(RIGHT(CONVERT(VARCHAR, StatusTime,100 ) ,7), 6, 0, ' '))) = '" + hAdjTime + "'";

                StrSql = "select * from HCAIStatusLog " +
                        "where IsCurrentStatus = 1 AND OCFStatus = " + hStatus + " AND StatusDate = '" + hAdjReplyDate.ToString("yyyy/MM/dd") + "' " +
                        "AND InvoiceID = " + OCFID + " " +
                        "AND convert(varchar(8), StatusTime, 8)  = '" + hAdjTime + "'";
            }
            else
            {
                //StrSql = "select * from HCAIStatusLog " +
                //        "where IsCurrentStatus = 1 AND OCFStatus = " + hStatus + " AND StatusDate = '" + hAdjReplyDate.ToString("yyyy/MM/dd") + "' " +
                //        "AND ReportID = " + OCFID + " " +
                //        "AND LTRIM(RTRIM(STUFF(RIGHT(CONVERT(VARCHAR, StatusTime,100 ) ,7), 6, 0, ' '))) = '" + hAdjTime + "'";
                StrSql = "select * from HCAIStatusLog " +
                        "where IsCurrentStatus = 1 AND OCFStatus = " + hStatus + " AND StatusDate = '" + hAdjReplyDate.ToString("yyyy/MM/dd") + "' " +
                        "AND ReportID = " + OCFID + " " +
                        "AND convert(varchar(8), StatusTime, 8) = '" + hAdjTime + "'";
            }

            SqlDataReader reader = ExecQuery(mCommand, StrSql);
            try
            {
                if (reader.Read() == true)
                {
                    if (MyReaderFldToString(reader, "HCAIDocNumber") == HCAI_Document_Number)
                    {
                        IsExists = true;
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error(ex.Message, ex);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: IsItemAlreadyInCurrentStatus \n" +
                        "ERROR MSG: " + ex.Message.ToString());
            }
            return IsExists;
        }

        //on hold for now
        //private bool IsValidAdjResponseToUpdate(SqlCommand mCommand, string HCAIDocNumber, DateTime AdjReplyDate, string OCFType, int NewOCFStatus, string AdjTime)
        //{
        //    bool RetValue = true;
        //    SqlDataReader reader = null;

        //    reader = ExecQuery(mCommand, "SELECT * FROM HCAIStatusLog " +
        //    "WHERE HCAIDocNumber = '" + HCAIDocNumber + "' AND IsCurrentStatus = 1 AND " +
        //    "OCFStatus = " + NewOCFStatus + " AND OCFType = '" + OCFType + "' AND " +
        //    "AND StatusDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "' AND " +
        //    "convert(varchar(8), StatusTime, 8)  = '" + ConvertTimeFomatHHMMSS(AdjTime) + "'");

        //    try
        //    {
        //        if (reader.Read() == true)
        //        {
        //            if (MyReaderFldToString(reader, "HCAIDocNumber") == HCAIDocNumber)
        //            {
        //                RetValue = false;
        //            }

        //        }
        //        reader.Close();
        //    }

        //    catch (Exception ex)
        //    {
        //        Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
        //                "in function: IsValidAdjResponseToUpdate \n" +
        //                "ERROR MSG: " + ex.Message.ToString());         
        //    }
        //    reader = null;
        //    return RetValue;
        //}

        private OCFMapReturnType UpdateOCF21BReply(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, DateTime rDate, string rTime)
        {
            string AdjTime;
            int NewStatus;
            string aStatus;
            OCFMapReturnType Succ = new OCFMapReturnType();

            long docId = ConvertStrToLong(Reply.PMS_Document_Key);

            DateTime AdjReplyDate = MyConvert.MyHcaiDateConvert(Reply.Adjuster_Response_Date);
            AdjTime = AdjReplyDate.ToShortTimeString();

            if (Config.HcaiBehavior == Config.HcaiStatusBehavior.DocumentStatusBased)
            {
                //aStatus = Reply.Document_Status;
                if (Reply.Document_Status.ToLower() == "approved" || Reply.Document_Status.ToLower() == "responded")
                    aStatus = "Approved";
                else if (Reply.Document_Status.ToLower() == "partially approved")
                    aStatus = "Partially Approved";
                else //if (Reply.Document_Status == "declined)
                    aStatus = "Not Approved";

                NewStatus = (int)GetHCAIStatus(aStatus);
            }
            else //Config.HcaiStatusBehavior.LineItemBased
            {

                aStatus = GetDocumentStatus(mCommand,
                                            ConvertStrToDouble(Reply.OCF21B_InsurerTotals_AutoInsurerTotal_Approved),
                                            docId, OCFType);

                if (aStatus == "Approved")
                    NewStatus = 14;
                else if (aStatus == "Partially Approved")
                    NewStatus = 13;
                else //denied                
                    NewStatus = 12;
            }

            //CHECK IF THIS STATUS ALREADY IN THE SYSTEM        
            //string AdjTime2 = Reply.Adjuster_Response_Date.Substring(11, 8).ToString();
            //AdjTime2 = AdjTime2.Substring(0, 2) + ":" + AdjTime2.Substring(3, 2) + ":00"; // +" " + rTime.Substring(9, 2);
            //////////////////////////////////////////////////////////////////////////////////////////////////
            if (IsItemAlreadyInCurrentStatus(mCommand, OCFType, docId,
                NewStatus, AdjReplyDate.ToLongTimeString(), AdjReplyDate, Reply.HCAI_Document_Number.ToString()) == true)
            {
                Succ.ForceAdjusterAck = true;
            }

            ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET InvoiceReplyDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', " +
            "RepliedAs = '" + aStatus.Replace("'", "''") + "'," +
            "ApprovedAmt = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_AutoInsurerTotal_Approved) + " where InvoiceID = " + docId);

            // TO TEST NOV 2014 RELEASE
            //UPDATE ADJUSTER RESPONSE IN INVOICE GENERAL INFO 
            //if (DateTime.Now >= Config.Nov2014) // NEEDS TESTING
            //{
            //todo - Ver3.13 need to update query to include OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation and OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation
            ExecQueryForWrite(mCommand, "UPDATE MVAInvoice SET MOHTotalApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost) + ", " +
            "MOHTotalApprovedReasonCode = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "MOHTotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "MOHTotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC1TotalApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost) + ", " +
            "EHC1TotalApprovedReasonCode = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC1TotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC1TotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC2TotalApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost) + ", " +
            "EHC2TotalApprovedReasonCode = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC2TotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC2TotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "MOHTotalApprovedLineCost_Debit = " + ConvertStrToDouble(Reply.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost) + ", " +
            "MOHTotalApprovedReasonCode_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "MOHTotalApprovedReasonCodeDesc_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "MOHTotalApprovedReasonCodeDescOther_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC1TotalApprovedLineCost_Debit = " + ConvertStrToDouble(Reply.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost) + ", " +
            "EHC1TotalApprovedReasonCode_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC1TotalApprovedReasonCodeDesc_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC1TotalApprovedReasonCodeDescOther_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC2TotalApprovedLineCost_Debit = " + ConvertStrToDouble(Reply.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost) + ", " +
            "EHC2TotalApprovedReasonCode_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC2TotalApprovedReasonCodeDesc_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC2TotalApprovedReasonCodeDescOther_Debit = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "InsurerTotalMOHApproved = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_MOH_Approved) + ", " +
            "InsurerTotalEHC12Approved = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_OtherInsurers_Approved) + ", " +
            "AutoInsurerTotalApproved = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_AutoInsurerTotal_Approved) + ", " +
            "GSTApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_GST_Approved_LineCost) + ", " +
            "GSTApprovedReasonCode = '" + MyReplace(Reply.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "GSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "GSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "PSTApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_PST_Approved_LineCost) + ", " +
            "PSTApprovedReasonCode = '" + MyReplace(Reply.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "PSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "PSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "SubtotalGSApproved = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved) + ", " +
            "TotalInteresedApproved = " + ConvertStrToDouble(Reply.OCF21B_InsurerTotals_Interest_Approved_LineCost) + ", " +
            "SubTotalOtherGS = 0, " +
            "AReimbursable_AdjusterResponseExplanation = '" + MyReplace(Reply.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation) + "', " +
            "AOtherInsuranceAmounts_AdjusterResponseExplanation = '" + MyReplace(Reply.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation) + "', " +
            "AClaimFormReceived = '" + MyReplace(Reply.OCF21B_InsurerSignature_ClaimFormReceived) + "', " +
            "AClaimFormReceivedDate = '" + MyReplace(Reply.OCF21B_InsurerSignature_ClaimFormReceivedDate) + "', " +
            "ApprovedByOnPDF = '" + MyReplace(Reply.ApprovedByOnPDF) + "' " +
            "where InvoiceID = " + docId);



            AdjusterResponseOCF21BLineItem OCF21BItem;
            string str_sql = "";
            OCF21BItem = Reply.OCF21B_LineItem;

            while (OCF21BItem != null)
            {
                // Console.WriteLine(GetTax(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST, "GST"));

                str_sql = str_sql + "UPDATE InvoiceItemDetails SET aItemRate = " + ConvertStrToDouble(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost) + "/ItemQty, " +
                "aItemQty = ItemQty, " +
                "aItemAmount = " + ConvertStrToDouble(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost) + "," +
                "aGST = " + GetTax(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST, "GST") + ", " +
                "aPST = " + GetTax(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST, "PST") + ", " +
                "aAdjusterReasonCode = '" + MyReplace(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode) + "', " +
                "aAdjusterReasonCodeDesc = '" + MyReplace(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
                "aAdjusterReasonOtherCodeDesc = '" + MyReplace(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "' " +
                "WHERE InvoiceID = " + docId + " AND PMSGSKey = " + ConvertStrToInt(OCF21BItem.OCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey) + " ";

                OCF21BItem = Reply.OCF21B_LineItem;
            }
            if (str_sql != "")
                RunSqlTransaction(str_sql);


            //Update HCAILOG Table
            //SqlDataReader reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + docId);

            SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
            "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
            "from HCAIStatusLog H " +
            "where  H.InvoiceID =  " + docId + " AND " +
            "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");

            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                if (!ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, docId, NewStatus, NewOrder, OCFType, AdjReplyDate, AdjTime, "", Reply.HCAI_Document_Number))
                {
                    //WRITE ERROR LOG
                    UOServiceLib.Log.ErrorFormat("ResetOCFHCAIStatusAndRecordNewForHCAIResponses failed for OCF Doc {0}, HCAI# {1}", OCFType, Reply.HCAI_Document_Number);
                }
            }
            else
            {
                UOServiceLib.Log.Error("UpdateOCF21BReply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: UpdateOCF21BReply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);

                //WRITE ERROR LOG
                reader.Close();
            }
            reader = null;

            //RECIEVE OCF9
            if (NewStatus == 14 || NewStatus == 13)
            {
                //approved or partially approved
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "B", true);
            }
            else
            {
                //denied
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "B", false);
            }

            //update succ return
            Succ.OCFType = OCFType;
            Succ.HCAI_Document_Number = Reply.HCAI_Document_Number;
            Succ.OCFID = docId;
            Succ.New_HCAI_Status = (HcaiStatus)NewStatus;
            Succ.StatusDate = AdjReplyDate.ToString(HCAITableInfo.DateFormat);
            Succ.StatusTime = AdjTime;
            Succ.ATotal = MyConvert.ConvertStrToFloat(Reply.OCF21B_InsurerTotals_AutoInsurerTotal_Approved);
            Succ.MinSentDate = GetMinSentDate(mCommand, docId, OCFType);
            return Succ;
        }

        private OCFMapReturnType UpdateOCF21CReply(SqlCommand mCommand, AdjusterResponse Reply, string OCFType, DateTime rDate, string rTime)
        {

            //string AdjDate;
            string AdjTime;
            int NewStatus;
            string aStatus;
            OCFMapReturnType Succ = new OCFMapReturnType();


            long docId = ConvertStrToLong(Reply.PMS_Document_Key);


            DateTime AdjReplyDate = MyConvert.MyHcaiDateConvert(Reply.Adjuster_Response_Date);
            AdjTime = AdjReplyDate.ToShortTimeString();



            if (Config.HcaiBehavior == Config.HcaiStatusBehavior.DocumentStatusBased)
            {
                //aStatus = Reply.Document_Status;
                if (Reply.Document_Status.ToLower() == "approved" || Reply.Document_Status.ToLower() == "responded")
                    aStatus = "Approved";
                else if (Reply.Document_Status.ToLower() == "partially approved")
                    aStatus = "Partially Approved";
                else //if (Reply.Document_Status == "declined)
                    aStatus = "Not Approved";

                NewStatus = (int)GetHCAIStatus(aStatus);
            }
            else //Config.HcaiStatusBehavior.LineItemBased
            {
                aStatus = GetDocumentStatus(mCommand,
                                            ConvertStrToDouble(Reply.OCF21C_InsurerTotals_AutoInsurerTotal_Approved),
                                            docId, OCFType);

                if (aStatus == "Approved")
                    NewStatus = 14;
                else if (aStatus == "Partially Approved")
                    NewStatus = 13;
                else //denied                
                    NewStatus = 12;
            }
            //CHECK IF THIS STATUS ALREADY IN THE SYSTEM
            //string AdjTime2 = Reply.Adjuster_Response_Date.Substring(11, 8).ToString();
            //AdjTime2 = AdjTime2.Substring(0, 2) + ":" + AdjTime2.Substring(3, 2) + ":00"; // +" " + rTime.Substring(9, 2);
            ////////////////////////////////////////////////////////////////////////////////////////////
            if (IsItemAlreadyInCurrentStatus(mCommand, OCFType, docId,
                NewStatus, AdjReplyDate.ToLongTimeString(), AdjReplyDate, Reply.HCAI_Document_Number.ToString()) == true)
            {
                Succ.ForceAdjusterAck = true;
            }

            ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET InvoiceReplyDate = '" + AdjReplyDate.ToString("yyyy/MM/dd") + "', " +
            "RepliedAs = '" + aStatus.Replace("'", "''") + "'," +
            "ApprovedAmt = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_AutoInsurerTotal_Approved) + " where InvoiceID = " + docId);

            //UPDATE ADJUSTER RESPONSE IN INVOICE GENERAL INFO 
            //if (DateTime.Now >= Config.Nov2014) // NEEDS TESTING
            //{
            //todo - Ver3.13 need to update query to include OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation and OCF21C_PAFReimbursableFees_AdjusterResponseExplanation
            ExecQueryForWrite(mCommand, "UPDATE MVAInvoice SET MOHTotalApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost) + ", " +
            "MOHTotalApprovedReasonCode = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "MOHTotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "MOHTotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC1TotalApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost) + ", " +
            "EHC1TotalApprovedReasonCode = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC1TotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC1TotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC2TotalApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost) + ", " +
            "EHC2TotalApprovedReasonCode = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC2TotalApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC2TotalApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "MOHTotalApprovedLineCost_Debit = " + ConvertStrToDouble(Reply.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost) + ", " +
            "MOHTotalApprovedReasonCode_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "MOHTotalApprovedReasonCodeDesc_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "MOHTotalApprovedReasonCodeDescOther_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC1TotalApprovedLineCost_Debit = " + ConvertStrToDouble(Reply.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost) + ", " +
            "EHC1TotalApprovedReasonCode_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC1TotalApprovedReasonCodeDesc_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC1TotalApprovedReasonCodeDescOther_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "EHC2TotalApprovedLineCost_Debit = " + ConvertStrToDouble(Reply.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost) + ", " +
            "EHC2TotalApprovedReasonCode_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "EHC2TotalApprovedReasonCodeDesc_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "EHC2TotalApprovedReasonCodeDescOther_Debit = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "InsurerTotalMOHApproved = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_MOH_Approved) + ", " +
            "InsurerTotalEHC12Approved = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_OtherInsurers_Approved) + ", " +
            "AutoInsurerTotalApproved = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_AutoInsurerTotal_Approved) + ", " +
            "GSTApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_GST_Approved_LineCost) + ", " +
            "GSTApprovedReasonCode = '" + MyReplace(Reply.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "GSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "GSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "PSTApprovedLineCost = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_PST_Approved_LineCost) + ", " +
            "PSTApprovedReasonCode = '" + MyReplace(Reply.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode) + "', " +
            "PSTApprovedReasonCodeDesc = '" + MyReplace(Reply.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
            "PSTApprovedReasonCodeDescOther = '" + MyReplace(Reply.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "', " +
            "SubtotalGSApproved = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_SubTotalPreApproved_Approved) + ", " +
            "TotalInteresedApproved = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_Interest_Approved_LineCost) + ", " +
            "SubTotalOtherGS = " + ConvertStrToDouble(Reply.OCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved) + ", " +
            "AReimbursable_AdjusterResponseExplanation = '" + MyReplace(Reply.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation) + "', " +
            "AOtherInsuranceAmounts_AdjusterResponseExplanation = '" + MyReplace(Reply.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation) + "', " +
            "AClaimFormReceived = '" + MyReplace(Reply.OCF21C_InsurerSignature_ClaimFormReceived) + "', " +
            "AClaimFormReceivedDate = '" + MyReplace(Reply.OCF21C_InsurerSignature_ClaimFormReceivedDate) + "', " +
            "ApprovedByOnPDF = '" + MyReplace(Reply.ApprovedByOnPDF) + "' " +
            "where InvoiceID = " + docId);



            //UPADATE PAF LINE ITMES
            AdjusterResponseOCF21CPAFReimbursableLineItem OCF21CItem;
            string str_sql = "";
            //double ASubtotal = 0;

            OCF21CItem = Reply.OCF21C_PAFReimbursable_LineItem;

            while (OCF21CItem != null)
            {
                str_sql = str_sql + "UPDATE InvoiceItemDetails SET aItemRate = " + ConvertStrToDouble(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost) + ", " +
                "aItemQty = ItemQty, " +
                "aItemAmount = " + ConvertStrToDouble(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost) + "," +
                "aGST = 0, aPST = 0, " +
                "aAdjusterReasonCode = '" + MyReplace(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode) + "', " +
                "aAdjusterReasonCodeDesc = '" + MyReplace(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc) + "', " +
                "aAdjusterReasonOtherCodeDesc = '" + MyReplace(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode) + "' " +
                "WHERE InvoiceID = " + docId + " AND ItemVCForShow = 2 AND PMSGSKey = " + ConvertStrToInt(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey) + " ";

                //ASubtotal = ASubtotal + ConvertStrToDouble(OCF21CItem.OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost.ToString());
                OCF21CItem = Reply.OCF21C_PAFReimbursable_LineItem;
            }
            //EXECUTE -- UPDATE DB
            if (str_sql != "")
                RunSqlTransaction(str_sql);

            //UPDATE OTHER ITEMS
            str_sql = "";

            AdjusterResponseOCF21COtherReimbursableLineItem OCF21CItemGS;
            OCF21CItemGS = Reply.OCF21C_OtherReimbursable_LineItem;
            while (OCF21CItemGS != null)
            {

                //OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST

                str_sql = str_sql + "UPDATE InvoiceItemDetails SET aItemRate = " + ConvertStrToDouble(OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost) + ", " +
                "aItemQty = ItemQty, " +
                "aItemAmount = " + ConvertStrToDouble(OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost) + "," +
                "aGST = 0, aPST = 0, " +
                "aAdjusterReasonCode = '" + MyReplace(OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode) + "', " +
                "aAdjusterReasonCodeDesc = '" + MyReplace(OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc) + "', " +
                "aAdjusterReasonOtherCodeDesc = '" + MyReplace(OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc) + "' " +
                "WHERE InvoiceID = " + docId + " AND ItemVCForShow = 0 AND PMSGSKey = " + ConvertStrToInt(OCF21CItemGS.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey) + " ";

                OCF21CItemGS = Reply.OCF21C_OtherReimbursable_LineItem;
            }
            //EXECUTE -- UPDATE DB
            if (str_sql != "")
                RunSqlTransaction(str_sql);

            //Update HCAILOG Table
            //SqlDataReader reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + docId);
            SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
            "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
            "from HCAIStatusLog H " +
            "where  H.InvoiceID =  " + docId + " AND " +
            "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");

            if (reader.Read() == true)
            {
                int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) + 1;
                reader.Close();
                if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, docId, NewStatus, NewOrder, OCFType, AdjReplyDate, AdjTime, "", Reply.HCAI_Document_Number) == false)
                {
                    //WRITE ERROR LOG
                    UOServiceLib.Log.ErrorFormat("ResetOCFHCAIStatusAndRecordNewForHCAIResponses failed for OCF Doc {0}, HCAI# {1}", OCFType, Reply.HCAI_Document_Number);
                }
            }
            else
            {
                UOServiceLib.Log.Error("UpdateOCF21CReply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: UpdateOCF21CReply, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + docId);
                //WRITE ERROR LOG
                reader.Close();
            }
            reader = null;

            //RECIEVE OCF9
            if (NewStatus == 14 || NewStatus == 13)
            {
                //approved or partially approved
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "C", true);
            }
            else
            {
                //denied
                ReceiveAndSaveOCF9(mCommand, Reply, OCFType, "C", false);
            }

            //update succ return
            Succ.OCFType = OCFType;
            Succ.HCAI_Document_Number = Reply.HCAI_Document_Number;
            Succ.OCFID = docId;
            Succ.New_HCAI_Status = (HcaiStatus)NewStatus;

            Succ.StatusDate = AdjReplyDate.ToString(HCAITableInfo.DateFormat);
            Succ.StatusTime = AdjTime;
            Succ.ATotal = MyConvert.ConvertStrToFloat(Reply.OCF21C_InsurerTotals_AutoInsurerTotal_Approved);
            Succ.MinSentDate = GetMinSentDate(mCommand, docId, OCFType);
            return Succ;
        }

        private string GetDueTimeForDB(string DueTime)
        {
            if (DueTime == null || DueTime == "")
                return "NULL";
            else
                return ("'" + DueTime + "'").ToString();
        }

        //private void ReceiveHCAISuccessfullyDelivered(SqlCommand mCommand, long OCFID, string OCFType, string HCAIDocumentNumber, DateTime rDate, string rTime)
        //{
        //    SqlDataReader reader = null;
        //    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
        //        reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + OCFID);
        //    else
        //        reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + OCFID);


        //    if (reader.Read() == true)
        //    {
        //        int NewOrder = ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder"));
        //        reader.Close();

        //        if (ResetOCFHCAIStatusAndRecordNewForHCAIResponses(mCommand, OCFID, 6, NewOrder, OCFType, rDate, rTime, "", HCAIDocumentNumber) == true)
        //        {
        //            //SET STATUS
        //            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
        //            {
        //                ExecQueryForWrite(mCommand, "UPDATE InvoceGeneralInfo SET InvoiceSentDate = '" + rDate.ToString("yyyy/MM/dd") + "', " +
        //                "InvoiceStatus = 1 where InvoiceID = " + OCFID);
        //            }
        //            else
        //            {
        //                DateTime DueDate = GetOCFDueDate(mCommand, OCFType, rDate, rTime);

        //                ExecQueryForWrite(mCommand, "UPDATE PaperWork SET SentDate = '" + rDate.ToString("yyyy/MM/dd") + "', " +
        //                "ReportStatus = 1, RepliedAs = '', ApprovedAmount = 0, DueDate = '" + DueDate.ToString("yyyy/MM/dd") + "', " +
        //                "DueTime = " + GetDueTimeForDB(rTime.ToString()) + ", ReplyDate = NULL " +
        //                "where ReportID = " + OCFID);
        //            }
        //        }
        //        else
        //        {
        //            //WRITE ERROR LOG
        //        }
        //        //SET HCAI DOCUMENT NUMBER
        //        if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
        //            ExecQueryForWrite(mCommand, "UPDATE MVAInvoice SET HCAIDocumentNumber = '" + HCAIDocumentNumber + "' WHERE InvoiceID = " + OCFID);
        //        else
        //            ExecQueryForWrite(mCommand, "UPDATE PaperWork SET HCAIDocumentNumber = '" + HCAIDocumentNumber + "' WHERE ReportID = " + OCFID);
        //    }
        //    else
        //    {
        //        reader.Close();
        //    }
        //    reader = null;
        //}


        private string GetHCAINumberByOCFID(SqlCommand mCommand, long OCFID, string OCFType)
        {
            SqlDataReader reader = null;
            string hNo = "";
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND InvoiceID = " + OCFID);

                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)");

            }
            else
            {
                //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND ReportID = " + OCFID);

                reader = ExecQuery(mCommand, "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)");
            }
            if (reader.Read() == true)
            {
                if (MyReaderFldToString(reader, "HCAIDocNumber") != null && MyReaderFldToString(reader, "HCAIDocNumber") != "")
                {
                    hNo = MyReaderFldToString(reader, "HCAIDocNumber");
                }

            }
            else
            {
                UOServiceLib.Log.Error("GetHCAINumberByOCFID, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: GetHCAINumberByOCFID, hcai current/max record was not found \n" +
                        "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);
            }
            reader.Close();
            reader = null;
            return hNo;
        }

        #endregion


        private void GetHCAIErrorMsg(ErrorMessage e)
        {
            try
            {
                string hStr = "";
                if (e.Error_CompleteErrorDesc.Trim() == "")
                {
                    string[] args = e.Error_HCAI_ErrorType.Split(Commands.SEP, StringSplitOptions.None);
                    if (args[0] == "901")
                    {
                        hStr = args[0] + " - Invalid NeCST message";
                    }
                    else if (args[0] == "902")
                    {
                        hStr = args[0] + " - Unsupported NeCST message";
                    }
                    else if (args[0] == "903")
                    {
                        hStr = args[0] + " - HCAI system error";
                    }
                    else if (args[0] == "101")
                    {
                        hStr = args[0] + " - Document has been previously submitted with the same unique identifier";
                    }
                    else if (args[0] == "102")
                    {
                        hStr = args[0] + " - Insurer or Branch does not exist. Cannot submit document.";
                    }
                    else if (args[0] == "103")
                    {
                        hStr = args[0] + " - Document data validation failed. The following are considered validation errors: \r\n" +
                               "a. Document failed field validation. Mandatory fields missing from the document. \r\n" +
                               "b. Document failed business rules validation. Data in the document did not pass business rule validation. \r\n" +
                               "A full list of specific errors will be returned. \r\n";
                    }
                    else if (args[0] == "104")
                    {
                        hStr = args[0] + " - Document could not be voided. Insurer has already looked at the document";
                    }
                    else if (args[0] == "106")
                    {
                        hStr = args[0] + " - Document number for associated plan is invalid";
                    }
                    else if (args[0] == "107")
                    {
                        hStr = args[0] + " - Document does not exist";
                    }
                    else if (args[0] == "108")
                    {
                        hStr = args[0] + " - User could not be authenticated";
                    }
                    else if (args[0] == "109")
                    {
                        hStr = args[0] + " - User does not have authorization";
                    }

                    else if (args[0] == "111")
                    {
                        hStr = args[0] + " - User is trying to acknowledge an adjuster response for a document that has not been adjusted.";
                    }

                    else if (args[0] == "112")
                    {
                        hStr = args[0] + " - OCF version is invalid. \n Following might be the reasons for the same:  \n " +
                        "a. The OCF version specified in the document being submitted is not the current or immediate previous version. \n " +
                        "b. The OCF version specified in the document being submitted is the immediate previous version to the current version and is no longer valid as its grace period has expired.";
                    }

                    else if (args[0] == "113")
                    {
                        hStr = args[0] + " - The HCAI server is busy. \n This error code is returned when the current request cannot be processed by the HCAI as it is busy.";
                    }
                    else if (args[0] == "114")
                    {
                        hStr = args[0] + " - The request is invalid. \n This error code is returned when the document submitted contains '<' character that is not supported by HCAI because of security reasons.";
                    }
                    else if (args[0] == "115")
                    {
                        hStr = args[0] + " - Activity List Date Validation Error. \n This error code is returned when an invalid start date or end date is specified or the start date specified is greater than or equal to end date while using Activity List.";
                    }

                    else if (args[0] == "201")
                    {
                        hStr = args[0] + " - An error has occurred in the toolkit";
                    }

                    else if (args[0] == "202")
                    {
                        hStr = args[0] + " - User Name has not been specified. \n This error code is returned when the user name is not specified while sending any request from toolkit to HCAI.";
                    }

                    else if (args[0] == "203")
                    {
                        hStr = args[0] + " - Password has not been specified. \n This error code is returned when the password is not specified while sending any request from toolkit to HCAI.";
                    }

                    else if (args[0] == "121")
                    {
                        hStr = args[0] + " - Archived records are not currently accessible.";
                    }

                    else
                    {
                        hStr = args[0];
                    }

                    if (hStr != "")
                        e.Error_CompleteErrorDesc = hStr + "\r\n" + args[1];
                    else
                        e.Error_CompleteErrorDesc = args[1];

                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error(e.Error_HCAI_ErrorType + ". " + ex.Message, ex);
                e.Error_CompleteErrorDesc = e.Error_HCAI_ErrorType;
            }
        }

        //September 11, 2017: document validation before submission to HCAI to avoid multiple submission

        public bool IsValidForHCAISubmission(SqlCommand mCommand, string OCFID, string OCFType)
        {
            string str_sql = "";

            //get last hcai ocf log status order                            
            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                str_sql = "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.InvoiceID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID)";
            }
            else
            {
                str_sql = "select DISTINCT  H.StatusOrder, H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, " +
                "H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber " +
                "from HCAIStatusLog H " +
                "where  H.ReportID =  " + OCFID + " AND " +
                "H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID)";

            }

            string sHCAIDocNumber;
            try
            {
                SqlDataReader reader = ExecQuery(mCommand, str_sql);
                if (reader.Read() == true)
                {
                    int hStatus = ConvertStrToInt(MyReaderFldToString(reader, "OCFStatus"));
                    sHCAIDocNumber = MyReaderFldToString(reader, "HCAIDocNumber");
                                       

                    reader.Close();
                    reader = null;
                    //hStatus == 3 --> skipped, Submission in progress status
                    if ( hStatus == 6 || hStatus == 7 || hStatus == 8 || hStatus == 10 || hStatus == 11 || hStatus == 12 || hStatus == 13 || hStatus == 14 || hStatus == 15)
                    {                        
                        DebugWrite($"Validation FAILED; STATUS IS NOT VALID FOR OCFID: {OCFID} AND OCFType: {OCFType} Status: {hStatus} HCAI DOC Number: {sHCAIDocNumber}");
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                }
                else
                {
                    DebugWrite($"Validation FAILED on ExecQuery; OCFID: {OCFID} AND OCFType: {OCFType}");
                    reader.Close();
                    reader = null;
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                DebugWrite($"Validation FAILED, in Exception for OCFID: {OCFID} AND OCFType: {OCFType}. Exception: {ex.Message}");
                return false;
            }
        }


        public OCFMapReturnType submitOCF18(string ocfId)
        {
            var Succ = new OCFMapReturnType();
            DebugWrite("Submitting OCF18 document OCFID: " + ocfId);

            //connect to DB
            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                Succ.OCFID = ConvertStrToLong(ocfId);
                Succ.OCFType = "OCF18";
                Succ.New_HCAI_Status = HcaiStatus.InformationError;
                return Succ;
            }

            var tDBConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            using (tDBConnection)

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            using (var tCommand = tDBConnection.CreateCommand())
            try
            {
                //VALIDATE BEFORE SUBMISSION
                if (!IsCredentialsValid() || !IsValidForHCAISubmission(mCommand, ocfId, "OCF18"))
                {
                    if (!IsCredentialsValid())
                        DebugWrite("Hcai credentials are empty.");
                    DebugWrite("submitOCF18 aborted.");
                    Succ.OCFID = ConvertStrToLong(ocfId);
                    Succ.OCFType = "OCF18";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;

                    return Succ;
                }

                var msg = pmsTK.createDoc(DocType.OCF18);
                var m_ocf18Keys = new PMSToolkit.DataObjects.OCF18Keys();
                string[] toSplit;

                DebugWrite("Starting OCF18");
                //Software IDs
                var res = msg.addValue(m_ocf18Keys.PMSFields_PMSSoftware, "Antibex Software");
                //DebugWrite("Soft Name.");
                res = msg.addValue(m_ocf18Keys.PMSFields_PMSVersion, "4.2.7");

                var version = "6";
                
                res = msg.addValue(m_ocf18Keys.OCFVersion, version);
                res = msg.addValue(m_ocf18Keys.PMSFields_PMSDocumentKey, ocfId);
                res = msg.addValue(m_ocf18Keys.PMSFields_PMSPatientKey, GetClientIDByOCFID(mCommand, "OCF18", ocfId));

                //get general info
                SqlDataReader reader = ExecQuery(mCommand, GetOCFSqlQuery("OCF18", ocfId));

                if (reader.Read() == true)
                {
                    //DebugWrite("Attachment.");       
                    res = msg.addValue(m_ocf18Keys.AttachmentsBeingSent, Flag(MyReaderFldToString(reader, "chkAttachments")));

                    //Header
                    res = msg.addValue(m_ocf18Keys.Header_ClaimNumber, MyReaderFldToString(reader, "CIClaimNo"));
                    res = msg.addValue(m_ocf18Keys.Header_PolicyNumber, MyReaderFldToString(reader, "CIPolicyNo"));
                    res = msg.addValue(m_ocf18Keys.Header_DateOfAccident, MyReaderFldToDateString(reader, "CIDOA")); //make sure dates are in proper format


                    //if (Sept2010 > DateTime.Now)                    
                    //{
                    //    res = msg.addValue(m_ocf18Keys.OCF18_Header_PlanNumber, MyReaderFldToString(reader, "PlanNo"));
                    //}
                    //Applicant Info
                    res = msg.addValue(m_ocf18Keys.Applicant_Name_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    res = msg.addValue(m_ocf18Keys.Applicant_Name_MiddleName, MyReaderFldToString(reader, "CIMiddleName"));
                    res = msg.addValue(m_ocf18Keys.Applicant_Name_LastName, MyReaderFldToString(reader, "CILastName"));
                    res = msg.addValue(m_ocf18Keys.Applicant_DateOfBirth, MyReaderFldToDateString(reader, "CIDOB"));  //date
                    res = msg.addValue(m_ocf18Keys.Applicant_Gender, Gender(MyReaderFldToString(reader, "CIGender")));

                    //DebugWrite("Gender:" + d.Applicant_Gender + "x");
                    res = msg.addValue(m_ocf18Keys.Applicant_TelephoneNumber, Phone(MyReaderFldToString(reader, "CITel")));
                    res = msg.addValue(m_ocf18Keys.Applicant_TelephoneExtension, ""); //we have no extension
                    res = msg.addValue(m_ocf18Keys.Applicant_Address_StreetAddress1, MyReaderFldToString(reader, "CIAddress"));
                    res = msg.addValue(m_ocf18Keys.Applicant_Address_StreetAddress2, ""); //we have no 2nd address
                    res = msg.addValue(m_ocf18Keys.Applicant_Address_City, MyReaderFldToString(reader, "CICity"));
                    res = msg.addValue(m_ocf18Keys.Applicant_Address_Province, FormatProvince(MyReaderFldToString(reader, "CIProvince")));
                    res = msg.addValue(m_ocf18Keys.Applicant_Address_PostalCode, FormatPostalCode(MyReaderFldToString(reader, "CIPostalCode")));

                    //Insurer Info
                    res = msg.addValue(m_ocf18Keys.Insurer_IBCInsurerID, MyReaderFldToString(reader, "MVAInsurerHCAIID"));
                    res = msg.addValue(m_ocf18Keys.Insurer_IBCBranchID, MyReaderFldToString(reader, "MVABranchHCAIID"));
                    //res = msg.addValue(m_ocf18Keys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(MyReaderFldToString(reader, "MVAPHSame"))); DOUBLE ENTRY
                    res = msg.addValue(m_ocf18Keys.Insurer_Adjuster_Name_FirstName, MyReaderFldToString(reader, "MVAAFirstName"));
                    res = msg.addValue(m_ocf18Keys.Insurer_Adjuster_Name_LastName, MyReaderFldToString(reader, "MVAALastName"));
                    res = msg.addValue(m_ocf18Keys.Insurer_Adjuster_TelephoneNumber, Phone(MyReaderFldToString(reader, "MVAATel")));
                    res = msg.addValue(m_ocf18Keys.Insurer_Adjuster_TelephoneExtension, MyReaderFldToString(reader, "MVAAExt"));
                    res = msg.addValue(m_ocf18Keys.Insurer_Adjuster_FaxNumber, Phone(MyReaderFldToString(reader, "MVAAFax")));
                    res = msg.addValue(m_ocf18Keys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(MyReaderFldToString(reader, "MVAPHSame")));
                    if (Flag(MyReaderFldToString(reader, "MVAPHSame")) != "Yes")
                    {
                        res = msg.addValue(m_ocf18Keys.Insurer_PolicyHolder_Name_LastName, MyReaderFldToString(reader, "MVAPHLastName"));
                        res = msg.addValue(m_ocf18Keys.Insurer_PolicyHolder_Name_FirstName, MyReaderFldToString(reader, "MVAPHFirstName"));
                    }

                    DebugWrite("OCF18_5");
                    //Other Insurer Info
                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage, Flag(MyReaderFldToString(reader, "qEHC")));

                    if (FlagNA(MyReaderFldToString(reader, "qEHC")) == Flag("Yes"))
                    {
                        //if(d.OCF18_OtherInsurance_OtherInsurance_Insurer1_ID != null && d.OCF18_OtherInsurance_OtherInsurance_Insurer1_ID != "") //WHY

                        res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_MOHAvailable, FlagNA(MyReaderFldToString(reader, "qOHIP")));

                        if (MyReaderFldToString(reader, "EHC1IDCertNo") != null && MyReaderFldToString(reader, "EHC1IDCertNo") != "")
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID, MyReaderFldToString(reader, "EHC1IDCertNo"));
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name, MyReaderFldToString(reader, "EHC1InsName"));
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber, MyReaderFldToString(reader, "EHC1PolicyNo"));

                            if (MyReaderFldToString(reader, "EHC1PlanMember") == null || MyReaderFldToString(reader, "EHC1PlanMember") == "")
                            {
                                res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, "");
                                res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, "");
                            }
                            else
                            {
                                toSplit = MyReaderFldToString(reader, "EHC1PlanMember").Split(',');
                                if (toSplit.Length > 1)
                                {
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, toSplit[1].ToString());
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, toSplit[0].ToString());
                                }
                                else
                                {
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, "");
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, toSplit[0].ToString());
                                }
                            }
                        }
                        else
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, "N/A"); //WHY? if needed correct above
                        }

                        DebugWrite("OCF18 2nd Insurer");

                        if (MyReaderFldToString(reader, "EHC2IDCertNo") != null && MyReaderFldToString(reader, "EHC2IDCertNo") != "")
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID, MyReaderFldToString(reader, "EHC2IDCertNo"));
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name, MyReaderFldToString(reader, "EHC2InsName"));
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber, MyReaderFldToString(reader, "EHC2PolicyNo"));
                            if (MyReaderFldToString(reader, "EHC2PlanMember") == null || MyReaderFldToString(reader, "EHC2PlanMember") == "")
                            {
                                res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, "");
                                res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, "");
                            }
                            else
                            {
                                toSplit = MyReaderFldToString(reader, "EHC2PlanMember").Split(',');
                                if (toSplit.Length > 1)
                                {
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, toSplit[1].ToString());
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, toSplit[0].ToString());
                                }
                                else
                                {
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, "");
                                    res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, toSplit[0].ToString());
                                }
                            }
                        }
                        else
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, "N/A"); //WHY? if needed correct above
                        }
                    }
                    DebugWrite("OCF18_6");

                    //Health Practitioner
                    if (Flag(MyReaderFldToString(reader, "chkIsSHPOther")) == Flag("No"))
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_FacilityRegistryID, MyReaderFldToString(reader, "SHPHCAIFacilityID"));
                        res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_ProviderRegistryID, MyReaderFldToString(reader, "SHPHCAIRegNo"));
                        res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_Occupation, GetHCAIProviderOccupation(tCommand, MyReaderFldToString(reader, "SHPOccupation")));

                        //if (Sept2010 > DateTime.Now)
                        //{
                        //    res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists, ReturnConflictsOpt(MyReaderFldToString(reader, "SHPOptConflicts")));
                        //    res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails, MyReaderFldToString(reader, "SHPDeclareText"));
                        //}

                        res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_IsSignatureOnFile, Flag("Yes"));  //STATIC FOR NOW
                        res = msg.addValue(m_ocf18Keys.OCF18_HealthPractitioner_DateSigned, MyReaderFldToDateString(reader, "SHPSignedDate"));
                    }
                    res = msg.addValue(m_ocf18Keys.OCF18_IsOtherHealthPractitioner, Flag(MyReaderFldToString(reader, "chkIsSHPOther")));

                    if (Flag(MyReaderFldToString(reader, "chkIsSHPOther")) == Flag("Yes"))
                    {
                        toSplit = MyReaderFldToString(reader, "SHPName").Split(',');
                        if (toSplit.Length > 1)
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_FirstName, toSplit[0]);
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_LastName, toSplit[1]);
                        }
                        else if (toSplit.Length == 1)
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_FirstName, toSplit[0]);
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_LastName, "");
                        }
                        else
                        {
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_FirstName, "");
                            res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_LastName, "");
                        }

                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_FacilityRegistryID, MyReaderFldToString(reader, "OtherHealthPractitioner_FacilityRegistryID"));

                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_FacilityName, MyReaderFldToString(reader, "SHPFacilityName"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_AISIFacilityNumber, MyReaderFldToString(reader, "SHPAISINo"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress1, MyReaderFldToString(reader, "SHPAddress"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress2, "");
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_City, MyReaderFldToString(reader, "SHPCity"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_Province, FormatProvince(MyReaderFldToString(reader, "SHPProvince")));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_PostalCode, FormatPostalCode(MyReaderFldToString(reader, "SHPPostalCode")));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_TelephoneNumber, Phone(MyReaderFldToString(reader, "SHPTel")));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_TelephoneExtension, MyReaderFldToString(reader, "SHPExt"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_FaxNumber, Phone(MyReaderFldToString(reader, "SHPFax")));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Email, MyReaderFldToString(reader, "SHPEmail"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_ProviderRegistryNumber, MyReaderFldToString(reader, "SHPCollegeNo"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_Occupation, GetHCAIProviderOccupation(tCommand, MyReaderFldToString(reader, "SHPOccupation")));

                        //if (Sept2010 > DateTime.Now)
                        //{
                        //    res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists, ReturnConflictsOpt(MyReaderFldToString(reader, "SHPOptConflicts")));
                        //    res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails, MyReaderFldToString(reader, "SHPDeclareText"));
                        //}
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_IsSignatureOnFile, Flag("Yes"));
                        res = msg.addValue(m_ocf18Keys.OCF18_OtherHealthPractitioner_DateSigned, MyReaderFldToDateString(reader, "SHPSignedDate"));	//Date
                    }

                    //Regulated Health Practitioner
                    res = msg.addValue(m_ocf18Keys.OCF18_IsRHPSameAsHealthPractitioner, Flag(MyReaderFldToString(reader, "SRHPSame")));
                    if (Flag(MyReaderFldToString(reader, "SRHPSame")) != Flag("Yes"))
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID, MyReaderFldToString(reader, "SRHPHCAIFacilityID"));
                        res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_ProviderRegistryID, MyReaderFldToString(reader, "SRHPHCAIRegNo"));
                        res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_Occupation, GetHCAIProviderOccupation(tCommand, MyReaderFldToString(reader, "SRHPOccupation"))); //WORKING

                        //if (Sept2010 > DateTime.Now)
                        //{
                        //    res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists, ReturnConflictsOpt(MyReaderFldToString(reader, "SRHPOptConflicts")));
                        //    res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails, MyReaderFldToString(reader, "SRHPDeclareText"));
                        //}
                        res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_IsSignatureOnFile, Flag("Yes"));
                        res = msg.addValue(m_ocf18Keys.OCF18_RegulatedHealthProfessional_DateSigned, MyReaderFldToDateString(reader, "SRHPSignedDate"));
                    }

                    DebugWrite("OCF18_7");
                    //Injury Codes
                    PMSToolkit.DataObjects.OCF18InjuryLineItemKeys m_injKeys = new PMSToolkit.DataObjects.OCF18InjuryLineItemKeys();
                    IDataItem inj;
                    SqlDataReader inj_reader = ExecQuery(tCommand, "select * from OCFInjuries where ReportID = " + ocfId + " order by iOrder");
                    while (inj_reader.Read())
                    {
                        inj = msg.newLineItem(PMSToolkit.LineItemType.InjuryLineItem);
                        inj.addValue(m_injKeys.OCF18_InjuriesAndSequelae_Injury_Code, MyReaderFldToString(inj_reader, "Code"));
                        res = msg.addLineItem(inj);
                    }
                    inj_reader.Close();
                    inj_reader = null;

                    DebugWrite("OCF18_7.3");

                    //Prior and Concurrent Conditions
                    res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Response, FlagUNK(MyReaderFldToString(reader, "Part8A1Opt")));
                    if (FlagUNK(MyReaderFldToString(reader, "Part8A1Opt")) == Flag("Yes"))
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation, MyReaderFldToString(reader, "Part8A1Text"));
                        res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response, FlagUNK(MyReaderFldToString(reader, "Part8A2Opt")));
                        res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation, MyReaderFldToString(reader, "Part8A2Text"));
                    }
                    res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response, FlagUNK(MyReaderFldToString(reader, "Part8BOpt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation, MyReaderFldToString(reader, "Part8BText"));

                    
                    res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Response, FlagNA(MyReaderFldToString(reader, "Part8COpt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance, MyReaderFldToString(reader, "PAF_Circumstance"));
                    
                    res = msg.addValue(m_ocf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation, MyReaderFldToString(reader, "Part8CText"));

                    DebugWrite("OCF18_7.5");
                    //Activity Limitations
                    res = msg.addValue(m_ocf18Keys.OCF18_ActivityLimitations_ToEmployment_Response, EmploymentFlag(MyReaderFldToString(reader, "Part9A1Opt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_ActivityLimitations_ToNormalLife_Response, FlagUNK(MyReaderFldToString(reader, "Part9A2Opt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_ActivityLimitations_ImpactOnAbilities, MyReaderFldToString(reader, "Part9BText"));
                    res = msg.addValue(m_ocf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Response, EmploymentFlag(MyReaderFldToString(reader, "Part9COpt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Explanation, MyReaderFldToString(reader, "Part9CText"));

                    DebugWrite("OCF18_7.6");

                    //Plans Goals Outcomes
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction, Flag(MyReaderFldToString(reader, "Part10AIChkPR")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength, Flag(MyReaderFldToString(reader, "Part10AIChkIS")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion, Flag(MyReaderFldToString(reader, "Part10AIChkIRM")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other, Flag(MyReaderFldToString(reader, "Part10AIChkO")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription, MyReaderFldToString(reader, "Part10AIOText"));

                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving, Flag(MyReaderFldToString(reader, "Part10AIIChkRANL")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities, Flag(MyReaderFldToString(reader, "Part10AIIChkRMWA")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities, Flag(MyReaderFldToString(reader, "Part10AIIChkRPWA")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other, Flag(MyReaderFldToString(reader, "Part10AIIChkO")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription, MyReaderFldToString(reader, "Part10AIIOText"));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation, MyReaderFldToString(reader, "Part10BIText"));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact, MyReaderFldToString(reader, "Part10BIIText"));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response, Flag(MyReaderFldToString(reader, "Part10CIOpt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation, MyReaderFldToString(reader, "Part10CIText"));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response, Flag(MyReaderFldToString(reader, "Part10CIIOpt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation, MyReaderFldToString(reader, "Part10CIIText"));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response, FlagUNK(MyReaderFldToString(reader, "Part10DOpt")));
                    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation, MyReaderFldToString(reader, "Part10DText"));

                    //if (Sept2010 > DateTime.Now)                    
                    //{
                    //    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response, Flag(MyReaderFldToString(reader, "Part10EOpt")));
                    //    res = msg.addValue(m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation, MyReaderFldToString(reader, "Part10EText"));                    
                    //}
                    DebugWrite("OCF18_8");
                    /////////////////////////// SESSIONS ARE NOT SUPPORTED IN CURRNET UO VERSION /////////////////////////////////////////
                    //Proposed Goods and Services (Session)
                    //PMSToolkit.DataObjects.SessionHeaderLineItemKeys gshKeys = new PMSToolkit.DataObjects.SessionHeaderLineItemKeys();
                    //IDataItem GSHeader;
                    //HCAIOCF18SesionHeaderEstimated sHeader = null; //d.OCF18_ProposedGoodsAndServices_SessionHeader_Estimated;
                    //PMSToolkit.DataObjects.SessionLineItemKeys liKeys = new PMSToolkit.DataObjects.SessionLineItemKeys();
                    //IDataItem GSLineItem;
                    //HCAIOCF18SesionLineItemEstimated sLineItem;
                    ////DebugWrite("starting Session");
                    //while(sHeader != null)
                    //{
                    //    GSHeader = msg.newLineItem(PMSToolkit.LineItemType.GS18SessionHeaderLineItem);
                    //    //deleted---GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_ReferenceNumber, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_ReferenceNumber);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Quantity, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Quantity);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Measure, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Measure);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count);
                    //    GSHeader.addValue(gshKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost, sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost);
                    //    sLineItem = sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_SessionLineItem;
                    //    while(sLineItem != null)
                    //    {
                    //        GSLineItem = GSHeader.newLineItem(PMSToolkit.LineItemType.GS18SessionLineItem);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure);
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST, Flag(sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST));
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST, Flag(sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST));
                    //        GSLineItem.addValue(liKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost, sLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost);
                    //        GSHeader.addLineItem(GSLineItem);
                    //        sLineItem = sHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_SessionLineItem;
                    //    }
                    //    msg.addLineItem(GSHeader);
                    //    DebugWrite("OCF18_8");
                    //    sHeader = d.OCF18_ProposedGoodsAndServices_SessionHeader_Estimated;
                    //}


                    //DebugWrite("End Session");
                    //deleted--//Proposed Goods and Services (Session) Approved Header
                    //PMSToolkit.DataObjects.SessionHeaderLineItemKeys gshaKeys = new PMSToolkit.DataObjects.SessionHeaderLineItemKeys();
                    //IDataItem GSAHeader;
                    //HCAIOCF18SessionHeaderApproved saHeader = null; d.OCF18_ProposedGoodsAndServices_SessionHeader_Approved;
                    //while(saHeader != null)
                    //{
                    //    GSAHeader = msg.newLineItem(PMSToolkit.LineItemType.GS18SessionHeaderLineItem);
                    //    GSAHeader.addValue(gshaKeys. OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost, saHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost);
                    //    GSAHeader.addValue(gshaKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count, saHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count);
                    //    GSAHeader.addValue(gshaKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost, saHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost);
                    //    GSAHeader.addValue(gshaKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode, saHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode);
                    //    GSAHeader.addValue(gshaKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc, saHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc);
                    //    GSAHeader.addValue(gshaKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc, saHeader.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc);
                    //    msg.addLineItem(GSAHeader);
                    //    saHeader = d.OCF18_ProposedGoodsAndServices_SessionHeader_Approved;
                    //}


                    //Proposed Goods and Services (Non-Session) Estimated 
                    PMSToolkit.DataObjects.GS18LineItemKeys liNonSKeys = new PMSToolkit.DataObjects.GS18LineItemKeys();
                    IDataItem li;
                    //HCAIOCF18NonSesionEstimated item = d.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Estimated;

                    SqlDataReader items_reader = ExecQuery(tCommand, "select * from OCFGoodsAndServices where ReportID = " + ocfId + " order by ItemOrder");

                    //string t ="";

                    //DebugWrite("Starting Non Session");
                    while (items_reader.Read())
                    {
                        //DebugWrite("\t Line Item: " + c++);
                        li = msg.newLineItem(PMSToolkit.LineItemType.GS18LineItem);
                        //if(li == null)
                        //	DebugWrite("Line item is NullReferenceException");
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey, MyReaderFldToString(items_reader, "ItemOrder"));
                        //deleted---li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ReferenceNumber,  MyReaderFldToString(items_reader, "ItemOrder"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", ""));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute, MyReaderFldToString(items_reader, "ItemAttribute"));

                        if (MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "GXX99" || MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "AXXOT")
                        {
                            li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Description, MyReaderFldToString(items_reader, "ItemDescription")); //todo - Ver3.13    
                        }

                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, (MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1)).ToString() + "ProviderHCAINo"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "Type"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity, MyReaderFldToString(items_reader, "ItemBaseUnit"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure, MyReaderFldToString(items_reader, "ItemMeasure"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST, FlagTax(MyReaderFldToString(items_reader, "GST")));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST, FlagTax(MyReaderFldToString(items_reader, "PST")));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemRate")).ToString("0.00"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count, MyReaderFldToString(items_reader, "ItemQty"));
                        li.addValue(liNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount")).ToString("0.00"));
                        msg.addLineItem(li);

                        //item = d.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Estimated;
                    }

                    items_reader.Close();
                    items_reader = null;
                    //DebugWrite("End Non Session");
                    //deleted---//Proposed Goods and Services (Non-Session) Approved 
                    //                PMSToolkit.DataObjects.GS18LineItemKeys aliNonSKeys = new PMSToolkit.DataObjects.GS18LineItemKeys();
                    //                IDataItem ali;
                    //                HCAIOCF18NonSesionApproved itemA = d.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Approved;
                    //DebugWrite("OCF18_10");
                    //                while(itemA != null)
                    //                {
                    //                    ali = msg.newLineItem(PMSToolkit.LineItemType.GS18LineItem);

                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST, Flag(itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST));
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST, Flag(itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST));
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost, itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost);
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count, itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count);
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost, itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost);
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode, itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode);
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc, itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc);
                    //                    ali.addValue(aliNonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc, itemA.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc);

                    //                    msg.addLineItem(ali);
                    //                    itemA = d.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Approved;
                    //                }


                    //DONE == TODO Nov2014 -- add field to database, mandatory  
                    if (Flag(MyReaderFldToString(reader, "optApplicantInitialsOnFile")) == "Yes")
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile, "Yes");
                    }
                    else
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile, "No");
                    }

                    res = msg.addValue(m_ocf18Keys.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration, MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "Weeks")).ToString());
                    res = msg.addValue(m_ocf18Keys.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits, MyReaderFldToString(reader, "Visits"));
                    res = msg.addValue(m_ocf18Keys.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments, MyReaderFldToString(reader, "Comments"));

                    //Totals
                    res = msg.addValue(m_ocf18Keys.OCF18_InsurerTotals_MOH_Proposed, "-" + MyReaderFldToString(reader, "OHIPTotal"));
                    res = msg.addValue(m_ocf18Keys.OCF18_InsurerTotals_OtherInsurers_Proposed, "-" + MyReaderFldToString(reader, "EHC12Total"));
                    res = msg.addValue(m_ocf18Keys.OCF18_InsurerTotals_GST_Proposed, MyReaderFldToString(reader, "GST"));
                    res = msg.addValue(m_ocf18Keys.OCF18_InsurerTotals_PST_Proposed, MyReaderFldToString(reader, "PST"));
                    res = msg.addValue(m_ocf18Keys.OCF18_InsurerTotals_SubTotal_Proposed, MyReaderFldToString(reader, "Subtotal"));
                    res = msg.addValue(m_ocf18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Proposed, MyReaderFldToString(reader, "Total"));

                    //Signature of insurer
                    if (Flag(MyReaderFldToString(reader, "chkWaivedByInsurer")) == "Yes")
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer, "Yes"); //WHY? this is for insurarers
                    }
                    else
                    {
                        res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer, "No"); //WHY? this is for insurarers
                    }

                    //res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureOnFile, "Yes"); //always yes for now
                    //res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_SigningApplicant_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    //res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_SigningApplicant_LastName, MyReaderFldToString(reader, "CILastName"));

                    res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureOnFile, Flag(MyReaderFldToString(reader, "chkSignatureOnFile")));
                    res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_SigningApplicant_FirstName, MyReaderFldToString(reader, "CISignedBy"));
                    res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_SigningApplicant_LastName, MyReaderFldToString(reader, "CISignedByLastName"));


                    res = msg.addValue(m_ocf18Keys.OCF18_ApplicantSignature_SigningDate, MyReaderFldToDateString(reader, "CISignedDate"));

                    res = msg.addValue(m_ocf18Keys.AdditionalComments, MyReaderFldToString(reader, "OCF18AdditionalComments"));
                }
                else
                {
                    //OCF DOES NOT EXISTS???
                    DebugWrite("error geting ocf from db: reportid: " + ocfId.ToString());
                }

                reader.Close();
                reader = null;
                                
                DebugWrite("OCF18 about to send");
                DebugWrite("Login: " + mHCAILogin);
                DebugWrite("Pass: " + mHCAIPassword);
                IDataItem r = pmsTK.submit(mHCAILogin, mHCAIPassword, msg);
                DebugWrite("Sent!");
                DateTime dt = DateTime.Now;
                if (r == null)
                {
                    ErrorMessage e = new ErrorMessage();
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");
                    e.MessageType = mType.Error;
                    //e.Error_ErrorType = "INFORMATION ERROR";
                    e.Error_PMS_Document_Key = ocfId;
                    e.Error_OCFType = "OCF18";
                    e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);
                    //e.Error_Applicant_FirstName = d.Applicant_Name_FirstName;
                    //e.Error_Applicant_LastName = d.Applicant_Name_LastName;
                    PopulateErrorMessages(e);
                    GetHCAIErrorMsg(e);
                    DebugWrite("Parsing Error.");
                    ParseError(e);
                    //SET DOCUMENT TO INFORMATION ERROR
                    ReceiveHCAIError(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType, e.Error_CompleteErrorDesc, 2, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                    //map change 
                    Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(ocfId);
                    Succ.OCFType = "OCF18";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;
                }
                else
                {
                    var resKeys = new SubmitResponseKeys();
                    var myRes = new SubmitResponse
                    {
                        MessageDate = dt.ToString("MM/dd/yyyy"),
                        MessageTime = dt.ToString("hh:mm:ss tt"),
                        MessageType = mType.SubmitResponse,
                        HCAI_Document_Number = r.getValue(resKeys.HCAI_Document_Number),
                        PMS_Document_Key = r.getValue(resKeys.PMSFields_PMSDocumentKey),
                        PMS_Patient_Key = r.getValue(resKeys.PMSFields_PMSPatientKey),
                        MessageOCFType = "OCF18"
                    };

                    //SET DOCUMENT TO SUCCESSFULL DELIVERY
                    ReceiveHCAISuccessfullyDelivered(mCommand, ConvertStrToLong(myRes.PMS_Document_Key), "OCF18", dt, myRes.MessageTime, myRes.HCAI_Document_Number);
                    //MAP CHANGE
                    Succ.HCAI_Document_Number = myRes.HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(myRes.PMS_Document_Key);
                    Succ.OCFType = "OCF18";
                    Succ.New_HCAI_Status = HcaiStatus.SuccessfullyDelivered;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("OCF18 submit error: " + ex.Message, ex);
                DebugWrite("OCF18 Exception");
                DateTime dt = DateTime.Now;
                var e = new ErrorMessage
                {
                    MessageDate = dt.ToString("MM/dd/yyyy"),
                    MessageTime = dt.ToString("hh:mm:ss tt"),
                    MessageType = mType.Error,
                    Error_PMS_Document_Key = ocfId,
                    Error_OCFType = "OCF18",
                    Error_HCAI_ErrorType = "903:?:Exception occurred!",
                    Error_CompleteErrorDesc =
                        "HCAI System Error.\n This document could not submit this document to HCAI.\n" +
                        "- Verify your connection to the internet\n- Make sure your PMS username and password are correct\n- HCAI server may be busy at this time\n" +
                        "Try to to submit this document again in a few minutes."
                };
                //SET DOCUMENT TO DELIVERY/MACHINE ERROR
                e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType);
                ReceiveHCAIError(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType, e.Error_CompleteErrorDesc, 1, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                //map change 
                Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                Succ.OCFID = ConvertStrToLong(ocfId);
                Succ.OCFType = "OCF18";
                Succ.New_HCAI_Status = HcaiStatus.DeliveryFailed;
            }

            return Succ;
        }


        public OCFMapReturnType submitOCF21C(string OCFID)
        {
            DebugWrite("Submitting OCF21C document OCFID: " + OCFID);

            var version = "6";
            var succ = new OCFMapReturnType();

            //connect to DB
            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                succ.OCFID = ConvertStrToLong(OCFID);
                succ.OCFType = "OCF21C";
                succ.New_HCAI_Status = HcaiStatus.InformationError;
                return succ;
            }

            var tDBConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            using (tDBConnection)

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            using (var tCommand = tDBConnection.CreateCommand())
                try
            {
                if (!IsCredentialsValid() || !IsValidForHCAISubmission(mCommand, OCFID, "OCF21C"))
                {
                    if (!IsCredentialsValid())
                        DebugWrite("Hcai credentials are empty.");
                    DebugWrite("submitOCF21C aborted.");
                    succ.OCFID = ConvertStrToLong(OCFID);
                    succ.OCFType = "OCF21C";
                    succ.New_HCAI_Status = HcaiStatus.InformationError;

                    return succ;
                }

                IDataItem msg = pmsTK.createDoc(DocType.OCF21C);
                var mOcf21CKeys = new PMSToolkit.DataObjects.OCF21CKeys();
                bool res;
                //string[] toSplit;

                //Software IDs
                res = msg.addValue(mOcf21CKeys.PMSFields_PMSSoftware, "Antibex Software");
                res = msg.addValue(mOcf21CKeys.PMSFields_PMSVersion, "4.2.7");
                res = msg.addValue(mOcf21CKeys.OCFVersion, version);
                DebugWrite("OCF21C -7");
                res = msg.addValue(mOcf21CKeys.PMSFields_PMSDocumentKey, OCFID);
                DebugWrite("OCF21C -6");
                res = msg.addValue(mOcf21CKeys.PMSFields_PMSPatientKey, GetClientIDByOCFID(mCommand, "OCF21C", OCFID));
                DebugWrite("OCF21C -5");

                //get general info
                SqlDataReader reader = ExecQuery(mCommand, GetOCFSqlQuery("OCF21C", OCFID));

                if (reader.Read() == true)
                {
                    res = msg.addValue(mOcf21CKeys.AttachmentsBeingSent, Flag(MyReaderFldToString(reader, "chkAttachments")));

                    DebugWrite("OCF21C -3");
                    //Header
                    res = msg.addValue(mOcf21CKeys.Header_ClaimNumber, MyReaderFldToString(reader, "CIClaimNo"));
                    res = msg.addValue(mOcf21CKeys.Header_PolicyNumber, MyReaderFldToString(reader, "CIPolicyNo"));
                    res = msg.addValue(mOcf21CKeys.Header_DateOfAccident, MyReaderFldToDateString(reader, "CIDOL")); //make sure dates are in proper format
                    DebugWrite("OCF21C -2");
                    //Applicant Info
                    res = msg.addValue(mOcf21CKeys.Applicant_Name_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    res = msg.addValue(mOcf21CKeys.Applicant_Name_MiddleName, MyReaderFldToString(reader, "CIMiddleName"));
                    res = msg.addValue(mOcf21CKeys.Applicant_Name_LastName, MyReaderFldToString(reader, "CILastName"));
                    res = msg.addValue(mOcf21CKeys.Applicant_DateOfBirth, MyReaderFldToDateString(reader, "CIDOB"));  //date
                    res = msg.addValue(mOcf21CKeys.Applicant_Gender, Gender(MyReaderFldToString(reader, "CIGender")));

                    res = msg.addValue(mOcf21CKeys.Applicant_TelephoneNumber, Phone(MyReaderFldToString(reader, "CITel")));
                    res = msg.addValue(mOcf21CKeys.Applicant_TelephoneExtension, "");
                    res = msg.addValue(mOcf21CKeys.Applicant_Address_StreetAddress1, MyReaderFldToString(reader, "CIAddress"));
                    res = msg.addValue(mOcf21CKeys.Applicant_Address_StreetAddress2, "");
                    res = msg.addValue(mOcf21CKeys.Applicant_Address_City, MyReaderFldToString(reader, "CICity"));
                    res = msg.addValue(mOcf21CKeys.Applicant_Address_Province, FormatProvince(MyReaderFldToString(reader, "CIProvince")));
                    res = msg.addValue(mOcf21CKeys.Applicant_Address_PostalCode, FormatPostalCode(MyReaderFldToString(reader, "CIPostalCode")));
                    DebugWrite("OCF21C -1");

                    //Insurer Info
                    res = msg.addValue(mOcf21CKeys.Insurer_IBCInsurerID, MyReaderFldToString(reader, "MVAInsurerHCAIID"));
                    res = msg.addValue(mOcf21CKeys.Insurer_IBCBranchID, MyReaderFldToString(reader, "MVABranchHCAIID"));
                    //res = msg.addValue(m_ocf21cKeys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(d.Insurer_PolicyHolder_IsSameAsApplicant)); //WHY DUPLICATE
                    res = msg.addValue(mOcf21CKeys.Insurer_Adjuster_Name_FirstName, MyReaderFldToString(reader, "IIAdjFirstName"));
                    res = msg.addValue(mOcf21CKeys.Insurer_Adjuster_Name_LastName, MyReaderFldToString(reader, "IIAdjLastName"));
                    res = msg.addValue(mOcf21CKeys.Insurer_Adjuster_TelephoneNumber, Phone(MyReaderFldToString(reader, "IIAdjTel")));
                    res = msg.addValue(mOcf21CKeys.Insurer_Adjuster_TelephoneExtension, MyReaderFldToString(reader, "IIAdjExt"));
                    res = msg.addValue(mOcf21CKeys.Insurer_Adjuster_FaxNumber, Phone(MyReaderFldToString(reader, "IIAdjFax")));
                    res = msg.addValue(mOcf21CKeys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(MyReaderFldToString(reader, "IIPHolderSame")));
                    if (Flag(MyReaderFldToString(reader, "IIPHolderSame")) != "Yes")
                    {
                        res = msg.addValue(mOcf21CKeys.Insurer_PolicyHolder_Name_LastName, MyReaderFldToString(reader, "IIPHolderLastName"));
                        res = msg.addValue(mOcf21CKeys.Insurer_PolicyHolder_Name_FirstName, MyReaderFldToString(reader, "IIPHolderFirstName"));
                    }

                    //added---
                    res = msg.addValue(mOcf21CKeys.OCF21C_HCAI_Plan_Document_Number, ""); ///TO BE UPDATED JULY 1, 2012

                    //added---
                    //Previously Approved

                    string hPlanNo = "";

                    hPlanNo = MyReaderFldToString(reader, "RefOfHCAI_PlanNumber");
                    if (hPlanNo.Trim() == "")
                    {
                        hPlanNo = MyReaderFldToString(reader, "pHCAIDocNumber");
                    }

                    if (MyReaderFldToString(reader, "PlanType") != null && MyReaderFldToString(reader, "PlanType") != "" && MyReaderFldToString(reader, "PlanDate") != null && MyReaderFldToString(reader, "PlanDate") != "")
                    {
                        if (MyReaderFldToString(reader, "CIDOL") != "")
                        {
                            //TODO - Aug2016
                            //if (Sept2010 > (DateTime)reader["CIDOL"])
                            //{
                            //    res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "OCF23WAD");
                            //}
                            //else
                            //{
                            //    res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "OCF23MIG");
                            //}
                            res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "OCF23MIG");
                        }
                        else
                        {
                            //TODO - Aug2016
                            //if (Sept2010 > (DateTime)reader["PlanDate"])
                            //{
                            //    res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "OCF23WAD");
                            //}
                            //else
                            //{
                            //    res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "OCF23MIG");
                            //}
                            res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "OCF23MIG");
                        }

                        
                        res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber, hPlanNo);
                        res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, "");
                    }
                    else
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber, hPlanNo.Trim() != "" ? hPlanNo : "exempt");
                    }
                    //Invoice Info
                    res = msg.addValue(mOcf21CKeys.OCF21C_InvoiceInformation_InvoiceNumber, OCFID);
                    res = msg.addValue(mOcf21CKeys.OCF21C_InvoiceInformation_FirstInvoice, Flag(MyReaderFldToString(reader, "FirstInvoice")));
                    res = msg.addValue(mOcf21CKeys.OCF21C_InvoiceInformation_LastInvoice, Flag(MyReaderFldToString(reader, "LastInvoice")));

                    //Payee
                    res = msg.addValue(mOcf21CKeys.OCF21C_Payee_FacilityRegistryID, mHCAIID.ToString());
                    res = msg.addValue(mOcf21CKeys.OCF21C_Payee_MakeChequePayableTo, MyReaderFldToString(reader, "FIPayTo"));
                    res = msg.addValue(mOcf21CKeys.OCF21C_Payee_FacilityIsPayee, Flag(MyReaderFldToString(reader, "FacilityIsPayee")));  //todo - Ver3.13

                    DebugWrite("OCF21C 0");
                    //Injury Codes
                    OCF21CInjuryLineItemKeys m_injKeys = new OCF21CInjuryLineItemKeys();
                    SqlDataReader inj_reader = ExecQuery(tCommand, "select * from InvoiceInjuries where InvoiceID = " + OCFID + " order by iOrder");

                    while (inj_reader.Read())
                    {
                        var inj = msg.newLineItem(LineItemType.InjuryLineItem);
                        inj.addValue(m_injKeys.OCF21C_InjuriesAndSequelae_Injury_Code, MyReaderFldToString(inj_reader, "Code"));
                        res = msg.addLineItem(inj);
                    }
                    inj_reader.Close();
                    inj_reader = null;

                    DebugWrite("OCF21C 1");
                    //Rendered Goods and Services
                    var rendKeys = new RenderedGSLineItemKeys();
                    IDataItem rendLI;
                    PAFReimbursableLineItemKeys pafKeys = new PAFReimbursableLineItemKeys();
                    IDataItem pafELI;
                    OtherReimbursableLineItemKeys otherKeys = new OtherReimbursableLineItemKeys();
                    IDataItem otherELI;

                    SqlDataReader items_reader = ExecQuery(tCommand, "select * from InvoiceItemDetails where InvoiceID = " + OCFID + " order by PMSGSKey, ItemDate, ItemOrder");
                    double tQty = 0;
                    double pSubTotal = 0;
                    double rSubTotal = 0;

                    int pCounter;
                    int[] pafItemProviders = new int[3];

                    while (items_reader.Read())
                    {
                        if (MyReaderFldToString(items_reader, "ItemVCForShow") == "1")
                        {
                            //PAGE 2 RENDERED GOODS AND SERVICE
                            rendLI = msg.newLineItem(PMSToolkit.LineItemType.GS21CRenderedGSLineItem);
                            //deleted---res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ReferenceNumber, myRendLI.OCF21C_RenderedGoodsAndServices_Items_Item_ReferenceNumber);
                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey, MyReaderFldToString(items_reader, "PMSGSKey"));
                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", ""));
                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute, MyReaderFldToString(items_reader, "ItemAttribute"));

                            // if (Config.June2015 <= DateTime.Now)
                            // {
                            if (MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "GXX99" || MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "AXXOT")
                            {
                                res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Description, MyReaderFldToString(items_reader, "ItemDescription"));   //todo - Ver3.13
                            }
                            // }




                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "Type"));
                            //DebugWrite("Rendered" + myRendLI.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID);
                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "ProviderHCAINo"));
                            /////
                            tQty = (double)(ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemQty")) * ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemBaseUnit")));
                            if (MyReaderFldToString(items_reader, "ItemMeasure") != "")
                            {
                                if (MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hr" || MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hour")
                                    res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                                else
                                    res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            }
                            else
                            {
                                res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            }

                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Measure, MyReaderFldToString(items_reader, "ItemMeasure"));
                            res = rendLI.addValue(rendKeys.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService, MyReaderFldToDateString(items_reader, "ItemDate"));
                            msg.addLineItem(rendLI);
                        }
                        else if (MyReaderFldToString(items_reader, "ItemVCForShow") == "2")
                        {
                            //PAGE 3 PAF REIMBERSABLE FEES
                            pafELI = msg.newLineItem(LineItemType.GS21CPAFReimbursableLineItem);

                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey, MyReaderFldToString(items_reader, "PMSGSKey"));
                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", ""));
                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_Attribute, ""); //depricated field, must be set to empty string
                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount")).ToString("0.00"));


                            Debugger.WriteToFile("OCF21C submitting document using version 4 (post Nov 1, 2012) format", 10);
                            pafItemProviders[0] = ConvertStrToInt(MyReaderFldToString(items_reader, "WhichItemPRPrimary"));
                            if (pafItemProviders[0] == 0)
                            {
                                pafItemProviders[0] = 1;
                            }
                            for (pCounter = 1; pCounter < 4; pCounter++)
                            {
                                if (pCounter != pafItemProviders[0])
                                {
                                    pafItemProviders[pCounter - 1] = pCounter;
                                }
                            }

                            //These need to be filled with DB field names
                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService, MyReaderFldToDateString(items_reader, "FirstDateOfService"));
                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[0]), 1) + "ProviderHCAINo"));
                            res = pafELI.addValue(pafKeys.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[0]), 1) + "Type"));

                            var secProvKeys = new GS21CPAFSecondaryProviderProfessionLineItemKeys();

                            //secondary provider 1
                            if (MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[1]) != "")
                            {
                                var secProvider1 = pafELI.newLineItem(LineItemType.PAFSecondaryProviderProfessionLineItem);
                                secProvider1.addValue(secProvKeys.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[1]), 1) + "ProviderHCAINo"));
                                secProvider1.addValue(secProvKeys.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[1]), 1) + "Type"));
                                pafELI.addLineItem(secProvider1);
                            }
                            //secondary provider 2
                            if (MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[2]) != "")
                            {
                                var secProvider2 = pafELI.newLineItem(LineItemType.PAFSecondaryProviderProfessionLineItem);
                                secProvider2.addValue(secProvKeys.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[2]), 1) + "ProviderHCAINo"));
                                secProvider2.addValue(secProvKeys.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR" + pafItemProviders[2]), 1) + "Type"));
                                pafELI.addLineItem(secProvider2);
                            }


                            pSubTotal = pSubTotal + ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount"));
                            msg.addLineItem(pafELI);
                        }
                        else //if (MyReaderFldToString(items_reader, "ItemVCForShow") == 0)
                        {
                            //PAGE 3 OTHER REIMBURSABLE GOODS AND SERVICES
                            otherELI = msg.newLineItem(PMSToolkit.LineItemType.GS21COtherReimbursableLineItem);
                            //deleted---res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ReferenceNumber, myOtherELI.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ReferenceNumber);
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey, MyReaderFldToString(items_reader, "PMSGSKey"));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", ""));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute, MyReaderFldToString(items_reader, "ItemAttribute"));

                            if (MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "GXX99" || MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "AXXOT")
                            {
                                res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description, MyReaderFldToString(items_reader, "ItemDescription"));   //todo - Ver3.13
                            }

                            //DebugWrite(myOtherELI.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID);
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "ProviderHCAINo"));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "Type"));

                            tQty = (double)(ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemQty")) * ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemBaseUnit")));

                            if (MyReaderFldToString(items_reader, "ItemMeasure") != "")
                            {
                                if (MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hr" || MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hour")
                                    otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                                else
                                    otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            }
                            else
                            {
                                otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            }
                            //res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity, MyReaderFldToString(items_reader, "FIDaclareText"));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure, MyReaderFldToString(items_reader, "ItemMeasure"));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService, MyReaderFldToDateString(items_reader, "ItemDate"));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST, FlagTax(MyReaderFldToString(items_reader, "GST")));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST, FlagTax(MyReaderFldToString(items_reader, "PST")));
                            res = otherELI.addValue(otherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount")).ToString("0.00"));

                            rSubTotal = rSubTotal + ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount"));

                            msg.addLineItem(otherELI);
                        }

                    }
                    items_reader.Close();
                    items_reader = null;


                    double oTotal = 0;
                    double oTotal1 = 0;
                    double oTotal2 = 0;

                    double oTotalDebits = 0;
                    double oTotal1Debits = 0;
                    double oTotal2Debits = 0;
                    DebugWrite("OCF21C 6");

                    //TODO Nov2014 check logic -- DONE
                    var isAmountRefused = false;
                    //if (DateTime.Now >= Config.Nov2014)
                    //{
                    if (Flag(MyReaderFldToString(reader, "IsAmountRefused")) == "Yes")
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_IsAmountRefused, "Yes");
                        isAmountRefused = true;
                    }
                    else
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_IsAmountRefused, "No");
                        isAmountRefused = false;
                    }
                    //}

                    //TODO Nov2014 check logic
                    //if (DateTime.Now >= Config.Nov2014)
                    //{
                    if (isAmountRefused == true)
                    {
                        if (IsNumeric(MyReaderFldToString(reader, "OHIPChiro")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro"))).ToString());
                            oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "OHIPPhysio")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio"))).ToString());
                            oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "OHIPMassage")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage"))).ToString());
                            oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "OHIPX")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_OtherService,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX"))).ToString());
                            oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX"));
                        }
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Proposed, oTotal.ToString());
                    }
                    //Other Insurance Amounts

                    if (IsNumeric(MyReaderFldToString(reader, "OHIPChiro_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Chiropractic,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro_Debit"))).ToString());
                        oTotalDebits += ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro_Debit"));
                    }
                    if (IsNumeric(MyReaderFldToString(reader, "OHIPPhysio_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Physiotherapy,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio_Debit"))).ToString());
                        oTotalDebits += ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio_Debit"));
                    }
                    if (IsNumeric(MyReaderFldToString(reader, "OHIPMassage_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_MassageTherapy,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage_Debit"))).ToString());
                        oTotalDebits += ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage_Debit"));

                    }
                    if (IsNumeric(MyReaderFldToString(reader, "OHIPX_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_OtherService,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX_Debit"))).ToString());
                        oTotalDebits += ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX_Debit"));
                    }

                    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Proposed, oTotalDebits.ToString());

                    //if (DateTime.Now > Config.Nov2014)
                    //{
                    if (isAmountRefused == true)
                    {
                        if (IsNumeric(MyReaderFldToString(reader, "EHC1Chiro")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"))).
                                                   ToString());
                            oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "EHC1Physio")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"))).
                                                   ToString());
                            oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "EHC1Massage")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"))).
                                                   ToString());
                            oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "EHC1X")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"))).ToString());
                            oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"));
                        }

                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed, oTotal1.ToString());

                    }
                    //NOV 2014 - EHC debits fields
                    if (IsNumeric(MyReaderFldToString(reader, "EHC1Chiro_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Chiropractic,
                                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro_Debit"))).ToString());
                        oTotal1Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro_Debit"));
                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC1Physio_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio_Debit"))).ToString());
                        oTotal1Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio_Debit"));

                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC1Massage_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy,
                                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage_Debit"))).ToString());
                        oTotal1Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage_Debit"));
                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC1X_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_OtherService,
                                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X_Debit"))).ToString());
                        oTotal1Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X_Debit"));
                    }

                    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed, oTotal1Debits.ToString());
                    //}
                    //else
                    //{
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC1Chiro")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic,
                    //                           (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"))).ToString());
                    //        oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"));
                    //    }
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC1Physio")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy,
                    //                           (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"))).ToString());
                    //        oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"));
                    //    }
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC1Massage")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy,
                    //                           (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"))).
                    //                               ToString());
                    //        oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"));
                    //    }
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC1X")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService,
                    //                           (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"))).ToString());
                    //        oTotal1 = oTotal1 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"));
                    //    }
                    //    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed, oTotal1.ToString());
                    //}



                    //if (DateTime.Now >= Config.Nov2014)
                    //{
                    if (isAmountRefused == true)
                    {
                        if (IsNumeric(MyReaderFldToString(reader, "EHC2Chiro")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"))).
                                                   ToString());
                            oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "EHC2Physio")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"))).
                                                   ToString());
                            oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "EHC2Massage")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"))).
                                                   ToString());
                            oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"));
                        }
                        if (IsNumeric(MyReaderFldToString(reader, "EHC2X")) == true)
                        {
                            res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService,
                                               (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"))).ToString());
                            oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"));
                        }
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed, oTotal2.ToString());

                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC2Chiro_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Chiropractic,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro_Debit"))).ToString());
                        oTotal2Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro_Debit"));
                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC2Physio_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio_Debit"))).ToString());
                        oTotal2Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio_Debit"));
                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC2Massage_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage_Debit"))).ToString());
                        oTotal2Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage_Debit"));

                    }
                    if (IsNumeric(MyReaderFldToString(reader, "EHC2X_Debit")))
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_OtherService,
                            (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X_Debit"))).ToString());
                        oTotal2Debits += ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X_Debit"));
                    }


                    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed, oTotal2Debits.ToString());


                    //}
                    //else
                    //{
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC2Chiro")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"))).ToString());
                    //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"));
                    //    }
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC2Physio")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"))).ToString());
                    //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"));
                    //    }
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC2Massage")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"))).ToString());
                    //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"));
                    //    }
                    //    if (IsNumeric(MyReaderFldToString(reader, "EHC2X")) == true)
                    //    {
                    //        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"))).ToString());
                    //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"));
                    //    }
                    //    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed, oTotal2.ToString());
                    //}


                    if (MyReaderFldToString(reader, "XService") != null && MyReaderFldToString(reader, "XService") != "")
                    {
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_OtherServiceType, MyReaderFldToString(reader, "XService"));
                        res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Debits_OtherServiceType, MyReaderFldToString(reader, "XService"));
                    }


                    //TODO Nov2014 add these fields to database  -- OK

                    //if (MyReaderFldToString(reader, "XService_Debit") != null && MyReaderFldToString(reader, "XService_Debit") != "")
                    //{
                    //    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInsuranceAmounts_Debits_OtherServiceType, MyReaderFldToString(reader, "XService_Debit"));
                    //}

                    //Account Activity
                    res = msg.addValue(mOcf21CKeys.OCF21C_AccountActivity_PriorBalance, ConvertStrToDouble(MyReaderFldToString(reader, "PriorBalance")).ToString("0.00"));
                    res = msg.addValue(mOcf21CKeys.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer, ConvertStrToDouble(MyReaderFldToString(reader, "PaymentReceived")).ToString("0.00"));
                    res = msg.addValue(mOcf21CKeys.OCF21C_AccountActivity_OverdueAmount, ConvertStrToDouble(MyReaderFldToString(reader, "PaymentReceived")).ToString("0.00"));


                    //if (DateTime.Now >= Config.Nov2014)
                    //{
                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Proposed, ConvertStrToDouble((((oTotal1 - oTotal1Debits) + (oTotal2 - oTotal2Debits))).ToString()).ToString("0.00"));
                    var total = ConvertStrToDouble(MyReaderFldToString(reader, "MVATotal")); // -((oTotal - oTotalDebits) + ((oTotal1 - oTotal1Debits) + (oTotal2 - oTotal2Debits)));
                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed, total.ToString());
                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_MOH_Proposed, (oTotal - oTotalDebits).ToString());
                    //}
                    //else
                    //{
                    //    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Proposed, (oTotal1 + oTotal2).ToString());
                    //    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "MVATotal")).ToString("0.00"));
                    //}

                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_GST_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "InvoiceGST")).ToString("0.00"));
                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_PST_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "InvoicePST")).ToString("0.00"));
                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_Interest_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "Interest")).ToString("0.00"));

                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed, rSubTotal.ToString("0.00"));
                    res = msg.addValue(mOcf21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed, pSubTotal.ToString("0.00"));

                    //Additional Info
                    res = msg.addValue(mOcf21CKeys.OCF21C_OtherInformation, MyReaderFldToString(reader, "Comments"));
                    res = msg.addValue(mOcf21CKeys.AdditionalComments, MyReaderFldToString(reader, "AdditionalComments"));

                    //Debugger.WriteToFile(Sept2010.ToString("MMMM/dd/yyyy"), true);

                    if (MyReaderFldToString(reader, "CIDOL") != "")
                    {
                        //TODO - Aug2016
                        //if (Sept2010 > (DateTime)reader["CIDOL"])
                        //{
                        //    //Debugger.WriteToFile("WAD1OR2 @1", true);
                        //    res = msg.addValue(mOcf21CKeys.OCF21C_PAFReimbursableFees_PAFType, "WAD1OR2");
                        //}
                        //else
                        //{
                        //    //Debugger.WriteToFile("MIG", true);
                        //    res = msg.addValue(mOcf21CKeys.OCF21C_PAFReimbursableFees_PAFType, "MIG");
                        //}
                        res = msg.addValue(mOcf21CKeys.OCF21C_PAFReimbursableFees_PAFType, "MIG");
                    }
                    else
                    {
                        // Debugger.WriteToFile("WAD1OR2 @2", true);
                        res = msg.addValue(mOcf21CKeys.OCF21C_PAFReimbursableFees_PAFType, "WAD1OR2"); //WHY? WHAT IS THIS?
                    }
                    DebugWrite("OCF21C 7");
                }
                else
                {
                    //WRITE ERROR LOG
                }

                reader.Close();
                reader = null;

                IDataItem r = pmsTK.submit(mHCAILogin, mHCAIPassword, msg);
                DebugWrite("OCF21C 8");

                DateTime dt = DateTime.Now;
                if (r == null)
                {
                    ErrorMessage e = new ErrorMessage();
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");
                    e.MessageType = mType.Error;
                    e.Error_OCFType = "OCF21C";
                    e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);

                    PopulateErrorMessages(e);

                    GetHCAIErrorMsg(e);

                    ParseError(e);
                    //return e;
                    //SET DOCUMENT TO INFORMATION ERROR
                    ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 2, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                    //map change 
                    //Succ = false;

                    succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                    succ.OCFID = ConvertStrToLong(OCFID);
                    succ.OCFType = "OCF21C";
                    succ.New_HCAI_Status = HcaiStatus.InformationError;

                }
                else
                {
                    PMSToolkit.DataObjects.SubmitResponseKeys resKeys = new PMSToolkit.DataObjects.SubmitResponseKeys();
                    SubmitResponse myRes = new SubmitResponse();
                    myRes.MessageDate = dt.ToString("MM/dd/yyyy");
                    myRes.MessageTime = dt.ToString("hh:mm:ss tt");
                    myRes.MessageType = mType.SubmitResponse;
                    myRes.HCAI_Document_Number = r.getValue(resKeys.HCAI_Document_Number);
                    myRes.PMS_Document_Key = r.getValue(resKeys.PMSFields_PMSDocumentKey);
                    myRes.PMS_Patient_Key = r.getValue(resKeys.PMSFields_PMSPatientKey);
                    myRes.MessageOCFType = "OCF21C";
                    //SET DOCUMENT TO SUCCESSFULL DELIVERY
                    ReceiveHCAISuccessfullyDelivered(mCommand, ConvertStrToLong(myRes.PMS_Document_Key), myRes.MessageOCFType, dt, myRes.MessageTime, myRes.HCAI_Document_Number);
                    //MAP CHANGE
                    succ.HCAI_Document_Number = myRes.HCAI_Document_Number;
                    succ.OCFID = ConvertStrToLong(myRes.PMS_Document_Key);
                    succ.OCFType = "OCF21C";
                    succ.New_HCAI_Status = HcaiStatus.SuccessfullyDelivered;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("OCF21C submit error: " + ex.Message, ex);
                DateTime dt = DateTime.Now;
                ErrorMessage e = new ErrorMessage();
                e.MessageDate = dt.ToString("MM/dd/yyyy");
                e.MessageTime = dt.ToString("hh:mm:ss tt");
                e.MessageType = mType.Error;
                e.Error_OCFType = "OCF21C";
                e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);
                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";
                e.Error_CompleteErrorDesc = "HCAI System Error.\n This document could not submit this document to HCAI.\n" +
                "- Verify your connection to the internet\n- Make sure your PMS username and password are correct\n- HCAI server may be busy at this time\n" +
                "Try to to submit this document again in a few minutes.";
                ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 1, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                //map change 
                succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                succ.OCFID = ConvertStrToLong(OCFID);
                succ.OCFType = "OCF21C";
                succ.New_HCAI_Status = HcaiStatus.DeliveryFailed;
            }

            return succ;
        }

        public OCFMapReturnType submitOCF23(string OCFID)
        {
            DebugWrite("Submitting OCF23 document OCFID: " + OCFID);
            var Succ = new OCFMapReturnType();

            string[] toSplit;
            IDataItem msg = pmsTK.createDoc(DocType.OCF23);
            var m_ocf23Keys = new PMSToolkit.DataObjects.OCF23Keys();
            bool res;

            //connect to DB
            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF23";
                Succ.New_HCAI_Status = HcaiStatus.InformationError;
                return Succ;
            }

            var tDBConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            using (tDBConnection)

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            using (var tCommand = tDBConnection.CreateCommand())
                try
            {
                if (!IsCredentialsValid() || IsValidForHCAISubmission(mCommand, OCFID, "OCF23") == false)
                {
                    if (!IsCredentialsValid())
                        DebugWrite("Hcai credentials are empty.");
                    DebugWrite("submitOCF23 aborted.");
                    Succ.OCFID = ConvertStrToLong(OCFID);
                    Succ.OCFType = "OCF23";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;
                    return Succ;
                }

                //Software IDs
                res = msg.addValue(m_ocf23Keys.PMSFields_PMSSoftware, "Antibex Software");
                res = msg.addValue(m_ocf23Keys.PMSFields_PMSVersion, "4.2.7");
                var version = "4";
                res = msg.addValue(m_ocf23Keys.OCFVersion, version);
                res = msg.addValue(m_ocf23Keys.PMSFields_PMSDocumentKey, OCFID.ToString());
                res = msg.addValue(m_ocf23Keys.PMSFields_PMSPatientKey, GetClientIDByOCFID(mCommand, "OCF23", OCFID));

                //get general info
                SqlDataReader reader = ExecQuery(mCommand, GetOCFSqlQuery("OCF23", OCFID));
                if (reader.Read() == true)
                {
                    res = msg.addValue(m_ocf23Keys.AttachmentsBeingSent, Flag(MyReaderFldToString(reader, "chkAttachments")));

                    //Header
                    res = msg.addValue(m_ocf23Keys.Header_ClaimNumber, MyReaderFldToString(reader, "CIClaimNo"));
                    res = msg.addValue(m_ocf23Keys.Header_PolicyNumber, MyReaderFldToString(reader, "CIPolicyNo"));
                    res = msg.addValue(m_ocf23Keys.Header_DateOfAccident, MyReaderFldToDateString(reader, "CIDOA")); //make sure dates are in proper format

                    //Applicant Info
                    res = msg.addValue(m_ocf23Keys.Applicant_Name_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    res = msg.addValue(m_ocf23Keys.Applicant_Name_MiddleName, MyReaderFldToString(reader, "CIMiddleName"));
                    res = msg.addValue(m_ocf23Keys.Applicant_Name_LastName, MyReaderFldToString(reader, "CILastName"));
                    res = msg.addValue(m_ocf23Keys.Applicant_DateOfBirth, MyReaderFldToDateString(reader, "CIDOB"));  //date
                    res = msg.addValue(m_ocf23Keys.Applicant_Gender, Gender(MyReaderFldToString(reader, "CIGender")));

                    res = msg.addValue(m_ocf23Keys.Applicant_TelephoneNumber, Phone(MyReaderFldToString(reader, "CITel")));
                    res = msg.addValue(m_ocf23Keys.Applicant_TelephoneExtension, "");
                    res = msg.addValue(m_ocf23Keys.Applicant_Address_StreetAddress1, MyReaderFldToString(reader, "CIAddress"));
                    res = msg.addValue(m_ocf23Keys.Applicant_Address_StreetAddress2, "");
                    res = msg.addValue(m_ocf23Keys.Applicant_Address_City, MyReaderFldToString(reader, "CICity"));
                    res = msg.addValue(m_ocf23Keys.Applicant_Address_Province, FormatProvince(MyReaderFldToString(reader, "CIProvince")));
                    res = msg.addValue(m_ocf23Keys.Applicant_Address_PostalCode, FormatPostalCode(MyReaderFldToString(reader, "CIPostalCode")));
                    DebugWrite("OCF23_1");

                    //Insurer Info
                    res = msg.addValue(m_ocf23Keys.Insurer_IBCInsurerID, MyReaderFldToString(reader, "MVAInsurerHCAIID"));
                    res = msg.addValue(m_ocf23Keys.Insurer_IBCBranchID, MyReaderFldToString(reader, "MVABranchHCAIID"));
                    //res = msg.addValue(m_ocf23Keys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(d.Insurer_PolicyHolder_IsSameAsApplicant));
                    res = msg.addValue(m_ocf23Keys.Insurer_Adjuster_Name_FirstName, MyReaderFldToString(reader, "MVAAFirstName"));
                    res = msg.addValue(m_ocf23Keys.Insurer_Adjuster_Name_LastName, MyReaderFldToString(reader, "MVAALastName"));
                    res = msg.addValue(m_ocf23Keys.Insurer_Adjuster_TelephoneNumber, Phone(MyReaderFldToString(reader, "MVAATel")));
                    res = msg.addValue(m_ocf23Keys.Insurer_Adjuster_TelephoneExtension, MyReaderFldToString(reader, "MVAAExt"));
                    res = msg.addValue(m_ocf23Keys.Insurer_Adjuster_FaxNumber, Phone(MyReaderFldToString(reader, "MVAAFax")));
                    res = msg.addValue(m_ocf23Keys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(MyReaderFldToString(reader, "MVAPHSame")));
                    if (Flag(MyReaderFldToString(reader, "MVAPHSame")) != "Yes")
                    {
                        res = msg.addValue(m_ocf23Keys.Insurer_PolicyHolder_Name_LastName, MyReaderFldToString(reader, "MVAPHLastName"));
                        res = msg.addValue(m_ocf23Keys.Insurer_PolicyHolder_Name_FirstName, MyReaderFldToString(reader, "MVAPHFirstName"));
                    }
                    res = msg.addValue(m_ocf23Keys.OCF23_DirectPaymentAssignment_Response, Flag(MyReaderFldToString(reader, "chkPart8_DirectPaymentAssignmentResponse")));

                    //Other Insurance
                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage, Flag(MyReaderFldToString(reader, "qEHC")));

                    if (FlagNA(MyReaderFldToString(reader, "qEHC")) == Flag("Yes"))
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_MOHAvailable, FlagNA(MyReaderFldToString(reader, "qOHIP")));

                        if (MyReaderFldToString(reader, "EHC1IDCertNo") != null && MyReaderFldToString(reader, "EHC1IDCertNo") != "")
                        {
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID, MyReaderFldToString(reader, "EHC1IDCertNo"));
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name, MyReaderFldToString(reader, "EHC1InsName"));
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber, MyReaderFldToString(reader, "EHC1PolicyNo"));
                            if (MyReaderFldToString(reader, "EHC1PlanMember") == null || MyReaderFldToString(reader, "EHC1PlanMember") == "")
                            {
                                res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, "");
                                res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, "");
                            }
                            else
                            {
                                toSplit = MyReaderFldToString(reader, "EHC1PlanMember").Split(',');
                                if (toSplit.Length > 1)
                                {
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, toSplit[1].ToString());
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, toSplit[0].ToString());
                                }
                                else
                                {
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, "");
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, toSplit[0].ToString());
                                }
                            }
                        }
                        else
                        {
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, "N/A"); //WHY?
                        }

                        if (MyReaderFldToString(reader, "EHC2IDCertNo") != null && MyReaderFldToString(reader, "EHC2IDCertNo") != "")
                        {
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID, MyReaderFldToString(reader, "EHC2IDCertNo"));
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name, MyReaderFldToString(reader, "EHC2InsName"));
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber, MyReaderFldToString(reader, "EHC2PolicyNo"));
                            if (MyReaderFldToString(reader, "EHC2PlanMember") == null || MyReaderFldToString(reader, "EHC2PlanMember") == "")
                            {
                                res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, "");
                                res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, "");
                            }
                            else
                            {
                                toSplit = MyReaderFldToString(reader, "EHC2PlanMember").Split(',');
                                if (toSplit.Length > 1)
                                {
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, toSplit[1].ToString());
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, toSplit[0].ToString());
                                }
                                else
                                {
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, "");
                                    res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, toSplit[0].ToString());
                                }
                            }
                        }
                        else
                        {
                            res = msg.addValue(m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, "N/A");
                        }
                    }

                    DebugWrite("OCF23_2");
                    //Health Practitioner
                    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_FacilityRegistryID, MyReaderFldToString(reader, "SHPHCAIFacilityID"));
                    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_ProviderRegistryID, MyReaderFldToString(reader, "SHPHCAIRegNo"));
                    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_Occupation, GetHCAIProviderOccupation(tCommand, MyReaderFldToString(reader, "SHPOccupation")));
                    //if (Sept2010 > DateTime.Now)
                    //{
                    //    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists, ReturnConflictsOpt(MyReaderFldToString(reader, "SHPOptConflicts")));
                    //    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails, MyReaderFldToString(reader, "SHPDeclareText"));
                    //}

                    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_DateSigned, MyReaderFldToDateString(reader, "SHPSignedDate"));
                    if (MyReaderFldToString(reader, "chkNotFirstSHP") == "1")
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner, Flag("No"));
                    }
                    else
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner, Flag("Yes"));
                    }
                    res = msg.addValue(m_ocf23Keys.OCF23_HealthPractitioner_IsSignatureOnFile, Flag("Yes"));

                    //Injury Codes
                    PMSToolkit.DataObjects.OCF23InjuryLineItemKeys m_injKeys = new PMSToolkit.DataObjects.OCF23InjuryLineItemKeys();
                    //string injCode = d.InjuriesAndSequelae_Injury_Code;
                    IDataItem inj;
                    SqlDataReader inj_reader = ExecQuery(tCommand, "select * from OCFInjuries where ReportID = " + OCFID + " order by iOrder");

                    while (inj_reader.Read())
                    {
                        inj = msg.newLineItem(PMSToolkit.LineItemType.InjuryLineItem);
                        inj.addValue(m_injKeys.OCF23_InjuriesAndSequelae_Injury_Code, MyReaderFldToString(inj_reader, "Code"));
                        res = msg.addLineItem(inj);
                        //injCode = d.InjuriesAndSequelae_Injury_Code;
                    }
                    inj_reader.Close();
                    inj_reader = null;

                    //Prior and Concurrent Condition
                    res = msg.addValue(m_ocf23Keys.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident, Flag(MyReaderFldToString(reader, "optPart7A")));
                    res = msg.addValue(m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Response, FlagUNK(MyReaderFldToString(reader, "optPart7B")));
                    res = msg.addValue(m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation, MyReaderFldToString(reader, "Part7BText"));

                    if (MyReaderFldToString(reader, "optPart7B").ToLower() == "yes")
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response, FlagUNK(MyReaderFldToString(reader, "optPart7C")));
                        res = msg.addValue(m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation, MyReaderFldToString(reader, "Part7CText"));
                    }
                    //Barriers to Recovery
                    res = msg.addValue(m_ocf23Keys.OCF23_BarriersToRecovery_Response, Flag(MyReaderFldToString(reader, "optPart8A")));
                    res = msg.addValue(m_ocf23Keys.OCF23_BarriersToRecovery_Explanation, MyReaderFldToString(reader, "Part8AText"));
                    DebugWrite("OCF23_3");
                    res = msg.addValue(m_ocf23Keys.OCF23_OtherGoodsAndServices_GoodsAndServicesComments, MyReaderFldToString(reader, "Comments"));

                    //PAF Services
                    if (MyReaderFldToString(reader, "PAFItem1Description") != "")
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType, "MIG");
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_EstimatedFee, ConvertStrToDouble(MyReaderFldToString(reader, "PAFItem1EstimatedFee")).ToString("0.00"));
                    }
                    else
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType, "");
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_EstimatedFee, "0.00");
                    }
                    if (MyReaderFldToString(reader, "PAFItem2Description") != "")
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description, MyReaderFldToString(reader, "PAFItem2Description"));
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee, ConvertStrToDouble(MyReaderFldToString(reader, "PAFItem2EstimatedFee")).ToString("0.00"));
                        res = msg.addValue(m_ocf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey, "1");
                    }


                    //PAFLineItem - XRAY ITEMS
                    PMSToolkit.DataObjects.PAFLineItemKeys m_pafKeys = new PMSToolkit.DataObjects.PAFLineItemKeys();
                    //HCAIOCF23PAFLineItem pafLI = d.OCF23_PAFLineItem;                                
                    SqlDataReader xray_reader = ExecQuery(tCommand, "select * from OCFGoodsAndServices where ReportID = " + OCFID + " AND ItemVCForShow = 2 order by ItemOrder");
                    IDataItem paf;

                    //string t = "";

                    while (xray_reader.Read())
                    {
                        paf = msg.newLineItem(PMSToolkit.LineItemType.GS23PAFLineItem);
                        paf.addValue(m_pafKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey, MyReaderFldToString(xray_reader, "ItemOrder"));
                        paf.addValue(m_pafKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code, MyReaderFldToString(xray_reader, "ItemServiceCode"));

                        if (MyReaderFldToString(xray_reader, "ItemType") != "")
                        {
                            paf.addValue(m_pafKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute, (xray_reader["ItemType"].ToString().Substring((xray_reader["ItemType"].ToString().Length) - 4, 3).ToString()));
                        }
                        else
                        {
                            paf.addValue(m_pafKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute, "");
                        }
                        paf.addValue(m_pafKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee, ConvertStrToDouble(MyReaderFldToString(xray_reader, "ItemAmount")).ToString("0.00"));
                        res = msg.addLineItem(paf);
                    }
                    xray_reader.Close();
                    xray_reader = null;


                    DebugWrite("OCF23_4");
                    //OtherGSLineItem (Estimated)
                    PMSToolkit.DataObjects.OtherGSLineItemKeys m_otherKeys = new PMSToolkit.DataObjects.OtherGSLineItemKeys();
                    //HCAIOCF23OtherLineItemEstimated otherELI = d.OCF23_OtherLineItem_Estimated;
                    IDataItem eli;
                    double tQty = 0;
                    //HCAIOCF23OtherLineItemEstimated hcaiOCF23PAFOtherGS;

                    SqlDataReader items_reader = ExecQuery(tCommand, "select * from OCFGoodsAndServices where ReportID = " + OCFID + " AND (ItemVCForShow = 0 OR ItemVCForShow = 1)  order by ItemOrder");

                    while (items_reader.Read())
                    {
                        eli = msg.newLineItem(PMSToolkit.LineItemType.GS23OtherGSLineItem);
                        //deleted---eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber, otherELI.OCF23_OtherGoodsandServices_Items_Item_ReferenceNumber);
                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey, MyReaderFldToString(items_reader, "ItemOrder"));

                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", ""));

                        //todo - Ver3.13 - missed
                        if (MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "GXX99" || MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "AXXOT")
                        {
                            eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Description, MyReaderFldToString(items_reader, "ItemDescription"));
                        }

                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Attribute, MyReaderFldToString(items_reader, "ItemAttribute"));
                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "ProviderHCAINo"));
                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "Type"));

                        tQty = (double)(ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemQty")) * ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemBaseUnit")));
                        if (MyReaderFldToString(items_reader, "ItemMeasure") != "")
                        {
                            if (MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hr" || MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hour")
                                eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            else
                                eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                        }
                        else
                        {
                            eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                        }
                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Item_Measure, MyReaderFldToString(items_reader, "ItemMeasure"));
                        eli.addValue(m_otherKeys.OCF23_OtherGoodsAndServices_Items_Estimated_LineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount")).ToString("0.00"));

                        res = msg.addLineItem(eli);
                    }

                    //deleted//OtherGSLineItem (Approved)

                    //PMSToolkit.DataObjects.OtherGSLineItemKeys m_otherKeys2 = new PMSToolkit.DataObjects.OtherGSLineItemKeys();
                    //HCAIOCF23OtherLineItemApproved otherALI = d.OCF23_OtherLineItem_Approved;
                    //IDataItem ali;
                    //while(otherALI != null)
                    //{
                    //    ali = msg.newLineItem(PMSToolkit.LineItemType.GS23OtherGSLineItem);
                    //    ali.addValue(m_otherKeys2.OCF23_OtherGoodsAndServices_Items_Approved_LineCost, otherALI.OCF23_OtherGoodsandServices_Items_Approved_LineCost);
                    //    ali.addValue(m_otherKeys2.OCF23_OtherGoodsAndServices_Items_Approved_ReasonCodeGroup_ReasonCode, otherALI.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode);
                    //    ali.addValue(m_otherKeys2.OCF23_OtherGoodsAndServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc, otherALI.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    //    ali.addValue(m_otherKeys2.OCF23_OtherGoodsAndServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc, otherALI.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //    //ali.addValue(m_otherKeys.OCF23_OtherGoodsandServices_GoodAndServicesComments, otherALI.OCF23_OtherGoodsandServices_GoodAndServicesComments); //missing

                    //    res = msg.addLineItem(ali);

                    //    otherALI = d.OCF23_OtherLineItem_Approved;
                    //}

                    DebugWrite("OCF23_5");
                    //Insurer Totals


                    res = msg.addValue(m_ocf23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Proposed, MyReaderFldToString(reader, "Total"));
                    res = msg.addValue(m_ocf23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed, MyReaderFldToString(reader, "SubtotalSG"));
                    res = msg.addValue(m_ocf23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Proposed, MyReaderFldToString(reader, "SubtotalPAFE"));

                    //Applicant Signature    
                    if (Flag(MyReaderFldToString(reader, "chkWaivedByInsurer")) == "Yes")
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer, Flag("Yes"));
                    }
                    else
                    {
                        res = msg.addValue(m_ocf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer, Flag("No"));
                    }
                    res = msg.addValue(m_ocf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureOnFile, Flag(MyReaderFldToString(reader, "chkSignatureOnFile")));
                    res = msg.addValue(m_ocf23Keys.OCF23_ApplicantSignature_SigningApplicant_FirstName, MyReaderFldToString(reader, "CISignedBy"));
                    res = msg.addValue(m_ocf23Keys.OCF23_ApplicantSignature_SigningApplicant_LastName, MyReaderFldToString(reader, "CISignedByLastName"));
                    res = msg.addValue(m_ocf23Keys.OCF23_ApplicantSignature_SigningDate, MyReaderFldToDateString(reader, "CISignedDate"));
                    res = msg.addValue(m_ocf23Keys.AdditionalComments, MyReaderFldToString(reader, "OCF23AdditionalComments"));
                }
                else
                {
                    //OCF DOES NOT EXISTS???
                    DebugWrite("error geting ocf from db: reportid: " + OCFID.ToString());
                }

                reader.Close();
                reader = null;
                IDataItem r = pmsTK.submit(mHCAILogin, mHCAIPassword, msg);
                DateTime dt = DateTime.Now;

                if (r == null)
                {
                    var e = new ErrorMessage
                    {
                        MessageDate = dt.ToString("MM/dd/yyyy"),
                        MessageTime = dt.ToString("hh:mm:ss tt"),
                        MessageType = mType.Error,
                        Error_OCFType = "OCF23"
                    };
                    e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType);
                    PopulateErrorMessages(e);
                    GetHCAIErrorMsg(e);
                    ParseError(e);
                    //SET DOCUMENT TO INFORMATION ERROR
                    ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 2, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                    //map change 
                    Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(OCFID);
                    Succ.OCFType = "OCF23";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;
                }
                else
                {
                    var resKeys = new SubmitResponseKeys();
                    var myRes = new SubmitResponse
                    {
                        MessageDate = dt.ToString("MM/dd/yyyy"),
                        MessageTime = dt.ToString("hh:mm:ss tt"),
                        MessageType = mType.SubmitResponse,
                        HCAI_Document_Number = r.getValue(resKeys.HCAI_Document_Number),
                        PMS_Document_Key = r.getValue(resKeys.PMSFields_PMSDocumentKey),
                        PMS_Patient_Key = r.getValue(resKeys.PMSFields_PMSPatientKey),
                        MessageOCFType = "OCF23"
                    };

                    DebugWrite("OCF23_6");
                    //SET DOCUMENT TO SUCCESSFULL DELIVERY
                    ReceiveHCAISuccessfullyDelivered(mCommand, ConvertStrToLong(myRes.PMS_Document_Key), "OCF23", dt, myRes.MessageTime, myRes.HCAI_Document_Number);
                    //MAP CHANGE
                    Succ.HCAI_Document_Number = myRes.HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(myRes.PMS_Document_Key);
                    Succ.OCFType = "OCF23";
                    Succ.New_HCAI_Status = HcaiStatus.SuccessfullyDelivered;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("OCF23 submit error: " + ex.Message, ex);
                DebugWrite("OCF23 Exception");
                DateTime dt = DateTime.Now;
                var e = new ErrorMessage
                {
                    MessageDate = dt.ToString("MM/dd/yyyy"),
                    MessageTime = dt.ToString("hh:mm:ss tt"),
                    MessageType = mType.Error,
                    Error_OCFType = "OCF23"
                };
                e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);;
                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";
                e.Error_CompleteErrorDesc = "HCAI System Error.\n This document could not submit this document to HCAI.\n" +
                "- Verify your connection to the internet\n- Make sure your PMS username and password are correct\n- HCAI server may be busy at this time\n" +
                "Try to to submit this document again in a few minutes.";
                ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 1, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                //WRITE ERROR LOG
                Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF23";
                Succ.New_HCAI_Status = HcaiStatus.DeliveryFailed;
            }

            return Succ;
        }

        public OCFMapReturnType submitForm1(string ocfId)
        {
            DebugWrite("Submitting Form1 document OCFID: " + ocfId);
            var succ = new OCFMapReturnType();

            //connect to DB
            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. submitAacn aborted.");
                succ.OCFID = ConvertStrToLong(ocfId);
                succ.OCFType = "Form1";
                succ.New_HCAI_Status = HcaiStatus.InformationError;
                return succ;
            }

            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                succ.OCFID = ConvertStrToLong(ocfId);
                succ.OCFType = "Form1";
                succ.New_HCAI_Status = HcaiStatus.InformationError;
                return succ;
            }

            var tDBConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            using (tDBConnection)

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            using (var tCommand = tDBConnection.CreateCommand())
                try
            {
                var msg = pmsTK.createDoc(DocType.AACN);
                var aacnKeys = new AACNKeys();
                //Software IDs
                msg.addValue(aacnKeys.PMSFields_PMSSoftware, "Antibex Software");
                msg.addValue(aacnKeys.PMSFields_PMSVersion, "4.2.7");
                msg.addValue(aacnKeys.FormVersion, "1");
                msg.addValue(aacnKeys.PMSFields_PMSDocumentKey, ocfId);
                msg.addValue(aacnKeys.PMSFields_PMSPatientKey, GetClientIDByOCFID(mCommand, "Form1", ocfId));

                //get general info
                //TODO - needto create query for AACN form
                SqlDataReader reader = ExecQuery(mCommand, GetOCFSqlQuery("FORM1", ocfId));

                if (reader.Read() == true)
                {
                    
                    msg.addValue(aacnKeys.AdditionalComments, MyReaderFldToString(reader, "AdditionalComments"));

                    msg.addValue(aacnKeys.AttachmentsBeingSent,
                                 Flag(MyReaderFldToString(reader, "AttachmentsBeingSent")));
                    msg.addValue(aacnKeys.AttachmentCount, MyReaderFldToString(reader, "AttachmentCount"));

                    msg.addValue(aacnKeys.Header_ClaimNumber, MyReaderFldToString(reader, "CIClaimNo"));
                    msg.addValue(aacnKeys.Header_PolicyNumber, MyReaderFldToString(reader, "CIPolicyNo"));
                    msg.addValue(aacnKeys.Header_DateOfAccident, MyReaderFldToDateString(reader, "CIDOA"));

                    msg.addValue(aacnKeys.Applicant_Name_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    msg.addValue(aacnKeys.Applicant_Name_MiddleName, MyReaderFldToString(reader, "CIMiddleName"));
                    msg.addValue(aacnKeys.Applicant_Name_LastName, MyReaderFldToString(reader, "CILastName"));
                    msg.addValue(aacnKeys.Applicant_DateOfBirth, MyReaderFldToDateString(reader, "CIDOB"));
                    msg.addValue(aacnKeys.Applicant_Gender, Gender(MyReaderFldToString(reader, "CIGender")));
                    msg.addValue(aacnKeys.Applicant_Telephone, Phone(MyReaderFldToString(reader, "CITel")));
                    msg.addValue(aacnKeys.Applicant_Extension, "");
                    msg.addValue(aacnKeys.Applicant_Address_StreetAddress1, MyReaderFldToString(reader, "CIAddress"));
                    msg.addValue(aacnKeys.Applicant_Address_StreetAddress2, "");
                    msg.addValue(aacnKeys.Applicant_Address_City, MyReaderFldToString(reader, "CICity"));
                    msg.addValue(aacnKeys.Applicant_Address_Province,
                                 FormatProvince(MyReaderFldToString(reader, "CIProvince")));
                    msg.addValue(aacnKeys.Applicant_Address_PostalCode,
                                 FormatPostalCode(MyReaderFldToString(reader, "CIPostalCode")));

                    msg.addValue(aacnKeys.AssessmentDate, MyReaderFldToDateString(reader, "AIAssDate"));
                    msg.addValue(aacnKeys.FirstAssessment, FlagUNK(MyReaderFldToString(reader, "AIQuestion")));
                    msg.addValue(aacnKeys.LastAssessmentDate, MyReaderFldToDateString(reader, "AILastAssDate"));
                    msg.addValue(aacnKeys.CurrentMonthlyAllowance, MyReaderFldToString(reader, "AIMonthlyAllowance"));

                    
                    //msg.addValue(aacnKeys.InsurerExamination, "Yes"); //??? can't find this field on form???
                    msg.addValue(aacnKeys.InsurerExamination, Flag(MyReaderFldToString(reader, "optInsurerExamination")));

                    msg.addValue(aacnKeys.Assessor_FacilityRegistryID, MyReaderFldToString(reader, "SHPHCAIFacilityID"));
                    msg.addValue(aacnKeys.Assessor_ProviderRegistryID, MyReaderFldToString(reader, "SHPHCAIRegNo"));
                    msg.addValue(aacnKeys.Assessor_Occupation,
                                 GetHCAIProviderOccupation(tCommand, MyReaderFldToString(reader, "SHPOccupation")));
                    msg.addValue(aacnKeys.Assessor_Email, MyReaderFldToString(reader, "SHPEmail"));
                    msg.addValue(aacnKeys.Assessor_IsSignatureOnFile, Flag(MyReaderFldToString(reader, "Assessor_IsSignatureOnFile")));
                    msg.addValue(aacnKeys.Assessor_DateSigned, MyReaderFldToDateString(reader, "SRHPSignedDate"));

                    msg.addValue(aacnKeys.Insurer_IBCInsurerID, MyReaderFldToString(reader, "MVAInsurerHCAIID"));
                    msg.addValue(aacnKeys.Insurer_IBCBranchID, MyReaderFldToString(reader, "MVABranchHCAIID"));

                    string IsPHSame = Flag(MyReaderFldToString(reader, "MVAPHSame"));
                    msg.addValue(aacnKeys.Insurer_PolicyHolder_IsSameAsApplicant, IsPHSame);
                    if (IsPHSame == "No")
                    {
                        msg.addValue(aacnKeys.Insurer_PolicyHolder_Name_FirstName, MyReaderFldToString(reader, "MVAPHFirstName"));
                        msg.addValue(aacnKeys.Insurer_PolicyHolder_Name_LastName, MyReaderFldToString(reader, "MVAPHLastName"));
                    }

                    var servicesItemKeys = new AACNServicesLineItemKeys();
                    //TODO - replace #### with query that returns service items

                    SqlDataReader servicesItemsReader = ExecQuery(tCommand, "select * from FORM1_Parts123 where ReportID = " + ocfId + " order by gPartNo, gRowOrder");
                    
                    while (servicesItemsReader.Read())
                    {
                        var item = msg.newLineItem(LineItemType.AACNServicesLineItem);
                        item.addValue(servicesItemKeys.AttendantCareServices_Item_ACSItemID, MyReaderFldToString(servicesItemsReader, "ACSItemID"));
                        item.addValue(servicesItemKeys.AttendantCareServices_Item_PMSLineItemKey, MyReaderFldToString(servicesItemsReader, "gPartNo") + ";" + MyReaderFldToString(servicesItemsReader, "gRowOrder"));
                        item.addValue(servicesItemKeys.AttendantCareServices_Item_Assessed_Minutes, MyReaderFldToString(servicesItemsReader, "NumberOfMin"));
                        item.addValue(servicesItemKeys.AttendantCareServices_Item_Assessed_TimesPerWeek, MyReaderFldToString(servicesItemsReader, "TxWeeks"));
                        msg.addLineItem(item);
                    }
                    servicesItemsReader.Close();
                    servicesItemsReader = null;

                    var part4Keys = new AACNAssessedPartKeys();
                    var part4LineItem = msg.newLineItem(LineItemType.AACNAssessedPart);
                    part4LineItem.addValue(part4Keys.Costs_Assessed_Part_ACSItemID, "1");
                    part4LineItem.addValue(part4Keys.Costs_Assessed_Part_HourlyRate,
                                           MyReaderFldToString(reader, "Part1HourlyRate"));
                    msg.addLineItem(part4LineItem);
                    part4LineItem = msg.newLineItem(LineItemType.AACNAssessedPart);
                    part4LineItem.addValue(part4Keys.Costs_Assessed_Part_ACSItemID, "2");
                    part4LineItem.addValue(part4Keys.Costs_Assessed_Part_HourlyRate,
                                           MyReaderFldToString(reader, "Part2HourlyRate"));
                    msg.addLineItem(part4LineItem);
                    part4LineItem = msg.newLineItem(LineItemType.AACNAssessedPart);
                    part4LineItem.addValue(part4Keys.Costs_Assessed_Part_ACSItemID, "3");
                    part4LineItem.addValue(part4Keys.Costs_Assessed_Part_HourlyRate,
                                           MyReaderFldToString(reader, "Part3HourlyRate"));
                    msg.addLineItem(part4LineItem);

                    reader.Close();
                }
                reader = null;
                IDataItem r = pmsTK.submit(mHCAILogin, mHCAIPassword, msg);
                DateTime dt = DateTime.Now;

                if (r == null)
                {
                    var e = new ErrorMessage
                    {
                        MessageDate = dt.ToString("MM/dd/yyyy"),
                        MessageTime = dt.ToString("hh:mm:ss tt"),
                        MessageType = mType.Error,
                        Error_OCFType = "Form1"
                    };
                    e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);
                    PopulateErrorMessages(e);
                    GetHCAIErrorMsg(e);
                    ParseError(e);
                    //SET DOCUMENT TO INFORMATION ERROR
                    ReceiveHCAIError(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType, e.Error_CompleteErrorDesc, 2, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                    //map change 
                    succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                    succ.OCFID = ConvertStrToLong(ocfId);
                    succ.OCFType = "Form1";
                    succ.New_HCAI_Status = HcaiStatus.InformationError;
                }
                else
                {
                    var resKeys = new AACNSubmitResponseKeys();
                    var myRes = new SubmitResponse
                    {
                        MessageDate = dt.ToString("MM/dd/yyyy"),
                        MessageTime = dt.ToString("hh:mm:ss tt"),
                        MessageType = mType.SubmitResponse,
                        HCAI_Document_Number = r.getValue(resKeys.DocumentNumber),
                        PMS_Document_Key = r.getValue(resKeys.PMSFields_PMSDocumentKey),
                        PMS_Patient_Key = r.getValue(resKeys.PMSFields_PMSPatientKey),
                        MessageOCFType = "Form1"
                    };

                    DebugWrite("Form1_6");
                    //SET DOCUMENT TO SUCCESSFULL DELIVERY
                    ReceiveHCAISuccessfullyDelivered(mCommand, ConvertStrToLong(myRes.PMS_Document_Key), "Form1", dt, myRes.MessageTime, myRes.HCAI_Document_Number);
                    //MAP CHANGE
                    succ.HCAI_Document_Number = myRes.HCAI_Document_Number;
                    succ.OCFID = ConvertStrToLong(myRes.PMS_Document_Key);
                    succ.OCFType = "Form1";
                    succ.New_HCAI_Status = HcaiStatus.SuccessfullyDelivered;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("AACN submit error: " + ex.Message, ex);
                DebugWrite("AACN Exception");
                ErrorMessage e = new ErrorMessage();
                DateTime dt = DateTime.Now;
                e.MessageDate = dt.ToString("MM/dd/yyyy");
                e.MessageTime = dt.ToString("hh:mm:ss tt");
                e.MessageType = mType.Error;
                e.Error_OCFType = "Form1";
                e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);;
                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";
                e.Error_CompleteErrorDesc = "HCAI System Error.\n This document could not submit this document to HCAI.\n" +
                "- Verify your connection to the internet\n- Make sure your PMS username and password are correct\n- HCAI server may be busy at this time\n" +
                "Try to to submit this document again in a few minutes.";
                ReceiveHCAIError(mCommand, ConvertStrToLong(ocfId), e.Error_OCFType, e.Error_CompleteErrorDesc, 1, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                //WRITE ERROR LOG
                succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                succ.OCFID = ConvertStrToLong(ocfId);
                succ.OCFType = "Form1";
                succ.New_HCAI_Status = HcaiStatus.DeliveryFailed;
            }

            return succ;
        }

        private string GetOCF22NatureOfAssessment(SqlDataReader reader)
        {
            if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H1Check1")) == 1)
                return "AllOtherAssessmentsNotRequiringApproval";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H1Check2")) == 1)
                return "ApprovalNotRequiredNotMoreThanThree";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H1Check3")) == 1)
                return "ApprovalNotRequiredAfterNotification";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H2Check1")) == 1)
                return "ApprovalNotRequiredImmediateRisk";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H3NCheck1")) == 1)
                return "DisabilityCertCostLessThanLimit";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H3NCheck2")) == 1)
                return "DisabilityCertCostMoreThanLimit";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H4NCheck1")) == 1)
                return "PrepareForm1Guideline";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H5NCheck1")) == 1)
                return "CatastrophicHospitalized";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H5NCheck2")) == 1)
                return "CatastrophicNotHospitalized";
            else if (ConvertStrToInt(MyReaderFldToString(reader, "Part4H6NCheck1")) == 1)
                return "AllOtherAssessmentsTreatmentPlans";
            else
                return "";
        }


        public OCFMapReturnType submitOCF22(string OCFID)
        {
            DebugWrite("Submitting OCF22 document OCFID: " + OCFID);

            OCFMapReturnType Succ = new OCFMapReturnType();

            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. submitOCF22 aborted.");
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF22";
                Succ.New_HCAI_Status = HcaiStatus.InformationError;
                return Succ;
            }

            //bool Succ = false;
            IDataItem msg = pmsTK.createDoc(DocType.OCF22);
            var m_ocf22Keys = new PMSToolkit.DataObjects.OCF22Keys();
            bool res;

            //connect to DB
            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF22";
                Succ.New_HCAI_Status = HcaiStatus.InformationError;
                return Succ;
            }

            var tDBConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            using (tDBConnection)

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            using (var tCommand = tDBConnection.CreateCommand())
                try
            {
                //Software IDs
                res = msg.addValue(m_ocf22Keys.PMSFields_PMSSoftware, "Antibex Software");
                res = msg.addValue(m_ocf22Keys.PMSFields_PMSVersion, "4.2.7");
                res = msg.addValue(m_ocf22Keys.OCFVersion, OCFVersion);
                res = msg.addValue(m_ocf22Keys.PMSFields_PMSDocumentKey, OCFID.ToString());
                res = msg.addValue(m_ocf22Keys.PMSFields_PMSPatientKey, GetClientIDByOCFID(mCommand, "OCF22", OCFID));

                //get general info
                SqlDataReader reader = ExecQuery(mCommand, GetOCFSqlQuery("OCF22", OCFID));
                if (reader.Read() == true)
                {
                    res = msg.addValue(m_ocf22Keys.AttachmentsBeingSent, Flag(MyReaderFldToString(reader, "chkAttachments")));

                    //Header
                    res = msg.addValue(m_ocf22Keys.Header_ClaimNumber, MyReaderFldToString(reader, "CIClaimNo"));
                    res = msg.addValue(m_ocf22Keys.Header_PolicyNumber, MyReaderFldToString(reader, "CIPolicyNo"));
                    res = msg.addValue(m_ocf22Keys.Header_DateOfAccident, MyReaderFldToDateString(reader, "CIDOA")); //make sure dates are in proper format

                    //Applicant Info
                    res = msg.addValue(m_ocf22Keys.Applicant_Name_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    res = msg.addValue(m_ocf22Keys.Applicant_Name_MiddleName, MyReaderFldToString(reader, "CIMiddleName"));
                    res = msg.addValue(m_ocf22Keys.Applicant_Name_LastName, MyReaderFldToString(reader, "CILastName"));
                    res = msg.addValue(m_ocf22Keys.Applicant_DateOfBirth, MyReaderFldToDateString(reader, "CIDOB"));  //DATE
                    res = msg.addValue(m_ocf22Keys.Applicant_Gender, Gender(MyReaderFldToString(reader, "CIGender")));

                    res = msg.addValue(m_ocf22Keys.Applicant_TelephoneNumber, Phone(MyReaderFldToString(reader, "CITel")));
                    res = msg.addValue(m_ocf22Keys.Applicant_TelephoneExtension, "");
                    res = msg.addValue(m_ocf22Keys.Applicant_Address_StreetAddress1, MyReaderFldToString(reader, "CIAddress"));
                    res = msg.addValue(m_ocf22Keys.Applicant_Address_StreetAddress2, "");
                    res = msg.addValue(m_ocf22Keys.Applicant_Address_City, MyReaderFldToString(reader, "CICity"));
                    res = msg.addValue(m_ocf22Keys.Applicant_Address_Province, FormatProvince(MyReaderFldToString(reader, "CIProvince")));
                    res = msg.addValue(m_ocf22Keys.Applicant_Address_PostalCode, FormatPostalCode(MyReaderFldToString(reader, "CIPostalCode")));


                    //Insurer Info
                    res = msg.addValue(m_ocf22Keys.Insurer_IBCInsurerID, MyReaderFldToString(reader, "MVAInsurerHCAIID"));
                    res = msg.addValue(m_ocf22Keys.Insurer_IBCBranchID, MyReaderFldToString(reader, "MVABranchHCAIID"));
                    //res = msg.addValue(m_ocf22Keys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(d.Insurer_PolicyHolder_IsSameAsApplicant)); //WHY DUP
                    res = msg.addValue(m_ocf22Keys.Insurer_Adjuster_Name_FirstName, MyReaderFldToString(reader, "MVAAFirstName"));
                    res = msg.addValue(m_ocf22Keys.Insurer_Adjuster_Name_LastName, MyReaderFldToString(reader, "MVAALastName"));
                    res = msg.addValue(m_ocf22Keys.Insurer_Adjuster_TelephoneNumber, Phone(MyReaderFldToString(reader, "MVAATel")));
                    res = msg.addValue(m_ocf22Keys.Insurer_Adjuster_TelephoneExtension, MyReaderFldToString(reader, "MVAAExt"));
                    res = msg.addValue(m_ocf22Keys.Insurer_Adjuster_FaxNumber, Phone(MyReaderFldToString(reader, "MVAAFax")));
                    res = msg.addValue(m_ocf22Keys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(MyReaderFldToString(reader, "MVAPHSame")));
                    if (Flag(MyReaderFldToString(reader, "MVAPHSame")) != "Yes")
                    {
                        res = msg.addValue(m_ocf22Keys.Insurer_PolicyHolder_Name_LastName, MyReaderFldToString(reader, "MVAPHLastName"));
                        res = msg.addValue(m_ocf22Keys.Insurer_PolicyHolder_Name_FirstName, MyReaderFldToString(reader, "MVAPHFirstName"));
                    }

                    //Regulated Health Professional
                    res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_FacilityRegistryID, MyReaderFldToString(reader, "SHPHCAIFacilityID"));
                    res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_ProviderRegistryID, MyReaderFldToString(reader, "SHPHCAIRegNo"));
                    res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_Occupation, GetHCAIProviderOccupation(tCommand, MyReaderFldToString(reader, "SHPOccupation")));
                    res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists, ReturnConflictsOpt(MyReaderFldToString(reader, "SHPOptConflicts")));

                    //DebugWrite(Flag(d.OCF22_RegulatedHealthProfessional_ConflictofInterest_ConflictExists));			
                    if (Flag(MyReaderFldToString(reader, "SHPOptConflicts")) == Flag("Yes"))
                    {
                        res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails, MyReaderFldToString(reader, "SHPDeclareText"));
                    }
                    res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_IsSignatureOnFile, Flag("Yes")); //Yes all the time for now
                    res = msg.addValue(m_ocf22Keys.OCF22_RegulatedHealthProfessional_DateSigned, MyReaderFldToDateString(reader, "SHPSignedDate")); //DATE

                    res = msg.addValue(m_ocf22Keys.OCF22_NatureOfAssessment_Response, GetOCF22NatureOfAssessment(reader));


                    //Provisional Clinical Info
                    res = msg.addValue(m_ocf22Keys.OCF22_ProvisionalClinicalInformation_ClinicalInformation_PresentComplaintsDesc, MyReaderFldToString(reader, "Part5AText"));
                    res = msg.addValue(m_ocf22Keys.OCF22_ProvisionalClinicalInformation_ClinicalInformation_AlreadyProvidedTreatment_Response, Flag(MyReaderFldToString(reader, "Part5AOpt")));
                    res = msg.addValue(m_ocf22Keys.OCF22_ProvisionalClinicalInformation_AssessmentInformation_Details, MyReaderFldToString(reader, "Part5BText"));
                    res = msg.addValue(m_ocf22Keys.OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_Response, Flag(MyReaderFldToString(reader, "Part5BOpt")));
                    res = msg.addValue(m_ocf22Keys.OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_AssessmentDate, MyReaderFldToDateString(reader, "Part5BDate"));

                    //GS22LineItem (Estimated)
                    PMSToolkit.DataObjects.GS22LineItemKeys m_gs22Keys = new PMSToolkit.DataObjects.GS22LineItemKeys();
                    //HCAIOCF22ProposedGoodsAndServicesEstimated myELI = d.OCF22_ProposedGoodsAndServices_LineItem_Estimated;
                    IDataItem eli;
                    double tQty = 0;
                    SqlDataReader items_reader = ExecQuery(tCommand, "select * from OCFGoodsAndServices where ReportID = " + OCFID + " order by ItemOrder");

                    while (items_reader.Read())
                    {
                        eli = msg.newLineItem(PMSToolkit.LineItemType.GS22LineItem);
                        //deleted---eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_ReferenceNumber, myELI.OCF22_ProposedGoodsAndServices_Items_Item_ReferenceNumber);
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_PMSGSKey, MyReaderFldToString(items_reader, "ItemOrder"));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode"));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Attribute, MyReaderFldToString(items_reader, "ItemAttribute"));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "ProviderHCAINo"));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "Type"));

                        tQty = (double)(ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemQty")) * ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemBaseUnit")));

                        if (MyReaderFldToString(items_reader, "ItemMeasure") != "")
                        {
                            if (MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hr" || MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hour")
                                eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            else
                                eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                        }
                        else
                        {
                            eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                        }
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Measure, MyReaderFldToString(items_reader, "ItemMeasure"));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_GST, FlagTax(MyReaderFldToString(items_reader, "GST")));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_PST, FlagTax(MyReaderFldToString(items_reader, "GST")));
                        eli.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_LineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount")).ToString("0.00"));

                        res = msg.addLineItem(eli);
                        //myELI = d.OCF22_ProposedGoodsAndServices_LineItem_Estimated;
                    }
                    items_reader.Close();
                    items_reader = null;

                    //deleted---//GS22LineItem (Approved)
                    //HCAIOCF22ProposedGoodsAndServicesApproved myALI = d.OCF22_ProposedGoodsAndServices_LineItem_Approved;
                    //IDataItem ali;
                    //while(myALI != null)
                    //{
                    //    ali = msg.newLineItem(PMSToolkit.LineItemType.GS22LineItem);
                    //    ali.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST, Flag(myALI.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST));
                    //    ali.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST, Flag(myALI.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST));
                    //    ali.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost, myALI.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost);
                    //    ali.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup, myALI.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCode);
                    //    ali.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc, myALI.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc);
                    //    ali.addValue(m_gs22Keys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc, myALI.OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc);

                    //    res = msg.addLineItem(ali);

                    //    myALI = d.OCF22_ProposedGoodsAndServices_LineItem_Approved;
                    //}

                    //Insurer Totals
                    res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_MOH_Proposed, "-" + (MyReaderFldToString(reader, "OHIPTotal"))); //MINUS leading mimus needed
                    res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_OtherInsurers_Proposed, "-" + (MyReaderFldToString(reader, "EHC12Total"))); //MINUS leading mimus needed
                    res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_SubTotal_Proposed, MyReaderFldToString(reader, "Subtotal"));
                    res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_GST_Proposed, MyReaderFldToString(reader, "GST"));
                    res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_PST_Proposed, MyReaderFldToString(reader, "PST"));
                    res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_AutoInsurerTotal_Proposed, MyReaderFldToString(reader, "Total"));
                    //deleted---res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_MOH_Approved, d.OCF22_InsurerTotals_MOH_Approved);				
                    //deleted---res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_OtherInsurers_Approved, d.OCF22_InsurerTotals_OtherInsurers_Approved);				
                    //res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_AutoInsurerTotal_Approved, d.OCF22_InsurerTotals_AutoInsurerTotal_Approved);				
                    //res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_GST_Approved, d.OCF22_InsurerTotals_GST_Approved);				
                    //res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_PST_Approved, d.OCF22_InsurerTotals_PST_Approved);				
                    //res = msg.addValue(m_ocf22Keys.OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved, d.OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved);			

                    //Applicant Info
                    res = msg.addValue(m_ocf22Keys.OCF22_ApplicantSignature_IsApplicantSignatureOnFile, Flag("Yes"));
                    res = msg.addValue(m_ocf22Keys.OCF22_ApplicantSignature_SigningApplicant_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    res = msg.addValue(m_ocf22Keys.OCF22_ApplicantSignature_SigningApplicant_LastName, MyReaderFldToString(reader, "CILastName"));
                    res = msg.addValue(m_ocf22Keys.OCF22_ApplicantSignature_SigningDate, MyReaderFldToDateString(reader, "CISignedDate"));
                    res = msg.addValue(m_ocf22Keys.AdditionalComments, MyReaderFldToString(reader, "OCF22AdditionalComments"));
                }
                else
                {
                    //WRITE ERROR LOG
                    //ERROR OCF WAS NOT FOUND IN DATABASE
                }

                reader.Close();
                reader = null;

                IDataItem r = pmsTK.submit(mHCAILogin, mHCAIPassword, msg);

                var dt = DateTime.Now;

                if (r == null)
                {
                    ErrorMessage e = new ErrorMessage();
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");
                    e.MessageType = mType.Error;
                    //e.Error_ErrorType = "INFORMATION ERROR";
                    e.Error_PMS_Document_Key = OCFID;
                    e.Error_OCFType = "OCF22";
                    e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);

                    //GET HCAI NUMBER BY OCF ID????
                    PopulateErrorMessages(e);

                    GetHCAIErrorMsg(e);

                    ParseError(e);
                    //return e;

                    //SET DOCUMENT TO INFORMATION ERROR
                    ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 2, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                    //map change 
                    //Succ = false;
                    Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(OCFID);
                    Succ.OCFType = "OCF22";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;
                }
                else
                {
                    PMSToolkit.DataObjects.SubmitResponseKeys resKeys = new PMSToolkit.DataObjects.SubmitResponseKeys();
                    SubmitResponse myRes = new SubmitResponse();

                    myRes.MessageDate = dt.ToString("MM/dd/yyyy");
                    myRes.MessageTime = dt.ToString("hh:mm:ss tt");
                    myRes.MessageType = mType.SubmitResponse;
                    myRes.HCAI_Document_Number = r.getValue(resKeys.HCAI_Document_Number);
                    myRes.PMS_Document_Key = r.getValue(resKeys.PMSFields_PMSDocumentKey);
                    myRes.PMS_Patient_Key = r.getValue(resKeys.PMSFields_PMSPatientKey);
                    myRes.MessageOCFType = "OCF22";

                    //SET DOCUMENT TO SUCCESSFULL DELIVERY
                    ReceiveHCAISuccessfullyDelivered(mCommand, ConvertStrToLong(myRes.PMS_Document_Key), "OCF22", dt, myRes.MessageTime, myRes.HCAI_Document_Number);
                    //MAP CHANGE

                    //return myRes;
                    //Succ = true;
                    Succ.HCAI_Document_Number = myRes.HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(myRes.PMS_Document_Key);
                    Succ.OCFType = "OCF22";
                    Succ.New_HCAI_Status = HcaiStatus.SuccessfullyDelivered;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("OCF22 submit error: " + ex.Message, ex);
                DebugWrite("OCF22 Exception");
                ErrorMessage e = new ErrorMessage();
                DateTime dt = DateTime.Now;
                e.MessageDate = dt.ToString("MM/dd/yyyy");
                e.MessageTime = dt.ToString("hh:mm:ss tt");
                e.MessageType = mType.Error;
                //e.Error_ErrorType ="SYSTEM ERROR";
                e.Error_PMS_Document_Key = OCFID;
                e.Error_OCFType = "OCF22";
                e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);

                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";
                //return e;
                e.Error_CompleteErrorDesc = "HCAI System Error.\n This document could not submit this document to HCAI.\n" +
                "- Verify your connection to the internet\n- Make sure your PMS username and password are correct\n- HCAI server may be busy at this time\n" +
                "Try to to submit this document again in a few minutes.";

                ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 1, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                //WRITE ERROR LOG

                //Succ = false;
                Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF22";
                Succ.New_HCAI_Status = HcaiStatus.DeliveryFailed;
            }

            return Succ;
        }

        public OCFMapReturnType submitOCF21B(string OCFID)
        {
            DebugWrite("Submitting OCF21B document OCFID: " + OCFID);
            OCFMapReturnType Succ = new OCFMapReturnType();

            //connect to DB
            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF21B";
                Succ.New_HCAI_Status = HcaiStatus.InformationError;
                return Succ;
            }

            var tDBConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            using (tDBConnection)

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            using (var tCommand = tDBConnection.CreateCommand())
                try
            {
                if (!IsCredentialsValid() || IsValidForHCAISubmission(mCommand, OCFID, "OCF21B") == false)
                {
                    if (!IsCredentialsValid())
                        DebugWrite("Hcai credentials are empty.");
                    DebugWrite("submitOCF21B aborted.");
                    Succ.OCFID = ConvertStrToLong(OCFID);
                    Succ.OCFType = "OCF21B";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;

                    return Succ;
                }

                IDataItem msg = pmsTK.createDoc(DocType.OCF21B);
                PMSToolkit.DataObjects.OCF21BKeys m_ocf21bKeys = new PMSToolkit.DataObjects.OCF21BKeys();
                bool res;
                string[] toSplit;

                //Software IDs
                res = msg.addValue(m_ocf21bKeys.PMSFields_PMSSoftware, "Antibex Software");
                res = msg.addValue(m_ocf21bKeys.PMSFields_PMSVersion, "4.2.7");
                res = msg.addValue(m_ocf21bKeys.OCFVersion, OCFVersion);

                var version = "5";
                res = msg.addValue(m_ocf21bKeys.OCFVersion, version);

                res = msg.addValue(m_ocf21bKeys.PMSFields_PMSDocumentKey, OCFID);
                res = msg.addValue(m_ocf21bKeys.PMSFields_PMSPatientKey, GetClientIDByOCFID(mCommand, "OCF21B", OCFID));

                //get general info
                SqlDataReader reader = ExecQuery(mCommand, GetOCFSqlQuery("OCF21B", OCFID));

                if (reader.Read() == true)
                {
                    res = msg.addValue(m_ocf21bKeys.AttachmentsBeingSent, Flag(MyReaderFldToString(reader, "chkAttachments")));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_HCAI_Plan_Document_Number, ""); //TO MATCH EXACT PLAN ON HCAI

                    //Header
                    res = msg.addValue(m_ocf21bKeys.Header_ClaimNumber, MyReaderFldToString(reader, "CIClaimNo"));
                    res = msg.addValue(m_ocf21bKeys.Header_PolicyNumber, MyReaderFldToString(reader, "CIPolicyNo"));
                    res = msg.addValue(m_ocf21bKeys.Header_DateOfAccident, MyReaderFldToDateString(reader, "CIDOL")); //make sure dates are in proper format

                    //Applicant Info
                    res = msg.addValue(m_ocf21bKeys.Applicant_Name_FirstName, MyReaderFldToString(reader, "CIFirstName"));
                    res = msg.addValue(m_ocf21bKeys.Applicant_Name_MiddleName, MyReaderFldToString(reader, "CIMiddleName"));
                    res = msg.addValue(m_ocf21bKeys.Applicant_Name_LastName, MyReaderFldToString(reader, "CILastName"));
                    res = msg.addValue(m_ocf21bKeys.Applicant_DateOfBirth, MyReaderFldToDateString(reader, "CIDOB"));  //date
                    res = msg.addValue(m_ocf21bKeys.Applicant_Gender, Gender(MyReaderFldToString(reader, "CIGender")));

                    res = msg.addValue(m_ocf21bKeys.Applicant_TelephoneNumber, Phone(MyReaderFldToString(reader, "CITel")));
                    res = msg.addValue(m_ocf21bKeys.Applicant_TelephoneExtension, "");
                    res = msg.addValue(m_ocf21bKeys.Applicant_Address_StreetAddress1, MyReaderFldToString(reader, "CIAddress"));
                    res = msg.addValue(m_ocf21bKeys.Applicant_Address_StreetAddress2, "");
                    res = msg.addValue(m_ocf21bKeys.Applicant_Address_City, MyReaderFldToString(reader, "CICity"));
                    res = msg.addValue(m_ocf21bKeys.Applicant_Address_Province, FormatProvince(MyReaderFldToString(reader, "CIProvince")));
                    res = msg.addValue(m_ocf21bKeys.Applicant_Address_PostalCode, FormatPostalCode(MyReaderFldToString(reader, "CIPostalCode")));

                    //Insurer Info
                    res = msg.addValue(m_ocf21bKeys.Insurer_IBCInsurerID, MyReaderFldToString(reader, "MVAInsurerHCAIID"));
                    res = msg.addValue(m_ocf21bKeys.Insurer_IBCBranchID, MyReaderFldToString(reader, "MVABranchHCAIID"));
                    //res = msg.addValue(m_ocf21bKeys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(d.Insurer_PolicyHolder_IsSameAsApplicant)); //WHY DUPLICATE
                    res = msg.addValue(m_ocf21bKeys.Insurer_Adjuster_Name_FirstName, MyReaderFldToString(reader, "IIAdjFirstName"));
                    res = msg.addValue(m_ocf21bKeys.Insurer_Adjuster_Name_LastName, MyReaderFldToString(reader, "IIAdjLastName"));
                    res = msg.addValue(m_ocf21bKeys.Insurer_Adjuster_TelephoneNumber, Phone(MyReaderFldToString(reader, "IIAdjTel")));
                    res = msg.addValue(m_ocf21bKeys.Insurer_Adjuster_TelephoneExtension, MyReaderFldToString(reader, "IIAdjExt"));
                    res = msg.addValue(m_ocf21bKeys.Insurer_Adjuster_FaxNumber, Phone(MyReaderFldToString(reader, "IIAdjFax")));
                    res = msg.addValue(m_ocf21bKeys.Insurer_PolicyHolder_IsSameAsApplicant, Flag(MyReaderFldToString(reader, "IIPHolderSame")));
                    if (Flag(MyReaderFldToString(reader, "IIPHolderSame")) != "Yes")
                    {
                        res = msg.addValue(m_ocf21bKeys.Insurer_PolicyHolder_Name_LastName, MyReaderFldToString(reader, "IIPHolderLastName"));
                        res = msg.addValue(m_ocf21bKeys.Insurer_PolicyHolder_Name_FirstName, MyReaderFldToString(reader, "IIPHolderFirstName"));
                    }
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InvoiceInformation_FirstInvoice, Flag(MyReaderFldToString(reader, "FirstInvoice")));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InvoiceInformation_LastInvoice, Flag(MyReaderFldToString(reader, "LastInvoice")));

                    //Previously Approved Goods and Services
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InvoiceInformation_InvoiceNumber, OCFID);

                    //july 2012 rule -- get ocf18 hcai number
                    string hPlanNo = "";

                    //if (July2012 <= DateTime.Now)
                    //{
                    hPlanNo = MyReaderFldToString(reader, "RefOfHCAI_PlanNumber");
                    if (hPlanNo.Trim() == "")
                    {
                        hPlanNo = MyReaderFldToString(reader, "pHCAIDocNumber");
                    }
                    //}
                    //else
                    //{
                    //    hPlanNo = ""; // "na";
                    //}

                    if (MyReaderFldToString(reader, "PlanType") != null && MyReaderFldToString(reader, "PlanType") != "" && MyReaderFldToString(reader, "PlanDate") != null && MyReaderFldToString(reader, "PlanDate") != "" && hPlanNo.Trim() != "")
                    {
                        //if (Sept2010 > DateTime.Now)
                        //{
                        //    res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type, MyConvert.MyLeft(MyReaderFldToString(reader, "PlanType"), 5));
                        //}
                        //else
                        //{
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type, "OCF18OR22");
                        //}

                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate, MyReaderFldToDateString(reader, "PlanDate"));
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved, ConvertStrToDouble(MyReaderFldToString(reader, "ApprovedAmt")).ToString("0.00"));
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled, ConvertStrToDouble(MyReaderFldToString(reader, "PrevBilled")).ToString("0.00"));

                        //if (July2012 <= DateTime.Now)
                        //{
                        //    ////july 2012 rule -- get ocf18 hcai number

                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber, hPlanNo);
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type, "");
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate, "");
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved, "");
                        res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled, "");

                        //}
                        //else
                        //{
                        //    if (Sept2010 > DateTime.Now) //(DateTime)reader["PlanDate"])
                        //    {
                        //        if (MyConvert.MyLeft(MyReaderFldToString(reader, "PlanType"), 5) == "OCF18")
                        //        {
                        //            if (MyReaderFldToString(reader, "PlanNumber") != null &&
                        //                MyReaderFldToString(reader, "PlanNumber") != "")
                        //            {
                        //                res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber,
                        //                        MyReaderFldToString(reader, "PlanNumber"));
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        //if (July2012 <= DateTime.Now)
                        //{
                        if (hPlanNo.Trim() != "")
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber, hPlanNo);
                        }
                        else
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber, "exempt");
                        }
                        //}
                    }

                    //Payee
                    res = msg.addValue(m_ocf21bKeys.OCF21B_Payee_FacilityRegistryID, mHCAIID);
                    res = msg.addValue(m_ocf21bKeys.OCF21B_Payee_MakeChequePayableTo, MyReaderFldToString(reader, "FIPayTo"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_Payee_FacilityIsPayee, Flag(MyReaderFldToString(reader, "FacilityIsPayee")));  //todo - Ver3.13

                    string Conflicts = "No";
                    if (MyReaderFldToString(reader, "FINoConflicts") != "")
                    {
                        if (MyReaderFldToString(reader, "FINoConflicts") == "0")
                        {
                            if (MyReaderFldToString(reader, "FIDaclare") != "")
                            {
                                if (MyReaderFldToString(reader, "FIDaclare") == "1")
                                    Conflicts = "Yes";
                            }
                        }
                    }

                    //if (Sept2010 > DateTime.Now)
                    //{
                    //    res = msg.addValue(m_ocf21bKeys.OCF21B_Payee_ConflictOfInterests_ConflictExists, Conflicts);
                    //    if (Conflicts == "Yes")
                    //        res = msg.addValue(m_ocf21bKeys.OCF21B_Payee_ConflictOfInterests_ConflictDetails, MyReaderFldToString(reader, "FIDaclareText"));
                    //}

                    //Missing --- res = msg.addValue(m_ocf21bKeys.OCF21B_Payee_DateSigned, d.OCF21B_Payee_DateSigned);

                    //Injury Codes
                    PMSToolkit.DataObjects.OCF21BInjuryLineItemKeys m_injKeys = new PMSToolkit.DataObjects.OCF21BInjuryLineItemKeys();
                    //string injCode = d.InjuriesAndSequelae_Injury_Code;
                    IDataItem inj;

                    SqlDataReader inj_reader = ExecQuery(tCommand, "select * from InvoiceInjuries where InvoiceID = " + OCFID + " order by iOrder");

                    while (inj_reader.Read())
                    {
                        inj = msg.newLineItem(PMSToolkit.LineItemType.InjuryLineItem);
                        inj.addValue(m_injKeys.OCF21B_InjuriesAndSequelae_Injury_Code, MyReaderFldToString(inj_reader, "Code"));
                        res = msg.addLineItem(inj);
                        //injCode = d.InjuriesAndSequelae_Injury_Code;
                    }
                    inj_reader.Close();
                    inj_reader = null;

                    //Proposed Goods and Services (Estimated)
                    PMSToolkit.DataObjects.GS21BLineItemKeys m_gs21BKeys = new PMSToolkit.DataObjects.GS21BLineItemKeys();
                    //HCAIOCF21BProposedGoodsAndServicesEstimated myELI = d.OCF21B_ProposedGoodsAndServices_Estimated;
                    IDataItem eli;
                    double tQty = 0;
                    SqlDataReader items_reader = ExecQuery(tCommand, "select * from InvoiceItemDetails where InvoiceID = " + OCFID + " order by PMSGSKey, ItemDate, ItemOrder");

                    while (items_reader.Read())
                    {
                        eli = msg.newLineItem(PMSToolkit.LineItemType.GS21BLineItem);
                        //deleted---eli.addValue(m_gs21BKeys.OCF21B_ProposedGoodsAndServices_Items_Item_ReferenceNumber, myELI.OCF21B_ProposedGoodsAndServices_Items_Item_ReferenceNumber);
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey, MyReaderFldToString(items_reader, "PMSGSKey"));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code, MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", ""));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute, MyReaderFldToString(items_reader, "ItemAttribute"));

                        if (MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "GXX99" || MyReaderFldToString(items_reader, "ItemServiceCode").Replace(".", "") == "AXXOT")
                        {
                            eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description, MyReaderFldToString(items_reader, "ItemDescription")); //todo - Ver3.13
                        }


                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "ProviderHCAINo"));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation, MyReaderFldToString(reader, MyConvert.MyLeft(MyReaderFldToString(items_reader, "ItemPR"), 1) + "Type"));

                        tQty = (double)(ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemQty")) * ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemBaseUnit")));

                        if (MyReaderFldToString(items_reader, "ItemMeasure") != "")
                        {
                            if (MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hr" || MyReaderFldToString(items_reader, "ItemMeasure").ToLower() == "hour")
                                eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                            else
                                eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                        }
                        else
                        {
                            eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity, tQty.ToString("0.00"));
                        }
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure, MyReaderFldToString(items_reader, "ItemMeasure"));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService, MyReaderFldToDateString(items_reader, "ItemDate"));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST, FlagTax(MyReaderFldToString(items_reader, "GST")));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST, FlagTax(MyReaderFldToString(items_reader, "PST")));
                        eli.addValue(m_gs21BKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost, ConvertStrToDouble(MyReaderFldToString(items_reader, "ItemAmount")).ToString("0.00"));

                        res = msg.addLineItem(eli);
                        //myELI = d.OCF21B_ProposedGoodsAndServices_Estimated;
                    }
                    items_reader.Close();
                    items_reader = null;

                    //Other Insurance
                    double MOHTotal = 0;
                    double oTotal = 0;
                    double oTotal2 = 0;

                    double MOHTotalDebits = 0;
                    double oTotalDebits = 0;
                    double oTotal2Debits = 0;

                    //TODO Nov2014 check logic
                    var isAmountRefused = true;
                    //if (DateTime.Now >= Config.Nov2014)
                    //{
                    if (Flag(MyReaderFldToString(reader, "IsAmountRefused")) == "Yes")
                    {
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_IsAmountRefused, "Yes");
                        isAmountRefused = true;
                    }
                    else
                    {
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_IsAmountRefused, "No");
                        isAmountRefused = false;
                    }
                    //}

                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage, Flag(MyReaderFldToString(reader, "qEHC")));

                    if (Flag(MyReaderFldToString(reader, "qEHC")) == Flag("Yes"))
                    {
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_MOHAvailable, FlagNA(MyReaderFldToString(reader, "qOHIP")));
                        if (FlagNA(MyReaderFldToString(reader, "qOHIP")) == FlagNA("Yes"))
                        {
                            //TODO Nov2014 add fields to database -- DONE
                            //if (DateTime.Now >= Config.Nov2014)
                            //{
                            if (isAmountRefused == true)
                            {
                                if (IsNumeric(MyReaderFldToString(reader, "OHIPChiro")) == true)
                                {
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOH_Chiropractic,
                                                       (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro")))
                                                           .ToString());
                                    MOHTotal = MOHTotal +
                                               ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro"));
                                }

                                if (IsNumeric(MyReaderFldToString(reader, "OHIPPhysio")) == true)
                                {
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy,
                                                       (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio")))
                                                           .ToString());
                                    MOHTotal = MOHTotal +
                                               ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio"));
                                }
                                if (IsNumeric(MyReaderFldToString(reader, "OHIPMassage")) == true)
                                {
                                    res = msg.addValue(
                                        m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy,
                                        (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage"))).ToString());
                                    MOHTotal = MOHTotal +
                                               ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage"));
                                }
                                if (IsNumeric(MyReaderFldToString(reader, "OHIPX")) == true)
                                {
                                    //TODO Nov2014 check logic -- OK
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOH_OtherService,
                                                       (ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX"))).
                                                           ToString());
                                    MOHTotal = MOHTotal + ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX"));
                                }

                            }
                            var otmp = ConvertStrToDouble(MyReaderFldToString(reader, "OHIPChiro_Debit"));
                            if (otmp >= 0)
                            {
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic, otmp.ToString());
                                MOHTotalDebits += otmp;
                            }

                            otmp = ConvertStrToDouble(MyReaderFldToString(reader, "OHIPPhysio_Debit"));
                            if (otmp >= 0)
                            {
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy, otmp.ToString());
                                MOHTotalDebits += otmp;
                            }

                            otmp = ConvertStrToDouble(MyReaderFldToString(reader, "OHIPMassage_Debit"));
                            if (otmp >= 0)
                            {
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy, otmp.ToString());
                                MOHTotalDebits += otmp;
                            }

                            otmp = ConvertStrToDouble(MyReaderFldToString(reader, "OHIPX_Debit"));
                            if (otmp >= 0)
                            {
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService, otmp.ToString());
                                MOHTotalDebits += otmp;
                            }
                        }

                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed, MOHTotalDebits.ToString("0.00"));

                        if (MyReaderFldToString(reader, "EHC1IDCertNo") != null && MyReaderFldToString(reader, "EHC1IDCertNo") != "")
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID, MyReaderFldToString(reader, "EHC1IDCertNo"));
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name, MyReaderFldToString(reader, "EHC1"));
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber, MyReaderFldToString(reader, "EHC1PolicyNo"));
                            if (MyReaderFldToString(reader, "EHC1PlanMember") == null || MyReaderFldToString(reader, "EHC1PlanMember") == "")
                            {
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, "");
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, "");
                            }
                            else
                            {
                                toSplit = MyReaderFldToString(reader, "EHC1PlanMember").Split(',');
                                if (toSplit.Length > 1)
                                {
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, toSplit[1].ToString());
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, toSplit[0].ToString());
                                }
                                else
                                {
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName, "");
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, toSplit[0].ToString());
                                }
                            }
                        }
                        else
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName, "N/A"); //WHY
                        }
                        oTotal = 0;

                        //TODO - Nove2014 add fields into database ==  NOV 2014 - EHC debits fields
                        //if (DateTime.Now > Config.Nov2014)
                        //{
                        if (isAmountRefused == true)
                        {
                            if (IsNumeric(MyReaderFldToString(reader, "EHC1Chiro")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic, "-" + MyReaderFldToString(reader, "EHC1Chiro"));
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic,
                                                   (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"))).
                                                       ToString());
                                oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"));
                            }
                            if (IsNumeric(MyReaderFldToString(reader, "EHC1Physio")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy, "-" + MyReaderFldToString(reader, "EHC1Physio"));
                                res = msg.addValue(
                                    m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy,
                                    (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"))).ToString());
                                oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"));
                            }
                            if (IsNumeric(MyReaderFldToString(reader, "EHC1Massage")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy, "-" + MyReaderFldToString(reader, "EHC1Massage"));
                                res = msg.addValue(
                                    m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy,
                                    (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"))).ToString());
                                oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"));
                            }
                            if (IsNumeric(MyReaderFldToString(reader, "EHC1X")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService, "-" + MyReaderFldToString(reader, "EHC1X"));
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService,
                                                   (ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"))).
                                                       ToString());
                                oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"));
                            }
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed,
                                               oTotal.ToString());

                        }
                        var tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic, tmp.ToString());
                            oTotalDebits += tmp;
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy, tmp.ToString());
                            oTotalDebits += tmp;
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy, tmp.ToString());
                            oTotalDebits += tmp;
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService, tmp.ToString());
                            oTotalDebits += tmp;
                        }
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed, oTotalDebits.ToString());
                        //}

                        //else
                        //{
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC1Chiro")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic, "-" + MyReaderFldToString(reader, "EHC1Chiro"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"))).ToString());
                        //        oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Chiro"));
                        //    }
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC1Physio")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy, "-" + MyReaderFldToString(reader, "EHC1Physio"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"))).ToString());
                        //        oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Physio"));
                        //    }
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC1Massage")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy, "-" + MyReaderFldToString(reader, "EHC1Massage"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"))).ToString());
                        //        oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1Massage"));
                        //    }
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC1X")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService, "-" + MyReaderFldToString(reader, "EHC1X"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"))).ToString());
                        //        oTotal = oTotal + ConvertStrToDouble(MyReaderFldToString(reader, "EHC1X"));
                        //    }
                        //    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed, oTotal.ToString());
                        //}


                        if (MyReaderFldToString(reader, "EHC2IDCertNo") != null && MyReaderFldToString(reader, "EHC2IDCertNo") != "")
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID, MyReaderFldToString(reader, "EHC2IDCertNo"));
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name, MyReaderFldToString(reader, "EHC2"));

                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber, MyReaderFldToString(reader, "EHC2PolicyNo"));

                            if (MyReaderFldToString(reader, "EHC2PlanMember") == null || MyReaderFldToString(reader, "EHC2PlanMember") == "")
                            {
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, "");
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, "");
                            }
                            else
                            {
                                toSplit = MyReaderFldToString(reader, "EHC2PlanMember").Split(',');
                                if (toSplit.Length > 1)
                                {
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, toSplit[1].ToString());
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, toSplit[0].ToString());
                                }
                                else
                                {
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName, "");
                                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, toSplit[0].ToString());
                                }
                            }
                        }
                        else
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName, "N/A"); //WHY
                        }


                        //TODO - Nove2014 add fields into database
                        //if (DateTime.Now >= Config.Nov2014)
                        //{

                        if (isAmountRefused == true)
                        {
                            if (IsNumeric(MyReaderFldToString(reader, "EHC2Chiro")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic, "-" + MyReaderFldToString(reader, "EHC2Chiro"));
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic,
                                                   (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"))).ToString());
                                oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"));
                            }
                            if (IsNumeric(MyReaderFldToString(reader, "EHC2Physio")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy, "-" + MyReaderFldToString(reader, "EHC2Physio"));
                                res = msg.addValue(
                                    m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy,
                                    (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"))).ToString());
                                oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"));
                            }
                            if (IsNumeric(MyReaderFldToString(reader, "EHC2Massage")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy, "-" + MyReaderFldToString(reader, "EHC2Massage"));
                                res = msg.addValue(
                                    m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy,
                                    (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"))).ToString());
                                oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"));
                            }
                            if (IsNumeric(MyReaderFldToString(reader, "EHC2X")) == true)
                            {
                                //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService, "-" + MyReaderFldToString(reader, "EHC2X"));
                                res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService,
                                                   (ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"))).ToString());
                                oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"));
                            }
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed,
                                               oTotal2.ToString());
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic, tmp.ToString());
                            oTotal2Debits += tmp;
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy, tmp.ToString());
                            oTotal2Debits += tmp;
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy, tmp.ToString());
                            oTotal2Debits += tmp;
                        }

                        tmp = ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X_Debit"));
                        if (tmp >= 0)
                        {
                            res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService, tmp.ToString());
                            oTotal2Debits += tmp;
                        }
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed, oTotal2Debits.ToString());
                        //}
                        //else
                        //{
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC2Chiro")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic, "-" + MyReaderFldToString(reader, "EHC2Chiro"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"))).ToString());
                        //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Chiro"));
                        //    }
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC2Physio")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy, "-" + MyReaderFldToString(reader, "EHC2Physio"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"))).ToString());
                        //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Physio"));
                        //    }
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC2Massage")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy, "-" + MyReaderFldToString(reader, "EHC2Massage"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"))).ToString());
                        //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2Massage"));
                        //    }
                        //    if (IsNumeric(MyReaderFldToString(reader, "EHC2X")) == true)
                        //    {
                        //        //res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService, "-" + MyReaderFldToString(reader, "EHC2X"));
                        //        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService, (-1 * ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"))).ToString());
                        //        oTotal2 = oTotal2 + ConvertStrToDouble(MyReaderFldToString(reader, "EHC2X"));
                        //    }
                        //    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed, oTotal2 > 0 ? (-oTotal2).ToString() : "0.00");
                        //}
                    }

                    if (MyReaderFldToString(reader, "XService") != null && MyReaderFldToString(reader, "XService") != "")
                    {
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_OtherServiceType, MyReaderFldToString(reader, "XService"));
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType, MyReaderFldToString(reader, "XService"));
                    }

                    //TODO Nov2014 add these fields to database  -- OK

                    //if (MyReaderFldToString(reader, "XService_Debit") != null && MyReaderFldToString(reader, "XService_Debit") != "")
                    //{
                    //    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType, MyReaderFldToString(reader, "XService_Debit"));
                    //}
                    //Account and Totals
                    res = msg.addValue(m_ocf21bKeys.OCF21B_AccountActivity_PriorBalance, ConvertStrToDouble(MyReaderFldToString(reader, "PriorBalance")).ToString("0.00"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer, ConvertStrToDouble(MyReaderFldToString(reader, "PaymentReceived")).ToString("0.00"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_AccountActivity_OverdueAmount, ConvertStrToDouble(MyReaderFldToString(reader, "PriorBalance")).ToString("0.00"));

                    //TODO - Nove2014 check logic -- looks good need to test it
                    //if (DateTime.Now >= Config.Nov2014)
                    //{
                    if (isAmountRefused == true)
                    {
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed, MOHTotal.ToString("0.00"));
                        res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed, MOHTotalDebits.ToString("0.00"));
                    }


                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_OtherInsurers_Proposed, ConvertStrToDouble((((oTotal - oTotalDebits) + (oTotal2 - oTotal2Debits))).ToString()).ToString("0.00"));
                    //var total = ConvertStrToDouble(MyReaderFldToString(reader, "MVATotal")) - ((MOHTotal-MOHTotalDebits) + ((oTotal - oTotalDebits) + (oTotal2 - oTotal2Debits)));

                    var total = ConvertStrToDouble(MyReaderFldToString(reader, "MVATotal"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed, total.ToString());
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_MOH_Proposed, (MOHTotal - MOHTotalDebits).ToString());
                    //}
                    //else
                    //{
                    //    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_OtherInsurers_Proposed, ConvertStrToDouble((-1 * (oTotal + oTotal2)).ToString()).ToString("0.00"));    
                    //    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "MVATotal")).ToString("0.00"));
                    //    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed,  (-1*MOHTotal).ToString());
                    //}

                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_GST_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "InvoiceGST")).ToString("0.00"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_PST_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "InvoicePST")).ToString("0.00"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_Interest_Proposed, ConvertStrToDouble(MyReaderFldToString(reader, "Interest")).ToString("0.00"));
                    res = msg.addValue(m_ocf21bKeys.OCF21B_InsurerTotals_SubTotal_Proposed, ConvertStrToDouble((ConvertStrToDouble(MyReaderFldToString(reader, "InvoiceTotal")) - (ConvertStrToDouble(MyReaderFldToString(reader, "Interest")) + ConvertStrToDouble(MyReaderFldToString(reader, "InvoiceGST")) + ConvertStrToDouble(MyReaderFldToString(reader, "InvoicePST")))).ToString()).ToString("0.00"));

                    res = msg.addValue(m_ocf21bKeys.OCF21B_OtherInformation, MyReaderFldToString(reader, "Comments"));
                    res = msg.addValue(m_ocf21bKeys.AdditionalComments, MyReaderFldToString(reader, "AdditionalComments"));
                }
                else
                {
                    //WRITE ERROR LOG
                    //OCF DOES NOT EXIST WHY?
                    DebugWrite("error geting ocf from db: reportid: " + OCFID.ToString());
                }

                reader.Close();
                reader = null;
                IDataItem r = pmsTK.submit(mHCAILogin, mHCAIPassword, msg);
                DateTime dt = DateTime.Now;

                if (r == null)
                {
                    ErrorMessage e = new ErrorMessage();
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");
                    e.MessageType = mType.Error;
                    e.Error_OCFType = "OCF21B";
                    e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);
                    PopulateErrorMessages(e);
                    GetHCAIErrorMsg(e);
                    ParseError(e);

                    //SET DOCUMENT TO INFORMATION ERROR
                    ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 2, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                    Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(OCFID);
                    Succ.OCFType = "OCF21B";
                    Succ.New_HCAI_Status = HcaiStatus.InformationError;
                }
                else
                {
                    PMSToolkit.DataObjects.SubmitResponseKeys resKeys = new PMSToolkit.DataObjects.SubmitResponseKeys();
                    SubmitResponse myRes = new SubmitResponse();
                    myRes.MessageDate = dt.ToString("MM/dd/yyyy");
                    myRes.MessageTime = dt.ToString("hh:mm:ss tt");
                    myRes.MessageType = mType.SubmitResponse;
                    myRes.HCAI_Document_Number = r.getValue(resKeys.HCAI_Document_Number);
                    myRes.PMS_Document_Key = r.getValue(resKeys.PMSFields_PMSDocumentKey);
                    myRes.PMS_Patient_Key = r.getValue(resKeys.PMSFields_PMSPatientKey);
                    myRes.MessageOCFType = "OCF21B";
                    //SET DOCUMENT TO SUCCESSFULL DELIVERY
                    ReceiveHCAISuccessfullyDelivered(mCommand, ConvertStrToLong(myRes.PMS_Document_Key), myRes.MessageOCFType, dt, myRes.MessageTime, myRes.HCAI_Document_Number);
                    //MAP CHANGE
                    Succ.HCAI_Document_Number = myRes.HCAI_Document_Number;
                    Succ.OCFID = ConvertStrToLong(myRes.PMS_Document_Key);
                    Succ.OCFType = "OCF21B";
                    Succ.New_HCAI_Status = HcaiStatus.SuccessfullyDelivered;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("OCF21B submit error: " + ex.Message, ex);
                DebugWrite("OCF21B Exception");
                ErrorMessage e = new ErrorMessage();
                DateTime dt = DateTime.Now;
                e.MessageDate = dt.ToString("MM/dd/yyyy");
                e.MessageTime = dt.ToString("hh:mm:ss tt");
                e.MessageType = mType.Error;
                e.Error_OCFType = "OCF21B";
                e.Error_HCAI_Document_Number = GetHCAINumberByOCFID(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType); //r.getValue(e.Error_HCAI_Document_Number);
                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";
                e.Error_CompleteErrorDesc = "HCAI System Error.\n This document could not submit this document to HCAI.\n" +
                "- Verify your connection to the internet\n- Make sure your PMS username and password are correct\n- HCAI server may be busy at this time\n" +
                "Try to to submit this document again in a few minutes.";
                ReceiveHCAIError(mCommand, ConvertStrToLong(OCFID), e.Error_OCFType, e.Error_CompleteErrorDesc, 1, dt, e.MessageTime, e.Error_HCAI_Document_Number);
                //WRITE ERROR LOG 
                Succ.HCAI_Document_Number = e.Error_HCAI_Document_Number;
                Succ.OCFID = ConvertStrToLong(OCFID);
                Succ.OCFType = "OCF21B";
                Succ.New_HCAI_Status = HcaiStatus.DeliveryFailed;
            }

            return Succ;
        }

        public bool IsOCFExists(SqlCommand mCommand, string HCAINumber, string OCFType, long OCFID)
        {
            SqlDataReader reader = null;
            bool IsExists = false;

            try
            {
                if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                {
                    reader = ExecQuery(mCommand, "select * from HCAIStatusLog H " +
                     "where H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.InvoiceID = H.InvoiceID) AND " +
                     "(H.HCAIDocNumber = '" + HCAINumber + "' OR H.InvoiceID = " + OCFID + ") AND H.OCFType = '" + MyConvert.MyLeft(OCFType, 6) + "'");
                }
                else
                {
                    //reader = ExecQuery(mCommand, "select * from HCAIStatusLog where IsCurrentStatus = 1 AND (HCAIDocNumber = '" + HCAINumber + "' OR ReportID = " + OCFID + ") AND OCFType = '" + MyConvert.MyLeft(OCFType, 5) + "'");

                    reader = ExecQuery(mCommand, "select * from HCAIStatusLog H " +
                    "where H.StatusOrder = (SELECT MAX(H2.StatusOrder) FROM HCAIStatusLog H2 WHERE H2.ReportID = H.ReportID) AND " +
                    "(H.HCAIDocNumber = '" + HCAINumber + "' OR H.ReportID = " + OCFID + ") AND H.OCFType = '" + MyConvert.MyLeft(OCFType, 5) + "'");
                }

                if (reader.Read() == true)
                {
                    if (MyReaderFldToDateString(reader, "StatusDate").ToString() != "")
                    {
                        IsExists = true;
                    }
                }
                else
                {
                    Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                            "in function: IsOCFExists, hcai current/max record was not found \n" +
                            "OCFType: " + MyConvert.MyLeft(OCFType, 5) + " OCFID: " + OCFID);
                }
                reader.Close();
                reader = null;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error(ex.Message, ex);
            }
            return IsExists;
        }

        //RESET DUPLICATE HCAI LOG STATUSES -- NOT USED UNTIL SUCH CLIENT IS FOUND
        //NEED TO PLUG IN INTO AJDUSTER RESPONSE AFTER 10000 ITERATIONS
        private bool CleanUpDupAdjResponses(SqlCommand mCommand, string HCAIDocNumber)
        {
            bool IsCleaned = true;
            int OCFStatus = 0;
            int MinStatusOrder = 80;

            //HCAIStatusLog fields
            int StatusOrder;
            string OCFType;
            DateTime StatusDate;
            int IsCurrentStatus;
            string ErrorMsg;
            float InvoiceID;
            float ReportID;
            string StatusTime;
            int Archived;
            int LockStatus;
            string str_sql;

            var reader = ExecQuery(mCommand, "select * from HCAIStatusLog H " +
                                             "where H.IsCurrentStatus = 1 AND H.HCAIDocNumber = '" + HCAIDocNumber + "'");
            try
            {
                if (reader.Read())
                {
                    if (MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder")) > 100)
                    {
                        //clean up duplicate adjuster responses when status order reached over 100 records
                        OCFStatus = MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "OCFStatus"));
                        StatusOrder = MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "StatusOrder"));
                        OCFType = MyReaderFldToString(reader, "OCFType");
                        StatusDate = (DateTime)reader["StatusDate"];
                        IsCurrentStatus = MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "IsCurrentStatus"));
                        ErrorMsg = MyReaderFldToString(reader, "ErrorMsg");
                        InvoiceID = MyConvert.ConvertStrToFloat(MyReaderFldToString(reader, "InvoiceID"));
                        ReportID = MyConvert.ConvertStrToFloat(MyReaderFldToString(reader, "ReportID"));
                        StatusTime = MyReaderFldToString(reader, "StatusTime");
                        Archived = MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "Archived"));
                        LockStatus = MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "LockStatus"));

                        reader.Close();
                        reader = null;

                        reader = ExecQuery(mCommand, "select MIN(H.StatusOrder) as M from HCAIStatusLog H " +
                        "where H.HCAIDocNumber = '" + HCAIDocNumber + "' AND H.OCFStatus = " + OCFStatus);

                        //1. set min status order
                        if (MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "M")) > 2 &&
                            MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "M")) < MinStatusOrder)
                        {
                            MinStatusOrder = MyConvert.ConvertStrToInt(MyReaderFldToString(reader, "M"));
                        }

                        reader.Close();
                        reader = null;

                        //2. remove all recorder where status order over MinStatusOrder
                        if (ExecQueryForWrite(mCommand, "DELETE FROM HCAIStatusLog " +
                        "WHERE HCAIDocNumber = '" + HCAIDocNumber + "' AND H.StatusOrder > '" + MinStatusOrder + "'") == true)
                        {
                            //3. record max status with order by
                            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                            {
                                str_sql = "INSERT INTO HCAIStatusLog (StatusOrder, OCFType, StatusDate, IsCurrentStatus, OCFStatus, InvoiceID, StatusTime, HCAIDocNumber, ErrorMsg) " +
                                "VALUES (" + MinStatusOrder + ",'" + MyConvert.MyLeft(OCFType, 6) + "','" + StatusDate.ToString("yyyy/MM/dd") + "',1," +
                                OCFStatus + "," + InvoiceID + ", '" +
                                StatusTime + "','" + HCAIDocNumber.Replace("'", "''").ToString() + "','" + ErrorMsg.Replace("'", "''").ToString() + "')";
                            }
                            else
                            {
                                str_sql = "INSERT INTO HCAIStatusLog (StatusOrder, OCFType, StatusDate, IsCurrentStatus, OCFStatus, ReportID, StatusTime, HCAIDocNumber, ErrorMsg) " +
                                "VALUES (" + MinStatusOrder + ",'" + MyConvert.MyLeft(OCFType, 6) + "','" + StatusDate.ToString("yyyy/MM/dd") + "',1," +
                                OCFStatus + "," + ReportID + ", '" +
                                StatusTime + "','" + HCAIDocNumber.Replace("'", "''").ToString() + "','" + ErrorMsg.Replace("'", "''").ToString() + "')";
                            }

                            if (ExecQueryForWrite(mCommand, str_sql) == false)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                reader.Close();
            }
            catch
            {
                ////
            }
            return IsCleaned;
        }


        public OCFMapReturnType getAdjusterResponse()
        {
            //return null;
            var Succ = new OCFMapReturnType();

            if (!IsCredentialsValid())
            {
                Debugger.LogWarn("Hcai credentials are empty. getAdjusterResponse aborted.");
                return Succ;
            }

            var e = new ErrorMessage { MessageType = mType.Error };
            e.Error_ErrorType = mType.RequestAdjusterResponse;

            var adRes = new AdjusterResponse();

            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                return Succ;
            }

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
                try
                {
                    Debugger.LogInfo("GetAdjusterResponse is called.");
                    IDataItem res = pmsTK.adjusterResponse(mHCAILogin, mHCAIPassword);

                    DateTime dt = DateTime.Now;
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");

                    if (res == null)
                    {
#if LOG_ADRES
                    //just for testing
                    StreamWriter mFd0;
                    mFd0 = File.AppendText("C:\\Program Files\\Antibex\\Universal CPR\\AdResponses.log");
                    mFd0.WriteLine("FAILED Adjuster Response:");
                    mFd0.WriteLine("\tTime " + e.MessageDate + " " + e.MessageTime);
                    mFd0.WriteLine("===================");
                    mFd0.WriteLine();
                    mFd0.Flush();
                    mFd0.Close();
                    //
#endif
                        PopulateErrorMessages(e);
                        ParseError(e);
                        //WRITE ERROR LOG
                        return Succ;
                    }

                    //AdjusterResponse adRes = new AdjusterResponse();
                    adRes.MessageType = mType.AdjusterResponseRecieved;

                    adRes.MessageDate = dt.ToString("MM/dd/yyyy");
                    adRes.MessageTime = dt.ToString("hh:mm:ss tt");

                    var resKeys = new AdjusterResponseResponseKeys();

                    GetCommonKeys(res, adRes);

                    adRes.PMS_Document_Key = GetUODocumentID_ByHCAINumber(mCommand, adRes.Document_Type,
                        adRes.HCAI_Document_Number, ConvertStrToLong(adRes.PMS_Document_Key)).ToString();

                    //validate that hcai document number exists in our DB
                    if (IsHCAIDocumentExistsInUO(mCommand, adRes.HCAI_Document_Number, adRes.PMS_Document_Key, true, adRes.Document_Type) == false)
                    {

                        Succ.OCFType = adRes.Document_Type;
                        Succ.HCAI_Document_Number = adRes.HCAI_Document_Number;
                        Succ.OCFID = ConvertStrToLong(adRes.PMS_Document_Key);
                        Succ.New_HCAI_Status = (HcaiStatus)0;
                        Succ.StatusDate = "";
                        Succ.StatusTime = "";
                        Succ.ATotal = 0;
                        Succ.MinSentDate = "";

                        // for testing purposes only
                        //this.sendAdjusterResponseAck(Succ.HCAI_Document_Number);
                        return Succ;
                    }

                    Succ.HCAI_Document_Number = adRes.HCAI_Document_Number;
                    Succ.OCFID = MyConvert.ConvertStrToLong(adRes.PMS_Document_Key);
                    Succ.OCFType = adRes.Document_Type;
                    if (adRes.Document_Status.ToLower() == "approved")
                        Succ.New_HCAI_Status = HcaiStatus.Approved;
                    else if (adRes.Document_Status.ToLower() == "partially approved")
                        Succ.New_HCAI_Status = HcaiStatus.PartiallyApproved;
                    else //denied                
                        Succ.New_HCAI_Status = HcaiStatus.NotApproved;


                    //CHECKING FOR MULTIPLE ADJUSTER RESPONSE TO THE SAME DOCUMENT
                    if (adRes.HCAI_Document_Number == mAdjResponseDocNumber)
                    {
                        mAdjResponseDocNumberCounter++;
                        mAdjResponseDocNumberCounter = mAdjResponseDocNumberCounter % mAdjResponseDocNumberMax;

                        //on every Nth time send email and allow db update
                        //other wise quit
                        if (mAdjResponseDocNumberCounter == 0)
                        {
                            Debugger.WriteToFile("Document " + mAdjResponseDocNumber + " is stuck in Adjuster Response.", 100);
                            //EMAIL ME
                            string msg = CreateErrorEmailBody("getAdjusterResponse DUP", adRes.HCAI_Document_Number,
                            "Duplicate HCAI Number", e.Error_HCAI_ErrorType);
                            Debugger.SendEmail("DUPLICATE: " + mAdjResponseDocNumberMax + "th try. " + BusinessName + " phone:" + BusinessPhone + " Release:" + WebID, msg);
                        }
                        else
                        {
                            Succ.AdjusterResponseStuck = true;
                            return Succ;
                        }
                    }
                    else
                    {
                        mAdjResponseDocNumber = adRes.HCAI_Document_Number;
                        mAdjResponseDocNumberCounter = 0;
                    }

                    //CHECK IF OCF EXISTS IN DATABASE
                    if (adRes.PMS_Document_Key.ToString() != "" || adRes.HCAI_Document_Number.ToString() != "" || adRes.Document_Type.ToString() != "")
                    {
                        if (IsOCFExists(mCommand, adRes.HCAI_Document_Number, adRes.Document_Type.ToString(), ConvertStrToLong(adRes.PMS_Document_Key.ToString())) == false)
                        {
                            //EMAIL ME
                            string msg = CreateErrorEmailBody("getAdjusterResponse", adRes.HCAI_Document_Number,
                            "HCAI Number is not found in DB", e.Error_HCAI_ErrorType);
                            Debugger.SendEmail(BusinessName + " phone:" + BusinessPhone + " Release:" + WebID, msg);
                            //if document does not exists, we should stop here ==> send ack for this ocf, and exit procedure
                            return Succ;
                        }
                    }
                    else
                    {
                        return Succ;
                    }

#if LOG_ADRES
                //just for testing
                StreamWriter mFd;
                mFd = File.AppendText("C:\\Program Files\\Antibex\\Universal CPR\\AdResponses.log");
                mFd.WriteLine("Recieved Adjuster Response:");
                mFd.WriteLine("\tTime " + adRes.MessageDate + " " + adRes.MessageTime);
                mFd.WriteLine("\tType " + adRes.Document_Type + " -> HCAI#:" + adRes.HCAI_Document_Number + ";  PMSKey: " + adRes.PMS_Document_Key);
                mFd.WriteLine("===================");
                mFd.WriteLine();
                mFd.Flush();
                mFd.Close();
                //
#endif

                    //START RECEIVING OCF9
                    //creating keys for all the fields in the document

                    var adOCF9Keys = new OCF9AdjusterResponseResponseKeys();
                    var adOCF9LIKeys = new OCF9AdjusterResponseResponseLineItemKeys();

                    adRes.Parent_Document_Number = res.getValue(adOCF9Keys.HCAI_Document_Number); ///?????????????????????
                    adRes.OCF9_OCFVersion = res.getValue(adOCF9Keys.OCFVersion);
                    adRes.OCF9_Header_DateRevised = res.getValue(adOCF9Keys.OCF9_Header_DateRevised);
                    adRes.OCF9_AttachmentsBeingSent = res.getValue(adOCF9Keys.AttachmentsBeingSent);
                    adRes.IsOCF9Present = adRes.Parent_Document_Number != "";

                    if (adRes.IsOCF9Present == true)
                    {
                        //OCF9 EXISTS
                    }
                    else
                    {
                        //OCF9 DOES NOT EXISTS
                    }
                    //getting LineItem List
                    IList liList2 = res.getList(LineItemType.GS9LineItem);
                    if (liList2 == null)
                    {
                        PopulateErrorMessages(e);
                        ParseError(e);
                        //return e;
                        //WRITE ERROR LOG
                        return Succ;
                    }
                    else
                    {
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < liList2.Count; i++)
                        {
                            AdjusterResponseOCF9LineItem li = new AdjusterResponseOCF9LineItem();
                            tmpItem = (IDataItem)liList2[i];
                            li.OCF9_GoodsAndServices_Items_Item_ReferenceNumber = tmpItem.getValue(adOCF9LIKeys.OCF9_GoodsAndServices_Items_Item_ReferenceNumber);
                            li.OCF9_GoodsAndServices_Items_Item_InterestPayable = tmpItem.getValue(adOCF9LIKeys.OCF9_GoodsAndServices_Items_Item_InterestPayable);

                            adRes.OCF9_LineItem = li;
                            adRes.OCF9_LineItem2.Add(li);
                        }
                    }


                    adRes.OCF9_AdditionalComments = res.getValue(adOCF9Keys.AdditionalComments);

                    /// END OCF9

                    //Error Info, in case we have an error later on
                    e.Error_HCAI_Document_Number = adRes.HCAI_Document_Number;
                    e.Error_PMS_Document_Key = adRes.PMS_Document_Key;
                    e.Error_Applicant_FirstName = adRes.Claimant_First_Name;
                    e.Error_Applicant_LastName = adRes.Claimant_Last_Name;

                    adRes.MessageOCFType = adRes.Document_Type;

                    switch (adRes.Document_Type.ToLower())
                    {
                        case "ocf18":
                            {
                                DebugWrite("AdjRes: OCF18");
                                e.Error_OCFType = "OCF18";

                                //creating keys for all the fields in the document
                                var adOCF18Keys = new OCF18AdjusterResponseResponseKeys();
                                var adOCF18SHKeys = new OCF18SessionHeaderAdjusterResponseResponseLineItemKeys();
                                var adOCF18NonSKeys = new OCF18NonSessionAdjusterResponseResponseLineItemKeys();

                                adRes.Insurer_IBCInsurerID = res.getValue(adOCF18Keys.Insurer_IBCInsurerID);
                                adRes.Insurer_IBCBranchID = res.getValue(adOCF18Keys.Insurer_IBCBranchID);

                                //getting Session Header List
                                IList shList = res.getList(LineItemType.GS18SessionHeaderLineItem);
                                if (shList == null)
                                {
                                    DebugWrite("AdjRes: OCF18 Header null");
                                    PopulateErrorMessages(e);

                                    ParseError(e);
                                    //return e;
                                    return Succ;
                                }
                                else
                                {
                                    DebugWrite("AdjRes: OCF18 Header");

                                    int i;
                                    IDataItem tmpItem;
                                    for (i = 0; i < shList.Count; i++)
                                    {
                                        DebugWrite("AdjRes: OCF18 Header1");
                                        var shLineItem = new AdjusterResponseOCF18SessionLineItem();
                                        tmpItem = (IDataItem)shList[i];
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost);
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count);
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost);
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                        shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc = tmpItem.getValue(adOCF18SHKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                        adRes.OCF18_Session_LineItem = shLineItem;
                                    }
                                }

                                //getting NonSession Header List
                                IList nonSList = res.getList(LineItemType.GS18NonSessionLineItem);
                                if (nonSList == null)
                                {
                                    DebugWrite("AdjRes: OCF18 Non Session null");
                                    PopulateErrorMessages(e);

                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }
                                else
                                {
                                    DebugWrite("counter = " + nonSList.Count);

                                    DebugWrite("AdjRes: OCF18 Non Session");
                                    int i;
                                    for (i = 0; i < nonSList.Count; i++)
                                    {
                                        DebugWrite("AdjRes: OCF18 Non Session1");
                                        var nonSLineItem = new AdjusterResponseOCF18NonSessionLineItem();
                                        var tmpItem = (IDataItem)nonSList[i];
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                        nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc = tmpItem.getValue(adOCF18NonSKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                        adRes.OCF18_NonSession_LineItem = nonSLineItem;
                                    }
                                }
                                //added---
                                adRes.OCF18_InsurerSignature_ApplicantSignatureWaived = res.getValue(adOCF18Keys.OCF18_InsurerSignature_ApplicantSignatureWaived);
                                //Totals
                                adRes.OCF18_InsurerTotals_AutoInsurerTotal_Approved = res.getValue(adOCF18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Approved);
                                //MOH
                                adRes.OCF18_InsurerTotals_MOH_Approved_LineCost = res.getValue(adOCF18Keys.OCF18_InsurerTotals_MOH_Approved_LineCost);
                                adRes.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                //Other Insurers
                                adRes.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost = res.getValue(adOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost);
                                adRes.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                //GST
                                adRes.OCF18_InsurerTotals_GST_Approved_LineCost = res.getValue(adOCF18Keys.OCF18_InsurerTotals_GST_Approved_LineCost);
                                adRes.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                //PST
                                adRes.OCF18_InsurerTotals_PST_Approved_LineCost = res.getValue(adOCF18Keys.OCF18_InsurerTotals_PST_Approved_LineCost);
                                adRes.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                adRes.OCF18_InsurerTotals_SubTotal_Approved = res.getValue(adOCF18Keys.OCF18_InsurerTotals_SubTotal_Approved);
                                adRes.OCF18_InsurerTotals_TotalCount_Approved = res.getValue(adOCF18Keys.OCF18_InsurerTotals_TotalCount_Approved);
                                adRes.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation = res.getValue(adOCF18Keys.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation);
                                adRes.NameOfAdjusterOnPDF = res.getValue(adOCF18Keys.NameOfAdjusterOnPDF); //todo - Ver3.21

                                DebugWrite("AdjRes: OCF18 End");

                                //UPDATE DATABASE FOR OCF18
                                dt = DateTime.Now;
                                Succ = UpdateOCF18Reply(mCommand, adRes, "OCF18", dt, dt.ToString("hh:mm:ss tt"));
                                //sendAdjusterResponseAck(adRes.HCAI_Document_Number);
                                //map update
                                break;
                            }
                        case "ocf21b":
                            {
                                DebugWrite("AdjRes: OCF21B");
                                e.Error_OCFType = "OCF21B";

                                //creating keys for all the fields in the document
                                var adOCF21BKeys = new OCF21BAdjusterResponseResponseKeys();
                                var adOCF21BLIKeys = new OCF21BAdjusterResponseResponseLineItemKeys();

                                adRes.HCAI_Plan_Document_Number = res.getValue(adOCF21BKeys.HCAI_Document_Number);
                                adRes.Insurer_IBCBranchID = res.getValue(adOCF21BKeys.Insurer_IBCBranchID);
                                adRes.Insurer_IBCInsurerID = res.getValue(adOCF21BKeys.Insurer_IBCInsurerID);

                                //getting LineItem List
                                IList liList = res.getList(LineItemType.GS21BLineItem);
                                if (liList == null)
                                {
                                    PopulateErrorMessages(e);

                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }

                                int i;
                                for (i = 0; i < liList.Count; i++)
                                {
                                    var li = new AdjusterResponseOCF21BLineItem();
                                    var tmpItem = (IDataItem)liList[i];

                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST);
                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST);
                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost);
                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                    li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(adOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                    adRes.OCF21B_LineItem = li;
                                }
                                adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost);
                                adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                adRes.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost);
                                adRes.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation = res.getValue(adOCF21BKeys.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation);
                                //todo - Ver3.13
                                adRes.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation = res.getValue(adOCF21BKeys.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation);
                                //todo - Ver3.13

                                adRes.OCF21B_InsurerTotals_MOH_Approved = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_MOH_Approved);
                                adRes.OCF21B_InsurerTotals_OtherInsurers_Approved = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_OtherInsurers_Approved);
                                adRes.OCF21B_InsurerTotals_AutoInsurerTotal_Approved = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Approved);
                                adRes.OCF21B_InsurerTotals_GST_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_LineCost);

                                adRes.OCF21B_InsurerTotals_GST_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_LineCost);
                                adRes.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21B_InsurerTotals_PST_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_LineCost);
                                adRes.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                adRes.OCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_SubTotal_Approved);
                                //deleted---						adRes.OCF21B_InsurerTotals_Interest_Approved = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved);
                                adRes.OCF21B_InsurerTotals_Interest_Approved_LineCost = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_LineCost);
                                adRes.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                adRes.OCF21B_InsurerSignature_ClaimFormReceivedDate = res.getValue(adOCF21BKeys.OCF21B_InsurerSignature_ClaimFormReceivedDate); //todo - Ver3.13
                                adRes.OCF21B_InsurerSignature_ClaimFormReceived = res.getValue(adOCF21BKeys.OCF21B_InsurerSignature_ClaimFormReceived); //todo - Ver3.13

                                adRes.ApprovedByOnPDF = res.getValue(adOCF21BKeys.ApprovedByOnPDF); //todo - Ver3.21

                                //UPDATE DATABASE FOR OCF21B
                                dt = DateTime.Now;
                                Succ = UpdateOCF21BReply(mCommand, adRes, "OCF21B", dt, dt.ToString("hh:mm:ss tt"));
                                //sendAdjusterResponseAck(adRes.HCAI_Document_Number);

                                break;
                            }
                        case "ocf21c":
                            {
                                DebugWrite("AdjRes: OCF21C");
                                e.Error_OCFType = "OCF21C";

                                //creating keys for all the fields in the document
                                var adOCF21CKeys = new OCF21CAdjusterResponseResponseKeys();
                                var adOCF21CPAFKeys = new OCF21CPAFReimbursableAdjusterResponseResponseLineItemKeys();
                                var adOCF21COtherKeys = new OCF21COtherReimbursableAdjusterResponseResponseLineItemKeys();

                                adRes.HCAI_OCF23_Document_Number = res.getValue(adOCF21CKeys.HCAI_Document_Number); //???
                                adRes.Insurer_IBCInsurerID = res.getValue(adOCF21CKeys.Insurer_IBCInsurerID);
                                adRes.Insurer_IBCBranchID = res.getValue(adOCF21CKeys.Insurer_IBCBranchID);

                                //getting PAFReimbursableLineItem List
                                IList pafList = res.getList(PMSToolkit.LineItemType.GS21CPAFReimbursableLineItem);
                                if (pafList == null)
                                {
                                    PopulateErrorMessages(e);
                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }

                                int i;
                                IDataItem tmpItem;
                                for (i = 0; i < pafList.Count; i++)
                                {
                                    var li = new AdjusterResponseOCF21CPAFReimbursableLineItem();
                                    tmpItem = (IDataItem)pafList[i];

                                    li.OCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey = tmpItem.getValue(adOCF21CPAFKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey);
                                    li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost = tmpItem.getValue(adOCF21CPAFKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost);
                                    li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode = tmpItem.getValue(adOCF21CPAFKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                    li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc = tmpItem.getValue(adOCF21CPAFKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                    li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode = tmpItem.getValue(adOCF21CPAFKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                    li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(adOCF21CPAFKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                    //added after update            
                                    adRes.OCF21C_PAFReimbursable_LineItem = li;
                                }

                                //getting OtherReimbursableLineItem List
                                IList otherList = res.getList(PMSToolkit.LineItemType.GS21COtherReimbursableLineItem);
                                if (otherList == null)
                                {
                                    PopulateErrorMessages(e);
                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }

                                for (i = 0; i < otherList.Count; i++)
                                {
                                    var li = new AdjusterResponseOCF21COtherReimbursableLineItem();
                                    tmpItem = (IDataItem)otherList[i];

                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST);
                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST);
                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost);
                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                    li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(adOCF21COtherKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                    adRes.OCF21C_OtherReimbursable_LineItem = li;
                                }
                                adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost);
                                adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                adRes.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost);
                                adRes.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation = res.getValue(adOCF21CKeys.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation);
                                //todo - Ver3.13
                                adRes.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation = res.getValue(adOCF21CKeys.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation);
                                //todo - Ver3.13

                                adRes.OCF21C_InsurerTotals_MOH_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_MOH_Approved);
                                adRes.OCF21C_InsurerTotals_OtherInsurers_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_OtherInsurers_Approved);
                                adRes.OCF21C_InsurerTotals_AutoInsurerTotal_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Approved);
                                //deleted---					adRes.OCF21C_InsurerTotals_GST_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_GST_Approved);
                                adRes.OCF21C_InsurerTotals_GST_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_LineCost);
                                adRes.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                //deleted---						adRes.OCF21C_InsurerTotals_PST_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_PST_Approved);
                                adRes.OCF21C_InsurerTotals_PST_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_LineCost);
                                adRes.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                //deleted---						adRes.OCF21C_InsurerTotals_Interest_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved);
                                adRes.OCF21C_InsurerTotals_Interest_Approved_LineCost = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_LineCost);
                                adRes.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                adRes.OCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved);
                                adRes.OCF21C_InsurerTotals_SubTotalPreApproved_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Approved);

                                adRes.OCF21C_InsurerSignature_ClaimFormReceivedDate = res.getValue(adOCF21CKeys.OCF21C_InsurerSignature_ClaimFormReceivedDate); //todo - Ver3.13
                                adRes.OCF21C_InsurerSignature_ClaimFormReceived = res.getValue(adOCF21CKeys.OCF21C_InsurerSignature_ClaimFormReceived); //todo - Ver3.13
                                adRes.ApprovedByOnPDF = res.getValue(adOCF21CKeys.ApprovedByOnPDF); //todo - Ver3.21

                                //UPDATE DATABASE FOR OCF18
                                dt = DateTime.Now;
                                Succ = UpdateOCF21CReply(mCommand, adRes, "OCF21C", dt, dt.ToString("hh:mm:ss tt"));
                                //                            sendAdjusterResponseAck(adRes.HCAI_Document_Number);

                                break;
                            }
                        case "ocf22":
                            {
                                DebugWrite("AdjRes: OCF22");
                                e.Error_OCFType = "OCF22";

                                //creating keys for all the fields in the document
                                var adOCF22Keys = new OCF22AdjusterResponseResponseKeys();
                                var adOCF22LIKeys = new OCF22AdjusterResponseResponseLineItemKeys();

                                adRes.OCF22_Insurer_IBCInsurerID = res.getValue(adOCF22Keys.Insurer_IBCInsurerID); //???
                                adRes.OCF22_Insurer_IBCBranchID = res.getValue(adOCF22Keys.Insurer_IBCBranchID); //???
                                                                                                                 //added---
                                adRes.OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18 =
                                    res.getValue(adOCF22Keys.OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18);

                                //getting LineItem List
                                IList liList = res.getList(LineItemType.GS22LineItem);
                                if (liList == null)
                                {
                                    PopulateErrorMessages(e);
                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }

                                int i;
                                for (i = 0; i < liList.Count; i++)
                                {
                                    var li = new AdjusterResponseOCF22LineItem();
                                    var tmpItem = (IDataItem)liList[i];

                                    li.OCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey =
                                        tmpItem.getValue(
                                            adOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PMSGSKey);
                                    li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST =
                                        tmpItem.getValue(
                                            adOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST);
                                    li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST =
                                        tmpItem.getValue(
                                            adOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST);
                                    li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost =
                                        tmpItem.getValue(
                                            adOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost);
                                    li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc =
                                        tmpItem.getValue(
                                            adOCF22LIKeys
                                                .OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                    li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup =
                                        tmpItem.getValue(
                                            adOCF22LIKeys
                                                .OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                    li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc =
                                        tmpItem.getValue(
                                            adOCF22LIKeys
                                                .OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                    adRes.OCF22_LineItem = li;
                                }
                                adRes.OCF22_InsurerTotals_AutoInsurerTotal_Approved =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_AutoInsurerTotal_Approved);
                                adRes.OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_SubTotal_Approved);
                                //deleted---						adRes.OCF22_InsurerTotals_GST_Approved = res.getValue(adOCF22Keys.OCF22_InsurerTotals_GST_Approved);
                                adRes.OCF22_InsurerTotals_GST_Approved_LineCost =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_GST_Approved_LineCost);
                                adRes.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                    res.getValue(
                                        adOCF22Keys.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                //deleted---						adRes.OCF22_InsurerTotals_PST_Approved = res.getValue(adOCF22Keys.OCF22_InsurerTotals_PST_Approved);
                                adRes.OCF22_InsurerTotals_PST_Approved_LineCost =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_PST_Approved_LineCost);
                                adRes.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                    res.getValue(
                                        adOCF22Keys.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF22_InsurerTotals_MOH_Approved_LineCost =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_MOH_Approved_LineCost);
                                adRes.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                    res.getValue(
                                        adOCF22Keys.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                                adRes.OCF22_InsurerTotals_OtherInsurers_Approved_LineCost =
                                    res.getValue(adOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_LineCost);
                                adRes.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode =
                                    res.getValue(
                                        adOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode);
                                adRes.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc =
                                    res.getValue(
                                        adOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                adRes.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                    res.getValue(
                                        adOCF22Keys
                                            .OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                //adRes.NameOfAdjusterOnPDF = res.getValue(adOCF22Keys.NameOfAdjusterOnPDF); //todo - Ver3.21

                                //UPDATE DATABASE FOR OCF22
                                dt = DateTime.Now;
                                Succ = UpdateOCF22Reply(mCommand, adRes, "OCF22", dt, dt.ToString("hh:mm:ss tt"));
                                //                            sendAdjusterResponseAck(adRes.HCAI_Document_Number);
                                break;
                            }
                        case "ocf23":
                            {
                                DebugWrite("AdjRes: OCF23");
                                e.Error_OCFType = "OCF23";

                                //creating keys for all the fields in the document
                                var adOCF23Keys = new OCF23AdjusterResponseResponseKeys();
                                var adOCF23LIKeys = new OCF23AdjusterResponseResponseLineItemKeys();

                                adRes.Insurer_IBCInsurerID = res.getValue(adOCF23Keys.Insurer_IBCInsurerID);
                                adRes.Insurer_IBCBranchID = res.getValue(adOCF23Keys.Insurer_IBCBranchID);

                                //getting LineItem List
                                IList liList = res.getList(LineItemType.GS23OtherGSLineItem);
                                if (liList == null)
                                {
                                    PopulateErrorMessages(e);
                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }
                                else
                                {
                                    int i;
                                    IDataItem tmpItem;
                                    for (i = 0; i < liList.Count; i++)
                                    {
                                        var li = new AdjusterResponseOCF23OtherGSLineItem();
                                        tmpItem = (IDataItem)liList[i];

                                        li.OCF23_OtherGoodsandServices_Items_Item_PMSGSKey =
                                            tmpItem.getValue(
                                                adOCF23LIKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey);
                                        li.OCF23_OtherGoodsandServices_Items_Approved_LineCost =
                                            tmpItem.getValue(
                                                adOCF23LIKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost);
                                        li.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode =
                                            tmpItem.getValue(
                                                adOCF23LIKeys
                                                    .OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                                        li.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc =
                                            tmpItem.getValue(
                                                adOCF23LIKeys
                                                    .OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                                        li.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                            tmpItem.getValue(
                                                adOCF23LIKeys
                                                    .OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                                        adRes.OCF23_OtherGS_LineItem = li;
                                    }
                                }

                                adRes.OCF23_OtherGoodsAndServices_AdjusterResponseExplanation = res.getValue(adOCF23Keys.OCF23_OtherGoodsAndServices_AdjusterResponseExplanation);
                                adRes.OCF23_InsurerSignature_ApplicantSignatureWaived = res.getValue(adOCF23Keys.OCF23_InsurerSignature_ApplicantSignatureWaived);
                                adRes.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices = res.getValue(adOCF23Keys.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices);
                                adRes.OCF23_InsurerSignature_PolicyInForceConfirmation = res.getValue(adOCF23Keys.OCF23_InsurerSignature_PolicyInForceConfirmation);

                                adRes.OCF23_InsurerTotals_AutoInsurerTotal_Approved = res.getValue(adOCF23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Approved);
                                adRes.OCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved = res.getValue(adOCF23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved);
                                adRes.OCF23_InsurerTotals_SubTotalPreApproved_Approved = res.getValue(adOCF23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Approved);
                                adRes.NameOfAdjusterOnPDF = res.getValue(adOCF23Keys.NameOfAdjusterOnPDF); //todo - Ver3.21

                                //UPDATE DATABASE FOR OCF23
                                dt = DateTime.Now;
                                Succ = UpdateOCF23Reply(mCommand, adRes, "OCF23", dt, dt.ToString("hh:mm:ss tt"));
                                //sendAdjusterResponseAck(adRes.HCAI_Document_Number);
                                break;
                            }
                        case "ocf9":
                            {
                                e.Error_OCFType = "OCF9";
                                DebugWrite("AdjRes: OCF9");
                                break;
                            }
                        case "form1":
                            {
                                DebugWrite("AdjRes: Form1");
                                var form1AdjKeys = new AACNAdjusterResponseKeys();
                                var form1AdjLineItemKeys = new AACNAdjusterResponseLineItemKeys();

                                e.Error_OCFType = "Form1";

                                adRes.Form1_ArchivalStatus = res.getValue(form1AdjKeys.ArchivalStatus);
                                adRes.Form1_BranchVersion = res.getValue(form1AdjKeys.BranchVersion);
                                adRes.Form1_Costs_Approved_AdjusterResponseExplanation = res.getValue(form1AdjKeys.Costs_Approved_AdjusterResponseExplanation);
                                adRes.Form1_Costs_Approved_Benefit = res.getValue(form1AdjKeys.Costs_Approved_Benefit);
                                adRes.Form1_Costs_Approved_CalculatedBenefit = res.getValue(form1AdjKeys.Costs_Approved_CalculatedBenefit);
                                adRes.Form1_Costs_Approved_ReasonCode = res.getValue(form1AdjKeys.Costs_Approved_ReasonCode);
                                adRes.Form1_Costs_Approved_ReasonDescription = res.getValue(form1AdjKeys.Costs_Approved_ReasonDescription);
                                adRes.Form1_EOB_EOBAdditionalComments = res.getValue(form1AdjKeys.EOB_EOBAdditionalComments);
                                adRes.Form1_FacilityVersion = res.getValue(form1AdjKeys.FacilityVersion);
                                adRes.Form1_InsurerVersion = res.getValue(form1AdjKeys.InsurerVersion);
                                adRes.Insurer_IBCInsurerID = res.getValue(form1AdjKeys.Insurer_IBCInsurerID);
                                adRes.Insurer_IBCBranchID = res.getValue(form1AdjKeys.Insurer_IBCBranchID);
                                adRes.Form1_AttachmentsReceivedDate = res.getValue(form1AdjKeys.AttachmentsReceivedDate);
                                adRes.Form1_DocumentVersion = res.getValue(form1AdjKeys.DocumentVersion);
                                adRes.NameOfAdjusterOnPDF = res.getValue(form1AdjKeys.NameOfAdjusterOnPDF); //todo - Ver3.21

                                IList liList = res.getList(LineItemType.AACNServicesLineItem);
                                if (liList == null)
                                {
                                    PopulateErrorMessages(e);
                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }

                                for (int i = 0; i < liList.Count; i++)
                                {
                                    var li = new AdjusterResponseForm1AcsLineItem();
                                    var tmpItem = (IDataItem)liList[i];
                                    li.AttendantCareServices_Item_ACSItemID = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_ACSItemID);
                                    li.AttendantCareServices_Item_Approved_IsItemDeclined = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Approved_IsItemDeclined);
                                    li.AttendantCareServices_Item_Approved_Minutes = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Approved_Minutes);
                                    li.AttendantCareServices_Item_Approved_ReasonCode = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Approved_ReasonCode);
                                    li.AttendantCareServices_Item_Approved_ReasonDescription = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Approved_ReasonDescription);
                                    li.AttendantCareServices_Item_Approved_TimesPerWeek = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Approved_TimesPerWeek);
                                    li.AttendantCareServices_Item_Approved_TotalMinutes = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Approved_TotalMinutes);
                                    li.AttendantCareServices_Item_Description = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_Description);

                                    adRes.Form1AcsLineItem.Add(li);
                                }

                                liList = res.getList(LineItemType.AACNApprovedPart);
                                if (liList == null)
                                {
                                    PopulateErrorMessages(e);
                                    ParseError(e);
                                    //return e;
                                    //WRITE ERROR LOG
                                    return Succ;
                                }
                                for (int i = 0; i < liList.Count; i++)
                                {
                                    var li = new AdjusterResponseForm1ApprovedCostLineItem();
                                    var tmpItem = (IDataItem)liList[i];
                                    li.Costs_Approved_Part_ACSItemID = tmpItem.getValue(form1AdjLineItemKeys.AttendantCareServices_Item_ACSItemID);
                                    li.Costs_Approved_Part_Benefit = tmpItem.getValue(form1AdjLineItemKeys.Costs_Approved_Part_Benefit);
                                    li.Costs_Approved_Part_HourlyRate = tmpItem.getValue(form1AdjLineItemKeys.Costs_Approved_Part_HourlyRate);
                                    li.Costs_Approved_Part_WeeklyHours = tmpItem.getValue(form1AdjLineItemKeys.Costs_Approved_Part_WeeklyHours);
                                    li.Costs_Approved_Part_MonthlyHours = tmpItem.getValue(form1AdjLineItemKeys.Costs_Approved_Part_MonthlyHours);
                                    li.Costs_Approved_Part_ReasonCode = tmpItem.getValue(form1AdjLineItemKeys.Costs_Approved_Part_ReasonCode);
                                    li.Costs_Approved_Part_ReasonDescription = tmpItem.getValue(form1AdjLineItemKeys.Costs_Approved_Part_ReasonDescription);

                                    adRes.Form1ApprovedCostLineItem.Add(li);
                                }

                                //UPDATE DATABASE FOR From1
                                dt = DateTime.Now;
                                Succ = UpdateForm1Reply(mCommand, adRes, "Form1", dt, dt.ToString("hh:mm:ss tt"));
                                break;
                            }
                        default:
                            break;
                    }

                    //return adRes; 
                    //WRITE ERROR LOG
                    return Succ;
                }
                catch (Exception ex)
                {
                    UOServiceLib.Log.Error("Adjuster response error: " + ex.Message);
                    e.MessageOCFType = adRes.Document_Type;
                    e.Error_HCAI_Document_Number = adRes.HCAI_Document_Number;
                    e.Error_PMS_Document_Key = adRes.PMS_Document_Key;
                    e.Error_PlanNumber = adRes.PMS_Document_Key;
                    e.Error_HCAI_ErrorType = "903:?:Exception occurred!";

                    //EMAIL ME
                    string msg = CreateErrorEmailBody("getAdjusterResponse", adRes.HCAI_Document_Number, "", e.Error_HCAI_ErrorType);
                    Debugger.SendEmail(BusinessName + " phone:" + BusinessPhone + " Release:" + WebID, msg);
                    //return e;
                    return Succ;
                }
        }

        private void GetCommonKeys(IDataItem pmsAdjusterResponse, AdjusterResponse localAdjusterResponse)
        {
            var ocfAdjResponseKeys = new AdjusterResponseResponseKeys();
            var form1AdjResponseKeys = new AACNAdjusterResponseKeys();
            //Common Keys
            localAdjusterResponse.HCAI_Document_Number = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.HCAI_Document_Number);
            localAdjusterResponse.PMS_Document_Key = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.PMSFields_PMSDocumentKey);
            localAdjusterResponse.Document_Type = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Document_Type);
            if(string.IsNullOrEmpty(localAdjusterResponse.HCAI_Document_Number))
            {
                //Form1 (AACN)
                localAdjusterResponse.HCAI_Document_Number = pmsAdjusterResponse.getValue(form1AdjResponseKeys.DocumentNumber);
                localAdjusterResponse.PMS_Document_Key = pmsAdjusterResponse.getValue(form1AdjResponseKeys.PMSFields_PMSDocumentKey);
                localAdjusterResponse.Document_Type = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Document_Type);
                localAdjusterResponse.Document_Type = localAdjusterResponse.Document_Type.ToLower() == "aacn"
                                                          ? "Form1"
                                                          : localAdjusterResponse.Document_Type;

                localAdjusterResponse.Submission_Date = pmsAdjusterResponse.getValue(form1AdjResponseKeys.SubmissionTime);
                localAdjusterResponse.Submission_Source = pmsAdjusterResponse.getValue(form1AdjResponseKeys.SubmissionSource);
                localAdjusterResponse.Adjuster_Response_Date = pmsAdjusterResponse.getValue(form1AdjResponseKeys.AdjusterResponseTime);
                localAdjusterResponse.In_Dispute = pmsAdjusterResponse.getValue(form1AdjResponseKeys.InDispute);
                localAdjusterResponse.Document_Status = pmsAdjusterResponse.getValue(form1AdjResponseKeys.DocumentState);
                localAdjusterResponse.SigningAdjuster_FirstName = pmsAdjusterResponse.getValue(form1AdjResponseKeys.SigningAdjusterName_FirstName);
                localAdjusterResponse.SigningAdjuster_LastName = pmsAdjusterResponse.getValue(form1AdjResponseKeys.SigningAdjusterName_LastName);
                localAdjusterResponse.Claimant_First_Name = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Name_FirstName);
                localAdjusterResponse.Claimant_Last_Name = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Name_LastName);
                localAdjusterResponse.Claimant_Middle_Name = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Name_MiddleName);
                localAdjusterResponse.Claimant_Date_Of_Birth = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_DateOfBirth);
                localAdjusterResponse.Claimant_Gender = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Gender);
                localAdjusterResponse.Claimant_Telephone = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Telephone);
                localAdjusterResponse.Claimant_Extension = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Extension);
                localAdjusterResponse.Claimant_Address1 = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Address_StreetAddress1);
                localAdjusterResponse.Claimant_Address2 = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Address_StreetAddress2);
                localAdjusterResponse.Claimant_City = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Address_City);
                localAdjusterResponse.Claimant_Province = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Address_Province);
                localAdjusterResponse.Claimant_Postal_Code = pmsAdjusterResponse.getValue(form1AdjResponseKeys.Claimant_Address_PostalCode);
            }
            else
            {
                //All OCFs
                localAdjusterResponse.Submission_Date = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Submission_Date);
                localAdjusterResponse.Submission_Source = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Submission_Source);
                localAdjusterResponse.Adjuster_Response_Date = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Adjuster_Response_Date);
                localAdjusterResponse.In_Dispute = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.In_Dispute);
                localAdjusterResponse.Document_Status = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Document_Status);
                localAdjusterResponse.SigningAdjuster_FirstName = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.SigningAdjuster_FirstName);
                localAdjusterResponse.SigningAdjuster_LastName = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.SigningAdjuster_LastName);
                localAdjusterResponse.Claimant_First_Name = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_First_Name);
                localAdjusterResponse.Claimant_Last_Name = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Last_Name);
                localAdjusterResponse.Claimant_Middle_Name = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Middle_Name);
                localAdjusterResponse.Claimant_Date_Of_Birth = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Date_Of_Birth);
                localAdjusterResponse.Claimant_Gender = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Gender);
                localAdjusterResponse.Claimant_Telephone = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Telephone);
                localAdjusterResponse.Claimant_Extension = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Extension);
                localAdjusterResponse.Claimant_Address1 = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Address1);
                localAdjusterResponse.Claimant_Address2 = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Address2);
                localAdjusterResponse.Claimant_City = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_City);
                localAdjusterResponse.Claimant_Province = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Province);
                localAdjusterResponse.Claimant_Postal_Code = pmsAdjusterResponse.getValue(ocfAdjResponseKeys.Claimant_Postal_Code);
            }
        }

        public IDataType sendAdjusterResponseAck(string hcaiDocumentNumber)
        {
            //return null;
            /**/
            ErrorMessage e = new ErrorMessage();
            e.MessageType = mType.Error;
            e.Error_ErrorType = mType.AdjusterResponseAcknowledged;

            DateTime dt = DateTime.Now;
            e.MessageDate = dt.ToString("MM/dd/yyyy");
            e.MessageTime = dt.ToString("hh:mm:ss tt");

            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. sendAdjusterResponseAck aborted.");
                e.Error_CompleteErrorDesc = "Hcai credentials are empty.";
                return e;
            }
            //return e; //TO BE REMOVED TEMP

#if LOG_ADRES
            //just for testing
            StreamWriter mFd;
            mFd = File.AppendText("C:\\Program Files\\Antibex\\Universal CPR\\AdResponses.log");
            mFd.WriteLine("Sending Adjuster Response Acknowledgement:");
            mFd.WriteLine("\tTime " + e.MessageDate + " " + e.MessageTime);
            mFd.WriteLine("\tHCAI#: " + d.HCAI_Document_Number);
            //
#endif

            try
            {
                IDataItem res = pmsTK.sendAdjusterResponseAck(mHCAILogin, mHCAIPassword, hcaiDocumentNumber);
                if (res == null)
                {
#if LOG_ADRES
                    mFd.WriteLine("FAILED");
                    mFd.WriteLine("===================");
                    mFd.WriteLine();
                    mFd.Flush();
                    mFd.Close();
                    //
#endif
                    e.Error_HCAI_Document_Number = hcaiDocumentNumber;
                    PopulateErrorMessages(e);
                    ParseError(e);
                    return e;
                }

#if LOG_ADRES
                mFd.WriteLine("SUCCESS");
                mFd.WriteLine("===================");
                mFd.WriteLine();
                mFd.Flush();
                mFd.Close();
                //
#endif
                // getAdjusterResponse();
                return null;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("Adj Ack error: " + ex.Message, ex);
                //WRITE ERROR LOG
                e.Error_HCAI_Document_Number = hcaiDocumentNumber;
                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";
                string msg = CreateErrorEmailBody("sendAdjusterResponseAck", hcaiDocumentNumber,
                    "", e.Error_HCAI_ErrorType);
                Debugger.SendEmail(BusinessName + " phone:" + BusinessPhone + " Release:" + WebID, msg);
                return e;
            }

            /**/

        }

        //WITHDRAW DOCUMENT
        public OCFMapReturnType VoidDocumentRequest(string OCFID, string OCFType, string HCAI_DocumentNumber)
        {
            var result = new OCFMapReturnType();

            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. VoidDocumentRequest aborted.");
                result.OCFID = ConvertStrToLong(OCFID);
                result.OCFType = OCFType;
                result.New_HCAI_Status = HcaiStatus.WithdrawRequestFailed;
                return result;
            }

            DateTime dt = DateTime.Now;

            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                result.OCFID = ConvertStrToLong(OCFID);
                result.OCFType = OCFType;
                result.New_HCAI_Status = HcaiStatus.InformationError;
                return result;
            }

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            try
            {
                //this should not happen   
                if (HCAI_DocumentNumber == "")
                {
                    //EMAIL ME
                    string msg = CreateErrorEmailBody("VoidDocument", HCAI_DocumentNumber, "Empty HCAI Number from DB", "");
                    Debugger.SendEmail(BusinessName + " phone:" + BusinessPhone + " Release:" + WebID, msg);

                    //Succ.HCAI_Document_Number = "";
                    //Succ.OCFID = 0;
                    //Succ.OCFType = "";
                    //Succ.New_HCAI_Status = -1;

                    ReceiveHCAIWithdrawResult(mCommand, ConvertStrToLong(OCFID), OCFType, dt, dt.ToString("hh:mm:ss tt"), 8, HCAI_DocumentNumber);
                    result.HCAI_Document_Number = HCAI_DocumentNumber;
                    result.OCFID = ConvertStrToLong(OCFID);
                    result.OCFType = OCFType;
                    result.New_HCAI_Status = HcaiStatus.WithdrawRequestFailed;

                    return result;
                }

                var e = new ErrorMessage
                {
                    MessageType = mType.Error,
                    Error_ErrorType = mType.VoidDocumentResponse,
                    MessageDate = dt.ToString("MM/dd/yyyy"),
                    MessageTime = dt.ToString("hh:mm tt"),
                    Error_HCAI_Document_Number = HCAI_DocumentNumber,
                    Error_OCFType = OCFType,
                    Error_HCAI_ErrorType = "100:?:Withdraw Document functionality has been discontinued"
                };

                //Withdraw Information error 
                DebugWrite("Returning Void response error");

                result.HCAI_Document_Number = HCAI_DocumentNumber;
                result.OCFID = ConvertStrToLong(OCFID);
                result.OCFType = OCFType;
                result.New_HCAI_Status = HcaiStatus.WithdrawDenied;

                return result;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("Void Document error: " + ex.Message, ex);
                //System Error
                var e = new ErrorMessage
                {
                    MessageType = mType.Error,
                    Error_ErrorType = mType.VoidDocumentResponse,
                    MessageDate = dt.ToString("MM/dd/yyyy"),
                    MessageTime = dt.ToString("hh:mm:ss tt"),
                    Error_HCAI_Document_Number = HCAI_DocumentNumber,
                    Error_OCFType = OCFType,
                    Error_HCAI_ErrorType = "903:?:Exception occurred!"
                };
                //DateTime dt = DateTime.Now;


                //WRITE ERROR LOG
                ReceiveHCAIWithdrawResult(mCommand, ConvertStrToLong(OCFID), OCFType, dt, dt.ToString("hh:mm:ss tt"), 8, HCAI_DocumentNumber);
                result.HCAI_Document_Number = HCAI_DocumentNumber;
                result.OCFID = ConvertStrToLong(OCFID);
                result.OCFType = OCFType;
                result.New_HCAI_Status = HcaiStatus.WithdrawRequestFailed;

                return result;
            }
        }


        public Batch GetActivityList_ToFindPendingOCFS_ByPeriods(DateTime FDate, DateTime TDate)
        {
            var bt = new Batch();
            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. GetActivityList_ToFindPendingOCFS_ByPeriods aborted.");
                return bt;
            }

            //connect to DB
            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                return bt;
            }

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            try
            {
                var res = new ActivityList();
                res.MessageType = mType.ActivityListRecieved;

                var e = new ErrorMessage();
                e.MessageType = mType.Error;
                e.Error_ErrorType = mType.ActivityListRecieved;
                
                IDataItem actLst = pmsTK.activityList(mHCAILogin, mHCAIPassword, FDate, TDate);

                var dt = DateTime.Now;
                res.MessageDate = dt.ToString("MM/dd/yyyy");
                res.MessageTime = dt.ToString("hh:mm:ss tt");

                IDataItem item;
                if (actLst == null)
                {
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");

                    PopulateErrorMessages(e);
                    ParseError(e);
                    return bt;
                }
                else
                {
                    //System.Console.WriteLine(actLst.isValidLineItemType(PMSToolkit.LineItemType.ActivityLineItem));

                    IList lst = actLst.getList(PMSToolkit.LineItemType.ActivityLineItem);

                    if (lst == null)
                    {
                        e.MessageDate = dt.ToString("MM/dd/yyyy");
                        e.MessageTime = dt.ToString("hh:mm:ss tt");

                        PopulateErrorMessages(e);
                        ParseError(e);

                        //WRITE ERROR LOG
                        return bt;
                    }

                    res.Count = lst.Count;
                    PMSToolkit.DataObjects.ActivityLineItemKeys actKeys = new PMSToolkit.DataObjects.ActivityLineItemKeys();
                    Activity act;

                    //PENDING STARTS HERE
                    DebugWrite("PENDING STARTS HERE");

                    for (int i = 0; i < lst.Count; i++)
                    {
                        item = (IDataItem) lst[i];
                        //not needed for now == to be deleted
                        act = new Activity
                        {
                            HCAI_Document_Number = item.getValue(actKeys.HCAI_Document_Number),
                            Document_Status = item.getValue(actKeys.Document_Status),
                            PMS_Document_Key = item.getValue(actKeys.PMSFields_PMSDocumentKey),
                            PMS_Patient_Key = item.getValue(actKeys.PMSFields_PMSPatientKey),
                            Submission_Date = item.getValue(actKeys.Submission_Date),
                            Submission_Source = item.getValue(actKeys.Submission_Source)
                        };

                        act.PMS_Document_Key = GetUODocumentID_ByHCAINumber(mCommand, "",
                                                 act.HCAI_Document_Number, ConvertStrToLong(act.PMS_Document_Key)).ToString();
                        res.Activities = act;

                        ////////////////////////////////////////////////////////////////////
                        //check if document is in pending status
                        if (act.HCAI_Document_Number != "" && ConvertStrToLong(act.PMS_Document_Key) > 0)
                        {
                            BatchItem it = CheckAndFixOCF(act, mCommand);
                            if (it != null)
                            {
                                bt.AddItem(it);
                            }
                        }
                    }
                    //DebugWrite("PENDING ENDS HERE");
                    return bt;
                }
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("Activity List error: " + ex.Message, ex);
                var e = new ErrorMessage {MessageType = mType.Error};
                var dt = DateTime.Now;
                e.MessageDate = dt.ToString("MM/dd/yyyy");
                e.MessageTime = dt.ToString("hh:mm:ss tt");

                e.Error_ErrorType = mType.RequestActivityList;
                e.Error_HCAI_ErrorType = "903:?:Exception occurred!";

                //WRITE TO ERROR LOG
                return bt;
            }
        }

        //ORIGINAL GET ACTIVITY LIST

        //public IDataType getActivityList(DateTime startDate, DateTime endDate)
        //{
        //    try
        //    {
        //        ActivityList res = new ActivityList();
        //        res.MessageType = mType.ActivityListRecieved;

        //        ErrorMessage e = new ErrorMessage();
        //        e.MessageType = mType.Error;
        //        e.Error_ErrorType = mType.ActivityListRecieved;

        //        if (!IsCredentialsValid())
        //        {
        //            DebugWrite("Hcai credentials are empty. getActivityList aborted.");
        //            e.Error_CompleteErrorDesc = "Hcai credentials are empty.";
        //            return e;
        //        }
        //        //string[] sd = startDate.Split('/');
        //        //string[] ed = endDate.Split('/');
        //        //IDataItem actLst = pmsTK.activityList(mHCAILogin, mHCAIPassword,
        //        //    new DateTime(Convert.ToInt32(sd[2]), Convert.ToInt32(sd[0]), Convert.ToInt32(sd[1])),
        //        //    new DateTime(Convert.ToInt32(ed[2]), Convert.ToInt32(ed[0]), Convert.ToInt32(ed[1])));

        //        IDataItem actLst = pmsTK.activityList(mHCAILogin, mHCAIPassword, startDate, endDate);

        //        DateTime dt = DateTime.Now;
        //        res.MessageDate = dt.ToString("MM/dd/yyyy");
        //        res.MessageTime = dt.ToString("hh:mm:ss tt");

        //        IDataItem item;
        //        if (actLst == null)
        //        {
        //            e.MessageDate = dt.ToString("MM/dd/yyyy");
        //            e.MessageTime = dt.ToString("hh:mm:ss tt");

        //            PopulateErrorMessages(e);
        //            ParseError(e);

        //            return e;
        //        }
        //        else
        //        {
        //            //					System.Console.WriteLine(actLst.isValidLineItemType(PMSToolkit.LineItemType.ActivityLineItem));

        //            IList lst = actLst.getList(PMSToolkit.LineItemType.ActivityLineItem);

        //            if (lst == null)
        //            {
        //                e.MessageDate = dt.ToString("MM/dd/yyyy");
        //                e.MessageTime = dt.ToString("hh:mm:ss tt");

        //                PopulateErrorMessages(e);
        //                ParseError(e);
        //                return e;
        //            }

        //            res.Count = lst.Count;
        //            PMSToolkit.DataObjects.ActivityLineItemKeys actKeys = new PMSToolkit.DataObjects.ActivityLineItemKeys();
        //            Activity act;

        //            for (int i = 0; i < lst.Count; i++)
        //            {
        //                item = (IDataItem)lst[i];
        //                act = new Activity();
        //                act.HCAI_Document_Number = item.getValue(actKeys.HCAI_Document_Number);
        //                act.Document_Status = item.getValue(actKeys.Document_Status);
        //                act.PMS_Document_Key = item.getValue(actKeys.PMSFields_PMSDocumentKey);
        //                act.PMS_Patient_Key = item.getValue(actKeys.PMSFields_PMSPatientKey);
        //                act.Submission_Date = item.getValue(actKeys.Submission_Date);
        //                act.Submission_Source = item.getValue(actKeys.Submission_Source);
        //                res.Activities = act;
        //                //Console.WriteLine(act.HCAI_Document_Number + ": " + act.Submission_Source + ", " + act.Document_Status);
        //            }

        //            return res;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        UOServiceLib.Log.Error("Activity List error: " + ex.Message, ex);
        //        //WRITE ERROR LOG
        //        ErrorMessage e = new ErrorMessage();
        //        e.MessageType = mType.Error;
        //        DateTime dt = DateTime.Now;
        //        e.MessageDate = dt.ToString("MM/dd/yyyy");
        //        e.MessageTime = dt.ToString("hh:mm:ss tt");

        //        e.Error_ErrorType = mType.RequestActivityList;
        //        e.Error_HCAI_ErrorType = "903:?:Exception occurred!";

        //        //WRITE TO ERROR LOG

        //        return e;
        //    }
        //}

        public OCFMapReturnType UpdateDocumentInDb(AdjusterResponse doc)
        {
            OCFMapReturnType Succ = null;

            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                return Succ;
            }

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            {
                //validate that hcai document number exists in our DB
                if (IsHCAIDocumentExistsInUO(mCommand, doc.HCAI_Document_Number, doc.PMS_Document_Key, true, doc.Document_Type) == false)
                {
                    Succ = new OCFMapReturnType
                    {
                        OCFType = doc.Document_Type,
                        HCAI_Document_Number = doc.HCAI_Document_Number,
                        OCFID = ConvertStrToLong(doc.PMS_Document_Key),
                        New_HCAI_Status = (HcaiStatus) 0,
                        StatusDate = "",
                        StatusTime = "",
                        ATotal = 0,
                        MinSentDate = ""
                    };

                    return Succ;
                }

                var dt = DateTime.Now;
                switch (doc.Document_Type.ToUpper())
                {
                    case "OCF18":
                        Succ = UpdateOCF18Reply(mCommand, doc, "OCF18", dt, dt.ToString("hh:mm:ss tt"));
                        break;
                    case "OCF21B":
                        Succ = UpdateOCF21BReply(mCommand, doc, "OCF21B", dt, dt.ToString("hh:mm:ss tt"));
                        break;
                    case "OCF21C":
                        Succ = UpdateOCF21CReply(mCommand, doc, "OCF21C", dt, dt.ToString("hh:mm:ss tt"));
                        break;
                    case "OCF23":
                        Succ = UpdateOCF23Reply(mCommand, doc, "OCF23", dt, dt.ToString("hh:mm:ss tt"));
                        break;
                    case "FORM1":
                        Succ = UpdateForm1Reply(mCommand, doc, "Form1", dt, dt.ToString("hh:mm:ss tt"));
                        break;
                    case "OCF22":
                        Succ = UpdateOCF22Reply(mCommand, doc, "OCF22", dt, dt.ToString("hh:mm:ss tt"));
                        break;
                    default:
                        break;
                }

                return Succ;
            }
        }

        public bool DocumentExistsOnHcaiServer(string hcaiNumber, string ocfType)
        {
            if (!IsCredentialsValid())
            {
                Debugger.WriteToFile("Hcai credentials are empty. DocumentExistsOnHcaiServer aborted.", 60);
                return false;
            }

            if (string.IsNullOrEmpty(hcaiNumber) || string.IsNullOrEmpty(ocfType))
            {
                Debugger.WriteToFile("Error: ExtractDocument hcai number or ocftype is null or empty", 60);
                return false;
            }

            Debugger.WriteToFile("Extracting doc: " + hcaiNumber + " type: " + ocfType, 60);
            var type = DocType.OCF9;

            switch (ocfType.ToUpper())
            {
                case "OCF18":
                    type = DocType.OCF18;
                    break;
                case "OCF21B":
                    type = DocType.OCF21B;
                    break;
                case "OCF21C":
                    type = DocType.OCF21C;
                    break;
                case "OCF22":
                    type = DocType.OCF22;
                    break;
                case "OCF23":
                    type = DocType.OCF23;
                    break;
                case "FORM1":
                    type = DocType.AACN;
                    break;
                default:
                    type = DocType.OCF23;
                    break;
            }

            var res = pmsTK.dataExtract(mHCAILogin, mHCAIPassword, hcaiNumber, type);
            if (res == null)
            {
                Debugger.WriteToFile("PMSToolkit failed to exstract doc: " + hcaiNumber + " type: " + ocfType, 60);
                return false;
            }

            var deKeys = new DataExtractResponseKeys();
            return hcaiNumber == res.getValue(deKeys.HCAI_Document_Number) &&
                   ocfType.ToLower() == res.getValue(deKeys.Document_Type).ToLower();
        }

        public AdjusterResponse ExtractDocument(string hcaiNumber, string ocfType)
        {
            if (!IsCredentialsValid())
            {
                Debugger.WriteToFile("Hcai credentials are empty. CheckAndFixOCF_Extracted aborted.", 60);
                return null;
            }

            if (string.IsNullOrEmpty(hcaiNumber) || string.IsNullOrEmpty(ocfType))
            {
                Debugger.WriteToFile("Error: ExtractDocument hcai number or ocftype is null or empty", 60);
                return null;
            }

            Debugger.WriteToFile("Extracting doc: " + hcaiNumber + " type: " + ocfType, 60);
            var type = DocType.OCF9;
            var adRes = new AdjusterResponse();

            switch (ocfType.ToUpper())
            {
                case "OCF18":
                    type = DocType.OCF18;
                    break;
                case "OCF21B":
                    type = DocType.OCF21B;
                    break;
                case "OCF21C":
                    type = DocType.OCF21C;
                    break;
                case "OCF22":
                    type = DocType.OCF22;
                    break;
                case "OCF23":
                    type = DocType.OCF23;
                    break;
                case "FORM1":
                    type = DocType.AACN;
                    break;
                default:
                    break;
            }

            var res = pmsTK.dataExtract(mHCAILogin, mHCAIPassword, hcaiNumber, type);
            if (res == null)
            {
                Debugger.WriteToFile("PMSToolkit failed to exstract doc: " + hcaiNumber + " type: " + ocfType, 60);
                return null;
            }

            var dt = DateTime.Now;
            adRes.MessageType = mType.DocumentExtracted;
            adRes.MessageDate = dt.ToString("MM/dd/yyyy");
            adRes.MessageTime = dt.ToString("hh:mm:ss tt");

            ExtractCommonFields(res, adRes);

            //START RECEIVING OCF9
            //creating keys for all the fields in the document
            var edOcf9Keys = new OCF9DataExtractResponseKeys();
            var edOcf9LiKeys = new OCF9DataExtractResponseLineItemKeys();

            adRes.Parent_Document_Number = res.getValue(edOcf9Keys.HCAI_Document_Number); ///?????????????????????
            adRes.OCF9_OCFVersion = res.getValue(edOcf9Keys.OCFVersion);
            adRes.OCF9_Header_DateRevised = res.getValue(edOcf9Keys.OCF9_Header_DateRevised);
            adRes.OCF9_AttachmentsBeingSent = res.getValue(edOcf9Keys.AttachmentsBeingSent);
            adRes.IsOCF9Present = adRes.Parent_Document_Number != "";

            if (adRes.IsOCF9Present == true)
            {
                //OCF9 EXISTS
            }
            else
            {
                //OCF9 DOES NOT EXISTS
            }
            //getting LineItem List
            IList liList2 = res.getList(LineItemType.GS9LineItem);
            if (liList2 != null)
            {
                int i;
                IDataItem tmpItem;
                for (i = 0; i < liList2.Count; i++)
                {
                    AdjusterResponseOCF9LineItem li = new AdjusterResponseOCF9LineItem();
                    tmpItem = (IDataItem)liList2[i];
                    li.OCF9_GoodsAndServices_Items_Item_ReferenceNumber = tmpItem.getValue(edOcf9LiKeys.OCF9_GoodsAndServices_Items_Item_ReferenceNumber);
                    li.OCF9_GoodsAndServices_Items_Item_InterestPayable = tmpItem.getValue(edOcf9LiKeys.OCF9_GoodsAndServices_Items_Item_InterestPayable);

                    adRes.OCF9_LineItem = li;
                }
            }

            adRes.OCF9_AdditionalComments = res.getValue(edOcf9Keys.AdditionalComments);
            // END OCF9

            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection is null)
            {
                return null;
            }

            using (dbConnection)
            using (var mCommand = dbConnection.CreateCommand())
            {
                adRes.PMS_Document_Key = GetUODocumentID_ByHCAINumber(mCommand, adRes.Document_Type,
                    adRes.HCAI_Document_Number, ConvertStrToLong(adRes.PMS_Document_Key)).ToString();
            }
            adRes.MessageOCFType = adRes.Document_Type;

            switch (adRes.Document_Type.ToUpper())
            {
                case "OCF18":
                {
                    DebugWrite("AdjRes: OCF18");

                    //creating keys for all the fields in the document
                    var edOCF18Keys = new OCF18DataExtractResponseKeys();
                    var edOCF18LISKeys = new OCF18DataExtractResponseLineItemKeys();

                    adRes.Insurer_IBCInsurerID = res.getValue(edOCF18Keys.Insurer_IBCInsurerID);
                    adRes.Insurer_IBCBranchID = res.getValue(edOCF18Keys.Insurer_IBCBranchID);


                    //getting Session Header List
                    IList shList = res.getList(PMSToolkit.LineItemType.DE_GS18SessionHeaderLineItem_Approved);
                    if (shList != null)
                    {
                        DebugWrite("AdjRes: OCF18 Header");

                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < shList.Count; i++)
                        {
                            DebugWrite("AdjRes: OCF18 Header1");
                            var shLineItem = new AdjusterResponseOCF18SessionLineItem();
                            tmpItem = (IDataItem) shList[i];
                            shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                            shLineItem
                                .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost);
                            shLineItem.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count);
                            shLineItem
                                .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost);
                            shLineItem
                                .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            shLineItem
                                .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            shLineItem
                                .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                            adRes.OCF18_Session_LineItem = shLineItem;
                        }
                    }

                    //getting NonSession Header List
                    IList nonSList = res.getList(PMSToolkit.LineItemType.DE_GS18LineItem_Approved);
                    if (nonSList != null)
                    {
                        DebugWrite("counter = " + nonSList.Count);

                        DebugWrite("AdjRes: OCF18 Non Session");
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < nonSList.Count; i++)
                        {
                            DebugWrite("AdjRes: OCF18 Non Session1");
                            var nonSLineItem = new AdjusterResponseOCF18NonSessionLineItem();
                            tmpItem = (IDataItem) nonSList[i];
                            nonSLineItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            nonSLineItem
                                .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc
                                =
                                tmpItem.getValue(
                                    edOCF18LISKeys
                                        .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                            adRes.OCF18_NonSession_LineItem = nonSLineItem;
                        }
                    }

                    adRes.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile =
                        res.getValue(edOCF18Keys.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile);
                    adRes.OCF18_OtherHealthPractitioner_LicenseNumber =
                        res.getValue(edOCF18Keys.OCF18_OtherHealthPractitioner_LicenseNumber);
                    adRes.OCF18_InsurerSignature_ApplicantSignatureWaived =
                        res.getValue(edOCF18Keys.OCF18_InsurerSignature_ApplicantSignatureWaived);
                    //Totals
                    adRes.OCF18_InsurerTotals_AutoInsurerTotal_Approved =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Approved);
                    //MOH
                    adRes.OCF18_InsurerTotals_MOH_Approved_LineCost =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_MOH_Approved_LineCost);
                    adRes.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //Other Insurers
                    adRes.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost);
                    adRes.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //GST
                    adRes.OCF18_InsurerTotals_GST_Approved_LineCost =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_GST_Approved_LineCost);
                    adRes.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //PST
                    adRes.OCF18_InsurerTotals_PST_Approved_LineCost =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_PST_Approved_LineCost);
                    adRes.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    adRes.OCF18_InsurerTotals_SubTotal_Approved =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_SubTotal_Approved);
                    adRes.OCF18_InsurerTotals_TotalCount_Approved =
                        res.getValue(edOCF18Keys.OCF18_InsurerTotals_TotalCount_Approved);

                    adRes.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation = res.getValue(edOCF18Keys.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation); //todo - Ver3.13
                    adRes.NameOfAdjusterOnPDF = res.getValue(edOCF18Keys.NameOfAdjusterOnPDF); //todo - Ver3.21
                    DebugWrite("AdjRes: OCF18 End");

                    break;
                }
                case "OCF21B":
                {
                    DebugWrite("AdjRes: OCF21B");

                    //creating keys for all the fields in the document
                    var edOCF21BKeys = new OCF21BDataExtractResponseKeys();
                    var edOCF21BLIKeys = new OCF21BDataExtractResponseLineItemKeys();

                    adRes.HCAI_Plan_Document_Number = res.getValue(edOCF21BKeys.HCAI_Document_Number);
                    adRes.Insurer_IBCBranchID = res.getValue(edOCF21BKeys.Insurer_IBCBranchID);
                    adRes.Insurer_IBCInsurerID = res.getValue(edOCF21BKeys.Insurer_IBCInsurerID);

                    //getting LineItem List
                    IList liList = res.getList(LineItemType.DE_GS21BLineItem_Approved);
                    if (liList != null)
                    {
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < liList.Count; i++)
                        {
                            AdjusterResponseOCF21BLineItem li = new AdjusterResponseOCF21BLineItem();
                            tmpItem = (IDataItem) liList[i];

                            li.OCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey =
                                tmpItem.getValue(
                                    edOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                            li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST =
                                tmpItem.getValue(
                                    edOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST);
                            li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST =
                                tmpItem.getValue(
                                    edOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST);
                            li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost =
                                tmpItem.getValue(
                                    edOCF21BLIKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost);
                            li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                                tmpItem.getValue(
                                    edOCF21BLIKeys
                                        .OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF21BLIKeys
                                        .OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            li.OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF21BLIKeys
                                        .OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                            adRes.OCF21B_LineItem = li;
                        }
                    }
                    adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost);
                    adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(
                            edOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys
                                .OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(
                            edOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys
                                .OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys
                                .OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(
                            edOCF21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys
                                .OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys
                                .OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    adRes.OCF21B_InsurerTotals_MOH_Approved =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_MOH_Approved);
                    adRes.OCF21B_InsurerTotals_OtherInsurers_Approved =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_OtherInsurers_Approved);
                    adRes.OCF21B_InsurerTotals_AutoInsurerTotal_Approved =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Approved);
                    adRes.OCF21B_InsurerTotals_GST_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_LineCost);

                    adRes.OCF21B_InsurerTotals_GST_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_LineCost);
                    adRes.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF21B_InsurerTotals_PST_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_LineCost);
                    adRes.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    adRes.OCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_SubTotal_Approved);
                    //deleted---						adRes.OCF21B_InsurerTotals_Interest_Approved = res.getValue(adOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved);
                    adRes.OCF21B_InsurerTotals_Interest_Approved_LineCost =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_LineCost);
                    adRes.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    adRes.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation =
                        res.getValue(edOCF21BKeys.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation);
                        //todo - Ver3.13
                    adRes.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation =
                        res.getValue(edOCF21BKeys.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation);
                        //todo - Ver3.13
                    adRes.OCF21B_Payee_FacilityIsPayee = res.getValue(edOCF21BKeys.OCF21B_Payee_FacilityIsPayee);
                        //todo - Ver3.13
                    adRes.OCF21B_InsurerSignature_ClaimFormReceivedDate =
                        res.getValue(edOCF21BKeys.OCF21B_InsurerSignature_ClaimFormReceivedDate); //todo - Ver3.13
                    adRes.OCF21B_InsurerSignature_ClaimFormReceived = res.getValue(edOCF21BKeys.OCF21B_InsurerSignature_ClaimFormReceived); //todo - Ver3.13
                    adRes.ApprovedByOnPDF = res.getValue(edOCF21BKeys.ApprovedByOnPDF); //todo - Ver3.21
                    break;
                }
                case "OCF21C":
                {
                    DebugWrite("AdjRes: OCF21C");

                    //creating keys for all the fields in the document
                    var edOCF21CKeys = new OCF21CDataExtractResponseKeys();
                    var edOCF21CLIKeys = new OCF21CDataExtractResponseLineItemKeys();

                    adRes.HCAI_OCF23_Document_Number = res.getValue(edOCF21CKeys.HCAI_Document_Number); //???
                    adRes.Insurer_IBCInsurerID = res.getValue(edOCF21CKeys.Insurer_IBCInsurerID);
                    adRes.Insurer_IBCBranchID = res.getValue(edOCF21CKeys.Insurer_IBCBranchID);

                    //getting PAFReimbursableLineItem List
                    IList pafList = res.getList(PMSToolkit.LineItemType.DE_GS21CPAFReimbursableLineItem_Approved);
                    if (pafList != null)
                    {
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < pafList.Count; i++)
                        {
                            var li = new AdjusterResponseOCF21CPAFReimbursableLineItem();
                            tmpItem = (IDataItem) pafList[i];

                            li.OCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey =
                                tmpItem.getValue(edOCF21CLIKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey);
                            li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost =
                                tmpItem.getValue(edOCF21CLIKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost);
                            li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            li.OCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                            //added after update            
                            adRes.OCF21C_PAFReimbursable_LineItem = li;
                        }
                    }

                    //getting OtherReimbursableLineItem List
                    IList otherList = res.getList(PMSToolkit.LineItemType.DE_GS21COtherReimbursableLineItem_Approved);
                    if (otherList != null)
                    {
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < otherList.Count; i++)
                        {
                            AdjusterResponseOCF21COtherReimbursableLineItem li =
                                new AdjusterResponseOCF21COtherReimbursableLineItem();
                            tmpItem = (IDataItem) otherList[i];

                            li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey =
                                tmpItem.getValue(
                                    edOCF21CLIKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                            li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST =
                                tmpItem.getValue(
                                    edOCF21CLIKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST);
                            li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST =
                                tmpItem.getValue(
                                    edOCF21CLIKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST);
                            li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost =
                                tmpItem.getValue(
                                    edOCF21CLIKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost);
                            li.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            li
                                .OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc
                                =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            li
                                .OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
                                =
                                tmpItem.getValue(
                                    edOCF21CLIKeys
                                        .OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                            adRes.OCF21C_OtherReimbursable_LineItem = li;
                        }
                    }
                    adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost =
                        res.getValue(edOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost);
                    adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(
                            edOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys
                                .OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost =
                        res.getValue(edOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(
                            edOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys
                                .OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys
                                .OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost =
                        res.getValue(edOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(
                            edOCF21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys
                                .OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys
                                .OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    adRes.OCF21C_InsurerTotals_MOH_Approved =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_MOH_Approved);
                    adRes.OCF21C_InsurerTotals_OtherInsurers_Approved =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_OtherInsurers_Approved);
                    adRes.OCF21C_InsurerTotals_AutoInsurerTotal_Approved =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Approved);
                    //deleted---					adRes.OCF21C_InsurerTotals_GST_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_GST_Approved);
                    adRes.OCF21C_InsurerTotals_GST_Approved_LineCost =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_LineCost);
                    adRes.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //deleted---						adRes.OCF21C_InsurerTotals_PST_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_PST_Approved);
                    adRes.OCF21C_InsurerTotals_PST_Approved_LineCost =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_LineCost);
                    adRes.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //deleted---						adRes.OCF21C_InsurerTotals_Interest_Approved = res.getValue(adOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved);
                    adRes.OCF21C_InsurerTotals_Interest_Approved_LineCost =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_LineCost);
                    adRes.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    adRes.OCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved =
                        res.getValue(
                            edOCF21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved);
                    adRes.OCF21C_InsurerTotals_SubTotalPreApproved_Approved =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Approved);

                    adRes.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation =
                        res.getValue(edOCF21CKeys.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation);
                        //todo - Ver3.13
                    adRes.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation =
                        res.getValue(edOCF21CKeys.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation);
                        //todo - Ver3.13
                    adRes.OCF21C_Payee_FacilityIsPayee = res.getValue(edOCF21CKeys.OCF21C_Payee_FacilityIsPayee);
                        //todo - Ver3.13
                    adRes.OCF21C_InsurerSignature_ClaimFormReceivedDate =
                        res.getValue(edOCF21CKeys.OCF21C_InsurerSignature_ClaimFormReceivedDate); //todo - Ver3.13
                    adRes.OCF21C_InsurerSignature_ClaimFormReceived = res.getValue(edOCF21CKeys.OCF21C_InsurerSignature_ClaimFormReceived); //todo - Ver3.13
                    adRes.ApprovedByOnPDF = res.getValue(edOCF21CKeys.ApprovedByOnPDF); //todo - Ver3.21

                    break;
                }
                case "OCF22":
                {
                    DebugWrite("AdjRes: OCF22");

                    //creating keys for all the fields in the document
                    var edOCF22Keys = new OCF22DataExtractResponseKeys();
                    var edOCF22LIKeys = new OCF22DataExtractResponseLineItemKeys();

                    adRes.OCF22_Insurer_IBCInsurerID = res.getValue(edOCF22Keys.Insurer_IBCInsurerID); //???
                    adRes.OCF22_Insurer_IBCBranchID = res.getValue(edOCF22Keys.Insurer_IBCBranchID); //???
                    //added---
                    adRes.OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18 =
                        res.getValue(edOCF22Keys.OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18);

                    //getting LineItem List
                    IList liList = res.getList(PMSToolkit.LineItemType.DE_GS22LineItem_Approved);
                    if (liList != null)
                    {
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < liList.Count; i++)
                        {
                            var li = new AdjusterResponseOCF22LineItem();
                            tmpItem = (IDataItem) liList[i];

                            li.OCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey =
                                tmpItem.getValue(
                                    edOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PMSGSKey);
                            li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST =
                                tmpItem.getValue(edOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST);
                            li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST =
                                tmpItem.getValue(edOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST);
                            li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost =
                                tmpItem.getValue(
                                    edOCF22LIKeys.OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost);
                            li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF22LIKeys
                                        .OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup =
                                tmpItem.getValue(
                                    edOCF22LIKeys
                                        .OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            li.OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF22LIKeys
                                        .OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                            adRes.OCF22_LineItem = li;
                        }
                    }
                    adRes.OCF22_InsurerTotals_AutoInsurerTotal_Approved =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_AutoInsurerTotal_Approved);
                    adRes.OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_SubTotal_Approved);
                    //deleted---						adRes.OCF22_InsurerTotals_GST_Approved = res.getValue(adOCF22Keys.OCF22_InsurerTotals_GST_Approved);
                    adRes.OCF22_InsurerTotals_GST_Approved_LineCost =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_GST_Approved_LineCost);
                    adRes.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    //deleted---						adRes.OCF22_InsurerTotals_PST_Approved = res.getValue(adOCF22Keys.OCF22_InsurerTotals_PST_Approved);
                    adRes.OCF22_InsurerTotals_PST_Approved_LineCost =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_PST_Approved_LineCost);
                    adRes.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF22_InsurerTotals_MOH_Approved_LineCost =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_MOH_Approved_LineCost);
                    adRes.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    adRes.OCF22_InsurerTotals_OtherInsurers_Approved_LineCost =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_LineCost);
                    adRes.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode =
                        res.getValue(edOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode);
                    adRes.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        res.getValue(
                            edOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    adRes.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        res.getValue(
                            edOCF22Keys.OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    break;
                }
                case "OCF23":
                {
                    DebugWrite("AdjRes: OCF23");

                    //creating keys for all the fields in the document
                    var edOCF23Keys = new OCF23DataExtractResponseKeys();
                    var edOCF23LIKeys = new OCF23DataExtractResponseLineItemKeys();

                    adRes.Insurer_IBCInsurerID = res.getValue(edOCF23Keys.Insurer_IBCInsurerID);
                    adRes.Insurer_IBCBranchID = res.getValue(edOCF23Keys.Insurer_IBCBranchID);

                    //getting LineItem List
                    IList liList = res.getList(PMSToolkit.LineItemType.DE_GS23OtherGSLineItem_Approved);
                    if (liList == null)
                    {
                        int i;
                        IDataItem tmpItem;
                        for (i = 0; i < liList.Count; i++)
                        {
                            AdjusterResponseOCF23OtherGSLineItem li = new AdjusterResponseOCF23OtherGSLineItem();
                            tmpItem = (IDataItem) liList[i];

                            //deleted---					li.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber = tmpItem.getValue(adOCF23LIKeys.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber);
                            li.OCF23_OtherGoodsandServices_Items_Item_PMSGSKey =
                                tmpItem.getValue(edOCF23LIKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey);
                            li.OCF23_OtherGoodsandServices_Items_Approved_LineCost =
                                tmpItem.getValue(edOCF23LIKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost);
                            li.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode =
                                tmpItem.getValue(
                                    edOCF23LIKeys
                                        .OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                            li.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF23LIKeys
                                        .OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                            li.OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                                tmpItem.getValue(
                                    edOCF23LIKeys
                                        .OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                            adRes.OCF23_OtherGS_LineItem = li;
                        }
                    }

                    adRes.OCF23_InsurerSignature_ApplicantSignatureWaived =
                        res.getValue(edOCF23Keys.OCF23_InsurerSignature_ApplicantSignatureWaived);
                    adRes.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices =
                        res.getValue(edOCF23Keys.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices);
                    adRes.OCF23_InsurerSignature_PolicyInForceConfirmation =
                        res.getValue(edOCF23Keys.OCF23_InsurerSignature_PolicyInForceConfirmation);

                    adRes.OCF23_InsurerTotals_AutoInsurerTotal_Approved =
                        res.getValue(edOCF23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Approved);
                    adRes.OCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved =
                        res.getValue(edOCF23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved);
                    adRes.OCF23_InsurerTotals_SubTotalPreApproved_Approved = res.getValue(edOCF23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Approved);
                    adRes.NameOfAdjusterOnPDF = res.getValue(edOCF23Keys.NameOfAdjusterOnPDF); //todo - Ver3.21

                    break;
                }
                case "FORM1":
                {
                    DebugWrite("AdjRes: Form1");
                    var form1DeKeys = new AACNDataExtractResponseKeys();
                    var form1DeLineItemKeys = new AACNDataExtractResponseLineItemKeys();

                    adRes.Form1_ArchivalStatus = res.getValue(form1DeKeys.ArchivalStatus);
                    adRes.Form1_BranchVersion = res.getValue(form1DeKeys.BranchVersion);
                    adRes.Form1_Costs_Approved_AdjusterResponseExplanation = res.getValue(form1DeKeys.Costs_Approved_AdjusterResponseExplanation);
                    adRes.Form1_Costs_Approved_Benefit = res.getValue(form1DeKeys.Costs_Approved_Benefit);
                    adRes.Form1_Costs_Approved_CalculatedBenefit = res.getValue(form1DeKeys.Costs_Approved_CalculatedBenefit);
                    adRes.Form1_Costs_Approved_ReasonCode = res.getValue(form1DeKeys.Costs_Approved_ReasonCode);
                    adRes.Form1_Costs_Approved_ReasonDescription = res.getValue(form1DeKeys.Costs_Approved_ReasonDescription);
                    adRes.Form1_EOB_EOBAdditionalComments = res.getValue(form1DeKeys.EOB_EOBAdditionalComments);
                    adRes.Form1_FacilityVersion = res.getValue(form1DeKeys.FacilityVersion);
                    adRes.Form1_InsurerVersion = res.getValue(form1DeKeys.InsurerVersion);
                    adRes.Insurer_IBCInsurerID = res.getValue(form1DeKeys.Insurer_IBCInsurerID);
                    adRes.Insurer_IBCBranchID = res.getValue(form1DeKeys.Insurer_IBCBranchID);
                    adRes.Form1_AttachmentsReceivedDate = res.getValue(form1DeKeys.AttachmentsReceivedDate);
                    adRes.Form1_DocumentVersion = res.getValue(form1DeKeys.DocumentVersion);

                    adRes.Form1_Costs_Assessed_Benefit = res.getValue(form1DeKeys.Costs_Assessed_Benefit);

                    adRes.Form1_AssessmentDate = res.getValue(form1DeKeys.AssessmentDate);
                    adRes.Form1_LastAssessmentDate = res.getValue(form1DeKeys.LastAssessmentDate);
                    adRes.Form1_Assessor_DateSigned = res.getValue(form1DeKeys.Assessor_DateSigned);
                    adRes.Form1_Assessor_Email = res.getValue(form1DeKeys.Assessor_Email);
                    adRes.Form1_Assessor_Facility_Name = res.getValue(form1DeKeys.Assessor_Facility_Name);
                    adRes.Form1_Assessor_FacilityRegistryID = res.getValue(form1DeKeys.Assessor_FacilityRegistryID);
                    adRes.Form1_Assessor_IsSignatureOnFile = res.getValue(form1DeKeys.Assessor_IsSignatureOnFile);
                    adRes.Form1_Assessor_Occupation = res.getValue(form1DeKeys.Assessor_Occupation);
                    adRes.Form1_Assessor_ProviderRegistryID = res.getValue(form1DeKeys.Assessor_ProviderRegistryID);

                    adRes.Form1_CurrentMonthlyAllowance = res.getValue(form1DeKeys.CurrentMonthlyAllowance);

                    adRes.Form1_FirstAssessment = res.getValue(form1DeKeys.FirstAssessment);
                    adRes.Form1_FormVersion = res.getValue(form1DeKeys.FormVersion);
                    adRes.Form1_Header_ClaimNumber = res.getValue(form1DeKeys.Header_ClaimNumber);
                    adRes.Form1_Header_DateOfAccident = res.getValue(form1DeKeys.Header_DateOfAccident);
                    adRes.Form1_Header_PolicyNumber = res.getValue(form1DeKeys.Header_PolicyNumber);

                    adRes.Form1_Insurer_Name = res.getValue(form1DeKeys.Insurer_Name);
                    adRes.Form1_Insurer_PolicyHolder_IsSameAsApplicant = res.getValue(form1DeKeys.Insurer_PolicyHolder_IsSameAsApplicant);
                    adRes.Form1_Insurer_PolicyHolder_Name_FirstName = res.getValue(form1DeKeys.Insurer_PolicyHolder_Name_FirstName);
                    adRes.Form1_Insurer_PolicyHolder_Name_LastName = res.getValue(form1DeKeys.Insurer_PolicyHolder_Name_LastName);
                    adRes.Form1_InsurerExamination = res.getValue(form1DeKeys.InsurerExamination);

                    adRes.Form1_AttachmentCount = res.getValue(form1DeKeys.AttachmentCount);
                    adRes.Form1_PMSFields_PMSPatientKey = res.getValue(form1DeKeys.PMSFields_PMSPatientKey);
                    adRes.NameOfAdjusterOnPDF = res.getValue(form1DeKeys.NameOfAdjusterOnPDF); //todo - Ver3.21

                    IList liList = res.getList(LineItemType.AACNServicesLineItem);
                    for (int i = 0; i < liList.Count; i++)
                    {
                        var li = new AdjusterResponseForm1AcsLineItem();
                        var tmpItem = (IDataItem)liList[i];
                        li.AttendantCareServices_Item_ACSItemID = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_ACSItemID);
                        li.AttendantCareServices_Item_Approved_IsItemDeclined = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Approved_IsItemDeclined);
                        li.AttendantCareServices_Item_Approved_Minutes = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Approved_Minutes);
                        li.AttendantCareServices_Item_Approved_ReasonCode = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Approved_ReasonCode);
                        li.AttendantCareServices_Item_Approved_ReasonDescription = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Approved_ReasonDescription);
                        li.AttendantCareServices_Item_Approved_TimesPerWeek = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Approved_TimesPerWeek);
                        li.AttendantCareServices_Item_Approved_TotalMinutes = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Approved_TotalMinutes);
                        li.AttendantCareServices_Item_Description = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_Description);

                        adRes.Form1AcsLineItem.Add(li);
                    }

                    liList = res.getList(LineItemType.AACNApprovedPart);
                    for (int i = 0; i < liList.Count; i++)
                    {
                        var li = new AdjusterResponseForm1ApprovedCostLineItem();
                        var tmpItem = (IDataItem)liList[i];
                        li.Costs_Approved_Part_ACSItemID = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_ACSItemID);
                        li.Costs_Approved_Part_Benefit = tmpItem.getValue(form1DeLineItemKeys.Costs_Approved_Part_Benefit);
                        li.Costs_Approved_Part_HourlyRate = tmpItem.getValue(form1DeLineItemKeys.Costs_Approved_Part_HourlyRate);
                        li.Costs_Approved_Part_WeeklyHours = tmpItem.getValue(form1DeLineItemKeys.Costs_Approved_Part_WeeklyHours);
                        li.Costs_Approved_Part_MonthlyHours = tmpItem.getValue(form1DeLineItemKeys.Costs_Approved_Part_MonthlyHours);
                        li.Costs_Approved_Part_ReasonCode = tmpItem.getValue(form1DeLineItemKeys.Costs_Approved_Part_ReasonCode);
                        li.Costs_Approved_Part_ReasonDescription = tmpItem.getValue(form1DeLineItemKeys.Costs_Approved_Part_ReasonDescription);

                        adRes.Form1ApprovedCostLineItem.Add(li);
                    }

                    liList = res.getList(LineItemType.AACNAssessedPart);
                    for (int i = 0; i < liList.Count; i++)
                    {
                        var li = new AdjusterResponseForm1AssessedCostLineItem();
                        var tmpItem = (IDataItem)liList[i];
                        li.Costs_Assessed_Part_ACSItemID = tmpItem.getValue(form1DeLineItemKeys.AttendantCareServices_Item_ACSItemID);
                        li.Costs_Assessed_Part_Benefit = tmpItem.getValue(form1DeLineItemKeys.Costs_Assessed_Part_Benefit);
                        li.Costs_Assessed_Part_HourlyRate = tmpItem.getValue(form1DeLineItemKeys.Costs_Assessed_Part_HourlyRate);
                        li.Costs_Assessed_Part_WeeklyHours = tmpItem.getValue(form1DeLineItemKeys.Costs_Assessed_Part_WeeklyHours);
                        li.Costs_Assessed_Part_MonthlyHours = tmpItem.getValue(form1DeLineItemKeys.Costs_Assessed_Part_MonthlyHours);

                        adRes.Form1AssessedCostLineItem.Add(li);
                    }

                    break;
                }
                default:
                    break;
            }

            return adRes;
        }

        private void ExtractCommonFields(IDataItem data, AdjusterResponse result)
        {
            var ocfKeys = new DataExtractResponseKeys();
            
            //Common Keys
            result.HCAI_Document_Number = data.getValue(ocfKeys.HCAI_Document_Number);
            if (string.IsNullOrEmpty(result.HCAI_Document_Number))
            {
                var form1Keys = new AACNDataExtractResponseKeys();
                result.HCAI_Document_Number = data.getValue(form1Keys.DocumentNumber);
                result.PMS_Document_Key = data.getValue(form1Keys.PMSFields_PMSDocumentKey);
                result.Document_Type = data.getValue(form1Keys.Document_Type);
                result.Document_Type = result.Document_Type.ToLower() == "aacn" ? "Form1" : result.Document_Type; 
                result.Submission_Date = data.getValue(form1Keys.SubmissionTime);
                result.Submission_Source = data.getValue(form1Keys.SubmissionSource);
                result.Adjuster_Response_Date = data.getValue(form1Keys.AdjusterResponseTime);
                result.In_Dispute = data.getValue(form1Keys.InDispute);
                result.Document_Status = data.getValue(form1Keys.DocumentState);
                result.Claimant_First_Name = data.getValue(form1Keys.Claimant_Name_FirstName);
                result.Claimant_Last_Name = data.getValue(form1Keys.Claimant_Name_LastName);
                result.Claimant_Middle_Name = data.getValue(form1Keys.Claimant_Name_MiddleName);
                result.Claimant_Date_Of_Birth = data.getValue(form1Keys.Claimant_DateOfBirth);
                result.Claimant_Gender = data.getValue(form1Keys.Claimant_Gender);
                result.Claimant_Telephone = data.getValue(form1Keys.Claimant_Telephone);
                result.Claimant_Extension = data.getValue(form1Keys.Claimant_Extension);
                result.Claimant_Address1 = data.getValue(form1Keys.Claimant_Address_StreetAddress1);
                result.Claimant_Address2 = data.getValue(form1Keys.Claimant_Address_StreetAddress2);
                result.Claimant_City = data.getValue(form1Keys.Claimant_Address_City);
                result.Claimant_Province = data.getValue(form1Keys.Claimant_Address_Province);
                result.Claimant_Postal_Code = data.getValue(form1Keys.Claimant_Address_PostalCode);
                result.SigningAdjuster_FirstName = data.getValue(form1Keys.SigningAdjusterName_FirstName);
                result.SigningAdjuster_LastName = data.getValue(form1Keys.SigningAdjusterName_LastName);
                result.Attachments_Received_Date = data.getValue(form1Keys.AttachmentsReceivedDate);

                result.Applicant_Address_City = data.getValue(form1Keys.Applicant_Address_City);
                result.Applicant_Address_PostalCode = data.getValue(form1Keys.Applicant_Address_PostalCode);
                result.Applicant_Address_Province = data.getValue(form1Keys.Applicant_Address_Province);
                result.Applicant_Address_StreetAddress1 = data.getValue(form1Keys.Applicant_Address_StreetAddress1);
                result.Applicant_Address_StreetAddress2 = data.getValue(form1Keys.Applicant_Address_StreetAddress2);
                result.Applicant_DateOfBirth = data.getValue(form1Keys.Applicant_DateOfBirth);
                result.Applicant_Gender = data.getValue(form1Keys.Applicant_Gender);
                result.Applicant_Name_FirstName = data.getValue(form1Keys.Applicant_Name_FirstName);
                result.Applicant_Name_LastName = data.getValue(form1Keys.Applicant_Name_LastName);
                result.Applicant_Name_MiddleName = data.getValue(form1Keys.Applicant_Name_MiddleName);
                result.Applicant_TelephoneNumber = data.getValue(form1Keys.Applicant_Telephone);
                result.Applicant_TelephoneExtension = data.getValue(form1Keys.Applicant_Extension);

                result.AdditionalComments = data.getValue(form1Keys.AdditionalComments);
                result.AttachmentsBeingSent = data.getValue(form1Keys.AttachmentsBeingSent);
            }
            else
            {
                result.PMS_Document_Key = data.getValue(ocfKeys.PMSFields_PMSDocumentKey);
                result.Document_Type = data.getValue(ocfKeys.Document_Type);
                result.Submission_Date = data.getValue(ocfKeys.Submission_Date);
                result.Submission_Source = data.getValue(ocfKeys.Submission_Source);
                result.Adjuster_Response_Date = data.getValue(ocfKeys.Adjuster_Response_Date);
                result.In_Dispute = data.getValue(ocfKeys.In_Dispute);
                result.Document_Status = data.getValue(ocfKeys.Document_Status);
                result.Claimant_First_Name = data.getValue(ocfKeys.Claimant_First_Name);
                result.Claimant_Last_Name = data.getValue(ocfKeys.Claimant_Last_Name);
                result.Claimant_Middle_Name = data.getValue(ocfKeys.Claimant_Middle_Name);
                result.Claimant_Date_Of_Birth = data.getValue(ocfKeys.Claimant_Date_Of_Birth);
                result.Claimant_Gender = data.getValue(ocfKeys.Claimant_Gender);
                result.Claimant_Telephone = data.getValue(ocfKeys.Claimant_Telephone);
                result.Claimant_Extension = data.getValue(ocfKeys.Claimant_Extension);
                result.Claimant_Address1 = data.getValue(ocfKeys.Claimant_Address1);
                result.Claimant_Address2 = data.getValue(ocfKeys.Claimant_Address2);
                result.Claimant_City = data.getValue(ocfKeys.Claimant_City);
                result.Claimant_Province = data.getValue(ocfKeys.Claimant_Province);
                result.Claimant_Postal_Code = data.getValue(ocfKeys.Claimant_Postal_Code);
                result.SigningAdjuster_FirstName = data.getValue(ocfKeys.SigningAdjuster_FirstName);
                result.SigningAdjuster_LastName = data.getValue(ocfKeys.SigningAdjuster_LastName);

                result.Applicant_Address_City = data.getValue(ocfKeys.Applicant_Address_City);
                result.Applicant_Address_PostalCode = data.getValue(ocfKeys.Applicant_Address_PostalCode);
                result.Applicant_Address_Province = data.getValue(ocfKeys.Applicant_Address_Province);
                result.Applicant_Address_StreetAddress1 = data.getValue(ocfKeys.Applicant_Address_StreetAddress1);
                result.Applicant_Address_StreetAddress2 = data.getValue(ocfKeys.Applicant_Address_StreetAddress2);
                result.Applicant_DateOfBirth = data.getValue(ocfKeys.Applicant_DateOfBirth);
                result.Applicant_Gender = data.getValue(ocfKeys.Applicant_Gender);
                result.Applicant_Name_FirstName = data.getValue(ocfKeys.Applicant_Name_FirstName);
                result.Applicant_Name_LastName = data.getValue(ocfKeys.Applicant_Name_LastName);
                result.Applicant_Name_MiddleName = data.getValue(ocfKeys.Applicant_Name_MiddleName);
                result.Applicant_TelephoneNumber = data.getValue(ocfKeys.Applicant_TelephoneNumber);
                result.Applicant_TelephoneExtension = data.getValue(ocfKeys.Applicant_TelephoneExtension);
                result.AdditionalComments = data.getValue(ocfKeys.AdditionalComments);
                result.AttachmentsBeingSent = data.getValue(ocfKeys.AttachmentsBeingSent);
            }
        }

        #region CHECK AND FIX OCF IN UO IF NEEDED

        private bool IsHCAIDocumentExistsInUO(SqlCommand mCommand, string HCAI_Document_Number, string PMS_Document_Key, bool MustBeCurrentlyActive, string Document_Type)
        {
            if (MyConvert.IsNumeric(PMS_Document_Key) == false
                || string.IsNullOrEmpty(Document_Type)
                || string.IsNullOrEmpty(HCAI_Document_Number)
                || string.IsNullOrEmpty(PMS_Document_Key))
            {
                return false;
            }
            
            SqlDataReader reader;
            if (MustBeCurrentlyActive == true)
            {
                if (Document_Type == "OCF21B" || Document_Type == "OCF21C")
                {
                    reader = ExecQuery(mCommand, "SELECT DISTINCT H.HCAIDocNumber " +
                                                 "from HCAIStatusLog H " +
                                                 "where H.IsCurrentStatus = 1 AND H.HCAIDocNumber = '" + HCAI_Document_Number + "' AND " +
                                                 "H.InvoiceID = '" + PMS_Document_Key + "'");
                }
                else
                {
                    reader = ExecQuery(mCommand, "SELECT DISTINCT H.HCAIDocNumber " +
                                                 "from HCAIStatusLog H " +
                                                 "where H.IsCurrentStatus = 1 AND H.HCAIDocNumber = '" + HCAI_Document_Number + "' AND " +
                                                 "H.ReportID = '" + PMS_Document_Key + "'");
                }

            }
            else
            {
                if (Document_Type == "OCF21B" || Document_Type == "OCF21C")
                {
                    reader = ExecQuery(mCommand, "SELECT DISTINCT H.HCAIDocNumber " +
                                                 "from HCAIStatusLog H " +
                                                 "where H.HCAIDocNumber = '" + HCAI_Document_Number + "' AND " +
                                                 "(H.InvoiceID = '" + PMS_Document_Key + "')");
                }
                else
                {

                    reader = ExecQuery(mCommand, "SELECT DISTINCT H.HCAIDocNumber " +
                                                 "from HCAIStatusLog H " +
                                                 "where H.HCAIDocNumber = '" + HCAI_Document_Number + "' AND " +
                                                 "(H.ReportID = '" + PMS_Document_Key + "')");
                }
            }

            try
            {
                if (reader.Read() == true)
                {
                    if (MyReaderFldToString(reader, "HCAIDocNumber") == HCAI_Document_Number)
                    {
                        reader.Close();
                        return true;
                    }
                    else
                    {
                        reader.Close();
                        return false;
                    }
                }
            }
            catch (Exception)
            {
            }

            return false;
        }

        private string GetOCFTypeFromHCAIStatusLog(SqlCommand mCommand, string HCAI_Document_Number, ref int OCFStatus, ref int OCFLockStatus, ref DateTime OCFStatusDate, ref string OCFID)
        {
            var reader = ExecQuery(mCommand, "SELECT DISTINCT H.OCFType, H.StatusDate, H.IsCurrentStatus, H.OCFStatus, H.InvoiceID, H.ReportID, H.StatusTime, H.Archived, H.HCAIDocNumber, H.LockStatus " +
                                             "from HCAIStatusLog H " +
                                             "where H.IsCurrentStatus = 1 AND H.HCAIDocNumber = '" + HCAI_Document_Number + "'");
            var OCFType = "";

            try
            {
                if (reader.Read() == true)
                {
                    OCFType = MyReaderFldToString(reader, "OCFType");
                    OCFStatus = ConvertStrToInt(MyReaderFldToString(reader, "OCFStatus"));
                    OCFLockStatus = ConvertStrToInt(MyReaderFldToString(reader, "LockStatus"));
                    OCFStatusDate = (DateTime)reader["StatusDate"];

                    if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
                    {
                        OCFID = MyReaderFldToString(reader, "InvoiceID");
                    }
                    else
                    {
                        OCFID = MyReaderFldToString(reader, "ReportID");
                    }

                }
                reader.Close();
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("GetOCFTypeFromHCAIStatusLog, hcai current/max record was not found \n" +
                        "HCAI_Document_Number: " + HCAI_Document_Number + "\n" +
                        "EXEPTION: " + ex.Message, ex);
                //error
                //Console.WriteLine("GetOCFTypeFromHCAIStatusLog"); 
                Debugger.SendEmail(BusinessName + " tel: " + BusinessPhone + " release: " + WebID,
                        "in function: GetOCFTypeFromHCAIStatusLog, hcai current/max record was not found \n" +
                        "HCAI_Document_Number: " + HCAI_Document_Number + "\n" +
                        "EXEPTION: " + ex.Message);

            }

            return OCFType;
        }

        private string GetOCFTypeFromHCAIStatusLogSearchAll(SqlCommand mCommand, string HCAI_Document_Number) //, int OCFStatus)
        {
            //SqlDataReader reader = ExecQuery(mCommand, "select DISTINCT H.InvoiceID, H.ReportID, H.OCFType, " +
            //"(SELECT MAX(H2.OCFStatus) from HCAIStatusLog H2 where H2.IsCurrentStatus = 1 AND H2.InvoiceID = H.InvoiceID " +
            //"AND H2.ReportID = H.ReportID AND H2.OCFType = H.OCFType) as OCFStatus " + 
            //"from HCAIStatusLog H where H.HCAIDocNumber = '" + HCAI_Document_Number + "'");

            var reader = ExecQuery(mCommand, "select DISTINCT H.OCFType " +
                                             "from HCAIStatusLog H where H.HCAIDocNumber = '" + HCAI_Document_Number + "'");

            string OCFType = "";

            try
            {
                if (reader.Read() == true)
                {
                    OCFType = MyReaderFldToString(reader, "OCFType");
                    //OCFStatus = ConvertStrToInt(MyReaderFldToString(reader, "OCFStatus")); 
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Debugger.LogError($"{nameof(GetOCFTypeFromHCAIStatusLogSearchAll)}: {ex.Message}");
            }

            return OCFType;
        }
        
        private int ConvertExtractedHcaiStatus(string hStatus)
        {
            var t = -1;

            switch (hStatus.ToLower())
            {
                case "pending":
                    t = (int)HcaiStatus.Pending;
                    break;
                case "submitted":
                    t = (int)HcaiStatus.SuccessfullyDelivered;
                    break;
                case "partiallyapproved":
                case "partially approved":
                    t = (int)HcaiStatus.PartiallyApproved;
                    break;
                case "declined":
                    t = (int)HcaiStatus.NotApproved;
                    break;
                case "approved":
                case "responded":
                    t = (int)HcaiStatus.Approved;
                    break;
                case "void":
                    t = (int)HcaiStatus.WithdrawGranted;
                    break;
                case "reviewrequired":
                case "review required":
                    t = (int)HcaiStatus.SuccessfullyDelivered;
                    break;
            }
            
            return t;
        }

        
        private BatchItem ResetOCFStatusOnly(SqlCommand mCommand, string HCAI_Document_Number, string OCFID, string OCFType, int NewOCFStatus, int OldOCFStatus)
        {
            //UPDATE STATUS
            ResetOCFToNewHCAIStatus(mCommand, ConvertStrToLong(OCFID), NewOCFStatus, OCFType, HCAI_Document_Number);

            SqlDataReader reader = null;
            BatchItem it = null;

            if (MyConvert.MyLeft(OCFType, 5) == "OCF21")
            {
                reader = ExecQuery(mCommand, "SELECT DISTINCT H.LockStatus, H.HCAIDocNumber, H.OCFType, H.InvoiceID as OCFID, H.OCFStatus, H.StatusDate, H.StatusTime, H.StatusOrder, " +
                  "C.client_id, C.last_name, C.first_name, CC.CaseID, M.IIAdjFirstName as [MVAAFirstName], M.IIAdjLastName " +
                  "as [MVAALastName], M.IIName as [MVAInsName], I.MVATotal as [Total], M.InvoiceVersion, " +
                  "(select SUM(D.aItemAmount) from InvoiceItemDetails D where D.InvoiceID = M.InvoiceID) as [ATotal], " +
                  "(select MAX(HPrev.OCFStatus) from HCAIStatusLog HPrev " +
                  "where HPrev.InvoiceID = H.InvoiceID AND HPrev.StatusOrder = (H.StatusOrder-1)) as [PrevStatus], " +
                  "(SELECT MIN(HD.StatusDate) FROM HCAIStatusLog HD " +
                  "WHERE H.OCFStatus > 11 AND H.InvoiceID = HD.InvoiceID AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate " +
                  "FROM HCAIStatusLog H inner join (MVAInvoice M inner join " +
                  "(InvoceGeneralInfo I inner join (Client_Case CC inner join Client C on CC.client_id = C.client_id) on " +
                  "I.CaseID = CC.CaseID) on M.InvoiceID = I.InvoiceID) on H.InvoiceID = M.InvoiceID WHERE " +
                  "h.IsCurrentStatus = 1 And h.Archived = 0 And h.OCFStatus <> 1 And M.InvoiceID = " + MyConvert.ConvertStrToDouble(OCFID));
            }
            else
            {
                reader = ExecQuery(mCommand, "SELECT DISTINCT H.LockStatus, H.HCAIDocNumber, H.OCFType, H.ReportID as OCFID, H.OCFStatus, H.StatusDate, H.StatusTime, " +
                  "C.client_id, C.last_name, C.first_name, CC.CaseID, O.MVAAFirstName, O.MVAALastName, O.MVAInsName, O.Total, " +
                  "O.ATotal, O.PlanNo, " +
                  "(select MAX(HPrev.OCFStatus) from HCAIStatusLog HPrev " +
                  "where HPrev.ReportID = H.ReportID AND HPrev.StatusOrder = (H.StatusOrder-1)) as [PrevStatus], " +
                  "(SELECT MIN(HD.StatusDate) FROM HCAIStatusLog HD " +
                  "WHERE H.OCFStatus > 11 AND H.ReportID = HD.ReportID AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate " +
                  "FROM HCAIStatusLog H inner join " +
                  "((select ReportID, MVAAFirstName, MVAALastName, MVAInsName, Total, ATotal, PlanNo from OCF18 UNION " +
                  "select ReportID, MVAAFirstName, MVAALastName, MVAInsName, Total, ATotal, PlanNo from OCF22 UNION " +
                  "select ReportID, MVAAFirstName, MVAALastName, MVAInsName, Total, ATotal, PlanNo from OCF23) as O " +
                  "inner join (PaperWork P inner join (Client_Case CC inner join Client C on CC.client_id = C.client_id) " +
                  "on P.CaseID = CC.CaseID) on O.ReportID = P.ReportID) on H.ReportID = O.ReportID " +
                  "Where h.IsCurrentStatus = 1 And h.Archived = 0 And h.OCFStatus <> 1  And O.ReportID = " + MyConvert.ConvertStrToDouble(OCFID));
            }

            try
            {
                if (reader.Read() == true)
                {
                    it = new BatchItem
                    {
                        OCFID = OCFID,
                        OCFType = OCFType,
                        NewStatus = NewOCFStatus,
                        OldStatus = OldOCFStatus,
                        StatusTime = MyReaderFldToString(reader, "StatusTime"),
                        StatusDate = MyReaderFldToString(reader, "StatusDate"),
                        ATotal = MyConvert.ConvertStrToFloat(MyReaderFldToString(reader, "ATotal")),
                        MinSentDate = MyReaderFldToDateStringMMDDYYYY(reader, "MinSentDate"),
                        HCAINumber = MyReaderFldToString(reader, "HCAIDocNumber")
                    };
                }
                else
                {
                    it = new BatchItem
                    {
                        OCFID = OCFID,
                        OCFType = OCFType,
                        NewStatus = NewOCFStatus,
                        OldStatus = OldOCFStatus,
                        MinSentDate = "",
                        HCAINumber = ""
                    };
                }
                reader.Close();
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error(e.Message, e);
                //error
                it = new BatchItem
                {
                    OCFID = OCFID,
                    OCFType = OCFType,
                    NewStatus = NewOCFStatus,
                    OldStatus = OldOCFStatus,
                    MinSentDate = "",
                    HCAINumber = ""
                };
            }

            return it;
        }

        private long GetErrorLogID(SqlCommand mCommand, string OCFID, string OCFType, bool IsResolved)
        {
            long HErrorID = 0;
            var reader = ExecQuery(mCommand, "select * " +
                                             "from HCAIErrorLog where OCFID = " + ConvertStrToLong(OCFID) + " AND OCFType = '" + OCFType + "'");
            try
            {
                if (reader.Read())
                {
                    HErrorID = ConvertStrToLong(MyReaderFldToString(reader, "HErrorID"));
                    if (ConvertStrToInt(MyReaderFldToString(reader, "IsResolved")) == 1)
                        IsResolved = true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error("OCF Doc " + OCFType + " <" + OCFID + ">. " + e.Message, e);
                //error
            }
            reader = null;
            return HErrorID;
        }

        private bool IsErrorInTheList(SqlCommand mCommand, string HCAI_Document_Number)
        {
            bool InList = false;

            var reader = ExecQuery(mCommand, "select * " +
                                             "from HCAIErrorLogItems where HCAI_Document_Number = '" + MyReplace(HCAI_Document_Number) + "'");

            try
            {
                if (reader.Read())
                {
                    if (ConvertStrToInt(MyReaderFldToString(reader, "OCFStatus")) > 0)
                        InList = true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error("OCF Doc " + HCAI_Document_Number + ". " + e.Message, e);
                //error
            }

            return InList;
        }

        private bool CreateHCAIErrorLog(SqlCommand mCommand, string OCFID, string OCFType, string ErrorMsg, int IsResolved)
        {
            var str_sql = "INSERT INTO HCAIErrorLog (OCFID, OCFType, ErrorMsg, IsResolved, CreateDateTime) " +
                          "VALUES (" + ConvertStrToLong(OCFID) + ", '" + OCFType + "', '" + MyReplace(ErrorMsg) + "', " +
                          IsResolved + ", '" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss tt") + "')";

            return ExecQueryForWrite(mCommand, str_sql);
        }

        private bool CreateHCAIErrorLogItem(SqlCommand mCommand, string HCAI_Document_Number, int OCFStatus, long HErrorID)
        {
            var str_sql = "INSERT INTO HCAIErrorLogItems (HCAI_Document_Number, OCFStatus, CreateDateTime, HErrorID) " +
                          "VALUES ('" + MyReplace(HCAI_Document_Number) + "', " + OCFStatus + ", " +
                          "'" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss tt") + "'," + HErrorID + ")";

            return ExecQueryForWrite(mCommand, str_sql);
        }

        private bool CreateErrorLog(SqlCommand mCommand, string HCAI_Document_Number, string OCFID, string OCFType, int OCFStatus, string ErrorMsg)
        {
            //1. check if error log already exists
            bool IsResolved = false;
            long HErrorID = GetErrorLogID(mCommand, OCFID, OCFType, IsResolved);

            if (HErrorID > 0)
            {
                if (IsResolved == true) //if it's resolved exit function
                {
                    return true;
                }
                else
                {
                    //check if this item with this issue already in the list
                    if (IsErrorInTheList(mCommand, HCAI_Document_Number) == true)
                    {
                        return true;
                    }
                    else
                    {
                        //create item with this issue
                        return CreateHCAIErrorLogItem(mCommand, HCAI_Document_Number, OCFStatus, HErrorID);
                    }
                }
            }
            else
            {
                // create error log
                if (CreateHCAIErrorLog(mCommand, OCFID, OCFType, ErrorMsg, 0) == true)
                {
                    HErrorID = GetErrorLogID(mCommand, OCFID, OCFType, IsResolved);
                    //create item with this issue
                    if (HErrorID > 0)
                    {
                        return CreateHCAIErrorLogItem(mCommand, HCAI_Document_Number, OCFStatus, HErrorID);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        private BatchItem CheckAndFixOCF(Activity OCFSummary, SqlCommand mCommand)
        {
            try
            {
                BatchItem it = null;
                int OCFStatus = ConvertExtractedHcaiStatus(OCFSummary.Document_Status.ToLower());
                
                if (OCFStatus > 0)
                {
                    string OCFID = "";
                    string OCFType;
                    int OCFStatusCur = 0;
                    int OCFLockStatus = 0;
                    var OCFStatusDate = new DateTime();

                    OCFType = GetOCFTypeFromHCAIStatusLog(mCommand, OCFSummary.HCAI_Document_Number, ref OCFStatusCur, ref OCFLockStatus, ref OCFStatusDate, ref OCFID);

                    if (OCFLockStatus == 1)
                    {
                        return it;
                    }

                    //if current status draft or hcai compliant exit function
                    if (OCFStatusCur <= 1 || OCFStatus == 9)
                    {
                        return it;
                    }

                    if (OCFType == "")
                    {
                        if (OCFStatus != (int)HcaiStatus.WithdrawGranted)
                        {
                            OCFType = GetOCFTypeFromHCAIStatusLogSearchAll(mCommand, OCFSummary.HCAI_Document_Number);
                            if (OCFType != "") //complete fixup required with CHANGE STATUS and Error Log
                            {
                                //CREATE ERROR LOG
                                //this method throws exceptions (eugene)
                                CreateErrorLog(mCommand, OCFSummary.HCAI_Document_Number, OCFSummary.PMS_Document_Key, OCFType, OCFStatus,
                                "This document has been submitted to HCAI more than once. This list below summarizes submission information.");
                            }
                        }
                    }
                    else
                    {
                        //check if ocf status is the same
                        if (OCFStatusCur != OCFStatus)
                        {
                            //IF DEEMED APPROVED ???
                            //Debugger.WriteToFile("Found inconsistency.", true);
                            //reset status to currend hcai status (basically SYNC ocf with hcai)
                            if (OCFStatus == (int)HcaiStatus.PartiallyApproved || OCFStatus == (int)HcaiStatus.Approved || OCFStatus == (int)HcaiStatus.NotApproved)
                            {
                                //EXTRACT ADJUSTER RESPONSE AND RE-ADJUDICATE OCF
                                //IsFixed = true;
                                var Succ = new OCFMapReturnType();
                                Debugger.WriteToFile("<<<<<<OCF_FIX\tExtracting document: " + OCFSummary.HCAI_Document_Number, 50);

                                try
                                {
                                    AdjusterResponse adRes = ExtractDocument(OCFSummary.HCAI_Document_Number, OCFType);
                                    Succ = UpdateDocumentInDb(adRes);
                                }
                                catch (Exception e)
                                {
                                    Debugger.WriteToFile("<<<<<<OCF_FIX\tException Error: " + e.Message, 50);
                                }

                                //IsFixed = true;
                                if (Succ != null)
                                {
                                    Debugger.WriteToFile("<<<<<<OCF_FIX\tFixing document: " + OCFSummary.HCAI_Document_Number, 50);
                                    it = new BatchItem
                                    {
                                        OCFID = OCFID, //OCFSummary.PMS_Document_Key;
                                        OCFType = Succ.OCFType, // OCFType;
                                        NewStatus = OCFStatus,
                                        OldStatus = OCFStatusCur,
                                        ATotal = Succ.ATotal,
                                        StatusDate = Succ.StatusDate,
                                        StatusTime = Succ.StatusTime,
                                        MinSentDate = Succ.MinSentDate,
                                        HCAINumber = Succ.HCAI_Document_Number
                                    };
                                }
                            }
                            else
                            {
                                if (OCFStatusCur != (int)HcaiStatus.DeemedApproved) //SKIP IF CURRENT STATUS DEEMED APPROVED
                                {
                                    //SIMPLE RESET OF OCF STATUS
                                    it = new BatchItem();
                                    it = ResetOCFStatusOnly(mCommand, OCFSummary.HCAI_Document_Number, OCFSummary.PMS_Document_Key, OCFType, OCFStatus, OCFStatusCur);
                                }
                                // IsFixed = true;
                            }
                        }
                        else
                        {
                            //everything is SYNCED 
                            //IsFixed = true;
                        }
                    }
                }
                return it;
            }
            catch (Exception e)
            {
                Debugger.WriteToFile("CheckAndFixOCF: Exception: " + e.Message + " Inner: " + e.InnerException.Message, 10);
                return null;
            }
        }

        public BatchItem CheckAndFixOCF_Extracted(AdjusterResponse adRes, HCAITableLineItem hItem)
        {
            if (!IsCredentialsValid())
            {
                DebugWrite("Hcai credentials are empty. CheckAndFixOCF_Extracted aborted.");
                return null;
            }

            var dbConnection = MyConvert.OpenSqlDbConnection(DB_CONNECTION_STRING);
            if (dbConnection == null) return null;

            using (dbConnection)
                try
                {
                    string OCFType;

                    BatchItem it = null;

                    using (var mCommand = dbConnection.CreateCommand())
                        if (hItem.HCAINumber != "" && hItem.OCFStatus > 0 && hItem.LockStatus == 0 && hItem.OCFType != "")
                        {
                            if (hItem.OCFType == "")
                            {
                                if (hItem.OCFStatus != (int)HcaiStatus.WithdrawGranted)
                                {
                                    OCFType = GetOCFTypeFromHCAIStatusLogSearchAll(mCommand, hItem.HCAINumber);
                                    if (OCFType != "")
                                    {
                                        //CREATE ERROR LOG
                                        //this method throws exceptions (eugene)
                                        CreateErrorLog(mCommand, hItem.HCAINumber, hItem.OCFID.ToString(), hItem.OCFType, hItem.OCFStatus,
                                            "This document has been submitted to HCAI more than once. This list below summarizes submission information.");
                                    }
                                }
                            }
                            else
                            {
                                int OCFStatus = ConvertExtractedHcaiStatus(adRes.Document_Status);

                                if (OCFStatus != hItem.OCFStatus)
                                {
                                    if (OCFStatus == (int)HcaiStatus.PartiallyApproved ||
                                        OCFStatus == (int)HcaiStatus.Approved || OCFStatus == (int)HcaiStatus.NotApproved)
                                    {
                                        OCFMapReturnType Succ = new OCFMapReturnType();
                                        Succ = UpdateDocumentInDb(adRes);
                                        if (Succ != null)
                                        {
                                            it = new BatchItem
                                            {
                                                OCFID = hItem.OCFID.ToString(),
                                                OCFType = Succ.OCFType,
                                                NewStatus = ConvertExtractedHcaiStatus(adRes.Document_Status),
                                                OldStatus = hItem.OCFStatus,
                                                ATotal = Succ.ATotal,
                                                StatusDate = Succ.StatusDate,
                                                StatusTime = Succ.StatusTime,
                                                MinSentDate = Succ.MinSentDate,
                                                HCAINumber = adRes.HCAI_Document_Number
                                            };
                                            return it;
                                        }
                                    }
                                    else
                                    {
                                        if (hItem.OCFStatus != (int)HcaiStatus.DeemedApproved) //SKIP IF CURRENT STATUS DEEMED APPROVED
                                        {
                                            //SIMPLE RESET OF OCF STATUS
                                            it = new BatchItem();
                                            it = ResetOCFStatusOnly(mCommand, adRes.HCAI_Document_Number,
                                                hItem.OCFID.ToString(), hItem.OCFType, OCFStatus, hItem.OCFStatus);
                                        }
                                    }
                                }
                            }
                        }

                    return null;
                }
                catch (Exception e)
                {
                    if (e.InnerException == null)
                        Debugger.WriteToFile("CheckAndFixOCF: Exception: " + e.Message, 10);
                    else
                        Debugger.WriteToFile("CheckAndFixOCF: Exception: " + e.Message + " Inner: " + e.InnerException.Message, 10);
                    return null;
                }
        }

        
        #endregion

        /////////////////////////////////////////////////////////////////////
        //#region CHANGE HCAI STATUS ONLY IN DATABASE
        //public bool ChangeHCAIStatusInDBOnly(string OCFID, string OCFType, string NewOCFStatus)
        //{ 
        //    //TO DO
        //    return true;
        //}
        //#endregion
        ///////////////////////////////////////////////////////////////////////
    }
}
