﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace UOServerLib
{
    public static class Debugger
    {
        private static readonly Mutex Mut = new Mutex();
        private const string FName = @"C:\Program Files\Antibex\Universal CPR\uoserver_debug.txt";

        public static void FlushFile()
        {
            Mut.WaitOne();
            if (File.Exists(FName))
                File.Delete(FName);
            Mut.ReleaseMutex();
        }

        public static void WriteToFile(string line, int level)
        {
            WriteToFile(line, level, false);
        }

        private static void WriteToFile(string line, int level, bool allBelow)
        {
#if OUTPUT_DEBUG_INFO_TO_LOG4NET
            UOServiceLib.Log.Info(line);
#endif

#if OUTPUT_DEBUG_INFO_TO_FILE
            if (level == Config.DebugLevel ||
                (allBelow && level < Config.DebugLevel))
            {
                mMut.WaitOne();
                File.AppendAllText(mFname, line + "\r\n");
                mMut.ReleaseMutex();
            }
#endif

#if OUTPUT_DEBUG_INFO_TO_CONSOLE
            mMut.WaitOne();
            Console.WriteLine(line);
            mMut.ReleaseMutex();
#endif

        }

        public static string SendEmail(string subject, string body)
        {
            try
            {
                var cr = new NetworkCredential("uoserver@antibex.com", "passc0de");
                var client = new SmtpClient("smtp.gmail.com", 587) {EnableSsl = true, Credentials = cr};
                client.Send("uoserver@antibex.com", "uoserver@antibex.com", subject, body);
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return null;
        }

        public static void LogInfo(string log)
        {
            UOServiceLib.Log.Info(log);
        }

        public static void LogError(string log)
        {
            UOServiceLib.Log.Error(log);
        }

        public static void LogError(string log, Exception e)
        {
            UOServiceLib.Log.Error(log, e);
        }

        public static void LogWarn(string log)
        {
            UOServiceLib.Log.Warn(log);
        }

        public static void LogDebug(string log)
        {
            UOServiceLib.Log.Debug(log);
        }
    }
}