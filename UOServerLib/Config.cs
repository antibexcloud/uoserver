﻿using System.IO;
using System.Xml;
using System;
using System.Globalization;

namespace UOServerLib
{
    public static class Config
    {
        public enum HcaiStatusBehavior
        {
            DocumentStatusBased,
            ApprovedAmtBased,
            DocumentStatusCustom
        }

        private const int MIN_ADJUSTER_RESPONSE_INTERVAL = 10800000; //3 hours

        private static bool mIsLoaded = false;
        private static bool mNeedsToBeSaved = false;
        private static int mMapReloadInterval = -1;
        private static int mAdjusterResponseInterval = -1;
        private static int mPort1 = -1;
        private static int mPort2 = -1;
        private static int mPendingInterval = -1;
        private static int mDebugLevel = -1;
        private static int mAdjusterResponseDuplicateMaxCounter = -1;
        private static bool mShowAllDebugsBelow = false;
        private static bool mStartAdjusterFixNow = true;
        private static HcaiStatusBehavior mHcaiStatusBehavior = HcaiStatusBehavior.DocumentStatusBased;
        private static int mCheckExtractLastHour = -1;
        private static bool mEnableEmailNotificationTriggers = false;
        private static int _checkPendingDaysLookUp = -1;
        private static int _activityListInterval = -1;
        private static DateTime _lastActivityCheckDate = new DateTime(2017, 1, 1);

        private static string mConfigName = @"C:\Program Files\Antibex\Universal CPR\UOServer\uoserver_config.xml";

        #region Get Functions

        public static bool ShowAllDebugsBelow
        {
            get
            {
                if (!mIsLoaded)
                {
                    LoadConfigFile();
                }
                return mShowAllDebugsBelow;
            }
        }

        public static int MapReloadInterval
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mMapReloadInterval;
            }
        }

        public static int AdjusterResponseInterval
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mAdjusterResponseInterval;
            }
        }

        public static int Port1
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mPort1;
            }
        }

        public static int Port2
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mPort2;
            }
        }

        public static int PendingInterval
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mPendingInterval;
            }
        }

        public static int DebugLevel
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mDebugLevel;
            }
        }

        public static int AdjusterResponseDuplicateMaxCounter
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mAdjusterResponseDuplicateMaxCounter;
            }
        }

        public static bool StartAdjusterFixNow
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mStartAdjusterFixNow;
            }
            set => mStartAdjusterFixNow = value;
        }

        public static HcaiStatusBehavior HcaiBehavior
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mHcaiStatusBehavior;
            }
        }

        public static int CheckExtractLastHour
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mCheckExtractLastHour;
            }
        }

        public static bool EnableEmailTriggers
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return mEnableEmailNotificationTriggers;
            }
        }

        public static int CheckPendingDaysLookUp
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return _checkPendingDaysLookUp;
            }
        }

        public static int ActivityListInterval
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return _activityListInterval;
            }
        }

        public static DateTime LastActivityCheckDate
        {
            get
            {
                if (!mIsLoaded) LoadConfigFile();
                return _lastActivityCheckDate;
            }
            set => _lastActivityCheckDate = value;
        }

        #endregion


        internal static void SaveConfig()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            var el1 = doc.CreateElement("settings");
            el1.SetAttribute("MapReloadInterval", mMapReloadInterval.ToString());
            el1.SetAttribute("AdjusterResponseInterval", mAdjusterResponseInterval.ToString());
            el1.SetAttribute("PendingInterval", mPendingInterval.ToString());
            el1.SetAttribute("Port1", mPort1.ToString());
            el1.SetAttribute("Port2", mPort2.ToString());
            el1.SetAttribute("debugLevel", mDebugLevel.ToString());
            el1.SetAttribute("AdjResDuplicateMaxCounter", mAdjusterResponseDuplicateMaxCounter.ToString());
            el1.SetAttribute("ShowAllDebugsBelow", mShowAllDebugsBelow.ToString());
            el1.SetAttribute("StartAdjusterFixNow", mStartAdjusterFixNow.ToString());
            el1.SetAttribute("HcaiStatusBehavior", ((int)mHcaiStatusBehavior).ToString());
            el1.SetAttribute("CheckExtractLastHour", (mCheckExtractLastHour).ToString());
            el1.SetAttribute("EnableTriggers", mEnableEmailNotificationTriggers.ToString());
            el1.SetAttribute("CheckPendingDaysLookUp", _checkPendingDaysLookUp.ToString());
            el1.SetAttribute("ActivityListInterval", _activityListInterval.ToString());
            el1.SetAttribute("LastActivityCheckDate", _lastActivityCheckDate.ToString("MM/dd/yyyy"));

            el.AppendChild(el1);

            try
            {
                doc.Save(mConfigName);
            }
            catch (Exception e)
            {
                var s = $"Failed to save UOServer config file: {mConfigName}. Exception: {e.Message}";
                Debugger.WriteToFile(s, 10);
            }
        }

        private static void LoadDefaultConfig()
        {
            if (mPort1 < 0)
                mPort1 = 5566;

            if (mPort2 < 0)
                mPort2 = 5567;

            if (mMapReloadInterval < 0)
                mMapReloadInterval = 1800000;   //30min

            if (mAdjusterResponseInterval < 0)
                mAdjusterResponseInterval = MIN_ADJUSTER_RESPONSE_INTERVAL; //3 hours

            if (mPendingInterval < 0)
                mPendingInterval = 30000; //30 sec
            
            if (mAdjusterResponseDuplicateMaxCounter <= 0)
                mAdjusterResponseDuplicateMaxCounter = 10000;

            if (mDebugLevel < 0)
                mDebugLevel = 0;

            if (mCheckExtractLastHour < 0)
                mCheckExtractLastHour = -1;

            if (_checkPendingDaysLookUp < 1)
                _checkPendingDaysLookUp = 120;

            if (_activityListInterval < 1)
                _activityListInterval = 3;

            if (_lastActivityCheckDate < DateTime.Now.AddMonths(-3))
                _lastActivityCheckDate = DateTime.Now.AddMonths(-3);
        }

        public static void LoadConfigFile()
        {
            if (File.Exists(mConfigName))
            {
                var doc = new XmlDocument();
                try
                {
                    doc.Load(mConfigName);
                    var el = (XmlElement)doc.DocumentElement.FirstChild;

                    mIsLoaded = true;
                    mMapReloadInterval = GetIntAttribute(el, "MapReloadInterval", 1800000);
                    mAdjusterResponseInterval = GetIntAttribute(el, "AdjusterResponseInterval", MIN_ADJUSTER_RESPONSE_INTERVAL);
                    mAdjusterResponseInterval = mAdjusterResponseInterval < MIN_ADJUSTER_RESPONSE_INTERVAL
                        ? MIN_ADJUSTER_RESPONSE_INTERVAL
                        : mAdjusterResponseInterval;
                    mPort1 = GetIntAttribute(el, "Port1", 5566);
                    mPort2 = GetIntAttribute(el, "Port2", 5567);
                    mPendingInterval = GetIntAttribute(el, "PendingInterval", 30000);
                    mDebugLevel = GetIntAttribute(el, "debugLevel", 0);
                    mAdjusterResponseDuplicateMaxCounter = GetIntAttribute(el, "AdjResDuplicateMaxCounter", 10000);
                    mCheckExtractLastHour = GetIntAttribute(el, "CheckExtractLastHour", -1);
                    _checkPendingDaysLookUp = GetIntAttribute(el, "CheckPendingDaysLookUp", 120);
                    _activityListInterval = GetIntAttribute(el, "ActivityListInterval", 3);
                    _lastActivityCheckDate = GetIntAttribute(el, "LastActivityCheckDate", DateTime.Now.AddMonths(-3));

                    mHcaiStatusBehavior = GetIntAttribute(el, "HcaiStatusBehavior", HcaiStatusBehavior.DocumentStatusBased);

                    mShowAllDebugsBelow = GetIntAttribute(el, "ShowAllDebugsBelow", false);
                    mStartAdjusterFixNow = GetIntAttribute(el, "StartAdjusterFixNow", false);
                    mEnableEmailNotificationTriggers = GetIntAttribute(el, "EnableTriggers", false);
                }
                catch(Exception e)
                {
                    Debugger.WriteToFile("Failed to read config file: " + e.Message, 10);
                    mNeedsToBeSaved = true;
                }
            }
            else
            {
                mNeedsToBeSaved = true;
            }

            if (mNeedsToBeSaved)
            {
                LoadDefaultConfig();
                SaveConfig();
                mNeedsToBeSaved = false;
            }
            mIsLoaded = true;
        }


        private static DateTime GetIntAttribute(XmlElement el, string attName, DateTime defaultVal)
        {
            var date = "";
            try
            {
                date = el.GetAttribute(attName);
                return DateTime.ParseExact(date, "MM/dd/yyyy", new CultureInfo("en-US"));
            }
            catch (Exception)
            {
                if (string.IsNullOrEmpty(date))
                    date = "";
                Debugger.WriteToFile(String.Format("Config file is missing {0} attribute or date time cannot be parsed. Value Extracted: \"{2}\" Excepcted format is MM/dd/yyy. Loading default value: {1}", attName, defaultVal, date), 10);
                mNeedsToBeSaved = true;
                return defaultVal;
            }
        }

        private static bool GetIntAttribute(XmlElement el, string attName, bool defaultVal)
        {
            try
            {
                return Convert.ToBoolean(el.GetAttribute(attName));
            }
            catch (Exception)
            {
                Debugger.WriteToFile($"Config file is missing {attName} attribute. Loading default value: {defaultVal}", 10);
                mNeedsToBeSaved = true;
                return defaultVal;
            }
        }

        private static int GetIntAttribute(XmlElement el, string attName, int defaultVal)
        {
            try
            {
                return Convert.ToInt32(el.GetAttribute(attName));
            }
            catch (Exception)
            {
                Debugger.WriteToFile($"Config file is missing {attName} attribute. Loading default value: {defaultVal}", 10);
                mNeedsToBeSaved = true;
                return defaultVal;
            }
        }

        private static HcaiStatusBehavior GetIntAttribute(XmlElement el, string attName, HcaiStatusBehavior defaultVal)
        {
            try
            {
                return (HcaiStatusBehavior)GetIntAttribute(el, attName, (int)defaultVal);
            }
            catch (Exception)
            {
                Debugger.WriteToFile($"HcaiStatusBehavior attribute is out of bounds. Loading default value: {defaultVal}", 10);
                mNeedsToBeSaved = true;
                return defaultVal;
            }
        }
    }
}
