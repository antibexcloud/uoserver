﻿#define OUTPUT_DEBUG_INFO_TO_LOG4NET

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System.Net;
using System.Xml.Serialization;
using EdtClient;

namespace UOServerLib
{
    public delegate void NewMessage(string s);

    public class ClientConnection
    {
        //Connection data
        private readonly Thread _thread;
        private readonly Thread _outgoingThread;
        private Socket _connectionIn;
        private Socket _connectionOut;
        private readonly int _portIn;

        private readonly object _cmdMutex = new object();
        private readonly List<UOMessage> _sendCommands; //Queue of outgoing commands

        private bool _threadStopped;    //Control variable for quitting the thread execution

        //Client data
        private string _dbName;
        private string _clientName;
        private readonly int _clientNumber;

        private string _dbConnectionString;
        private string _serverName;

        //HCAI members
        private HCAIModule _hcai;

        //HC Validation module
        private EdtClientLib _hcvModule;
        private readonly object _hcvModuleLock = new object();

        private readonly UOServiceLib _parent; //Parent reference

        //Misc
        private readonly Random _rand;
        private byte[] _bufOut;
        private readonly byte[] _bufIn = new byte[32767];

        private DateTime InCommandStartTime {get; set;}
        private DateTime OutCommandStartTime { get; set; }
        public DateTime CreationTime { get; private set; }
        private static readonly TimeSpan CommandMaxExecTime = new TimeSpan(0, 5, 0);

        private ClientStatus InThreadStatus { get; set; }
        private ClientStatus OutThreadStatus { get; set; }

        //TMP Mutex
        private readonly object _sendMut = new object();
        private readonly object _recvMut = new object();

        public event NewMessage NewMassageEvent;

        private void FireNewMessageEvent(string s)
        {
            if (NewMassageEvent == null) return;

            var eventHandlers = NewMassageEvent.GetInvocationList();
            foreach (var d in eventHandlers)
            {
                var target = d.Target as ISynchronizeInvoke;

                if (target != null && target.InvokeRequired)
                {
                    target.Invoke(d, new object[] { s });
                }
                else
                {
                    d.DynamicInvoke(new object[] { s });
                }
            }
        }

        public TimeSpan InCommandRunningTime
        { 
            get 
            {
                var res = new TimeSpan(0, 0, 0);
                if (InThreadStatus != ClientStatus.Idle)
                {
                    res = DateTime.Now - InCommandStartTime;
                }
                return res; 
            }
        }

        public TimeSpan OutCommandRunningTime
        {
            get
            {
                var res = new TimeSpan(0, 0, 0);
                if (OutThreadStatus != ClientStatus.Idle)
                {
                    res = DateTime.Now - OutCommandStartTime;
                }
                return res;
            }
        }

        public string ClientName => _clientName;

        public ClientConnection(int number, Socket connIn, Socket connOut, UOServiceLib parent)
        {
            _thread = new Thread(new ThreadStart(RunClient));
            _outgoingThread = new Thread(new ThreadStart(RunClientOutgoing));
            _connectionIn = connIn;
            _connectionOut = connOut;
            _clientNumber = number;
            InCommandStartTime = DateTime.Now;
            OutCommandStartTime = DateTime.Now;
            CreationTime = DateTime.Now;

            _connectionIn.ReceiveTimeout = 0;
            _connectionOut.ReceiveTimeout = 0;
            //_connectionIn.NoDelay = true;
            //_connectionOut.NoDelay = true;
            
            _portIn = ((IPEndPoint)connIn.RemoteEndPoint).Port;
            _sendCommands = new List<UOMessage>();
            _threadStopped = false;
            _parent = parent;
            _rand = new Random(DateTime.Now.Millisecond);
        }

        public string DBName => _dbName;

        public void Start()
        {
            if (_thread.ThreadState == ThreadState.Unstarted)
                _thread.Start();
        }

        public void Stop()
        {
            var msg = new UOMessage(Commands.DISCONNECT);
            AddCommand(msg);
            if (_thread.Join(2000)) return;

            _threadStopped = true;
            _thread.Abort();
            _outgoingThread.Abort();
        }

        public bool IsRunning()
        {
            return _thread.IsAlive && _outgoingThread.IsAlive && _connectionIn.Connected && _connectionOut.Connected;
        }

        public bool HasBeenRunningTooLong()
        {
            return (DateTime.Now - CreationTime) > new TimeSpan(12, 0, 0);
        }

        public bool IsStuck()
        {
            return (InThreadStatus != ClientStatus.Idle && InCommandRunningTime > CommandMaxExecTime) ||
                   (OutThreadStatus != ClientStatus.Idle && OutCommandRunningTime > CommandMaxExecTime);
        }

        public void SendStringToClient(string msg)
        {
            var cmd = new UOMessage(Commands.TEST_CON) {Params = new object[1]};
            cmd.Params[0] = msg;

            AddCommand(cmd);
        }

        public void SendHcaiTable()
        {
            var cmd = new UOMessage(Commands.SEND_HCAI_TABLE);
            AddCommand(cmd);
        }


        private int SendString2(Socket conn, string msg, int msgID)
        {
            Debugger.WriteToFile("send string to uoclient" + msg.Substring(0, Math.Min(100, msg.Length)), 50);
            var size = -1;

            try
            {
                lock (_sendMut)
                {
                    //var ep = (IPEndPoint) conn.RemoteEndPoint;
                    //var str = ep.Port == _portIn
                    //    ? "<<<<< id=" + msgID + " Send String IN to: "
                    //    : "<<<<< id=" + msgID + " Send String OUT to: ";
                    //Debugger.WriteToFile(str + mClientName + ":" + mClientNumber);

                    var sz = msg.Length;

                    //send ID
                    _bufOut = BitConverter.GetBytes(msgID);
                    size = conn.Send(_bufOut, 4, SocketFlags.None);

                    //send size
                    _bufOut = BitConverter.GetBytes(sz);
                    size = conn.Send(_bufOut, 4, SocketFlags.None);

                    _bufOut = Encoding.ASCII.GetBytes(msg);
                    size = conn.Send(_bufOut, msg.Length, SocketFlags.None);

                    //Debugger.WriteToFile(msg + "\r\n id=" + msgID + ">>>>>>>>>>>>>>>>>\r\n");
                }
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error("EXCEPTION\r\n id=" + msgID + ">>>>>>>>>>>>>>>>>\r\n" + e.Message, e);
                Debugger.WriteToFile("EXCEPTION\r\n id=" + msgID + ">>>>>>>>>>>>>>>>>\r\n" + e.Message, 30);
            }
            return size;
        }

        private string ReceiveString2(Socket conn, int msgID)
        {
            return ReceiveString2(conn, ref msgID);
        }

        private string ReceiveString2(Socket conn, ref int msgID)
        {
            int size;
            var c = 0;
            var timeout = 60; // 60;
            string msg = null;

            try
            {
                lock (_recvMut)
                {
                    var port = "OUT";
                    var ep = (IPEndPoint) conn.RemoteEndPoint;
                    if (ep.Port == _portIn)
                        port = "IN";

                    //Debugger.WriteToFile("<<<<< Read String on port: " + port + " " + mClientName + ":" + mClientNumber);
                    while (c < timeout)
                    {
                        if (conn.Available > 7)
                        {
                            size = conn.Receive(_bufIn, 8, SocketFlags.None);
                            break;
                        }
                        c++;
                        Thread.Sleep(50);
                    }

                    if (c >= timeout)
                    {
                        //did not get any data, quit
                        Debugger.WriteToFile("<<<<< remoteId=" + msgID + " Read String on port: " + port + " " + _clientName + ":" +
                                            _clientNumber + " \r\nTimeout on receive.", 30);
                        msg = "";
                    }
                    else
                    {
                        msgID = BitConverter.ToInt32(_bufIn, 0);
                        var msgLength = BitConverter.ToInt32(_bufIn, 4);

                        //Debugger.WriteToFile("<<<<< remoteIid=" + msgID + " Read String on port: " + port + " " + mClientName + ":" + mClientNumber);

                        size = 0;
                        c = 0;
                        while (size < msgLength && c < timeout)
                        {
                            if (conn.Available > 0)
                            {
                                size += conn.Receive(_bufIn, size, msgLength - size, SocketFlags.None);
                                c = 0;
                            }
                            c++;
                            Thread.Sleep(50);
                        }

                        if (c >= timeout)
                        {
                            //did not get any data, quit
                            Debugger.WriteToFile("<<<<< remoted=" + msgID + " Read String on port: " + port + " " + _clientName + ":" +
                                                _clientNumber + " \r\nTimeout on receive. No Data.", 30);
                            msg = "";
                        }
                        else
                        {
                            msg = Encoding.ASCII.GetString(_bufIn, 0, size);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error(" EXCEPTION\r\n remoteId=" + msgID + " Read>>>>>>>>>>>>>>\r\n" + e.Message, e);
                Debugger.WriteToFile(" EXCEPTION\r\n remoteId=" + msgID + " Read>>>>>>>>>>>>>>\r\n" + e.Message, 30);
            }
            return msg;
        }

        //This method processes outgoing messages
        private int SendUOMessage(UOMessage msg, int id)
        {
            var result = 0;
            int size;

            string cmd;

            if (msg.Command == Commands.APPT_STRING ||
                msg.Command == Commands.HCV_VALIDATE_CARD)
            {
                cmd = Commands.SEP[0] + msg.Command + Commands.SEP[0] + (string)msg.Params[0] + Commands.SEP[0];

                size = SendString2(_connectionOut, cmd, id);

                if (size != cmd.Length)
                    result = -1;
            }
            else if (msg.Command == Commands.HCAI_DOES_DOC_EXIST_ON_SERVER)
            {
                cmd = Commands.SEP[0] + msg.Command;
                for (var i = 0; i < ((ICollection) msg.Params).Count; i++)
                {
                    cmd += Commands.SEP[0] + (string)msg.Params[i];
                }
                cmd += Commands.SEP[0];

                size = SendString2(_connectionOut, cmd, id);

                if (size != cmd.Length)
                    result = -1;
            }
            else if (msg.Command == Commands.SEND_HCAI_TABLE)
            {
                /*                cmd = Commands.SEND_HCAI_TABLE;
                                SendString2(mConnectionOut, cmd, id);
                
                                SendHCAITable(mConnectionOut, id);
                */
            }
            else if (msg.Command == Commands.SEND_HCAI_TABLE_GROUP)
            {
                /*               cmd = Commands.SEND_HCAI_TABLE_GROUP;
                               SendString2(mConnectionOut, cmd, id);
                               int t = (int)msg.Params[0];
                               SendHCAITable(mConnectionOut, t, id);
                 */
            }
            else if (msg.Command == Commands.SENDING_HCAI_TABLE_LEFT_SIDE)
            {
                result = SendHcaiTableMapLeftSide(_connectionOut, id);
            }
            else if (msg.Command == Commands.TEST_CON)
            {
                cmd = Commands.SEP[0] + msg.Command + Commands.SEP[0] + (string)msg.Params[0] + Commands.SEP[0];

                size = SendString2(_connectionOut, cmd, id);
                if (size != cmd.Length)
                    result = -2;
            }
            else if (msg.Command == Commands.HCAI_CHANGE_STATUS_AND_SUBMIT)
            {
                //this code is executed only during batch submit
                var ocfId = (string)msg.Params[0];
                var ocfType = (string)msg.Params[1];
                SubmitToHCAI(ocfId, ocfType);
                
                result = -10; //this means no response from client is expected
                //                SendHcaiTableMapLeftSide(mConnectionOut);
            }
            else if (msg.Command == Commands.HCAI_WITHDRAW_DOCUMENT)
            {
                //this code is executed only during batch withdraw
                var ocfId = (string)msg.Params[0];
                var ocfType = (string)msg.Params[1];
                var hcaiDocumentNumber = (string)msg.Params[2];

                var ret = _hcai.VoidDocumentRequest(ocfId, ocfType, hcaiDocumentNumber);

                //withdrawn successfully
                //change map

                var map = _parent.GetMap(_serverName, _dbName);
                try
                {
                    map?.UpdateDocumentStatus(ret.OCFID, ret.OCFType, ret.New_HCAI_Status, "");
                }
                catch (Exception e)
                {
                    Debugger.WriteToFile("\tSend Message HCAI_WITHDRAW_DOCUMENT Exception: " + ". Message: " + e.Message, 30);
                }
                finally
                {
                    _parent.ReleaseMap(map);
                }
                
                result = -10;  //this means no response from client is expected
            }
            else if (msg.Command == Commands.HCAI_BATCH_FINISHED)
            {
                var m = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                SendMessageToAllButMe(m);
                SendHcaiTableMapLeftSide(_connectionOut, id);
            }

            return result;
        }
        
        public void AddCommand(UOMessage cmd)
        {
            lock (_cmdMutex)
            {
                _sendCommands.Add(cmd);
            }
        }

        private UOMessage GetCommand()
        {
            lock (_cmdMutex)
            {
                if (_sendCommands.Count <= 0) return null;

                var cmd = _sendCommands[0];
                _sendCommands.RemoveAt(0);

                ////DEBUGING MESSAGE REMOVAL MUST BE DELETED
                //if(mSendCommands[0].Command == Commands.HCAI_CHANGE_STATUS_AND_SUBMIT)
                //{
                //    mSendCommands.Add(cmd);
                //    cmd = null;
                //}

                return cmd;
            }
        }

        public void RemoveOldMessages()
        {
            Debugger.WriteToFile("RemoveOldMessages: Checking and removing old messages from client queue: " + ClientName, 100);
            var batch = new Batch();
            lock (_cmdMutex)
            {
                var tmp = new List<UOMessage>();
                foreach (var msg in _sendCommands)
                {
                    var dt = DateTime.Now - msg.TimeStamp;
                    if (dt <= CommandMaxExecTime) continue;

                    Debugger.WriteToFile("Message is too old: " + dt, 100);
                    Debugger.WriteToFile("Message command: " + msg.Command + ". Msg will be removed.", 100);
                    tmp.Add(msg);
                }

                var map = _parent.GetMap(_serverName, _dbName);
                try
                {
                    if (map != null)
                    {
                        foreach (var msg in tmp)
                        {
                            _sendCommands.Remove(msg);
                            if (msg.Command != Commands.HCAI_CHANGE_STATUS_AND_SUBMIT) continue;

                            var ocfid = (string) msg.Params[0];
                            var ocftype = (string) msg.Params[1];
                            Debugger.WriteToFile("Submitting document is in the queue: id = " + ocfid + "; type = " + ocftype, 100);
                            var item = map.FindLineItem(ocfid, ocftype);
                            if (item == null || item.OCFStatus != (int) HcaiStatus.Submitted) continue;

                            Debugger.WriteToFile("Found stuck document: id = " + ocfid + "; type = " + ocftype, 100);
                            map.UpdateDocumentStatus(ocfid, ocfid, HcaiStatus.Submitted, item.MinSentDate);
                            var it = new BatchItem
                            {
                                OCFID = item.OCFID.ToString(),
                                OCFType = item.OCFType,
                                MinSentDate = item.MinSentDate,
                                NewStatus = (int) HcaiStatus.DeliveryFailed,
                                HCAINumber = ""
                            };

                            Debugger.WriteToFile("Changing status to " + HcaiStatus.DeliveryFailed, 100);
                            map.UpdateDocumentStatus(it.OCFID, it.OCFType, HcaiStatus.DeliveryFailed, it.MinSentDate);
                            batch.AddItem(it);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debugger.WriteToFile("\tRemoveOldMessages Exception: " + ". Message: " + e.Message, 30);
                }
                finally
                {
                    _parent.ReleaseMap(map);
                }
            }

            DBUpdate(batch);
            Debugger.WriteToFile("RemoveOldMessages: Finished with client " + ClientName, 100);
        }

        private SqlDataReader ExecQuery(SqlCommand command, string strSql)
        {
            try
            {
                command.CommandText = strSql;
                var reader = command.ExecuteReader();
                //reader.Read();
                return reader;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("ExecQuery Exception. StrSql = '" + strSql + "'" + ex.Message, ex);
                return null;
            }
        }

        private bool ExecQueryForWrite(SqlCommand command, string strSql)
        {
            if (command is null) return false;

            try
            {
                command.CommandText = strSql;
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("ExecQueryForWrite Exception. StrSql = '" + strSql + "'" + ex.Message, ex);
                return false;             
            }
        }

        private readonly object _closeMutex = new object();

        private void CloseConnections()
        {
            lock (_closeMutex)
            {
                if (_connectionIn != null)
                {
                    if (_connectionIn.Connected)
                    {
                        _connectionIn.Shutdown(SocketShutdown.Both);
                        _connectionIn.Close();
                    }
                    _connectionIn = null;
                }
                if (_connectionOut != null)
                {
                    if (_connectionOut.Connected)
                    {
                        _connectionOut.Shutdown(SocketShutdown.Both);
                        _connectionOut.Close();
                    }
                    _connectionOut = null;
                }
            }
        }

        /// <summary>
        /// Main Thread loop
        /// </summary>     
        private void RunClient()
        {
            int res = 0;
            string msg;
            //UOMessage cmd;
            try
            {
                Debugger.WriteToFile("Initializing connection.", 30);
                InThreadStatus = ClientStatus.EstablishingConnection;
                //inform client that connection is successful
                var size = SendString2(_connectionOut, Commands.CON_SUCCESS, 0);

                InThreadStatus = ClientStatus.ReceivingConnectionParameters;
                //(initialization) receive client parameters
                msg = ReceiveString2(_connectionOut, ref res);
                if (!string.IsNullOrEmpty(msg))
                {
                    Debugger.WriteToFile("Parameters Received.", 30);
                    InThreadStatus = ClientStatus.ProcessingParameters;

                    //Parsing the parameters sent by the client
                    res = ProcessCommand(msg, 0);
                    if (res == 0)
                    {
                        InThreadStatus = ClientStatus.ProcessingParameters;
                        var dbConnection = MyConvert.OpenSqlDbConnection(_dbConnectionString);

                        if (dbConnection != null)
                        {
                            using (dbConnection)
                            using (var command = dbConnection.CreateCommand())
                            {
                                var reader = ExecQuery(command, @"SELECT * FROM Business_Center");

                                if (reader.Read())
                                {
                                    var tHCAILogin = MyConvert.MyReaderFldToString(reader, "HCAILogin");
                                    var tHCAIPassword = MyConvert.MyReaderFldToString(reader, "HCAIPassword");
                                    var tHCAIID = MyConvert.MyReaderFldToString(reader, "HCAIID");

                                    _hcai = new HCAIModule(_serverName, _dbName, tHCAILogin, tHCAIPassword, tHCAIID)
                                    {
                                        BusinessName = MyConvert.MyReaderFldToString(reader, "BusinessName"),
                                        BusinessPhone = MyConvert.MyReaderFldToString(reader, "PNLine1"),
                                        WebID = MyConvert.MyReaderFldToString(reader, "ReleaseDate")
                                    };

                                    Debugger.WriteToFile("Creating HCV Module", 30);
                                    Debugger.WriteToFile("Working dir: " + Environment.CurrentDirectory, 30);

                                    InitializeHcvModule(reader);
                                }

                                reader.Close();

                                SendString2(_connectionOut, Commands.RECV_OK, 0);

                                Debugger.WriteToFile("Client " + _clientNumber + " Started", 30);
                                FireNewMessageEvent("Client Started");

                                _thread.Name = _clientName + "_In:" + _clientNumber;
                                _outgoingThread.Name = _clientName + "_Out:" + _clientNumber;

                                InThreadStatus = ClientStatus.ConnectionEstablished;
                                if (_outgoingThread.ThreadState == ThreadState.Unstarted)
                                    _outgoingThread.Start();
                            }
                        }
                        else
                        {
                            FireNewMessageEvent("DB Connection cannot be opened for " + _serverName);
                            SendString2(_connectionOut, Commands.ERROR, 0);
                            _threadStopped = true;
                        }
                    }
                    else
                    {
                        FireNewMessageEvent("Initial communication with the server has failed. Connection aborted");
                        SendString2(_connectionOut, Commands.ERROR, 0);
                        _threadStopped = true;
                    }
                }
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error("Exception on initial connection. Connection aborted. " + e.Message, e);
                FireNewMessageEvent("Exception on initial connection. Connection aborted");
                SendString2(_connectionOut, Commands.ERROR, 0);
                Debugger.WriteToFile(_clientName + ":" + _clientNumber + ": Exception on connection: " + e.Message, 30);
                _threadStopped = true;
            }

            try
            {
                var id = 0;
                //MAIN PROCESSING LOOP
                while (!_threadStopped && _connectionIn.Connected && _connectionOut.Connected)
                {
                    InThreadStatus = ClientStatus.Idle;
                    //checking if there is data available from the client
                    if (_connectionIn.Available > 0)
                    {
                        InThreadStatus = ClientStatus.NewCommandIsAvailable;
                        InCommandStartTime = DateTime.Now;
                        //recieving the command from client
                        msg = ReceiveString2(_connectionIn, ref id);
                        InThreadStatus = ClientStatus.CommandReceived;
                        FireNewMessageEvent("Client's Message: " + _clientName + ":" + _clientNumber + " " + msg);

                        Debugger.WriteToFile("Incoming Command: " + msg + " ===" + _clientName + ":" + _clientNumber + ": id=" + id, 30);
                        res = ProcessCommand(msg, id);
                        InThreadStatus = ClientStatus.CommandProcessed;

                        //letting the client know that server received and processed the command
                        if (res == 0)
                        {
                            Debugger.WriteToFile("Incoming Command (Sending RECV_OK): ===" + _clientName + ":" + _clientNumber + ": id=" + id + "\r\n", 30);
                            SendString2(_connectionIn, Commands.RECV_OK, id);
                        }
                        else if (res == -10)
                        {
                            //unrecognized command
                            Debugger.WriteToFile("Incoming Command (Sending ERROR_UNKNOWN_COMMAND): ===" + _clientName + ":" + _clientNumber + ": id=" + id + " Error code: " + res + "\r\n", 30);
                            SendString2(_connectionIn, Commands.ERROR_UNKNOWN_COMMAND, id);
                        }
                        else
                        {
                            Debugger.WriteToFile("Incoming Command (Sending ERROR): ===" + _clientName + ":" + _clientNumber + ": id=" + id + " Error code: " + res + "\r\n", 30);
                            SendString2(_connectionIn, Commands.ERROR, id);
                        }
                        //InCommandStartTime = NULL_TIME;
                    }

                    Thread.Sleep(200);
                }
                
                //exiting the thread and closing the network connection
                CloseConnections();

                Debugger.WriteToFile(_clientName + ":" + _clientNumber + ": Client Exited Incoming Thread", 30);
            }
            catch (Exception error)
            {
                UOServiceLib.Log.Fatal("Exception in clients IN loop. Exiting. " + error.Message, error);
                //this shit throws exception on exit
                //               FireNewMessageEvent("Exception, exiting Incoming thread: " + error.Message);
                CloseConnections();
                Debugger.WriteToFile(_clientName + ":" + _clientNumber + ": Exception in incoming thread: " + error, 30);
            }
            FireNewMessageEvent("Client " + _clientNumber + " Exited");
        }
        
        private void RunClientOutgoing()
        {
            var buf = new byte[1024];

            try
            {
                var remId = 0;
                while (!_threadStopped && _connectionIn.Connected && _connectionOut.Connected)
                {
                    OutThreadStatus = ClientStatus.Idle;
                    //checking if we have a command to send to the client
                    var cmd = GetCommand();
                    if (cmd != null)
                    {
                        OutThreadStatus = ClientStatus.NewCommandIsAvailable;
                        OutCommandStartTime = DateTime.Now;
                        //flushing the connection buffer if something got stuck in there
                        while (_connectionOut.Available > 0)
                        {
                            var m = Math.Min(1024, _connectionOut.Available);
                            _connectionOut.Receive(buf, m, SocketFlags.None);
                        }

                        OutThreadStatus = ClientStatus.SendingCommand;
                        var id = _rand.Next(100000);
                        Debugger.WriteToFile(" Outgoing command:" + cmd.Command + "===" + _clientName + ":" + _clientNumber + ": id=" + id, 30);
                        var res = SendUOMessage(cmd, id);

                        OutThreadStatus = ClientStatus.CommandSent;
                        if (res >= 0)
                        {
                            //waiting for the client to acknowledge the transmission
                            var msg = ReceiveString2(_connectionOut, ref remId);
                            Debugger.WriteToFile("OUT Port Recieved ack: " + msg + " :::" + _clientName + ":" + _clientNumber + "id=" + id + " remoteID=" + remId + "\r\n", 30);
                        }
                        else if (res == -10)
                        {
                            //no action is required since are not sending any info back
                        }
                        else
                        {
                            Debugger.WriteToFile("Error occurred while sending a commnad: " + cmd.Command + " :::" + _clientName + ":" + _clientNumber + "id=" + id + "; Error code = " + res + "\r\n", 30);
                        }
                        //OutCommandStartTime = NULL_TIME;
                    }

                    Thread.Sleep(200);
                }

                //exiting the thread and closing the network connection
                CloseConnections();
                Debugger.WriteToFile(_clientName + ":" + _clientNumber + ": Client Exited Outgoing Thread", 30);
            }
            catch (Exception error)
            {
                UOServiceLib.Log.Fatal("Exeption in client's OUT loop. Exiting. " + error.Message, error);
                //FireNewMessageEvent(mClientName + ":" + mClientNumber + "Exception, exiting out thread: " + error.Message);
                CloseConnections();
                Debugger.WriteToFile(_clientName + ":" + _clientNumber + "Exception, exiting out thread: " + error.Message, 30);
            }

            FireNewMessageEvent("Client Exited");
        }

        private bool ToFileDBUpdate(BatchItem b)
        {
            var dbConnection = MyConvert.OpenSqlDbConnection(_dbConnectionString);
            if (dbConnection == null) return false;

            using (dbConnection)
            using (var command = dbConnection.CreateCommand())
            {
                var sqlStr = MyConvert.MyLeft(b.OCFType, 5) == "OCF21"
                    ? "UPDATE HCAIStatusLog SET Archived = 1" +
                      "  WHERE IsCurrentStatus = 1 AND InvoiceID = '" + b.OCFID + "'"
                    : "UPDATE HCAIStatusLog SET Archived = 1" +
                      "  WHERE IsCurrentStatus = 1 AND ReportID = '" + b.OCFID + "'";
                var t = ExecQueryForWrite(command, sqlStr);
                return t;
            }
        }

        //This method processes incoming messages
        private int ProcessCommand(string msg, int id)
        {
            var result = -1;
            try
            {
                var args = msg.Split(Commands.SEP, StringSplitOptions.None);

                var cmd = args[1];
                if (cmd == Commands.TEST_CON)
                {
                    FireNewMessageEvent(args[2]);
                    result = 0;

                    var uomsg = new UOMessage(Commands.TEST_CON) {Params = new object[1]};
                    uomsg.Params[0] = args[2];

                    SendMessageToAllButMe(uomsg);

                }
                else if (cmd == Commands.HCV_VALIDATE_CARD)
                {
                    var hcvRequest = new HcvRequest
                    {
                        HealthNumber = args[2],
                        VersionCode = args[3],
                        FeeServiceCodes = args.Skip(4)
                    };
                    var res = ValidateHealthCard(hcvRequest);
                    var x = new XmlSerializer(res.GetType());
                    using (var writer = new StringWriter())
                    {
                        x.Serialize(writer, res);
                        var tmpmsg = new UOMessage(Commands.HCV_VALIDATE_CARD){Params = new object[1]};
                        tmpmsg.Params[0] = writer.ToString();
                        AddCommand(tmpmsg);
                        //if (SendString2(mConnectionIn, writer.ToString(), id) > 0)
                        //{
                        //    result = 0;
                        //}
                    }
                }
                else if (cmd == Commands.RESTART_SERVER)
                {
                    Debugger.WriteToFile("Client command: Restart Server.", 10); //Eugene-Added
                    _parent.RestartService(null);
                }
                else if (cmd == Commands.CLIENT_PARAMS)
                {
                    var tmp = args[2]; //1. server name
                    var host = Dns.GetHostName();
                    var index = tmp.IndexOf("\\");

                    _serverName = index < 0 ? host : host + tmp.Substring(index);
                    _serverName = _serverName.ToUpper();

                    _dbName = args[3].ToUpper(); //2. db name
                    _clientName = args[4]; //3. dummy client name

                    _dbConnectionString = @"Database=" + _dbName + ";User Id=uouser; password=passc0de;Server=" + _serverName;
                    
                    result = 0;
                }
                else if (cmd == Commands.DISCONNECT)
                {
                    //removing reference to the connection from the list of connectons
                    _parent.RemoveClientFromArray(this);

                    //stopping the thread
                    _threadStopped = true;
                    result = 0;

                    FireNewMessageEvent("Client's Message: " + _clientName + ":" + _clientNumber + " Disconnected");
                }
                else if (cmd == Commands.APPT_STRING_SEND_ALL_BUT_ME)
                {
                    //FORWARD THIS MESSAGE TO ALL CLIENTS (BUT THIS ONE) THAT ARE CONNECTED TO THE SAME DATABASE

                    var uomsg = new UOMessage(Commands.APPT_STRING) {Params = new object[1]};
                    uomsg.Params[0] = args[2];

                    SendMessageToAllButMe(uomsg);

                    result = 0;
                }
                else if (cmd == Commands.GET_HCAI_INSURERS)
                {
                    //GET HCAI INSURERS AND FORWARD TO REQESTED CLIENT ONLY

                    var ins = _hcai.getInsurerList();
                    var doc = ins.CreateXMLDocument();

                    //if (SendXMLDocument(mConnectionIn, doc, id) > 0)
                    if (SendString2(_connectionIn, doc.OuterXml, id) > 0)
                    {
                        result = 0;
                    }
                }
                else if (cmd == Commands.GET_HCAI_PROVIDERS)
                {
                    //GET HCAI PROVIDERS AND FORWARD TO REQESTED CLIENT ONLY

                    var fac = _hcai.getFacilityList();
                    var doc = fac.CreateXMLDocument();

                    //if (SendXMLDocument(mConnectionIn, doc, id) > 0)
                    if (SendString2(_connectionIn, doc.OuterXml, id) > 0)
                    {
                        result = 0;
                    }
                }
                else if (cmd == Commands.HCAI_CHANGE_STATUS_AND_SUBMIT_BATCH)
                {
                    var doc = new XmlDocument();
                    var mem = new MemoryStream(Encoding.ASCII.GetBytes(args[2]));
                    doc.Load(mem);

                    var bt = new Batch();
                    bt.ParseXmlDocument(doc);

                    //CHANGE STATUS FOR MULTIPLE OCFS WITH FOLLOWING STEPS FOR EACH
                    var map = _parent.GetMap(_serverName, _dbName);
                    try
                    {
                        if (map != null)
                        {
                            BatchItem it;
                            for (var i = 0; i < bt.Count; i++)
                            {
                                it = bt.GetItem(i);
                                it.NewStatus = (int) HcaiStatus.Submitted;
                                var item = map.FindLineItem(it.OCFID, it.OCFType);

                                if (item != null)
                                {
                                    var s = $"Item Found. Id = {item.OCFID}, type = {item.OCFType}, status = {(HcaiStatus) item.OCFStatus}";
                                    Debugger.LogInfo(s);
                                }

                                //check if document is being submitted multiple times
                                if (item == null)
                                {
                                    it.SkipItem = true;
                                    var s = $"Document id: {it.OCFID}, type: {it.OCFType} was NOT found in the map {_serverName}/{_dbName}." +
                                            $" Cannot send it to HCAI";
                                    Debugger.LogWarn(s);
                                }
                                else if (item.OCFStatus == (int) HcaiStatus.Submitted ||
                                         item.OCFStatus == (int) HcaiStatus.SuccessfullyDelivered)
                                {
                                    it.SkipItem = true;
                                    var s = $"Document id: {item.OCFID}, type: {item.OCFType} will NOT be send to" +
                                            $" HCAI because its status is {(HcaiStatus) item.OCFStatus}";
                                    Debugger.LogWarn(s);
                                }
                                else
                                {
                                    var s = $"Document id: {it.OCFID}, type: {it.OCFType} will be send to HCAI";
                                    Debugger.LogInfo(s);
                                    map.UpdateDocumentStatus(it.OCFID, it.OCFType, HcaiStatus.Submitted, it.MinSentDate);
                                    it.SkipItem = false;
                                }
                            }

                            var tmp = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                            SendMessageToAll(tmp);

                            //1. UPDATE DATABASE in a separate thread
                            //update DB in a separate thread
                            //Thread th = new Thread(new ParameterizedThreadStart(DBUpdate));
                            //th.Start(bt);

                            //update DB in the thread
                            DBUpdate(bt);
                            
                            for (var i = 0; i < bt.Count; i++)
                            {
                                it = bt.GetItem(i);
                                if (!it.SkipItem)
                                {
                                    //2. SUBMIT OCF TO HCAI, populate the outgoing queue
                                    var m = new UOMessage(Commands.HCAI_CHANGE_STATUS_AND_SUBMIT)
                                    {
                                        Params = new object[2]
                                    };
                                    m.Params[0] = it.OCFID;
                                    m.Params[1] = it.OCFType;
                                    AddCommand(m);
                                }
                                //3. UPDATE MAP
                                //map.UpdateDocumentStatus(it.OCFID, it.OCFType, (HCAIStatus)it.NewStatus);
                            }

                            result = 0;
                            var ms = new UOMessage(Commands.HCAI_BATCH_FINISHED);
                            AddCommand(ms);
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command HCAI_CHANGE_STATUS_AND_SUBMIT_BATCH Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }
                    //WHEN BATCH IS COMPLETE UPDATE REPRT TO ALL
                    //4. REPORT TO ALL CONNECTED CLIENTS TO THE SAME DB
                    //5. WHERE IS HCAI ACK AND ADJUSTER RESPONSES????    
                }
                else if (cmd == Commands.HCAI_TO_FILE_BATCH)
                {
                    var doc = new XmlDocument();
                    var mem = new MemoryStream(Encoding.ASCII.GetBytes(args[2]));
                    doc.Load(mem);

                    var bt = new Batch();
                    bt.ParseXmlDocument(doc);

                    var map = _parent.GetMap(_serverName, _dbName);
                    var oldStatus = -1;
                    try
                    {
                        if (map != null)
                        {
                            for (var i = 0; i < bt.Count; i++)
                            {
                                var it = bt.GetItem(i);
                                if (!ToFileDBUpdate(it)) continue;

                                var res = map.RemoveItemFromMap(it.OCFID, it.OCFType);
                                if (oldStatus < 0)
                                    oldStatus = res;
                            }
                            result = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command HCAI_TO_FILE_BATCH Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }

                    if (oldStatus > 0)
                    {
                        var m = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                        SendMessageToAll(m);
                    }
                }
                else if (cmd == Commands.HCAI_CHANGE_STATUS_ONLY_BATCH)
                {
                    var doc = new XmlDocument();
                    var mem = new MemoryStream(Encoding.ASCII.GetBytes(args[2]));
                    doc.Load(mem);

                    var bt = new Batch();
                    bt.ParseXmlDocument(doc);

                    //2. UPDATE MAP
                    var map = _parent.GetMap(_serverName, _dbName);
                    try
                    {
                        if (map != null)
                        {
                            for (var i = 0; i < bt.Count; i++)
                            {
                                var it = bt.GetItem(i);

                                map.UpdateDocumentStatus(it.OCFID, it.OCFType, (HcaiStatus) it.NewStatus, it.ATotal,
                                    it.StatusDate, it.StatusTime, it.MinSentDate);
                            }

                            var tmp = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                            SendMessageToAll(tmp);
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command HCAI_CHANGE_STATUS_ONLY_BATCH Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }
                    result = 0;

                    //WHEN BATCH IS COMPLETE UPDATE REPORT TO ALL
                    //4. REPORT TO ALL CONNECTED CLIENTS TO THE SAME DB
                    var m = new UOMessage(Commands.HCAI_BATCH_FINISHED);
                    AddCommand(m);
                }
                else if (cmd == Commands.REQUEST_HCAI_TABLE)
                {
                    //send hcai table back to the client
                    result = SendHCAITable(_connectionIn, id);
                }
                else if (cmd == Commands.REQUEST_HCAI_TABLE_GROUP)
                {
                    //send hcai table back to the client
                    var group = MyConvert.ConvertStrToInt(args[2]);
                    result = SendHCAITable(_connectionIn, group, id);
                }
                else if (cmd == Commands.REQUEST_HCAI_TABLE_LEFT_SIDE)
                {
                    //send hcai table's left side back to the client
                    result = SendHcaiTableMapLeftSide(_connectionIn, id);
                }
                else if (cmd == Commands.ADD_DOCUMENT_TO_MAP)
                {
                    var li = new HCAITableLineItem
                    {
                        OCFType = args[2],
                        OCFID = MyConvert.ConvertStrToLong(args[3]),
                        OCFStatus = MyConvert.ConvertStrToInt(args[4]),
                        StatusDate = args[5],
                        StatusTime = args[6],
                        client_id = MyConvert.ConvertStrToInt(args[7]),
                        last_name = args[8],
                        first_name = args[9],
                        CaseID = args[10],
                        MVAAFirstName = args[11],
                        MVAALastName = args[12],
                        MVAInsName = args[13],
                        Total = MyConvert.ConvertStrToFloat(args[14]),
                        InvoiceVersion = args[15],
                        ATotal = MyConvert.ConvertStrToFloat(args[16]),
                        PlanNo = MyConvert.ConvertStrToInt(args[17]),
                        NumberOfDays = MyConvert.ConvertStrToInt(args[18]),
                        IsYellow = MyConvert.ConvertStrToBool(args[19]),
                        CategoryName = args[20],
                        MinSentDate = args[21]
                    };

                    var map = _parent.GetMap(_serverName, _dbName);
                    try
                    {
                        if (map != null)
                        {
                            map.AddLineItemToMap(li);
                            result = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command ADD_DOCUMENT_TO_MAP Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }

                    var m = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                    SendMessageToAllButMe(m);
                }
                else if (cmd == Commands.ADD_DOCUMENT_TO_MAP_BATCH)
                {
                    var doc = new XmlDocument();
                    var mem = new MemoryStream(Encoding.ASCII.GetBytes(args[2]));
                    doc.Load(mem);

                    var bt = new HCAITableInfo(doc);
                    var c = 0;

                    var map = _parent.GetMap(_serverName, _dbName);
                    try
                    {
                        if (map != null)
                        {
                            for (var i = 0; i < bt.GetNumberOfStatuses(); i++)
                            {
                                for (var j = 0; j < bt.GetNumberOfRowsInStatus(i); j++)
                                {
                                    var li = bt.GetLineItemByStatus(i, j);
                                    map.AddLineItemToMap(li);
                                    c++;
                                }
                            }
                            result = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command ADD_DOCUMENT_TO_MAP_BATCH Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }
                    
                    if (c > 0)
                    {
                        var m = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                        SendMessageToAll(m);
                    }
                }
                else if (cmd == Commands.REMOVE_DOCUMENT_FROM_MAP_BATCH)
                {
                    var doc = new XmlDocument();
                    var mem = new MemoryStream(Encoding.ASCII.GetBytes(args[2]));
                    doc.Load(mem);

                    var bt = new Batch();
                    bt.ParseXmlDocument(doc);

                    var map = _parent.GetMap(_serverName, _dbName);
                    var oldStatus = -1;
                    try
                    {
                        if (map != null)
                        {
                            for (var i = 0; i < bt.Count; i++)
                            {
                                var it = bt.GetItem(i);
                                var res = map.RemoveItemFromMap(it.OCFID, it.OCFType);
                                if (oldStatus < 0)
                                    oldStatus = res;
                            }
                            result = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command HCAI_CHANGE_STATUS_AND_SUBMIT_BATCH Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }
                    
                    if (oldStatus > 0)
                    {
                        var m = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                        SendMessageToAll(m);
                    }

                }
                else if (cmd == Commands.HCAI_WITHDRAW_DOCUMENT_BATCH)
                {
                    var doc = new XmlDocument();
                    var mem = new MemoryStream(Encoding.ASCII.GetBytes(args[2]));
                    doc.Load(mem);

                    var bt = new Batch();
                    bt.ParseXmlDocument(doc);

                    //CHANGE STATUS FOR MULTIPLE OCFS WITH FOLLOWING STEPS FOR EACH
                    var map = _parent.GetMap(_serverName, _dbName);
                    try
                    {
                        if (map != null)
                        {
                            //1. UPDATE MAP
                            BatchItem it;
                            for (var i = 0; i < bt.Count; i++)
                            {
                                it = bt.GetItem(i);

                                //check if item is in SuccessfullyDelivered then do withdraw request
                                //otherwise skip item.
                                var item = map.FindLineItem(it.OCFID, it.OCFType);
                                if (item.OCFStatus == (int) HcaiStatus.SuccessfullyDelivered ||
                                    item.OCFStatus == (int) HcaiStatus.WithdrawRequestFailed)
                                {
                                    it.NewStatus = (int) HcaiStatus.WithdrawRequested;
                                    map.UpdateDocumentStatus(it.OCFID, it.OCFType, HcaiStatus.WithdrawRequested, it.MinSentDate);

                                    it.SkipItem = false;
                                }
                                else
                                {
                                    it.SkipItem = true;
                                }
                            }

                            var tmp = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                            SendMessageToAll(tmp);

                            DBUpdate(bt);

                            for (var i = 0; i < bt.Count; i++)
                            {
                                it = bt.GetItem(i);
                                if (!it.SkipItem)
                                {
                                    //1. SUBMIT OCF TO HCAI
                                    var m = new UOMessage(Commands.HCAI_WITHDRAW_DOCUMENT) {Params = new object[3]};
                                    m.Params[0] = it.OCFID;
                                    m.Params[1] = it.OCFType;
                                    m.Params[2] = it.HCAINumber;
                                    AddCommand(m);
                                }
                                //2. UPDATE DATABASE
                                //3. UPDATE MAP
                                //map.UpdateDocumentStatus(it.OCFID, it.OCFType, (HCAIStatus)it.NewStatus);
                            }

                            result = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        Debugger.WriteToFile("\tSend Command HCAI_WITHDRAW_DOCUMENT_BATCH Exception: " + ". Message: " + e.Message, 30);
                    }
                    finally
                    {
                        _parent.ReleaseMap(map);
                    }

                    var ms = new UOMessage(Commands.HCAI_BATCH_FINISHED);
                    AddCommand(ms);
                }
                else if (cmd == Commands.HCAI_CHANGE_CREDENTIALS)
                {
                    var newId = args[2];
                    var newLogin = args[3];
                    var newPassword = args[4];
                    //changing hcai credentials in all clients
                    _parent.ChangeCredentialsInAllClients(this.DBName, newId, newLogin, newPassword);
                }
                else if (cmd == Commands.HCV_RESET_CREDENTIALS)
                {
                    _parent.ChangeHcvCredentialsInAllClients(this.DBName);
                }
                else if (cmd == Commands.HCAI_DOES_DOC_EXIST_ON_SERVER)
                {
                    var hcaiNumber = args[2];
                    var ocfType = args[3];
                    var exists = _hcai.DocumentExistsOnHcaiServer(hcaiNumber, ocfType);

                    var message = new UOMessage(Commands.HCAI_DOES_DOC_EXIST_ON_SERVER) { Params = new object[3] };
                    message.Params[0] = hcaiNumber;
                    message.Params[1] = ocfType;
                    message.Params[2] = exists.ToString();
                    AddCommand(message);
                }
                else
                {
                    //command is not recognized
                    return -10;
                }
            }
            catch (Exception e)
            {
                UOServiceLib.Log.ErrorFormat("Client {0} threw exception while processing message '{1}'", ClientName, msg);
                UOServiceLib.Log.Error(e.Message, e);
                FireNewMessageEvent("Process Command Exception: " + e.Message);

                result = -1;
            }
            return result;
        }

        public void ChangeHCAICredentials(string id, string login, string pass)
        {
            _hcai.setIdLoginPass(id, login, pass);
        }

        private void DBUpdate(Batch bt)
        {
            if (bt.Count <= 0) return;

            var dbConnection = MyConvert.OpenSqlDbConnection(_dbConnectionString);
            if (dbConnection is null) return;

            using (dbConnection)
                try
                {
                    using (var command = dbConnection.CreateCommand())
                    {
                        for (var i = 0; i < bt.Count; i++)
                        {
                            var it = bt.GetItem(i);
                            if (it.SkipItem) continue;

                            var ocfId = MyConvert.ConvertStrToLong(it.OCFID);
                            _hcai.ResetOCFToNewHCAIStatus(command, ocfId, it.NewStatus, it.OCFType, it.HCAINumber);
                        }
                    }
                }
                catch (Exception e)
                {
                    UOServiceLib.Log.Error(e.Message, e);
                }
        }

        private int SubmitToHCAI(string OCFID, string OCFType)
        {
            var oldStatus = -1;

            if (!long.TryParse(OCFID, out var id))
            {
                UOServiceLib.Log.ErrorFormat("SubmitToHCAI: OCF Doc {0} OCFID {1} failed to convert to int64", OCFType, OCFID);
                Debugger.WriteToFile("SubmitToHCAI: OCFID failed conversion", 30);
                return -1;
            }

            try
            {
                OCFMapReturnType rStatus = null;
                //CHANGE SINGLE STATUS WITH FOLLOWING STEPS
                //1. SUBMIT OCF TO HCAI
                var ocfType = OCFType.ToLower();
                if (ocfType.Contains("ocf18"))
                {
                    rStatus = _hcai.submitOCF18(OCFID);
                }
                else if (ocfType.Contains("ocf21b"))
                {
                    rStatus = _hcai.submitOCF21B(OCFID);
                }
                else if (ocfType.Contains("ocf21c"))
                {
                    rStatus = _hcai.submitOCF21C(OCFID);
                }
                else if (ocfType.Contains("ocf22"))
                {
                    rStatus = _hcai.submitOCF22(OCFID);
                }
                else if (ocfType.Contains("ocf23"))
                {
                    rStatus = _hcai.submitOCF23(OCFID);
                }
                else if (ocfType.Contains("form1"))
                {
                    rStatus = _hcai.submitForm1(OCFID);
                }
                else
                {
                    Debugger.WriteToFile($"SubmitToHCAI: OCFID failed type: {ocfType}", 30);
                    return -1;
                }
                
                if (rStatus.New_HCAI_Status == HcaiStatus.InformationError)
                {
                    Debugger.WriteToFile("HCAI information error while sending doc: " + rStatus.OCFType + "#" + rStatus.OCFID, 60);
                }
                else if (rStatus.New_HCAI_Status == HcaiStatus.DeliveryFailed)
                {
                    Debugger.WriteToFile("HCAI delivery error while sending doc: " + rStatus.OCFType + "#" + rStatus.OCFID, 60);
                }
                else
                {
                    Debugger.WriteToFile("Document sent to HCAI, status " + rStatus.New_HCAI_Status, 60);
                }

                //2. UPDATE DATABASE ==> do not update db its done in submit function
                //mHcai.ResetOCFToNewHCAIStatus(mCommand, rStatus.OCFID, (int)rStatus.New_HCAI_Status, rStatus.OCFType, rStatus.HCAI_Document_Number);


                //3. UPDATE MAP
                var map = _parent.GetMap(_serverName, _dbName);
                try
                {
                    if (map != null)
                    {
                        oldStatus = map.UpdateDocumentStatus(OCFID, OCFType, rStatus.New_HCAI_Status, rStatus.MinSentDate, rStatus.HCAI_Document_Number);
                    }
                }
                catch (Exception e)
                {
                    Debugger.WriteToFile("\tSubmitToHCAI map Exception: " + ". Message: " + e.Message, 30);
                }
                finally
                {
                    _parent.ReleaseMap(map);
                }

                return oldStatus;
            }
            catch(Exception e)
            {
                UOServiceLib.Log.ErrorFormat("Failed to submit OCF Doc {0}<{1}>", OCFType, OCFID);
                UOServiceLib.Log.Error(e.Message, e);
                Debugger.WriteToFile("Submit Failed.", 30);
                return -1;
            }
        }

        private void SendMessageToAllButMe(UOMessage msg)
        {
            _parent.SendMessageToAllOtherClients(this, msg);
        }

        private void SendMessageToAll(UOMessage msg)
        {
            _parent.SendMessageToAllClients(this.DBName, msg);
        }

        private int SendXMLDocument(Socket conn, XmlDocument doc, int id)
        {
            /*            string msg;
                        //generating a random filename
                        int size = 0;
                        byte[] buf;

                        try
                        {
                            buf = Encoding.ASCII.GetBytes(doc.OuterXml);

            //                doc.Save(fname);
            //                buf = File.ReadAllBytes(fname);

                            msg = Commands.SEND_XML_DOC + Commands.SEP[0] + buf.Length.ToString();
                            SendString2(conn, msg, id);
                            msg = ReceiveString2(conn, id);
                            if (msg == Commands.BEGIN_SEND_XML_DOC)
                            {
                                FireNewMessageEvent("Sending xml file.");
                                size = conn.Send(buf, buf.Length, SocketFlags.None);

                                //ackloegement from the client that file is recieved
                                msg = ReceiveString2(conn, id);
                            }
                            else
                            {
                                FireNewMessageEvent("Miscomunication" + msg);
                                size = 0;
                            }
                            return size;
                        }
                        catch (SocketException e)
                        {
                            FireNewMessageEvent(e.Message);
                            return -1;
                        }
            */
            return -1;
        }

        private int SendHcaiTableMapLeftSide(Socket conn, int id)
        {
            var cmd = Commands.SEP[0] + Commands.SENDING_HCAI_TABLE_LEFT_SIDE;
            int size;
            var result = 0;

            var map = _parent.GetMap(_serverName, _dbName);
            if (map != null)
            {
                try
                {
                    //loop through all groups and send number of documents in each
                    for (var i = 0; i < (int) HcaiGroup.NumberOfGroups; i++)
                    {
                        cmd += Commands.SEP[0] + map.GetNumberOfRowsInGroup(i).ToString();
                    }
                    cmd += Commands.SEP[0];

                    Debugger.WriteToFile("\tSending Leftside: " + cmd + " :::" + _clientName + ":" + _clientNumber + "id=" + id, 30);
                    size = SendString2(conn, cmd, id);

                    if (size != cmd.Length)
                        result = -1;
                }
                catch (Exception e)
                {
                    Debugger.WriteToFile("\tSending Leftside Exception: " + cmd + " :::" + _clientName + ":" + _clientNumber + "id=" +
                                        id + ". Message: " + e.Message, 30);
                }
                finally
                {
                    _parent.ReleaseMap(map);
                }
            }
            else
            {
                Debugger.WriteToFile("\tSending Leftside ERROR: " + cmd + " :::" + _clientName + ":" + _clientNumber + "id=" + id, 30);
                size = SendString2(conn, Commands.ERROR, id);

                if (size != cmd.Length)
                    result = -1;
            }

            return result;
        }

        private int SendHCAITable(Socket conn, int id)
        {
            return SendHCAITable(conn, -1, id);
        }

        private int SendHCAITable(Socket conn, int group, int id)
        {
            var res = -1;
            var map = _parent.GetMap(_serverName, _dbName);
            try
            {
                if (map != null)
                {
                    var doc = group < 0 ? map.CreateXmlDocument() : map.CreateXmlDocumentForGroup(group);
                    Debugger.WriteToFile("\tSending Group: " + group + " :::" + _clientName + ":" + _clientNumber + "id=" + id, 30);
                    if (SendString2(conn, doc.OuterXml, id) > 0)
                        res = 0;
                }
                else
                {
                    //client is waiting for xml, but we have an error here
                    //so we let the client know that no xml is coming
                    Debugger.WriteToFile("\tSending Group ERROR:  :::" + _clientName + ":" + _clientNumber + "id=" + id, 30);
                    SendString2(conn, Commands.ERROR, id);
                }
            }
            catch (Exception e)
            {
                Debugger.WriteToFile("\tSendHCAITable map Exception: " + ". Message: " + e.Message, 30);
            }
            finally
            {
                _parent.ReleaseMap(map);
            }
            
            return res;
        }

        private HcvResult ValidateHealthCard(HcvRequest request)
        {
            lock (_hcvModuleLock)
            {
                return _hcvModule.ValidateCard(new List<HcvRequest> { request });
            }
        }

        private void InitializeHcvModule(SqlDataReader reader)
        {
            lock (_hcvModuleLock)
            {
                var encryptedKey = MyConvert.MyReaderFldToString(reader, "UoServerLicenseKey");

                if (encryptedKey == "")
                {
                    return;
                }
                var decryptedKey = StringCipher.Decrypt(encryptedKey);

                //Debugger.WriteToFile("key: " + decryptedKey , 30); //tobe deleted
                var c = new EdtHcvClientConfig
                {
                    ModulesToInitialize = InitializationType.HcvOnly,
                    //HcvConformanceKey = "d713d137-6738-45c9-acd9-a66ea745f526", //test conf id
                    //HcvConformanceKey = "f4c932f7-f267-4e8b-9976-3051731f24a1", //production conf id
                    HcvConformanceKey = decryptedKey,
                    HcvMohId = MyConvert.MyReaderFldToString(reader, "MohID"),//"282673",
                    UserLogin = MyConvert.MyReaderFldToString(reader, "MohUserName"),//"confsu21@outlook.com",
                    UserPassword = MyConvert.MyReaderFldToString(reader, "MohPassword")//"Password1!"
                };
                _hcvModule = new EdtClientLib(c);
            }
        }

        public void ResetHcvModule()
        {
            var dbConnection = MyConvert.OpenSqlDbConnection(_dbConnectionString);
            if (dbConnection == null) return;

            using (dbConnection)
            using (var command = dbConnection.CreateCommand())
            {
                var reader = ExecQuery(command, @"SELECT * FROM Business_Center");
                if (reader.Read())
                {
                    InitializeHcvModule(reader);
                }
            }
        }
    }
}
