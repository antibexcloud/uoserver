﻿using System;
using System.Data.SqlClient;
using System.Globalization;

namespace UOServerLib
{
    public class MyConvert
    {
        #region MY STRING TO NUMBER, DATE/TIME CONVESIONS

        public static string MyLeft(string text, int length)
        {
            if (length <= 0 || string.IsNullOrEmpty(text))
                return "";
            if (text.Length <= length)
                return text;
            return text.Substring(0, length);
        }

        public static bool ConvertStrToBool(string tStr)
        {
            if (!IsNumeric(tStr)) return false;

            try
            {
                var t = tStr.Replace("$", "").Replace(" ", "");
                return Convert.ToBoolean(ConvertStrToInt(t));
            }
            catch(Exception e)
            {
                Debugger.LogError("Cannot convert '" + tStr + "' to bool", e);
                return false;
            }
        }

        public static string ConvertToCurrencyFormat(string t)
        {
            if (!IsNumeric(t)) return "0.00";
            var d = ConvertStrToDouble(t);
            return d.ToString("F");
        }

        public static bool IsNumeric(string n)
        {
            if (string.IsNullOrEmpty(n)) return false;
            
            try
            {
                var t = n.Replace("$", "").Replace(" ", "");
                Convert.ToDouble(t);  //Convert.ToDouble(n.ToString());
                return true;
            }
            catch
            {              
                return false;
            }
        }

        public static int ConvertStrToInt(string tStr)
        {
            return ConvertStrToInt(tStr, 0);
        }

        public static int ConvertStrToInt(string tStr, int defaultV)
        {
            if (!IsNumeric(tStr)) return defaultV;

            try
            {
                var t = tStr.Replace("$", "").Replace(" ", "");
                return Convert.ToInt32(t);
            }
            catch
            {
                return defaultV;
            }
        }

        public static long ConvertStrToLong(string tStr)
        {
            if (!IsNumeric(tStr)) return 0;
            
            try
            {
                var t = tStr.Replace("$", "").Replace(" ", "");
                return Convert.ToInt64(t);
            }
            catch
            {
                return 0;
            }
        }

        public static double ConvertStrToDouble(string tStr)
        {
            if (!IsNumeric(tStr)) return 0;

            try
            {
                var t = tStr.Replace("$", "").Replace(" ", "");
                return Convert.ToDouble(t);
            }
            catch
            {
                return 0;
            }
        }

        public static float ConvertStrToFloat(string tStr)
        {
            if (!IsNumeric(tStr)) return 0;

            try
            {
                var t = tStr.Replace("$", "").Replace(" ", "");
                return Convert.ToSingle(t);
            }
            catch
            {
                return 0;
            }
        }

        public static string ConvertToHcaiDateFormat(string tDate)
        {
            if (string.IsNullOrEmpty(tDate)) return "";
            
            try
            {
                var dDate = DateTime.Parse(tDate, CultureInfo.InvariantCulture);
                return dDate.ToString("yyyyMMdd");
            }
            catch(Exception e)
            {
                Debugger.LogError("Cannot convert '" + tDate + "' to DateTime", e);
                return "";
            }
        }

        public static string MyReplace(string str)
        {
            return str == null ? "" : str.Replace("'", "''");
        }

        public static string MyReaderFldToString(SqlDataReader reader, string fldName)
        {
            try
            {
                if (reader[fldName] != null && reader[fldName].ToString() != "")
                    return reader[fldName].ToString();
                return "";
            }
            catch (Exception e)
            {
                Debugger.LogError("MyReaderFldToString Exception.", e);
                return "";
            }
        }
        #endregion
        
        #region My DATA FORMAT

        public static DateTime MyHcaiDateConvert(string date)
        {
            var formats = new []{ "yyyyMMddHHmmss.ffzzz", "yyyy-MM-dd", "yyyy-MM-ddTHH:mm:ss.ffZ", "yyy-MM-ddTHH:mm:ss.ff%K"};

            if (DateTime.TryParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out var res) ||
                DateTime.TryParse(date, out res)) 
                return res;

            Debugger.WriteToFile("Failed to convert date: " + date, 100);
            res = new DateTime(1990, 1, 1);
            return res;
        }

        public static string GetMyTimeFormat(string t)
        {
            var hour = ConvertStrToInt(t.Substring(0, 2));
            var min = ConvertStrToInt(t.Substring(3, 2));
            var tExt = hour < 12 ? "AM" : "PM";

            if (hour == 12 || hour == 0)
                hour = 12;
            else
                hour %= 12;
            return hour.ToString("00") + ":" + min.ToString("00") + " " + tExt;
        }

        public static string GetMyTimeFormatV2(string t)
        {
            var hour = ConvertStrToInt(t.Substring(0, 2));
            var min = ConvertStrToInt(t.Substring(2, 2));
            var tExt = hour < 12 ? "AM" : "PM";

            if (hour == 12 || hour == 0)
                hour = 12;
            else
                hour %= 12;
            return hour.ToString("00") + ":" + min.ToString("00") + " " + tExt;    
        }
       
        #endregion

        public static SqlConnection OpenSqlDbConnection(string connStr)
        {
            var dbConnection = new SqlConnection(connStr);
            try
            {
                dbConnection.Open();
                return dbConnection;
            }
            catch (Exception ex)
            {
                Debugger.WriteToFile("OpenSqlDbConnection error: " + ex.Message, 10);
                dbConnection.Dispose();
                return null;
            }
        }
    }
}
