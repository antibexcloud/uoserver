﻿namespace UOServerLib
{
    static class Commands
    {
        public static string[] SEP = { @":?:" };

        public static string RECV_OK = @"RECV_OK";
        public static string ERROR = @"ERROR";
        public static string ERROR_UNKNOWN_COMMAND = @"ERROR_UNKNOWN_COMMAND";
        public static string TEST_CON = @"TEST_CON";
        public static string CON_SUCCESS = @"CON_SUCCESS";
        public static string CLIENT_PARAMS = @"CLIENT_PARAMS";
        public static string DISCONNECT = @"DISCONNECT";
        public static string RESTART_SERVER = @"RESTART_SERVER";

        //HCAI Table commands
        public static string REQUEST_HCAI_TABLE = @"REQUEST_HCAI_TABLE";
        public static string SEND_HCAI_TABLE = @"SEND_HCAI_TABLE";
        public static string BEGIN_SEND_HCAI_TABLE = @"BEGIN_SEND_HCAI_TABLE";
        public static string REQUEST_HCAI_TABLE_GROUP = @"REQUEST_HCAI_TABLE_GROUP";
        public static string SEND_HCAI_TABLE_GROUP = @"SEND_HCAI_TABLE_GROUP";
        public static string REQUEST_HCAI_TABLE_LEFT_SIDE = @"REQUEST_HCAI_TABLE_LEFT_SIDE";
        public static string SENDING_HCAI_TABLE_LEFT_SIDE = @"SENDING_HCAI_TABLE_LEFT_SIDE";
        public static string ADD_DOCUMENT_TO_MAP = @"ADD_DOCUMENT_TO_MAP";
        public static string ADD_DOCUMENT_TO_MAP_BATCH = @"ADD_DOCUMENT_TO_MAP_BATCH";
        public static string REMOVE_DOCUMENT_FROM_MAP = @"REMOVE_DOCUMENT_FROM_MAP";
        public static string REMOVE_DOCUMENT_FROM_MAP_BATCH = @"REMOVE_DOCUMENT_FROM_MAP_BATCH";
        public static string HCAI_CHANGE_CREDENTIALS = @"HCAI_CHANGE_CREDENTIALS";


        public static string SEND_XML_DOC = @"SEND_XML_DOC";
        public static string BEGIN_SEND_XML_DOC = @"BEGIN_SEND_XML_DOC";


        public static string APPT_STRING = @"APPT_STRING";
        public static string APPT_STRING_SEND_ALL_BUT_ME = @"APPT_STRING_SEND_ALL_BUT_ME";

        //HCV COMMAND
        public static string HCV_VALIDATE_CARD = @"HCV_VALIDATE_CARD";
        public static string HCV_RESET_CREDENTIALS = @"HCV_RESET_CREDENTIALS";

        //HCAI COMMANDS
        public static string GET_HCAI_PROVIDERS = @"GET_HCAI_PROVIDERS";
        public static string GET_HCAI_INSURERS = @"GET_HCAI_INSURERS";

        public static string HCAI_CHANGE_STATUS_AND_SUBMIT = @"HCAI_CHANGE_STATUS_AND_SUBMIT";
        public static string HCAI_CHANGE_STATUS_ONLY = @"HCAI_CHANGE_STATUS_ONLY";
        public static string HCAI_DOCUMENT_STATUS_CHANGED = @"HCAI_DOCUMENT_STATUS_CHANGED";
        public static string HCAI_WITHDRAW_DOCUMENT = @"HCAI_WITHDRAW_DOCUMENT";
        public static string HCAI_WITHDRAW_DOCUMENT_BATCH = @"HCAI_WITHDRAW_DOCUMENT_BATCH";
        public static string HCAI_BATCH_FINISHED = @"HCAI_BATCH_FINISHED";

        public static string HCAI_CHANGE_STATUS_AND_SUBMIT_BATCH = @"HCAI_CHANGE_STATUS_AND_SUBMIT_BATCH";
        public static string HCAI_CHANGE_STATUS_ONLY_BATCH = @"HCAI_CHANGE_STATUS_ONLY_BATCH";
        public static string HCAI_TO_FILE_BATCH = @"HCAI_TO_FILE_BATCH";
        public static string HCAI_DOES_DOC_EXIST_ON_SERVER = @"HCAI_DOES_DOC_EXIST_ON_SERVER";
    }
}