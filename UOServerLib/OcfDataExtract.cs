﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using PMSToolkit;
using UOServerLib.OCFSyncTableAdapters;
using System.Linq;
using System.Data.SqlClient;

namespace UOServerLib
{
    public class OcfDataExtract
    {
        private DataExtractConfig _config;
        private static readonly MessageType MsgType = new MessageType();
        private readonly Toolkit _pmsTk = new Toolkit();

        

        //readonly OCFSync.OCF18ARDataTable _ocf18ArDataTable = new OCFSync.OCF18ARDataTable();
        //readonly OCFSync.OCF18SubmitDataTable _ocf18SubmitDataTable = new OCFSync.OCF18SubmitDataTable();
        //readonly OCFSync.SessionHeaderLineItemSubmitDataTable _sessionHeaderDataTable = new OCFSync.SessionHeaderLineItemSubmitDataTable();
        readonly OCFSync _mainDataSet = new OCFSync();

        public OcfDataExtract(DataExtractConfig config)
        {
            _config = config;
        }

        private bool ExecQueryForWrite(SqlCommand mCommand, string StrSql)
        {

            bool IsSucc = false;
            try
            {
                mCommand.CommandText = StrSql;
                mCommand.ExecuteNonQuery();

                IsSucc = true;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("Query exe error: sql = '" + StrSql + "'. " + ex.Message, ex);
                //Console.WriteLine(ex.Message.ToString());

                IsSucc = false;
            }
            return IsSucc;
        }

        public void ExtractFullDocuments(DateTime from, DateTime to)
        {
            var result = GetActivityList(from, to);
           
            if (result != null)
            {
                List<Tuple<string, string>> HcaiIDs = new List<Tuple<string, string>>();
                string ocfType;
                foreach (var doc in result)
                {
                    ocfType = ExtractSingleFullDocument(doc.HCAI_Document_Number);
                    if (!string.IsNullOrEmpty(ocfType))
                    {
                        HcaiIDs.Add(new Tuple<string, string>(doc.HCAI_Document_Number, ocfType));
                    }
                }

                // delete all extracted hcai ids
                SqlConnection mDBConnection = MyConvert.OpenSQLDBConnection(@"Database=" + "HCAIOCFSync" + ";User Id=uouser; password=passc0de;Server=" + "localhost\\anxsqlserver");
                SqlCommand mCommand = mDBConnection.CreateCommand();

                foreach (var i in HcaiIDs)
                {
                    //delete from db
                    if (i.Item2 != "OCF9")
                    {
                        if (ExecQueryForWrite(mCommand, "delete from " + i.Item2 + "Submit where HCAI_Document_Number = '" + i.Item1 + "'") == false)
                        {
                            Debuger.WriteToFile("ExecQueryForWrite: " + i.Item2, 500);
                        }
                        else
                        {
                            if (ExecQueryForWrite(mCommand, "delete from OCF9AR where HCAI_Document_Number = '" + i.Item1 + "'") == false)
                            {
                                Debuger.WriteToFile("ExecQueryForWrite: " + i.Item2, 500);
                            }                                            
                        }
                    }
                    else
                    {
                        if (ExecQueryForWrite(mCommand, "delete from OCF9AR where HCAI_Document_Number = '" + i.Item1 + "'") == false)
                        {
                            Debuger.WriteToFile("ExecQueryForWrite: " + i.Item2, 500);
                        }                    
                    }
                }

                mDBConnection = null;
                mCommand = null;
            }
        }


        #region check extracted docs
            
        #endregion

        public string ExtractSingleFullDocument(string hcaiNumber)
        {
            string ocfType = "";
            if (string.IsNullOrEmpty(hcaiNumber))
            {
                Debuger.WriteToFile("Error: ExtractDocument hcai number is null or empty", 60);
                return "";
            }

            Debuger.WriteToFile("Extracting doc: " + hcaiNumber, 60);

            var deKeys = new PMSToolkit.DataObjects.DataExtractResponseKeys();
            var res = _pmsTk.dataExtract(_config.Login, _config.Pass, hcaiNumber, DocType.OCF18);

            string Papa_HCAI_Document_Number = "";
            
            if (res != null)
            {
                ocfType = res.getValue(deKeys.Document_Type);
                string HCAI_Document_Number = res.getValue(deKeys.HCAI_Document_Number);

                switch (ocfType)
                {
                    case "OCF18":
                        ExtractOcf18(res, ref Papa_HCAI_Document_Number);
                        ExtractOcf9(res, "OCF18", Papa_HCAI_Document_Number);
                        break;
                    case "OCF21B":
                        ExtractOcf21B(res, ref Papa_HCAI_Document_Number);
                        ExtractOcf9(res, "OCF21B", Papa_HCAI_Document_Number);
                        break;
                    case "OCF21C":
                        ExtractOcf21C(res, ref Papa_HCAI_Document_Number);
                        ExtractOcf9(res, "OCF21C", Papa_HCAI_Document_Number);
                        break;
                    case "OCF23":
                        ExtractOcf23(res, ref Papa_HCAI_Document_Number);
                        ExtractOcf9(res, "OCF23", Papa_HCAI_Document_Number);
                        break;
                    //case "OCF9":
                    //    ExtractOcf9(res);
                    //    break;
                    default:
                        ocfType = "";
                        break;
                }
            }
            return ocfType;
        }

        private void ExtractOcf9(IDataItem doc, string ParentOCFType, string Papa_HCAI_Document_Number)
        {
            var edOcf9Keys = new PMSToolkit.DataObjects.OCF9DataExtractResponseKeys();
            
            var edOcf9LisKeys = new PMSToolkit.DataObjects.OCF9DataExtractResponseLineItemKeys();

            var rowSubmited = _mainDataSet.OCF9AR.NewOCF9ARRow();

            rowSubmited.HCAI_Document_Number = Papa_HCAI_Document_Number;

            rowSubmited.OCF9_HCAI_Document_Number = doc.getValue(edOcf9Keys.HCAI_Document_Number);

            if (rowSubmited.OCF9_HCAI_Document_Number.Trim() == "")
            {
                return;
            }

            rowSubmited.OCF9_Header_DateRevised = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf9Keys.OCF9_Header_DateRevised));
            rowSubmited.AdditionalComments = doc.getValue(edOcf9Keys.AdditionalComments);
            rowSubmited.OCFVersion = MyConvert.ConvertStrToInt(doc.getValue(edOcf9Keys.OCFVersion));
            rowSubmited.AttachmentsBeingSent = doc.getValue(edOcf9Keys.AttachmentsBeingSent);
            
            //Add row to OCF9AR table
            switch (ParentOCFType)
            {
                case "OCF18":                    
                    if (_mainDataSet.OCF18AR.FindByHCAI_Document_Number(rowSubmited.HCAI_Document_Number) == null)
                    {
                        Debuger.WriteToFile("DataExtract OCF18's ocf9: ExtractOcf9 ", 500);
                        return;
                    }                    
                    break;

                case "OCF23":
                    if (_mainDataSet.OCF23AR.FindByHCAI_Document_Number(rowSubmited.HCAI_Document_Number) == null)
                    {
                        Debuger.WriteToFile("DataExtract OCF23's ocf9: ExtractOcf9 ", 500);
                        return;
                    }
                    break;

                case "OCF21B":
                    if (_mainDataSet.OCF21BAR.FindByHCAI_Document_Number(rowSubmited.HCAI_Document_Number) == null)
                    {
                        Debuger.WriteToFile("DataExtract OCF21B's ocf9: ExtractOcf9 ", 500);
                        return;
                    }
                    break;

                case "OCF21C":                    
                    if (_mainDataSet.OCF21CAR.FindByHCAI_Document_Number(rowSubmited.HCAI_Document_Number) == null)
                    {
                        Debuger.WriteToFile("DataExtract OCF21C's ocf9: ExtractOcf9 ", 500);
                        return;
                    }
                    break;
                    
                default:
                    break;

            }
            _mainDataSet.OCF9AR.AddOCF9ARRow(rowSubmited);

            
            var oList = doc.getList(LineItemType.GS9LineItem);

            if (oList != null)
            {
                Debuger.WriteToFile("DataExtract: GS9LineItemAR", 500);
                for (int i = 0; i < oList.Count; i++)
                {
                    var GS9LineItemAR = _mainDataSet.GS9LineItemAR.NewGS9LineItemARRow();
                    var tmpItem = (IDataItem)oList[i];
                    Debuger.WriteToFile("DataExtract: GS9LineItemAR", 500);

                    GS9LineItemAR.OCF9_HCAI_Document_Number = rowSubmited.OCF9_HCAI_Document_Number;
                    GS9LineItemAR.OCF9_GoodsAndServices_Items_Item_InterestPayable = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf9LisKeys.OCF9_GoodsAndServices_Items_Item_InterestPayable));
                    GS9LineItemAR.OCF9_GoodsAndServices_Items_Item_ReferenceNumber = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf9LisKeys.OCF9_GoodsAndServices_Items_Item_ReferenceNumber));
                    GS9LineItemAR.OCF9_GoodsAndServices_Items_Item_Code = tmpItem.getValue(edOcf9LisKeys.OCF9_GoodsAndServices_Items_Item_Code);
                                     
                    _mainDataSet.GS9LineItemAR.AddGS9LineItemARRow(GS9LineItemAR);
                }
            }
            
            //throw new NotImplementedException();
        }

        private void ExtractOcf23(IDataItem doc, ref string Papa_HCAI_Document_Number)
        {
            var edOcf23Keys = new PMSToolkit.DataObjects.OCF23DataExtractResponseKeys();
            var rowSubmited = _mainDataSet.OCF23Submit.NewOCF23SubmitRow();
            var edOcf23LisKeys = new PMSToolkit.DataObjects.OCF23DataExtractResponseLineItemKeys();

            rowSubmited.HCAI_Document_Number = doc.getValue(edOcf23Keys.HCAI_Document_Number);
            Papa_HCAI_Document_Number = rowSubmited.HCAI_Document_Number;

            rowSubmited.PMSFields_PMSSoftware = doc.getValue(edOcf23Keys.PMSFields_PMSSoftware);
            rowSubmited.PMSFields_PMSVersion = doc.getValue(edOcf23Keys.PMSFields_PMSVersion);
            rowSubmited.PMSFields_PMSDocumentKey = doc.getValue(edOcf23Keys.PMSFields_PMSDocumentKey);
            rowSubmited.PMSFields_PMSPatientKey = doc.getValue(edOcf23Keys.PMSFields_PMSPatientKey);
            rowSubmited.AttachmentsBeingSent = doc.getValue(edOcf23Keys.AttachmentsBeingSent);
            rowSubmited.Header_ClaimNumber = doc.getValue(edOcf23Keys.Header_ClaimNumber);
            rowSubmited.Header_PolicyNumber = doc.getValue(edOcf23Keys.Header_PolicyNumber);
            rowSubmited.Header_DateOfAccident = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.Header_DateOfAccident));
            rowSubmited.Applicant_Name_FirstName = doc.getValue(edOcf23Keys.Applicant_Name_FirstName);
            rowSubmited.Applicant_Name_MiddleName = doc.getValue(edOcf23Keys.Applicant_Name_MiddleName);
            rowSubmited.Applicant_Name_LastName = doc.getValue(edOcf23Keys.Applicant_Name_LastName);
            rowSubmited.Applicant_DateOfBirth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.Applicant_DateOfBirth));
            rowSubmited.Applicant_Gender = doc.getValue(edOcf23Keys.Applicant_Gender);
            rowSubmited.Applicant_TelephoneNumber = doc.getValue(edOcf23Keys.Applicant_TelephoneNumber);
            rowSubmited.Applicant_TelephoneExtension = doc.getValue(edOcf23Keys.Applicant_TelephoneExtension);
            rowSubmited.Applicant_Address_StreetAddress1 = doc.getValue(edOcf23Keys.Applicant_Address_StreetAddress1);
            rowSubmited.Applicant_Address_StreetAddress2 = doc.getValue(edOcf23Keys.Applicant_Address_StreetAddress2);
            rowSubmited.Applicant_Address_City = doc.getValue(edOcf23Keys.Applicant_Address_City);
            rowSubmited.Applicant_Address_Province = doc.getValue(edOcf23Keys.Applicant_Address_Province);
            rowSubmited.Applicant_Address_PostalCode = doc.getValue(edOcf23Keys.Applicant_Address_PostalCode);
            rowSubmited.Insurer_IBCInsurerID = doc.getValue(edOcf23Keys.Insurer_IBCInsurerID);
            rowSubmited.Insurer_IBCBranchID = doc.getValue(edOcf23Keys.Insurer_IBCBranchID);
            rowSubmited.Insurer_Adjuster_Name_FirstName = doc.getValue(edOcf23Keys.Insurer_Adjuster_Name_FirstName);
            rowSubmited.Insurer_Adjuster_Name_LastName = doc.getValue(edOcf23Keys.Insurer_Adjuster_Name_LastName);
            rowSubmited.Insurer_Adjuster_TelephoneNumber = doc.getValue(edOcf23Keys.Insurer_Adjuster_TelephoneNumber);
            rowSubmited.Insurer_Adjuster_TelephoneExtension = doc.getValue(edOcf23Keys.Insurer_Adjuster_TelephoneExtension);
            rowSubmited.Insurer_Adjuster_FaxNumber = doc.getValue(edOcf23Keys.Insurer_Adjuster_FaxNumber);
            rowSubmited.Insurer_PolicyHolder_IsSameAsApplicant = doc.getValue(edOcf23Keys.Insurer_PolicyHolder_IsSameAsApplicant);
            rowSubmited.Insurer_PolicyHolder_Name_FirstName = doc.getValue(edOcf23Keys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmited.Insurer_PolicyHolder_Name_LastName = doc.getValue(edOcf23Keys.Insurer_PolicyHolder_Name_LastName);
            rowSubmited.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage);
            rowSubmited.OCF23_OtherInsurance_MOHAvailable = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_MOHAvailable);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmited.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.getValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
            rowSubmited.OCF23_HealthPractitioner_FacilityRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_FacilityRegistryID));
            rowSubmited.OCF23_HealthPractitioner_ProviderRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_ProviderRegistryID));
            rowSubmited.OCF23_HealthPractitioner_Occupation = doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_Occupation);
            rowSubmited.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists = doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists);
            rowSubmited.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails = doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails);
            rowSubmited.OCF23_HealthPractitioner_IsSignatureOnFile = doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_IsSignatureOnFile);
            rowSubmited.OCF23_HealthPractitioner_DateSigned = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_DateSigned));
            rowSubmited.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner = doc.getValue(edOcf23Keys.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner);
            rowSubmited.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident = doc.getValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident);
            rowSubmited.OCF23_PriorAndConcurrentConditions_PriorCondition_Response = doc.getValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Response);
            rowSubmited.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation = doc.getValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation);
            rowSubmited.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response = doc.getValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response);
            rowSubmited.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation = doc.getValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);
            rowSubmited.OCF23_BarriersToRecovery_Response = doc.getValue(edOcf23Keys.OCF23_BarriersToRecovery_Response);
            rowSubmited.OCF23_BarriersToRecovery_Explanation = doc.getValue(edOcf23Keys.OCF23_BarriersToRecovery_Explanation);
            rowSubmited.OCF23_PAFPreApproveServices_PAF_PAFType = doc.getValue(edOcf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType);
            rowSubmited.OCF23_PAFPreApproveServices_PAF_EstimatedFee = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_PAFPreApproveServices_PAF_EstimatedFee));
            rowSubmited.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description = doc.getValue(edOcf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description);
            rowSubmited.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee));
            rowSubmited.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey = doc.getValue(edOcf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey);
            rowSubmited.OCF23_InsurerTotals_AutoInsurerTotal_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Proposed));
            rowSubmited.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed));
            rowSubmited.OCF23_InsurerTotals_SubTotalPreApproved_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Proposed));
            rowSubmited.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer = doc.getValue(edOcf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer);
            rowSubmited.OCF23_ApplicantSignature_IsApplicantSignatureOnFile = doc.getValue(edOcf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureOnFile);
            rowSubmited.OCF23_ApplicantSignature_SigningApplicant_FirstName = doc.getValue(edOcf23Keys.OCF23_ApplicantSignature_SigningApplicant_FirstName);
            rowSubmited.OCF23_ApplicantSignature_SigningApplicant_LastName = doc.getValue(edOcf23Keys.OCF23_ApplicantSignature_SigningApplicant_LastName);
            rowSubmited.OCF23_ApplicantSignature_SigningDate = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.OCF23_ApplicantSignature_SigningDate));
            rowSubmited.OCF23_OtherGoodsAndServices_GoodsAndServicesComments = doc.getValue(edOcf23Keys.OCF23_OtherGoodsAndServices_GoodsAndServicesComments);
            rowSubmited.AdditionalComments = doc.getValue(edOcf23Keys.AdditionalComments);
            //rowSubmited.DEC_Error_Count = doc.getValue(edOcf23Keys.DEC_Error_Count);
            //rowSubmited.DEC_Received_Date = doc.getValue(edOcf23Keys.DEC_Received_Date);
            rowSubmited.OCFVersion = MyConvert.ConvertStrToInt(doc.getValue(edOcf23Keys.OCFVersion));
            
            //Add row to OCF23Submit table
            _mainDataSet.OCF23Submit.AddOCF23SubmitRow(rowSubmited);
            

            //OtherGSLineItemSubmit

            var oList = doc.getList(LineItemType.DE_GS23OtherGSLineItem_Estimated);

            if (oList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF23 OtherGSLineItemSubmit", 500);
                for (int i = 0; i < oList.Count; i++)
                {
                    var OtherGSLineItemSubmitRow = _mainDataSet.OtherGSLineItemSubmit.NewOtherGSLineItemSubmitRow();
                    var tmpItem = (IDataItem)oList[i];
                    Debuger.WriteToFile("DataExtract: OCF23 OtherGSLineItemSubmit", 500);

                    OtherGSLineItemSubmitRow.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Code = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Code);
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Attribute = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Attribute);
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID));
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Quantity = MyConvert.ConvertStrToFloat( tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Quantity));
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Measure = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Measure);
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost));
                    OtherGSLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber));

                    _mainDataSet.OtherGSLineItemSubmit.AddOtherGSLineItemSubmitRow(OtherGSLineItemSubmitRow);
                }
            }


            //PAFLineItemSubmit


            var pList = doc.getList(LineItemType.DE_GS23PAFLineItem);
            
            if (pList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF23 PAFLineItemSubmit", 500);
                for (int i = 0; i < pList.Count; i++)
                {
                    var PAFLineItemSubmitRow = _mainDataSet.PAFLineItemSubmit.NewPAFLineItemSubmitRow();

                    var tmpItem = (IDataItem)pList[i];
                    Debuger.WriteToFile("DataExtract: OCF23 PAFLineItemSubmitRow", 500);

                    PAFLineItemSubmitRow.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    PAFLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey = tmpItem.getValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey);
                    PAFLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code = tmpItem.getValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code);
                    PAFLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute = tmpItem.getValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute);
                    PAFLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee));
                    
                    _mainDataSet.PAFLineItemSubmit.AddPAFLineItemSubmitRow(PAFLineItemSubmitRow);
                }
            }


            //OCF23InjuryLineItemSubmit
            
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF23InjuryLineItemSubmit", 500);
                for (int i = 0; i < injList.Count; i++)
                {
                    var OCF23InjuryLineItemSubmit = _mainDataSet.OCF23InjuryLineItemSubmit.NewOCF23InjuryLineItemSubmitRow();
                    var tmpItem = (IDataItem)injList[i];
                    Debuger.WriteToFile("DataExtract: OCF23InjuryLineItemSubmit ", 500);
                    OCF23InjuryLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OCF23InjuryLineItemSubmit.OCF23_InjuriesAndSequelae_Injury_Code = tmpItem.getValue(edOcf23LisKeys.OCF23_InjuriesAndSequelae_Injury_Code);

                    _mainDataSet.OCF23InjuryLineItemSubmit.AddOCF23InjuryLineItemSubmitRow(OCF23InjuryLineItemSubmit);
                }
            }

            //Adjuster Responce
            //OCF23AR
            
            var OCF23ARrow = _mainDataSet.OCF23AR.NewOCF23ARRow();
            OCF23ARrow.HCAI_Document_Number = doc.getValue(edOcf23Keys.HCAI_Document_Number);
            OCF23ARrow.PMSFields_PMSDocumentKey = doc.getValue(edOcf23Keys.PMSFields_PMSDocumentKey);
            OCF23ARrow.Submission_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.Submission_Date));
            OCF23ARrow.Submission_Source = doc.getValue(edOcf23Keys.Submission_Source);
            OCF23ARrow.Adjuster_Response_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.Adjuster_Response_Date));
            OCF23ARrow.In_Dispute = doc.getValue(edOcf23Keys.In_Dispute);
            OCF23ARrow.Document_Type = doc.getValue(edOcf23Keys.Document_Type);
            OCF23ARrow.Document_Status = doc.getValue(edOcf23Keys.Document_Status);
            OCF23ARrow.Attachments_Received_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.Attachments_Received_Date));
            OCF23ARrow.Claimant_First_Name = doc.getValue(edOcf23Keys.Claimant_First_Name);
            OCF23ARrow.Claimant_Middle_Name = doc.getValue(edOcf23Keys.Claimant_Middle_Name);
            OCF23ARrow.Claimant_Last_Name = doc.getValue(edOcf23Keys.Claimant_Last_Name);
            OCF23ARrow.Claimant_Date_Of_Birth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf23Keys.Claimant_Date_Of_Birth));
            OCF23ARrow.Claimant_Gender = doc.getValue(edOcf23Keys.Claimant_Gender);
            OCF23ARrow.Claimant_Telephone = doc.getValue(edOcf23Keys.Claimant_Telephone);
            OCF23ARrow.Claimant_Extension = doc.getValue(edOcf23Keys.Claimant_Extension);
            OCF23ARrow.Claimant_Address1 = doc.getValue(edOcf23Keys.Claimant_Address1);
            OCF23ARrow.Claimant_Address2 = doc.getValue(edOcf23Keys.Claimant_Address2);
            OCF23ARrow.Claimant_City = doc.getValue(edOcf23Keys.Claimant_City);
            OCF23ARrow.Claimant_Province = doc.getValue(edOcf23Keys.Claimant_Province);
            OCF23ARrow.Claimant_Postal_Code = doc.getValue(edOcf23Keys.Claimant_Postal_Code);
            OCF23ARrow.Insurer_IBCInsurerID = doc.getValue(edOcf23Keys.Insurer_IBCInsurerID);
            OCF23ARrow.Insurer_IBCBranchID = doc.getValue(edOcf23Keys.Insurer_IBCBranchID);
            OCF23ARrow.Status_Code = doc.getValue(edOcf23Keys.Status_Code);
            OCF23ARrow.Messages = doc.getValue(edOcf23Keys.Messages);
            OCF23ARrow.OCF23_InsurerTotals_AutoInsurerTotal_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Approved));
            OCF23ARrow.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved));
            OCF23ARrow.OCF23_InsurerTotals_SubTotalPreApproved_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Approved));
            OCF23ARrow.OCF23_InsurerSignature_ApplicantSignatureWaived = doc.getValue(edOcf23Keys.OCF23_InsurerSignature_ApplicantSignatureWaived);
            OCF23ARrow.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices = doc.getValue(edOcf23Keys.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices);
            OCF23ARrow.OCF23_InsurerSignature_PolicyInForceConfirmation = doc.getValue(edOcf23Keys.OCF23_InsurerSignature_PolicyInForceConfirmation);
            OCF23ARrow.SigningAdjuster_FirstName = doc.getValue(edOcf23Keys.SigningAdjuster_FirstName);
            OCF23ARrow.SigningAdjuster_LastName = doc.getValue(edOcf23Keys.SigningAdjuster_LastName);
            OCF23ARrow.Archival_Status = doc.getValue(edOcf23Keys.Archival_Status);

            //Add row to OCF23ARrow table
            _mainDataSet.OCF23AR.AddOCF23ARRow(OCF23ARrow);

            //OtherGSLineItemAR
            
            var ARoList = doc.getList(LineItemType.DE_GS23OtherGSLineItem_Approved);

            if (ARoList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF23 OtherGSLineItemSubmit", 500);
                for (int i = 0; i < ARoList.Count; i++)
                {
                    var OtherGSLineItemAR = _mainDataSet.OtherGSLineItemAR.NewOtherGSLineItemARRow();
                    var tmpItem = (IDataItem)ARoList[i];
                    Debuger.WriteToFile("DataExtract: OCF23 OtherGSLineItemSubmit", 500);

                    OtherGSLineItemAR.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OtherGSLineItemAR.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    OtherGSLineItemAR.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost));
                    OtherGSLineItemAR.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    OtherGSLineItemAR.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    OtherGSLineItemAR.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    _mainDataSet.OtherGSLineItemAR.AddOtherGSLineItemARRow(OtherGSLineItemAR);
                }
            }            



            //throw new NotImplementedException();
        }

        private void ExtractOcf21C(IDataItem doc, ref string Papa_HCAI_Document_Number)
        {
            var edOcf21CKeys = new PMSToolkit.DataObjects.OCF21CDataExtractResponseKeys();
            var rowSubmited = _mainDataSet.OCF21CSubmit.NewOCF21CSubmitRow();

            var edOcf21CLisKeys = new PMSToolkit.DataObjects.OCF21CDataExtractResponseLineItemKeys();

            rowSubmited.HCAI_Document_Number = doc.getValue(edOcf21CKeys.HCAI_Document_Number);
            Papa_HCAI_Document_Number = rowSubmited.HCAI_Document_Number;

            rowSubmited.PMSFields_PMSSoftware = doc.getValue(edOcf21CKeys.PMSFields_PMSSoftware);
            rowSubmited.PMSFields_PMSVersion = doc.getValue(edOcf21CKeys.PMSFields_PMSVersion);
            rowSubmited.PMSFields_PMSDocumentKey = doc.getValue(edOcf21CKeys.PMSFields_PMSDocumentKey);
            rowSubmited.PMSFields_PMSPatientKey = doc.getValue(edOcf21CKeys.PMSFields_PMSPatientKey);
            rowSubmited.AttachmentsBeingSent = doc.getValue(edOcf21CKeys.AttachmentsBeingSent);
            //'WRONG
            rowSubmited.OCF21C_HCAI_Plan_Document_Number = doc.getValue(edOcf21CKeys.OCF21C_HCAI_Plan_Document_Number);
            rowSubmited.Header_ClaimNumber = doc.getValue(edOcf21CKeys.Header_ClaimNumber);
            rowSubmited.Header_PolicyNumber = doc.getValue(edOcf21CKeys.Header_PolicyNumber);
            rowSubmited.Header_DateOfAccident = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.Header_DateOfAccident));
            rowSubmited.Applicant_Name_FirstName = doc.getValue(edOcf21CKeys.Applicant_Name_FirstName);
            rowSubmited.Applicant_Name_MiddleName = doc.getValue(edOcf21CKeys.Applicant_Name_MiddleName);
            rowSubmited.Applicant_Name_LastName = doc.getValue(edOcf21CKeys.Applicant_Name_LastName);
            rowSubmited.Applicant_DateOfBirth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.Applicant_DateOfBirth));
            rowSubmited.Applicant_Gender = doc.getValue(edOcf21CKeys.Applicant_Gender);
            rowSubmited.Applicant_TelephoneNumber = doc.getValue(edOcf21CKeys.Applicant_TelephoneNumber);
            rowSubmited.Applicant_TelephoneExtension = doc.getValue(edOcf21CKeys.Applicant_TelephoneExtension);
            rowSubmited.Applicant_Address_StreetAddress1 = doc.getValue(edOcf21CKeys.Applicant_Address_StreetAddress1);
            rowSubmited.Applicant_Address_StreetAddress2 = doc.getValue(edOcf21CKeys.Applicant_Address_StreetAddress2);
            rowSubmited.Applicant_Address_City = doc.getValue(edOcf21CKeys.Applicant_Address_City);
            rowSubmited.Applicant_Address_Province = doc.getValue(edOcf21CKeys.Applicant_Address_Province);
            rowSubmited.Applicant_Address_PostalCode = doc.getValue(edOcf21CKeys.Applicant_Address_PostalCode);
            rowSubmited.Insurer_IBCInsurerID = doc.getValue(edOcf21CKeys.Insurer_IBCInsurerID);
            rowSubmited.Insurer_IBCBranchID = doc.getValue(edOcf21CKeys.Insurer_IBCBranchID);
            rowSubmited.Insurer_Adjuster_Name_FirstName = doc.getValue(edOcf21CKeys.Insurer_Adjuster_Name_FirstName);
            rowSubmited.Insurer_Adjuster_Name_LastName = doc.getValue(edOcf21CKeys.Insurer_Adjuster_Name_LastName);
            rowSubmited.Insurer_Adjuster_TelephoneNumber = doc.getValue(edOcf21CKeys.Insurer_Adjuster_TelephoneNumber);
            rowSubmited.Insurer_Adjuster_TelephoneExtension = doc.getValue(edOcf21CKeys.Insurer_Adjuster_TelephoneExtension);
            rowSubmited.Insurer_Adjuster_FaxNumber = doc.getValue(edOcf21CKeys.Insurer_Adjuster_FaxNumber);
            rowSubmited.Insurer_PolicyHolder_IsSameAsApplicant = doc.getValue(edOcf21CKeys.Insurer_PolicyHolder_IsSameAsApplicant);
            rowSubmited.Insurer_PolicyHolder_Name_FirstName = doc.getValue(edOcf21CKeys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmited.Insurer_PolicyHolder_Name_LastName = doc.getValue(edOcf21CKeys.Insurer_PolicyHolder_Name_LastName);
            rowSubmited.OCF21C_InvoiceInformation_InvoiceNumber = doc.getValue(edOcf21CKeys.OCF21C_InvoiceInformation_InvoiceNumber);
            rowSubmited.OCF21C_InvoiceInformation_FirstInvoice = doc.getValue(edOcf21CKeys.OCF21C_InvoiceInformation_FirstInvoice);
            rowSubmited.OCF21C_InvoiceInformation_LastInvoice = doc.getValue(edOcf21CKeys.OCF21C_InvoiceInformation_LastInvoice);
            rowSubmited.OCF21C_Payee_FacilityRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf21CKeys.OCF21C_Payee_FacilityRegistryID));
            rowSubmited.OCF21C_Payee_MakeChequePayableTo = doc.getValue(edOcf21CKeys.OCF21C_Payee_MakeChequePayableTo);
            rowSubmited.OCF21C_Payee_ConflictOfInterests_ConflictExists = doc.getValue(edOcf21CKeys.OCF21C_Payee_ConflictOfInterests_ConflictExists);
            rowSubmited.OCF21C_Payee_ConflictOfInterests_ConflictDetails = doc.getValue(edOcf21CKeys.OCF21C_Payee_ConflictOfInterests_ConflictDetails);
            rowSubmited.OCF21C_PAFReimbursableFees_PAFType = doc.getValue(edOcf21CKeys.OCF21C_PAFReimbursableFees_PAFType);
            rowSubmited.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic));
            rowSubmited.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy));
            rowSubmited.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy));
            rowSubmited.OCF21C_OtherInsuranceAmounts_MOH_OtherService = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_OtherService));
            rowSubmited.OCF21C_OtherInsuranceAmounts_MOH_Total_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Proposed));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService));
            rowSubmited.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed));
            rowSubmited.OCF21C_OtherInsuranceAmounts_OtherServiceType = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_OtherServiceType);
            rowSubmited.OCF21C_AccountActivity_PriorBalance = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_AccountActivity_PriorBalance));
            rowSubmited.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer));
            rowSubmited.OCF21C_AccountActivity_OverdueAmount = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_AccountActivity_OverdueAmount));
            rowSubmited.OCF21C_InsurerTotals_OtherInsurers_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Proposed));
            rowSubmited.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed));
            rowSubmited.OCF21C_InsurerTotals_GST_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Proposed));
            rowSubmited.OCF21C_InsurerTotals_PST_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Proposed));
            rowSubmited.OCF21C_InsurerTotals_Interest_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Proposed));
            rowSubmited.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed));
            rowSubmited.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed));
            rowSubmited.OCF21C_OtherInformation = doc.getValue(edOcf21CKeys.OCF21C_OtherInformation);
            rowSubmited.AdditionalComments = doc.getValue(edOcf21CKeys.AdditionalComments);
            //rowSubmited.DEC_Error_Count = doc.getValue(edOcf21CKeys.DEC_Error_Count);
            //rowSubmited.DEC_Received_Date = doc.getValue(edOcf21CKeys.DEC_Received_Date);
            rowSubmited.OCF21C_PreviouslyApprovedGoodsAndServices_Type = doc.getValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type);
            rowSubmited.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate));
            rowSubmited.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber = doc.getValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber);
            rowSubmited.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved));
            rowSubmited.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled));
            rowSubmited.OCFVersion = MyConvert.ConvertStrToInt(doc.getValue(edOcf21CKeys.OCFVersion));
                       
            //Add row to OCF21B Submit table
            _mainDataSet.OCF21CSubmit.AddOCF21CSubmitRow(rowSubmited);

            //RenderedGSLineItemSubmit
            
            var rnList = doc.getList(LineItemType.GS21CRenderedGSLineItem);

            if (rnList != null)
            {
                Debuger.WriteToFile("DataExtract: RenderedGSLineItemSubmit", 500);
                for (int i = 0; i < rnList.Count; i++)
                {
                    var RenderedGSLineItemSubmit = _mainDataSet.RenderedGSLineItemSubmit.NewRenderedGSLineItemSubmitRow();
                    var tmpItem = (IDataItem)rnList[i];
                    Debuger.WriteToFile("DataExtract: RenderedGSLineItemSubmit", 500);
                    RenderedGSLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Code = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Code));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Measure = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Measure));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService = MyConvert.MyHCAIDateConvert(tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService));
                    RenderedGSLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ReferenceNumber = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ReferenceNumber));

                    _mainDataSet.RenderedGSLineItemSubmit.AddRenderedGSLineItemSubmitRow(RenderedGSLineItemSubmit);

                }
            }


            //OCF21CInjuryLineItemSubmit
            
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF21CInjuryLineItemSubmit", 500);
                for (int i = 0; i < injList.Count; i++)
                {
                    var OCF21CInjuryLineItemSubmit = _mainDataSet.OCF21CInjuryLineItemSubmit.NewOCF21CInjuryLineItemSubmitRow();
                    var tmpItem = (IDataItem)injList[i];
                    Debuger.WriteToFile("DataExtract: OCF21CInjuryLineItemSubmit ", 500);
                    OCF21CInjuryLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OCF21CInjuryLineItemSubmit.OCF21C_InjuriesAndSequelae_Injury_Code = tmpItem.getValue(edOcf21CLisKeys.OCF21C_InjuriesAndSequelae_Injury_Code);

                    _mainDataSet.OCF21CInjuryLineItemSubmit.AddOCF21CInjuryLineItemSubmitRow(OCF21CInjuryLineItemSubmit);
                }
            }

            //OtherReimbursableLineItemSubmit
            
            var orList = doc.getList(LineItemType.DE_GS21COtherReimbursableLineItem_Estimated);

            if (orList != null)
            {
                Debuger.WriteToFile("DataExtract: OtherReimbursableLineItemSubmit", 500);
                for (int i = 0; i < orList.Count; i++)
                {
                    var OtherReimbursableLineItemSubmit = _mainDataSet.OtherReimbursableLineItemSubmit.NewOtherReimbursableLineItemSubmitRow();
                    var tmpItem = (IDataItem)orList[i];
                    Debuger.WriteToFile("DataExtract: OtherReimbursableLineItemSubmit", 500);
                    OtherReimbursableLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService = MyConvert.MyHCAIDateConvert(tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost));
                    OtherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ReferenceNumber = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ReferenceNumber));
                    
                    _mainDataSet.OtherReimbursableLineItemSubmit.AddOtherReimbursableLineItemSubmitRow(OtherReimbursableLineItemSubmit);

                }
            }


            //PAFReimbursableLineItemSubmit
            
            var pafList = doc.getList(LineItemType.DE_GS21CPAFReimbursableLineItem_Estimated);

            if (pafList != null)
            {
                Debuger.WriteToFile("DataExtract: OtherReimbursableLineItemSubmit", 500);
                for (int i = 0; i < pafList.Count; i++)
                {
                    var PAFReimbursableLineItemSubmit = _mainDataSet.PAFReimbursableLineItemSubmit.NewPAFReimbursableLineItemSubmitRow();
                    var tmpItem = (IDataItem)pafList[i];
                    Debuger.WriteToFile("DataExtract: OtherReimbursableLineItemSubmit", 500);
                    PAFReimbursableLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    PAFReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey));
                    PAFReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Code = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Code));
                    PAFReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Attribute = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Attribute));
                    PAFReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost));
                    
                    _mainDataSet.PAFReimbursableLineItemSubmit.AddPAFReimbursableLineItemSubmitRow(PAFReimbursableLineItemSubmit);

                }
            }


            //OCF21C Adjuster Response Table
            var OCF21CAR = _mainDataSet.OCF21CAR.NewOCF21CARRow();
            OCF21CAR.HCAI_Document_Number = doc.getValue(edOcf21CKeys.HCAI_Document_Number);
            OCF21CAR.PMSFields_PMSDocumentKey = doc.getValue(edOcf21CKeys.PMSFields_PMSDocumentKey);
            OCF21CAR.Submission_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.Submission_Date));
            OCF21CAR.Submission_Source = doc.getValue(edOcf21CKeys.Submission_Source);
            OCF21CAR.Adjuster_Response_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.Adjuster_Response_Date));
            OCF21CAR.In_Dispute = doc.getValue(edOcf21CKeys.In_Dispute);
            OCF21CAR.Document_Type = doc.getValue(edOcf21CKeys.Document_Type);
            OCF21CAR.Document_Status = doc.getValue(edOcf21CKeys.Document_Status);
            OCF21CAR.Attachments_Received_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.Attachments_Received_Date));
            OCF21CAR.Claimant_First_Name = doc.getValue(edOcf21CKeys.Claimant_First_Name);
            OCF21CAR.Claimant_Middle_Name = doc.getValue(edOcf21CKeys.Claimant_Middle_Name);
            OCF21CAR.Claimant_Last_Name = doc.getValue(edOcf21CKeys.Claimant_Last_Name);
            OCF21CAR.Claimant_Date_Of_Birth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21CKeys.Claimant_Date_Of_Birth));
            OCF21CAR.Claimant_Gender = doc.getValue(edOcf21CKeys.Claimant_Gender);
            OCF21CAR.Claimant_Telephone = doc.getValue(edOcf21CKeys.Claimant_Telephone);
            OCF21CAR.Claimant_Extension = doc.getValue(edOcf21CKeys.Claimant_Extension);
            OCF21CAR.Claimant_Address1 = doc.getValue(edOcf21CKeys.Claimant_Address1);
            OCF21CAR.Claimant_Address2 = doc.getValue(edOcf21CKeys.Claimant_Address2);
            OCF21CAR.Claimant_City = doc.getValue(edOcf21CKeys.Claimant_City);
            OCF21CAR.Claimant_Province = doc.getValue(edOcf21CKeys.Claimant_Province);
            OCF21CAR.Claimant_Postal_Code = doc.getValue(edOcf21CKeys.Claimant_Postal_Code);
            OCF21CAR.Insurer_IBCInsurerID = doc.getValue(edOcf21CKeys.Insurer_IBCInsurerID);
            OCF21CAR.Insurer_IBCBranchID = doc.getValue(edOcf21CKeys.Insurer_IBCBranchID);
            OCF21CAR.Status_Code = doc.getValue(edOcf21CKeys.Status_Code);
            OCF21CAR.Messages = doc.getValue(edOcf21CKeys.Messages);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost));
            OCF21CAR.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost));
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost));
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21CAR.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_MOH_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_MOH_Approved));
            OCF21CAR.OCF21C_InsurerTotals_OtherInsurers_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Approved));
            OCF21CAR.OCF21C_InsurerTotals_AutoInsurerTotal_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Approved));
            OCF21CAR.OCF21C_InsurerTotals_GST_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_LineCost));
            OCF21CAR.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            OCF21CAR.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_PST_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_LineCost));
            OCF21CAR.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            OCF21CAR.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_Interest_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_LineCost));
            OCF21CAR.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
            OCF21CAR.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21CAR.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved));
            OCF21CAR.OCF21C_InsurerTotals_SubTotalPreApproved_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Approved));
            OCF21CAR.SigningAdjuster_FirstName = doc.getValue(edOcf21CKeys.SigningAdjuster_FirstName);
            OCF21CAR.SigningAdjuster_LastName = doc.getValue(edOcf21CKeys.SigningAdjuster_LastName);
            OCF21CAR.Archival_Status = doc.getValue(edOcf21CKeys.Archival_Status);
          
            //Add row to OCF21CAR table
            _mainDataSet.OCF21CAR.AddOCF21CARRow(OCF21CAR);


            //OtherReimbursableLineItemSubmit
            
            var orARList = doc.getList(LineItemType.DE_GS21COtherReimbursableLineItem_Approved);

            if (orARList != null)
            {
                Debuger.WriteToFile("DataExtract: OtherReimbursableLineItemSubmit", 500);
                for (int i = 0; i < orARList.Count; i++)
                {
                    var OtherReimbursableLineItemAR = _mainDataSet.OtherReimbursableLineItemAR.NewOtherReimbursableLineItemARRow();
                    var tmpItem = (IDataItem)orARList[i];
                    Debuger.WriteToFile("DataExtract: OtherReimbursableLineItemSubmit", 500);
                    OtherReimbursableLineItemAR.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey));
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST));
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST));
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost));
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode));
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc));
                    OtherReimbursableLineItemAR.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc));

                    _mainDataSet.OtherReimbursableLineItemAR.AddOtherReimbursableLineItemARRow(OtherReimbursableLineItemAR);
                }
            }


            //PAFReimbursableLineItemAR
            
            var pafARList = doc.getList(LineItemType.DE_GS21CPAFReimbursableLineItem_Approved);

            if (pafARList != null)
            {
                Debuger.WriteToFile("DataExtract: PAFReimbursableLineItemAR", 500);
                for (int i = 0; i < pafARList.Count; i++)
                {
                    var PAFReimbursableLineItemAR = _mainDataSet.PAFReimbursableLineItemAR.NewPAFReimbursableLineItemARRow();
                    var tmpItem = (IDataItem)pafARList[i];
                    Debuger.WriteToFile("DataExtract: PAFReimbursableLineItemAR", 500);

                    PAFReimbursableLineItemAR.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    PAFReimbursableLineItemAR.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey));
                    PAFReimbursableLineItemAR.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost));
                    PAFReimbursableLineItemAR.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode));
                    PAFReimbursableLineItemAR.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc));
                    PAFReimbursableLineItemAR.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = (tmpItem.getValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc));

                    _mainDataSet.PAFReimbursableLineItemAR.AddPAFReimbursableLineItemARRow(PAFReimbursableLineItemAR);
                }
            }


            //throw new NotImplementedException();
        }

        private void ExtractOcf21B(IDataItem doc, ref string Papa_HCAI_Document_Number)
        {

            var edOcf21BKeys = new PMSToolkit.DataObjects.OCF21BDataExtractResponseKeys();
            var rowSubmited = _mainDataSet.OCF21BSubmit.NewOCF21BSubmitRow();

            var edOcf21BLisKeys = new PMSToolkit.DataObjects.OCF21BDataExtractResponseLineItemKeys();

            rowSubmited.HCAI_Document_Number = doc.getValue(edOcf21BKeys.HCAI_Document_Number);
            Papa_HCAI_Document_Number = rowSubmited.HCAI_Document_Number;

            rowSubmited.PMSFields_PMSSoftware = doc.getValue(edOcf21BKeys.PMSFields_PMSSoftware);

            rowSubmited.PMSFields_PMSVersion = doc.getValue(edOcf21BKeys.PMSFields_PMSVersion);
            rowSubmited.PMSFields_PMSDocumentKey = doc.getValue(edOcf21BKeys.PMSFields_PMSDocumentKey);
            rowSubmited.PMSFields_PMSPatientKey = doc.getValue(edOcf21BKeys.PMSFields_PMSPatientKey);
            rowSubmited.AttachmentsBeingSent = doc.getValue(edOcf21BKeys.AttachmentsBeingSent);
            rowSubmited.OCF21B_HCAI_Plan_Document_Number = doc.getValue(edOcf21BKeys.OCF21B_HCAI_Plan_Document_Number);
            rowSubmited.Header_ClaimNumber = doc.getValue(edOcf21BKeys.Header_ClaimNumber);
            rowSubmited.Header_PolicyNumber = doc.getValue(edOcf21BKeys.Header_PolicyNumber);
            rowSubmited.Header_DateOfAccident = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.Header_DateOfAccident));
            rowSubmited.Applicant_Name_FirstName = doc.getValue(edOcf21BKeys.Applicant_Name_FirstName);
            rowSubmited.Applicant_Name_MiddleName = doc.getValue(edOcf21BKeys.Applicant_Name_MiddleName);
            rowSubmited.Applicant_Name_LastName = doc.getValue(edOcf21BKeys.Applicant_Name_LastName);
            rowSubmited.Applicant_DateOfBirth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.Applicant_DateOfBirth));
            rowSubmited.Applicant_Gender = doc.getValue(edOcf21BKeys.Applicant_Gender);
            rowSubmited.Applicant_TelephoneNumber = doc.getValue(edOcf21BKeys.Applicant_TelephoneNumber);
            rowSubmited.Applicant_TelephoneExtension = doc.getValue(edOcf21BKeys.Applicant_TelephoneExtension);
            rowSubmited.Applicant_Address_StreetAddress1 = doc.getValue(edOcf21BKeys.Applicant_Address_StreetAddress1);
            rowSubmited.Applicant_Address_StreetAddress2 = doc.getValue(edOcf21BKeys.Applicant_Address_StreetAddress2);
            rowSubmited.Applicant_Address_City = doc.getValue(edOcf21BKeys.Applicant_Address_City);
            rowSubmited.Applicant_Address_Province = doc.getValue(edOcf21BKeys.Applicant_Address_Province);
            rowSubmited.Applicant_Address_PostalCode = doc.getValue(edOcf21BKeys.Applicant_Address_PostalCode);
            rowSubmited.Insurer_IBCInsurerID = doc.getValue(edOcf21BKeys.Insurer_IBCInsurerID);
            rowSubmited.Insurer_IBCBranchID = doc.getValue(edOcf21BKeys.Insurer_IBCBranchID);
            rowSubmited.Insurer_Adjuster_Name_FirstName = doc.getValue(edOcf21BKeys.Insurer_Adjuster_Name_FirstName);
            rowSubmited.Insurer_Adjuster_Name_LastName = doc.getValue(edOcf21BKeys.Insurer_Adjuster_Name_LastName);
            rowSubmited.Insurer_Adjuster_TelephoneNumber = doc.getValue(edOcf21BKeys.Insurer_Adjuster_TelephoneNumber);
            rowSubmited.Insurer_Adjuster_TelephoneExtension = doc.getValue(edOcf21BKeys.Insurer_Adjuster_TelephoneExtension);
            rowSubmited.Insurer_Adjuster_FaxNumber = doc.getValue(edOcf21BKeys.Insurer_Adjuster_FaxNumber);
            rowSubmited.Insurer_PolicyHolder_IsSameAsApplicant = doc.getValue(edOcf21BKeys.Insurer_PolicyHolder_IsSameAsApplicant);
            rowSubmited.Insurer_PolicyHolder_Name_FirstName = doc.getValue(edOcf21BKeys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmited.Insurer_PolicyHolder_Name_LastName = doc.getValue(edOcf21BKeys.Insurer_PolicyHolder_Name_LastName);
            rowSubmited.OCF21B_InvoiceInformation_InvoiceNumber = doc.getValue(edOcf21BKeys.OCF21B_InvoiceInformation_InvoiceNumber);
            rowSubmited.OCF21B_InvoiceInformation_FirstInvoice = doc.getValue(edOcf21BKeys.OCF21B_InvoiceInformation_FirstInvoice);
            rowSubmited.OCF21B_InvoiceInformation_LastInvoice = doc.getValue(edOcf21BKeys.OCF21B_InvoiceInformation_LastInvoice);
            rowSubmited.OCF21B_PreviouslyApprovedGoodsAndServices_Type = doc.getValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type);
            rowSubmited.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate));
            rowSubmited.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber = doc.getValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber);
            rowSubmited.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved));
            rowSubmited.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled =MyConvert.ConvertStrToFloat( doc.getValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled));
            rowSubmited.OCF21B_Payee_FacilityRegistryID = MyConvert.ConvertStrToInt( doc.getValue(edOcf21BKeys.OCF21B_Payee_FacilityRegistryID));
            rowSubmited.OCF21B_Payee_MakeChequePayableTo = doc.getValue(edOcf21BKeys.OCF21B_Payee_MakeChequePayableTo);
            rowSubmited.OCF21B_Payee_ConflictOfInterests_ConflictExists = doc.getValue(edOcf21BKeys.OCF21B_Payee_ConflictOfInterests_ConflictExists);
            rowSubmited.OCF21B_Payee_ConflictOfInterests_ConflictDetails = doc.getValue(edOcf21BKeys.OCF21B_Payee_ConflictOfInterests_ConflictDetails);
            rowSubmited.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage);
            rowSubmited.OCF21B_OtherInsurance_MOHAvailable = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_MOHAvailable);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmited.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.getValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
            rowSubmited.OCF21B_OtherInsuranceAmounts_MOH_Chiropractic = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Chiropractic));
            rowSubmited.OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy));
            rowSubmited.OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy));
            rowSubmited.OCF21B_OtherInsuranceAmounts_MOH_OtherService = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_OtherService));
            rowSubmited.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService));
            rowSubmited.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed));
            rowSubmited.OCF21B_OtherInsuranceAmounts_OtherServiceType = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_OtherServiceType);
            rowSubmited.OCF21B_AccountActivity_PriorBalance = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_AccountActivity_PriorBalance));
            rowSubmited.OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer));
            rowSubmited.OCF21B_AccountActivity_OverdueAmount = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_AccountActivity_OverdueAmount));
            rowSubmited.OCF21B_InsurerTotals_OtherInsurers_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_OtherInsurers_Proposed));
            rowSubmited.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed =MyConvert.ConvertStrToFloat( doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed));
            rowSubmited.OCF21B_InsurerTotals_GST_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Proposed));
            rowSubmited.OCF21B_InsurerTotals_PST_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Proposed));
            rowSubmited.OCF21B_InsurerTotals_SubTotal_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_SubTotal_Proposed));
            rowSubmited.OCF21B_InsurerTotals_Interest_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Proposed));
            rowSubmited.OCF21B_OtherInformation = doc.getValue(edOcf21BKeys.OCF21B_OtherInformation);
            rowSubmited.AdditionalComments = doc.getValue(edOcf21BKeys.AdditionalComments);
            //rowSubmited.DEC_Error_Count = doc.getValue(edOcf21BKeys.DEC_Error_Count);
            //rowSubmited.DEC_Received_Date = doc.getValue(edOcf21BKeys.DEC_Received_Date);
            rowSubmited.OCFVersion = MyConvert.ConvertStrToInt( doc.getValue(edOcf21BKeys.OCFVersion));


            //Add row to OCF21B Submit table
            _mainDataSet.OCF21BSubmit.AddOCF21BSubmitRow(rowSubmited);


            //GS21BLineItemSubmit
            
            var gsList = doc.getList(LineItemType.DE_GS21BLineItem_Estimated);

            if (gsList != null)
            {
                Debuger.WriteToFile("DataExtract: GS21BLineItemSubmit", 500);

                for (int i = 0; i < gsList.Count; i++)
                {
                    var GS21BLineItemSubmitRow = _mainDataSet.GS21BLineItemSubmit.NewGS21BLineItemSubmitRow();
                    var tmpItem = (IDataItem)gsList[i];
                    Debuger.WriteToFile("DataExtract: GS21BLineItemSubmit", 500);
                    GS21BLineItemSubmitRow.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID));                    
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService = MyConvert.MyHCAIDateConvert(tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST = (tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost));
                    GS21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber));
                   
                    _mainDataSet.GS21BLineItemSubmit.AddGS21BLineItemSubmitRow(GS21BLineItemSubmitRow);

                }
            }


            //OCF21BInjuryLineItemSubmit
            
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF21BInjuryLineItemSubmit", 500);
                for (int i = 0; i < injList.Count; i++)
                {
                    var OCF21BInjuryLineItemSubmit = _mainDataSet.OCF21BInjuryLineItemSubmit.NewOCF21BInjuryLineItemSubmitRow();
                    var tmpItem = (IDataItem)injList[i];
                    Debuger.WriteToFile("DataExtract: OCF21BInjuryLineItemSubmit ", 500);
                    OCF21BInjuryLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OCF21BInjuryLineItemSubmit.OCF21B_InjuriesAndSequelae_Injury_Code = tmpItem.getValue(edOcf21BLisKeys.OCF21B_InjuriesAndSequelae_Injury_Code);

                    _mainDataSet.OCF21BInjuryLineItemSubmit.AddOCF21BInjuryLineItemSubmitRow(OCF21BInjuryLineItemSubmit);
                }
            }



            //OCF21B Adjuster Response Table
            var OCF21BARrow = _mainDataSet.OCF21BAR.NewOCF21BARRow();
            OCF21BARrow.HCAI_Document_Number = doc.getValue(edOcf21BKeys.HCAI_Document_Number);

            OCF21BARrow.PMSFields_PMSDocumentKey = doc.getValue(edOcf21BKeys.PMSFields_PMSDocumentKey);
            OCF21BARrow.Submission_Date =  MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.Submission_Date));
            OCF21BARrow.Submission_Source = doc.getValue(edOcf21BKeys.Submission_Source);
            OCF21BARrow.Adjuster_Response_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.Adjuster_Response_Date));
            OCF21BARrow.In_Dispute = doc.getValue(edOcf21BKeys.In_Dispute);
            OCF21BARrow.Document_Type = doc.getValue(edOcf21BKeys.Document_Type);
            OCF21BARrow.Document_Status = doc.getValue(edOcf21BKeys.Document_Status);
            OCF21BARrow.Attachments_Received_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.Attachments_Received_Date));
            OCF21BARrow.Claimant_First_Name = doc.getValue(edOcf21BKeys.Claimant_First_Name);
            OCF21BARrow.Claimant_Middle_Name = doc.getValue(edOcf21BKeys.Claimant_Middle_Name);
            OCF21BARrow.Claimant_Last_Name = doc.getValue(edOcf21BKeys.Claimant_Last_Name);
            OCF21BARrow.Claimant_Date_Of_Birth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf21BKeys.Claimant_Date_Of_Birth));
            OCF21BARrow.Claimant_Gender = doc.getValue(edOcf21BKeys.Claimant_Gender);
            OCF21BARrow.Claimant_Telephone = doc.getValue(edOcf21BKeys.Claimant_Telephone);
            OCF21BARrow.Claimant_Extension = doc.getValue(edOcf21BKeys.Claimant_Extension);
            OCF21BARrow.Claimant_Address1 = doc.getValue(edOcf21BKeys.Claimant_Address1);
            OCF21BARrow.Claimant_Address2 = doc.getValue(edOcf21BKeys.Claimant_Address2);
            OCF21BARrow.Claimant_City = doc.getValue(edOcf21BKeys.Claimant_City);
            OCF21BARrow.Claimant_Province = doc.getValue(edOcf21BKeys.Claimant_Province);
            OCF21BARrow.Claimant_Postal_Code = doc.getValue(edOcf21BKeys.Claimant_Postal_Code);
            OCF21BARrow.Insurer_IBCInsurerID = doc.getValue(edOcf21BKeys.Insurer_IBCInsurerID);
            OCF21BARrow.Insurer_IBCBranchID = doc.getValue(edOcf21BKeys.Insurer_IBCBranchID);
            OCF21BARrow.Status_Code = doc.getValue(edOcf21BKeys.Status_Code);
            OCF21BARrow.Messages = doc.getValue(edOcf21BKeys.Messages);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost));
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost));
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost));
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21BARrow.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21BARrow.OCF21B_InsurerTotals_MOH_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_MOH_Approved));
            OCF21BARrow.OCF21B_InsurerTotals_OtherInsurers_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_OtherInsurers_Approved));
            OCF21BARrow.OCF21B_InsurerTotals_AutoInsurerTotal_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Approved));
            OCF21BARrow.OCF21B_InsurerTotals_GST_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_LineCost));
            OCF21BARrow.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            OCF21BARrow.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21BARrow.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21BARrow.OCF21B_InsurerTotals_PST_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_LineCost));
            OCF21BARrow.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            OCF21BARrow.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21BARrow.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21BARrow.OCF21B_InsurerTotals_SubTotal_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_SubTotal_Approved));
            OCF21BARrow.OCF21B_InsurerTotals_Interest_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_LineCost));
            OCF21BARrow.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
            OCF21BARrow.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF21BARrow.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF21BARrow.SigningAdjuster_FirstName = doc.getValue(edOcf21BKeys.SigningAdjuster_FirstName);
            OCF21BARrow.SigningAdjuster_LastName = doc.getValue(edOcf21BKeys.SigningAdjuster_LastName);
            OCF21BARrow.Archival_Status = doc.getValue(edOcf21BKeys.Archival_Status);
            
            //Add row to OCF18AR table
            _mainDataSet.OCF21BAR.AddOCF21BARRow(OCF21BARrow);
            
            //GS21BLineItemAR
            var arList = doc.getList(LineItemType.DE_GS21BLineItem_Approved);

            if (arList != null)
            {
                Debuger.WriteToFile("DataExtract: GS21BLineItemAR", 500);
                for (int i = 0; i < arList.Count; i++)
                {
                    var GS21BLineItemAR = _mainDataSet.GS21BLineItemAR.NewGS21BLineItemARRow();
                    var tmpItem = (IDataItem)arList[i];

                    Debuger.WriteToFile("DataExtract: GS21BLineItemAR ", 500);
                    GS21BLineItemAR.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;

                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey = tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST = tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST);
                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST = tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST);
                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost));
                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    GS21BLineItemAR.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    _mainDataSet.GS21BLineItemAR.AddGS21BLineItemARRow(GS21BLineItemAR);
                }
            }

            ////throw new NotImplementedException();

        }




        private void ExtractOcf18(IDataItem doc, ref string Papa_HCAI_Document_Number)
        {
            var edOcf18Keys = new PMSToolkit.DataObjects.OCF18DataExtractResponseKeys();
            var rowSubmited = _mainDataSet.OCF18Submit.NewOCF18SubmitRow();

            var edOcf18LisKeys = new PMSToolkit.DataObjects.OCF18DataExtractResponseLineItemKeys();

            rowSubmited.HCAI_Document_Number = doc.getValue(edOcf18Keys.HCAI_Document_Number);

            Papa_HCAI_Document_Number = rowSubmited.HCAI_Document_Number;

            rowSubmited.PMSFields_PMSSoftware = doc.getValue(edOcf18Keys.PMSFields_PMSSoftware);
            rowSubmited.PMSFields_PMSVersion = doc.getValue(edOcf18Keys.PMSFields_PMSVersion);
            rowSubmited.PMSFields_PMSDocumentKey = doc.getValue(edOcf18Keys.PMSFields_PMSDocumentKey);
            rowSubmited.PMSFields_PMSPatientKey = doc.getValue(edOcf18Keys.PMSFields_PMSPatientKey);
            rowSubmited.AttachmentsBeingSent = doc.getValue(edOcf18Keys.AttachmentsBeingSent);
            rowSubmited.Header_ClaimNumber = doc.getValue(edOcf18Keys.Header_ClaimNumber);
            rowSubmited.Header_PolicyNumber = doc.getValue(edOcf18Keys.Header_PolicyNumber);
            rowSubmited.Header_DateOfAccident = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.Header_DateOfAccident));
            rowSubmited.OCF18_Header_PlanNumber = doc.getValue(edOcf18Keys.OCF18_Header_PlanNumber);
            rowSubmited.Applicant_Name_FirstName = doc.getValue(edOcf18Keys.Applicant_Name_FirstName);
            rowSubmited.Applicant_Name_MiddleName = doc.getValue(edOcf18Keys.Applicant_Name_MiddleName);
            rowSubmited.Applicant_Name_LastName = doc.getValue(edOcf18Keys.Applicant_Name_LastName);
            rowSubmited.Applicant_DateOfBirth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.Applicant_DateOfBirth));
            rowSubmited.Applicant_Gender = doc.getValue(edOcf18Keys.Applicant_Gender);
            rowSubmited.Applicant_TelephoneNumber = doc.getValue(edOcf18Keys.Applicant_TelephoneNumber);
            rowSubmited.Applicant_TelephoneExtension = doc.getValue(edOcf18Keys.Applicant_TelephoneExtension);
            rowSubmited.Applicant_Address_StreetAddress1 = doc.getValue(edOcf18Keys.Applicant_Address_StreetAddress1);
            rowSubmited.Applicant_Address_StreetAddress2 = doc.getValue(edOcf18Keys.Applicant_Address_StreetAddress2);
            rowSubmited.Applicant_Address_City = doc.getValue(edOcf18Keys.Applicant_Address_City);
            rowSubmited.Applicant_Address_Province = doc.getValue(edOcf18Keys.Applicant_Address_Province);
            rowSubmited.Applicant_Address_PostalCode = doc.getValue(edOcf18Keys.Applicant_Address_PostalCode);
            rowSubmited.Insurer_IBCInsurerID = doc.getValue(edOcf18Keys.Insurer_IBCInsurerID);
            rowSubmited.Insurer_IBCBranchID = doc.getValue(edOcf18Keys.Insurer_IBCBranchID);
            rowSubmited.Insurer_Adjuster_Name_FirstName = doc.getValue(edOcf18Keys.Insurer_Adjuster_Name_FirstName);
            rowSubmited.Insurer_Adjuster_Name_LastName = doc.getValue(edOcf18Keys.Insurer_Adjuster_Name_LastName);
            rowSubmited.Insurer_Adjuster_TelephoneNumber = doc.getValue(edOcf18Keys.Insurer_Adjuster_TelephoneNumber);
            rowSubmited.Insurer_Adjuster_TelephoneExtension = doc.getValue(edOcf18Keys.Insurer_Adjuster_TelephoneExtension);
            rowSubmited.Insurer_Adjuster_FaxNumber = doc.getValue(edOcf18Keys.Insurer_Adjuster_FaxNumber);
            rowSubmited.Insurer_PolicyHolder_IsSameAsApplicant = doc.getValue(edOcf18Keys.Insurer_PolicyHolder_IsSameAsApplicant);
            rowSubmited.Insurer_PolicyHolder_Name_FirstName = doc.getValue(edOcf18Keys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmited.Insurer_PolicyHolder_Name_LastName = doc.getValue(edOcf18Keys.Insurer_PolicyHolder_Name_LastName);
            rowSubmited.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage);
            rowSubmited.OCF18_OtherInsurance_MOHAvailable = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_MOHAvailable);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmited.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.getValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
            rowSubmited.OCF18_HealthPractitioner_FacilityRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_FacilityRegistryID));
            rowSubmited.OCF18_HealthPractitioner_ProviderRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_ProviderRegistryID));
            rowSubmited.OCF18_HealthPractitioner_Occupation = doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_Occupation);
            rowSubmited.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists = doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists);
            rowSubmited.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails = doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails);
            rowSubmited.OCF18_HealthPractitioner_IsSignatureOnFile = doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_IsSignatureOnFile);
            rowSubmited.OCF18_HealthPractitioner_DateSigned = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.OCF18_HealthPractitioner_DateSigned));
            rowSubmited.OCF18_IsOtherHealthPractitioner = doc.getValue(edOcf18Keys.OCF18_IsOtherHealthPractitioner);
            rowSubmited.OCF18_OtherHealthPractitioner_Name_FirstName = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Name_FirstName);
            rowSubmited.OCF18_OtherHealthPractitioner_Name_LastName = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Name_LastName);
            rowSubmited.OCF18_OtherHealthPractitioner_FacilityName = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_FacilityName);
            rowSubmited.OCF18_OtherHealthPractitioner_AISIFacilityNumber = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_AISIFacilityNumber);
            rowSubmited.OCF18_OtherHealthPractitioner_Address_StreetAddress1 = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress1);
            rowSubmited.OCF18_OtherHealthPractitioner_Address_StreetAddress2 = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress2);
            rowSubmited.OCF18_OtherHealthPractitioner_Address_City = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_City);
            rowSubmited.OCF18_OtherHealthPractitioner_Address_Province = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_Province);
            rowSubmited.OCF18_OtherHealthPractitioner_Address_PostalCode = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_PostalCode);
            rowSubmited.OCF18_OtherHealthPractitioner_TelephoneNumber = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_TelephoneNumber);
            rowSubmited.OCF18_OtherHealthPractitioner_TelephoneExtension = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_TelephoneExtension);
            rowSubmited.OCF18_OtherHealthPractitioner_FaxNumber = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_FaxNumber);
            rowSubmited.OCF18_OtherHealthPractitioner_Email = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Email);
            rowSubmited.OCF18_OtherHealthPractitioner_ProviderRegistryNumber = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_ProviderRegistryNumber);
            rowSubmited.OCF18_OtherHealthPractitioner_Occupation = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Occupation);
            rowSubmited.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists);
            rowSubmited.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails);
            rowSubmited.OCF18_OtherHealthPractitioner_IsSignatureOnFile = doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_IsSignatureOnFile);
            rowSubmited.OCF18_OtherHealthPractitioner_DateSigned = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.OCF18_OtherHealthPractitioner_DateSigned));
            rowSubmited.OCF18_IsRHPSameAsHealthPractitioner = doc.getValue(edOcf18Keys.OCF18_IsRHPSameAsHealthPractitioner);
            rowSubmited.OCF18_RegulatedHealthProfessional_FacilityRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID));
            rowSubmited.OCF18_RegulatedHealthProfessional_ProviderRegistryID = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_ProviderRegistryID));
            rowSubmited.OCF18_RegulatedHealthProfessional_Occupation = doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_Occupation);
            rowSubmited.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists = doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists);
            rowSubmited.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails = doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails);
            rowSubmited.OCF18_RegulatedHealthProfessional_IsSignatureOnFile = doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_IsSignatureOnFile);
            rowSubmited.OCF18_RegulatedHealthProfessional_DateSigned = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_DateSigned));
            rowSubmited.OCF18_PriorAndConcurrentConditions_PriorCondition_Response = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Response);
            rowSubmited.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation);
            rowSubmited.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response);
            rowSubmited.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);
            rowSubmited.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response);
            rowSubmited.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation);
            rowSubmited.OCF18_PriorAndConcurrentConditions_IsPAF_Response = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Response);
            rowSubmited.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation = doc.getValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation);
            rowSubmited.OCF18_ActivityLimitations_ToEmployment_Response = doc.getValue(edOcf18Keys.OCF18_ActivityLimitations_ToEmployment_Response);
            rowSubmited.OCF18_ActivityLimitations_ToNormalLife_Response = doc.getValue(edOcf18Keys.OCF18_ActivityLimitations_ToNormalLife_Response);
            rowSubmited.OCF18_ActivityLimitations_ImpactOnAbilities = doc.getValue(edOcf18Keys.OCF18_ActivityLimitations_ImpactOnAbilities);
            rowSubmited.OCF18_ActivityLimitations_ModifiedEmployment_Response = doc.getValue(edOcf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Response);
            rowSubmited.OCF18_ActivityLimitations_ModifiedEmployment_Explanation = doc.getValue(edOcf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Explanation);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response);
            rowSubmited.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation = doc.getValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation);
            rowSubmited.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration));
            rowSubmited.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits));
            rowSubmited.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments = doc.getValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments);
            rowSubmited.OCF18_InsurerTotals_MOH_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Proposed));
            rowSubmited.OCF18_InsurerTotals_OtherInsurers_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Proposed));
            rowSubmited.OCF18_InsurerTotals_AutoInsurerTotal_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Proposed));
            rowSubmited.OCF18_InsurerTotals_GST_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_GST_Proposed));
            rowSubmited.OCF18_InsurerTotals_PST_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_PST_Proposed));
            rowSubmited.OCF18_InsurerTotals_SubTotal_Proposed = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_SubTotal_Proposed));
            rowSubmited.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer = doc.getValue(edOcf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer);
            rowSubmited.OCF18_ApplicantSignature_IsApplicantSignatureOnFile = doc.getValue(edOcf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureOnFile);
            rowSubmited.OCF18_ApplicantSignature_SigningApplicant_FirstName = doc.getValue(edOcf18Keys.OCF18_ApplicantSignature_SigningApplicant_FirstName);
            rowSubmited.OCF18_ApplicantSignature_SigningApplicant_LastName = doc.getValue(edOcf18Keys.OCF18_ApplicantSignature_SigningApplicant_LastName);
            rowSubmited.OCF18_ApplicantSignature_SigningDate = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.OCF18_ApplicantSignature_SigningDate));
            //rowSubmited.AdditionalComments = doc.getValue(edOcf18Keys.AdditionalComments);
            //rowSubmited.DEC_Error_Count = doc.getValue(edOcf18Keys.DEC_Error_Count);
            //rowSubmited.DEC_Received_Date = doc.getValue(edOcf18Keys.DEC_Received_Date);
            rowSubmited.OCFVersion = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCFVersion));


            //Add row to OCF18Submit table
            _mainDataSet.OCF18Submit.AddOCF18SubmitRow(rowSubmited);


            //OCF18 Session Header
            
            var shList = doc.getList(LineItemType.DE_GS18SessionHeaderLineItem_Estimated);

            if (shList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF18 Submit Session Header", 500);

                for (int i = 0; i < shList.Count; i++)
                {
                    var sessionHeaderRow = _mainDataSet.SessionHeaderLineItemSubmit.NewSessionHeaderLineItemSubmitRow();
                    var tmpItem = (IDataItem)shList[i];
                    Debuger.WriteToFile("DataExtract: OCF18 Header", 500);
                    sessionHeaderRow.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_ReferenceNumber = Convert.ToInt32(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_ReferenceNumber));
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code);
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Measure = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure);
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Quantity = Convert.ToDouble(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity));
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost = Convert.ToDouble(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost));
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count = Convert.ToInt32(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count));
                    sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost = Convert.ToDouble(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost));

                    _mainDataSet.SessionHeaderLineItemSubmit.AddSessionHeaderLineItemSubmitRow(sessionHeaderRow);
                }                
            }

            //OCF18 Session Line Item
            
            var snList = doc.getList(LineItemType.DE_GS18SessionLineItem_Estimated);

            if (snList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF18 Submit SessionLineItemSubmit", 500);

                for (int i = 0; i < snList.Count; i++)
                {
                    var SessionLineItemSubmit = _mainDataSet.SessionLineItemSubmit.NewSessionLineItemSubmitRow();
                    var tmpItem = (IDataItem)snList[i];
                    Debuger.WriteToFile("DataExtract: OCF18 SessionLineItemSubmit", 500);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID));
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity));
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST);
                    SessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost));
                    
                    _mainDataSet.SessionLineItemSubmit.AddSessionLineItemSubmitRow(SessionLineItemSubmit);
                }
            }

            
            //GS18LineItemSubmit
            
            var gsList = doc.getList(LineItemType.DE_GS18LineItem_Estimated);

            if (gsList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF18 Submit Session Header", 500);
                for (int i = 0; i < gsList.Count; i++)
                {
                    var GS18LineItemSubmitRow = _mainDataSet.GS18LineItemSubmit.NewGS18LineItemSubmitRow();
                    var tmpItem = (IDataItem)gsList[i];
                    Debuger.WriteToFile("DataExtract: GS18LineItemSubmit ", 500);
                    GS18LineItemSubmitRow.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID = Convert.ToInt32(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID));
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity = Convert.ToDouble(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity));
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST);
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost = Convert.ToDouble(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost));
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count = Convert.ToInt32(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count));
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost = Convert.ToDouble(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost));
                    GS18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ReferenceNumber = Convert.ToInt32(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ReferenceNumber));

                    _mainDataSet.GS18LineItemSubmit.AddGS18LineItemSubmitRow(GS18LineItemSubmitRow);
                }
            }

            //OCF18InjuryLineItemSubmit
            
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF18InjuryLineItemSubmit", 500);
                for (int i = 0; i < injList.Count; i++)
                {
                    var OCF18InjuryLineItemSubmit = _mainDataSet.OCF18InjuryLineItemSubmit.NewOCF18InjuryLineItemSubmitRow();
                    var tmpItem = (IDataItem)injList[i];
                    Debuger.WriteToFile("DataExtract: OCF18InjuryLineItemSubmit ", 500);
                    OCF18InjuryLineItemSubmit.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    OCF18InjuryLineItemSubmit.OCF18_InjuriesAndSequelae_Injury_Code = tmpItem.getValue(edOcf18LisKeys.OCF18_InjuriesAndSequelae_Injury_Code);                    
                    _mainDataSet.OCF18InjuryLineItemSubmit.AddOCF18InjuryLineItemSubmitRow(OCF18InjuryLineItemSubmit);
                }
            }



            //OCF18 Adjuster Response Table
            var OCF18ARrow = _mainDataSet.OCF18AR.NewOCF18ARRow();
            OCF18ARrow.HCAI_Document_Number = doc.getValue(edOcf18Keys.HCAI_Document_Number);

            OCF18ARrow.PMSFields_PMSDocumentKey = doc.getValue(edOcf18Keys.PMSFields_PMSDocumentKey);
            OCF18ARrow.Submission_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.Submission_Date));
            OCF18ARrow.Submission_Source = doc.getValue(edOcf18Keys.Submission_Source);
            OCF18ARrow.Adjuster_Response_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.Adjuster_Response_Date));
            OCF18ARrow.In_Dispute = doc.getValue(edOcf18Keys.In_Dispute);
            OCF18ARrow.Document_Type = doc.getValue(edOcf18Keys.Document_Type);
            OCF18ARrow.Document_Status = doc.getValue(edOcf18Keys.Document_Status);
            OCF18ARrow.Attachments_Received_Date = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.Attachments_Received_Date));
            OCF18ARrow.Claimant_First_Name = doc.getValue(edOcf18Keys.Claimant_First_Name);
            OCF18ARrow.Claimant_Middle_Name = doc.getValue(edOcf18Keys.Claimant_Middle_Name);
            OCF18ARrow.Claimant_Last_Name = doc.getValue(edOcf18Keys.Claimant_Last_Name);
            OCF18ARrow.Claimant_Date_Of_Birth = MyConvert.MyHCAIDateConvert(doc.getValue(edOcf18Keys.Claimant_Date_Of_Birth));
            OCF18ARrow.Claimant_Gender = doc.getValue(edOcf18Keys.Claimant_Gender);
            OCF18ARrow.Claimant_Telephone = doc.getValue(edOcf18Keys.Claimant_Telephone);
            OCF18ARrow.Claimant_Extension = doc.getValue(edOcf18Keys.Claimant_Extension);
            OCF18ARrow.Claimant_Address1 = doc.getValue(edOcf18Keys.Claimant_Address1);
            OCF18ARrow.Claimant_Address2 = doc.getValue(edOcf18Keys.Claimant_Address2);
            OCF18ARrow.Claimant_City = doc.getValue(edOcf18Keys.Claimant_City);
            OCF18ARrow.Claimant_Province = doc.getValue(edOcf18Keys.Claimant_Province);
            OCF18ARrow.Claimant_Postal_Code = doc.getValue(edOcf18Keys.Claimant_Postal_Code);
            OCF18ARrow.Insurer_IBCInsurerID = doc.getValue(edOcf18Keys.Insurer_IBCInsurerID);
            OCF18ARrow.Insurer_IBCBranchID = doc.getValue(edOcf18Keys.Insurer_IBCBranchID);
            OCF18ARrow.Status_Code = doc.getValue(edOcf18Keys.Status_Code);
            OCF18ARrow.Messages = doc.getValue(edOcf18Keys.Messages);
            OCF18ARrow.OCF18_InsurerTotals_AutoInsurerTotal_Approved = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Approved));
            OCF18ARrow.OCF18_InsurerTotals_GST_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_LineCost));
            OCF18ARrow.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            OCF18ARrow.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_PST_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_LineCost));
            OCF18ARrow.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            OCF18ARrow.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost = MyConvert.ConvertStrToFloat(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost));
            OCF18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode);
            OCF18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode);
            OCF18ARrow.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.getValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            OCF18ARrow.OCF18_InsurerTotals_SubTotal_Approved = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_SubTotal_Approved));
            OCF18ARrow.OCF18_InsurerTotals_TotalCount_Approved = MyConvert.ConvertStrToInt(doc.getValue(edOcf18Keys.OCF18_InsurerTotals_TotalCount_Approved));
            OCF18ARrow.OCF18_InsurerSignature_ApplicantSignatureWaived = doc.getValue(edOcf18Keys.OCF18_InsurerSignature_ApplicantSignatureWaived);
            OCF18ARrow.SigningAdjuster_FirstName = doc.getValue(edOcf18Keys.SigningAdjuster_FirstName);
            OCF18ARrow.SigningAdjuster_LastName = doc.getValue(edOcf18Keys.SigningAdjuster_LastName);
            OCF18ARrow.Archival_Status = doc.getValue(edOcf18Keys.Archival_Status);                     

            //Add row to OCF18AR table
            _mainDataSet.OCF18AR.AddOCF18ARRow(OCF18ARrow);
            
            //SessionHeaderLineItemAR
            
            var ARshList = doc.getList(LineItemType.DE_GS18SessionHeaderLineItem_Approved);

            if (ARshList != null)
            {
                Debuger.WriteToFile("DataExtract: OCF18 Submit Session Header", 500);

                for (int i = 0; i < ARshList.Count; i++)
                {
                    var SessionHeaderLineItemARRow = _mainDataSet.SessionHeaderLineItemAR.NewSessionHeaderLineItemARRow();
                    var tmpItem = (IDataItem)ARshList[i];
                    Debuger.WriteToFile("DataExtract: OCF18 Header", 500);

                    SessionHeaderLineItemARRow.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost = MyConvert.ConvertStrToFloat( tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost));
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count));
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost));
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    SessionHeaderLineItemARRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    _mainDataSet.SessionHeaderLineItemAR.AddSessionHeaderLineItemARRow(SessionHeaderLineItemARRow);
                }
            }



            //GS18LineItemAR
            
            var arList = doc.getList(LineItemType.DE_GS18LineItem_Approved);

            if (arList != null)
            {
                Debuger.WriteToFile("DataExtract: GS18LineItemAR", 500);
                for (int i = 0; i < arList.Count; i++)
                {
                    var GS18LineItemAR = _mainDataSet.GS18LineItemAR.NewGS18LineItemARRow();
                    var tmpItem = (IDataItem)arList[i];

                    Debuger.WriteToFile("DataExtract: GS18LineItemAR ", 500);
                    GS18LineItemAR.HCAI_Document_Number = rowSubmited.HCAI_Document_Number;
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST);
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST);
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost));
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count = MyConvert.ConvertStrToInt(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count));
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost = MyConvert.ConvertStrToFloat(tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost));
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    GS18LineItemAR.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = tmpItem.getValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

                    _mainDataSet.GS18LineItemAR.AddGS18LineItemARRow(GS18LineItemAR);
                }
            }


        }

        /// <summary>
        /// Saving data to the database
        /// </summary>
        public void SaveData()
        {
            var tblManager = new TableAdapterManager
                                 {
                                     OCF18SubmitTableAdapter = new OCF18SubmitTableAdapter(),
                                     OCF21BSubmitTableAdapter = new OCF21BSubmitTableAdapter(),
                                     OCF21CSubmitTableAdapter = new OCF21CSubmitTableAdapter(),
                                     OCF23SubmitTableAdapter = new OCF23SubmitTableAdapter(),

                                     OCF18ARTableAdapter = new OCF18ARTableAdapter(),
                                     OCF21BARTableAdapter = new OCF21BARTableAdapter(),
                                     OCF21CARTableAdapter = new OCF21CARTableAdapter(),
                                     OCF23ARTableAdapter = new OCF23ARTableAdapter(),

                                     OCF18InjuryLineItemSubmitTableAdapter = new OCF18InjuryLineItemSubmitTableAdapter(),
                                     OCF21BInjuryLineItemSubmitTableAdapter = new OCF21BInjuryLineItemSubmitTableAdapter(),
                                     OCF21CInjuryLineItemSubmitTableAdapter = new OCF21CInjuryLineItemSubmitTableAdapter(),
                                     OCF23InjuryLineItemSubmitTableAdapter = new OCF23InjuryLineItemSubmitTableAdapter(),
                                     OCF9ARTableAdapter = new OCF9ARTableAdapter(),

                                     GS18LineItemSubmitTableAdapter = new GS18LineItemSubmitTableAdapter(),
                                     SessionHeaderLineItemSubmitTableAdapter = new SessionHeaderLineItemSubmitTableAdapter(),
                                     SessionLineItemSubmitTableAdapter = new SessionLineItemSubmitTableAdapter(),
                                     GS21BLineItemSubmitTableAdapter = new GS21BLineItemSubmitTableAdapter(),
                                     OtherGSLineItemSubmitTableAdapter = new OtherGSLineItemSubmitTableAdapter(),
                                     OtherReimbursableLineItemSubmitTableAdapter = new OtherReimbursableLineItemSubmitTableAdapter(),
                                     PAFLineItemSubmitTableAdapter = new PAFLineItemSubmitTableAdapter(),
                                     PAFReimbursableLineItemSubmitTableAdapter = new PAFReimbursableLineItemSubmitTableAdapter(),
                                     RenderedGSLineItemSubmitTableAdapter = new RenderedGSLineItemSubmitTableAdapter(),

                                     GS18LineItemARTableAdapter = new GS18LineItemARTableAdapter(),
                                     SessionHeaderLineItemARTableAdapter = new SessionHeaderLineItemARTableAdapter(),
                                     GS21BLineItemARTableAdapter = new GS21BLineItemARTableAdapter(),
                                     GS9LineItemARTableAdapter = new GS9LineItemARTableAdapter(),
                                     OtherGSLineItemARTableAdapter = new OtherGSLineItemARTableAdapter(),
                                     OtherReimbursableLineItemARTableAdapter = new OtherReimbursableLineItemARTableAdapter(),
                                     PAFReimbursableLineItemARTableAdapter = new PAFReimbursableLineItemARTableAdapter(),

                                     FacilityLineItemTableAdapter = new FacilityLineItemTableAdapter(),
                                     BranchLineItemTableAdapter = new BranchLineItemTableAdapter(),
                                     InsurerLineItemTableAdapter = new InsurerLineItemTableAdapter(),
                                     ProviderLineItemTableAdapter = new ProviderLineItemTableAdapter(),
                                     RegistrationLineItemTableAdapter = new RegistrationLineItemTableAdapter(),
                                     
                                     
                                 };
            tblManager.UpdateAll(_mainDataSet);
        }

        public bool ExtractFacilityInfo()
        {
            IList providerList;
            IList providerRegList;
            IDataItem fcl;
            IDataItem provider;
            IDataItem provReg;

            var providerKeys = new PMSToolkit.DataObjects.ProviderLineItemKeys();
            var regKeys = new PMSToolkit.DataObjects.RegistrationLineItemKeys();
            var facilityKeys = new PMSToolkit.DataObjects.FacilityKeys();

            IDataItem facility = _pmsTk.facilityInfo(_config.Login, _config.Pass);

            if (facility == null)
            {
                var e = new ErrorMessage();
                PopulateErrorMessages(e);

                Debuger.WriteToFile("HCAI returned empty facility list. " + e.Error_ErrorList, 50);
                return false;
            }

            IList facList = facility.getList(LineItemType.FacilityLineItem);
            if (facList == null)
            {
                var e = new ErrorMessage();
                PopulateErrorMessages(e);
                return false;
            }

            if (facList.Count == 0)
            {
                facList = facility.getList(LineItemType.FacilityLineItem);
            }

            if (facList == null)
            {
                var e = new ErrorMessage();
                PopulateErrorMessages(e);
                return false;
            }

            var facilities = _mainDataSet.FacilityLineItem;
            
            for (int k = 0; k < facList.Count; k++)
            {
                var f = facilities.NewFacilityLineItemRow();
                fcl = (IDataItem)facList[k];
                f.HCAI_Facility_Registry_ID = Convert.ToInt32(fcl.getValue(facilityKeys.HCAI_Facility_Registry_ID));
                f.Facility_Name = fcl.getValue(facilityKeys.Facility_Name);
                f.Address_Line_1 = fcl.getValue(facilityKeys.Address_Line_1);
                f.Address_Line_2 = fcl.getValue(facilityKeys.Address_Line_2);
                f.City = fcl.getValue(facilityKeys.City);
                f.Province = fcl.getValue(facilityKeys.Province);
                f.Postal_Code = fcl.getValue(facilityKeys.Postal_Code);
                f.AISI_Facility_Number = fcl.getValue(facilityKeys.AISI_Facility_Number);
                f.Telephone_Number = fcl.getValue(facilityKeys.Telephone_Number);
                //VERSION_2.0_beta
                f.Fax_Number = fcl.getValue(facilityKeys.Fax_Number);
                //end
                f.Authorizing_Officer_First_Name = fcl.getValue(facilityKeys.Authorizing_Officer_First_Name);
                f.Authorizing_Officer_Last_Name = fcl.getValue(facilityKeys.Authorizing_Officer_Last_Name);
                f.Authorizing_Officer_Title = fcl.getValue(facilityKeys.Authorizing_Officer_Title);
                f.Lock_Payable_Flag = fcl.getValue(facilityKeys.Lock_Payable_Flag);

                facilities.AddFacilityLineItemRow(f);

                //Providers
                providerList = fcl.getList(LineItemType.ProviderLineItem);
                if (providerList == null)
                {
                    var e = new ErrorMessage();
                    PopulateErrorMessages(e);
                    return false;
                }

                var providers = _mainDataSet.ProviderLineItem;

                for (int i = 0; i < providerList.Count; i++)
                {
                    var pr = providers.NewProviderLineItemRow();
                    provider = (IDataItem)providerList[i];
                    pr.Provider_First_Name = provider.getValue(providerKeys.Provider_First_Name);
                    pr.Provider_Last_Name = provider.getValue(providerKeys.Provider_Last_Name);
                    pr.HCAI_Provider_Registry_ID = Convert.ToInt32(provider.getValue(providerKeys.HCAI_Provider_Registry_ID));

                    //VERSION_2.0_beta
                    pr.Provider_Status = provider.getValue(providerKeys.Provider_Status);
                    pr.Provider_Start_Date = MyConvert.MyHCAIDateConvert(provider.getValue(providerKeys.Provider_Start_Date));
                    pr.Provider_End_Date = MyConvert.MyHCAIDateConvert(provider.getValue(providerKeys.Provider_End_Date));
                    //end

                    providers.AddProviderLineItemRow(pr);

                    providerRegList = provider.getList(PMSToolkit.LineItemType.RegistrationLineItem);
                    if (providerRegList == null)
                    {
                        var e = new ErrorMessage();
                        PopulateErrorMessages(e);
                        return false;
                    }

                    var providerRegistration = _mainDataSet.RegistrationLineItem;
                    for (int j = 0; j < providerRegList.Count; j++)
                    {
                        var prr = providerRegistration.NewRegistrationLineItemRow();
                        provReg = (IDataItem)providerRegList[j];
                        prr.Profession = provReg.getValue(regKeys.Profession);
                        prr.Registration_Number = provReg.getValue(regKeys.Registration_Number);

                        providerRegistration.AddRegistrationLineItemRow(prr);
                    }
                }
            }

            return true;
        }

        public bool ExtractInsurersInfo()
        {
            
            IList insList;
            IList branchList;
            IDataItem branch;
            IDataItem insurer;
            var insKeys = new PMSToolkit.DataObjects.InsurerLineItemKeys();
            var branchKey = new PMSToolkit.DataObjects.BranchLineItemKeys();

            var insurers = _pmsTk.insurerList(_config.Login, _config.Pass);

            if (insurers == null)
            {
                var e = new ErrorMessage();
                PopulateErrorMessages(e);

                Debuger.WriteToFile("HCAI returned empty insurer list. " + e.Error_ErrorList, 50);
                return false;
            }

            insList = insurers.getList(LineItemType.InsurerLineItem);
            if (insList == null)
            {
                var e = new ErrorMessage();
                PopulateErrorMessages(e);

                Debuger.WriteToFile("Insurer list is empty. " + e.Error_ErrorList, 50);
                return false;
            }
            else
            {
                var insurerTable = _mainDataSet.InsurerLineItem;

                for (int i = 0; i < insList.Count; i++)
                {
                    var ins = insurerTable.NewInsurerLineItemRow();
                    insurer = (IDataItem)insList[i];
                    ins.Insurer_ID = insurer.getValue(insKeys.Insurer_ID);
                    ins.Insurer_Name = insurer.getValue(insKeys.Insurer_Name);
                    ins.Insurer_Status = insurer.getValue(insKeys.Insurer_Status);

                    insurerTable.AddInsurerLineItemRow(ins);


                    branchList = insurer.getList(LineItemType.BranchLineItem);
                    if (branchList == null)
                    {
                        var e = new ErrorMessage();
                        PopulateErrorMessages(e);
                        return false;
                    }

                    var branches = _mainDataSet.BranchLineItem;
                    for (int j = 0; j < branchList.Count; j++)
                    {
                        var ib = branches.NewBranchLineItemRow();
                        branch = (IDataItem)branchList[j];
                        ib.Branch_ID = branch.getValue(branchKey.Branch_ID);
                        ib.Branch_Name = branch.getValue(branchKey.Branch_Name);
                        ib.Branch_Status = branch.getValue(branchKey.Branch_Status);

                        branches.AddBranchLineItemRow(ib);
                    }
                }
            }
            return true;
        }

        public IEnumerable<Activity> GetActivityList(DateTime startDate, DateTime endDate)
        {
            try
            {
                var res = new List<Activity>();

                var e = new ErrorMessage
                            {
                                MessageType = MsgType.Error,
                                Error_ErrorType = MsgType.ActivityListRecieved
                            };

                var actLst = _pmsTk.activityList(_config.Login, _config.Pass, startDate, endDate);

                var dt = DateTime.Now;
                
                IDataItem item;
                if (actLst == null)
                {
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");

                    PopulateErrorMessages(e);
                    Debuger.WriteToFile("Errors during getting the activity list:\n" + e.Error_ErrorList, 500);
                    return null;
                }
                //					System.Console.WriteLine(actLst.isValidLineItemType(PMSToolkit.LineItemType.ActivityLineItem));

                var lst = actLst.getList(LineItemType.ActivityLineItem);

                if (lst == null)
                {
                    e.MessageDate = dt.ToString("MM/dd/yyyy");
                    e.MessageTime = dt.ToString("hh:mm:ss tt");

                    PopulateErrorMessages(e);
                    Debuger.WriteToFile("Errors during getting the activity list line item:\n" + e.Error_ErrorList, 500);
                    return null;
                }

                var actKeys = new PMSToolkit.DataObjects.ActivityLineItemKeys();
                //Activity act;

                for (var i = 0; i < lst.Count; i++ )
                {
                    item = (IDataItem)lst[i];
                    var act = new Activity
                              {
                                  HCAI_Document_Number = item.getValue(actKeys.HCAI_Document_Number),
                                  Document_Status = item.getValue(actKeys.Document_Status),
                                  PMS_Document_Key = item.getValue(actKeys.PMSFields_PMSDocumentKey),
                                  PMS_Patient_Key = item.getValue(actKeys.PMSFields_PMSPatientKey),
                                  Submission_Date = item.getValue(actKeys.Submission_Date),
                                  Submission_Source = item.getValue(actKeys.Submission_Source)
                              };
                    res.Add(act);
                    //Console.WriteLine(act.HCAI_Document_Number + ": " + act.Submission_Source + ", " + act.Document_Status);
                }

                return res;
            }
            catch (Exception ex)
            {
                UOServiceLib.Log.Error("Activity List error: " + ex.Message, ex);
                //WRITE ERROR LOG
                var e = new ErrorMessage {MessageType = MsgType.Error};
                DateTime dt = DateTime.Now;
                e.MessageDate = dt.ToString("MM/dd/yyyy");
                e.MessageTime = dt.ToString("hh:mm:ss tt");

                e.Error_ErrorType = MsgType.RequestActivityList;
                e.Error_HCAI_ErrorType = "903:?:Exception occured!";

                //WRITE TO ERROR LOG
                Debuger.WriteToFile("Activity list exception:\n" + e.Error_ErrorList, 500);
                return null;
            }
        }

        //private static DateTime ConvertHcaiStrToDate(string date)
        //{
        //    const string formatLong = "yyyyMMddHHmmss.ffzzz";
        //    const string formatShort = "yyyy-MM-dd";
        //    DateTime res;
        //    if (!DateTime.TryParseExact(date, formatLong, CultureInfo.InvariantCulture, DateTimeStyles.None, out res) &&
        //        !DateTime.TryParseExact(date, formatShort, CultureInfo.InvariantCulture, DateTimeStyles.None, out res))
        //    {
        //        Debuger.WriteToFile("Failed to convert date: " + date, 100);
        //        res = new DateTime(1990, 1, 1);
        //    }
        //    return res;
        //}

        private void PopulateErrorMessages(ErrorMessage e)
        {
            e.Error_CompleteErrorDesc = "";
            var err = _pmsTk.getError();

            string tmp = err.getErrorType() + ":?:" + err.getErrorDescription();
            e.Error_HCAI_ErrorType = tmp;

            //if (err.getErrorType() == "109")
            //    e.Error_CompleteErrorDesc = "Authentication Error: Check login and password\r\n";

            IList errList = err.getErrorList();
            PMSToolkit.ToolkitError.ErrorDescription li;

            //Common Errors
            foreach (object t in errList)
            {
                li = (PMSToolkit.ToolkitError.ErrorDescription)t;
                tmp = KeyFormat(li.getKey());
                tmp = tmp + "; " + li.getDescription();
                e.Error_ErrorList = tmp;
                e.Error_CompleteErrorDesc += tmp + "\r\n";
            }
        }

        private static string KeyFormat(string s)
        {
            return s.Replace("/", " ");
        }
    }
}