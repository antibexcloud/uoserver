﻿using System;

namespace UOServerLib
{
    public class UOMessage
    {
        public UOMessage(string cmd)
        {
            Command = cmd;
            TimeStamp = DateTime.Now;
        }

        public DateTime TimeStamp;
        public string Command;
        public object[] Params;
    }
}