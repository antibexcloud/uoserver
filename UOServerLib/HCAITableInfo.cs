﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Threading;
using System.Globalization;

namespace UOServerLib
{
    public enum HcaiStatus
    {
        Draft = 0,
        Created,
        HcaiCompliant,
        Submitted,
        DeliveryFailed,
        InformationError,
        SuccessfullyDelivered,
        WithdrawRequested,
        WithdrawRequestFailed,
        WithdrawGranted,
        WithdrawDenied,
        Pending,
        NotApproved,
        PartiallyApproved,
        Approved,
        DeemedApproved,
        NumberOfStatuses
    };

    public enum HcaiGroup
    {
        None = 0,
        DocumentsForReview,
        ReadyToSubmit,
        SubmissionsInProgress,
        SuccessfullyDelivered,
        Pending,
        AdjusterResponse,
        NumberOfGroups
    };


    public class CompareByInvoiceNumber : IComparer
    {
        #region IComparer Members

        int IComparer.Compare(object x, object y)
        {
            var x1 = (HCAITableLineItem)x;
            var y1 = (HCAITableLineItem)y;

            //compare the types
            var res = x1.OCFType.CompareTo(y1.OCFType);

            //if types are the same compare the ID
            if (res == 0)
            {
                res = x1.OCFID.CompareTo(y1.OCFID);
            }

            return res;
        }

        #endregion
    }

    public class CompareByDate : IComparer
    {
        #region IComparer Members

        int IComparer.Compare(object x, object y)
        {
            var x1 = (HCAITableLineItem)x;
            var y1 = (HCAITableLineItem)y;

            return y1.CombinedDataTime.CompareTo(x1.CombinedDataTime);
            //return x1.CombinedDataTime.CompareTo(y1.CombinedDataTime);
        }
        #endregion
    }

    public class HCAITableInfo
    {
        public static string DateFormat = "MM/dd/yyyy";
        public static string TimeFormat = "hh:mm tt";

        public readonly Mutex MLocalMutex = new Mutex();
        private ArrayList[] _mHcaiMap;
        private ArrayList[] _mGroupMap;

        private readonly CompareByInvoiceNumber _mInvoiceCompare = new CompareByInvoiceNumber();
        private readonly CompareByDate _mDateCompare = new CompareByDate();

        #region HCAITableInfoInterface Members

        public string ServerName { get; set; }
        public string DBName { get; set; }
        public int ServerVersion { get; set; }
        public int RequestedGroup { get; set; }

        #endregion

        public HCAITableInfo()
        {
            Init();
        }

        public int GetTotalCount()
        {
            int c = 0;
            for (int i = 0; i < (int)HcaiStatus.NumberOfStatuses; i++)
            {
                c += _mHcaiMap[i].Count;
            }

            return c;
        }

        public HCAITableInfo(XmlDocument doc)
        {
            Init();
            ParseXml(doc);
        }

        private void Init()
        {
            _mHcaiMap = new ArrayList[(int)HcaiStatus.NumberOfStatuses];
            for (int i = 0; i < (int)HcaiStatus.NumberOfStatuses; i++)
            {
                _mHcaiMap[i] = new ArrayList();
            }

            _mGroupMap = new ArrayList[(int)HcaiGroup.NumberOfGroups];
            for (int i = 0; i < (int)HcaiGroup.NumberOfGroups; i++)
            {
                _mGroupMap[i] = new ArrayList();
            }
        }

        private static int GetStatusGroup(HcaiStatus status)
        {
            int res;
            if (status < 0 || status >= HcaiStatus.NumberOfStatuses)
            {
                return -1;
            }

            switch (status)
            {
                case HcaiStatus.Created:
                    res = (int)HcaiGroup.None;
                    break;
                case HcaiStatus.Draft:
                case HcaiStatus.InformationError:
                case HcaiStatus.WithdrawGranted:
                    res = (int)HcaiGroup.DocumentsForReview;
                    break;
                case HcaiStatus.HcaiCompliant:
                case HcaiStatus.DeliveryFailed:
                    res = (int)HcaiGroup.ReadyToSubmit;
                    break;
                case HcaiStatus.Submitted:
                case HcaiStatus.WithdrawRequested:
                    res = (int)HcaiGroup.SubmissionsInProgress;
                    break;
                case HcaiStatus.SuccessfullyDelivered:
                case HcaiStatus.WithdrawRequestFailed:
                case HcaiStatus.WithdrawDenied:
                    res = (int)HcaiGroup.SuccessfullyDelivered;
                    break;
                case HcaiStatus.Pending:
                    res = (int)HcaiGroup.Pending;
                    break;
                case HcaiStatus.NotApproved:
                case HcaiStatus.PartiallyApproved:
                case HcaiStatus.Approved:
                case HcaiStatus.DeemedApproved:
                    res = (int)HcaiGroup.AdjusterResponse;
                    break;
                default:
                    res = -1;
                    break;
            }

            return res;
        }

        public bool AddLineItemToMap(HCAITableLineItem li)
        {
            var result = true;

            if (FindLineItem(li.OCFID, li.OCFType) != null)
            {
                //item exists, do nothing
                result = false;
            }
            else if (li.OCFStatus >= 0 && li.OCFStatus < (int)HcaiStatus.NumberOfStatuses)
            {
                //adding item to the map according to the status
                //mHcaiMap[li.OCFStatus].Add(li);

                //Adding item in a sorted array according to the status
                var pos = _mHcaiMap[li.OCFStatus].BinarySearch(li, _mInvoiceCompare);
                if (pos < 0)
                    pos = ~pos;

                _mHcaiMap[li.OCFStatus].Insert(pos, li);

                //also adding item to the appropriate group, groups are sorted by date
                var group = GetStatusGroup((HcaiStatus)li.OCFStatus);
                pos = _mGroupMap[group].BinarySearch(li, _mDateCompare);
                if (pos < 0)
                    pos = ~pos;

                _mGroupMap[group].Insert(pos, li);
            }
            else
            {
                result = false;
            }

            return result;
        }

        public int RemoveItemFromMap(string ocfId, string ocfType)
        {
            try
            {
                return RemoveItemFromMap(MyConvert.ConvertStrToLong(ocfId), ocfType);
            }
            catch(Exception e)
            {
                UOServiceLib.Log.ErrorFormat("Failed to remove item {0}<{1}> from a map.", ocfId, ocfType);
                UOServiceLib.Log.Error(e.Message, e);
                return -1;
            }
        }

        public List<HCAITableLineItem> GetAllItemsInGroup(HcaiGroup group)
        {
            return _mGroupMap[(int) @group].Cast<HCAITableLineItem>().ToList();
        }

        private int RemoveItemFromMap(long ocfId, string ocfType)
        {
            var status = -1;
            var li = FindLineItem(ocfId, ocfType);

            if (li != null)
            {
                _mHcaiMap[li.OCFStatus].Remove(li);

                var gr = GetStatusGroup((HcaiStatus)li.OCFStatus);
                _mGroupMap[gr].Remove(li);

                status = li.OCFStatus;
            }

            return status;
        }

        public int GetNumberOfStatuses()
        {
            return _mHcaiMap.Length;
        }

        public int GetNumberOfRowsInStatus(int status)
        {
            if (status < 0 || status > (int)HcaiStatus.NumberOfStatuses - 1)
                return -1;
            return _mHcaiMap[status].Count;
        }

        public int GetNumberOfRowsInGroup(int group)
        {
            if (group < 0 || group > (int)HcaiGroup.NumberOfGroups - 1)
                return -1;
            return _mGroupMap[group].Count;
        }

        public HCAITableLineItem GetLineItemByStatus(int status, int pos)
        {
            if (status < 0 || status > (int)HcaiStatus.NumberOfStatuses - 1 || pos < 0 || pos >= _mHcaiMap[status].Count)
                return null;
            return (HCAITableLineItem)_mHcaiMap[status][pos];
        }

        public HCAITableLineItem GetLineItemByGroup(int group, int pos)
        {
            if (group < 0 || group > (int)HcaiGroup.NumberOfGroups - 1 || pos < 0 || pos >= _mGroupMap[group].Count)
                return null;
            return (HCAITableLineItem)_mGroupMap[group][pos];
        }

        public HCAITableLineItem FindLineItem(string ocfId, string ocfType)
        {
            try
            {
                return FindLineItem(Convert.ToInt64(ocfId), ocfType);
            }
            catch
            {
                return null;
            }
        }

        private HCAITableLineItem FindLineItem(long ocfId, string ocfType)
        {
            var tmp = new HCAITableLineItem {OCFID = ocfId, OCFType = ocfType};
            foreach (var ar in _mHcaiMap)
            {
                var res = ar.BinarySearch(tmp, _mInvoiceCompare);
                if (res >= 0)
                {
                    //found it return it
                    return (HCAITableLineItem)ar[res];
                }
            }

            //return null if found nothing
            return null;
        }

        public void ClearAll()
        {
            for (var i = 0; i < (int)HcaiStatus.NumberOfStatuses; i++)
            {
                _mHcaiMap[i].Clear();
            }
            for (var i = 0; i < (int)HcaiGroup.NumberOfGroups; i++)
            {
                _mGroupMap[i].Clear();
            }
        }

        public bool ClearGroup(int group)
        {
            if (group < 0 || group > (int)HcaiGroup.NumberOfGroups - 1)
                return false;

            _mGroupMap[group].Clear();
            for (var i = 0; i < (int)HcaiStatus.NumberOfStatuses; i++)
            {
                if (group == GetStatusGroup((HcaiStatus)i))
                    _mHcaiMap[i].Clear();
            }

            return true;
        }

        /// <summary>
        /// Changes the status of the document and moves the document to the appropriate status and group list
        /// </summary>
        /// <param name="ocfId">OCFID</param>
        /// <param name="ocfType">OCFType</param>
        /// <param name="newStatus">New Status</param>
        /// <param name="minSentDate"> </param>
        /// <param name="it">if we know the item we need to change we can provide it here, if it's null 
        /// we'll look for it using ocfID and ocfType</param>
        /// <param name="total"> </param>
        /// <param name="statusdate"> </param>
        /// <param name="statustime"> </param>
        /// <returns>Old status of the document of -1 if document was not found</returns>
        private int UpdateDocumentStatus(long ocfId, string ocfType, HcaiStatus newStatus, float total, string statusdate, string statustime, string minSentDate, HCAITableLineItem it, string proposedAmount, string hcaiNumber)
        {
            var dt = DateTime.Now;
            var res = it ?? FindLineItem(ocfId, ocfType);
            var date = string.IsNullOrEmpty(statusdate) ? $"{dt:MM}/{dt:dd}/{dt:yyyy}" : statusdate;
            var time = string.IsNullOrEmpty(statustime) ? dt.ToString(TimeFormat) : statustime;
            var r = -1;
            if (res == null) return r;

            if (!string.IsNullOrEmpty(proposedAmount))
            {
                res.Total = MyConvert.ConvertStrToFloat(proposedAmount);
            }

            //we check if the document status is the same, do nothing
            if (res.OCFStatus == (int)newStatus)
            {
                r = res.OCFStatus;
            }
            else
            {
                res.IsYellow = false;
                if ((newStatus == HcaiStatus.Approved ||
                     newStatus == HcaiStatus.NotApproved ||
                     newStatus == HcaiStatus.PartiallyApproved) &&
                    (HcaiStatus)res.OCFStatus == HcaiStatus.DeemedApproved)
                {
                    res.IsYellow = true;
                }
                //move item to the new status
                var gr = GetStatusGroup((HcaiStatus)res.OCFStatus);
                r = res.OCFStatus;
                _mHcaiMap[r].Remove(res);
                _mGroupMap[gr].Remove(res);
                res.OCFStatus = (int)newStatus;
                res.StatusDate = date;
                res.StatusTime = time;
                res.MinSentDate = minSentDate;
                if (!string.IsNullOrEmpty(hcaiNumber.Trim()))
                    res.HCAINumber = hcaiNumber;

                if (total >= 0)
                    res.ATotal = total;

                AddLineItemToMap(res);
            }

            return r;
        }


        public int UpdateDocumentStatus(string ocfId, string ocfType, HcaiStatus newStatus, float total, string statusdate, string statustime, string minSentDate)
        {
            return UpdateDocumentStatus( MyConvert.ConvertStrToLong(ocfId), ocfType, newStatus, total, statusdate, statustime, minSentDate, null, "", "");
        }
       
        public int UpdateDocumentStatus(OCFMapReturnType newSt)
        {
            return UpdateDocumentStatus(newSt.OCFID, newSt.OCFType, newSt.New_HCAI_Status, newSt.ATotal, newSt.StatusDate, newSt.StatusTime, newSt.MinSentDate, null, "", "");
        }
        
        public int UpdateDocumentStatus(long ocfId, string ocfType, HcaiStatus newStatus, string minSentDate)
        {
            return UpdateDocumentStatus(ocfId, ocfType, newStatus, -1, "", "", minSentDate, null, "", "");
        }

        public int UpdateDocumentStatus(string ocfId, string ocfType, HcaiStatus newStatus, string minSentDate, string hcaiNumber = "")
        {
            try
            {
                return UpdateDocumentStatus(Convert.ToInt64(ocfId), ocfType, newStatus, -1, "", "", minSentDate, null, "", hcaiNumber);
            }
            catch(Exception e)
            {
                var s = $"Failed to update status. Id = {ocfId}, Type: {ocfType}, newStatus = {newStatus}, date = {minSentDate}";
                Debugger.LogError(s, e);
                return -1;
            }
        }

        public int UpdateDocumentStatus(HCAITableLineItem it, HcaiStatus newStatus)
        {
            return UpdateDocumentStatus(0, "", newStatus, -1, "", "", "", it, "", "");
        }

        private void ParseXml(XmlDocument doc)
        {
            var el = (XmlElement) doc.DocumentElement.FirstChild;

            ServerVersion = Convert.ToInt32(doc.DocumentElement.GetAttribute("ServerVersion"));
            ServerName = doc.DocumentElement.GetAttribute("ServerName");
            DBName = doc.DocumentElement.GetAttribute("DBName");
            RequestedGroup = Convert.ToInt32(doc.DocumentElement.GetAttribute("RequestedGroup"));

            while (el != null)
            {
                var tmp = new HCAITableLineItem();
                tmp.PopulateLineItem(el);

                //add item to the map
                AddLineItemToMap(tmp);

                //going through all line items
                el = (XmlElement)el.NextSibling;
            }
        }

        public XmlDocument CreateXmlDocumentForGroup(int group)
        {
            if (group < 0 || group >= (int)HcaiGroup.NumberOfGroups)
                return null;

            //creating and saving custom XML file
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            el.SetAttribute("RequestedGroup", group.ToString(CultureInfo.InvariantCulture));
            el.SetAttribute("ServerName", ServerName);
            el.SetAttribute("DBName", DBName);
            el.SetAttribute("ServerVersion", ServerVersion.ToString(CultureInfo.InvariantCulture));

            doc.AppendChild(el);

            //add every LineItem to XML Doc
            foreach (var ob in _mGroupMap[group])
            {
                var tmp = (HCAITableLineItem)ob;
                tmp.AddItemToXmlDocument(doc);
            }

            return doc;
        }

        public XmlDocument CreateXmlDocument()
        {
            //creating and saving custom XML file
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);
            el.SetAttribute("RequestedGroup", "-1");
            el.SetAttribute("ServerName", ServerName);
            el.SetAttribute("DBName", DBName);
            el.SetAttribute("ServerVersion", ServerVersion.ToString(CultureInfo.InvariantCulture));


            //going throught every list of the map
            for (var i = 0; i < (int)HcaiStatus.NumberOfStatuses; i++)
            {
                //add every LineItem to XML Doc
                foreach (var ob in _mHcaiMap[i])
                {
                    var tmp = (HCAITableLineItem)ob;
                    tmp.AddItemToXmlDocument(doc);
                }
            }
            return doc;
        }
    }



    public class HCAITableLineItem
    {
        public HCAITableLineItem()
        {
            OCFType = "";
            OCFID = 0;
            last_name = "";
            first_name = "";
            CaseID = "";
            MVAAFirstName = "";
            MVAALastName = "";
            MVAInsName = "";
            InvoiceVersion = "";
            CategoryName = "";
            MinSentDate = "";
            MinSentDate = "";
            HCAINumber = "";
            EHC1CompanyName = "";
            EHC2CompanyName = "";
        }

        private CultureInfo enUS = new CultureInfo("en-US");
        private string mStatusDate;
        private string mStatusTime;

        public string OCFType { get; set; }
        public long OCFID { get; set; }
        public int OCFStatus { get; set; }

        public string StatusDate
        {
            get => mStatusDate;
            set
            {
                mStatusDate = value;
                if (!string.IsNullOrEmpty(StatusTime))
                {
                    SetDateTime(value, StatusTime);
                }
            }
        }

        public string StatusTime
        {
            get => mStatusTime;
            set
            {
                mStatusTime = value;
                if (!string.IsNullOrEmpty(StatusDate))
                {
                    SetDateTime(StatusDate, value);
                }
            }
        }

        public int client_id { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string CaseID { get; set; }
        public string MVAAFirstName { get; set; }
        public string MVAALastName { get; set; }
        public string MVAInsName { get; set; }
        public float Total { get; set; }
        public string InvoiceVersion { get; set; }
        public float ATotal { get; set; }
        public int PlanNo { get; set; }
        public int NumberOfDays { get; set; }
        public bool IsYellow { get; set; }
        public string CategoryName { get; set; }
        public string MinSentDate { get; set; }
        public string HCAINumber { get; set; }
        public int LockStatus { get; set; }
        public string EHC1CompanyName { get; set; }
        public string EHC2CompanyName { get; set; }

        private DateTime mDateTime;
        public DateTime CombinedDataTime => mDateTime;


        private bool ConvertStringToInt(string s, out int i)
        {
            var res = true;
            try
            {
                i = Convert.ToInt32(s);
            }
            catch (Exception ex)
            {
                Debugger.LogDebug($"Failed to convert string '{s}' to int");
                res = false;
                i = 0;
            }
            return res;
        }

        private DateTime SetDateTime(string date, string time)
        {
            var now = DateTime.Now;
            if (string.IsNullOrEmpty(date))
            {
                Debugger.WriteToFile("SetDateTime: date string is null or empty. Using current date.", 100);
                date = $"{now:MM}/{now:dd}/{now:yyyy}";
            }
            if (string.IsNullOrEmpty(time))
            {
                Debugger.WriteToFile("SetDateTime: time string is null or empty. Using current date.", 100);
                time = DateTime.Now.ToString(HCAITableInfo.TimeFormat);
            }

            if (date.Contains(":"))    //Eugene-newAdd
            {
                Debugger.WriteToFile("Something is wrong with date format: " + date, 100);
                date = date.Substring(0, date.IndexOf(' '));
                Debugger.WriteToFile("Attempting to fix date format. Fixed result: " + date, 100);
            }

            if (time.Contains("/")) //Eugene-newAdd
            {
                Debugger.WriteToFile("Something is wrong with time format: " + time, 100);
                var t = time.IndexOf(' ');
                if (t >= 0 && t < time.Length)
                {
                    time = time.Substring(t);
                    Debugger.WriteToFile("Attempting to fix time format. Fixed result: " + time, 100);
                }
                else
                {
                    Debugger.WriteToFile("Cannot apply fix. ' ' is not found.", 10);
                }
            }

            int day, month, year;
            int hour, minute;

            var dt = DateTime.Now;
            //dealing with date
            var dateParts = date.Split(new[] { '/' });
            if (dateParts.Length < 3)
            {
                Debugger.WriteToFile($"Not a valid date format: '{date}'. Using current date.", 100);
                year = dt.Year;
                month = dt.Month;
                day = dt.Day;
            }
            else
            {
                if (!ConvertStringToInt(dateParts[0], out var tmp))
                {
                    tmp = dt.Month;
                }
                if (tmp > 12)
                {
                    day = tmp;
                    if (!ConvertStringToInt(dateParts[1], out tmp))
                    {
                        tmp = dt.Month;
                    }
                    month = tmp;
                }
                else
                {
                    month = tmp;
                    if (!ConvertStringToInt(dateParts[1], out tmp))
                    {
                        tmp = dt.Day;
                    }
                    day = tmp;
                }
                if (!ConvertStringToInt(dateParts[2], out tmp))
                {
                    tmp = dt.Year;
                }
                year = tmp;
            }

            //dealing with time
            var timeParts = time.Split(new[] { ':', ' ' });
            if (dateParts.Length < 2)
            {
                Debugger.WriteToFile($"Not a valid time format: '{date}'. Using current time.", 100);
                hour = dt.Hour;
                minute = dt.Minute;
            }
            else
            {
                if (!ConvertStringToInt(timeParts[0], out var tmp))
                {
                    tmp = dt.Hour;
                }
                hour = tmp;

                if (!ConvertStringToInt(timeParts[1], out tmp))
                {
                    tmp = dt.Minute;
                }
                minute = tmp;


                if ((timeParts.Length >= 3 && timeParts[2].ToUpper() == "AM") ||
                    (timeParts.Length >= 4 && timeParts[3].ToUpper() == "AM") ||
                    (timeParts.Length >= 3 && timeParts[2].ToUpper() == "PM") ||
                    (timeParts.Length >= 4 && timeParts[3].ToUpper() == "PM"))
                {
                    hour = hour % 12;
                }

                if ((timeParts.Length >= 3 && timeParts[2].ToUpper() == "PM") ||
                    (timeParts.Length >= 4 && timeParts[3].ToUpper() == "PM"))
                {
                    hour += 12;
                }
            }

            //sanity checks
            if (day < 1 || day > 31 ||
                month < 1 || month > 12)
            {
                Debugger.WriteToFile($"Day or Month are outside of the valid range. day = {day}; month = {month}, Setting day and month to current date.", 100);
                day = dt.Day;
                month = dt.Month;
            }
            if (year < 1 || year > 9999)
            {
                Debugger.WriteToFile($"Year is outside of the valid range. year = {year}, Setting day to current year.", 100);
                year = dt.Year;
            }
            if (hour < 0 || hour > 23)
            {
                Debugger.WriteToFile($"Hour is outside of the valid range. hour = {hour}, Setting day to current hour.", 100);
                hour = dt.Hour;
            }
            if (minute < 0 || minute > 59)
            {
                Debugger.WriteToFile($"Minute is outside of the valid range. minute = {minute}, Setting day to current minute.", 100);
                minute = dt.Minute;
            }
            return new DateTime(year, month, day, hour, minute, 0);
        }


        public void SetDateTimeOld(string date, string time)
        {
            var now = DateTime.Now;
            try
            {
                if(string.IsNullOrEmpty(date))
                {
                    Debugger.WriteToFile("SetDateTime: date string is null or empty. Using current date.", 100);
                    date = $"{now:MM}/{now:dd}/{now:yyyy}";
                }
                if(string.IsNullOrEmpty(time))
                {
                    Debugger.WriteToFile("SetDateTime: time string is null or empty. Using current date.", 100);
                    time = DateTime.Now.ToString(HCAITableInfo.TimeFormat);
                }

                if(date.Contains(":"))    //Eugene-newAdd
                {
                    Debugger.WriteToFile("Something is wrong with date format: " + date, 100);
                    date = date.Substring(0, date.IndexOf(' '));
                    Debugger.WriteToFile("Attempting to fix date format. Fixed result: " + date, 100);
                }

                if (time.Contains("/")) //Eugene-newAdd
                {
                    Debugger.WriteToFile("Something is wrong with time format: " + time, 100);
                    int t = time.IndexOf(' ');
                    if (t >= 0 && t < time.Length)
                    {
                        time = time.Substring(t);
                        Debugger.WriteToFile("Attempting to fix time format. Fixed result: " + time, 100);
                    }
                    else
                    {
                        Debugger.WriteToFile("Cannot apply fix. ' ' is not found.", 10);
                    }
                }

                var count = time.Count(c => c == ':');
                if(count == 2)
                {
                    Debugger.WriteToFile("There's still something wrong with time format: " + time, 100);
                    var tmp = time.Split(new[]{":", " "}, StringSplitOptions.RemoveEmptyEntries);
                    if(tmp.Count() < 3)
                    {
                        time = DateTime.Now.ToString(HCAITableInfo.TimeFormat);
                        Debugger.WriteToFile("Time cannot be fixed using current time: " + time, 10);
                    }
                    else
                    {
                        time = tmp[0] + ":" + tmp[1] + " " + tmp[tmp.Length - 1];
                        Debugger.WriteToFile("Attempting to fix time format. Fixed result: " + time, 100);
                    }
                }


                //                t = StatusDate.Substring(0, StatusDate.IndexOf(' '));
                //                t += StatusTime.Substring(StatusTime.IndexOf(' '));
                //                mDateTime = DateTime.ParseExact(t.ToLower(), HCAITableInfo.DateFormat + " h:mm:ss tt", enUS, DateTimeStyles.None);
                //mDateTime = DateTime.ParseExact(StatusDate + " " + StatusTime, HCAITableInfo.DateFormat + " " + HCAITableInfo.TimeFormat, enUS, DateTimeStyles.None);
                mDateTime = DateTime.ParseExact(date + " " + time, HCAITableInfo.DateFormat + " " + HCAITableInfo.TimeFormat, enUS, DateTimeStyles.None);
                mStatusDate = $"{mDateTime:MM}/{mDateTime:dd}/{mDateTime:yyyy}";
                mStatusTime = mDateTime.ToString(HCAITableInfo.TimeFormat);
                //mDateTime = DateTime.Parse(t, enUS);
            }
            catch (Exception e)
            {
                //format error, use current time
                UOServiceLib.Log.ErrorFormat("Failed to parse: date = {0}, time = {1}. ", date, time);
                UOServiceLib.Log.Error("DateTime format error using current time. " + e.Message, e);
                mDateTime = DateTime.Now;
            }
        }

        public void AddItemToXmlDocument(XmlDocument doc)
        {
            var el = doc.CreateElement("lineitem");
            var t = typeof(HCAITableLineItem);

            var prop = t.GetProperties();
            foreach (var p in prop)
            {
                var o = p.GetValue(this, null);
                el.SetAttribute(p.Name, o != null ? o.ToString() : "");
            }

            doc.DocumentElement?.AppendChild(el);
        }

        public void PopulateLineItem(XmlElement el)
        {
            var t = typeof(HCAITableLineItem);
            var prop = t.GetProperties();
            foreach (var p in prop)
            {
                var s = el.GetAttribute(p.Name);
                if (p.PropertyType == typeof(string))
                {
                    p.SetValue(this, s, null);
                }
                else if (p.PropertyType == typeof(int))
                {
                    int v;
                    try
                    {
                        v = Convert.ToInt32(s);
                    }
                    catch
                    {
                        v = 0;
                    }
                    p.SetValue(this, v, null);
                }
                else if (p.PropertyType == typeof(long))
                {
                    long l;
                    try
                    {
                        l = Convert.ToInt64(s);
                    }
                    catch
                    {
                        l = 0;
                    }
                    p.SetValue(this, l, null);
                }
                else if (p.PropertyType == typeof(float))
                {
                    float f;
                    try
                    {
                        f = Convert.ToSingle(s);
                    }
                    catch
                    {
                        f = 0;
                    }
                    p.SetValue(this, f, null);
                }
                else if (p.PropertyType == typeof(bool))
                {
                    bool b;
                    try
                    {
                        b = Convert.ToBoolean(s);
                    }
                    catch
                    {
                        b = false;
                    }
                    p.SetValue(this, b, null);
                }
            }
        }
    }
}


