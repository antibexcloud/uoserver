using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Collections;
using System.Xml;

namespace UOServerLib
{
    /// <summary>
    /// Static class which defines different types of messages
    /// </summary>
    [Guid("84A653B6-5133-40b6-B597-975423E291FC")]
    public interface MessageTypeInterface
    {
        [DispId(5010)]
        int Empty { get; }

        [DispId(5011)]
        int RequestFacilityInfo { get; }

        [DispId(5012)]
        int ReceivedFacilityInfo { get; }

        [DispId(5013)]
        int RequestInsurerList { get; }

        [DispId(5014)]
        int ReceivedInsurerList { get; }

        [DispId(5015)]
        int SubmitResponse { get; }

        [DispId(5016)]
        int SubmitOCF18 { get; }

        [DispId(5017)]
        int SubmitOCF21C { get; }

        [DispId(5018)]
        int SubmitOCF23 { get; }

        [DispId(5019)]
        int SubmitOCF22 { get; }

        [DispId(5020)]
        int SubmitOCF21B { get; }

        [DispId(5021)]
        int RequestAdjusterResponse { get; }

        [DispId(5022)]
        int SendAdjusterResponseAck { get; }

        [DispId(5023)]
        int AdjusterResponseAcknowledged { get; }

        [DispId(5024)]
        int AdjusterResponseRecieved { get; }

        [DispId(5025)]
        int Error { get; }

        [DispId(5026)]
        int VoidDocument { get; }

        [DispId(5027)]
        int VoidDocumentResponse { get; }

        [DispId(5028)]
        int RequestActivityList { get; }

        [DispId(5029)]
        int ActivityListRecieved { get; }

        [DispId(5030)]
        int BatchProcessingCompleted { get; }

        [DispId(5031)]
        int DocumentExtracted { get; }
    }

    [Serializable]
    [Guid("D39E9839-B913-41e8-B8FE-256CFEF33F50")]
    [ClassInterface(ClassInterfaceType.None)]
    public class MessageType : MessageTypeInterface
    {
        public int Empty => 0;

        public int RequestFacilityInfo => 1;

        public int ReceivedFacilityInfo => 2;

        public int RequestInsurerList => 3;

        public int ReceivedInsurerList => 4;

        public int SubmitResponse => 5;


        public int SubmitOCF18 => 6;

        public int SubmitOCF21C => 7;

        public int SubmitOCF23 => 8;

        public int SubmitOCF22 => 9;

        public int SubmitOCF21B => 10;

        public int RequestAdjusterResponse => 11;

        public int SendAdjusterResponseAck => 12;

        public int AdjusterResponseRecieved => 13;

        public int Error => 14;

        public int AdjusterResponseAcknowledged => 15;

        public int VoidDocument => 16;

        public int VoidDocumentResponse => 17;

        public int RequestActivityList => 18;

        public int ActivityListRecieved => 19;

        public int BatchProcessingCompleted => 20;

        public int DocumentExtracted => 21;
    }


    //*************************************************************************************************************
    /// <summary>
    /// Generic data type. Not sure if we'll need it.
    /// </summary>
    [Guid("6C6B174A-32BA-4139-838C-DAB72831D7AF")]
    public interface IDataType
    {
        [DispId(5000)]
        int MessageType { set; get; }

        [DispId(5001)]
        string MessageDate { set; get; }

        [DispId(5002)]
        string MessageTime { set; get; }

        [DispId(5003)]
        string MessageOCFType { set; get; }

        [DispId(5004)]
        XmlDocument CreateXMLDocument();

        [DispId(5005)]
        int ReadXMLDocument(XmlDocument doc);

    }

    [Serializable]
    [Guid("A5E0F4A2-5740-4d29-992C-31C6D0087E81")]
    [ClassInterface(ClassInterfaceType.None)]
    public class BasicMessage : IDataType
    {
        #region IDataType Members

        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            doc.DocumentElement.AppendChild(el);

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
            }
            else
                result = -1;

            return result;
        }

        #endregion
    }

    //**************************************************************************************************************
    /// <summary>
    /// Batch status change
    /// </summary>
    [Guid("E49A4DCB-C76E-4601-839D-1B06AADAD136")]
    public interface BatchInterface
    {
        [DispId(3050)]
        BatchItem GetItem(int i);

        [DispId(3051)]
        void SetItem(int i, BatchItem v);

        [DispId(3052)]
        void AddItem(BatchItem v);

        [DispId(3053)]
        XmlDocument CreateXmlDocument();

        [DispId(3054)]
        void ParseXmlDocument(XmlDocument doc);

        [DispId(3055)]
        int Count { get; }

    }

    [Serializable]
    [Guid("0E73AEBF-D6EC-4fb9-BEA9-FD0A0042C993")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Batch : BatchInterface
    {
        private ArrayList _items = new ArrayList();

        #region BatchInterface Members

        public int Count => _items.Count;

        public BatchItem GetItem(int i)
        {
            return _items.Count > 0 ? (BatchItem) _items[i] : null;
        }

        public void SetItem(int i, BatchItem v)
        {
            _items[i] = v;

        }

        public void AddItem(BatchItem v)
        {
            _items.Add(v);
        }

        public XmlDocument CreateXmlDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            foreach (BatchItem it in _items)
            {
                it.AddItemToXMLElement(doc.DocumentElement);
            }

            return doc;
        }

        public void ParseXmlDocument(XmlDocument doc)
        {
            var el = (XmlElement) doc.DocumentElement.FirstChild;

            while (el != null)
            {
                if (el.Name == "batchitem")
                {
                    var it = new BatchItem();
                    it.PopulateItemFromXMLElement(el);

                    AddItem(it);
                }

                el = (XmlElement)el.NextSibling;
            }
        }

        #endregion
    }

    [Guid("2C548ABA-4EDC-4979-A074-792F6DCADDC2")]
    public interface BatchItemInterface
    {
        [DispId(3020)]
        string OCFType { get; set; }

        [DispId(3021)]
        string OCFID { get; set; }

        [DispId(3022)]
        int OldStatus { get; set; }

        [DispId(3023)]
        int NewStatus { get; set; }

        [DispId(3024)]
        string HCAINumber { get; set; }

        [DispId(3025)]
        float ATotal { get; set; }

        [DispId(3026)]
        string StatusDate { get; set; }

        [DispId(3027)]
        string StatusTime { get; set; }

        [DispId(3028)]
        string MinSentDate { get; set; }

        [DispId(3029)]
        string ProposedAmount { get; set; }
    }

    [Serializable]
    [Guid("C803AD21-B072-4274-88FF-4576CF6B140C")]
    [ClassInterface(ClassInterfaceType.None)]
    public class BatchItem : BatchItemInterface
    {
        #region BatchInterface Members

        public string OCFType { get; set; }
        public string OCFID { get; set; }
        public int OldStatus { get; set; }
        public int NewStatus { get; set; }
        public string HCAINumber { get; set; }
        public float ATotal { get; set; }
        public string StatusDate { get; set; }
        public string StatusTime { get; set; }
        public string MinSentDate { get; set; }
        public string ProposedAmount { get; set; }
        #endregion

        public bool SkipItem { set; get; }

        public void AddItemToXMLElement(XmlElement el)
        {
            var doc = el.OwnerDocument;
            var elem = doc.CreateElement("batchitem");

            elem.SetAttribute("OCFType", OCFType);
            elem.SetAttribute("OCFID", OCFID);
            elem.SetAttribute("OldStatus", OldStatus.ToString());
            elem.SetAttribute("NewStatus", NewStatus.ToString());
            elem.SetAttribute("HCAINumber", HCAINumber);
            elem.SetAttribute("ATotal", ATotal.ToString());
            elem.SetAttribute("StatusDate", StatusDate);
            elem.SetAttribute("StatusTime", StatusTime);
            elem.SetAttribute("MinSentDate", MinSentDate);
            elem.SetAttribute("MinSentDate", ProposedAmount);

            el.AppendChild(elem);
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            OCFType = el.GetAttribute("OCFType");
            OCFID = el.GetAttribute("OCFID");
            OldStatus = Convert.ToInt32(el.GetAttribute("OldStatus"));
            NewStatus = Convert.ToInt32(el.GetAttribute("NewStatus"));
            HCAINumber = el.GetAttribute("HCAINumber");
            ATotal = Convert.ToSingle(el.GetAttribute("ATotal"));
            StatusDate = el.GetAttribute("StatusDate");
            StatusTime = el.GetAttribute("StatusTime");
            MinSentDate = el.GetAttribute("MinSentDate");
            ProposedAmount = el.GetAttribute("ProposedAmount");
        }
    }




    //**************************************************************************************************************
    /// <summary>
    /// Facilities List classes
    /// </summary>
    [Guid("8347C39D-B524-4a89-8F15-55C5132FDAE0")]
    public interface FacilityListInterface : IDataType
    {
        [DispId(937)]
        int Count { set; get; }

        [DispId(938)]
        Facility GetFacility(int num);

        [DispId(939)]
        void SetFacility(Facility f, int num);
    }

    [Serializable]
    [Guid("DF195A5D-88F8-4ebe-AD02-2CC4CEFDE7CD")]
    [ClassInterface(ClassInterfaceType.None)]
    public class FacilityList : FacilityListInterface
    {
        #region FacilityListInterface Members
        private Facility[] mFacilities = null;
        private int mCount = 0;

        public FacilityList()
        {
        }

        public FacilityList(int size)
        {
            if (size <= 0) return;
            mCount = size;
            mFacilities = new Facility[size];
        }

        public int Count
        {
            get => mCount;
            set => mCount = value;
        }

        public Facility GetFacility(int num)
        {
            if (num >= 0 && num < mCount)
                return mFacilities[num];
            return null;
        }

        public void SetFacility(Facility f, int num)
        {
            if (num >= 0 && num < mCount)
                mFacilities[num] = f;
        }

        #endregion

        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);
            el.SetAttribute("Count", Count.ToString());

            doc.DocumentElement.AppendChild(el);

            for (var i = 0; i < Count; i++)
            {
                var fc = GetFacility(i);

                fc.AddItemToXMLElement(doc.DocumentElement);
            }

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var i = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
                Count = Convert.ToInt32(el.GetAttribute("Count"));

                mFacilities = new Facility[Count];
            }
            else
                result = -1;


            el = (XmlElement) el.NextSibling;
            while (el != null)
            {
                if (el.Name == "facility")
                {
                    Facility fc = new Facility();
                    fc.PopulateItemFromXMLElement(el);

                    SetFacility(fc, i);
                    i++;
                }
                else
                    result = -2;

                el = (XmlElement) el.NextSibling;
            }

            return result;
        }
        #endregion
    }

    //**************************************************************************************************************
    /// <summary>
    /// Facility classes
    /// </summary>
    [Guid("062143CF-415D-44e3-8DB1-D8A9150AA191")]
    public interface FacilityInterface
    {
        [DispId(900)]
        string HCAI_Facility_Registry_ID { set; get; }

        [DispId(901)]
        string Facility_Name { set; get; }

        [DispId(902)]
        string Billing_Address_Line_1 { set; get; }

        [DispId(903)]
        string Billing_Address_Line_2 { set; get; }

        [DispId(904)]
        string Billing_City { set; get; }

        [DispId(905)]
        string Billing_Province { set; get; }

        [DispId(906)]
        string Billing_Postal_Code { set; get; }

        [DispId(907)]
        string AISI_Facility_Number { set; get; }

        [DispId(908)]
        string Telephone_Number { set; get; }

        [DispId(9008)]
        string Telephone_Extension { set; get; }

        [DispId(909)]
        string Authorizing_Officer_First_Name { set; get; }

        [DispId(910)]
        string Authorizing_Officer_Last_Name { set; get; }

        [DispId(911)]
        string Authorizing_Officer_Title { set; get; }

        [DispId(912)]
        string Lock_Payable_Flag { set; get; }

        [DispId(913)]
        ProviderList Providers { set; get; }

        [DispId(914)]
        FacilityLicense Licenses { set; get; }

        [DispId(915)]
        string Fax_Number { set; get; }

        [DispId(916)]
        string SameServiceAddress { set; get; }

        [DispId(917)]
        string Service_Address_Line_1 { set; get; }

        [DispId(918)]
        string Service_Address_Line_2 { set; get; }

        [DispId(919)]
        string Service_City { set; get; }

        [DispId(9020)]
        string Service_Province { set; get; }

        [DispId(9021)]
        string Service_Postal_Code { set; get; }
    }

    [Serializable]
    [Guid("D7E63C6B-C477-4212-BB6E-04A5B82CE573")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Facility : FacilityInterface
    {
        private Queue<ProviderList> mProviders = new Queue<ProviderList>();
        private Queue<FacilityLicense> mLincenses = new Queue<FacilityLicense>();


        #region FacilityListInterface Members

        public string HCAI_Facility_Registry_ID { get; set; }
        public string Facility_Name { get; set; }
        public string Billing_Address_Line_1 { get; set; }
        public string Billing_Address_Line_2 { get; set; }
        public string Billing_City { get; set; }
        public string Billing_Province { get; set; }
        public string Billing_Postal_Code { get; set; }
        public string AISI_Facility_Number { get; set; }
        public string Telephone_Number { get; set; }
        public string Telephone_Extension { get; set; }
        public string Authorizing_Officer_First_Name { get; set; }
        public string Authorizing_Officer_Last_Name { get; set; }
        public string Authorizing_Officer_Title { get; set; }
        public string Lock_Payable_Flag { get; set; }

        public ProviderList Providers
        {
            get => mProviders.Count > 0 ? mProviders.Dequeue() : null;
            set => mProviders.Enqueue(value);
        }

        public FacilityLicense Licenses
        {
            get => mLincenses.Count > 0 ? mLincenses.Dequeue() : null;
            set => mLincenses.Enqueue(value);
        }

        public string Fax_Number { set; get; }
        public string SameServiceAddress { get; set; }
        public string Service_Address_Line_1 { get; set; }
        public string Service_Address_Line_2 { get; set; }
        public string Service_City { get; set; }
        public string Service_Province { get; set; }
        public string Service_Postal_Code { get; set; }

        #endregion

        public void AddItemToXMLElement(XmlElement el)
        {
            var doc = el.OwnerDocument;
            var elem = doc.CreateElement("facility");

            elem.SetAttribute("HCAI_Facility_Registry_ID", HCAI_Facility_Registry_ID);
            elem.SetAttribute("Facility_Name", Facility_Name);
            elem.SetAttribute("Billing_Address_Line_1", Billing_Address_Line_1);
            elem.SetAttribute("Billing_Address_Line_2", Billing_Address_Line_2);
            elem.SetAttribute("Billing_City", Billing_City);
            elem.SetAttribute("Billing_Province", Billing_Province);
            elem.SetAttribute("Billing_Postal_Code", Billing_Postal_Code);
            elem.SetAttribute("SameServiceAddress", SameServiceAddress);
            elem.SetAttribute("Service_Address_Line_1", Service_Address_Line_1);
            elem.SetAttribute("Service_Address_Line_2", Service_Address_Line_2);
            elem.SetAttribute("Service_City", Service_City);
            elem.SetAttribute("Service_Province", Service_Province);
            elem.SetAttribute("Service_Postal_Code", Service_Postal_Code);
            elem.SetAttribute("AISI_Facility_Number", AISI_Facility_Number);
            elem.SetAttribute("Telephone_Number", Telephone_Number);
            elem.SetAttribute("Telephone_Extension", Telephone_Extension);
            elem.SetAttribute("Fax_Number", Fax_Number);
            elem.SetAttribute("Authorizing_Officer_First_Name", Authorizing_Officer_First_Name);
            elem.SetAttribute("Authorizing_Officer_Last_Name", Authorizing_Officer_Last_Name);
            elem.SetAttribute("Authorizing_Officer_Title", Authorizing_Officer_Title);
            elem.SetAttribute("Lock_Payable_Flag", Lock_Payable_Flag);

            var pr = Providers;
            while (pr != null)
            {
                pr.AddItemToXMLElement(elem);
                pr = Providers;
            }

            var ls = Licenses;
            while (ls != null)
            {
                ls.AddItemToXMLElement(elem);
                ls = Licenses;
            }

            el.AppendChild(elem);
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            HCAI_Facility_Registry_ID = el.GetAttribute("HCAI_Facility_Registry_ID");
            Facility_Name = el.GetAttribute("Facility_Name");
            Billing_Address_Line_1 = el.GetAttribute("Billing_Address_Line_1");
            Billing_Address_Line_2 = el.GetAttribute("Billing_Address_Line_2");
            Billing_City = el.GetAttribute("Billing_City");
            Billing_Province = el.GetAttribute("Billing_Province");
            Billing_Postal_Code = el.GetAttribute("Billing_Postal_Code");
            SameServiceAddress = el.GetAttribute("SameServiceAddress");
            Billing_Address_Line_1 = el.GetAttribute("Billing_Address_Line_1");
            Billing_Address_Line_2 = el.GetAttribute("Billing_Address_Line_2");
            Billing_City = el.GetAttribute("Billing_City");
            Billing_Province = el.GetAttribute("Billing_Province");
            Billing_Postal_Code = el.GetAttribute("Billing_Postal_Code");
            AISI_Facility_Number = el.GetAttribute("AISI_Facility_Number");
            Telephone_Number = el.GetAttribute("Telephone_Number");
            Telephone_Extension = el.GetAttribute("Telephone_Extension");
            Fax_Number = el.GetAttribute("Fax_Number");
            Authorizing_Officer_First_Name = el.GetAttribute("Authorizing_Officer_First_Name");
            Authorizing_Officer_Last_Name = el.GetAttribute("Authorizing_Officer_Last_Name");
            Authorizing_Officer_Title = el.GetAttribute("Authorizing_Officer_Title");
            Lock_Payable_Flag = el.GetAttribute("Lock_Payable_Flag");

            var subEl = (XmlElement)el.FirstChild;
            while (subEl != null)
            {
                switch(el.Name)
                {
                    case "provider":
                        var prov = new ProviderList();
                        prov.PopulateItemFromXMLElement(subEl);
                        Providers = prov;
                        break;
                    case "license":
                        var ls = new FacilityLicense();
                        ls.PopulateItemFromXMLElement(subEl);
                        Licenses = ls;
                        break;
                }
                subEl = (XmlElement)subEl.NextSibling;
            }
        }
    }



    //--------------------------------------------------------------------------------------------------------------
    [Guid("3812EEEB-DBA7-4c3e-8118-065571BD296C")]
    public interface ProviderListInterface
    {

        [DispId(920)]
        string HCAI_Provider_Registry_ID { set; get; }

        [DispId(921)]
        string Provider_Last_Name { set; get; }

        [DispId(922)]
        string Provider_First_Name { set; get; }

        //VERSION_2.0_beta
        [DispId(923)]
        string Provider_Status { set; get; }

        [DispId(924)]
        string Provider_Start_Date { set; get; }

        [DispId(925)]
        string Provider_End_Date { set; get; }
         //end

        [DispId(926)]
        ProviderRegistration ProviderReg { set; get; }
    }

    [Serializable]
    [Guid("C86ACE23-E503-4c17-AF8B-E0BE21836997")]
    [ClassInterface(ClassInterfaceType.None)]
    public class ProviderList : ProviderListInterface
    {
        #region ProviderListInterface Members
        private string mHCAI_Provider_Registry_ID;
        private string mProvider_Last_Name;
        private string mProvider_First_Name;
        private Queue mProviderReg = new Queue();

        public string HCAI_Provider_Registry_ID
        {
            get => mHCAI_Provider_Registry_ID;
            set => mHCAI_Provider_Registry_ID = value;
        }

        public string Provider_Last_Name
        {
            get => mProvider_Last_Name;
            set => mProvider_Last_Name = value;
        }

        public string Provider_First_Name
        {
            get => mProvider_First_Name;
            set => mProvider_First_Name = value;
        }

        //VERSION_2.0_beta
        public string Provider_Status { set; get; }

        public string Provider_Start_Date { set; get; }

        public string Provider_End_Date { set; get; }
        //end

        public ProviderRegistration ProviderReg
        {
            get
            {
                if (mProviderReg.Count > 0)
                    return (ProviderRegistration)mProviderReg.Dequeue();
                return null;
            }
            set => mProviderReg.Enqueue(value);
        }

        #endregion

        public void AddItemToXMLElement(XmlElement el)
        {
            var doc = el.OwnerDocument;
            var elem = doc.CreateElement("provider");

            elem.SetAttribute("HCAI_Provider_Registry_ID", HCAI_Provider_Registry_ID);
            elem.SetAttribute("Provider_Last_Name", Provider_Last_Name);
            elem.SetAttribute("Provider_First_Name", Provider_First_Name);

            //VERSION_2.0_beta
            elem.SetAttribute("Provider_Status", Provider_Status);
            elem.SetAttribute("Provider_Start_Date", Provider_Start_Date);
            elem.SetAttribute("Provider_End_Date", Provider_End_Date);
            //end

            var reg = ProviderReg;
            while (reg != null)
            {
                reg.AddItemToXMLElement(elem);
                reg = ProviderReg;
            }

            el.AppendChild(elem);
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            HCAI_Provider_Registry_ID = el.GetAttribute("HCAI_Provider_Registry_ID");
            Provider_Last_Name = el.GetAttribute("Provider_Last_Name");
            Provider_First_Name = el.GetAttribute("Provider_First_Name");

            //VERSION_2.0_beta
            Provider_Status = el.GetAttribute("Provider_Status");
            Provider_Start_Date = el.GetAttribute("Provider_Start_Date");
            Provider_End_Date = el.GetAttribute("Provider_End_Date");
            //end

            var reg = (XmlElement) el.FirstChild;
            while (reg != null)
            {
                var preg = new ProviderRegistration();
                preg.PopulateItemFromXMLElement(reg);
                ProviderReg = preg;

                reg = (XmlElement) reg.NextSibling;
            }
        }
    }



    //--------------------------------------------------------------------------------------------------------------
    [Guid("E6811526-C64C-4fed-B3E6-59254344EB84")]
    public interface ProviderRegistrationInterface
    {
        [DispId(927)]
        string Registration_Number { set; get; }

        [DispId(928)]
        string Profession { set; get; }
    }


    [Serializable]
    [Guid("22B025F5-16CF-47ae-96BA-AFD0FF2BD79E")]
    [ClassInterface(ClassInterfaceType.None)]
    public class ProviderRegistration : ProviderRegistrationInterface
    {
        public string Registration_Number { set; get; }
        public string Profession { set; get; }
     
        public void AddItemToXMLElement(XmlElement el)
        {
            var doc = el.OwnerDocument;
            var elem = doc.CreateElement("registration");

            elem.SetAttribute("Registration_Number", Registration_Number);
            elem.SetAttribute("Profession", Profession);

            el.AppendChild(elem);
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            Registration_Number = el.GetAttribute("Registration_Number");
            Profession = el.GetAttribute("Profession");
        }
    }

    [Guid("38D73BF0-E035-446E-BE7F-4180E771CEAD")]
    public interface FacilityLicenseInterface
    {
        [DispId(930)]
        string License_Number { set; get; }
        
        [DispId(931)]
        string License_Status { set; get; }
        
        [DispId(932)]
        string License_End_Date { set; get; }

        [DispId(933)]
        string License_Last_Modified_Date { set; get; }
        
    }

    [Serializable]
    [Guid("6FA3FE36-089F-43F2-AD76-735E89DEF15C")]
    [ClassInterface(ClassInterfaceType.None)]
    public class FacilityLicense : FacilityLicenseInterface
    {
        public string License_Number { get; set; }
        public string License_Status { get; set; }
        public string License_End_Date { get; set; }
        public string License_Last_Modified_Date { get; set; }

        public void AddItemToXMLElement(XmlElement el)
        {
            var doc = el.OwnerDocument;
            var elem = doc.CreateElement("license");

            elem.SetAttribute("License_Number", License_Number);
            elem.SetAttribute("License_Status", License_Status);
            elem.SetAttribute("License_End_Date", License_End_Date);
            elem.SetAttribute("License_Last_Modified_Date", License_Last_Modified_Date);

            el.AppendChild(elem);
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            License_Number = el.GetAttribute("License_Number");
            License_Status = el.GetAttribute("License_Status");
            License_End_Date = el.GetAttribute("License_End_Date"); 
            License_Last_Modified_Date = el.GetAttribute("License_Last_Modified_Date");
        }
    }

    //**************************************************************************************************************
    /// <summary>
    /// Activity List
    /// </summary>
    [Guid("567D69C8-42B0-4451-9CD8-0ED3B4864844")]
    public interface ActivityListInterface : IDataType
    {
        [DispId(1000)]
        int Count { set; get; }

        [DispId(1001)]
        Activity Activities { set; get; }
    }

    [Serializable]
    [Guid("007E061F-12E0-4c5e-9B8E-AF97D5AADD5C")]
    [ClassInterface(ClassInterfaceType.None)]
    public class ActivityList : ActivityListInterface
    {
        #region ActivityListInterface Members
        private Queue mActivities = new Queue();
        private int mCount;

        public int Count
        {
            get => mCount;
            set => mCount = value;
        }

        public Activity Activities
        {
            get
            {
                if (mActivities.Count > 0)
                    return (Activity)mActivities.Dequeue();
                return null;
            }
            set => mActivities.Enqueue(value);
        }

        #endregion

        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            doc.DocumentElement.AppendChild(el);

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var el = (XmlElement) doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
            }
            else
                result = -1;

            return result;
        }
        #endregion
    }


    //--------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Activity
    /// </summary>
    [Guid("FA5E17A3-62EE-4c0c-A39A-206AD480C913")]
    public interface ActivityInterface
    {
        [DispId(1002)]
        string HCAI_Document_Number { set; get; }

        [DispId(1003)]
        string PMS_Document_Key { set; get; }

        [DispId(1004)]
        string PMS_Patient_Key { set; get; }

        [DispId(1005)]
        string Submission_Source { set; get; }

        [DispId(1006)]
        string Submission_Date { set; get; }

        [DispId(1007)]
        string Document_Status { set; get; }
    }

    [Serializable]
    [Guid("4DD8E3C9-4680-41a6-8053-10C5F92A5A5C")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Activity : ActivityInterface
    {
        #region ActivityInterface Members
        private string mHCAI_Document_Number;
        private string mPMS_Document_Key;
        private string mPMS_Patient_Key;
        private string mSubmission_Source;
        private string mSubmission_Date;
        private string mDocument_Status;

        public string HCAI_Document_Number
        {
            get => mHCAI_Document_Number;
            set => mHCAI_Document_Number = value;
        }

        public string PMS_Document_Key
        {
            get => mPMS_Document_Key;
            set => mPMS_Document_Key = value;
        }

        public string PMS_Patient_Key
        {
            get => mPMS_Patient_Key;
            set => mPMS_Patient_Key = value;
        }

        public string Submission_Source
        {
            get => mSubmission_Source;
            set => mSubmission_Source = value;
        }

        public string Submission_Date
        {
            get => mSubmission_Date;
            set => mSubmission_Date = value;
        }

        public string Document_Status
        {
            get => mDocument_Status;
            set => mDocument_Status = value;
        }

        #endregion
    }


    //*************************************************************************************************************
    /// <summary>
    /// Void document response
    /// </summary>
    [Guid("C37B4B6C-C79E-4d9f-BEE8-3792FB8F70C7")]
    public interface VoidDocumentResponseInterface : IDataType
    {
        [DispId(3760)]
        string HCAI_DocumentNumber { set; get; }

        [DispId(3761)]
        string PMS_Document_Key { set; get; }

        [DispId(3762)]
        string Result { set; get; }

    }

    [Serializable]
    [Guid("CEA6BAEC-E5B9-4f65-BD5B-5192AAC20D95")]
    [ClassInterface(ClassInterfaceType.None)]
    public class VoidDocumentResponse : VoidDocumentResponseInterface
    {
        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            doc.DocumentElement.AppendChild(el);

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
            }
            else
                result = -1;

            return result;
        }
        #endregion


        #region VoidDocumentResponseInterface Members

        private string mHCAI_DocumentNumber;
        private string mPMS_Document_Key;
        private string mResult;

        public string HCAI_DocumentNumber
        {
            get => mHCAI_DocumentNumber;
            set => mHCAI_DocumentNumber = value;
        }

        public string PMS_Document_Key
        {
            get => mPMS_Document_Key;
            set => mPMS_Document_Key = value;
        }

        public string Result
        {
            get => mResult;
            set => mResult = value;
        }

        #endregion
    }

    //**************************************************************************************************************
    /// <summary>
    /// Insurer List classes
    /// </summary>
    [Guid("8D561E75-58FE-4d52-A908-979EC6D236E0")]
    public interface InsurerListInterface : IDataType
    {
        [DispId(940)]
        int Count { set; get; }

        [DispId(941)]
        void SetInsurer(Insurer ns, int num);

        [DispId(942)]
        Insurer GetInsurer(int num);
    }

    [Serializable]
    [Guid("347DA378-EA75-4261-832B-7B91DBD725BA")]
    [ClassInterface(ClassInterfaceType.None)]
    public class InsurerList : InsurerListInterface
    {
        #region InsurerListInterface Members
        private Insurer[] mInsurers = null;
        private int mCount = 0;

        public InsurerList()
        {
        }

        public InsurerList(int size)
        {
            if (size <= 0) return;
            mCount = size;
            mInsurers = new Insurer[size];
        }

        public int Count
        {
            get => mCount;
            set => mCount = value;
        }

        public Insurer GetInsurer(int num)
        {
            if (num >= 0 && num < mCount)
                return mInsurers[num];
            return null;
        }

        public void SetInsurer(Insurer ns, int num)
        {
            if (num >= 0 && num < mCount)
                mInsurers[num] = ns;
        }

        #endregion

        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            el.SetAttribute("Count", Count.ToString());
            
            doc.DocumentElement.AppendChild(el);

            for (int i = 0; i < Count; i++)
            {
                Insurer ins = GetInsurer(i);

                ins.AddItemToXMLElement(doc.DocumentElement);
            }

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var i = 0;
            var result = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                MessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                MessageDate = el.GetAttribute("MessageDate");
                MessageTime = el.GetAttribute("MessageTime");
                MessageOCFType = el.GetAttribute("MessageOCFType");
                Count = Convert.ToInt32(el.GetAttribute("Count"));

                mInsurers = new Insurer[Count];
            }
            else
                result = -1;

            el = (XmlElement) el.NextSibling;
            while (el != null)
            {
                if (el.Name == "insurer")
                {
                    Insurer ins = new Insurer();
                    ins.PopulateItemFromXMLElement(el);

                    SetInsurer(ins, i);
                    i++;
                }
                else
                    result = -2;

                el = (XmlElement) el.NextSibling;
            }

            return result;
        }
        #endregion
    }



    //--------------------------------------------------------------------------------------------------------------
    [Guid("8CB755B0-01AF-4b2a-A4FE-053B66FA9ABD")]
    public interface InsurerInterface
    {
        [DispId(943)]
        string Insurer_ID { set; get; }

        [DispId(944)]
        string Insurer_Name { set; get; }

        [DispId(945)]
        string Insurer_Status { set; get; }

        [DispId(946)]
        InsurerBranch Branches { set; get; }
    }

    [Serializable]
    [Guid("D3CD7638-78E1-451d-8E99-752707C596AF")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Insurer : InsurerInterface
    {
        #region InsurerInterface Members
        private string mInsurer_ID;
        private string mInsurer_Name;
        private string mInsurer_Status;
        private Queue mBranches = new Queue();

        public string Insurer_ID
        {
            get => mInsurer_ID;
            set => mInsurer_ID = value;
        }

        public string Insurer_Name
        {
            get => mInsurer_Name;
            set => mInsurer_Name = value;
        }

        public string Insurer_Status
        {
            get => mInsurer_Status;
            set => mInsurer_Status = value;
        }

        public InsurerBranch Branches
        {
            get
            {
                if (mBranches.Count > 0)
                    return (InsurerBranch)mBranches.Dequeue();
                return null;
            }
            set => mBranches.Enqueue(value);
        }

        public void AddItemToXMLElement(XmlElement el)
        {
            try
            {
                var doc = el.OwnerDocument;
                var elem = doc.CreateElement("insurer");

                elem.SetAttribute("Insurer_ID", Insurer_ID);
                elem.SetAttribute("Insurer_Name", Insurer_Name);
                elem.SetAttribute("Insurer_Status", Insurer_Status);

                var branch = Branches;
                while (branch != null)
                {
                    branch.AddItemToXMLElement(elem);
                    branch = Branches;
                }

                el.AppendChild(elem);
            }
            catch (Exception e)
            {
                UOServiceLib.Log.Error(e.Message);
            }
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            Insurer_ID = el.GetAttribute("Insurer_ID");
            Insurer_Name = el.GetAttribute("Insurer_Name");
            Insurer_Status = el.GetAttribute("Insurer_Status");

            var branch = (XmlElement)el.FirstChild;
            while (branch != null)
            {
                var inbr = new InsurerBranch();
                inbr.PopulateItemFromXMLElement(branch);
                Branches = inbr;

                branch = (XmlElement)branch.NextSibling;
            }
        }

        #endregion
    }



    //--------------------------------------------------------------------------------------------------------------
    [Guid("5ECBE701-335D-41c4-BB48-461207E4A44F")]
    public interface InsurerBranchInterface
    {
        [DispId(947)]
        string Branch_ID { set; get; }

        [DispId(948)]
        string Branch_Name { set; get; }

        [DispId(949)]
        string Branch_Status { set; get; }
    }


    [Serializable]
    [Guid("F927ECAE-B4CE-4985-A16B-5EFE48900D34")]
    [ClassInterface(ClassInterfaceType.None)]
    public class InsurerBranch : InsurerBranchInterface
    {
        #region InsurerBranchInterface Members

        private string mBranch_ID;
        private string mBranch_Name;
        private string mBranch_Status;

        public string Branch_ID
        {
            get => mBranch_ID;
            set => mBranch_ID = value;
        }

        public string Branch_Name
        {
            get => mBranch_Name;
            set => mBranch_Name = value;
        }

        public string Branch_Status
        {
            get => mBranch_Status;
            set => mBranch_Status = value;
        }

        #endregion

        public void AddItemToXMLElement(XmlElement el)
        {
            var doc = el.OwnerDocument;
            var elem = doc.CreateElement("branch");

            elem.SetAttribute("Branch_ID", Branch_ID);
            elem.SetAttribute("Branch_Name", Branch_Name);
            elem.SetAttribute("Branch_Status", Branch_Status);

            el.AppendChild(elem);
        }

        public void PopulateItemFromXMLElement(XmlElement el)
        {
            Branch_ID = el.GetAttribute("Branch_ID");
            Branch_Name = el.GetAttribute("Branch_Name");
            Branch_Status = el.GetAttribute("Branch_Status");
        }
    }


    //*************************************************************************************************************
    /// <summary>
    /// ErrorMessage
    /// </summary>
    [Guid("3CFD1E06-4A69-4ea6-BFAD-AD3FDDD7E18D")]
    public interface ErrorMessageInterface : IDataType
    {
        [DispId(3600)]
        string Error_OCFType { set; get; }

        [DispId(3601)]
        string Error_Applicant_FirstName { set; get; }

        [DispId(3602)]
        string Error_Applicant_LastName { set; get; }

        [DispId(3603)]
        string Error_PlanNumber { set; get; }

        [DispId(3604)]
        string Error_HCAI_ErrorType { set; get; }

        [DispId(3605)]
        string Error_ErrorList { set; get; }

        [DispId(3606)]
        string Error_PMS_Document_Key { set; get; }

        [DispId(3607)]
        int Error_ErrorType { set; get; }

        [DispId(3608)]
        string Error_HCAI_Document_Number { set; get; }

        [DispId(3609)]
        string Error_CompleteErrorDesc { set; get; }
    }

    [Serializable]
    [Guid("F7770CCA-47B2-4a9e-9725-020D223861D3")]
    [ClassInterface(ClassInterfaceType.None)]
    public class ErrorMessage : ErrorMessageInterface
    {
        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");
            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            doc.DocumentElement.AppendChild(el);

            el = doc.CreateElement("errormessage");
            el.SetAttribute("Error_OCFType", Error_OCFType);
            el.SetAttribute("Error_Applicant_FirstName", Error_Applicant_FirstName);
            el.SetAttribute("Error_Applicant_LastName", Error_Applicant_LastName);
            el.SetAttribute("Error_PlanNumber", Error_PlanNumber);
            el.SetAttribute("Error_HCAI_ErrorType", Error_HCAI_ErrorType);
            el.SetAttribute("Error_PMS_Document_Key", Error_PMS_Document_Key);
            el.SetAttribute("Error_ErrorType", Error_ErrorType.ToString());
            el.SetAttribute("Error_HCAI_Document_Number", Error_HCAI_Document_Number);

            doc.DocumentElement.AppendChild(el);

            var tmp = Error_ErrorList;
            while (tmp != Commands.SEP[0])
            {
                el = doc.CreateElement("errorlistitem");
                el.InnerText = tmp;

                doc.DocumentElement.AppendChild(el);
            }

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
            }
            else
                result = -1;

            el = (XmlElement) el.NextSibling;

            if (el.Name == "errormessage")
            {
                Error_OCFType = el.GetAttribute("Error_OCFType");
                Error_Applicant_FirstName = el.GetAttribute("Error_Applicant_FirstName");
                Error_Applicant_LastName = el.GetAttribute("Error_Applicant_LastName");
                Error_PlanNumber = el.GetAttribute("Error_PlanNumber");
                Error_HCAI_ErrorType = el.GetAttribute("Error_HCAI_ErrorType");
                Error_PMS_Document_Key = el.GetAttribute("Error_PMS_Document_Key");
                Error_ErrorType = Convert.ToInt32(el.GetAttribute("Error_ErrorType"));
                Error_HCAI_Document_Number = el.GetAttribute("Error_HCAI_Document_Number");
            }
            else
                result = -2;

            el = (XmlElement) el.NextSibling;
            while (el != null)
            {
                if (el.Name == "errorlistitem")
                {
                    Error_ErrorList = el.InnerText;
                }

                el = (XmlElement)el.NextSibling;
            }

            return result;
        }
        #endregion


        #region ErrorMessageInterface Members

        private string mError_PMS_Document_Key;
        private string mError_OCFType;
        private string mError_Applicant_FirstName;
        private string mError_Applicant_LastName;
        private string mError_PlanNumber;
        private string mError_HCAI_ErrorType;
        private Queue mError_ErrorList = new Queue();
        private int mError_ErrorType;
        private string mError_HCAI_Document_Number;
        private string mError_CompleteErrorDesc;

        public string Error_HCAI_Document_Number
        {
            get => mError_HCAI_Document_Number;
            set => mError_HCAI_Document_Number = value;
        }

        public string Error_PMS_Document_Key
        {
            get => mError_PMS_Document_Key;
            set => mError_PMS_Document_Key = value;
        }

        public string Error_OCFType
        {
            get => mError_OCFType;
            set => mError_OCFType = value;
        }

        public string Error_Applicant_FirstName
        {
            get => mError_Applicant_FirstName;
            set => mError_Applicant_FirstName = value;
        }

        public string Error_Applicant_LastName
        {
            get => mError_Applicant_LastName;
            set => mError_Applicant_LastName = value;
        }

        public string Error_PlanNumber
        {
            get => mError_PlanNumber;
            set => mError_PlanNumber = value;
        }

        public string Error_HCAI_ErrorType
        {
            get => mError_HCAI_ErrorType;
            set => mError_HCAI_ErrorType = value;
        }

        public string Error_ErrorList
        {
            get
            {
                if (mError_ErrorList.Count > 0)
                {
                    return (string)mError_ErrorList.Dequeue();
                }

                return Commands.SEP[0];
            }
            set => mError_ErrorList.Enqueue(value);
        }

        public int Error_ErrorType
        {
            get => mError_ErrorType;
            set => mError_ErrorType = value;
        }

        public string Error_CompleteErrorDesc
        {
            get => mError_CompleteErrorDesc;
            set => mError_CompleteErrorDesc = value;
        }

        #endregion
    }

    //**************************************************************************************************************
    /// <summary>
    /// Submit Response
    /// </summary>
    [Guid("722497CC-890B-4efb-8B50-3E1AC284E469")]
    public interface SubmitResponseInterface : IDataType
    {
        [DispId(960)]
        string HCAI_Document_Number { set; get; }

        [DispId(961)]
        string PMS_Document_Key { set; get; }

        [DispId(962)]
        string PMS_Patient_Key { set; get; }
    }

    [Serializable]
    [Guid("71A55D44-AA2B-4ff3-AC5B-1260497FD415")]
    [ClassInterface(ClassInterfaceType.None)]
    public class SubmitResponse : SubmitResponseInterface
    {
        #region SubmitResponseInterface Members
        private string mHCAI_Document_Number;
        private string mPMS_Document_Key;
        private string mPMS_Patient_Key;

        public string HCAI_Document_Number
        {
            get => mHCAI_Document_Number;
            set => mHCAI_Document_Number = value;
        }

        public string PMS_Document_Key
        {
            get => mPMS_Document_Key;
            set => mPMS_Document_Key = value;
        }

        public string PMS_Patient_Key
        {
            get => mPMS_Patient_Key;
            set => mPMS_Patient_Key = value;
        }

        #endregion

        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            doc.DocumentElement.AppendChild(el);

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
            }
            else
                result = -1;

            return result;
        }
        #endregion
    }



    //**************************************************************************************************************
    /// <summary>
    /// Adjuster Response
    /// </summary>
    [Guid("1AFA0984-01A8-4665-85C1-C557178299DB")]
    public interface AdjusterResponseInterface : IDataType
    {
        //General
        [DispId(1010)]
        string HCAI_Document_Number { set; get; }

        [DispId(1011)]
        string PMS_Document_Key { set; get; }

        [DispId(1012)]
        string Submission_Date { set; get; }

        [DispId(1013)]
        string Submission_Source { set; get; }

        [DispId(1014)]
        string Adjuster_Response_Date { set; get; }

        [DispId(1015)]
        string In_Dispute { set; get; }

        [DispId(1016)]
        string Document_Type { set; get; }

        [DispId(1017)]
        string Document_Status { set; get; }

        [DispId(1018)]
        string Attachments_Received_Date { set; get; }

        //Claimant Keys
        [DispId(1019)]
        string Claimant_First_Name { set; get; }

        [DispId(1020)]
        string Claimant_Middle_Name { set; get; }

        [DispId(1021)]
        string Claimant_Last_Name { set; get; }

        [DispId(1022)]
        string Claimant_Date_Of_Birth { set; get; }

        [DispId(1023)]
        string Claimant_Gender { set; get; }

        [DispId(1024)]
        string Claimant_Telephone { set; get; }

        [DispId(1025)]
        string Claimant_Extension { set; get; }

        [DispId(1026)]
        string Claimant_Address1 { set; get; }

        [DispId(1027)]
        string Claimant_Address2 { set; get; }

        [DispId(1028)]
        string Claimant_City { set; get; }

        [DispId(1029)]
        string Claimant_Province { set; get; }

        [DispId(1030)]
        string Claimant_Postal_Code { set; get; }



        //Keys for OCF18
        //Insurer_Keys
        [DispId(1031)]
        string Insurer_IBCInsurerID { set; get; }

        [DispId(1032)]
        string Insurer_IBCBranchID { set; get; }

        //Session keys
        [DispId(1033)]
        AdjusterResponseOCF18SessionLineItem OCF18_Session_LineItem { set; get; }

        //Non Session Keys
        [DispId(1034)]
        AdjusterResponseOCF18NonSessionLineItem OCF18_NonSession_LineItem { set; get; }

        [DispId(1054)]
        string OCF18_InsurerSignature_ApplicantSignatureWaived { set; get; }
        
        [DispId(10055)]
        string OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile { set; get; }

        [DispId(10056)]
        string OCF18_OtherHealthPractitioner_LicenseNumber { set; get; }

        //Insurer totals
        [DispId(1035)]
        string OCF18_InsurerTotals_MOH_Approved_LineCost { set; get; }

        [DispId(1036)]
        string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1037)]
        string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1038)]
        string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //		[DispId(1053)]
        //		string OCF18_InsurerTotals_OtherInsurers_Approved {set; get;}	

        [DispId(1039)]
        string OCF18_InsurerTotals_OtherInsurers_Approved_LineCost { set; get; }

        [DispId(1040)]
        string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1041)]
        string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1042)]
        string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1043)]
        string OCF18_InsurerTotals_AutoInsurerTotal_Approved { set; get; }

        [DispId(1370)]
        string OCF18_InsurerTotals_GST_Approved { set; get; }

        [DispId(1044)]
        string OCF18_InsurerTotals_GST_Approved_LineCost { set; get; }

        [DispId(1045)]
        string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1046)]
        string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1047)]
        string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1048)]
        string OCF18_InsurerTotals_PST_Approved { set; get; }

        [DispId(1049)]
        string OCF18_InsurerTotals_PST_Approved_LineCost { set; get; }

        [DispId(1050)]
        string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1051)]
        string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1052)]
        string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1055)]
        string OCF18_InsurerTotals_SubTotal_Approved { set; get; }

        [DispId(1056)]
        string OCF18_InsurerTotals_TotalCount_Approved { set; get; }

        [DispId(1057)]
        string OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation { set; get; }
        


        //Keys for OCF21B
        [DispId(1060)]
        string HCAI_Plan_Document_Number { set; get; }

        /*		[DispId(1061)]
                string Insurer_IBCInsurerID {set; get;}

                [DispId(1062)]
                string Insurer_IBCBranchID {set; get;}
        */
        [DispId(1063)]
        AdjusterResponseOCF21BLineItem OCF21B_LineItem { set; get; }

        [DispId(1064)]
        string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost { set; get; }

        [DispId(1065)]
        string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1066)]
        string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1067)]
        string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1068)]
        string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost { set; get; }

        [DispId(1069)]
        string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1070)]
        string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1071)]
        string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1072)]
        string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost { set; get; }

        [DispId(1073)]
        string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1074)]
        string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1075)]
        string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //---------
        [DispId(10064)]
        string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost { set; get; }

        [DispId(10065)]
        string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(10066)]
        string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(10067)]
        string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(10068)]
        string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost { set; get; }

        [DispId(10069)]
        string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(10070)]
        string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(10071)]
        string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(10072)]
        string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost { set; get; }

        [DispId(10073)]
        string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(10074)]
        string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(10075)]
        string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(20075)]
        string OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation { set; get; }

        [DispId(20076)]
        string OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation { set; get; }
        
        
        //------------

        [DispId(1076)]
        string OCF21B_InsurerTotals_MOH_Approved { set; get; }

        [DispId(1077)]
        string OCF21B_InsurerTotals_OtherInsurers_Approved { set; get; }

        [DispId(1078)]
        string OCF21B_InsurerTotals_AutoInsurerTotal_Approved { set; get; }

        [DispId(1079)]
        string OCF21B_InsurerTotals_GST_Approved_LineCost { set; get; }

        [DispId(1080)]
        string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1081)]
        string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1082)]
        string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //		[DispId(1083)]
        //		string OCF21B_InsurerTotals_PST_Approved {set; get;}

        [DispId(1084)]
        string OCF21B_InsurerTotals_PST_Approved_LineCost { set; get; }

        [DispId(1085)]
        string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1086)]
        string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1087)]
        string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1089)]
        string OCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved { set; get; }

        //		[DispId(1090)]
        //		string OCF21B_InsurerTotals_Interest_Approved {set; get;}

        [DispId(1091)]
        string OCF21B_InsurerTotals_Interest_Approved_LineCost { set; get; }

        [DispId(1092)]
        string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1093)]
        string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1094)]
        string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }



        //Keys for 21C
        [DispId(1100)]
        string HCAI_OCF23_Document_Number { set; get; }

        /*		[DispId(1101)]
                string Insurer_IBCInsurerID {set; get;}

                [DispId(1102)]
                string Insurer_IBCBranchID {set; get;}
        */
        [DispId(1103)]
        AdjusterResponseOCF21CPAFReimbursableLineItem OCF21C_PAFReimbursable_LineItem { set; get; }

        [DispId(1104)]
        AdjusterResponseOCF21COtherReimbursableLineItem OCF21C_OtherReimbursable_LineItem { set; get; }

        [DispId(1105)]
        string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost { set; get; }

        [DispId(1106)]
        string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1107)]
        string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1108)]
        string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1109)]
        string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost { set; get; }

        [DispId(1110)]
        string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1111)]
        string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1112)]
        string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1113)]
        string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost { set; get; }

        [DispId(1114)]
        string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1115)]
        string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1116)]
        string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //---------
        [DispId(11064)]
        string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost { set; get; }

        [DispId(11065)]
        string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(11066)]
        string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(11067)]
        string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(11068)]
        string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost { set; get; }

        [DispId(11069)]
        string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(11070)]
        string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(11071)]
        string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(11072)]
        string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost { set; get; }

        [DispId(11073)]
        string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(11074)]
        string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(11075)]
        string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(21075)]
        string OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation { set; get; }

        [DispId(21076)]
        string OCF21C_PAFReimbursableFees_AdjusterResponseExplanation { set; get; }

        //------------

        [DispId(1117)]
        string OCF21C_InsurerTotals_MOH_Approved { set; get; }

        [DispId(1118)]
        string OCF21C_InsurerTotals_OtherInsurers_Approved { set; get; }

        [DispId(1119)]
        string OCF21C_InsurerTotals_AutoInsurerTotal_Approved { set; get; }

        //		[DispId(1134)]
        //		string OCF21C_InsurerTotals_GST_Approved {set; get;}

        [DispId(1120)]
        string OCF21C_InsurerTotals_GST_Approved_LineCost { set; get; }

        [DispId(1121)]
        string OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1122)]
        string OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1123)]
        string OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //		[DispId(1135)]
        //		string OCF21C_InsurerTotals_PST_Approved {set; get;}

        [DispId(1124)]
        string OCF21C_InsurerTotals_PST_Approved_LineCost { set; get; }

        [DispId(1125)]
        string OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1126)]
        string OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1127)]
        string OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //		[DispId(1136)]
        //		string OCF21C_InsurerTotals_Interest_Approved {set; get;}

        [DispId(1128)]
        string OCF21C_InsurerTotals_Interest_Approved_LineCost { set; get; }

        [DispId(1129)]
        string OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1130)]
        string OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1131)]
        string OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1132)]
        string OCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved { set; get; }

        [DispId(1133)]
        string OCF21C_InsurerTotals_SubTotalPreApproved_Approved { set; get; }


        //Keys for OCF22
        [DispId(1200)]
        string OCF22_Insurer_IBCInsurerID { set; get; }

        [DispId(1201)]
        string OCF22_Insurer_IBCBranchID { set; get; }

        [DispId(1202)]
        AdjusterResponseOCF22LineItem OCF22_LineItem { set; get; }

        [DispId(1203)]
        string OCF22_InsurerTotals_MOH_Approved_LineCost { set; get; }

        [DispId(1204)]
        string OCF22_InsurerTotals_OtherInsurers_Approved_LineCost { set; get; }

        [DispId(1205)]
        string OCF22_InsurerTotals_AutoInsurerTotal_Approved { set; get; }

        [DispId(1206)]
        string OCF22_InsurerTotals_GST_Approved_LineCost { set; get; }

        //		[DispId(1207)]
        //		string OCF22_InsurerTotals_PST_Approved {set; get;}

        [DispId(1208)]
        string OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved { set; get; }

        [DispId(1209)]
        string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1210)]
        string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1211)]
        string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1212)]
        string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1213)]
        string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1214)]
        string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //		[DispId(1371)]
        //		string OCF22_InsurerTotals_GST_Approved {set; get;}

        [DispId(1215)]
        string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1216)]
        string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1217)]
        string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        [DispId(1218)]
        string OCF22_InsurerTotals_PST_Approved_LineCost { set; get; }

        [DispId(1219)]
        string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(1220)]
        string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(1221)]
        string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }




        //Keys for OCF23
        /*		[DispId(1300)]
                string Insurer_IBCInsurerID {set; get;}

                [DispId(1301)]
                string Insurer_IBCBranchID {set; get;}
        */
        [DispId(1302)]
        AdjusterResponseOCF23OtherGSLineItem OCF23_OtherGS_LineItem { set; get; }

        [DispId(1303)]
        string OCF23_InsurerTotals_AutoInsurerTotal_Approved { set; get; }

        [DispId(1304)]
        string OCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved { set; get; }

        [DispId(1305)]
        string OCF23_InsurerTotals_SubTotalPreApproved_Approved { set; get; }

        [DispId(1306)]
        string OCF23_InsurerSignature_ApplicantSignatureWaived { set; get; }

        [DispId(1307)]
        string OCF23_InsurerSignature_ResponseForOtherGoodsAndServices { set; get; }

        [DispId(1308)]
        string OCF23_InsurerSignature_PolicyInForceConfirmation { set; get; }

        [DispId(1310)]
        string OCF23_OtherGoodsAndServices_AdjusterResponseExplanation { set; get; }

        [DispId(1309)]
        string OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18 { set; get; }


        //Keys for OCF9
        [DispId(1350)]
        string OCF9_OCFVersion { set; get; }

        [DispId(1351)]
        string Parent_Document_Number { set; get; }

        [DispId(1352)]
        string OCF9_AttachmentsBeingSent { set; get; }

        /*		[DispId(1353)]
                string OCF9_Header_DateOfAccident {set; get;}
        */
        [DispId(1354)]
        string OCF9_Header_DateRevised { set; get; }

        /*		[DispId(1355)]
                string OCF9_Applicant_Name_FirstName {set; get;}

                [DispId(1356)]
                string OCF9_Applicant_Name_MiddleName {set; get;}

                [DispId(1358)]
                string OCF9_Applicant_Name_LastName {set; get;}

                [DispId(1359)]
                string OCF9_Applicant_DateOfBirth {set; get;}

                [DispId(1360)]
                string OCF9_Applicant_Gender {set; get;}

                [DispId(1361)]
                string OCF9_Applicant_Telephone_TelephoneNumber {set; get;}

                [DispId(1362)]
                string OCF9_Applicant_Telephone_TelephoneExtension {set; get;}

                [DispId(1363)]
                string OCF9_Applicant_Address_StreetAddress1 {set; get;}

                [DispId(1364)]
                string OCF9_Applicant_Address_StreetAddress2 {set; get;}

                [DispId(1365)]
                string OCF9_Applicant_Address_City {set; get;}

                [DispId(1366)]
                string OCF9_Applicant_Address_Province {set; get;}

                [DispId(1367)] //this number is used
                string OCF9_Applicant_Address_PostalCode {set; get;}
        */
        [DispId(1367)]
        AdjusterResponseOCF9LineItem OCF9_LineItem { set; get; }

        [DispId(1374)]
        IList<AdjusterResponseOCF9LineItem> OCF9_LineItem2 { get; }

        [DispId(1368)]
        string OCF9_AdditionalComments { set; get; }

        [DispId(1369)]
        bool IsOCF9Present { set; get; }

        //VERSION_2.0_beta
        [DispId(1372)]
        string SigningAdjuster_FirstName { set; get; }

        [DispId(1373)]
        string SigningAdjuster_LastName { set; get; }
        //end


        //Keys for Form1
        [DispId(1400)]
        string Form1_AdjusterResponseTime { get; set; }
        [DispId(1401)]
        string Form1_ArchivalStatus { get; set; }
        [DispId(1402)]
        string Form1_AttachmentsReceivedDate { get; set; }
        [DispId(1403)]
        string Form1_BranchVersion { get; set; }

        //string Form1_Claimant_Address_City { get; set; }
        //string Form1_Claimant_Address_PostalCode { get; set; }
        //string Form1_Claimant_Address_Province { get; set; }
        //string Form1_Claimant_Address_StreetAddress1 { get; set; }
        //string Form1_Claimant_Address_StreetAddress2 { get; set; }
        //string Form1_Claimant_DateOfBirth { get; set; }
        //string Form1_Claimant_Extension { get; set; }
        //string Form1_Claimant_Gender { get; set; }
        //string Form1_Claimant_Name_FirstName { get; set; }
        //string Form1_Claimant_Name_LastName { get; set; }
        //string Form1_Claimant_Name_MiddleName { get; set; }
        //string Form1_Claimant_Telephone { get; set; }

        [DispId(1404)]
        string Form1_Costs_Approved_AdjusterResponseExplanation { get; set; }
        [DispId(1405)]
        string Form1_Costs_Approved_Benefit { get; set; }
        [DispId(1406)]
        string Form1_Costs_Approved_CalculatedBenefit { get; set; }
        [DispId(1407)]
        string Form1_Costs_Approved_ReasonCode { get; set; }
        [DispId(1408)]
        string Form1_Costs_Approved_ReasonDescription { get; set; }

        //string Form1_Document_Type { get; set; }
        
        //string Form1_DocumentState { get; set; }
        [DispId(1411)]
        string Form1_DocumentVersion { get; set; }
        [DispId(1412)]
        string Form1_EOB_EOBAdditionalComments { get; set; }
        [DispId(1413)]
        string Form1_FacilityVersion { get; set; }
        //string Form1_InDispute { get; set; }
        //string Form1_Insurer_IBCBranchID { get; set; }
        //string Form1_Insurer_IBCInsurerID { get; set; }
        
        //string Form1_PMSFields_PMSDocumentKey { get; set; }
        //string Form1_PMSFields_PMSPatientKey { get; set; }
        //string Form1_SigningAdjusterName_FirstName { get; set; }
        //string Form1_SigningAdjusterName_LastName { get; set; }
        //string Form1_SubmissionSource { get; set; }
        //string Form1_SubmissionTime { get; set; }
        [DispId(1414)]
        string Form1_VersionDate { get; set; }
        [DispId(1415)]
        string Form1_InsurerVersion { get; set; }
        [DispId(1416)]
        string Form1_EOB_EOBDocumentNumber { get; set; }
        [DispId(1417)]
        string Form1_EOB_DateRevised { get; set; }
        [DispId(1418)]
        string Form1_EOB_OCFVersion { get; set; }
    }

    [Serializable]
    [Guid("C6756C36-6AB9-4f2e-8AD5-C77B75855DF4")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponse : AdjusterResponseInterface
    {
        public AdjusterResponse()
        {
            OCF9_LineItem2 = new List<AdjusterResponseOCF9LineItem>();
        }

        #region AdjusterResponseInterface Members

        //Session keys
        private Queue mOCF18_Session_LineItem = new Queue();
        //Non Session Keys
        private Queue mOCF18_NonSession_LineItem = new Queue();


        //Insurer Totals

        private string mOCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF18_InsurerTotals_SubTotal_Approved;
        private string mOCF18_InsurerTotals_TotalCount_Approved;

        //Keys for mOCF21B
        private string mHCAI_Plan_Document_Number;
        //		private string Insurer_IBCInsurerID;
        //		private string Insurer_IBCBranchID;	
        private Queue mOCF21B_LineItem = new Queue();
        private string mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost;
        private string mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost;
        private string mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost;
        private string mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21B_InsurerTotals_MOH_Approved;
        private string mOCF21B_InsurerTotals_OtherInsurers_Approved;
        private string mOCF21B_InsurerTotals_AutoInsurerTotal_Approved;
        private string mOCF21B_InsurerTotals_GST_Approved_LineCost;
        private string mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        //		private string mOCF21B_InsurerTotals_PST_Approved;
        private string mOCF21B_InsurerTotals_PST_Approved_LineCost;
        private string mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        //		private string mOCF21B_InsurerTotals_GST_Approved;
        private string mOCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved;
        //		private string mOCF21B_InsurerTotals_Interest_Approved;
        private string mOCF21B_InsurerTotals_Interest_Approved_LineCost;
        private string mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

        //Keys for 21C
        private string mHCAI_OCF23_Document_Number;
        //		private string Insurer_IBCInsurerID;
        //		private string Insurer_IBCBranchID;
        private Queue mOCF21C_PAFReimbursable_LineItem = new Queue();
        private Queue mOCF21C_OtherReimbursable_LineItem = new Queue();
        private string mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost;
        private string mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost;
        private string mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost;
        private string mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21C_InsurerTotals_MOH_Approved;
        private string mOCF21C_InsurerTotals_OtherInsurers_Approved;
        private string mOCF21C_InsurerTotals_AutoInsurerTotal_Approved;
        //		private string mOCF21C_InsurerTotals_GST_Approved;
        private string mOCF21C_InsurerTotals_GST_Approved_LineCost;
        private string mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        //		private string mOCF21C_InsurerTotals_PST_Approved;
        private string mOCF21C_InsurerTotals_PST_Approved_LineCost;
        private string mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        //		private string mOCF21C_InsurerTotals_Interest_Approved;
        private string mOCF21C_InsurerTotals_Interest_Approved_LineCost;
        private string mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved;
        private string mOCF21C_InsurerTotals_SubTotalPreApproved_Approved;

        //Keys for mOCF22
        private string mOCF22_Insurer_IBCInsurerID;
        private string mOCF22_Insurer_IBCBranchID;
        private Queue mOCF22_LineItem = new Queue();
        private string mOCF22_InsurerTotals_MOH_Approved_LineCost;
        private string mOCF22_InsurerTotals_OtherInsurers_Approved_LineCost;
        private string mOCF22_InsurerTotals_AutoInsurerTotal_Approved;
        private string mOCF22_InsurerTotals_GST_Approved_LineCost;
        //		private string mOCF22_InsurerTotals_PST_Approved;
        private string mOCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved;
        private string mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        //		private string mOCF22_InsurerTotals_GST_Approved;
        private string mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF22_InsurerTotals_PST_Approved_LineCost;
        private string mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
        private string mOCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18;

        //Keys for mOCF23
        //		private string Insurer_IBCInsurerID;
        //		private string Insurer_IBCBranchID;
        private Queue mOCF23_OtherGS_LineItem = new Queue();
        private string mOCF23_InsurerTotals_AutoInsurerTotal_Approved;
        private string mOCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved;
        private string mOCF23_InsurerTotals_SubTotalPreApproved_Approved;
        private string mOCF23_InsurerSignature_ApplicantSignatureWaived;
        private string mOCF23_InsurerSignature_ResponseForOtherGoodsAndServices;
        private string mOCF23_InsurerSignature_PolicyInForceConfirmation;

        //Keys for mOCF9
        private string mOCFVersion;
        private string mParent_Document_Number;
        private string mOCF9_AttachmentsBeingSent;
        //		private string mOCF9_Header_DateOfAccident;

        private string mOCF9_Header_DateRevised;
        /*		private string mOCF9_Applicant_Name_FirstName;
                private string mOCF9_Applicant_Name_MiddleName;
                private string mOCF9_Applicant_Name_LastName;
                private string mOCF9_Applicant_DateOfBirth;
                private string mOCF9_Applicant_Gender;
                private string mOCF9_Applicant_Telephone_TelephoneNumber;
                private string mOCF9_Applicant_Telephone_TelephoneExtension;
                private string mOCF9_Applicant_Address_StreetAddress1;
                private string mOCF9_Applicant_Address_StreetAddress2;
                private string mOCF9_Applicant_Address_City;
                private string mOCF9_Applicant_Address_Province;
                private string mOCF9_Applicant_Address_PostalCode;
        */
        private Queue mOCF9_LineItem = new Queue();
        public IList<AdjusterResponseOCF9LineItem> OCF9_LineItem2 { get; private set; }

        private string mOCF9_AdditionalComments;
        private bool mIsOCF9Present;

        public string HCAI_Document_Number { get; set; }

        public string PMS_Document_Key { get; set; }

        public string Submission_Date { get; set; }

        public string Submission_Source { get; set; }

        public string Adjuster_Response_Date { get; set; }

        public string In_Dispute { get; set; }

        public string Document_Type { get; set; }

        public string Document_Status { get; set; }

        public string Attachments_Received_Date { get; set; }

        public string Claimant_First_Name { get; set; }

        public string Claimant_Middle_Name { get; set; }

        public string Claimant_Last_Name { get; set; }

        public string Claimant_Date_Of_Birth { get; set; }

        public string Claimant_Gender { get; set; }

        public string Claimant_Telephone { get; set; }

        public string Claimant_Extension { get; set; }

        public string Claimant_Address1 { get; set; }

        public string Claimant_Address2 { get; set; }

        public string Claimant_City { get; set; }

        public string Claimant_Province { get; set; }

        public string Claimant_Postal_Code { get; set; }

        public string Insurer_IBCInsurerID { get; set; }

        public string Insurer_IBCBranchID { get; set; }

        public string NameOfAdjusterOnPDF { get; set; }
        public string ApprovedByOnPDF { get; set; }

        public AdjusterResponseOCF18SessionLineItem OCF18_Session_LineItem
        {
            get
            {
                if (mOCF18_Session_LineItem.Count > 0)
                    return (AdjusterResponseOCF18SessionLineItem)mOCF18_Session_LineItem.Dequeue();
                return null;
            }
            set => mOCF18_Session_LineItem.Enqueue(value);
        }

        public AdjusterResponseOCF18NonSessionLineItem OCF18_NonSession_LineItem
        {
            get
            {
                if (mOCF18_NonSession_LineItem.Count > 0)
                    return (AdjusterResponseOCF18NonSessionLineItem)mOCF18_NonSession_LineItem.Dequeue();
                return null;
            }
            set => mOCF18_NonSession_LineItem.Enqueue(value);
        }

        public string OCF18_InsurerSignature_ApplicantSignatureWaived { get; set; }

        public string OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile { get; set; }
        public string OCF18_OtherHealthPractitioner_LicenseNumber { get; set; }

        public string OCF18_InsurerTotals_MOH_Approved_LineCost { get; set; }

        public string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF18_InsurerTotals_OtherInsurers_Approved_LineCost { get; set; }

        public string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF18_InsurerTotals_OtherInsurers_Approved { get; set; }

        public string OCF18_InsurerTotals_AutoInsurerTotal_Approved { get; set; }

        public string OCF18_InsurerTotals_GST_Approved { get; set; }

        public string OCF18_InsurerTotals_GST_Approved_LineCost { get; set; }

        public string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF18_InsurerTotals_PST_Approved { get; set; }

        public string OCF18_InsurerTotals_PST_Approved_LineCost { get; set; }

        public string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF18_InsurerTotals_SubTotal_Approved
        {
            get => mOCF18_InsurerTotals_SubTotal_Approved;
            set => mOCF18_InsurerTotals_SubTotal_Approved = value;
        }

        public string OCF18_InsurerTotals_TotalCount_Approved
        {
            get => mOCF18_InsurerTotals_TotalCount_Approved;
            set => mOCF18_InsurerTotals_TotalCount_Approved = value;
        }

        public string OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation { get; set; }
        
        public string HCAI_Plan_Document_Number
        {
            get => mHCAI_Plan_Document_Number;
            set => mHCAI_Plan_Document_Number = value;
        }

        public AdjusterResponseOCF21BLineItem OCF21B_LineItem
        {
            get
            {
                if (mOCF21B_LineItem.Count > 0)
                    return (AdjusterResponseOCF21BLineItem)mOCF21B_LineItem.Dequeue();
                return null;
            }
            set => mOCF21B_LineItem.Enqueue(value);
        }

        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost
        {
            get => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost;
            set => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = value;
        }

        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost;
            set => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost;
            set => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation { get; set; }
        public string OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation { get; set; }
        public string OCF21B_Payee_FacilityIsPayee { get; set; }
        public string OCF21B_InsurerSignature_ClaimFormReceived { get; set; }
        public string OCF21B_InsurerSignature_ClaimFormReceivedDate { get; set; }

        public string OCF21B_InsurerTotals_MOH_Approved
        {
            get => mOCF21B_InsurerTotals_MOH_Approved;
            set => mOCF21B_InsurerTotals_MOH_Approved = value;
        }

        public string OCF21B_InsurerTotals_OtherInsurers_Approved
        {
            get => mOCF21B_InsurerTotals_OtherInsurers_Approved;
            set => mOCF21B_InsurerTotals_OtherInsurers_Approved = value;
        }

        public string OCF21B_InsurerTotals_AutoInsurerTotal_Approved
        {
            get => mOCF21B_InsurerTotals_AutoInsurerTotal_Approved;
            set => mOCF21B_InsurerTotals_AutoInsurerTotal_Approved = value;
        }

        /*		public string OCF21B_InsurerTotals_GST_Approved_LineCost
                {
                    get
                    {
                        return mOCF21B_InsurerTotals_GST_Approved_LineCost;
                    }
                    set
                    {
                        mOCF21B_InsurerTotals_GST_Approved_LineCost = value;
                    }
                }
        */
        public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        /*		public string OCF21B_InsurerTotals_PST_Approved
                {
                    get
                    {
                        return mOCF21B_InsurerTotals_PST_Approved;
                    }
                    set
                    {
                        mOCF21B_InsurerTotals_PST_Approved = value;
                    }
                }
        */
        public string OCF21B_InsurerTotals_PST_Approved_LineCost
        {
            get => mOCF21B_InsurerTotals_PST_Approved_LineCost;
            set => mOCF21B_InsurerTotals_PST_Approved_LineCost = value;
        }

        public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        /*		public string OCF21B_InsurerTotals_GST_Approved
                {
                    get
                    {
                        return mOCF21B_InsurerTotals_GST_Approved;
                    }
                    set
                    {
                        mOCF21B_InsurerTotals_GST_Approved = value;
                    }
                }
        */
        public string OCF21B_InsurerTotals_GST_Approved_LineCost
        {
            get => mOCF21B_InsurerTotals_GST_Approved_LineCost;
            set => mOCF21B_InsurerTotals_GST_Approved_LineCost = value;
        }

        public string OCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved
        {
            get => mOCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved;
            set => mOCF21B_InsurerTotals_SubTotalProposedGoodsAndServices_Approved = value;
        }

        /*		public string OCF21B_InsurerTotals_Interest_Approved
                {
                    get
                    {
                        return mOCF21B_InsurerTotals_Interest_Approved;
                    }
                    set
                    {
                        mOCF21B_InsurerTotals_Interest_Approved = value;
                    }
                }
        */
        public string OCF21B_InsurerTotals_Interest_Approved_LineCost
        {
            get => mOCF21B_InsurerTotals_Interest_Approved_LineCost;
            set => mOCF21B_InsurerTotals_Interest_Approved_LineCost = value;
        }

        public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string HCAI_OCF23_Document_Number
        {
            get => mHCAI_OCF23_Document_Number;
            set => mHCAI_OCF23_Document_Number = value;
        }

        public AdjusterResponseOCF21CPAFReimbursableLineItem OCF21C_PAFReimbursable_LineItem
        {
            get
            {
                if (mOCF21C_PAFReimbursable_LineItem.Count > 0)
                    return (AdjusterResponseOCF21CPAFReimbursableLineItem)mOCF21C_PAFReimbursable_LineItem.Dequeue();
                return null;
            }
            set => mOCF21C_PAFReimbursable_LineItem.Enqueue(value);
        }

        public AdjusterResponseOCF21COtherReimbursableLineItem OCF21C_OtherReimbursable_LineItem
        {
            get
            {
                if (mOCF21C_OtherReimbursable_LineItem.Count > 0)
                    return (AdjusterResponseOCF21COtherReimbursableLineItem)mOCF21C_OtherReimbursable_LineItem.Dequeue();
                return null;
            }
            set => mOCF21C_OtherReimbursable_LineItem.Enqueue(value);
        }

        public string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost
        {
            get => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost;
            set => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = value;
        }

        public string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost;
            set => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost;
            set => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost { get; set; }
        public string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost { get; set; }
        public string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost { get; set; }
        public string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }

        public string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }

        public string OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }

        public string OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation { get; set; }
        public string OCF21C_PAFReimbursableFees_AdjusterResponseExplanation { get; set; }
        public string OCF21C_Payee_FacilityIsPayee { get; set; }
        public string OCF21C_InsurerSignature_ClaimFormReceived { get; set; }
        public string OCF21C_InsurerSignature_ClaimFormReceivedDate { get; set; }

        public string OCF21C_InsurerTotals_MOH_Approved
        {
            get => mOCF21C_InsurerTotals_MOH_Approved;
            set => mOCF21C_InsurerTotals_MOH_Approved = value;
        }

        public string OCF21C_InsurerTotals_OtherInsurers_Approved
        {
            get => mOCF21C_InsurerTotals_OtherInsurers_Approved;
            set => mOCF21C_InsurerTotals_OtherInsurers_Approved = value;
        }

        public string OCF21C_InsurerTotals_AutoInsurerTotal_Approved
        {
            get => mOCF21C_InsurerTotals_AutoInsurerTotal_Approved;
            set => mOCF21C_InsurerTotals_AutoInsurerTotal_Approved = value;
        }

        /*		public string OCF21C_InsurerTotals_GST_Approved
                {
                    get
                    {
                        return mOCF21C_InsurerTotals_GST_Approved;
                    }
                    set
                    {
                        mOCF21C_InsurerTotals_GST_Approved = value;
                    }
                }
        */
        public string OCF21C_InsurerTotals_GST_Approved_LineCost
        {
            get => mOCF21C_InsurerTotals_GST_Approved_LineCost;
            set => mOCF21C_InsurerTotals_GST_Approved_LineCost = value;
        }

        public string OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        /*		public string OCF21C_InsurerTotals_PST_Approved
                {
                    get
                    {
                        return mOCF21C_InsurerTotals_PST_Approved;
                    }
                    set
                    {
                        mOCF21C_InsurerTotals_PST_Approved = value;
                    }
                }
        */
        public string OCF21C_InsurerTotals_PST_Approved_LineCost
        {
            get => mOCF21C_InsurerTotals_PST_Approved_LineCost;
            set => mOCF21C_InsurerTotals_PST_Approved_LineCost = value;
        }

        public string OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        /*		public string OCF21C_InsurerTotals_Interest_Approved
                {
                    get
                    {
                        return mOCF21C_InsurerTotals_Interest_Approved;
                    }
                    set
                    {
                        mOCF21C_InsurerTotals_Interest_Approved = value;
                    }
                }
        */
        public string OCF21C_InsurerTotals_Interest_Approved_LineCost
        {
            get => mOCF21C_InsurerTotals_Interest_Approved_LineCost;
            set => mOCF21C_InsurerTotals_Interest_Approved_LineCost = value;
        }

        public string OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved
        {
            get => mOCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved;
            set => mOCF21C_InsurerTotals_SubTotalOtherReimbursibleGoodsAndServices_Approved = value;
        }

        public string OCF21C_InsurerTotals_SubTotalPreApproved_Approved
        {
            get => mOCF21C_InsurerTotals_SubTotalPreApproved_Approved;
            set => mOCF21C_InsurerTotals_SubTotalPreApproved_Approved = value;
        }

        public string OCF22_Insurer_IBCInsurerID
        {
            get => mOCF22_Insurer_IBCInsurerID;
            set => mOCF22_Insurer_IBCInsurerID = value;
        }

        public string OCF22_Insurer_IBCBranchID
        {
            get => mOCF22_Insurer_IBCBranchID;
            set => mOCF22_Insurer_IBCBranchID = value;
        }

        public AdjusterResponseOCF22LineItem OCF22_LineItem
        {
            get
            {
                if (mOCF22_LineItem.Count > 0)
                    return (AdjusterResponseOCF22LineItem)mOCF22_LineItem.Dequeue();
                return null;
            }
            set => mOCF22_LineItem.Enqueue(value);
        }

        public string OCF22_InsurerTotals_MOH_Approved_LineCost
        {
            get => mOCF22_InsurerTotals_MOH_Approved_LineCost;
            set => mOCF22_InsurerTotals_MOH_Approved_LineCost = value;
        }

        public string OCF22_InsurerTotals_OtherInsurers_Approved_LineCost
        {
            get => mOCF22_InsurerTotals_OtherInsurers_Approved_LineCost;
            set => mOCF22_InsurerTotals_OtherInsurers_Approved_LineCost = value;
        }

        public string OCF22_InsurerTotals_AutoInsurerTotal_Approved
        {
            get => mOCF22_InsurerTotals_AutoInsurerTotal_Approved;
            set => mOCF22_InsurerTotals_AutoInsurerTotal_Approved = value;
        }

        public string OCF22_InsurerTotals_GST_Approved_LineCost
        {
            get => mOCF22_InsurerTotals_GST_Approved_LineCost;
            set => mOCF22_InsurerTotals_GST_Approved_LineCost = value;
        }

        /*		public string OCF22_InsurerTotals_PST_Approved
                {
                    get
                    {
                        return mOCF22_InsurerTotals_PST_Approved;
                    }
                    set
                    {
                        mOCF22_InsurerTotals_PST_Approved = value;
                    }
                }
        */
        public string OCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved
        {
            get => mOCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved;
            set => mOCF22_InsurerTotals_SubTotalProposedGoodsAndServices_Approved = value;
        }

        public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        /*		public string OCF22_InsurerTotals_GST_Approved
                {
                    get
                    {
                        return mOCF22_InsurerTotals_GST_Approved;
                    }
                    set
                    {
                        mOCF22_InsurerTotals_GST_Approved = value;
                    }
                }
        */
        public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF22_InsurerTotals_PST_Approved_LineCost
        {
            get => mOCF22_InsurerTotals_PST_Approved_LineCost;
            set => mOCF22_InsurerTotals_PST_Approved_LineCost = value;
        }

        public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        public string OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18
        {
            get => mOCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18;
            set => mOCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18 = value;
        }


        public AdjusterResponseOCF23OtherGSLineItem OCF23_OtherGS_LineItem
        {
            get
            {
                if (mOCF23_OtherGS_LineItem.Count > 0)
                    return (AdjusterResponseOCF23OtherGSLineItem)mOCF23_OtherGS_LineItem.Dequeue();
                return null;
            }
            set => mOCF23_OtherGS_LineItem.Enqueue(value);
        }

        public string OCF23_InsurerTotals_AutoInsurerTotal_Approved
        {
            get => mOCF23_InsurerTotals_AutoInsurerTotal_Approved;
            set => mOCF23_InsurerTotals_AutoInsurerTotal_Approved = value;
        }

        public string OCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved
        {
            get => mOCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved;
            set => mOCF23_InsurerTotals_SubTotalOtherGoodsandServices_Approved = value;
        }

        public string OCF23_InsurerTotals_SubTotalPreApproved_Approved
        {
            get => mOCF23_InsurerTotals_SubTotalPreApproved_Approved;
            set => mOCF23_InsurerTotals_SubTotalPreApproved_Approved = value;
        }

        public string OCF23_OtherGoodsAndServices_AdjusterResponseExplanation { get; set; }

        public string OCF23_InsurerSignature_ApplicantSignatureWaived
        {
            get => mOCF23_InsurerSignature_ApplicantSignatureWaived;
            set => mOCF23_InsurerSignature_ApplicantSignatureWaived = value;
        }

        public string OCF23_InsurerSignature_ResponseForOtherGoodsAndServices
        {
            get => mOCF23_InsurerSignature_ResponseForOtherGoodsAndServices;
            set => mOCF23_InsurerSignature_ResponseForOtherGoodsAndServices = value;
        }

        public string OCF23_InsurerSignature_PolicyInForceConfirmation
        {
            get => mOCF23_InsurerSignature_PolicyInForceConfirmation;
            set => mOCF23_InsurerSignature_PolicyInForceConfirmation = value;
        }

        public string OCF9_OCFVersion
        {
            get => mOCFVersion;
            set => mOCFVersion = value;
        }

        public string Parent_Document_Number
        {
            get => mParent_Document_Number;
            set => mParent_Document_Number = value;
        }


        public string OCF9_AttachmentsBeingSent
        {
            get => mOCF9_AttachmentsBeingSent;
            set => mOCF9_AttachmentsBeingSent = value;
        }
        /*
                public string OCF9_Header_ClaimNumber
                {
                    get
                    {
                        return mOCF9_Header_ClaimNumber;
                    }
                    set
                    {
                        mOCF9_Header_ClaimNumber = value;
                    }
                }

                public string OCF9_Header_DateOfAccident
                {
                    get
                    {
                        return mOCF9_Header_DateOfAccident;
                    }
                    set
                    {
                        mOCF9_Header_DateOfAccident = value;
                    }
                }
        */
        public string OCF9_Header_DateRevised
        {
            get => mOCF9_Header_DateRevised;
            set => mOCF9_Header_DateRevised = value;
        }
        /*
                public string OCF9_Applicant_Name_FirstName
                {
                    get
                    {
                        return mOCF9_Applicant_Name_FirstName;
                    }
                    set
                    {
                        mOCF9_Applicant_Name_FirstName = value;
                    }
                }

                public string OCF9_Applicant_Name_MiddleName
                {
                    get
                    {
                        return mOCF9_Applicant_Name_MiddleName;
                    }
                    set
                    {
                        mOCF9_Applicant_Name_MiddleName = value;
                    }
                }

                public string OCF9_Applicant_Name_LastName
                {
                    get
                    {
                        return mOCF9_Applicant_Name_LastName;
                    }
                    set
                    {
                        mOCF9_Applicant_Name_LastName = value;
                    }
                }

                public string OCF9_Applicant_DateOfBirth
                {
                    get
                    {
                        return mOCF9_Applicant_DateOfBirth;
                    }
                    set
                    {
                        mOCF9_Applicant_DateOfBirth = value;
                    }
                }

                public string OCF9_Applicant_Gender
                {
                    get
                    {
                        return mOCF9_Applicant_Gender;
                    }
                    set
                    {
                        mOCF9_Applicant_Gender = value;
                    }
                }

                public string OCF9_Applicant_Telephone_TelephoneNumber
                {
                    get
                    {
                        return mOCF9_Applicant_Telephone_TelephoneNumber;
                    }
                    set
                    {
                        mOCF9_Applicant_Telephone_TelephoneNumber = value;
                    }
                }

                public string OCF9_Applicant_Telephone_TelephoneExtension
                {
                    get
                    {
                        return mOCF9_Applicant_Telephone_TelephoneExtension;
                    }
                    set
                    {
                        mOCF9_Applicant_Telephone_TelephoneExtension = value;
                    }
                }

                public string OCF9_Applicant_Address_StreetAddress1
                {
                    get
                    {
                        return mOCF9_Applicant_Address_StreetAddress1;
                    }
                    set
                    {
                        mOCF9_Applicant_Address_StreetAddress1 = value;
                    }
                }

                public string OCF9_Applicant_Address_StreetAddress2
                {
                    get
                    {
                        return mOCF9_Applicant_Address_StreetAddress2;
                    }
                    set
                    {
                        mOCF9_Applicant_Address_StreetAddress2 = value;
                    }
                }

                public string OCF9_Applicant_Address_City
                {
                    get
                    {
                        return mOCF9_Applicant_Address_City;
                    }
                    set
                    {
                        mOCF9_Applicant_Address_City = value;
                    }
                }

                public string OCF9_Applicant_Address_Province
                {
                    get
                    {
                        return mOCF9_Applicant_Address_Province;
                    }
                    set
                    {
                        mOCF9_Applicant_Address_Province = value;
                    }
                }

                public string OCF9_Applicant_Address_PostalCode
                {
                    get
                    {
                        return mOCF9_Applicant_Address_PostalCode;
                    }
                    set
                    {
                        mOCF9_Applicant_Address_PostalCode = value;
                    }
                }
        */

        public AdjusterResponseOCF9LineItem OCF9_LineItem
        {
            get
            {
                if (mOCF9_LineItem.Count > 0)
                    return (AdjusterResponseOCF9LineItem)mOCF9_LineItem.Dequeue();
                return null;
            }
            set => mOCF9_LineItem.Enqueue(value);
        }

        public string OCF9_AdditionalComments
        {
            get => mOCF9_AdditionalComments;
            set => mOCF9_AdditionalComments = value;
        }

        public bool IsOCF9Present
        {
            get => mIsOCF9Present;
            set => mIsOCF9Present = value;
        }

        //VERSION_2.0_beta
        public string SigningAdjuster_FirstName { set; get; }
        public string SigningAdjuster_LastName { set; get; }

        //Form1 Keys
        public string Form1_AdjusterResponseTime { get; set; }
        public string Form1_ArchivalStatus { get; set; }
        public string Form1_AttachmentsReceivedDate { get; set; }
        public string Form1_BranchVersion { get; set; }
        public string Form1_Costs_Approved_AdjusterResponseExplanation { get; set; }
        public string Form1_Costs_Approved_Benefit { get; set; }
        public string Form1_Costs_Approved_CalculatedBenefit { get; set; }
        public string Form1_Costs_Approved_ReasonCode { get; set; }
        public string Form1_Costs_Approved_ReasonDescription { get; set; }
        public string Form1_DocumentVersion { get; set; }
        public string Form1_EOB_EOBAdditionalComments { get; set; }
        public string Form1_EOB_EOBDocumentNumber { get; set; }
        public string Form1_EOB_DateRevised { get; set; }
        public string Form1_EOB_OCFVersion { get; set; }
        public string Form1_FacilityVersion { get; set; }
        public string Form1_VersionDate { get; set; }
        public string Form1_InsurerVersion { get; set; }

        public string Applicant_Address_City { get; set; }
        public string Applicant_Address_PostalCode { get; set; }
        public string Applicant_Address_Province { get; set; }
        public string Applicant_Address_StreetAddress1 { get; set; }
        public string Applicant_Address_StreetAddress2 { get; set; }
        public string Applicant_DateOfBirth { get; set; }
        public string Applicant_Gender { get; set; }
        public string Applicant_Name_FirstName { get; set; }
        public string Applicant_Name_LastName { get; set; }
        public string Applicant_Name_MiddleName { get; set; }
        public string Applicant_TelephoneNumber { get; set; }
        public string Applicant_TelephoneExtension { get; set; }
        public string AdditionalComments { get; set; }
        public string AttachmentsBeingSent { get; set; }
        
        public string Form1_AssessmentDate { get; set; }
        public string Form1_Assessor_DateSigned { get; set; }
        public string Form1_Assessor_Email { get; set; }
        public string Form1_Assessor_Facility_Name { get; set; }
        public string Form1_Assessor_FacilityRegistryID { get; set; }
        public string Form1_Assessor_IsSignatureOnFile { get; set; }
        public string Form1_Assessor_Occupation { get; set; }
        public string Form1_Assessor_ProviderRegistryID { get; set; }

        public string Form1_AttachmentCount { get; set; }
        public string Form1_Costs_Assessed_Benefit { get; set; }
        public string Form1_CurrentMonthlyAllowance { get; set; }

        public string Form1_FirstAssessment { get; set; }
        public string Form1_FormVersion { get; set; }
        public string Form1_Header_ClaimNumber { get; set; }
        public string Form1_Header_DateOfAccident { get; set; }
        public string Form1_Header_PolicyNumber { get; set; }

        public string Form1_Insurer_Name { get; set; }
        public string Form1_Insurer_PolicyHolder_IsSameAsApplicant { get; set; }
        public string Form1_Insurer_PolicyHolder_Name_FirstName { get; set; }
        public string Form1_Insurer_PolicyHolder_Name_LastName { get; set; }
        public string Form1_InsurerExamination { get; set; }
        
        public string Form1_LastAssessmentDate { get; set; }
        public string Form1_PMSFields_PMSPatientKey { get; set; }
        
        public List<AdjusterResponseForm1AcsLineItem> Form1AcsLineItem = new List<AdjusterResponseForm1AcsLineItem>();
        public List<AdjusterResponseForm1ApprovedCostLineItem> Form1ApprovedCostLineItem = new List<AdjusterResponseForm1ApprovedCostLineItem>();
        public List<AdjusterResponseForm1AssessedCostLineItem> Form1AssessedCostLineItem = new List<AdjusterResponseForm1AssessedCostLineItem>();
        

        //end
        #endregion

        #region IDataType Members
        private int mMessageType;
        public int MessageType
        {
            get => mMessageType;
            set => mMessageType = value;
        }

        private string mMessageDate;
        private string mMessageTime;
        public string MessageDate
        {
            get => mMessageDate;
            set => mMessageDate = value;
        }

        public string MessageTime
        {
            get => mMessageTime;
            set => mMessageTime = value;
        }

        private string mMessageOCFType;
        public string MessageOCFType
        {
            get => mMessageOCFType;
            set => mMessageOCFType = value;
        }

        public virtual XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            var el = doc.CreateElement("root");
            doc.AppendChild(el);

            el = doc.CreateElement("basicmessage");

            el.SetAttribute("MessageType", MessageType.ToString());
            el.SetAttribute("MessageDate", MessageDate);
            el.SetAttribute("MessageTime", MessageTime);
            el.SetAttribute("MessageOCFType", MessageOCFType);

            doc.DocumentElement.AppendChild(el);

            return doc;
        }

        public virtual int ReadXMLDocument(XmlDocument doc)
        {
            var result = 0;
            var el = (XmlElement)doc.DocumentElement.FirstChild;

            if (el.Name == "basicmessage")
            {
                mMessageType = Convert.ToInt32(el.GetAttribute("MessageType"));
                mMessageDate = el.GetAttribute("MessageDate");
                mMessageTime = el.GetAttribute("MessageTime");
                mMessageOCFType = el.GetAttribute("MessageOCFType");
            }
            else
                result = -1;

            return result;
        }
        #endregion
    }


    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF18 Session items
    /// </summary>
    [Guid("AD171B81-6E79-43d0-BF3B-9D927E7BB63A")]
    public interface AdjusterResponseOCF18SessionLineItemInterface
    {
        [DispId(2000)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey { set; get; }

        [DispId(2001)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost { set; get; }

        [DispId(2002)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count { set; get; }

        [DispId(2003)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost { set; get; }

        [DispId(2004)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode { set; get; }

        [DispId(2005)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc { set; get; }

        [DispId(2006)]
        string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc { set; get; }
    }


    [Serializable]
    [Guid("0EDA6F36-3BD3-455f-8E85-BF230390EB5D")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF18SessionLineItem : AdjusterResponseOCF18SessionLineItemInterface
    {
        #region AdjusterResponseOCF18SessionLineItemInterface Members

        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey;
        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost;
        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count;
        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost;
        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode;
        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc;
        private string mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc;

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_PMSGSKey = value;
        }

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost = value;
        }

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count = value;
        }

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost = value;
        }

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCode = value;
        }

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc = value;
        }

        public string OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc
        {
            get => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc;
            set => mOCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc = value;
        }

        #endregion

    }


    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF18 Non Session items
    /// </summary>
    [Guid("B00A9959-3D2A-4d32-BB35-3569DB7368AA")]
    public interface AdjusterResponseOCF18NonSessionLineItemInterface
    {

        [DispId(2010)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey { set; get; }

        [DispId(2011)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST { set; get; }

        [DispId(2012)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST { set; get; }

        [DispId(2013)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost { set; get; }

        [DispId(2014)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count { set; get; }

        [DispId(2015)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost { set; get; }

        [DispId(2016)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode { set; get; }

        [DispId(2017)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc { set; get; }

        [DispId(2018)]
        string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc { set; get; }


    }


    [Serializable]
    [Guid("C8C9F96D-FCB1-497f-A07C-75CFA1EC8888")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF18NonSessionLineItem : AdjusterResponseOCF18NonSessionLineItemInterface
    {
        #region AdjusterResponseOCF18NonSessionLineItemInterface Members
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc;
        private string mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc;

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_PMSGSKey = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCode = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeDesc = value;
        }

        public string OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc
        {
            get => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc;
            set => mOCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc = value;
        }

        #endregion

    }


    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF21B items
    /// </summary>
    [Guid("C46C061E-4700-442b-9EC3-0FF3C680FC07")]
    public interface AdjusterResponseOCF21BLineItemInterface
    {
        [DispId(2020)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey { set; get; }

        [DispId(2021)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST { set; get; }

        [DispId(2022)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST { set; get; }

        [DispId(2023)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost { set; get; }

        [DispId(2024)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(2025)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(2026)]
        string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }
    }

    [Serializable]
    [Guid("060E8B8B-6EFE-4e2d-A9C0-3030549441AB")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF21BLineItem : AdjusterResponseOCF21BLineItemInterface
    {
        #region AdjusterResponseOCF21BLineItemInterface Members

        private string mOCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey;
        private string mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST;
        private string mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST;
        private string mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost;
        private string mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;


        public string OCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_PMSGSKey = value;
        }

        public string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_GST = value;
        }

        public string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_PST = value;
        }

        public string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_LineCost = value;
        }

        public string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21B_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }
        #endregion
    }

    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF21C PAFReimbursable items
    /// </summary>
    [Guid("C2003013-D8E0-4fc0-8027-F06030108399")]
    public interface AdjusterResponseOCF21CPAFReimbursableLineItemInterface
    {
        [DispId(2030)]
        string OCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey { set; get; }

        [DispId(2031)]
        string OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost { set; get; }

        [DispId(2032)]
        string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode { set; get; }

        [DispId(2033)]
        string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(2034)]
        string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode { set; get; }

        [DispId(2035)]
        string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

    }

    [Serializable]
    [Guid("8C5D0F27-B442-41ee-9C3B-02D1DC842C9A")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF21CPAFReimbursableLineItem : AdjusterResponseOCF21CPAFReimbursableLineItemInterface
    {
        #region AdjusterResponseOCF21CPAFReimbursableLineItemInterface Members
        private string mOCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey;
        private string mOCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost;
        private string mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode;
        private string mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode;
        private string mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

        public string OCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey
        {
            get => mOCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey;
            set => mOCF21C_PAFReimbursibleFees_Items_Item_PMSGSKey = value;
        }

        public string OCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost
        {
            get => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost;
            set => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_LineCost = value;
        }

        public string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode
        {
            get => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode;
            set => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode
        {
            get => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode;
            set => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ResonCodeGroup_OtherReasonCode = value;
        }

        public string OCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_PAFReimbursibleFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        #endregion
    }

    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF21C OtherReimbursable items
    /// </summary>
    [Guid("2B64D97C-8ECE-47ec-8D63-C9E20202DE13")]
    public interface AdjusterResponseOCF21COtherReimbursableLineItemInterface
    {
        [DispId(2040)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey { set; get; }

        [DispId(2041)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST { set; get; }

        [DispId(2042)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST { set; get; }

        [DispId(2043)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost { set; get; }

        [DispId(2044)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(2045)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(2046)]
        string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }
    }

    [Serializable]
    [Guid("D0B6A23C-06D5-4ce6-AC32-8A586471AA0C")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF21COtherReimbursableLineItem : AdjusterResponseOCF21COtherReimbursableLineItemInterface
    {
        #region AdjusterResponseOCF21COtherReimbursableLineItemInterface Members
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey;
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST;
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST;
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost;
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_PMSGSKey = value;
        }

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST = value;
        }

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST = value;
        }

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost = value;
        }

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        #endregion
    }



    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF22 OtherReimbursable items
    /// </summary>
    [Guid("998D588F-8492-4382-9583-C7B1C1BE953D")]
    public interface AdjusterResponseOCF22LineItemInterface
    {
        [DispId(2050)]
        string OCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey { set; get; }

        [DispId(2051)]
        string OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST { set; get; }

        [DispId(2052)]
        string OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST { set; get; }

        [DispId(2053)]
        string OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost { set; get; }

        [DispId(2054)]
        string OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup { set; get; }

        [DispId(2055)]
        string OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc { set; get; }

        [DispId(2056)]
        string OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc { set; get; }
    }

    [Serializable]
    [Guid("6F550A58-40D8-4399-84BE-B1EFA01DCE4F")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF22LineItem : AdjusterResponseOCF22LineItemInterface
    {
        #region AdjusterResponseOCF22LineItemInterface Members
        private string mOCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey;
        private string mOCF22_ProposedGoodsAndServices_Items_Item_Approved_GST;
        private string mOCF22_ProposedGoodsAndServices_Items_Item_Approved_PST;
        private string mOCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost;
        private string mOCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup;
        private string mOCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc;
        private string mOCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc;

        public string OCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_PMSGSKey = value;
        }

        public string OCF22_ProposedGoodsAndServices_Items_Item_Approved_GST
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_GST;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_GST = value;
        }

        public string OCF22_ProposedGoodsAndServices_Items_Item_Approved_PST
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_PST;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_PST = value;
        }

        public string OCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_LineCost = value;
        }

        public string OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeGroup = value;
        }

        public string OCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_ReasonCodeDesc = value;
        }

        public string OCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc
        {
            get => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc;
            set => mOCF22_ProposedGoodsAndServices_Items_Item_Approved_OtherReasonCodeDesc = value;
        }

        #endregion
    }

    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF23 OtherReimbursable items
    /// </summary>
    [Guid("A8E90DE5-A168-4448-A3CD-754CB558C467")]
    public interface AdjusterResponseOCF23OtherGSLineItemInterface
    {
        [DispId(2060)]
        string OCF23_OtherGoodsandServices_Items_Item_PMSGSKey { set; get; }

        [DispId(2061)]
        string OCF23_OtherGoodsandServices_Items_Approved_LineCost { set; get; }

        [DispId(2062)]
        string OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode { set; get; }

        [DispId(2063)]
        string OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc { set; get; }

        [DispId(2064)]
        string OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc { set; get; }

        //		[DispId(2065)]
        //		string OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber {set; get;}

    }

    [Serializable]
    [Guid("A0CCFFED-2F05-4b74-AFDC-F3557B99DC39")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF23OtherGSLineItem : AdjusterResponseOCF23OtherGSLineItemInterface
    {
        #region AdjusterResponseOCF23OtherGSLineItemInterface Members
        //		private string mOCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber;
        private string mOCF23_OtherGoodsandServices_Items_Item_PMSGSKey;
        private string mOCF23_OtherGoodsandServices_Items_Approved_LineCost;
        private string mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode;
        private string mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc;
        private string mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

        /*		public string OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber
                {
                    get
                    {
                        return mOCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber;
                    }
                    set
                    {
                        mOCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber = value;
                    }
                }
        */
        public string OCF23_OtherGoodsandServices_Items_Item_PMSGSKey
        {
            get => mOCF23_OtherGoodsandServices_Items_Item_PMSGSKey;
            set => mOCF23_OtherGoodsandServices_Items_Item_PMSGSKey = value;
        }

        public string OCF23_OtherGoodsandServices_Items_Approved_LineCost
        {
            get => mOCF23_OtherGoodsandServices_Items_Approved_LineCost;
            set => mOCF23_OtherGoodsandServices_Items_Approved_LineCost = value;
        }

        public string OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode
        {
            get => mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode;
            set => mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCode = value;
        }

        public string OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc
        {
            get => mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc;
            set => mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_ReasonCodeDesc = value;
        }

        public string OCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc
        {
            get => mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            set => mOCF23_OtherGoodsandServices_Items_Approved_ReasonCodeGroup_OtherReasonCodeDesc = value;
        }

        #endregion
    }


    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for OCF9 items
    /// </summary>
    [Guid("B93F671A-3E40-4b06-8C7A-C41EA2845702")]
    public interface AdjusterResponseOCF9LineItemInterface
    {
        /*		[DispId(2070)]
                string OCF9_GoodsAndServicess_Code {set; get;}

                [DispId(2071)]
                string OCF9_GoodsAndServicess_AmountClaimed {set; get;}

                [DispId(2072)]
                string OCF9_GoodsAndServicess_AmountPayable {set; get;}

                [DispId(2073)]
                string OCF9_GoodsAndServicess_InterestPayable {set; get;}

                [DispId(2074)]
                string OCF9_GoodsAndServicess_ReasonCode {set; get;}

                [DispId(2075)]
                string OCF9_GoodsAndServicess_ReasonCodeDesc {set; get;}
        */
        [DispId(2076)]
        string OCF9_GoodsAndServices_Items_Item_InterestPayable { set; get; }

        [DispId(2077)]
        string OCF9_GoodsAndServices_Items_Item_ReferenceNumber { set; get; }

    }

    [Serializable]
    [Guid("DC43D111-8740-407c-A436-C40E23BF6761")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AdjusterResponseOCF9LineItem : AdjusterResponseOCF9LineItemInterface
    {
        #region AdjusterResponseOCF9LineItemInterface Members
        /*		private string mOCF9_GoodsAndServicess_Code;
		private string mOCF9_GoodsAndServicess_AmountClaimed;
		private string mOCF9_GoodsAndServicess_AmountPayable;
		private string mOCF9_GoodsAndServicess_InterestPayable;
		private string mOCF9_GoodsAndServicess_ReasonCode;
		private string mOCF9_GoodsAndServicess_ReasonCodeDesc;
*/
        private string mOCF9_GoodsAndServices_Items_Item_InterestPayable;
        private string mOCF9_GoodsAndServices_Items_Item_ReferenceNumber;


        public string OCF9_GoodsAndServices_Items_Item_ReferenceNumber
        {
            get => mOCF9_GoodsAndServices_Items_Item_ReferenceNumber;
            set => mOCF9_GoodsAndServices_Items_Item_ReferenceNumber = value;
        }

        public string OCF9_GoodsAndServices_Items_Item_InterestPayable
        {
            get => mOCF9_GoodsAndServices_Items_Item_InterestPayable;
            set => mOCF9_GoodsAndServices_Items_Item_InterestPayable = value;
        }
        /*
                public string OCF9_GoodsAndServicess_AmountClaimed
                {
                    get
                    {
                        return mOCF9_GoodsAndServicess_AmountClaimed;
                    }
                    set
                    {
                        mOCF9_GoodsAndServicess_AmountClaimed = value;
                    }
                }

                public string OCF9_GoodsAndServicess_AmountPayable
                {
                    get
                    {
                        return mOCF9_GoodsAndServicess_AmountPayable;
                    }
                    set
                    {
                        mOCF9_GoodsAndServicess_AmountPayable = value;
                    }
                }

                public string OCF9_GoodsAndServicess_InterestPayable
                {
                    get
                    {
                        return mOCF9_GoodsAndServicess_InterestPayable;
                    }
                    set
                    {
                        mOCF9_GoodsAndServicess_InterestPayable = value;
                    }
                }

                public string OCF9_GoodsAndServicess_ReasonCode
                {
                    get
                    {
                        return mOCF9_GoodsAndServicess_ReasonCode;
                    }
                    set
                    {
                        mOCF9_GoodsAndServicess_ReasonCode = value;
                    }
                }

                public string OCF9_GoodsAndServicess_ReasonCodeDesc
                {
                    get
                    {
                        return mOCF9_GoodsAndServicess_ReasonCodeDesc;
                    }
                    set
                    {
                        mOCF9_GoodsAndServicess_ReasonCodeDesc = value;
                    }
                }

                public string OCF9_GoodsAndServicess_OtherReasonCodeDesc
                {
                    get
                    {
                        return mOCF9_GoodsAndServicess_OtherReasonCodeDesc;
                    }
                    set
                    {
                        mOCF9_GoodsAndServicess_OtherReasonCodeDesc = value;
                    }
                }
        */
        #endregion
    }


    //--------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Adjuster Response Repeatable keys for Form1 items
    /// </summary>
    public class AdjusterResponseForm1AcsLineItem
    {
        #region AdjusterResponseForm1AcsLineItem Members
        public string AttendantCareServices_Item_ACSItemID { get; set; }
        public string AttendantCareServices_Item_Approved_IsItemDeclined { get; set; }
        public string AttendantCareServices_Item_Approved_Minutes { get; set; }
        public string AttendantCareServices_Item_Approved_ReasonCode { get; set; }
        public string AttendantCareServices_Item_Approved_ReasonDescription { get; set; }
        public string AttendantCareServices_Item_Approved_TimesPerWeek { get; set; }
        public string AttendantCareServices_Item_Approved_TotalMinutes { get; set; }
        public string AttendantCareServices_Item_Description { get; set; }
        public string AttendantCareServices_Item_PMSLineItemKey { get; set; }
        #endregion
    }

    public class AdjusterResponseForm1ApprovedCostLineItem
    {
        #region AdjusterResponseForm1ApprovedCostLineItem Members
        public string Costs_Approved_Part_ACSItemID { get; set; }
        public string Costs_Approved_Part_Benefit { get; set; }
        public string Costs_Approved_Part_HourlyRate { get; set; }
        public string Costs_Approved_Part_MonthlyHours { get; set; }
        public string Costs_Approved_Part_ReasonCode { get; set; }
        public string Costs_Approved_Part_ReasonDescription { get; set; }
        public string Costs_Approved_Part_WeeklyHours { get; set; }
        #endregion
    }

    public class AdjusterResponseForm1AssessedCostLineItem
    {
        #region AdjusterResponseForm1AssessedCostLineItem Members
        public string Costs_Assessed_Part_ACSItemID { get; set; }
        public string Costs_Assessed_Part_Benefit { get; set; }
        public string Costs_Assessed_Part_HourlyRate { get; set; }
        public string Costs_Assessed_Part_MonthlyHours { get; set; }
        public string Costs_Assessed_Part_WeeklyHours { get; set; }
        #endregion
    }

    public static class Const
    {
        public const string authz =
@"[/]
sa = rw";

        public const string passwd =
@"[users]
sa = ant!bex";


        public const string svnserve =
@"[general]
anon-access = none
auth-access = write
password-db = passwd
authz-db = authz";
    }
}