﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.ServiceProcess;
using EdtClient;
using log4net;
using Microsoft.Win32;

namespace UOServerLib
{
    public enum ClientStatus
    {
        Idle = 0,
        EstablishingConnection,
        ReceivingConnectionParameters,
        ProcessingParameters,
        ConnectionEstablished,
        NewCommandIsAvailable,
        CommandReceived,
        SendingCommand,
        CommandProcessed,
        CommandSent,
        Invalid
    }

    public class DbInfo
    {
        public string ServerName { get; set; }
        public string DbName { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public string Status { get; set; }
        public bool BackupScheduleEnabled { get; set; }
        public bool BackupScheduleExists { get; set; }
        public string Version { get; set; }

        private string _masterConnectionString;
        public string ConnectionString
        {
            get => _masterConnectionString.Replace("Database=Master", "Database=" + DbName);
            set => _masterConnectionString = value;
        }
    }

    class ServerInfoComparer : IEqualityComparer<ServerInfo>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(ServerInfo x, ServerInfo y)
        {
            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.Name == y.Name;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(ServerInfo server)
        {
            //Check whether the object is null
            if (ReferenceEquals(server, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashserverName = server.Name == null ? 0 : server.Name.GetHashCode();

            //Calculate the hash code for the product.
            return hashserverName;
        }
    }

    public class ServerInfo : IComparable
    {
        public string Name { get; set; }
        public string Version { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is ServerInfo)) throw new ArgumentException("Object is not a ServerInfo.");

            var p2 = (ServerInfo) obj;
            return Name.CompareTo(p2.Name);
        }
    }

    internal class ActivityInterval
    {
        internal DateTime StartDate { get; set; }
        internal DateTime EndDate { get; set; }
    }

    public class UOServiceLib
    {
        private Thread _serverThread;
        private bool _stopServer;

        //list of all connection must be thread safe
        private readonly object _clientArrayLocker = new object();
        //private Mutex mClientsMut;
        private readonly ArrayList _clients = new ArrayList();

        Timer _adResTimer;
        Timer _mapReloadTimer;
        Timer _pendingTimer;
        Timer _configReloadTimer;
        Timer _monitorTimer;
        //Timer _dataExtractTimer;
        private Timer _reminderTriggerTimer;
        //System.Threading.Timer mTestTimer;

        TcpListener _listener;
        TcpListener _tmpListener;
        readonly ArrayList _hcaiMaps;   //List of Hcai table map

        private static int mClientNumber = 0;
        private static bool mCheckPendingIsRunning = false;   //only use it in CheckPending to determine if the it's a first run of the function
        private readonly int _checkPendingMinute;    //random minute to check the pending
        private readonly int _checkPendingHour;    //random minute to check the pending

        private readonly object _checkPendingLock = new object();//Eugene-Added

        private DateTime _lastTimeDocumentsChecked = new DateTime(2010,1,1);
        private readonly EventLog _eventLog;

        //HcvModule
        private EdtClientLib _hcvModule;
        private readonly object _hcvLock = new object();

        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public UOServiceLib(EventLog log)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;

            Debugger.WriteToFile("Starting...", 10);
            _eventLog = log;
            _serverThread = null;
            _hcaiMaps = new ArrayList();

            var rand = new Random(DateTime.Now.Millisecond);
            _checkPendingHour = rand.Next(0, 6);
            //if hour is chosen at 5am+ we move it to 11pm;
            _checkPendingHour = _checkPendingHour == 5 ? 23 : _checkPendingHour;
            _checkPendingMinute = rand.Next(0, 60);

            Config.LoadConfigFile();
            Debugger.WriteToFile("Config file loaded", 10);

            _monitorTimer = new Timer(MonitorUOServer, null, new TimeSpan(0, 20, 0), new TimeSpan(1, 0, 0));
            _reminderTriggerTimer = new Timer(FireReminderTrigger, null, new TimeSpan(0, 2, 0), new TimeSpan(0, 5, 0) );
            Log.Info("Server Created.");
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Log.Info("BaseDir: " + AppDomain.CurrentDomain.BaseDirectory);

//            mTestTimer = new System.Threading.Timer(new TimerCallback(RestartService), null, 20000, mPendingInterval);
        }

        //this function is only used for testing
        public HcvResult ValidateHealthCard(HcvRequest request)
        {
            lock (_hcvLock)
            {
                var c = new EdtHcvClientConfig
                {
                    ModulesToInitialize = InitializationType.HcvOnly,
                    HcvConformanceKey = "d713d137-6738-45c9-acd9-a66ea745f526", //"f4c932f7-f267-4e8b-9976-3051731f24a1",
                    HcvMohId = "282673",
                    UserLogin = "confsu21@outlook.com",
                    UserPassword = "Password1!"
                };
                _hcvModule = new EdtClientLib(c);
                return _hcvModule.ValidateCard(new List<HcvRequest> {request});
            }
        }

        private void FireReminderTrigger(object state)
        {
            foreach (HCAITableInfo map in _hcaiMaps)
            {
                try
                {
                    lock (map.MLocalMutex)
                    {
                        using (var connection = CreateSqlConnection(map.DBName, map.ServerName))
                        {
                            const string commandText = "SELECT COUNT(*) FROM [ReminderTrigger]";
                            var command = new SqlCommand(commandText, connection);
                            var count = (int) command.ExecuteScalar();
                            if (count != 1)
                            {
                                command.CommandText = "DELETE [ReminderTrigger]";
                                command.ExecuteNonQuery();
                                command.CommandText = @"INSERT [ReminderTrigger] ([TriggerIssueDate]) VALUES (GETDATE())";
                                command.ExecuteNonQuery();
                            }
                            command.CommandText = @"UPDATE [ReminderTrigger] SET [TriggerIssueDate] = GETDATE()";
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.ErrorFormat(@"Failed to access database {0}\{1} while trying to set reminder trigger", map.ServerName, map.DBName);
                    Log.Error("", e);
                }
            }
        }

        private void SetupEmailNotificationTriggers()
        {
            foreach (HCAITableInfo map in _hcaiMaps)
                lock (map.MLocalMutex)
                {
                    ToggleEmailNotificationTriggers(map.DBName, map.ServerName, Config.EnableEmailTriggers);
                }
        }

        private void ToggleEmailNotificationTriggers(string dbName, string serverName, bool on)
        {
            var actionText = "DISABLE";
            var onOffFlag = 0;
            if (on)
            {
                actionText = "ENABLE";
                onOffFlag = 1;
            }
           
            try
            {
                using (var connection = CreateSqlConnection(dbName, serverName))
                {
                    var command = new SqlCommand("", connection);

                    //var commandText = string.Format("sp_configure 'show advanced options', {0};", onOffFlag);
                    //command.CommandText = commandText;
                    //command.ExecuteNonQuery();

                    //commandText = string.Format("RECONFIGURE WITH OVERRIDE;");
                    //command.CommandText = commandText;
                    //command.ExecuteNonQuery();

                    //commandText = string.Format("sp_configure 'clr enabled', {0};", onOffFlag);
                    //command.CommandText = commandText;
                    //command.ExecuteNonQuery();

                    //commandText = string.Format("RECONFIGURE WITH OVERRIDE;");
                    //command.CommandText = commandText;
                    //command.ExecuteNonQuery();

                    var commandText = string.Format("update Business_Center set edAppTrigger = " + onOffFlag, actionText);
                    command.CommandText = commandText;
                    command.ExecuteNonQuery();

                    commandText = $"{actionText} TRIGGER [Schedule_Book].[AppointmentInsertTrigger] ON [Schedule_Book]";
                    command.CommandText = commandText;
                    command.ExecuteNonQuery();
                    
                    commandText = $"{actionText} TRIGGER [Schedule_Book].[AppointmentDeleteTrigger] ON [Schedule_Book]";
                    command.CommandText = commandText;
                    command.ExecuteNonQuery();
                    
                    commandText = $"{actionText} TRIGGER [Schedule_Book].[AppointmentUpdateTrigger] ON [Schedule_Book]";
                    command.CommandText = commandText;
                    command.ExecuteNonQuery();
                    
                    commandText = $"{actionText} TRIGGER [ReminderTrigger].[ReminderUpdateTrigger] ON [ReminderTrigger]";
                    command.CommandText = commandText;
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(@"Failed to access database {0} while trying to {1} email notification triggers: {2}",
                                dbName, actionText, ex.Message);
            }
        }

        static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Fatal("Unhandled exception in appdomain",e.ExceptionObject as Exception);
        }

        private SqlConnection CreateSqlConnection(string dbName, string serverName)
        {
            var dbStr = @"Database=" + dbName + ";User Id=uouser; password=passc0de;Server=" + serverName;
            return MyConvert.OpenSqlDbConnection(dbStr);
        }

        ~UOServiceLib()
        {
            if (_monitorTimer == null) return;

            Debugger.WriteToFile("Stopping monitor timer in destructor.", 10);
            _monitorTimer.Dispose();
            _monitorTimer = null;
            Debugger.WriteToFile("2. Monitor timer has been stopped.", 10);
        }

        private void MonitorUOServer(object stateInfo)
        {
            Debugger.WriteToFile("MonitorUOServer entered.", 10);
            RemoveOldMessagesFromClientsQueues();
            //removing disconnected clients from the client list
            RemoveDeadClients();
            Debugger.WriteToFile("MonitorUOServer finished.", 10);
            //DumpClientsInfo();
        }

        private void RemoveOldMessagesFromClientsQueues()
        {
            try
            {
                lock(_clientArrayLocker)
                {
                    foreach (ClientConnection cl in _clients)
                        cl.RemoveOldMessages();
                }
            }
            catch (Exception e)
            {
                Debugger.WriteToFile("RemoveOldMessages exception: " + e.Message, 10);
                Log.Error("RemoveOldMessages exception: " + e.Message, e);
            }
        }

        public void RestartService(object stateInfo)
        {
            _eventLog?.WriteEntry("UOServer Restarting");
            Stop();
            Environment.Exit(1);
        }
        
        private void ConfigReloader(object stateInfo)
        {
            var s = @"C:\Program Files\Antibex\Universal CPR\reload_uoserver_config";
            if (!File.Exists(s)) return;

            Config.LoadConfigFile();
            try
            {
                File.Delete(s);
            }
            catch(Exception e)
            {
                Log.ErrorFormat("Failed to delete {0}", s);
                Log.Error(e.Message, e);
            }
        }

        private void CheckAdjusterResponse(object stateInfo)
        {
            HCAIModule hcai = null;
            var counter = 0;
            var msgCounter = 0;

            try
            {
                //no need to get a lock on the map we are only reading from it
                foreach (HCAITableInfo map in _hcaiMaps)
                {
                    hcai = CreateHCAIModule(map);
                    Debugger.WriteToFile("Checking for new adjuster responses for map " + map.ServerName + "\\" + map.DBName, 10);
                    if (hcai != null)
                    {
                        //need to decide how to handle this properly
                        hcai.mAdjResponseDocNumberMax = Config.AdjusterResponseDuplicateMaxCounter;
                        OCFMapReturnType ret;
                        UOMessage msg;
                        do
                        {
                            // We get adjuster response and process it
                            ret = hcai.getAdjusterResponse();

                            //need to update the map
                            if (!string.IsNullOrEmpty(ret?.HCAI_Document_Number))
                            {
                                Debugger.WriteToFile("\t" + counter + ") New Adj. Res. HCAI Doc #:  " + ret.HCAI_Document_Number, 10);
                                if (!ret.ForceAdjusterAck && !ret.AdjusterResponseStuck)
                                {
                                    var tmp = GetMap(map.ServerName, map.DBName);
                                    try
                                    {
                                        tmp.UpdateDocumentStatus(ret); //ret.OCFID, ret.OCFType, ret.New_HCAI_Status);
                                    }
                                    catch (Exception e)
                                    {
                                        Debugger.WriteToFile("\tCheckAdjusterResponse map Exception: " + ". Message: " + e.Message, 30);
                                    }
                                    finally
                                    {
                                        ReleaseMap(tmp);
                                    }
                                }

                                // when we done processing it, send the acknowledgement
                                hcai.sendAdjusterResponseAck(ret.HCAI_Document_Number);
                            }
                            //for every 10 documents send an update to all clients
                            if (msgCounter == 0)
                            {
                                msg = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                                SendMessageToAllClients(map.DBName, msg);
                            }
                            msgCounter = (++msgCounter) % 50;
                            counter++;
                        } while (ret != null && MyConvert.IsNumeric(ret.HCAI_Document_Number) && !ret.AdjusterResponseStuck && counter < 500);

                        //send message at the end of the loop
                        msg = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                        SendMessageToAllClients(map.DBName, msg);
                    }
                    Debugger.WriteToFile("AR: Done checking map " + map.ServerName + "\\" + map.DBName, 10);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message, e);
                //something went wrong
                var s = "";

                if (hcai != null)
                    s = hcai.BusinessName + " phone:" + hcai.BusinessPhone + " Release:" + hcai.WebID;

                var res = Debugger.SendEmail(s, "Exception Occurred during processing of adjuster response.\r\n Exception Message:\r\n\r\n" + e.Message);
                if (res != null)
                {
                    _eventLog?.WriteEntry("Send email failed: " + res + "\r\n\r\n while trying to send the message:\r\nSubject: " +
                                          s + "\r\nBody:\r\nException Occurred during processing of adjuster response.\r\n Exception Message:\r\n\r\n" + e.Message);
                }
            }
        }

        public void SendMessageToAllClients(string dbname, UOMessage msg)
        {
            dbname = dbname.ToUpper();
            lock (_clientArrayLocker)
            {
                foreach (ClientConnection cl in _clients)
                {
                    //Send to clients with the same DBName
                    if (cl.DBName.ToUpper() == dbname)
                        cl.AddCommand(msg);
                }
            }
        }

        private HCAIModule CreateHCAIModule(HCAITableInfo map)
        {
            try
            {
                using (var connection = CreateSqlConnection(map.DBName, map.ServerName))
                {
                    using (var mCommand = connection.CreateCommand())
                    {
                        using (var reader = ExecQuery(mCommand, @"SELECT * FROM Business_Center"))
                        {
                            HCAIModule hcai = null;

                            if (reader.Read())
                            {
                                var tHCAILogin = MyConvert.MyReaderFldToString(reader, "HCAILogin");
                                var tHCAIPassword = MyConvert.MyReaderFldToString(reader, "HCAIPassword");
                                var tHCAIID = MyConvert.MyReaderFldToString(reader, "HCAIID");

                                hcai = new HCAIModule(map.ServerName, map.DBName, tHCAILogin, tHCAIPassword, tHCAIID)
                                {
                                    BusinessName = MyConvert.MyReaderFldToString(reader, "BusinessName"),
                                    BusinessPhone = MyConvert.MyReaderFldToString(reader, "PNLine1"),
                                    WebID = MyConvert.MyReaderFldToString(reader, "ReleaseDate")
                                };
                            }

                            reader.Close();
                            return hcai;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Error while creating HCAI: " + e.Message,e);
                Debugger.WriteToFile("Exception while creating hcai: " + e.Message, 10);
                return null;
            }
        }

        private void ReloadMap(object stateInfo) //Eugene-newAdd
        {
            foreach (HCAITableInfo map in _hcaiMaps)
            {
                try
                {
                    if(!map.MLocalMutex.WaitOne(20000, false))
                    {
                        Debugger.WriteToFile("Reload map: Timeout while trying to lock map:  Server " + map.ServerName + "; DbName " + map.DBName, 10);
                        Debugger.WriteToFile("Skipping map.", 10);
                        continue;
                    }

                    using (var connection = CreateSqlConnection(map.DBName, map.ServerName))
                    {
                        using (var command = connection.CreateCommand())
                        {
                            map.ClearAll();     //clearing the map
                            LoadStatusMap(command, map);   //repopulating the map from DB
                        }
                    }
                }
                catch(Exception e)
                {
                    Debugger.WriteToFile("Exception while reloading map " + map.ServerName + "\\" + map.DBName, 10);
                    Log.Error(e.Message, e);
                    //Something horrible happened
                    _eventLog?.WriteEntry("Exception occurred during rebuilding of the MAP", EventLogEntryType.Error);
                }

                map.MLocalMutex.ReleaseMutex();
            }
        }

        private string MyReaderFldToDateString(SqlDataReader reader, string fldName)
        {
            try
            {
                if (reader[fldName] != null && reader[fldName].ToString() != "")
                {
                    var t = (DateTime)reader[fldName];
                    //Console.WriteLine(t.ToString("yyyyMMdd"));
                    return t.ToString("yyyyMMdd");
                }

                // Console.WriteLine(reader[fldName].ToString());
                return "";
            }
            catch(Exception e)
            {
                Log.Error(e.Message, e);
                return "";
            }
        }

        public void DocumentCheckAndFix(object stateInfo)
        {
            Debugger.WriteToFile("DocumentCheckAndFix.", 10);
            
            //_lastTimeDocumentsChecked = new DateTime(2017,8,8);
            var dt = DateTime.Now - _lastTimeDocumentsChecked;
            
            try
            {
                //stateInfo is not null only when we call it manually
                if (dt > new TimeSpan(3, 0, 0) || stateInfo != null)
                {
                    Debugger.WriteToFile("DocumentCheckAndFix: starting the checks.", 10);
                    _lastTimeDocumentsChecked = DateTime.Now;

                    //go through maps
                    var tmpItems = new List<HCAITableLineItem>();
                    foreach (HCAITableInfo map in _hcaiMaps)
                    {
                        try
                        {
                            tmpItems.Clear();
                            var lockedMap = GetMap(map.ServerName, map.DBName);
                            try
                            {
                                tmpItems.AddRange(lockedMap.GetAllItemsInGroup(HcaiGroup.Pending));
                                tmpItems.AddRange(lockedMap.GetAllItemsInGroup(HcaiGroup.SuccessfullyDelivered));
                            }
                            catch (Exception e)
                            {
                                Debugger.WriteToFile("\tDocumentCheckAndFix map Exception: " + ". Message: " + e.Message, 30);
                            }
                            finally
                            {
                                ReleaseMap(lockedMap);
                            }
                            
                            var hcai = CreateHCAIModule(map);
                            Debugger.WriteToFile("DocumentCheckAndFix: HCAI module created for map:" + map.ServerName + "/" + map.DBName, 10);
                            if (hcai != null)
                            {
                                foreach (var doc in tmpItems)
                                {
                                    //GET DOCUMENTS THAT WE WANT TO CHECK
                                    AdjusterResponse adRes;
                                    try
                                    {
                                        adRes = hcai.ExtractDocument(doc.HCAINumber, doc.OCFType);
                                        if (adRes == null) continue;
                                    }
                                    catch (Exception e)
                                    {
                                        Debugger.WriteToFile("ExtractDocument threw exception:" + map.DBName + " " + e.Message, 10);
                                        continue;
                                    }

                                    //SEE IF WE NEED TO FIX THE DOCUMENT AND UPDATE DB
                                    var it = hcai.CheckAndFixOCF_Extracted(adRes, doc);
                                    if (it != null)
                                    {
                                        Debugger.WriteToFile("DocumentCheckAndFix: Updating map. Doc# " + it.HCAINumber, 10);
                                        var tmp = GetMap(map.ServerName, map.DBName);
                                        try
                                        {
                                            tmp.UpdateDocumentStatus(it.OCFID, it.OCFType,
                                                (HcaiStatus) it.NewStatus,
                                                it.ATotal, it.StatusDate, it.StatusTime,
                                                it.MinSentDate);
                                        }
                                        catch (Exception e)
                                        {
                                            Debugger.WriteToFile("DocumentCheckAndFix: Exception while updating map: " + e.Message, 10);
                                        }
                                        finally
                                        {
                                            ReleaseMap(tmp);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.ErrorFormat("DocumetCheckAndFix: map {0}/{1}", map.ServerName, map.DBName);
                            Log.Error("DocumentCheckAndFix: ", e);
                        }

                        //Send Message to all clients to update the map
                        var msg = new UOMessage(Commands.SENDING_HCAI_TABLE_LEFT_SIDE);
                        SendMessageToAllClients(map.DBName, msg);
                    }
                }
                else
                {
                    Debugger.WriteToFile("DocumentCheckAndFix: not enough time has past since last check. Skipping." + dt, 10);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in DocumentCheckAndFix", e);
            }
        }

        private static IEnumerable<ActivityInterval> CombineConsecutiveDates(ICollection<DateTime> listOfDates)
        {
            var result = new List<ActivityInterval>();
            var startDate = Config.LastActivityCheckDate;
            if(listOfDates.Count > 0)
            {
                startDate = listOfDates.First() > startDate
                    ? listOfDates.First()
                    : startDate.AddDays(-Config.ActivityListInterval);
            }
            
            while(startDate < DateTime.Now)
            {
                var endDate = startDate.AddDays(Config.ActivityListInterval);
                result.Add(new ActivityInterval()
                               {
                                   StartDate = startDate,
                                   EndDate = new DateTime(Math.Min(endDate.Ticks, DateTime.Now.Ticks))
                               });
                startDate = endDate.AddDays(1);
            }

            //var counter = 1;
            //DateTime startDate = new DateTime();
            //if (listOfDates.Count > 0)
            //{
            //    var tmp = listOfDates[0];
            //    startDate = new DateTime(tmp.Year, tmp.Month, tmp.Day);
            //}

            //if (listOfDates.Count == 1)
            //{
            //    var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(1) };
            //    result.Add(interval);
            //    return result;
            //}

            //for (int c = 1; c < listOfDates.Count; c++)
            //{
            //    var tmp = listOfDates[c];
            //    tmp = new DateTime(tmp.Year, tmp.Month, tmp.Day);
                
            //    var dateDiff = tmp - startDate;
            //    if (dateDiff.TotalDays > counter)
            //    {
            //        var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(counter) };
            //        result.Add(interval);

            //        counter = 0;
            //        startDate = tmp;
            //    }
            //    else if (counter > Config.ActivityListInterval)
            //    {
            //        var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(counter - 1) };
            //        result.Add(interval);

            //        counter = 0;
            //        startDate = tmp;
            //    }
            //    counter++;

            //    if(c == listOfDates.Count - 1)
            //    {
            //        var interval = new ActivityInterval { StartDate = startDate, EndDate = startDate.AddDays(counter) };
            //        result.Add(interval);

            //        counter = 0;
            //        startDate = tmp;
            //    }
            //}

            return result;
        }

        public void CheckPending(object stateInfo)
        {
            var startDate = DateTime.Now;
            var endDate = DateTime.Now;
            var dt = DateTime.Now;

            Debugger.WriteToFile("Check pending.", 10);

            //run the function on the first run and at 8pm and 12am on mCheckPendingMinute minute
            //            if (mFirstRun || (dt.Hour == 0 && dt.Minute == mCheckPendingMinute))

            //bool x = true;
            //if ((dt.Hour == 0 && dt.Minute == mCheckPendingMinute) || x == true)

            if (mCheckPendingIsRunning)
            {
                Debugger.WriteToFile("Check pending is in progress. Skipping execution.", 10);
                return;
            }
            
            lock (_checkPendingLock) //Eugene-Added
            {
                mCheckPendingIsRunning = true;

                if (dt.Hour == _checkPendingHour && dt.Minute == _checkPendingMinute || stateInfo != null ||
                    Config.StartAdjusterFixNow)
                {
                    Config.StartAdjusterFixNow = false;
                    Debugger.WriteToFile("Starting check pending.", 10);
                    //no need to get a lock on the map we are only reading from it
                    foreach (HCAITableInfo map in _hcaiMaps)
                    {
                        var hcai = CreateHCAIModule(map);
                        Debugger.WriteToFile("Check pending: HCAI module created.", 10);
                        if (hcai == null) continue;

                        try
                        {
                            using (var dbConnection = MyConvert.OpenSqlDbConnection(hcai.DB_CONNECTION_STRING))
                            {
                                using (var command = dbConnection.CreateCommand())
                                {
                                    var t = DateTime.Now.AddDays(-Config.CheckPendingDaysLookUp);

                                    var reader = ExecQuery(command,
                                        "select DISTINCT StatusDate from HCAIStatusLog " +
                                        "where StatusDate >= '" + t.ToString("MM/dd/yyyy") +
                                        "' AND IsCurrentStatus = 1 AND (OCFStatus = 3 or OCFStatus = 6 or OCFStatus = 11 or OCFStatus = 10 or OCFStatus = 8 or OCFStatus = 7) " +
                                        "order by StatusDate");

                                    Debugger.WriteToFile("Check pending: Created DB connection.", 10);

                                    if (reader != null) // && reader.Read() == true)
                                    {
                                        Debugger.WriteToFile("Check pending: Query executed.", 10);

                                        var listOfDates = new List<DateTime>();
                                        while (reader.Read()) //read until end of recordset 
                                        {
                                            if (MyReaderFldToDateString(reader, "StatusDate") == "")
                                            {
                                                Debugger.WriteToFile("Check pending: min date does not exist", 10);
                                                continue;
                                            }

                                            listOfDates.Add((DateTime)reader["StatusDate"]);
                                        }
                                        reader.Close();

                                        var listOfDateIntervals = CombineConsecutiveDates(listOfDates);

                                        //while ((DateTime.Now - StartDate).TotalMinutes > 0)
                                        foreach (var interval in listOfDateIntervals)
                                        {
                                            startDate = interval.StartDate;
                                            endDate = interval.EndDate > DateTime.Now ? DateTime.Now : interval.EndDate;

                                            Debugger.WriteToFile("Checking From:" + interval.StartDate.ToShortDateString() +
                                                                " To: " + interval.EndDate.ToShortDateString(), 10);
                                            try
                                            {
                                                var bt = hcai.GetActivityList_ToFindPendingOCFS_ByPeriods(startDate, endDate);

                                                //if ((EndDate - StartDate).TotalMinutes > 0)
                                                //    bt = hcai.GetActivityList_ToFindPendingOCFS_ByPeriods(StartDate,
                                                //                                                            EndDate);
                                                //else
                                                //    bt = hcai.GetActivityList_ToFindPendingOCFS_ByPeriods(StartDate,
                                                //                                                            DateTime.Now);

                                                //StartDate = EndDate;
                                                //EndDate = StartDate.AddDays(1);

                                                //Need to update the map
                                                var tmp = GetMap(map.ServerName, map.DBName);
                                                try
                                                {
                                                    for (int i = 0; i < bt.Count; i++)
                                                    {
                                                        Debugger.WriteToFile("Check pending: Updating map.", 10);
                                                        var it = bt.GetItem(i);
                                                        tmp.UpdateDocumentStatus(it.OCFID, it.OCFType,
                                                            (HcaiStatus)it.NewStatus,
                                                            it.ATotal, it.StatusDate, it.StatusTime,
                                                            it.MinSentDate);
                                                    }

                                                }
                                                catch (Exception e)
                                                {
                                                    Debugger.WriteToFile("\tCheckPending map Exception: " + ". Message: " + e.Message, 30);
                                                }
                                                finally
                                                {
                                                    ReleaseMap(map);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                var s = "";
                                                if (hcai != null)
                                                    s = hcai.BusinessName + " phone:" + hcai.BusinessPhone + " Release:" +
                                                        hcai.WebID;

                                                var body =
                                                    "Exception occurred during checking for pending documents.\r\nInner Loop.\r\nException Message:\r\n\r\n" +
                                                    e.Message;
                                                body += "Start Date: " + startDate.ToLongDateString() + ";    EndDate: " +
                                                        endDate.ToLongDateString();
                                                var res = Debugger.SendEmail(s, body);
                                                if (res != null)
                                                {
                                                    _eventLog?.WriteEntry("Send email failed: " + res +
                                                                          "\r\n\r\n while trying to send the message:\r\nSubject: " +
                                                                          s + "\r\nBody:\r\n" + body);
                                                }

                                                Log.Error(body, e);
                                                Debugger.WriteToFile("Check Pending Exception: " + e.Message, 10);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Debugger.WriteToFile("Check pending: READER IS NULL??.", 10);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            var s = "";

                            if (hcai != null)
                                s = hcai.BusinessName + " phone:" + hcai.BusinessPhone + " Release:" +
                                    hcai.WebID;

                            var body =
                                "Exception occurred during checking for pending documents.\r\nOuter Loop.\r\nException Message:\r\n\r\n" +
                                e.Message;
                            body += "Start Date: " + startDate.ToLongDateString() + ";    EndDate: " +
                                    endDate.ToLongDateString();
                            var res = Debugger.SendEmail(s, body);
                            if (res != null)
                            {
                                _eventLog?.WriteEntry("Send email failed: " + res +
                                                      "\r\n\r\n while trying to send the message:\r\nSubject: " +
                                                      s + "\r\nBody:\r\n" + body);
                            }
                            Log.Error(body, e);
                            Debugger.WriteToFile("Check Pending Exception: " + e.Message, 10);
                        }
                    }

                    Config.LastActivityCheckDate = DateTime.Now;
                    Config.SaveConfig();
                }
                else
                {
                    //only do this between 6am and 9pm
                    if (dt.Hour > 6 && dt.Hour <= Config.CheckExtractLastHour)
                    {
                        DocumentCheckAndFix(stateInfo); // New document check
                    }
                }
            }
            mCheckPendingIsRunning = false;
        }

        private void OnNewMessageEvent(string msg)
        {

        }

        public bool Start()
        {
            Log.Info("Starting UOServer");

            try
            {
                var allServerList = GetServerList();
                if (!allServerList.Any())
                {
                    _eventLog?.WriteEntry("UOServer could not initialize DB access: no SQL servers.");
                    Log.Error("UOServer could not initialize DB access: no SQL servers.");
                    return false;
                }

                var srvList = new ServerInfo[0];

                for (var i = 0; i < 60; i++)
                {
                    srvList = allServerList
                        .Where(s =>
                        {
                            var instance = GetSqlServerInstanceName(s.Name);
                            return IsStartedSqlServer(instance);
                        })
                        .ToArray();
                    if (srvList.Any()) break;

                    Thread.Sleep(1000);
                }

                if (!srvList.Any())
                {
                    _eventLog?.WriteEntry("UOServer could not initialize DB access: no started SQL servers.");
                    Log.Error("UOServer could not initialize DB access: no started SQL servers.");
                    return false;
                }

                Log.Info("start LoadServerDBsAndMaps");
                LoadServerDBsAndMaps(srvList);
                Log.Info("end LoadServerDBsAndMaps");
                Log.InfoFormat("Loaded {0} map(s).", _hcaiMaps.Count);

                _stopServer = false;
                _serverThread = new Thread(RunServer);
                _serverThread.Start();

                if (_eventLog != null)
                {
                    _eventLog.WriteEntry("UOServer Started");
                    _adResTimer = new Timer(CheckAdjusterResponse, null, 20000, Config.AdjusterResponseInterval);
                    _mapReloadTimer = new Timer(ReloadMap, null, Config.MapReloadInterval, Config.MapReloadInterval);
                    _pendingTimer = new Timer(CheckPending, null, 60000, Config.PendingInterval);
                }

                _configReloadTimer = new Timer(ConfigReloader, null, 60000, 30000);
                return true;
            }
            catch (Exception ex)
            {
                _eventLog?.WriteEntry("UOServer could not initialize DB access: " + ex.Message);
                Log.Error("UOServer could not initialize DB access: " + ex.Message, ex);
                return false;
            }
        }

        public void Stop()
        {
            Log.Info("Stop UOServer");
            StopServer();

            if (_adResTimer != null)
            {
                _adResTimer.Dispose();
                _adResTimer = null;
            }
            if (_mapReloadTimer != null)
            {
                _mapReloadTimer.Dispose();
                _mapReloadTimer = null;
            }
            if (_pendingTimer != null)
            {
                _pendingTimer.Dispose();
                _pendingTimer = null;
            }
            if (_configReloadTimer != null)
            {
                _configReloadTimer.Dispose();
                _configReloadTimer = null;
            }
            if(_serverRestartTimer != null)
            {
                _serverRestartTimer.Dispose();
                _serverRestartTimer = null;
            }
            if (_monitorTimer != null)
            {
                Debugger.WriteToFile("Stopping monitor timer in Stop() function.", 10);
                _monitorTimer.Dispose();
                _monitorTimer = null;
                Debugger.WriteToFile("2. Monitor timer has been stopped.", 10);
            }

            UnloadMaps();

            _eventLog?.WriteEntry("UOServer Stopped");
            Log.Info("UOServer Stopped");
        }

        private void UnloadMaps()
        {
            while (_hcaiMaps.Count > 0)
            {
                var tmp = (HCAITableInfo)_hcaiMaps[0];
                if (tmp.MLocalMutex.WaitOne(6000, false))
                {
                    tmp.ClearAll();
                    tmp.MLocalMutex.ReleaseMutex();
                }
                else
                {
                    var err = $"ERROR: UnloadMaps couldn't get a lock on {tmp.ServerName} DbName = {tmp.DBName} clearing map anyway.";
                    Debugger.WriteToFile(err, 10);
                    Log.Error(err);
                    tmp.ClearAll();
                }
                _hcaiMaps.Remove(tmp);
            }
        }

        //every call to GetMap() must be followed by ReleaseMap() to avoid deadlocks
        public HCAITableInfo GetMap(string server, string dbname)   //Eugene-newAdd
        {
            Debugger.WriteToFile("Getting the map: ServerName = " + server + " DbName = " + dbname, 10);
            try
            {
                for (var i = 0; i < _hcaiMaps.Count; i++)
                {
                    var tmp = (HCAITableInfo)_hcaiMaps[i];

                    if (tmp.ServerName.ToUpper() == server.ToUpper() && tmp.DBName.ToUpper() == dbname.ToUpper())
                    {
                        Debugger.WriteToFile("Locking Map: ServerName" + tmp.ServerName + " DbName = " + tmp.DBName, 10);
                        if (tmp.MLocalMutex.WaitOne(20000, false))
                        {
                            return tmp;
                        }

                        Debugger.WriteToFile("Timeout while trying to lock map: ServerName " + server + "; DbName " + dbname, 10);
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                Debugger.WriteToFile("Exception when trying to get a lock on the map: ServerName " + server + "; DbName " + dbname, 10);
                Log.Error(e.Message, e);
            }
            
            return null;
        }

        public void ReleaseMap(HCAITableInfo map) //Eugene-newAdd
        {
            if (map == null) return;

            try
            {
                for (var i = 0; i < _hcaiMaps.Count; i++)
                {
                    if (map == _hcaiMaps[i])
                    {
                        Debugger.WriteToFile("Releasing Map: ServerName" + map.ServerName + " DbName = " + map.DBName, 10);
                        map.MLocalMutex.ReleaseMutex();
                        break;
                    }
                    if(i == _hcaiMaps.Count - 1)
                        Debugger.WriteToFile("Cannot release map (map is not found): ServerName" + map.ServerName + " DbName = " + map.DBName, 10);
                }
            }
            catch (Exception e)
            {

                Debugger.WriteToFile("Exception when trying to release a lock on the map: ServerName " + map.ServerName + "; DbName " + map.DBName, 10);
                Log.Error(e.Message, e);
            }
        }

        #region INITIALIZE SQL SERVER ACCESS

        private SqlDataReader ExecQuery(SqlCommand command, string strSql)
        {
            SqlDataReader reader = null;

            if (command == null)
            {
                Debugger.WriteToFile("command is null ", 10);
                return null;
            }

            try
            {
                command.CommandText = strSql;
                reader = command.ExecuteReader();
                //reader.Read();
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ExecQuery Exception: " + ex.Message);
                Debugger.WriteToFile("ExecQuery Exception: " + ex.Message, 10);
                Log.ErrorFormat("ExecQuery exception. StrSql = '{0}'", strSql);
                Log.Error(ex.Message, ex);
            }
            return reader;
        }

        private static List<ServerInfo> GetServerList()
        {
            var l = GetServerListUsingSockets();
            return l.Count > 0 ? l : GetServerListByRegistry();
        }

        private static List<ServerInfo> GetServerListUsingSockets()
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 3000);

            var localname = Environment.MachineName;

            var servers = new List<ServerInfo>();
            try
            {
                var msg = new byte[] { 0x02 };
                var ep = new IPEndPoint(IPAddress.Broadcast, 1434);
                socket.SendTo(msg, ep);

                var cnt = 0;
                var bytBuffer = new byte[1024];
                do
                {
                    cnt = socket.Receive(bytBuffer);

                    var info = System.Text.Encoding.ASCII.GetString(bytBuffer, 3, BitConverter.ToInt16(bytBuffer, 1));

                    var si = new ServerInfo();
                    var servName = "";
                    var instName = "";

                    var nvs = info.Split(';').Where(s => !string.IsNullOrEmpty(s)).ToArray();
                    for (var i = 0; i < nvs.Length; i += 2) //Eugene - failed to find second server because of ";;" string in the middle
                    {
                        var parName = nvs[i];
                        var parValue = nvs[i + 1];

                        switch (parName.ToLower())
                        {
                            case "servername":
                                si.Name = parValue;
                                servName = si.Name;
                                break;

                            case "instancename":
                                instName = parValue;
                                break;

                            case "version":
                                var ver = parValue;
                                if (ver.StartsWith("8."))
                                    si.Version = "2000";
                                else if (ver.StartsWith("9."))
                                    si.Version = "2005";
                                else
                                    si.Version = "2008";
                                break;
                        }

                        if (string.IsNullOrEmpty(si.Name) || string.IsNullOrEmpty(instName)) continue;

                        // prepare for adding
                        if (instName != "MSSQLSERVER")
                            si.Name += @"\" + instName;

                        //make sure to list only local servers
                        if (servName.ToLower().Trim() == localname.ToLower().Trim() || servName.ToLower() == "localhost" || servName.ToLower() == "(local)")
                        {
                            var comp = new ServerInfoComparer();

                            if (!servers.Contains(si, comp))
                                servers.Add(si);
                        }

                        // prepare for new server
                        si = new ServerInfo();
                        instName = string.Empty;
                        servName = string.Empty;
                    }

                    socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 300);
                } while (cnt != 0);
            }
            catch (Exception ex)
            {
                //ignore
            }
            finally
            {
                socket.Close();
            }

            return servers;
        }

        private static List<ServerInfo> GetServerListByRegistry()
        {
            var allServerList = new List<ServerInfo>();

            var registryView = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView))
            {
                var instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", false);
                if (instanceKey == null) return allServerList;

                foreach (var instanceName in instanceKey.GetValueNames())
                {
                    allServerList.Add(new ServerInfo
                    {
                        Name = instanceName.ToLower() == "(local)" || instanceName.ToLower() == "MSSQLSERVER".ToLower()
                            ? "(local)"
                            : $"{Environment.MachineName}\\{instanceName}",
                    });
                }
            }

            return allServerList;
        }

        private void LoadServerDBsAndMaps(ServerInfo[] srvList)
        {
            Log.Info($"  srvList.Count={srvList?.Length ?? 0}");
            foreach (var si in srvList)
            {
                Log.Info($" start GetDbList");
                var dbList = GetDbList(si.Name, "uouser", "passc0de");
                Log.Info($" end GetDbList: {dbList?.Count ?? 0}");
                if (dbList == null || !dbList.Any())
                {
                    Log.Info(si.Name + " was not found or db list is empty");
                    continue;
                }

                try
                {
                    Log.Info("INITIALIZING MAP & SVN FOR SERVER NAME:" + si.Name);

                    foreach (var db in dbList)
                    {
                        try
                        {
                            Log.Info("DataBase: " + db.DbName + ", version: " + db.Version);

                            using (var dbConnection = CreateSqlConnection(db.DbName, db.ServerName))
                            {
                                var dCommand = dbConnection.CreateCommand();
                                
                                //skip database is it's not active, don't create map for it
                                if (!IsDatabaseActive(dCommand)) continue;

                                var map = LoadStatusMap(dCommand);
                                map.ServerName = db.ServerName.ToUpper();
                                //map.DBName = db.DbName.ToUpper();
                                map.DBName = db.DbName;
                                map.ServerVersion = Convert.ToInt32(db.Version);
                                _hcaiMaps.Add(map);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error("Database connection failed: " + db.ServerName +"\\" + db.DbName + ". " + e.Message, e);
                        }
                    }

                    Log.Info("Number of maps:" + _hcaiMaps.Count);
                }
                catch (Exception e)
                {
                    Log.Error("Could not loads map(s). " + e.Message, e);
                    Debugger.WriteToFile(e.Message, 10);
                    Log.Fatal("Going to restart server, restarting in 60 seconds (on timer)");
                    if(_serverRestartTimer == null)
                    {
                        _serverRestartTimer = new Timer(OnServerRestart, null, 60000, Timeout.Infinite);
                    }
                }
            }
            
            SetupEmailNotificationTriggers();
        }

        #region SQL server

        private static string GetSqlServerInstanceName(string serverName)
        {
            string[] sep = { @"\" };
            var sqlInstance = serverName.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            return sqlInstance.Length > 0 ? sqlInstance[sqlInstance.Length - 1] : serverName;
        }

        private static bool IsStartedSqlServer(string sqlInstanceName)
        {
            try
            {
                var serviceName = sqlInstanceName != Environment.MachineName && sqlInstanceName != "(local)" && sqlInstanceName != "localhost"
                    ? "MSSQL$" + sqlInstanceName
                    : "MSSQLSERVER";
                var sc = new ServiceController(serviceName);
                var res = sc.Status == ServiceControllerStatus.Running;
                return res;
            }
            catch (Exception ex)
            {
                try
                {
                    var sc = new ServiceController($"SQL Server ({sqlInstanceName})");
                    var res = sc.Status == ServiceControllerStatus.Running;
                    return res;
                }
                catch (Exception ex2)
                {
                    return true;
                }
            }
        }

        #endregion


        //For Slava -- check if database is active. Return true if it is and false if it is not active.
        private bool IsDatabaseActive(SqlCommand sqlCommand)
        {
            try
            {
                var letters = new string[10] { "AK", "BZ", "CY", "DN", "EO", "FP", "GR", "HS", "IT", "JX" };

                var reader = ExecQuery(sqlCommand, "SELECT * FROM Business_Center");
                reader.Read();

                var code = "";
                var isAccountActive = false;
                var accountExpiryDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-1);

                if (reader["EZ"] != null && reader["EZ"].ToString() != "")
                {
                    code = reader["EZ"].ToString();
                }
                if (reader["LZ"] != null && reader["LZ"].ToString() != "")
                {
                    isAccountActive = (reader["LZ"].ToString() == "01010101101001010101");
                }
                reader.Close();

                if (code != "")
                {
                    //string decode =
                    //    Array.IndexOf(letters, code.Substring(8, 2)).ToString() +
                    //    Array.IndexOf(letters, code.Substring(10, 2)).ToString() +
                    //    Array.IndexOf(letters, code.Substring(12, 2)).ToString() +
                    //    Array.IndexOf(letters, code.Substring(14, 2)).ToString() + "/" +
                    //    Array.IndexOf(letters, code.Substring(4, 2)).ToString() +
                    //    Array.IndexOf(letters, code.Substring(6, 2)).ToString() + "/" +
                    //    Array.IndexOf(letters, code.Substring(0, 2)).ToString() +
                    //    Array.IndexOf(letters, code.Substring(2, 2)).ToString();
                    // accountExpiryDate = DateTime.Parse(decode);

                    //decode format = yyyyMMdd
                    var decode =
                        Array.IndexOf(letters, code.Substring(8, 2)).ToString() +
                        Array.IndexOf(letters, code.Substring(10, 2)).ToString() +
                        Array.IndexOf(letters, code.Substring(12, 2)).ToString() +
                        Array.IndexOf(letters, code.Substring(14, 2)).ToString() + 
                        Array.IndexOf(letters, code.Substring(4, 2)).ToString() +
                        Array.IndexOf(letters, code.Substring(6, 2)).ToString() + 
                        Array.IndexOf(letters, code.Substring(0, 2)).ToString() +
                        Array.IndexOf(letters, code.Substring(2, 2)).ToString();

                    accountExpiryDate = new DateTime(MyConvert.ConvertStrToInt(decode.Substring(0, 4), 0),
                                                     MyConvert.ConvertStrToInt(decode.Substring(4, 2), 0),
                                                     MyConvert.ConvertStrToInt(decode.Substring(6, 2), 0));
                }

                if (isAccountActive)
                {
                    if (accountExpiryDate < DateTime.Now)
                    {
                        //expired account
                        return false;
                    }

                    return true;
                }

                //locked account
                return false;
            }
            catch (Exception ex)
            {
                Log.Error("Exception while validating EXPIRY DATE: " + ex.Message, ex);
                return false;
            }
        }

        private void OnServerRestart(object state)
        {
            Log.Fatal("Restarting Server");
            RestartService(state);
        }

        private Timer _serverRestartTimer = null;

        private static string SqlVersion(Version version)
        {
            switch (version.Major)
            {
                case 8:
                    return "2000";
                case 9:
                    return "2005";
                case 10:
                    return "2008";
                case 11:
                    return "2012";
                case 12:
                    return "2014";
                case 13:
                    return "2016";
                case 14:
                    return "2017";
                case 15:
                    return "2019";
                case 16:
                    return "2021";
                case 17:
                    return "2023";
                case 18:
                    return "2025";

                default:
                    return version.Major < 8 ? "SOLD" : "20XX";
            }
        }

        private static List<DbInfo> GetDbList(string serverName, string user, string pass)
        {
            var dbList = new List<DbInfo>();

            //connect to server and get list of antibex databases
            var connStr = $"Server={serverName};Database=Master;User ID={user};Password={pass}";

            using (var connection = new SqlConnection(connStr))
            {
                try
                {
                    try
                    {
                        connection.Open();
                    }
                    catch (Exception ex)
                    {
                        Log.Info($" error: {ex.Message}");
                        return dbList;
                    }
                    var command = connection.CreateCommand();

                    var sql = "SELECT SERVERPROPERTY('productversion')";
                    command.CommandText = sql;
                    var sVersion = (string)command.ExecuteScalar();
                    var sqlVersion = Version.Parse(sVersion);
                    var ver = SqlVersion(sqlVersion);

                    sql = @"SELECT d.name as dbname, isnull(mf.name, d.name) as logicalname, isnull((mf.size*8)/1024, 0) as SizeMB
                          FROM sys.databases d left join
                               sys.master_files mf on mf.database_id=d.database_id
                          where (d.name like 'UniversalCPR%')
                            and isnull(mf.data_space_id,1)=1";
                    command = connection.CreateCommand();
                    command.CommandText = sql;
                    command.CommandTimeout = 60;

                    var dt = new DataTable("DBS");
                    using (var adpt = new SqlDataAdapter(command))
                        adpt.Fill(dt);

                    //if database is sql2000 query each db for logical name
                    foreach (var dr in dt.Rows.OfType<DataRow>())
                    {
                        var dbi = new DbInfo
                        {
                            DbName = dr["dbname"].ToString(),
                            Description = dr["logicalname"].ToString(),
                            ServerName = serverName,
                            Version = ver,
                        };
                        dbi.ConnectionString = $"Server={serverName};Database={dbi.DbName};User ID=uouser;Password=passc0de";

                        // check if master_files is empty
                        var dbSize = int.Parse(dr["SizeMB"].ToString());
                        if (dbSize == 0)
                        {
                            using (var connection2 = new SqlConnection(dbi.ConnectionString))
                                try
                                {
                                    connection2.Open();

                                    var cmdDbInfo = connection2.CreateCommand();
                                    cmdDbInfo.CommandText = "select name from sys.database_files where type=0";

                                    var dtDbInfo = new DataTable("DbInfo");
                                    var adptDbInfo = new SqlDataAdapter(cmdDbInfo);
                                    adptDbInfo.Fill(dtDbInfo);

                                    var rowDbInfo = dtDbInfo.Rows.OfType<DataRow>().FirstOrDefault();
                                    if (rowDbInfo != null)
                                    {
                                        dbi.Description = rowDbInfo["name"].ToString();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error($"  {dbi.ConnectionString}", ex);
                                }
                        }

                        dbList.Add(dbi);
                    }
                    connection.Close();
                }
                catch(Exception ex)
                {
                    Log.Info("Exception while getting DB List: " + ex.Message);
                }
            }

            return dbList;
        }

        private static bool HasColumn(IDataRecord dr, string columnName)
        {
            for (var i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Create a map row and add it to the map
        /// </summary>
        /// <param name="mCommand"></param>
        //parses current row and adds it to the hcai map
        private void AddRowToMap2(SqlDataReader reader, HCAITableInfo map)
        {
            var tmp = new HCAITableLineItem();
            var name = "";

            for (var i = 0; i < reader.FieldCount; i++)
            {
                try
                {
                    name = reader.GetName(i);
                    
                    if (reader[i].ToString() == "")
                    {
                        continue;
                    }

                    if (name == "OCFType")
                    {
                        tmp.OCFType = (string)reader[i];
                    }
                    else if (name == "LockStatus")
                    {
                        tmp.LockStatus = Convert.ToInt32(reader[name]);
                    }
                    else if (name == "HCAIDocNumber")
                    {
                        if (reader[i] == null)
                            tmp.HCAINumber = "";
                        else
                            tmp.HCAINumber = (string)reader[i];
                    }
                    else if (name == "OCFID")
                    {
                        tmp.OCFID = (long)reader.GetDouble(i);
                    }
                    else if (name == "OCFStatus")
                    {
                        tmp.OCFStatus = reader.GetInt16(i);
                    }
                    else if (name == "StatusDate")
                    {
                        var dt = reader.GetDateTime(i);
                        tmp.StatusDate = $"{dt:MM}/{dt:dd}/{dt:yyyy}";
                    }
                    else if (name == "StatusTime")
                    {
                        tmp.StatusTime = reader.GetDateTime(i).ToString(HCAITableInfo.TimeFormat);
                    }
                    else if (name == "client_id")
                    {
                        tmp.client_id = (int)reader.GetDouble(i);
                    }
                    else if (name == "last_name")
                    {
                        tmp.last_name = (string)reader[i];
                    }
                    else if (name == "first_name")
                    {
                        tmp.first_name = (string)reader[i];
                    }
                    else if (name == "CaseID")
                    {
                        tmp.CaseID = (string)reader[i];
                    }
                    else if (name == "MVAAFirstName")
                    {
                        tmp.MVAAFirstName = (string)reader[i];
                    }
                    else if (name == "MVAALastName")
                    {
                        tmp.MVAALastName = (string)reader[i];
                    }
                    else if (name == "MVAInsName")
                    {
                        tmp.MVAInsName = (string)reader[i];
                    }
                    else if (name == "Total")
                    {
                        tmp.Total = MyConvert.ConvertStrToFloat(reader[name].ToString());

                    }
                    else if (name == "InvoiceVersion")
                    {
                        tmp.InvoiceVersion = (string)reader[i];
                    }
                    else if (name == "ATotal")
                    {
                        tmp.ATotal = (float)reader.GetDouble(i);
                    }
                    else if (name == "PlanNo")
                    {
                        tmp.PlanNo = reader.GetInt32(i);
                    }
                    else if (name == "NumberOfDays")
                    {
                        var str = reader[i].ToString();
                        if (!string.IsNullOrEmpty(str))
                            tmp.NumberOfDays = Convert.ToInt32(str);
                    }
                    else if (name == "IsYellow")
                    {
                        tmp.IsYellow = reader.GetInt32(i) != 0;
                    }
                    else if (name == "CategoryName")
                    {                        
                        tmp.CategoryName = (string)reader[i];
                    }
                    else if (name == "MinSentDate")
                    {
                        try
                        {
                            var dt = reader.GetDateTime(i);
                            tmp.MinSentDate = $"{dt:MM}/{dt:dd}/{dt:yyyy}";
                        }
                        catch
                        {
                            tmp.MinSentDate = "";
                        }
                    }
                    else if (name == "EHC1CompanyName")
                    {
                        tmp.EHC1CompanyName = (string)reader[i];
                    }
                    else if (name == "EHC2CompanyName")
                    {
                        tmp.EHC2CompanyName = (string)reader[i];
                    }
                }
                catch (Exception e)
                {
                    Log.Error("Exception while converting a property of a map row. " + e.Message, e);
                }
            }

            map.AddLineItemToMap(tmp);
        }

        /// <summary>
        /// Create a map row and add it to the map
        /// </summary>
        //parses current row and adds it to the hcai map
        private void AddRowToMap(SqlDataReader reader, HCAITableInfo map)
        {
            string s;
            int v;
            float f;
            long l;

            var t = typeof(HCAITableLineItem);
            var prop = t.GetProperties();
            var tmp = new HCAITableLineItem();

            foreach (PropertyInfo p in prop)
            {
                try
                {
                    if (HasColumn(reader, p.Name))
                    {
                        s = reader[p.Name].ToString();
                        if (s != "")
                        {
                            if (p.PropertyType == typeof(string))
                            {
                                p.SetValue(tmp, s, null);
                            }
                            else if (p.PropertyType == typeof(int))
                            {
                                v = Convert.ToInt32(s);
                                p.SetValue(tmp, v, null);
                            }
                            else if (p.PropertyType == typeof(float))
                            {
                                f = Convert.ToSingle(s);
                                p.SetValue(tmp, f, null);
                            }
                            else if (p.PropertyType == typeof(long))
                            {
                                l = Convert.ToInt64(s);
                                p.SetValue(tmp, l, null);
                            }
                        }
                        else
                        {
                            if (p.PropertyType == typeof(string))
                            {
                                p.SetValue(tmp, s, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error($"  {p.Name}", ex);
                    //do nothing skip to next field
                }
            }

            map.AddLineItemToMap(tmp);
        }

        private HCAITableInfo LoadStatusMap(SqlCommand command)
        {
            return LoadStatusMap(command, null);
        }

        private HCAITableInfo LoadStatusMap(SqlCommand command, HCAITableInfo m)
        {
            Log.Info("Loading new map");
            var map = m ?? new HCAITableInfo();

            command.CommandTimeout = 180;
            command.CommandText = @"SELECT DISTINCT H.LockStatus, H.HCAIDocNumber, H.OCFType, H.InvoiceID as OCFID, H.OCFStatus, H.StatusDate, H.StatusTime, " +
             @"C.client_id, C.last_name, C.first_name, CC.EHC1CompanyName, CC.EHC2CompanyName, CC.CaseID, M.IIAdjFirstName as [MVAAFirstName], M.IIAdjLastName " +
             @"as [MVAALastName], M.IIName as [MVAInsName], I.MVATotal as [Total], M.InvoiceVersion, " +
             @"M.AutoInsurerTotalApproved as [ATotal], " +
             @"(CASE WHEN H.OCFStatus > 11 " +
             @"THEN DATEDIFF(day, (select MAX(H2.StatusDate) from HCAIStatusLog H2 " +
             @"where H2.InvoiceID = H.InvoiceID AND H2.OCFStatus = 6), H.StatusDate) " +
             @"WHEN H.OCFStatus = 10 OR " +
             @"H.OCFStatus = 11 OR H.OCFStatus = 8 OR H.OCFStatus = 6 THEN DATEDIFF(day, H.StatusDate, convert(varchar(10), getdate(), 101)) " +
             @"ELSE NULL END) as NumberOfDays, " +
             @"(CASE WHEN H.OCFStatus > 11 THEN " +
             @"(CASE WHEN EXISTS " +
             @"(select * from HCAIStatusLog HY " +
             @"where HY.InvoiceID = H.InvoiceID AND HY.OCFStatus = 15 AND " +
             @"HY.StatusOrder = (select MAX(HTemp.StatusOrder) " +
             @"from HCAIStatusLog HTemp " +
             @"where HTemp.InvoiceID = H.InvoiceID AND HTemp.OCFStatus <> H.OCFStatus))" +             
             @"THEN 1 ELSE 0 END) " +
             @"ELSE 0 END) as IsYellow, " +
             @"(select CategoryName from TTCategories TTC where TTC.CategoryID = I.CategoryID) as CategoryName, " +
             @"(SELECT MIN(StatusDate) FROM HCAIStatusLog HD " +
             @"WHERE H.OCFStatus > 11 AND H.InvoiceID = HD.InvoiceID AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate " +
             @"FROM HCAIStatusLog H inner join (MVAInvoice M inner join " +
             @"(InvoceGeneralInfo I inner join (Client_Case CC inner join Client C on CC.client_id = C.client_id) on " +
             @"I.CaseID = CC.CaseID) on M.InvoiceID = I.InvoiceID) on H.InvoiceID = M.InvoiceID WHERE " +
             @"H.IsCurrentStatus = 1 AND H.Archived = 0 AND H.OCFStatus <> 1 ORDER BY H.StatusDate, H.StatusTime";

            
            using (var reader = command.ExecuteReader())
            {
                Log.Info("First query executed");

                //reading row from the query and populating map items
                var r = 0;
                while (reader.Read())
                {
                    AddRowToMap2(reader, map);
                    r++;
                }
                Log.InfoFormat("Building Map: 1st query # rows = {0}", r);
            }
            
            command.CommandTimeout = 1500;

            command.CommandText = @"SELECT DISTINCT H.LockStatus, H.HCAIDocNumber, H.OCFType, H.ReportID as OCFID, H.OCFStatus, H.StatusDate, H.StatusTime, " +
            @"C.client_id, C.last_name, C.first_name, CC.EHC1CompanyName, CC.EHC2CompanyName, CC.CaseID, O.MVAAFirstName, O.MVAALastName, O.MVAInsName, O.Total, " +
            @"O.ATotal, O.PlanNo, " +
            @"(CASE WHEN H.OCFStatus > 11 " +
            @"THEN ((select T1.WDay from CalendarTable T1 where " +
            @"T1.Date =  H.StatusDate) - ((select T2.WDay from CalendarTable T2 where T2.Date = (select MAX(H2.StatusDate) " +
            @"from HCAIStatusLog H2 where H2.ReportID = H.ReportID AND H2.OCFStatus = 6) + " +
            @"(CASE WHEN ((select T6.WDay from CalendarTable T6 where T6.Date =  convert(varchar(10), getdate(), 101)) - (select T5.WDay from CalendarTable T5 where T5.Date = H.StatusDate)) > 0 " +
            @"AND LTRIM(RTRIM(STUFF(RIGHT(CONVERT(VARCHAR,H.StatusTime,100 ) ,7), 6, 0, ' '))) > '4:45 PM' THEN 1 ELSE 0 END)))) " +
            @"WHEN H.OCFStatus = 10 OR H.OCFStatus = 11 OR H.OCFStatus = 8 OR H.OCFStatus = 6 THEN " +
            @"(((select T3.WDay from CalendarTable T3 where T3.Date =  convert(varchar(10), getdate(), 101)) - " +
            @"((select T4.WDay from CalendarTable T4 where T4.Date = H.StatusDate)  + " +
            @"(CASE WHEN ((select T8.WDay from CalendarTable T8 where T8.Date =  convert(varchar(10), getdate(), 101)) - (select T7.WDay from CalendarTable T7 where T7.Date = H.StatusDate)) > 0  " +
            @"AND LTRIM(RTRIM(STUFF(RIGHT(CONVERT(VARCHAR,H.StatusTime,100 ) ,7), 6, 0, ' '))) > '4:45 PM' THEN 1 ELSE 0 END)))) " +
            @"ELSE NULL END) as NumberOfDays, " +
            @"(CASE WHEN H.OCFStatus > 11 THEN " +
            @"(CASE WHEN EXISTS " +
            @"(select * from HCAIStatusLog HY " +
            @"where HY.ReportID = H.ReportID AND HY.OCFStatus = 15 AND " +
            @"HY.StatusOrder = (select MAX(HTemp.StatusOrder) " +
            @"from HCAIStatusLog HTemp " +
            @"where HTemp.ReportID = H.ReportID AND HTemp.OCFStatus <> H.OCFStatus)) " +            
            @"THEN 1 ELSE 0 END) " +
            @"ELSE 0 END) as IsYellow, " +
            @"(select CategoryName from TTCategories TTC where TTC.CategoryID = P.CategoryID) as CategoryName, " +
            @"(SELECT MIN(StatusDate) FROM HCAIStatusLog HD " +
            @"WHERE H.OCFStatus > 11 AND H.ReportID = HD.ReportID AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate " +
            @"FROM HCAIStatusLog H inner join " +
            @"((select ReportID, MVAAFirstName, MVAALastName, MVAInsName, Total, ATotal, PlanNo from OCF18 UNION " +
            @"select ReportID, MVAAFirstName, MVAALastName, MVAInsName, Total, ATotal, PlanNo from OCF22 UNION " +
            @"select ReportID, MVAAFirstName, MVAALastName, MVAInsName, Total, ATotal, PlanNo from OCF23) as O " +
            @"inner join (PaperWork P inner join (Client_Case CC inner join Client C on CC.client_id = C.client_id) " +
            @"on P.CaseID = CC.CaseID) on O.ReportID = P.ReportID) on H.ReportID = O.ReportID " +
            @"WHERE H.IsCurrentStatus = 1 AND H.Archived = 0 AND H.OCFStatus <> 1 " +
            @"ORDER BY H.StatusDate, H.StatusTime";

            using (var reader = command.ExecuteReader())
            {
                Log.Info("Plans query executed");
                //reading row from the query and populating map items
                var r = 0;
                while (reader.Read())
                {
                    AddRowToMap2(reader, map);
                    r++;
                }
                Log.InfoFormat("Building Map: 2nd query # rows = {0}", r);
            }

            command.CommandTimeout = 1500;
         

            command.CommandText = @"SELECT DISTINCT H.LockStatus, H.HCAIDocNumber, H.OCFType, H.ReportID as OCFID, H.OCFStatus, H.StatusDate, H.StatusTime, " +
            "C.client_id, C.last_name, C.first_name, CC.EHC1CompanyName, CC.EHC2CompanyName, CC.CaseID,  " +
            "F.MVAAFirstName, F.MVAALastName, F.MVAInsName,  " +
            "CONVERT(varchar, CONVERT(money, (ISNULL((CONVERT(DECIMAL(16,4), (select sum(NumberOfMin*TxWeeks) from FORM1_Parts123 F1 where F1.ReportID = F.ReportID AND gpartno = 1))/60.00*4.3*CONVERT(DECIMAL(16,4), F.Part1HourlyRate)), 0) + " +
            "ISNULL((CONVERT(DECIMAL(16,4), (select sum(NumberOfMin*TxWeeks) from FORM1_Parts123 F1 where F1.ReportID = F.ReportID AND gpartno = 2))/60.00*4.3*CONVERT(DECIMAL(16,4), F.Part2HourlyRate)), 0) + " +
            "ISNULL((CONVERT(DECIMAL(16,4), (select sum(NumberOfMin*TxWeeks) from FORM1_Parts123 F1 where F1.ReportID = F.ReportID AND gpartno = 3))/60.00*4.3*CONVERT(DECIMAL(16,4), F.Part3HourlyRate)), 0))), 0) as [Total], " +
            "(ISNULL(F.Part4_AR_Approved_Part_Benefit1, 0) + ISNULL(F.Part4_AR_Approved_Part_Benefit2, 0) + ISNULL(F.Part4_AR_Approved_Part_Benefit3, 0)) as ATotal, " +
			"(CASE WHEN H.OCFStatus > 11 " +
            "THEN ((select T1.WDay from CalendarTable T1 where  " +
            "T1.Date =  H.StatusDate) - ((select T2.WDay from CalendarTable T2 where T2.Date = (select MAX(H2.StatusDate)  " +
            "from HCAIStatusLog H2 where H2.ReportID = H.ReportID AND H2.OCFStatus = 6) +  " +
            "(CASE WHEN ((select T6.WDay from CalendarTable T6 where T6.Date =  convert(varchar(10), getdate(), 101)) - (select T5.WDay from CalendarTable T5 where T5.Date = H.StatusDate)) > 0  " +
            "AND LTRIM(RTRIM(STUFF(RIGHT(CONVERT(VARCHAR,H.StatusTime,100 ) ,7), 6, 0, ' '))) > '4:45 PM' THEN 1 ELSE 0 END))))  " +
            "WHEN H.OCFStatus = 10 OR H.OCFStatus = 11 OR H.OCFStatus = 8 OR H.OCFStatus = 6 THEN  " +
            "(((select T3.WDay from CalendarTable T3 where T3.Date =  convert(varchar(10), getdate(), 101)) -  " +
            "((select T4.WDay from CalendarTable T4 where T4.Date = H.StatusDate)  +  " +
            "(CASE WHEN ((select T8.WDay from CalendarTable T8 where T8.Date =  convert(varchar(10), getdate(), 101)) - (select T7.WDay from CalendarTable T7 where T7.Date = H.StatusDate)) > 0   " +
            "AND LTRIM(RTRIM(STUFF(RIGHT(CONVERT(VARCHAR,H.StatusTime,100 ) ,7), 6, 0, ' '))) > '4:45 PM' THEN 1 ELSE 0 END))))  " +
            "ELSE NULL END) as NumberOfDays,  " +
            "(CASE WHEN H.OCFStatus > 11 THEN (CASE WHEN EXISTS (select * from HCAIStatusLog HY  " +
            "where HY.ReportID = H.ReportID AND HY.OCFStatus = 15 AND HY.StatusOrder = (select MAX(HTemp.StatusOrder)  " +
            "from HCAIStatusLog HTemp where HTemp.ReportID = H.ReportID AND HTemp.OCFStatus <> H.OCFStatus)) THEN 1 ELSE 0 END) ELSE 0 END) as IsYellow,  " +
            "(SELECT MIN(StatusDate) FROM HCAIStatusLog HD WHERE H.OCFStatus > 11 AND H.ReportID = HD.ReportID AND HD.HCAIDocNumber = H.HCAIDocNumber AND HD.OCFStatus = 6) as MinSentDate  " +
            "FROM HCAIStatusLog H inner join (FORM1 F inner join (PaperWork P inner join (Client_Case CC inner join Client C on CC.client_id = C.client_id)  " +
            "on P.CaseID = CC.CaseID) on F.ReportID = P.ReportID) on H.ReportID = F.ReportID  " +
            "WHERE H.IsCurrentStatus = 1 AND H.Archived = 0 AND H.OCFStatus <> 1  " +
            "ORDER BY H.StatusDate, H.StatusTime";
            using (var reader = command.ExecuteReader())
            {
                Log.Info("Form1 query executed");
                //reading row from the query and populating map items
                var r = 0;
                while (reader.Read())
                {
                    AddRowToMap2(reader, map);
                    r++;
                }
                Log.InfoFormat("Building Map: Form1 query # rows = {0}", r);
            }

            return map;
        }
        
        #endregion


        private void RunServer()
        {
            Trace.AutoFlush = true;
            //Trace.WriteLine("Starting UOServer");

            Socket clientSock2 = null;
            try
            {
                _listener = new TcpListener(IPAddress.Any, Config.Port1);
                _listener.Start();
                _tmpListener = new TcpListener(IPAddress.Any, Config.Port2);
                _tmpListener.Start();
                //Trace.WriteLine("Starting UOServer");
                GC.SuppressFinalize(_listener); //Eugene-newAdd
                GC.SuppressFinalize(_tmpListener);

                Debugger.WriteToFile("Server Started", 10);


                while (!_stopServer)
                {
                    //wait for a connection request (blocking call)
                    if (_listener.Pending())
                    {
                        Debugger.WriteToFile("Connection 1 accepted.", 10);
                        //TcpListener tmpListener = new TcpListener(IPAddress.Any, mPort2);
                        //tmpListener.Start();

                        var clientSock = _listener.AcceptSocket();

                        //two connection scenario
                        int t = 0;  //will wait for a second connection for couple of seconds
                        while (t < 20)
                        {
                            Debugger.WriteToFile("Waiting for connection 2.", 10);
                            if (_tmpListener.Pending())
                            {
                                Debugger.WriteToFile("Connection 2 accepted.", 10);
                                //ONLY Accept if request is from the same IP
                                clientSock2 = _tmpListener.AcceptSocket();

                                var ip1 = (IPEndPoint)clientSock.RemoteEndPoint;
                                var ip2 = (IPEndPoint)clientSock2.RemoteEndPoint;

                                if (!ip1.Address.Equals(ip2.Address))
                                {
                                    //illegal connection
                                    Debugger.WriteToFile("Illegal connection (wrong ip). Connection refused.", 10);

                                    clientSock.Close();
                                    clientSock2.Close();
                                }
                                t = -1;
                                break;
                            }
                            t++;
                            Thread.Sleep(200);
                        }

                        Log.Info("Final checks...");
                        //if t < 0 the second connection has been established
                        if (t < 0 && clientSock.Connected && clientSock2.Connected)
                        {
                            //connection requested and accepted
                            var client = new ClientConnection(mClientNumber, clientSock2, clientSock, this);
                            client.NewMassageEvent += OnNewMessageEvent;

                            mClientNumber++;
                            lock(_clientArrayLocker)
                            {
                                _clients.Add(client);
                            }

                            client.Start();
                            Log.Info("All is good starting the client.");
                        }
                    }

                    Thread.Sleep(200);
                }

                Debugger.WriteToFile("Server Exited", 10);
                _listener.Stop();
                _tmpListener.Stop();
            }
            catch (Exception error)
            {
                _listener.Stop();
                _tmpListener?.Stop();
                Debugger.WriteToFile("Server Crashed: " + error.Message, 10);
                Log.Fatal("Server Crashed: " + error.Message, error);
            }

            GC.ReRegisterForFinalize(_listener); //Eugene-newAdd
            if (_tmpListener != null)
            {
                GC.ReRegisterForFinalize(_tmpListener);
            }
        }

        private void StopServer()
        {
            Debugger.WriteToFile("Stopping Server.", 10);
            _stopServer = true;
            if (_serverThread != null && _serverThread.IsAlive)
            {
                if (!_serverThread.Join(2000))
                {
                    _serverThread.Abort();
                }
                _listener.Stop();
            }

            Debugger.WriteToFile("Clearing Client Connections.", 10);
            lock(_clientArrayLocker)
            {
                foreach (ClientConnection cl in _clients)
                {
                    cl.Stop();
                }

                _clients.Clear();
            }
            Debugger.WriteToFile("StopServer Finished.", 10);
        }


        public void RemoveDeadClients()
        {
            var tmp = new ArrayList();
            try
            {
                Debugger.WriteToFile("RemoveDeadClients: Enter", 10);
                lock(_clientArrayLocker)
                {
                    foreach (ClientConnection cl in _clients)
                    {
                        if (!cl.IsRunning())
                        {
                            Debugger.WriteToFile("Client " + cl.ClientName + " is not running. Removing client.", 10);
                            tmp.Add(cl);
                        }
                        else if (cl.IsStuck())
                        {
                            Debugger.WriteToFile("Client " + cl.ClientName + " got stuck. Aborting thread.", 10);
                            tmp.Add(cl);
                            cl.Stop();
                        }
                        else if (cl.HasBeenRunningTooLong())
                        {
                            Debugger.WriteToFile("Client " + cl.ClientName +
                                                " has been running too long.\nDisconnecting and aborting thread.", 10);
                            var m = new UOMessage(Commands.DISCONNECT);
                            cl.AddCommand(m);
                            Thread.Sleep(200);
                            tmp.Add(cl);
                            cl.Stop();
                        }
                    }

                    foreach (ClientConnection cl in tmp)
                    {
                        Debugger.WriteToFile("Client " + cl.ClientName + " was stopped and removed.", 10);
                        cl.Stop();
                        _clients.Remove(cl);
                    }
                    tmp.Clear();
                }
            }
            catch (Exception e)
            {
                Debugger.WriteToFile("RemoverDeadClient exception: " + e.Message, 10);
                Log.Error("Exception while removing dead clients: " + e.Message, e);
            }
            Debugger.WriteToFile("RemoveDeadClients: Exit", 10);
        }

        public void DumpClientsInfo()
        {
            lock(_clientArrayLocker)
            {
                Debugger.WriteToFile("<<<Clients Dump:", 9);
                foreach (ClientConnection cl in _clients)
                {
                    Debugger.WriteToFile("Name: " + cl.ClientName +
                                        "; Created: " + cl.CreationTime.ToString("g", new CultureInfo("en-US")), 9);
                    Debugger.WriteToFile("\tIn Command running time: " + cl.InCommandRunningTime.ToString(), 9);
                    Debugger.WriteToFile("\tOut Command running time: " + cl.OutCommandRunningTime.ToString(), 9);
                    Debugger.WriteToFile("---", 9);
                }
                Debugger.WriteToFile(">>>", 9);
            }
        }

        #region ClientArray Functions

        public void RemoveClientFromArray(ClientConnection client)
        {
            lock(_clientArrayLocker)
            {
                _clients.Remove(client);
            }
        }

        public void ChangeCredentialsInAllClients(string dbname, string id, string login, string pass)
        {
            lock (_clientArrayLocker)
            {
                foreach (ClientConnection cl in _clients)
                {
                    if (cl.DBName == dbname)
                        cl.ChangeHCAICredentials(id, login, pass);
                }
            }
        }

        public void ChangeHcvCredentialsInAllClients(string dbname)
        {
            lock (_clientArrayLocker)
            {
                foreach (ClientConnection cl in _clients)
                {
                    if (cl.DBName == dbname)
                        cl.ResetHcvModule();
                }
            }
        }

        public void SendMessageToAllOtherClients(ClientConnection client, UOMessage msg)
        {
            lock (_clientArrayLocker)
            {
                foreach (ClientConnection cl in _clients)
                {
                    //Send to clients with the same DBName except for myself
                    if (client != cl && client.DBName == cl.DBName)
                    {
                        cl.AddCommand(msg);
                    }
                }
            }
        }

        #endregion
    }
}
