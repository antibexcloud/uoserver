﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.BaseMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;

namespace PMSToolkit.Mapper
{
  internal abstract class BaseMapper : IMapper
  {
    protected string getSubmissionHeader(object sEncodedFICRValue)
    {
      return string.Format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><{1} xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:hl7-org:v3\" xsi:schemaLocation=\"urn:hl7-org:v3 {1}.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.2\" /><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30.1.2\" extension=\"{1}\"/><processingCode code=\"P\"/><processingModeCode code=\"P\"/><acceptAckCode code=\"AL\"/>", (object) DateTime.Now.ToString("yyyyMMddHHmmss"), sEncodedFICRValue);
    }

    protected string getReceiverElement(object sEncodedIBCInsurerID, object sEncodedInsurerBranchID, object sEncodedAdjusterTelephone, object sEncodedAdjusterExtension, object sEncodedAdjusterFax, object sEncodedAdjusterFirstName, object sEncodedAdjusterLastName)
    {
      return string.Format("<receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/><asAgent><representedOrganization><id nullFlavor=\"NI\"/><notificationParty><id root=\"2.16.840.1.113883.3.30.2\" extension=\"{0}\"/><contactOrganization><id root=\"2.16.840.1.113883.3.30.2\" extension=\"{1}\"/><contactParty><telecom value=\"tel:{2};ext={3}\"/><telecom value=\"fax:{4}\"/><contactPerson><name><given>{5}</given><family>{6}</family></name></contactPerson></contactParty></contactOrganization></notificationParty></representedOrganization></asAgent></device></receiver>", sEncodedIBCInsurerID, sEncodedInsurerBranchID, sEncodedAdjusterTelephone, sEncodedAdjusterExtension, sEncodedAdjusterFax, sEncodedAdjusterFirstName, sEncodedAdjusterLastName);
    }

    protected string getSenderElement(object oSoftwareName, object oSoftwareVersion)
    {
      return string.Format("<sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/><softwareName codeSystemVersion=\"{1}\">{0}</softwareName></device></sender>", oSoftwareName, oSoftwareVersion);
    }

    protected string getCreditElement(object oFacilityRegistryID, object oPayeeName)
    {
      return string.Format("<credit><account><holder><payeeRole classCode=\"PROV\"><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{0}\"/><payeeOrganization><name>{1}</name></payeeOrganization></payeeRole></holder></account></credit>", oFacilityRegistryID, oPayeeName);
    }

    public virtual IDataItem transform(IDataItem data)
    {
      return (IDataItem) null;
    }
  }
}
