﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitResponseMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Xml;

namespace PMSToolkit.Mapper
{
  internal class SubmitResponseMapper : BaseMapper
  {
    public override IDataItem transform(IDataItem data)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      xmlDocument.LoadXml(xml);
      SubmitResponseKeys submitResponseKeys = new SubmitResponseKeys();
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      nsmgr.AddNamespace("hl7", "urn:hl7-org:v3");
      string str = string.Empty;
      string empty1 = string.Empty;
      string empty2 = string.Empty;
      string empty3 = string.Empty;
      string empty4 = string.Empty;
      string empty5 = string.Empty;
      IDataItem dataItem = (IDataItem) new SubmitResponse();
      try
      {
        string innerText1 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.HCAI_Document_Number + "\"]", nsmgr).InnerText;
        string innerText2 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:targetMessage/hl7:id/@extension", nsmgr).InnerText;
        string innerText3 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.PMSFields_PMSPatientKey + "\"]", nsmgr).InnerText;
        dataItem.addValue(submitResponseKeys.HCAI_Document_Number, innerText1);
        dataItem.addValue(submitResponseKeys.PMSFields_PMSDocumentKey, innerText2);
        dataItem.addValue(submitResponseKeys.PMSFields_PMSPatientKey, innerText3);
        try
        {
          str = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type + "\"]", nsmgr).InnerText;
          str = "OCF21B";
        }
        catch (Exception ex)
        {
        }
        try
        {
          if (str == string.Empty)
          {
            str = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type + "\"]", nsmgr).InnerText;
            str = "OCF21C";
          }
        }
        catch (Exception ex)
        {
        }
        if (!string.IsNullOrEmpty(str) && str.Equals("OCF21B"))
        {
          string innerText4 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type + "\"]", nsmgr).InnerText;
          string innerText5 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate + "\"]", nsmgr).InnerText;
          string innerText6 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber + "\"]", nsmgr).InnerText;
          string innerText7 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved + "\"]", nsmgr).InnerText;
          string innerText8 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled + "\"]", nsmgr).InnerText;
          dataItem.addValue(submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type, innerText4);
          dataItem.addValue(submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled, innerText8);
          dataItem.addValue(submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber, innerText6);
          dataItem.addValue(submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate, innerText5);
          dataItem.addValue(submitResponseKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved, innerText7);
        }
        if (!string.IsNullOrEmpty(str))
        {
          if (str.Equals("OCF21C"))
          {
            string innerText4 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type + "\"]", nsmgr).InnerText;
            string innerText5 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate + "\"]", nsmgr).InnerText;
            string innerText6 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber + "\"]", nsmgr).InnerText;
            string innerText7 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved + "\"]", nsmgr).InnerText;
            string innerText8 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail/hl7:text[following-sibling::hl7:location=\"" + submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled + "\"]", nsmgr).InnerText;
            dataItem.addValue(submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type, innerText4);
            dataItem.addValue(submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled, innerText8);
            dataItem.addValue(submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber, innerText6);
            dataItem.addValue(submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate, innerText5);
            dataItem.addValue(submitResponseKeys.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved, innerText7);
          }
        }
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Data in response XML not found where expected.", ex);
      }
      return dataItem;
    }
  }
}
