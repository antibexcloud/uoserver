﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.ApplicantTypeName
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Xml.Serialization;

namespace PMSToolkit.Mapper
{
  [XmlType(AnonymousType = true, Namespace = "http://hcai.ca")]
  public class ApplicantTypeName
  {
    public string FirstName { get; set; }

    public string MiddleName { get; set; }

    public string LastName { get; set; }
  }
}
