﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitFacilityUpdateMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;

namespace PMSToolkit.Mapper
{
  internal class SubmitFacilityUpdateMapper : BaseMapper
  {
    private FacilityUpdateKeys m_facilityUpdateKeys = new FacilityUpdateKeys();

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Facility xmlns=\"http://hcai.ca\">\r\n    <FacilityRegistryNumber>{40}</FacilityRegistryNumber>\r\n\t<FacilityName>{0}</FacilityName>\r\n\t<CorporationNumber>{1}</CorporationNumber>\r\n\t<BillingAddress>\r\n\t\t<StreetAddress1>{2}</StreetAddress1>\r\n\t\t<StreetAddress2>{3}</StreetAddress2>\r\n\t\t<City>{4}</City>\r\n\t\t<Province>{5}</Province>\r\n\t\t<PostalCode>{6}</PostalCode>\r\n\t</BillingAddress>\r\n    <SameServiceAddress>{7}</SameServiceAddress>\r\n    <ServiceAddress>\r\n\t\t<StreetAddress1>{8}</StreetAddress1>\r\n\t\t<StreetAddress2>{9}</StreetAddress2>\r\n\t\t<City>{10}</City>\r\n\t\t<Province>{11}</Province>\r\n\t\t<PostalCode>{12}</PostalCode>\r\n\t</ServiceAddress>\r\n\t<AISIFacilityNumber>{13}</AISIFacilityNumber>\r\n\t<Telephone>{14}</Telephone>\r\n\t<Extension>{15}</Extension>\r\n\t<Fax>{16}</Fax>\r\n\t<AuthorizingOfficerEmail>{17}</AuthorizingOfficerEmail>\r\n\t<AuthorizingOfficerConfirmEmail>{18}</AuthorizingOfficerConfirmEmail>\r\n\t<DefaultPerkmRate>{19}</DefaultPerkmRate>\r\n\t<ContactOne>\r\n\t\t<FirstName>{20}</FirstName>\r\n\t\t<LastName>{21}</LastName>\r\n\t</ContactOne>\r\n\t<ContactOneTitle>{22}</ContactOneTitle>\r\n\t<ContactOneEmail>{23}</ContactOneEmail>\r\n\t<ContactOneConfirmEmail>{24}</ContactOneConfirmEmail>\r\n\t<ContactOneTelephone>{25}</ContactOneTelephone>\r\n\t<ContactOneTelephoneExtension>{26}</ContactOneTelephoneExtension>\r\n\t<ContactTwo>\r\n\t\t<FirstName>{27}</FirstName>\r\n\t\t<LastName>{28}</LastName>\r\n\t</ContactTwo>\r\n\t<ContactTwoTitle>{29}</ContactTwoTitle>\r\n\t<ContactTwoEmail>{30}</ContactTwoEmail>\r\n\t<ContactTwoConfirmEmail>{31}</ContactTwoConfirmEmail>\r\n\t<ContactTwoTelephone>{32}</ContactTwoTelephone>\r\n\t<ContactTwoTelephoneExtension>{33}</ContactTwoTelephoneExtension>\r\n\t<MakeChequePayableTo>{34}</MakeChequePayableTo>\r\n\t<LockPayableFlag>{35}</LockPayableFlag>\r\n\t<PayeeNumber>{36}</PayeeNumber>\r\n\t<PayeeName>\r\n\t\t\t<FirstName>{37}</FirstName>\r\n\t\t\t<LastName>{38}</LastName>\r\n\t</PayeeName>\r\n\t<PMSVendor>{39}</PMSVendor>\r\n</Facility>\r\n", (object[]) this.getFacilityUpdateStringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue(new PMSResponseKeys().PMSResponseMessageKey, val);
      return (IDataItem) responseMessage;
    }

    private string[] getFacilityUpdateStringArgs(IDataItem data)
    {
      return new string[41]
      {
        data.getEncodedValue(this.m_facilityUpdateKeys.Facility_Name),
        data.getEncodedValue(this.m_facilityUpdateKeys.CorporationNumber),
        data.getEncodedValue(this.m_facilityUpdateKeys.Billing_Street_Address_1),
        data.getEncodedValue(this.m_facilityUpdateKeys.Billing_Street_Address_2),
        data.getEncodedValue(this.m_facilityUpdateKeys.Billing_City),
        data.getEncodedValue(this.m_facilityUpdateKeys.Billing_Province),
        data.getEncodedValue(this.m_facilityUpdateKeys.Billing_PostalCode),
        data.getEncodedValue(this.m_facilityUpdateKeys.SameServiceAddress),
        data.getEncodedValue(this.m_facilityUpdateKeys.Service_Street_Address_1),
        data.getEncodedValue(this.m_facilityUpdateKeys.Service_Street_Address_2),
        data.getEncodedValue(this.m_facilityUpdateKeys.Service_City),
        data.getEncodedValue(this.m_facilityUpdateKeys.Service_Province),
        data.getEncodedValue(this.m_facilityUpdateKeys.Service_PostalCode),
        data.getEncodedValue(this.m_facilityUpdateKeys.AISIFacilityNumber),
        data.getEncodedValue(this.m_facilityUpdateKeys.Telephone),
        data.getEncodedValue(this.m_facilityUpdateKeys.Extension),
        data.getEncodedValue(this.m_facilityUpdateKeys.Fax),
        data.getEncodedValue(this.m_facilityUpdateKeys.AuthorizingOfficerEmail),
        data.getEncodedValue(this.m_facilityUpdateKeys.AuthorizingOfficerConfirmEmail),
        data.getEncodedValue(this.m_facilityUpdateKeys.DefaultPerkmRate),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOne_FirstName),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOne_LastName),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOneTitle),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOneEmail),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOneConfirmEmail),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOneTelephone),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactOneTelephoneExtension),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwo_FirstName),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwo_LastName),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwoTitle),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwoEmail),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwoConfirmEmail),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwoTelephone),
        data.getEncodedValue(this.m_facilityUpdateKeys.ContactTwoTelephoneExtension),
        data.getEncodedValue(this.m_facilityUpdateKeys.MakeChequePayableTo),
        data.getEncodedValue(this.m_facilityUpdateKeys.LockPayableFlag),
        data.getEncodedValue(this.m_facilityUpdateKeys.PayeeNumber),
        data.getEncodedValue(this.m_facilityUpdateKeys.PayeeName_FirstName),
        data.getEncodedValue(this.m_facilityUpdateKeys.PayeeName_LastName),
        data.getEncodedValue(this.m_facilityUpdateKeys.PMSVendor),
        data.getEncodedValue(this.m_facilityUpdateKeys.FacilityRegistryNumber)
      };
    }
  }
}
