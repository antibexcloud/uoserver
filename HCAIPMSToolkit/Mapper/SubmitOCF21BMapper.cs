﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitOCF21BMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Collections;

namespace PMSToolkit.Mapper
{
  internal class SubmitOCF21BMapper : BaseMapper
  {
    private OCF21BKeys m_ocf21BKeys = new OCF21BKeys();
    private const string m_xmlTemplate = "{0}{22}{2}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{4}\"/><amt value=\"{71}\"/>{99}{40}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"CSPINV\"/><netAmt value=\"{71}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF21B><InvoiceInformation><InvoiceNumber>{31}</InvoiceNumber><FirstInvoice>{32}</FirstInvoice><LastInvoice>{33}</LastInvoice></InvoiceInformation><PreviouslyApprovedGoodsAndServices><Type>{34}</Type><PlanDate>{35}</PlanDate><PlanNumber>{36}</PlanNumber><AmountApproved>{37}</AmountApproved><PreviouslyBilled>{38}</PreviouslyBilled></PreviouslyApprovedGoodsAndServices><Payee><ConflictOfInterest><ConflictExists>{41}</ConflictExists><ConflictDetails>{42}</ConflictDetails></ConflictOfInterest></Payee><InjuriesAndSequelae>{76}</InjuriesAndSequelae><OtherInsurance><IsThereOtherInsuranceCoverage>{78}</IsThereOtherInsuranceCoverage></OtherInsurance><OtherInsuranceAmounts><OtherServiceTypeForDebits>{97}</OtherServiceTypeForDebits><IsAmountRefusedByOtherInsurance>{98}</IsAmountRefusedByOtherInsurance><OtherServiceType>{59}</OtherServiceType></OtherInsuranceAmounts><AccountActivity><PriorBalance>{60}</PriorBalance><PaymentReceivedFromAutoInsurer>{61}</PaymentReceivedFromAutoInsurer><OverdueAmount>{62}</OverdueAmount></AccountActivity><OtherInformation>{72}</OtherInformation><AdditionalComments>{73}</AdditionalComments><AttachmentsBeingSent>{74}</AttachmentsBeingSent><OCFVersion>{1}</OCFVersion><PatientKey>{5}</PatientKey></OCF21B></inlineData></value></healthDocumentAttachment></pertinentInformation1>{79}<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {7}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {6}/><code {28}/><coveredPartyAsPatientPerson><name><given>{9}</given><given>{10}</given><family>{11}</family></name><administrativeGenderCode code=\"{13}\"/><birthTime value=\"{12}\"/><addr><streetAddressLine>{16}</streetAddressLine><streetAddressLine>{17}</streetAddressLine><city>{18}</city><state>{19}</state><postalCode>{20}</postalCode></addr><telecom value=\"tel:{14};ext={15}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{29}</given><family>{30}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{8}\"/></accident></pertinentInformation></policyOrAccount></coverage>{75}<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"ReimbursableGoodsAndServices\"/><code code=\"CSPINV\"/><netAmt value=\"{70}\"/>{77}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{65}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHAmounts\"/><code code=\"FININV\"/><netAmt value=\"{63}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHDebits\"/><code code=\"FININV\"/>{86}{82}{83}{84}{85}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/>{48}{44}{45}{46}{47}</invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><netAmt value=\"{64}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1Debits\"/><code code=\"FININV\"/>{91}{87}{88}{89}{90}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2Debits\"/><code code=\"FININV\"/>{96}{92}{93}{94}{95}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1\"/><code code=\"FININV\"/>{53}{49}{50}{51}{52}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2\"/><code code=\"FININV\"/>{58}{54}{55}{56}{57}</invoiceElementGroup></component></invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{69}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{66}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{66}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{67}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{67}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"INTEREST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{68}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{68}\"/></invoiceElementDetail></component></invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN600204UV02>";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("{0}{22}{2}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{4}\"/><amt value=\"{71}\"/>{99}{40}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"CSPINV\"/><netAmt value=\"{71}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF21B><InvoiceInformation><InvoiceNumber>{31}</InvoiceNumber><FirstInvoice>{32}</FirstInvoice><LastInvoice>{33}</LastInvoice></InvoiceInformation><PreviouslyApprovedGoodsAndServices><Type>{34}</Type><PlanDate>{35}</PlanDate><PlanNumber>{36}</PlanNumber><AmountApproved>{37}</AmountApproved><PreviouslyBilled>{38}</PreviouslyBilled></PreviouslyApprovedGoodsAndServices><Payee><ConflictOfInterest><ConflictExists>{41}</ConflictExists><ConflictDetails>{42}</ConflictDetails></ConflictOfInterest></Payee><InjuriesAndSequelae>{76}</InjuriesAndSequelae><OtherInsurance><IsThereOtherInsuranceCoverage>{78}</IsThereOtherInsuranceCoverage></OtherInsurance><OtherInsuranceAmounts><OtherServiceTypeForDebits>{97}</OtherServiceTypeForDebits><IsAmountRefusedByOtherInsurance>{98}</IsAmountRefusedByOtherInsurance><OtherServiceType>{59}</OtherServiceType></OtherInsuranceAmounts><AccountActivity><PriorBalance>{60}</PriorBalance><PaymentReceivedFromAutoInsurer>{61}</PaymentReceivedFromAutoInsurer><OverdueAmount>{62}</OverdueAmount></AccountActivity><OtherInformation>{72}</OtherInformation><AdditionalComments>{73}</AdditionalComments><AttachmentsBeingSent>{74}</AttachmentsBeingSent><OCFVersion>{1}</OCFVersion><PatientKey>{5}</PatientKey></OCF21B></inlineData></value></healthDocumentAttachment></pertinentInformation1>{79}<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {7}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {6}/><code {28}/><coveredPartyAsPatientPerson><name><given>{9}</given><given>{10}</given><family>{11}</family></name><administrativeGenderCode code=\"{13}\"/><birthTime value=\"{12}\"/><addr><streetAddressLine>{16}</streetAddressLine><streetAddressLine>{17}</streetAddressLine><city>{18}</city><state>{19}</state><postalCode>{20}</postalCode></addr><telecom value=\"tel:{14};ext={15}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{29}</given><family>{30}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{8}\"/></accident></pertinentInformation></policyOrAccount></coverage>{75}<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"ReimbursableGoodsAndServices\"/><code code=\"CSPINV\"/><netAmt value=\"{70}\"/>{77}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{65}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHAmounts\"/><code code=\"FININV\"/><netAmt value=\"{63}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHDebits\"/><code code=\"FININV\"/>{86}{82}{83}{84}{85}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/>{48}{44}{45}{46}{47}</invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><netAmt value=\"{64}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1Debits\"/><code code=\"FININV\"/>{91}{87}{88}{89}{90}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2Debits\"/><code code=\"FININV\"/>{96}{92}{93}{94}{95}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1\"/><code code=\"FININV\"/>{53}{49}{50}{51}{52}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2\"/><code code=\"FININV\"/>{58}{54}{55}{56}{57}</invoiceElementGroup></component></invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{69}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{66}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{66}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{67}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{67}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"INTEREST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{68}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{68}\"/></invoiceElementDetail></component></invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN600204UV02>", (object[]) this.getOCF21BStringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private string[] getOCF21BStringArgs(IDataItem data)
    {
      string[] strArray = new string[100];
      string encodedValue1 = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_FacilityRegistryID);
      string encodedValue2 = data.getEncodedValue(this.m_ocf21BKeys.PMSFields_PMSDocumentKey);
      string encodedValue3 = data.getEncodedValue(this.m_ocf21BKeys.Insurer_IBCInsurerID);
      strArray[0] = this.getSubmissionHeader((object) "FICR_IN600204UV02");
      strArray[22] = this.getReceiverElement((object) encodedValue3, (object) data.getEncodedValue(this.m_ocf21BKeys.Insurer_IBCBranchID), (object) data.getEncodedValue(this.m_ocf21BKeys.Insurer_Adjuster_TelephoneNumber), (object) data.getEncodedValue(this.m_ocf21BKeys.Insurer_Adjuster_TelephoneExtension), (object) data.getEncodedValue(this.m_ocf21BKeys.Insurer_Adjuster_FaxNumber), (object) data.getEncodedValue(this.m_ocf21BKeys.Insurer_Adjuster_Name_FirstName), (object) data.getEncodedValue(this.m_ocf21BKeys.Insurer_Adjuster_Name_LastName));
      strArray[1] = data.getEncodedValue(this.m_ocf21BKeys.OCFVersion);
      strArray[2] = this.getSenderElement((object) data.getEncodedValue(this.m_ocf21BKeys.PMSFields_PMSSoftware), (object) data.getEncodedValue(this.m_ocf21BKeys.PMSFields_PMSVersion));
      strArray[4] = encodedValue2;
      strArray[40] = !(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_FacilityIsPayee) == "Yes") ? SubmitOCF21BMapper.GetFacilityIsNotPayeeCreditElement(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_MakeChequePayableTo), data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_FacilityIsPayee)) : SubmitOCF21BMapper.GetFacilityIsPayeeCreditElement(encodedValue1, data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_MakeChequePayableTo));
      strArray[5] = data.getEncodedValue(this.m_ocf21BKeys.PMSFields_PMSPatientKey);
      strArray[6] = this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.Header_ClaimNumber));
      strArray[7] = this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.Header_PolicyNumber));
      strArray[8] = data.getEncodedValue(this.m_ocf21BKeys.Header_DateOfAccident);
      strArray[9] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_FirstName);
      strArray[10] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_MiddleName);
      strArray[11] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_LastName);
      strArray[12] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_DateOfBirth);
      strArray[13] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Gender);
      strArray[14] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_TelephoneNumber);
      strArray[15] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_TelephoneExtension);
      strArray[16] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Address_StreetAddress1);
      strArray[17] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Address_StreetAddress2);
      strArray[18] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Address_City);
      strArray[19] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Address_Province);
      strArray[20] = data.getEncodedValue(this.m_ocf21BKeys.Applicant_Address_PostalCode);
      strArray[21] = encodedValue3;
      strArray[28] = ConvertUtil.convertPolicyHolderSameAsToNeCST(data.getEncodedValue(this.m_ocf21BKeys.Insurer_PolicyHolder_IsSameAsApplicant));
      strArray[29] = data.getEncodedValue(this.m_ocf21BKeys.Insurer_PolicyHolder_Name_FirstName);
      strArray[30] = data.getEncodedValue(this.m_ocf21BKeys.Insurer_PolicyHolder_Name_LastName);
      strArray[31] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InvoiceInformation_InvoiceNumber);
      strArray[32] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InvoiceInformation_FirstInvoice);
      strArray[33] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InvoiceInformation_LastInvoice);
      strArray[34] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type);
      strArray[35] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate);
      strArray[36] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber);
      strArray[37] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved);
      strArray[38] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled);
      strArray[39] = encodedValue1;
      strArray[41] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_ConflictOfInterests_ConflictExists);
      strArray[42] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_Payee_ConflictOfInterests_ConflictDetails);
      strArray[43] = (string) null;
      strArray[44] = this.getOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Chiropractic));
      strArray[45] = this.getOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy));
      strArray[46] = this.getOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy));
      strArray[47] = this.getOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_OtherService));
      if (string.IsNullOrEmpty(strArray[44]) && string.IsNullOrEmpty(strArray[45]) && (string.IsNullOrEmpty(strArray[46]) && string.IsNullOrEmpty(strArray[47])))
        strArray[47] = this.getEmptyComponent();
      strArray[48] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed));
      strArray[49] = this.getOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic));
      strArray[50] = this.getOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy));
      strArray[51] = this.getOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy));
      strArray[52] = this.getOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService));
      if (string.IsNullOrEmpty(strArray[49]) && string.IsNullOrEmpty(strArray[50]) && (string.IsNullOrEmpty(strArray[51]) && string.IsNullOrEmpty(strArray[52])))
        strArray[52] = this.getEmptyComponent();
      strArray[53] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed));
      strArray[54] = this.getOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic));
      strArray[55] = this.getOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy));
      strArray[56] = this.getOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy));
      strArray[57] = this.getOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService));
      if (string.IsNullOrEmpty(strArray[54]) && string.IsNullOrEmpty(strArray[55]) && (string.IsNullOrEmpty(strArray[56]) && string.IsNullOrEmpty(strArray[57])))
        strArray[57] = this.getEmptyComponent();
      strArray[58] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed));
      strArray[59] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_OtherServiceType);
      strArray[60] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_AccountActivity_PriorBalance);
      strArray[61] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer);
      strArray[62] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_AccountActivity_OverdueAmount);
      strArray[63] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_MOH_Proposed);
      strArray[64] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_OtherInsurers_Proposed);
      strArray[66] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_GST_Proposed);
      strArray[67] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_PST_Proposed);
      strArray[68] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_Interest_Proposed);
      try
      {
        strArray[69] = (Decimal.Parse(strArray[66]) + Decimal.Parse(strArray[67]) + Decimal.Parse(strArray[68])).ToString();
      }
      catch
      {
        strArray[69] = "0.0";
      }
      strArray[70] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_SubTotal_Proposed);
      strArray[71] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed);
      strArray[72] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInformation);
      strArray[73] = data.getEncodedValue(this.m_ocf21BKeys.AdditionalComments);
      strArray[74] = data.getEncodedValue(this.m_ocf21BKeys.AttachmentsBeingSent);
      strArray[75] = this.getOtherInsuranceCoverage(data);
      strArray[76] = this.getInjuries(data);
      strArray[77] = this.getGSLineItems(data);
      strArray[78] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage);
      strArray[79] = this.getHCAIPlanDocNumber(data);
      strArray[80] = "";
      strArray[81] = "";
      strArray[82] = this.getOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic));
      strArray[83] = this.getOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy));
      strArray[84] = this.getOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy));
      strArray[85] = this.getOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService));
      if (string.IsNullOrEmpty(strArray[82]) && string.IsNullOrEmpty(strArray[83]) && (string.IsNullOrEmpty(strArray[84]) && string.IsNullOrEmpty(strArray[85])))
        strArray[85] = this.getEmptyComponent();
      strArray[86] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed));
      strArray[87] = this.getOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic));
      strArray[88] = this.getOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy));
      strArray[89] = this.getOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy));
      strArray[90] = this.getOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService));
      if (string.IsNullOrEmpty(strArray[87]) && string.IsNullOrEmpty(strArray[88]) && (string.IsNullOrEmpty(strArray[89]) && string.IsNullOrEmpty(strArray[90])))
        strArray[90] = this.getEmptyComponent();
      strArray[91] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed));
      strArray[92] = this.getOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic));
      strArray[93] = this.getOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy));
      strArray[94] = this.getOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy));
      strArray[95] = this.getOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService));
      if (string.IsNullOrEmpty(strArray[92]) && string.IsNullOrEmpty(strArray[93]) && (string.IsNullOrEmpty(strArray[94]) && string.IsNullOrEmpty(strArray[95])))
        strArray[95] = this.getEmptyComponent();
      strArray[96] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed));
      strArray[97] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType);
      try
      {
        strArray[65] = (Decimal.Parse(strArray[63]) + Decimal.Parse(strArray[64])).ToString();
      }
      catch
      {
        strArray[65] = "0.0";
      }
      strArray[98] = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsuranceAmounts_IsAmountRefused);
      strArray[99] = SubmitOCF21BMapper.GetPrimaryPerformer(encodedValue1);
      return strArray;
    }

    private string getOtherInsuranceCoverage(IDataItem data)
    {
      data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage);
      return this.getMOH(data) + this.getOtherInsurer1(data) + this.getOtherInsurer2(data);
    }

    private string getMOH(IDataItem data)
    {
      return string.Format("<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.1.1.1\" {0}/><code code=\"PUBLICPOL\"/><beneficiary><coveredPartyAsPatient><code code=\"NI\"/><coveredPartyAsPatientPerson><name><given>{1}</given><given>{2}</given><family>{3}</family></name><birthTime value=\"{4}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><author><carrierRole><id root=\"2.16.840.1.113883.3.30.1.1.1\"/></carrierRole></author></policyOrAccount></coverage>", (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_MOHAvailable)), (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_FirstName), (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_MiddleName), (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_LastName), (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_DateOfBirth));
    }

    private string getOtherInsurer1(IDataItem data)
    {
      return this.getOtherInsurer(new object[8]
      {
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID)),
        (object) this.getExtensionValue("assigningAuthorityName", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name)),
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber)),
        (object) data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_FirstName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_LastName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_MiddleName)
      });
    }

    private string getOtherInsurer2(IDataItem data)
    {
      return this.getOtherInsurer(new object[8]
      {
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID)),
        (object) this.getExtensionValue("assigningAuthorityName", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name)),
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber)),
        (object) data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_FirstName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_LastName),
        (object) data.getEncodedValue(this.m_ocf21BKeys.Applicant_Name_MiddleName)
      });
    }

    private string getOtherInsurer(object[] args)
    {
      return string.Format("<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.1.1.2\" {2}/><code code=\"EHCPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.1.1.2\" {0}/><code nullFlavor=\"NI\"/><coveredPartyAsPatientPerson><name><given>{5}</given><given>{7}</given><family>{6}</family></name><birthTime nullFlavor=\"NI\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{3}</given><family>{4}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.1.1.2\" {1}/></carrierRole></author></policyOrAccount></coverage>", args);
    }

    private string getOtherInsurerOtherService(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherService\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string getOtherInsurerMassageTherapy(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MassageTherapy\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string getOtherInsurerPhysiotherapy(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Physiotherapy\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string getOtherInsurerChiropractic(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Chiropractic\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string getGSLineItems(IDataItem data)
    {
      string empty = string.Empty;
      GS21BLineItemKeys gs21BlineItemKeys = new GS21BLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) data.getList(LineItemType.GS21BLineItem))
      {
        object[] objArray = new object[12]
        {
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity),
          (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure)),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost),
          (object) dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey),
          (object) this.populateDisplayNameAttribute(dataItem.getEncodedValue(gs21BlineItemKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description))
        };
        empty += string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{10}\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"{4}\" unit=\"{5}\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{9}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{9}\"/><fstInd value=\"{7}\"/><pstInd value=\"{8}\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{0}\"{11}><qualifier code=\"{1}\"/></code><effectiveTime value=\"{6}\"/><performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><code code=\"{3}\"/></healthCareProvider></performer><location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component>", objArray);
      }
      return empty;
    }

    private string getHCAIPlanDocNumber(IDataItem data)
    {
      string encodedValue = data.getEncodedValue(this.m_ocf21BKeys.OCF21B_HCAI_Plan_Document_Number);
      if (string.IsNullOrEmpty(encodedValue))
        return string.Empty;
      return string.Format("<reference><adjudicatedInvoiceElementGroup><id root=\"2.16.840.1.113883.3.30.1.1\" extension=\"{0}\"/><code nullFlavor=\"NI\"/><statusCode nullFlavor=\"NI\"/><netAmt nullFlavor=\"NI\"/></adjudicatedInvoiceElementGroup></reference>", new object[1]
      {
        (object) encodedValue
      });
    }

    private string populateDisplayNameAttribute(string strDescription)
    {
      if (!string.IsNullOrWhiteSpace(strDescription))
        return " displayName=\"" + strDescription + "\"";
      return "";
    }

    private string getInjuries(IDataItem data)
    {
      string str1 = "";
      IList list = data.getList(LineItemType.InjuryLineItem);
      OCF21BInjuryLineItemKeys binjuryLineItemKeys = new OCF21BInjuryLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        string str2 = dataItem.getValue(binjuryLineItemKeys.OCF21B_InjuriesAndSequelae_Injury_Code);
        str1 += string.Format("<Injury><Code>{0}</Code></Injury>", (object) str2);
      }
      return str1;
    }

    private string getNetAmountValue(string value)
    {
      if (string.IsNullOrEmpty(value))
        return "<netAmt nullFlavor=\"NI\"/>";
      return "<netAmt value=\"" + value + "\"/>";
    }

    private string getExtensionValue(string nodename, string value)
    {
      if (string.IsNullOrEmpty(value))
        return "";
      return nodename + "=\"" + value + "\"";
    }

    private string getEmptyComponent()
    {
      return "<component><invoiceElementDetail><id></id><code></code><unitQuantity><numerator></numerator><denominator></denominator></unitQuantity><unitPriceAmt><numerator></numerator><denominator></denominator></unitPriceAmt><netAmt></netAmt></invoiceElementDetail></component>";
    }

    private static string GetPrimaryPerformer(string facilityRegistryId)
    {
      return string.Format("<primaryPerformer><contactParty classCode=\"CON\"><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{0}\"/><code/></contactParty></primaryPerformer>", (object) facilityRegistryId);
    }

    private static string GetFacilityIsPayeeCreditElement(string facilityRegistryId, string payeeName)
    {
      return string.Format("<credit><account><holder><payeeRole classCode=\"PROV\"><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{0}\"/><payeeOrganization><name>{1}</name></payeeOrganization></payeeRole></holder></account></credit>", (object) facilityRegistryId, (object) payeeName);
    }

    private static string GetFacilityIsNotPayeeCreditElement(string payeeName, string payeeType)
    {
      if (payeeType == "No")
        payeeType = "COVPTY";
      return string.Format("<credit><account><holder><payeeRole classCode=\"{1}\"><id root=\"2.16.840.1.113883.3.30.4\"/><payeeOrganization><name>{0}</name></payeeOrganization></payeeRole></holder></account></credit>", (object) payeeName, (object) payeeType);
    }
  }
}
