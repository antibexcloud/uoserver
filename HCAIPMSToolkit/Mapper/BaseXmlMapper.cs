﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.BaseXmlMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Xml;

namespace PMSToolkit.Mapper
{
  internal class BaseXmlMapper : BaseMapper
  {
    private XmlNamespaceManager _XmlNSMgr;

    public BaseXmlMapper()
    {
      this._XmlNSMgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      this._XmlNSMgr.AddNamespace("hl7", "urn:hl7-org:v3");
    }

    public XmlNamespaceManager XmlNSMgr
    {
      get
      {
        return this._XmlNSMgr;
      }
    }
  }
}
