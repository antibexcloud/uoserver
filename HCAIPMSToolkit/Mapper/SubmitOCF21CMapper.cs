﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitOCF21CMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Collections;

namespace PMSToolkit.Mapper
{
  internal class SubmitOCF21CMapper : BaseMapper
  {
    private OCF21CKeys m_ocf21CKeys = new OCF21CKeys();
    private string OCFVersion = string.Empty;
    private const string m_xmlTemplate = "{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{102}{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF21C><InvoiceInformation><InvoiceNumber>{13}</InvoiceNumber><FirstInvoice>{14}</FirstInvoice><LastInvoice>{15}</LastInvoice></InvoiceInformation><PreviouslyApprovedGoodsAndServices><Type>{79}</Type><PlanDate>{80}</PlanDate><PlanNumber>{81}</PlanNumber><AmountApproved>{82}</AmountApproved><PreviouslyBilled>{83}</PreviouslyBilled></PreviouslyApprovedGoodsAndServices><Payee><FacilityRegistryID>{75}</FacilityRegistryID><MakeChequePayableTo>{76}</MakeChequePayableTo><ConflictOfInterest><ConflictExists>{16}</ConflictExists><ConflictDetails>{17}</ConflictDetails></ConflictOfInterest></Payee><InjuriesAndSequelae>{18}</InjuriesAndSequelae><OtherInsuranceAmounts><OtherServiceTypeForDebits>{84}</OtherServiceTypeForDebits><IsAmountRefusedByOtherInsurance>{100}</IsAmountRefusedByOtherInsurance><OtherServiceType>{19}</OtherServiceType></OtherInsuranceAmounts><AccountActivity><PriorBalance>{20}</PriorBalance><PaymentReceivedFromAutoInsurer>{21}</PaymentReceivedFromAutoInsurer><OverdueAmount>{22}</OverdueAmount></AccountActivity><OtherInformation>{23}</OtherInformation><AdditionalComments>{24}</AdditionalComments><AttachmentsBeingSent>{74}</AttachmentsBeingSent><PAFType>{71}</PAFType><OCFVersion>{25}</OCFVersion><PatientKey>{72}</PatientKey></OCF21C></inlineData></value></healthDocumentAttachment></pertinentInformation1>{73}<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {26}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {27}/><code {28}/><coveredPartyAsPatientPerson><name><given>{29}</given><given>{30}</given><family>{31}</family></name><administrativeGenderCode code=\"{32}\"/><birthTime value=\"{33}\"/><addr><streetAddressLine>{34}</streetAddressLine><streetAddressLine>{35}</streetAddressLine><city>{36}</city><state>{37}</state><postalCode>{38}</postalCode></addr><telecom value=\"tel:{39};ext={40}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{41}</given><family>{42}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{43}\"/></accident></pertinentInformation></policyOrAccount></coverage><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"RenderedGoodsAndServices\"/><code code=\"CSPINV\"/><netAmt value=\"0.00\"/>{45}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherReimbursableGoodsAndServices\"/><code code=\"CSPINV\"/><netAmt value=\"{46}\"/>{47}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{50}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHAmounts\"/><code code=\"FININV\"/><netAmt value=\"{44}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHDebits\"/><code code=\"FININV\"/><netAmt value=\"{85}\"/>{86}{87}{88}{89}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/>{48}{51}{52}{53}{54}</invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><netAmt value=\"{49}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1Debits\"/><code code=\"FININV\"/><netAmt value=\"{90}\"/>{91}{92}{93}{94}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2Debits\"/><code code=\"FININV\"/><netAmt value=\"{95}\"/>{96}{97}{98}{99}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1\"/><code code=\"FININV\"/>{55}{56}{57}{58}{59}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2\"/><code code=\"FININV\"/>{60}{61}{62}{63}{64}</invoiceElementGroup></component></invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{69}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{66}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{66}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{67}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{67}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"INTEREST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{68}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{68}\"/></invoiceElementDetail></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"PAFReimbursableFees\"/><code code=\"FININV\"/><netAmt value=\"{65}\"/>{70}</invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN600204UV02>";
    private const string OCFVersion2 = "2";
    private const string OCFVersion3 = "3";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{102}{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF21C><InvoiceInformation><InvoiceNumber>{13}</InvoiceNumber><FirstInvoice>{14}</FirstInvoice><LastInvoice>{15}</LastInvoice></InvoiceInformation><PreviouslyApprovedGoodsAndServices><Type>{79}</Type><PlanDate>{80}</PlanDate><PlanNumber>{81}</PlanNumber><AmountApproved>{82}</AmountApproved><PreviouslyBilled>{83}</PreviouslyBilled></PreviouslyApprovedGoodsAndServices><Payee><FacilityRegistryID>{75}</FacilityRegistryID><MakeChequePayableTo>{76}</MakeChequePayableTo><ConflictOfInterest><ConflictExists>{16}</ConflictExists><ConflictDetails>{17}</ConflictDetails></ConflictOfInterest></Payee><InjuriesAndSequelae>{18}</InjuriesAndSequelae><OtherInsuranceAmounts><OtherServiceTypeForDebits>{84}</OtherServiceTypeForDebits><IsAmountRefusedByOtherInsurance>{100}</IsAmountRefusedByOtherInsurance><OtherServiceType>{19}</OtherServiceType></OtherInsuranceAmounts><AccountActivity><PriorBalance>{20}</PriorBalance><PaymentReceivedFromAutoInsurer>{21}</PaymentReceivedFromAutoInsurer><OverdueAmount>{22}</OverdueAmount></AccountActivity><OtherInformation>{23}</OtherInformation><AdditionalComments>{24}</AdditionalComments><AttachmentsBeingSent>{74}</AttachmentsBeingSent><PAFType>{71}</PAFType><OCFVersion>{25}</OCFVersion><PatientKey>{72}</PatientKey></OCF21C></inlineData></value></healthDocumentAttachment></pertinentInformation1>{73}<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {26}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {27}/><code {28}/><coveredPartyAsPatientPerson><name><given>{29}</given><given>{30}</given><family>{31}</family></name><administrativeGenderCode code=\"{32}\"/><birthTime value=\"{33}\"/><addr><streetAddressLine>{34}</streetAddressLine><streetAddressLine>{35}</streetAddressLine><city>{36}</city><state>{37}</state><postalCode>{38}</postalCode></addr><telecom value=\"tel:{39};ext={40}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{41}</given><family>{42}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{43}\"/></accident></pertinentInformation></policyOrAccount></coverage><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"RenderedGoodsAndServices\"/><code code=\"CSPINV\"/><netAmt value=\"0.00\"/>{45}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherReimbursableGoodsAndServices\"/><code code=\"CSPINV\"/><netAmt value=\"{46}\"/>{47}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{50}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHAmounts\"/><code code=\"FININV\"/><netAmt value=\"{44}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOHDebits\"/><code code=\"FININV\"/><netAmt value=\"{85}\"/>{86}{87}{88}{89}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/>{48}{51}{52}{53}{54}</invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><netAmt value=\"{49}\"/><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1Debits\"/><code code=\"FININV\"/><netAmt value=\"{90}\"/>{91}{92}{93}{94}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2Debits\"/><code code=\"FININV\"/><netAmt value=\"{95}\"/>{96}{97}{98}{99}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer1\"/><code code=\"FININV\"/>{55}{56}{57}{58}{59}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Insurer2\"/><code code=\"FININV\"/>{60}{61}{62}{63}{64}</invoiceElementGroup></component></invoiceElementGroup></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{69}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{66}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{66}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{67}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{67}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"INTEREST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{68}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{68}\"/></invoiceElementDetail></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"PAFReimbursableFees\"/><code code=\"FININV\"/><netAmt value=\"{65}\"/>{70}</invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN600204UV02>", (object[]) this.getOCF21CStringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private string[] getOCF21CStringArgs(IDataItem data)
    {
      string[] strArray = new string[103];
      string encodedValue1 = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityRegistryID);
      string encodedValue2 = data.getEncodedValue(this.m_ocf21CKeys.PMSFields_PMSDocumentKey);
      string encodedValue3 = data.getEncodedValue(this.m_ocf21CKeys.Insurer_IBCInsurerID);
      strArray[0] = this.getSubmissionHeader((object) "FICR_IN600204UV02");
      strArray[1] = encodedValue1;
      strArray[2] = encodedValue2;
      strArray[3] = encodedValue3;
      strArray[4] = this.getReceiverElement((object) encodedValue3, (object) data.getEncodedValue(this.m_ocf21CKeys.Insurer_IBCBranchID), (object) data.getEncodedValue(this.m_ocf21CKeys.Insurer_Adjuster_TelephoneNumber), (object) data.getEncodedValue(this.m_ocf21CKeys.Insurer_Adjuster_TelephoneExtension), (object) data.getEncodedValue(this.m_ocf21CKeys.Insurer_Adjuster_FaxNumber), (object) data.getEncodedValue(this.m_ocf21CKeys.Insurer_Adjuster_Name_FirstName), (object) data.getEncodedValue(this.m_ocf21CKeys.Insurer_Adjuster_Name_LastName));
      strArray[5] = this.getSenderElement((object) data.getEncodedValue(this.m_ocf21CKeys.PMSFields_PMSSoftware), (object) data.getEncodedValue(this.m_ocf21CKeys.PMSFields_PMSVersion));
      strArray[6] = !(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityIsPayee) == "Yes") ? SubmitOCF21CMapper.GetFacilityIsNotPayeeCreditElement(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_MakeChequePayableTo), data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityIsPayee)) : SubmitOCF21CMapper.GetFacilityIsPayeeCreditElement(encodedValue1, data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_MakeChequePayableTo));
      strArray[11] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed);
      strArray[13] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InvoiceInformation_InvoiceNumber);
      strArray[14] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InvoiceInformation_FirstInvoice);
      strArray[15] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InvoiceInformation_LastInvoice);
      strArray[16] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_ConflictOfInterests_ConflictExists);
      strArray[17] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_ConflictOfInterests_ConflictDetails);
      strArray[18] = this.getInjuries(data);
      strArray[19] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_OtherServiceType);
      strArray[20] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_AccountActivity_PriorBalance);
      strArray[21] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer);
      strArray[22] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_AccountActivity_OverdueAmount);
      strArray[23] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInformation);
      strArray[24] = data.getEncodedValue(this.m_ocf21CKeys.AdditionalComments);
      strArray[25] = data.getEncodedValue(this.m_ocf21CKeys.OCFVersion);
      this.OCFVersion = strArray[25];
      strArray[26] = this.GetExtensionValue("extension", data.getEncodedValue(this.m_ocf21CKeys.Header_PolicyNumber));
      strArray[27] = this.GetExtensionValue("extension", data.getEncodedValue(this.m_ocf21CKeys.Header_ClaimNumber));
      strArray[28] = ConvertUtil.convertPolicyHolderSameAsToNeCST(data.getEncodedValue(this.m_ocf21CKeys.Insurer_PolicyHolder_IsSameAsApplicant));
      strArray[29] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Name_FirstName);
      strArray[30] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Name_MiddleName);
      strArray[31] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Name_LastName);
      strArray[32] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Gender);
      strArray[33] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_DateOfBirth);
      strArray[34] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Address_StreetAddress1);
      strArray[35] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Address_StreetAddress2);
      strArray[36] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Address_City);
      strArray[37] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Address_Province);
      strArray[38] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_Address_PostalCode);
      strArray[39] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_TelephoneNumber);
      strArray[40] = data.getEncodedValue(this.m_ocf21CKeys.Applicant_TelephoneExtension);
      strArray[41] = data.getEncodedValue(this.m_ocf21CKeys.Insurer_PolicyHolder_Name_FirstName);
      strArray[42] = data.getEncodedValue(this.m_ocf21CKeys.Insurer_PolicyHolder_Name_LastName);
      strArray[43] = data.getEncodedValue(this.m_ocf21CKeys.Header_DateOfAccident);
      strArray[44] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_MOH_Proposed);
      strArray[45] = this.getRenderedGSLineItems(data);
      strArray[46] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed);
      strArray[47] = this.getOtherReimbursableLineItems(data);
      strArray[48] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Proposed));
      strArray[49] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Proposed);
      strArray[51] = this.GetOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic));
      strArray[52] = this.GetOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy));
      strArray[53] = this.GetOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy));
      strArray[54] = this.GetOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_OtherService));
      if (string.IsNullOrEmpty(strArray[54]) && string.IsNullOrEmpty(strArray[51]) && (string.IsNullOrEmpty(strArray[52]) && string.IsNullOrEmpty(strArray[53])))
        strArray[54] = this.getEmptyComponent();
      strArray[55] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed));
      strArray[56] = this.GetOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic));
      strArray[57] = this.GetOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy));
      strArray[58] = this.GetOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy));
      strArray[59] = this.GetOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService));
      if (string.IsNullOrEmpty(strArray[59]) && string.IsNullOrEmpty(strArray[56]) && (string.IsNullOrEmpty(strArray[57]) && string.IsNullOrEmpty(strArray[58])))
        strArray[59] = this.getEmptyComponent();
      strArray[60] = this.getNetAmountValue(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed));
      strArray[61] = this.GetOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic));
      strArray[62] = this.GetOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy));
      strArray[63] = this.GetOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy));
      strArray[64] = this.GetOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService));
      if (string.IsNullOrEmpty(strArray[64]) && string.IsNullOrEmpty(strArray[61]) && (string.IsNullOrEmpty(strArray[62]) && string.IsNullOrEmpty(strArray[63])))
        strArray[64] = this.getEmptyComponent();
      strArray[65] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed);
      strArray[66] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_GST_Proposed);
      strArray[67] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_PST_Proposed);
      strArray[68] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_InsurerTotals_Interest_Proposed);
      try
      {
        strArray[69] = (Decimal.Parse(strArray[66]) + Decimal.Parse(strArray[67]) + Decimal.Parse(strArray[68])).ToString();
      }
      catch
      {
        strArray[69] = "0.0";
      }
      strArray[70] = this.getPAFLineItems(data);
      strArray[71] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_PAFReimbursableFees_PAFType);
      strArray[72] = data.getEncodedValue(this.m_ocf21CKeys.PMSFields_PMSPatientKey);
      strArray[73] = this.getHCAIPlanDocNumber(data);
      strArray[74] = data.getEncodedValue(this.m_ocf21CKeys.AttachmentsBeingSent);
      strArray[75] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityRegistryID);
      strArray[76] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_MakeChequePayableTo);
      strArray[77] = "";
      strArray[78] = "";
      strArray[79] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type);
      strArray[80] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate);
      strArray[81] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber);
      strArray[82] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved);
      strArray[83] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled);
      strArray[84] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Debits_OtherServiceType);
      strArray[85] = !string.IsNullOrEmpty(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Proposed)) ? data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Proposed) : "0.0";
      strArray[86] = this.GetOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Chiropractic));
      strArray[87] = this.GetOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Physiotherapy));
      strArray[88] = this.GetOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_MassageTherapy));
      strArray[89] = this.GetOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_OtherService));
      if (string.IsNullOrEmpty(strArray[89]) && string.IsNullOrEmpty(strArray[86]) && (string.IsNullOrEmpty(strArray[87]) && string.IsNullOrEmpty(strArray[88])))
        strArray[89] = this.getEmptyComponent();
      string encodedValue4 = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed);
      strArray[90] = string.IsNullOrEmpty(encodedValue4) || string.IsNullOrEmpty(encodedValue4.Trim()) ? "0.0" : encodedValue4;
      strArray[91] = this.GetOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Chiropractic));
      strArray[92] = this.GetOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy));
      strArray[93] = this.GetOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy));
      strArray[94] = this.GetOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_OtherService));
      if (string.IsNullOrEmpty(strArray[94]) && string.IsNullOrEmpty(strArray[93]) && (string.IsNullOrEmpty(strArray[92]) && string.IsNullOrEmpty(strArray[91])))
        strArray[94] = this.getEmptyComponent();
      string encodedValue5 = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed);
      strArray[95] = string.IsNullOrEmpty(encodedValue5) || string.IsNullOrEmpty(encodedValue5.Trim()) ? "0.0" : encodedValue5;
      strArray[96] = this.GetOtherInsurerChiropractic(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Chiropractic));
      strArray[97] = this.GetOtherInsurerPhysiotherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy));
      strArray[98] = this.GetOtherInsurerMassageTherapy(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy));
      strArray[99] = this.GetOtherInsurerOtherService(data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_OtherService));
      if (string.IsNullOrEmpty(strArray[99]) && string.IsNullOrEmpty(strArray[96]) && (string.IsNullOrEmpty(strArray[97]) && string.IsNullOrEmpty(strArray[98])))
        strArray[99] = this.getEmptyComponent();
      try
      {
        strArray[50] = (Decimal.Parse(strArray[44]) + Decimal.Parse(strArray[49])).ToString();
      }
      catch
      {
        strArray[50] = "0.0";
      }
      strArray[100] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_OtherInsuranceAmounts_IsAmountRefused);
      strArray[101] = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityIsPayee);
      strArray[102] = SubmitOCF21CMapper.GetPrimaryPerformer(encodedValue1);
      return strArray;
    }

    private string getRenderedGSLineItems(IDataItem data)
    {
      string empty = string.Empty;
      RenderedGSLineItemKeys renderedGsLineItemKeys = new RenderedGSLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) data.getList(LineItemType.GS21CRenderedGSLineItem))
      {
        object[] objArray = new object[14]
        {
          (object) "",
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Code),
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute),
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID),
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation),
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity),
          (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Measure)),
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService),
          (object) ConvertUtil.convertFlagToNeCSTBool("No"),
          (object) ConvertUtil.convertFlagToNeCSTBool("No"),
          (object) "0.00",
          (object) data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityRegistryID),
          (object) dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey),
          (object) this.populateDisplayNameAttribute(dataItem.getEncodedValue(renderedGsLineItemKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Description))
        };
        empty += string.Format(this.getGSLineItemTemplate(), objArray);
      }
      return empty;
    }

    private string GetOtherInsurerOtherService(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      string empty = string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherService\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string GetOtherInsurerMassageTherapy(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      string empty = string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MassageTherapy\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string GetOtherInsurerPhysiotherapy(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      string empty = string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Physiotherapy\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string GetExtensionValue(string nodename, string value)
    {
      if (string.IsNullOrEmpty(value))
        return "";
      return nodename + "=\"" + value + "\"";
    }

    private string GetOtherInsurerChiropractic(string data)
    {
      if (string.IsNullOrEmpty(data))
        return string.Empty;
      string empty = string.Empty;
      return string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Chiropractic\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{0}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{0}\"/></invoiceElementDetail></component>", (object) data);
    }

    private string getEmptyComponent()
    {
      return "<component><invoiceElementDetail><id></id><code></code><unitQuantity><numerator></numerator><denominator></denominator></unitQuantity><unitPriceAmt><numerator></numerator><denominator></denominator></unitPriceAmt><netAmt></netAmt></invoiceElementDetail></component>";
    }

    private string getOtherReimbursableLineItems(IDataItem data)
    {
      string str = string.Empty;
      OtherReimbursableLineItemKeys reimbursableLineItemKeys = new OtherReimbursableLineItemKeys();
      IList list = data.getList(LineItemType.GS21COtherReimbursableLineItem);
      if (list.Count == 0)
        str = this.getEmptyComponent();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        object[] objArray = new object[14]
        {
          (object) "",
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity),
          (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure)),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost),
          (object) data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityRegistryID),
          (object) dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey),
          (object) this.populateDisplayNameAttribute(dataItem.getEncodedValue(reimbursableLineItemKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description))
        };
        str = objArray[1] != null && !string.IsNullOrEmpty(objArray[1].ToString()) || objArray[2] != null && !string.IsNullOrEmpty(objArray[2].ToString()) || (objArray[3] != null && !string.IsNullOrEmpty(objArray[3].ToString()) || objArray[4] != null && !string.IsNullOrEmpty(objArray[4].ToString())) || (objArray[5] != null && !string.IsNullOrEmpty(objArray[5].ToString()) || objArray[7] != null && !string.IsNullOrEmpty(objArray[7].ToString()) || objArray[10] != null && !string.IsNullOrEmpty(objArray[10].ToString())) ? str + string.Format(this.getGSLineItemTemplate(), objArray) : str + this.getEmptyComponent();
      }
      return str;
    }

    private string getHCAIPlanDocNumber(IDataItem data)
    {
      string encodedValue = data.getEncodedValue(this.m_ocf21CKeys.OCF21C_HCAI_Plan_Document_Number);
      if (string.IsNullOrEmpty(encodedValue))
        return string.Empty;
      return string.Format("<reference><adjudicatedInvoiceElementGroup><id root=\"2.16.840.1.113883.3.30.1.1\" extension=\"{0}\"/><code nullFlavor=\"NI\"/><statusCode nullFlavor=\"NI\"/><netAmt nullFlavor=\"NI\"/></adjudicatedInvoiceElementGroup></reference>", new object[1]
      {
        (object) encodedValue
      });
    }

    private string getPAFLineItems(IDataItem data)
    {
      string str = string.Empty;
      PAFReimbursableLineItemKeys reimbursableLineItemKeys = new PAFReimbursableLineItemKeys();
      IList list = data.getList(LineItemType.GS21CPAFReimbursableLineItem);
      if (list.Count == 0)
        str = this.getEmptyComponent();
      foreach (IDataItem data1 in (IEnumerable) list)
      {
        object[] objArray = new object[13]
        {
          null,
          (object) data1.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_Code),
          (object) data1.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_Attribute),
          (object) this.getPAFFirstDateOFServiceTemplate(data1),
          (object) this.getPAFProviderRefrenceTemplate(data1),
          (object) "",
          (object) "",
          null,
          (object) "",
          (object) "",
          (object) data1.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost),
          (object) data.getEncodedValue(this.m_ocf21CKeys.OCF21C_Payee_FacilityRegistryID),
          (object) data1.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey)
        };
        str = objArray[1] != null && !string.IsNullOrEmpty(objArray[1].ToString()) || objArray[2] != null && !string.IsNullOrEmpty(objArray[2].ToString()) || objArray[10] != null && !string.IsNullOrEmpty(objArray[10].ToString()) ? str + string.Format(this.getPAFLineItemTemplate(), objArray) : str + this.getEmptyComponent();
      }
      return str;
    }

    private string getPAFLineItemTemplate()
    {
      return "<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{12}\"/><code nullFlavor=\"NA\"/><unitQuantity><numerator value=\"1\" /><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{10}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{10}\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{1}\"><qualifier code=\"{2}\"/></code>{3}{4}<location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component>";
    }

    private string getPAFFirstDateOFServiceTemplate(IDataItem data)
    {
      PAFReimbursableLineItemKeys reimbursableLineItemKeys = new PAFReimbursableLineItemKeys();
      string format = "<effectiveTime {0}=\"{1}\"/>";
      string encodedValue = data.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService);
      return this.OCFVersion == "2" || this.OCFVersion == "3" ? (!string.IsNullOrEmpty(encodedValue) ? string.Format(format, (object) "value", (object) encodedValue) : string.Format(format, (object) "nullFlavor", (object) "NA")) : (encodedValue != null ? string.Format(format, (object) "value", (object) encodedValue) : string.Format(format, (object) "nullFlavor", (object) "NA"));
    }

    private string getPAFProviderRefrenceTemplate(IDataItem data)
    {
      string format = "<secondaryPerformer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" {0}=\"{1}\"/><code code=\"{2}\"/></healthCareProvider></secondaryPerformer>";
      string str = "";
      PAFReimbursableLineItemKeys reimbursableLineItemKeys = new PAFReimbursableLineItemKeys();
      string encodedValue1 = data.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation);
      string encodedValue2 = data.getEncodedValue(reimbursableLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID);
      if (this.OCFVersion == "2" || this.OCFVersion == "3")
        str = !string.IsNullOrEmpty(encodedValue1) || !string.IsNullOrEmpty(encodedValue2) ? str + string.Format("<performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" {0}=\"{1}\"/><code code=\"{2}\"/></healthCareProvider></performer>", (object) "extension", (object) encodedValue2, (object) encodedValue1) : str + string.Format("<performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" {0}=\"{1}\"/><code code=\"{2}\"/></healthCareProvider></performer>", (object) "nullFlavor", (object) "NA", (object) string.Empty);
      else if (encodedValue1 != null || encodedValue2 != null)
        str += string.Format("<performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" {0}=\"{1}\"/><code code=\"{2}\"/></healthCareProvider></performer>", (object) "extension", (object) encodedValue2, (object) encodedValue1);
      IList list = data.getList(LineItemType.PAFSecondaryProviderProfessionLineItem);
      GS21CPAFSecondaryProviderProfessionLineItemKeys professionLineItemKeys = new GS21CPAFSecondaryProviderProfessionLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        string encodedValue3 = dataItem.getEncodedValue(professionLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation);
        string encodedValue4 = dataItem.getEncodedValue(professionLineItemKeys.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID);
        str = this.OCFVersion == "2" || this.OCFVersion == "3" ? (!string.IsNullOrEmpty(encodedValue3) || !string.IsNullOrEmpty(encodedValue4) ? str + string.Format(format, (object) "extension", (object) encodedValue4, (object) encodedValue3) : str + string.Format(format, (object) "nullFlavor", (object) "NA", (object) string.Empty)) : str + string.Format(format, (object) "extension", (object) encodedValue4, (object) encodedValue3);
      }
      return str;
    }

    private string getGSLineItemTemplate()
    {
      return "<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{12}\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"{5}\" unit=\"{6}\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{10}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{10}\"/><fstInd value=\"{8}\"/><pstInd value=\"{9}\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{1}\"{13}><qualifier code=\"{2}\"/></code><effectiveTime value=\"{7}\"/><performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{3}\"/><code code=\"{4}\"/></healthCareProvider></performer><location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component>";
    }

    private string populateDisplayNameAttribute(string strDescription)
    {
      if (!string.IsNullOrWhiteSpace(strDescription))
        return " displayName=\"" + strDescription + "\"";
      return "";
    }

    private string getInjuries(IDataItem data)
    {
      string str1 = "";
      IList list = data.getList(LineItemType.InjuryLineItem);
      OCF21BInjuryLineItemKeys binjuryLineItemKeys = new OCF21BInjuryLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        string str2 = dataItem.getValue(binjuryLineItemKeys.OCF21B_InjuriesAndSequelae_Injury_Code);
        str1 += string.Format("<Injury><Code>{0}</Code></Injury>", (object) str2);
      }
      return str1;
    }

    private string getNetAmountValue(string value)
    {
      if (string.IsNullOrEmpty(value))
        return "<netAmt nullFlavor=\"NI\"/>";
      return "<netAmt value=\"" + value + "\"/>";
    }

    private static string GetPrimaryPerformer(string facilityRegistryId)
    {
      return string.Format("<primaryPerformer><contactParty classCode=\"CON\"><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{0}\"/><code/></contactParty></primaryPerformer>", (object) facilityRegistryId);
    }

    private static string GetFacilityIsPayeeCreditElement(string facilityRegistryId, string payeeName)
    {
      return string.Format("<credit><account><holder><payeeRole classCode=\"PROV\"><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{0}\"/><payeeOrganization><name>{1}</name></payeeOrganization></payeeRole></holder></account></credit>", (object) facilityRegistryId, (object) payeeName);
    }

    private static string GetFacilityIsNotPayeeCreditElement(string payeeName, string payeeType)
    {
      if (payeeType == "No")
        payeeType = "COVPTY";
      return string.Format("<credit><account><holder><payeeRole classCode=\"{1}\"><id root=\"2.16.840.1.113883.3.30.4\"/><payeeOrganization><name>{0}</name></payeeOrganization></payeeRole></holder></account></credit>", (object) payeeName, (object) payeeType);
    }
  }
}
