﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.GetAdjusterResponseRequestMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;

namespace PMSToolkit.Mapper
{
  internal class GetAdjusterResponseRequestMapper : BaseMapper
  {
    private AdjusterResponseRequestKeys m_adjusterResponseRequestKeys = new AdjusterResponseRequestKeys();
    private const string m_xmlTemplate = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><MCCI_IN102001 xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 MCCI_IN102001.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.1.2\"/><!-- OID of toolkit --><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30\" extension=\"MCCI_IN102001\"/><processingCode code=\"P\"/><!-- P = Production, T = Testing, D = Debugging --><processingModeCode code=\"T\"/><acceptAckCode code=\"AL\"/><!-- AL = Always send response --><receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/><!-- application OID goes here --></device></receiver><sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/><!-- OID of toolkit --></device></sender></MCCI_IN102001>";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("<?xml version=\"1.0\" encoding=\"UTF-8\"?><MCCI_IN102001 xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 MCCI_IN102001.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.1.2\"/><!-- OID of toolkit --><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30\" extension=\"MCCI_IN102001\"/><processingCode code=\"P\"/><!-- P = Production, T = Testing, D = Debugging --><processingModeCode code=\"T\"/><acceptAckCode code=\"AL\"/><!-- AL = Always send response --><receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/><!-- application OID goes here --></device></receiver><sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/><!-- OID of toolkit --></device></sender></MCCI_IN102001>", this.getAdjusterResponseRequestKeysArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private object[] getAdjusterResponseRequestKeysArgs(IDataItem data)
    {
      return new object[1]
      {
        (object) DateTime.Now.ToString("yyyyMMddHHmmss")
      };
    }
  }
}
