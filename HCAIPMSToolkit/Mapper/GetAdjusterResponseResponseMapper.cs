﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.GetAdjusterResponseResponseMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Xml;

namespace PMSToolkit.Mapper
{
  internal class GetAdjusterResponseResponseMapper : BaseXmlMapper
  {
    public override IDataItem transform(IDataItem data)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDoc = new XmlDocument();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      xmlDoc.LoadXml(xml);
      string innerText = xmlDoc.SelectSingleNode("/").InnerText;
      return innerText == "MCCI_IN000002UV01" ? (IDataItem) this.NoDocumentReturned(xmlDoc) : (innerText == "FICR_IN310304UV02" ? (IDataItem) this.PlanReturned(xmlDoc) : (innerText == "FICR_IN610304UV02" ? (IDataItem) this.InvoiceReturned(xmlDoc) : (IDataItem) this.DocumentReturned(xmlDoc)));
    }

    private AdjusterResponseResponse PlanReturned(XmlDocument xmlDoc)
    {
      return this.DocumentReturned(xmlDoc);
    }

    private AdjusterResponseResponse InvoiceReturned(XmlDocument xmlDoc)
    {
      return this.DocumentReturned(xmlDoc);
    }

    private AdjusterResponseResponse NoDocumentReturned(XmlDocument xmlDoc)
    {
      throw new NotImplementedException();
    }

    private AdjusterResponseResponse DocumentReturned(XmlDocument xmlDoc)
    {
      AdjusterResponseResponse responseResponse = new AdjusterResponseResponse();
      string str1 = "";
      try
      {
        string str2 = !(xmlDoc.FirstChild.Name.ToString() == "AACNAdjusterResponse") ? xmlDoc.SelectSingleNode("/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:id/@extension", this.XmlNSMgr).InnerText : "AACN";
      }
      catch (Exception ex)
      {
        int num = str1 == "" ? 1 : 0;
      }
      responseResponse.Xmn = (XmlNode) xmlDoc;
      return responseResponse;
    }
  }
}
