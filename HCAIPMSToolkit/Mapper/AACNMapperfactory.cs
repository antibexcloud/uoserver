﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.AACNMapperfactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.Action;
using System;

namespace PMSToolkit.Mapper
{
  internal class AACNMapperfactory
  {
    public static IMapper newMapper(IDataItem data, IAction action)
    {
      Type type = data.GetType();
      if (!(action.GetType() == typeof (SubmitAACNAction)))
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "AACNMapperFactory::newMapper(IDataItem, IAACNAction) - type of IDataItem is not recognized.");
      if (type == typeof (PMSToolkit.DataObjects.AACN))
        return (IMapper) new SubmitAACNMapper();
      return (IMapper) new AACNResponseMapper();
    }
  }
}
