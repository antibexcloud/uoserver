﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.AssessorType
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Xml.Serialization;

namespace PMSToolkit.Mapper
{
  [XmlType(Namespace = "http://hcai.ca")]
  public class AssessorType
  {
    public string FacilityRegistryID { get; set; }

    public string ProviderRegistryID { get; set; }

    public string Occupation { get; set; }

    public string Email { get; set; }

    public string IsSignatureOnFile { get; set; }

    public string DateSigned { get; set; }
  }
}
