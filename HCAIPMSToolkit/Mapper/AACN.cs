﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.AACN
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Xml.Serialization;

namespace PMSToolkit.Mapper
{
  [XmlType(Namespace = "http://hcai.ca")]
  [XmlRoot(IsNullable = false, Namespace = "http://hcai.ca")]
  public class AACN
  {
    public string FormVersion { get; set; }

    public HeaderType Header { get; set; }

    public ApplicantType Applicant { get; set; }

    public string AssessmentDate { get; set; }

    public string FirstAssessment { get; set; }

    public string LastAssessmentDate { get; set; }

    public string CurrentMonthlyAllowance { get; set; }

    public string InsurerExamination { get; set; }

    public AssessorType Assessor { get; set; }

    public InsurerType Insurer { get; set; }

    public Item[] AttendantCareServices { get; set; }

    public CostType Costs { get; set; }

    public string AdditionalComments { get; set; }

    public string AttachmentsBeingSent { get; set; }

    public string AttachmentCount { get; set; }

    public AACNPMSFields PMSFields { get; set; }
  }
}
