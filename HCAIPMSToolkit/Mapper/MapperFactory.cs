﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.MapperFactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.Action;
using PMSToolkit.DataObjects;
using System;

namespace PMSToolkit.Mapper
{
  internal class MapperFactory
  {
    public static IMapper newMapper(IDataItem data, IAction action)
    {
      Type type1 = data.GetType();
      Type type2 = action.GetType();
      if (type2 == typeof (SubmitAction))
      {
        if (type1 == typeof (OCF18))
          return (IMapper) new SubmitOCF18Mapper();
        if (type1 == typeof (OCF21B))
          return (IMapper) new SubmitOCF21BMapper();
        if (type1 == typeof (OCF21C))
          return (IMapper) new SubmitOCF21CMapper();
        if (type1 == typeof (OCF22))
          return (IMapper) new SubmitOCF22Mapper();
        if (type1 == typeof (OCF23))
          return (IMapper) new SubmitOCF23Mapper();
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new SubmitResponseMapper();
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "MapperFactory::newMapper(IDataItem, IAction) - type of IDataItem is not recognized.");
      }
      if (type2 == typeof (GetAdjusterResponseAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new GetAdjusterResponseResponseMapper();
        return (IMapper) new GetAdjusterResponseRequestMapper();
      }
      if (type2 == typeof (DataExtractAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new DataExtractResponseMapper();
        return (IMapper) new DataExtractRequestMapper();
      }
      if (type2 == typeof (ActivityListAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new ActivityListResponseMapper();
        return (IMapper) new ActivityListRequestMapper();
      }
      if (type2 == typeof (InsurerListAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new InsurerListResponseMapper();
        throw new NotSupportedException();
      }
      if (type2 == typeof (FacilityInfoAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new FacilityInfoResponseMapper();
        throw new NotSupportedException();
      }
      if (type2 == typeof (AdjusterResponseAckAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new AdjusterResponseAckResponseMapper();
        return (IMapper) new AdjusterResponseAckRequestMapper();
      }
      if (type2 == typeof (UpdateFacilityAction))
      {
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new FacilityUpdateResponseMapper();
        return (IMapper) new SubmitFacilityUpdateMapper();
      }
      if (type2 == typeof (ProviderAction))
      {
        if (type1 == typeof (ProviderAdd))
          return (IMapper) new AddProviderMapper();
        if (type1 == typeof (ProviderUpdate))
          return (IMapper) new UpdateProviderMapper();
        if (type1 == typeof (ResponseMessage))
          return (IMapper) new ProviderResponseMapper();
      }
      throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "MapperFactory::newMapper(IDataItem, IAction) - type of IAction is not recognized.");
    }
  }
}
