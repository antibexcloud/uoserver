﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitAACNMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace PMSToolkit.Mapper
{
  internal class SubmitAACNMapper : IMapper
  {
    private AACNKeys m_aacnKeys = new AACNKeys();
    private AACN aacnSchemaObject = new AACN();

    public IDataItem transform(IDataItem data)
    {
      UTF8StringWriter utF8StringWriter = new UTF8StringWriter();
      IDataItem dataItem = (IDataItem) new AACNResponseMessage();
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      AACN aacn = this.MapAACNSchemaObject(data);
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (AACN));
      try
      {
        xmlSerializer.Serialize((TextWriter) utF8StringWriter, (object) aacn);
        string val = utF8StringWriter.ToString();
        dataItem.addValue(pmsResponseKeys.PMSResponseMessageKey, val);
        return dataItem;
      }
      finally
      {
        utF8StringWriter.Close();
      }
    }

    private AACN MapAACNSchemaObject(IDataItem data)
    {
      this.aacnSchemaObject.Header = new HeaderType();
      this.aacnSchemaObject.Applicant = new ApplicantType();
      this.aacnSchemaObject.PMSFields = new AACNPMSFields();
      this.aacnSchemaObject.Applicant.Name = new ApplicantTypeName();
      this.aacnSchemaObject.Applicant.Address = new AddressType();
      this.aacnSchemaObject.Insurer = new InsurerType();
      this.aacnSchemaObject.Assessor = new AssessorType();
      this.aacnSchemaObject.Insurer.PolicyHolder = new PolicyHolderType();
      this.aacnSchemaObject.Insurer.PolicyHolder.Name = new PolicyHolderTypeName();
      this.aacnSchemaObject.FormVersion = data.getValue(this.m_aacnKeys.FormVersion);
      this.aacnSchemaObject.Header.PolicyNumber = data.getValue(this.m_aacnKeys.Header_PolicyNumber);
      this.aacnSchemaObject.Header.ClaimNumber = data.getValue(this.m_aacnKeys.Header_ClaimNumber);
      this.aacnSchemaObject.Header.DateOfAccident = data.getValue(this.m_aacnKeys.Header_DateOfAccident);
      this.aacnSchemaObject.Applicant.Name.FirstName = data.getValue(this.m_aacnKeys.Applicant_Name_FirstName);
      this.aacnSchemaObject.Applicant.Name.MiddleName = data.getValue(this.m_aacnKeys.Applicant_Name_MiddleName);
      this.aacnSchemaObject.Applicant.Name.LastName = data.getValue(this.m_aacnKeys.Applicant_Name_LastName);
      this.aacnSchemaObject.Applicant.DateOfBirth = data.getValue(this.m_aacnKeys.Applicant_DateOfBirth);
      this.aacnSchemaObject.Applicant.Gender = data.getValue(this.m_aacnKeys.Applicant_Gender);
      this.aacnSchemaObject.Applicant.Telephone = data.getValue(this.m_aacnKeys.Applicant_Telephone);
      this.aacnSchemaObject.Applicant.Extension = data.getValue(this.m_aacnKeys.Applicant_Extension);
      this.aacnSchemaObject.Applicant.Address.StreetAddress1 = data.getValue(this.m_aacnKeys.Applicant_Address_StreetAddress1);
      this.aacnSchemaObject.Applicant.Address.StreetAddress2 = data.getValue(this.m_aacnKeys.Applicant_Address_StreetAddress2);
      this.aacnSchemaObject.Applicant.Address.City = data.getValue(this.m_aacnKeys.Applicant_Address_City);
      this.aacnSchemaObject.Applicant.Address.Province = data.getValue(this.m_aacnKeys.Applicant_Address_Province);
      this.aacnSchemaObject.Applicant.Address.PostalCode = data.getValue(this.m_aacnKeys.Applicant_Address_PostalCode);
      this.aacnSchemaObject.AssessmentDate = data.getValue(this.m_aacnKeys.AssessmentDate);
      this.aacnSchemaObject.FirstAssessment = data.getValue(this.m_aacnKeys.FirstAssessment);
      this.aacnSchemaObject.LastAssessmentDate = data.getValue(this.m_aacnKeys.LastAssessmentDate);
      this.aacnSchemaObject.CurrentMonthlyAllowance = data.getValue(this.m_aacnKeys.CurrentMonthlyAllowance);
      this.aacnSchemaObject.InsurerExamination = data.getValue(this.m_aacnKeys.InsurerExamination);
      this.aacnSchemaObject.Assessor.FacilityRegistryID = data.getValue(this.m_aacnKeys.Assessor_FacilityRegistryID);
      this.aacnSchemaObject.Assessor.ProviderRegistryID = data.getValue(this.m_aacnKeys.Assessor_ProviderRegistryID);
      this.aacnSchemaObject.Assessor.Occupation = data.getValue(this.m_aacnKeys.Assessor_Occupation);
      this.aacnSchemaObject.Assessor.Email = data.getValue(this.m_aacnKeys.Assessor_Email);
      this.aacnSchemaObject.Assessor.IsSignatureOnFile = data.getValue(this.m_aacnKeys.Assessor_IsSignatureOnFile);
      this.aacnSchemaObject.Assessor.DateSigned = data.getValue(this.m_aacnKeys.Assessor_DateSigned);
      this.aacnSchemaObject.Insurer.IBCInsurerID = data.getValue(this.m_aacnKeys.Insurer_IBCInsurerID);
      this.aacnSchemaObject.Insurer.IBCBranchID = data.getValue(this.m_aacnKeys.Insurer_IBCBranchID);
      this.aacnSchemaObject.Insurer.PolicyHolder.IsSameAsApplicant = data.getValue(this.m_aacnKeys.Insurer_PolicyHolder_IsSameAsApplicant);
      this.aacnSchemaObject.Insurer.PolicyHolder.Name.FirstName = data.getValue(this.m_aacnKeys.Insurer_PolicyHolder_Name_FirstName);
      this.aacnSchemaObject.Insurer.PolicyHolder.Name.LastName = data.getValue(this.m_aacnKeys.Insurer_PolicyHolder_Name_LastName);
      this.getAACNACSList(data);
      this.getAACNCostAssessedPart(data);
      this.aacnSchemaObject.AdditionalComments = data.getValue(this.m_aacnKeys.AdditionalComments);
      this.aacnSchemaObject.AttachmentsBeingSent = data.getValue(this.m_aacnKeys.AttachmentsBeingSent);
      this.aacnSchemaObject.AttachmentCount = data.getValue(this.m_aacnKeys.AttachmentCount);
      this.aacnSchemaObject.PMSFields.PMSDocumentKey = data.getValue(this.m_aacnKeys.PMSFields_PMSDocumentKey);
      this.aacnSchemaObject.PMSFields.PMSPatientKey = data.getValue(this.m_aacnKeys.PMSFields_PMSPatientKey);
      this.aacnSchemaObject.PMSFields.PMSSoftware = data.getValue(this.m_aacnKeys.PMSFields_PMSSoftware);
      this.aacnSchemaObject.PMSFields.PMSVersion = data.getValue(this.m_aacnKeys.PMSFields_PMSVersion);
      return this.aacnSchemaObject;
    }

    private void getAACNACSList(IDataItem data)
    {
      string empty = string.Empty;
      IList list = data.getList(LineItemType.AACNServicesLineItem);
      AACNServicesLineItemKeys servicesLineItemKeys = new AACNServicesLineItemKeys();
      int count = list.Count;
      this.aacnSchemaObject.AttendantCareServices = new Item[count];
      for (int index = 0; index < count; ++index)
      {
        this.aacnSchemaObject.AttendantCareServices[index] = new Item();
        this.aacnSchemaObject.AttendantCareServices[index].Assessed = new AttendantCareServicesAssessedType();
        this.aacnSchemaObject.AttendantCareServices[index].ACSItemID = ((IDataItem) list[index]).getValue(servicesLineItemKeys.AttendantCareServices_Item_ACSItemID);
        this.aacnSchemaObject.AttendantCareServices[index].PMSLineItemKey = ((IDataItem) list[index]).getValue(servicesLineItemKeys.AttendantCareServices_Item_PMSLineItemKey);
        this.aacnSchemaObject.AttendantCareServices[index].Assessed.Minutes = ((IDataItem) list[index]).getValue(servicesLineItemKeys.AttendantCareServices_Item_Assessed_Minutes);
        this.aacnSchemaObject.AttendantCareServices[index].Assessed.TimesPerWeek = ((IDataItem) list[index]).getValue(servicesLineItemKeys.AttendantCareServices_Item_Assessed_TimesPerWeek);
      }
    }

    private void getAACNCostAssessedPart(IDataItem data)
    {
      string empty = string.Empty;
      IList list = data.getList(LineItemType.AACNAssessedPart);
      AACNAssessedPartKeys assessedPartKeys = new AACNAssessedPartKeys();
      int count = list.Count;
      this.aacnSchemaObject.Costs = new CostType();
      this.aacnSchemaObject.Costs.Assessed = new Part[count];
      for (int index = 0; index < count; ++index)
      {
        this.aacnSchemaObject.Costs.Assessed[index] = new Part();
        this.aacnSchemaObject.Costs.Assessed[index].ACSItemID = ((IDataItem) list[index]).getValue(assessedPartKeys.Costs_Assessed_Part_ACSItemID);
        this.aacnSchemaObject.Costs.Assessed[index].HourlyRate = ((IDataItem) list[index]).getValue(assessedPartKeys.Costs_Assessed_Part_HourlyRate);
      }
    }
  }
}
