﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitOCF22Mapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Collections;

namespace PMSToolkit.Mapper
{
  internal class SubmitOCF22Mapper : BaseMapper
  {
    private OCF22Keys m_ocf22Keys = new OCF22Keys();
    private const string m_xmlTemplate = "{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" /><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF22><RegulatedHealthProfessional><FacilityRegistryID>{56}</FacilityRegistryID><ProviderRegistryID>{16}</ProviderRegistryID><Occupation>{17}</Occupation><ConflictOfInterest><ConflictExists>{18}</ConflictExists><ConflictDetails>{19}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{57}</IsSignatureOnFile><DateSigned>{58}</DateSigned></RegulatedHealthProfessional><NatureOfAssessment><Response>{20}</Response></NatureOfAssessment><ProvisionalClinicalInformation><ClinicalInformation><PresentComplaintsDesc>{21}</PresentComplaintsDesc><AlreadyProvidedTreatment><Response>{22}</Response></AlreadyProvidedTreatment></ClinicalInformation><AssessmentInformation><Details>{23}</Details><PriorAssessment><Response>{24}</Response><AssessmentDate>{25}</AssessmentDate></PriorAssessment></AssessmentInformation></ProvisionalClinicalInformation><ApplicantSignature><IsApplicantSignatureOnFile>{26}</IsApplicantSignatureOnFile><SigningApplicant><FirstName>{27}</FirstName><LastName>{28}</LastName></SigningApplicant><SigningDate>{29}</SigningDate></ApplicantSignature><AdditionalComments>{59}</AdditionalComments><AttachmentsBeingSent>{60}</AttachmentsBeingSent><OCFVersion>{54}</OCFVersion><PatientKey>{55}</PatientKey></OCF22></inlineData></value></healthDocumentAttachment></pertinentInformation1><coverage><policyOrAccount><id root=\"{3}\" extension=\"{30}\"/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"{3}\" extension=\"{13}\"/><code {31}/><coveredPartyAsPatientPerson><name><given>{32}</given><given>{33}</given><family>{34}</family></name><administrativeGenderCode code=\"{35}\"/><birthTime value=\"{36}\"/><addr><streetAddressLine>{37}</streetAddressLine><streetAddressLine>{38}</streetAddressLine><city>{39}</city><state>{40}</state><postalCode>{41}</postalCode></addr><telecom value=\"tel:{14};ext={15}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{43}</given><family>{44}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{45}\"/></accident></pertinentInformation></policyOrAccount></coverage><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"ProposedGoodsAndServices\"/><code code=\"FININV\"/><netAmt value=\"{46}\"/>{47}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{50}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{48}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{48}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{49}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{49}\"/></invoiceElementDetail></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{53}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{51}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{51}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{52}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{52}\"/></invoiceElementDetail></component></invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN300204UV02>";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" /><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF22><RegulatedHealthProfessional><FacilityRegistryID>{56}</FacilityRegistryID><ProviderRegistryID>{16}</ProviderRegistryID><Occupation>{17}</Occupation><ConflictOfInterest><ConflictExists>{18}</ConflictExists><ConflictDetails>{19}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{57}</IsSignatureOnFile><DateSigned>{58}</DateSigned></RegulatedHealthProfessional><NatureOfAssessment><Response>{20}</Response></NatureOfAssessment><ProvisionalClinicalInformation><ClinicalInformation><PresentComplaintsDesc>{21}</PresentComplaintsDesc><AlreadyProvidedTreatment><Response>{22}</Response></AlreadyProvidedTreatment></ClinicalInformation><AssessmentInformation><Details>{23}</Details><PriorAssessment><Response>{24}</Response><AssessmentDate>{25}</AssessmentDate></PriorAssessment></AssessmentInformation></ProvisionalClinicalInformation><ApplicantSignature><IsApplicantSignatureOnFile>{26}</IsApplicantSignatureOnFile><SigningApplicant><FirstName>{27}</FirstName><LastName>{28}</LastName></SigningApplicant><SigningDate>{29}</SigningDate></ApplicantSignature><AdditionalComments>{59}</AdditionalComments><AttachmentsBeingSent>{60}</AttachmentsBeingSent><OCFVersion>{54}</OCFVersion><PatientKey>{55}</PatientKey></OCF22></inlineData></value></healthDocumentAttachment></pertinentInformation1><coverage><policyOrAccount><id root=\"{3}\" extension=\"{30}\"/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"{3}\" extension=\"{13}\"/><code {31}/><coveredPartyAsPatientPerson><name><given>{32}</given><given>{33}</given><family>{34}</family></name><administrativeGenderCode code=\"{35}\"/><birthTime value=\"{36}\"/><addr><streetAddressLine>{37}</streetAddressLine><streetAddressLine>{38}</streetAddressLine><city>{39}</city><state>{40}</state><postalCode>{41}</postalCode></addr><telecom value=\"tel:{14};ext={15}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{43}</given><family>{44}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{45}\"/></accident></pertinentInformation></policyOrAccount></coverage><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"ProposedGoodsAndServices\"/><code code=\"FININV\"/><netAmt value=\"{46}\"/>{47}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{50}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{48}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{48}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{49}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{49}\"/></invoiceElementDetail></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{53}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{51}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{51}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{52}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{52}\"/></invoiceElementDetail></component></invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN300204UV02>", (object[]) this.getOCF22StringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private string[] getOCF22StringArgs(IDataItem data)
    {
      string[] strArray = new string[63];
      string encodedValue1 = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_FacilityRegistryID);
      string encodedValue2 = data.getEncodedValue(this.m_ocf22Keys.PMSFields_PMSDocumentKey);
      string encodedValue3 = data.getEncodedValue(this.m_ocf22Keys.Insurer_IBCInsurerID);
      strArray[0] = this.getSubmissionHeader((object) "FICR_IN300204UV02");
      strArray[1] = encodedValue1;
      strArray[2] = encodedValue2;
      strArray[3] = encodedValue3;
      strArray[4] = this.getReceiverElement((object) encodedValue3, (object) data.getEncodedValue(this.m_ocf22Keys.Insurer_IBCBranchID), (object) data.getEncodedValue(this.m_ocf22Keys.Insurer_Adjuster_TelephoneNumber), (object) data.getEncodedValue(this.m_ocf22Keys.Insurer_Adjuster_TelephoneExtension), (object) data.getEncodedValue(this.m_ocf22Keys.Insurer_Adjuster_FaxNumber), (object) data.getEncodedValue(this.m_ocf22Keys.Insurer_Adjuster_Name_FirstName), (object) data.getEncodedValue(this.m_ocf22Keys.Insurer_Adjuster_Name_LastName));
      strArray[5] = this.getSenderElement((object) data.getEncodedValue(this.m_ocf22Keys.PMSFields_PMSSoftware), (object) data.getEncodedValue(this.m_ocf22Keys.PMSFields_PMSVersion));
      strArray[6] = this.getCreditElement((object) encodedValue1, (object) null);
      strArray[11] = data.getEncodedValue(this.m_ocf22Keys.OCF22_InsurerTotals_AutoInsurerTotal_Proposed);
      strArray[13] = data.getEncodedValue(this.m_ocf22Keys.Header_ClaimNumber);
      strArray[14] = data.getEncodedValue(this.m_ocf22Keys.Applicant_TelephoneNumber);
      strArray[15] = data.getEncodedValue(this.m_ocf22Keys.Applicant_TelephoneExtension);
      strArray[16] = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_ProviderRegistryID);
      strArray[17] = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_Occupation);
      strArray[18] = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists);
      strArray[19] = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails);
      strArray[20] = data.getEncodedValue(this.m_ocf22Keys.OCF22_NatureOfAssessment_Response);
      strArray[21] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ProvisionalClinicalInformation_ClinicalInformation_PresentComplaintsDesc);
      strArray[22] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ProvisionalClinicalInformation_ClinicalInformation_AlreadyProvidedTreatment_Response);
      strArray[23] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ProvisionalClinicalInformation_AssessmentInformation_Details);
      strArray[24] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_Response);
      strArray[25] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_AssessmentDate);
      strArray[26] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ApplicantSignature_IsApplicantSignatureOnFile);
      strArray[27] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ApplicantSignature_SigningApplicant_FirstName);
      strArray[28] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ApplicantSignature_SigningApplicant_LastName);
      strArray[29] = data.getEncodedValue(this.m_ocf22Keys.OCF22_ApplicantSignature_SigningDate);
      strArray[30] = data.getEncodedValue(this.m_ocf22Keys.Header_PolicyNumber);
      strArray[31] = ConvertUtil.convertPolicyHolderSameAsToNeCST(data.getEncodedValue(this.m_ocf22Keys.Insurer_PolicyHolder_IsSameAsApplicant));
      strArray[32] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Name_FirstName);
      strArray[33] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Name_MiddleName);
      strArray[34] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Name_LastName);
      strArray[35] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Gender);
      strArray[36] = data.getEncodedValue(this.m_ocf22Keys.Applicant_DateOfBirth);
      strArray[37] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Address_StreetAddress1);
      strArray[38] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Address_StreetAddress2);
      strArray[39] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Address_City);
      strArray[40] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Address_Province);
      strArray[41] = data.getEncodedValue(this.m_ocf22Keys.Applicant_Address_PostalCode);
      strArray[43] = data.getEncodedValue(this.m_ocf22Keys.Insurer_PolicyHolder_Name_FirstName);
      strArray[44] = data.getEncodedValue(this.m_ocf22Keys.Insurer_PolicyHolder_Name_LastName);
      strArray[45] = data.getEncodedValue(this.m_ocf22Keys.Header_DateOfAccident);
      strArray[46] = data.getEncodedValue(this.m_ocf22Keys.OCF22_InsurerTotals_SubTotal_Proposed);
      strArray[47] = this.getGSLineItems(data);
      strArray[48] = data.getEncodedValue(this.m_ocf22Keys.OCF22_InsurerTotals_GST_Proposed);
      strArray[49] = data.getEncodedValue(this.m_ocf22Keys.OCF22_InsurerTotals_PST_Proposed);
      try
      {
        strArray[50] = (Decimal.Parse(strArray[48]) + Decimal.Parse(strArray[49])).ToString();
      }
      catch
      {
        strArray[50] = "0.0";
      }
      strArray[51] = strArray[51] = data.getEncodedValue(this.m_ocf22Keys.OCF22_InsurerTotals_MOH_Proposed);
      strArray[52] = data.getEncodedValue(this.m_ocf22Keys.OCF22_InsurerTotals_OtherInsurers_Proposed);
      try
      {
        strArray[53] = (Decimal.Parse(strArray[51]) + Decimal.Parse(strArray[52])).ToString();
      }
      catch
      {
        strArray[53] = "0.0";
      }
      strArray[54] = data.getEncodedValue(this.m_ocf22Keys.OCFVersion);
      strArray[55] = data.getEncodedValue(this.m_ocf22Keys.PMSFields_PMSPatientKey);
      strArray[56] = encodedValue1;
      strArray[57] = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_IsSignatureOnFile);
      strArray[58] = data.getEncodedValue(this.m_ocf22Keys.OCF22_RegulatedHealthProfessional_DateSigned);
      strArray[59] = data.getEncodedValue(this.m_ocf22Keys.AdditionalComments);
      strArray[60] = data.getEncodedValue(this.m_ocf22Keys.AttachmentsBeingSent);
      strArray[61] = "";
      strArray[62] = "";
      return strArray;
    }

    private string getGSLineItems(IDataItem data)
    {
      string str = "";
      GS22LineItemKeys gs22LineItemKeys = new GS22LineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) data.getList(LineItemType.GS22LineItem))
      {
        object[] objArray = new object[10]
        {
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Quantity),
          (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Measure)),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_LineCost),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_GST),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_PST),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Code),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_ProviderReference_Occupation),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Attribute),
          (object) dataItem.getEncodedValue(gs22LineItemKeys.OCF22_ProposedGoodsAndServices_Items_Item_Estimated_PMSGSKey)
        };
        str += string.Format("<component><sequenceNumber value=\"\"/><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{9}\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"{0}\" unit=\"{1}\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{2}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{2}\"/><fstInd value=\"{3}\"/><pstInd value=\"{4}\"/><factorNumber value=\"1\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{5}\"><qualifier code=\"{8}\"/></code><effectiveTime nullFlavor=\"NA\"/><performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{6}\"/><code code=\"{7}\"/></healthCareProvider></performer><location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component>", objArray);
      }
      return str;
    }
  }
}
