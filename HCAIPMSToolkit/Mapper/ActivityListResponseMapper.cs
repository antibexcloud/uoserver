﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.ActivityListResponseMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Xml;

namespace PMSToolkit.Mapper
{
  internal class ActivityListResponseMapper : BaseXmlMapper
  {
    public override IDataItem transform(IDataItem data)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument1 = new XmlDocument();
      xmlDocument1.LoadXml(xml);
      ActivityList activityList = new ActivityList();
      XmlDocument xmlDocument2 = xmlDocument1;
      activityList.Xmn = (XmlNode) xmlDocument2;
      return (IDataItem) activityList;
    }
  }
}
