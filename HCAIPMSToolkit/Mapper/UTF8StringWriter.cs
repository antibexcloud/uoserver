﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.UTF8StringWriter
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.IO;
using System.Text;

namespace PMSToolkit.Mapper
{
  public class UTF8StringWriter : StringWriter
  {
    public override Encoding Encoding
    {
      get
      {
        return (Encoding) new UTF8Encoding(false);
      }
    }
  }
}
