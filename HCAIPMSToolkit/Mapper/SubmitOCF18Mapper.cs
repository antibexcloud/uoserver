﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitOCF18Mapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Collections;

namespace PMSToolkit.Mapper
{
  internal class SubmitOCF18Mapper : BaseMapper
  {
    private OCF18Keys m_ocf18Keys = new OCF18Keys();
    private const string m_xmlTemplate = "{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF18><Header><PlanNumber>{13}</PlanNumber></Header><HealthPractitioner><FacilityRegistryID>{134}</FacilityRegistryID><ProviderRegistryID>{28}</ProviderRegistryID><Occupation>{29}</Occupation><ConflictOfInterest><ConflictExists>{30}</ConflictExists><ConflictDetails>{31}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{32}</IsSignatureOnFile><DateSigned>{33}</DateSigned></HealthPractitioner><OtherInsurance><IsThereOtherInsuranceCoverage>{16}</IsThereOtherInsuranceCoverage></OtherInsurance><IsOtherHealthPractitioner>{34}</IsOtherHealthPractitioner><OtherHealthPractitioner><Name><FirstName>{35}</FirstName><LastName>{36}</LastName></Name><FacilityName>{37}</FacilityName><AISIFacilityNumber>{38}</AISIFacilityNumber><FacilityRegistryID>{138}</FacilityRegistryID><Address><StreetAddress1>{39}</StreetAddress1><StreetAddress2>{40}</StreetAddress2><City>{41}</City><Province>{42}</Province><PostalCode>{43}</PostalCode></Address><TelephoneNumber>{44}</TelephoneNumber><TelephoneExtension>{45}</TelephoneExtension><FaxNumber>{46}</FaxNumber><EMail>{47}</EMail><ProviderRegistryID>{48}</ProviderRegistryID><Occupation>{49}</Occupation><ConflictOfInterest><ConflictExists>{50}</ConflictExists><ConflictDetails>{51}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{52}</IsSignatureOnFile><DateSigned>{53}</DateSigned></OtherHealthPractitioner><IsRHPSameAsHealthPractitioner>{54}</IsRHPSameAsHealthPractitioner><RegulatedHealthProfessional><FacilityRegistryID>{131}</FacilityRegistryID><ProviderRegistryID>{56}</ProviderRegistryID><Occupation>{57}</Occupation><ConflictOfInterest><ConflictExists>{58}</ConflictExists><ConflictDetails>{59}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{126}</IsSignatureOnFile><DateSigned>{127}</DateSigned></RegulatedHealthProfessional><InjuriesAndSequelae>{60}</InjuriesAndSequelae><PriorAndConcurrentConditions><PriorCondition><Response>{61}</Response><Explanation>{62}</Explanation><InvestigationOrTreatment><Response>{63}</Response><Explanation>{64}</Explanation></InvestigationOrTreatment></PriorCondition><ConcurrentCondition><Response>{65}</Response><Explanation>{66}</Explanation></ConcurrentCondition><IsPAF><Response>{67}</Response><Circumstance>{139}</Circumstance><Explanation>{68}</Explanation></IsPAF></PriorAndConcurrentConditions><ActivityLimitations><ToEmployment><Response>{69}</Response></ToEmployment><ToNormalLife><Response>{70}</Response></ToNormalLife><ImpactOnAbilities>{71}</ImpactOnAbilities><ModifiedEmployment><Response>{72}</Response><Explanation>{73}</Explanation></ModifiedEmployment></ActivityLimitations><PlanGoalsOutcomesAndMethods><Goals><PainReduction>{74}</PainReduction><IncreaseInStrength>{75}</IncreaseInStrength><IncreasedRangeOfMotion>{76}</IncreasedRangeOfMotion><Other>{77}</Other><OtherDescription>{78}</OtherDescription></Goals><FunctionalGoals><ReturnToNormalLiving>{79}</ReturnToNormalLiving><ReturnToModifiedWorkActivities>{80}</ReturnToModifiedWorkActivities><ReturnToPreAccidentWorkActivities>{81}</ReturnToPreAccidentWorkActivities><Other>{82}</Other><OtherDescription>{83}</OtherDescription></FunctionalGoals><Evaluation><ProgressEvaluation>{84}</ProgressEvaluation><PreviousPlanImpact>{85}</PreviousPlanImpact></Evaluation><BarriersToRecovery><Response>{86}</Response><Explanation>{87}</Explanation><Recommendations><Response>{88}</Response><Explanation>{89}</Explanation></Recommendations></BarriersToRecovery><ConcurrentTreatment><Response>{90}</Response><Explanation>{91}</Explanation></ConcurrentTreatment><Consistency><Response>{92}</Response><Explanation>{93}</Explanation></Consistency></PlanGoalsOutcomesAndMethods><ProposedGoodsAndServices><TreatmentPlanDuration>{94}</TreatmentPlanDuration><AlreadyProvidedTreatmentVisits>{95}</AlreadyProvidedTreatmentVisits><GoodsAndServicesComments>{96}</GoodsAndServicesComments><ApplicantInitialsOnFile>{137}</ApplicantInitialsOnFile></ProposedGoodsAndServices><ApplicantSignature><IsApplicantSignatureWaivedByInsurer>{97}</IsApplicantSignatureWaivedByInsurer><IsApplicantSignatureOnFile>{98}</IsApplicantSignatureOnFile><SigningApplicant><FirstName>{99}</FirstName><LastName>{100}</LastName></SigningApplicant><SigningDate>{101}</SigningDate></ApplicantSignature><AdditionalComments>{132}</AdditionalComments><AttachmentsBeingSent>{133}</AttachmentsBeingSent><OCFVersion>{128}</OCFVersion><PatientKey>{130}</PatientKey></OCF18></inlineData></value></healthDocumentAttachment></pertinentInformation1><coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {55}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {102}/><code {113}/><coveredPartyAsPatientPerson><name><given>{103}</given><given>{104}</given><family>{105}</family></name><administrativeGenderCode code=\"{106}\"/><birthTime value=\"{107}\"/><addr><streetAddressLine>{108}</streetAddressLine><streetAddressLine>{109}</streetAddressLine><city>{110}</city><state>{111}</state><postalCode>{112}</postalCode></addr><telecom value=\"tel:{14};ext={15}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{114}</given><family>{115}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{116}\"/></accident></pertinentInformation></policyOrAccount></coverage>{129}<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"ProposedGoodsAndServices\"/><code code=\"FININV\"/><netAmt value=\"{117}\"/>{118}{119}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{122}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{120}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{120}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{121}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{121}\"/></invoiceElementDetail></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{125}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{123}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{123}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{124}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{124}\"/></invoiceElementDetail></component></invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN300204UV02>";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF18><Header><PlanNumber>{13}</PlanNumber></Header><HealthPractitioner><FacilityRegistryID>{134}</FacilityRegistryID><ProviderRegistryID>{28}</ProviderRegistryID><Occupation>{29}</Occupation><ConflictOfInterest><ConflictExists>{30}</ConflictExists><ConflictDetails>{31}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{32}</IsSignatureOnFile><DateSigned>{33}</DateSigned></HealthPractitioner><OtherInsurance><IsThereOtherInsuranceCoverage>{16}</IsThereOtherInsuranceCoverage></OtherInsurance><IsOtherHealthPractitioner>{34}</IsOtherHealthPractitioner><OtherHealthPractitioner><Name><FirstName>{35}</FirstName><LastName>{36}</LastName></Name><FacilityName>{37}</FacilityName><AISIFacilityNumber>{38}</AISIFacilityNumber><FacilityRegistryID>{138}</FacilityRegistryID><Address><StreetAddress1>{39}</StreetAddress1><StreetAddress2>{40}</StreetAddress2><City>{41}</City><Province>{42}</Province><PostalCode>{43}</PostalCode></Address><TelephoneNumber>{44}</TelephoneNumber><TelephoneExtension>{45}</TelephoneExtension><FaxNumber>{46}</FaxNumber><EMail>{47}</EMail><ProviderRegistryID>{48}</ProviderRegistryID><Occupation>{49}</Occupation><ConflictOfInterest><ConflictExists>{50}</ConflictExists><ConflictDetails>{51}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{52}</IsSignatureOnFile><DateSigned>{53}</DateSigned></OtherHealthPractitioner><IsRHPSameAsHealthPractitioner>{54}</IsRHPSameAsHealthPractitioner><RegulatedHealthProfessional><FacilityRegistryID>{131}</FacilityRegistryID><ProviderRegistryID>{56}</ProviderRegistryID><Occupation>{57}</Occupation><ConflictOfInterest><ConflictExists>{58}</ConflictExists><ConflictDetails>{59}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{126}</IsSignatureOnFile><DateSigned>{127}</DateSigned></RegulatedHealthProfessional><InjuriesAndSequelae>{60}</InjuriesAndSequelae><PriorAndConcurrentConditions><PriorCondition><Response>{61}</Response><Explanation>{62}</Explanation><InvestigationOrTreatment><Response>{63}</Response><Explanation>{64}</Explanation></InvestigationOrTreatment></PriorCondition><ConcurrentCondition><Response>{65}</Response><Explanation>{66}</Explanation></ConcurrentCondition><IsPAF><Response>{67}</Response><Circumstance>{139}</Circumstance><Explanation>{68}</Explanation></IsPAF></PriorAndConcurrentConditions><ActivityLimitations><ToEmployment><Response>{69}</Response></ToEmployment><ToNormalLife><Response>{70}</Response></ToNormalLife><ImpactOnAbilities>{71}</ImpactOnAbilities><ModifiedEmployment><Response>{72}</Response><Explanation>{73}</Explanation></ModifiedEmployment></ActivityLimitations><PlanGoalsOutcomesAndMethods><Goals><PainReduction>{74}</PainReduction><IncreaseInStrength>{75}</IncreaseInStrength><IncreasedRangeOfMotion>{76}</IncreasedRangeOfMotion><Other>{77}</Other><OtherDescription>{78}</OtherDescription></Goals><FunctionalGoals><ReturnToNormalLiving>{79}</ReturnToNormalLiving><ReturnToModifiedWorkActivities>{80}</ReturnToModifiedWorkActivities><ReturnToPreAccidentWorkActivities>{81}</ReturnToPreAccidentWorkActivities><Other>{82}</Other><OtherDescription>{83}</OtherDescription></FunctionalGoals><Evaluation><ProgressEvaluation>{84}</ProgressEvaluation><PreviousPlanImpact>{85}</PreviousPlanImpact></Evaluation><BarriersToRecovery><Response>{86}</Response><Explanation>{87}</Explanation><Recommendations><Response>{88}</Response><Explanation>{89}</Explanation></Recommendations></BarriersToRecovery><ConcurrentTreatment><Response>{90}</Response><Explanation>{91}</Explanation></ConcurrentTreatment><Consistency><Response>{92}</Response><Explanation>{93}</Explanation></Consistency></PlanGoalsOutcomesAndMethods><ProposedGoodsAndServices><TreatmentPlanDuration>{94}</TreatmentPlanDuration><AlreadyProvidedTreatmentVisits>{95}</AlreadyProvidedTreatmentVisits><GoodsAndServicesComments>{96}</GoodsAndServicesComments><ApplicantInitialsOnFile>{137}</ApplicantInitialsOnFile></ProposedGoodsAndServices><ApplicantSignature><IsApplicantSignatureWaivedByInsurer>{97}</IsApplicantSignatureWaivedByInsurer><IsApplicantSignatureOnFile>{98}</IsApplicantSignatureOnFile><SigningApplicant><FirstName>{99}</FirstName><LastName>{100}</LastName></SigningApplicant><SigningDate>{101}</SigningDate></ApplicantSignature><AdditionalComments>{132}</AdditionalComments><AttachmentsBeingSent>{133}</AttachmentsBeingSent><OCFVersion>{128}</OCFVersion><PatientKey>{130}</PatientKey></OCF18></inlineData></value></healthDocumentAttachment></pertinentInformation1><coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {55}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {102}/><code {113}/><coveredPartyAsPatientPerson><name><given>{103}</given><given>{104}</given><family>{105}</family></name><administrativeGenderCode code=\"{106}\"/><birthTime value=\"{107}\"/><addr><streetAddressLine>{108}</streetAddressLine><streetAddressLine>{109}</streetAddressLine><city>{110}</city><state>{111}</state><postalCode>{112}</postalCode></addr><telecom value=\"tel:{14};ext={15}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{114}</given><family>{115}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{116}\"/></accident></pertinentInformation></policyOrAccount></coverage>{129}<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"ProposedGoodsAndServices\"/><code code=\"FININV\"/><netAmt value=\"{117}\"/>{118}{119}</invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"TaxesAndInterest\"/><code code=\"FININV\"/><netAmt value=\"{122}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"FST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{120}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{120}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\"/><code code=\"PST\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{121}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{121}\"/></invoiceElementDetail></component></invoiceElementGroup></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsuranceAmounts\"/><code code=\"FININV\"/><netAmt value=\"{125}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"MOH\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{123}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{123}\"/></invoiceElementDetail></component><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherInsurers\"/><code code=\"FININV\"/><unitQuantity><numerator value=\"1\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{124}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{124}\"/></invoiceElementDetail></component></invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN300204UV02>", (object[]) this.getOCF18StringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private string[] getOCF18StringArgs(IDataItem data)
    {
      string[] strArray = new string[140];
      string encodedValue1 = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID);
      if (string.IsNullOrEmpty(encodedValue1) && data.getEncodedValue(this.m_ocf18Keys.OCF18_IsRHPSameAsHealthPractitioner).ToUpper() == "YES")
        encodedValue1 = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_FacilityRegistryID);
      string encodedValue2 = data.getEncodedValue(this.m_ocf18Keys.PMSFields_PMSDocumentKey);
      string encodedValue3 = data.getEncodedValue(this.m_ocf18Keys.Insurer_IBCInsurerID);
      strArray[0] = this.getSubmissionHeader((object) "FICR_IN300204UV02");
      strArray[1] = (string) null;
      strArray[2] = encodedValue2;
      strArray[3] = encodedValue3;
      strArray[4] = this.getReceiverElement((object) encodedValue3, (object) data.getEncodedValue(this.m_ocf18Keys.Insurer_IBCBranchID), (object) data.getEncodedValue(this.m_ocf18Keys.Insurer_Adjuster_TelephoneNumber), (object) data.getEncodedValue(this.m_ocf18Keys.Insurer_Adjuster_TelephoneExtension), (object) data.getEncodedValue(this.m_ocf18Keys.Insurer_Adjuster_FaxNumber), (object) data.getEncodedValue(this.m_ocf18Keys.Insurer_Adjuster_Name_FirstName), (object) data.getEncodedValue(this.m_ocf18Keys.Insurer_Adjuster_Name_LastName));
      strArray[5] = this.getSenderElement((object) data.getEncodedValue(this.m_ocf18Keys.PMSFields_PMSSoftware), (object) data.getEncodedValue(this.m_ocf18Keys.PMSFields_PMSVersion));
      strArray[6] = this.getCreditElement((object) encodedValue1, (object) null);
      strArray[11] = data.getEncodedValue(this.m_ocf18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Proposed);
      strArray[13] = data.getEncodedValue(this.m_ocf18Keys.OCF18_Header_PlanNumber);
      strArray[14] = data.getEncodedValue(this.m_ocf18Keys.Applicant_TelephoneNumber);
      strArray[15] = data.getEncodedValue(this.m_ocf18Keys.Applicant_TelephoneExtension);
      strArray[16] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage);
      strArray[17] = (string) null;
      strArray[18] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID);
      strArray[19] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name);
      strArray[20] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
      strArray[21] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
      strArray[22] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
      strArray[23] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID);
      strArray[24] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name);
      strArray[25] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
      strArray[26] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
      strArray[27] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
      strArray[28] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_ProviderRegistryID);
      strArray[29] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_Occupation);
      strArray[30] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists);
      strArray[31] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails);
      strArray[32] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_IsSignatureOnFile);
      strArray[33] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_DateSigned);
      strArray[34] = data.getEncodedValue(this.m_ocf18Keys.OCF18_IsOtherHealthPractitioner);
      strArray[35] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_FirstName);
      strArray[36] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Name_LastName);
      strArray[37] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_FacilityName);
      strArray[38] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_AISIFacilityNumber);
      strArray[39] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress1);
      strArray[40] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress2);
      strArray[41] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_City);
      strArray[42] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_Province);
      strArray[43] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Address_PostalCode);
      strArray[44] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_TelephoneNumber);
      strArray[45] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_TelephoneExtension);
      strArray[46] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_FaxNumber);
      strArray[47] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Email);
      strArray[48] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_ProviderRegistryNumber);
      strArray[49] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_Occupation);
      strArray[50] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists);
      strArray[51] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails);
      strArray[52] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_IsSignatureOnFile);
      strArray[53] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_DateSigned);
      strArray[54] = data.getEncodedValue(this.m_ocf18Keys.OCF18_IsRHPSameAsHealthPractitioner);
      strArray[55] = this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.Header_PolicyNumber));
      strArray[56] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_ProviderRegistryID);
      strArray[57] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_Occupation);
      strArray[58] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists);
      strArray[59] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails);
      strArray[60] = this.getInjuries(data);
      strArray[61] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Response);
      strArray[62] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation);
      strArray[63] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response);
      strArray[64] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);
      strArray[65] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response);
      strArray[66] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation);
      strArray[67] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Response);
      strArray[68] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation);
      strArray[69] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ActivityLimitations_ToEmployment_Response);
      strArray[70] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ActivityLimitations_ToNormalLife_Response);
      strArray[71] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ActivityLimitations_ImpactOnAbilities);
      strArray[72] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Response);
      strArray[73] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Explanation);
      strArray[74] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction);
      strArray[75] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength);
      strArray[76] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion);
      strArray[77] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other);
      strArray[78] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription);
      strArray[79] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving);
      strArray[80] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities);
      strArray[81] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities);
      strArray[82] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other);
      strArray[83] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription);
      strArray[84] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation);
      strArray[85] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact);
      strArray[86] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response);
      strArray[87] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation);
      strArray[88] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response);
      strArray[89] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation);
      strArray[90] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response);
      strArray[91] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation);
      strArray[92] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response);
      strArray[93] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation);
      strArray[94] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration);
      strArray[95] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits);
      strArray[96] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments);
      strArray[97] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer);
      strArray[98] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureOnFile);
      strArray[99] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ApplicantSignature_SigningApplicant_FirstName);
      strArray[100] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ApplicantSignature_SigningApplicant_LastName);
      strArray[101] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ApplicantSignature_SigningDate);
      strArray[102] = this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.Header_ClaimNumber));
      strArray[103] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_FirstName);
      strArray[104] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_MiddleName);
      strArray[105] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_LastName);
      strArray[106] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Gender);
      strArray[107] = data.getEncodedValue(this.m_ocf18Keys.Applicant_DateOfBirth);
      strArray[108] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Address_StreetAddress1);
      strArray[109] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Address_StreetAddress2);
      strArray[110] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Address_City);
      strArray[111] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Address_Province);
      strArray[112] = data.getEncodedValue(this.m_ocf18Keys.Applicant_Address_PostalCode);
      strArray[113] = ConvertUtil.convertPolicyHolderSameAsToNeCST(data.getEncodedValue(this.m_ocf18Keys.Insurer_PolicyHolder_IsSameAsApplicant));
      strArray[114] = data.getEncodedValue(this.m_ocf18Keys.Insurer_PolicyHolder_Name_FirstName);
      strArray[115] = data.getEncodedValue(this.m_ocf18Keys.Insurer_PolicyHolder_Name_LastName);
      strArray[116] = data.getEncodedValue(this.m_ocf18Keys.Header_DateOfAccident);
      strArray[117] = data.getEncodedValue(this.m_ocf18Keys.OCF18_InsurerTotals_SubTotal_Proposed);
      strArray[118] = this.getSessionHeaderLineItems(data);
      strArray[119] = this.getNonSessionLineItems(data);
      strArray[120] = data.getEncodedValue(this.m_ocf18Keys.OCF18_InsurerTotals_GST_Proposed);
      strArray[121] = data.getEncodedValue(this.m_ocf18Keys.OCF18_InsurerTotals_PST_Proposed);
      try
      {
        strArray[122] = (Decimal.Parse(strArray[120]) + Decimal.Parse(strArray[121])).ToString();
      }
      catch
      {
        strArray[122] = "0.0";
      }
      strArray[123] = data.getEncodedValue(this.m_ocf18Keys.OCF18_InsurerTotals_MOH_Proposed);
      strArray[124] = data.getEncodedValue(this.m_ocf18Keys.OCF18_InsurerTotals_OtherInsurers_Proposed);
      try
      {
        strArray[125] = (Decimal.Parse(strArray[123]) + Decimal.Parse(strArray[124])).ToString();
      }
      catch
      {
        strArray[125] = "0.0";
      }
      strArray[126] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_IsSignatureOnFile);
      strArray[(int) sbyte.MaxValue] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_DateSigned);
      strArray[128] = data.getEncodedValue(this.m_ocf18Keys.OCFVersion);
      strArray[129] = this.getOtherInsuranceCoverage(data);
      strArray[130] = data.getEncodedValue(this.m_ocf18Keys.PMSFields_PMSPatientKey);
      strArray[131] = data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID);
      strArray[132] = data.getEncodedValue(this.m_ocf18Keys.AdditionalComments);
      strArray[133] = data.getEncodedValue(this.m_ocf18Keys.AttachmentsBeingSent);
      strArray[134] = data.getEncodedValue(this.m_ocf18Keys.OCF18_HealthPractitioner_FacilityRegistryID);
      strArray[135] = "";
      strArray[136] = "";
      strArray[137] = data.getEncodedValue(this.m_ocf18Keys.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile);
      strArray[138] = data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherHealthPractitioner_FacilityRegistryID);
      strArray[139] = data.getEncodedValue(this.m_ocf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance);
      return strArray;
    }

    private string getNonSessionLineItems(IDataItem data)
    {
      string str1 = string.Empty;
      GS18LineItemKeys gs18LineItemKeys = new GS18LineItemKeys();
      IList list = data.getList(LineItemType.GS18LineItem);
      if (list.Count > 0)
      {
        string str2 = "<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"NonSessionGoodsAndServices\"/><code code=\"\"/><netAmt  nullFlavor=\"NI\"/>";
        foreach (IDataItem dataItem in (IEnumerable) list)
        {
          object[] objArray = new object[15]
          {
            (object) "",
            (object) data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity),
            (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure)),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation),
            (object) dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey),
            (object) this.populateDisplayNameAttribute(dataItem.getEncodedValue(gs18LineItemKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Description))
          };
          str2 = objArray[2] == null || !string.IsNullOrEmpty(objArray[2].ToString()) || (objArray[3] == null || !string.IsNullOrEmpty(objArray[3].ToString())) || (objArray[4] == null || !string.IsNullOrEmpty(objArray[4].ToString()) || (objArray[5] == null || !string.IsNullOrEmpty(objArray[5].ToString()))) || (objArray[6] == null || !string.IsNullOrEmpty(objArray[6].ToString()) || (objArray[7] == null || !string.IsNullOrEmpty(objArray[7].ToString())) || (objArray[8] == null || !string.IsNullOrEmpty(objArray[8].ToString()) || (objArray[9] == null || !string.IsNullOrEmpty(objArray[9].ToString())))) || (objArray[10] == null || !string.IsNullOrEmpty(objArray[10].ToString()) || (objArray[11] == null || !string.IsNullOrEmpty(objArray[11].ToString())) || (objArray[12] == null || !string.IsNullOrEmpty(objArray[12].ToString()))) ? str2 + string.Format(this.getLineItemTemplate(), objArray) : str2 + this.getEmptyComponent();
        }
        str1 = str2 + "</invoiceElementGroup></component>";
      }
      return str1;
    }

    private string getSessionHeaderLineItems(IDataItem data)
    {
      string str1 = "";
      IList list = data.getList(LineItemType.GS18SessionHeaderLineItem);
      if (list.Count > 0)
      {
        Decimal num = new Decimal(0, 0, 0, false, (byte) 1);
        SessionHeaderLineItemKeys headerLineItemKeys = new SessionHeaderLineItemKeys();
        try
        {
          foreach (IDataItem dataItem in (IEnumerable) list)
            num += Decimal.Parse(dataItem.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost));
        }
        catch
        {
          num = new Decimal(0, 0, 0, false, (byte) 1);
        }
        string str2 = string.Format("<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"SessionGoodsAndServices\"/><code nullFlavor=\"NI\"/><netAmt value=\"{0}\"/>", (object) num);
        object[] objArray = new object[8];
        foreach (IDataItem data1 in (IEnumerable) list)
        {
          objArray[0] = (object) data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Quantity);
          objArray[1] = (object) ConvertUtil.convertMeasureToNeCST(data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Measure));
          objArray[2] = (object) data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost);
          objArray[3] = (object) data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost);
          objArray[4] = (object) data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count);
          objArray[5] = (object) data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code);
          objArray[6] = (object) this.getSessionLineItems(data1);
          objArray[7] = (object) data1.getEncodedValue(headerLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
          str2 += string.Format("<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"Session\"/><code nullFlavor=\"NI\"/><netAmt value=\"{3}\"/><component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{7}\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"{0}\" unit=\"{1}\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{2}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{3}\"/><fstInd nullFlavor=\"NA\"/><pstInd nullFlavor=\"NA\"/><factorNumber value=\"{4}\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{5}\"/><effectiveTime nullFlavor=\"NA\"/><location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component><component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"SessionData\"/><code nullFlavor=\"NI\"/><netAmt nullFlavor=\"NI\"/>{6}</invoiceElementGroup></component></invoiceElementGroup></component>", objArray);
        }
        str1 = str2 + "</invoiceElementGroup></component>";
      }
      return str1;
    }

    private string getSessionLineItems(IDataItem data)
    {
      string empty = string.Empty;
      SessionLineItemKeys sessionLineItemKeys = new SessionLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) data.getList(LineItemType.GS18SessionLineItem))
      {
        object[] objArray = new object[15]
        {
          null,
          (object) data.getEncodedValue(this.m_ocf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity),
          (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure)),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST),
          (object) "1",
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation),
          (object) dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey),
          (object) this.populateDisplayNameAttribute(dataItem.getEncodedValue(sessionLineItemKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Description))
        };
        empty += string.Format(this.getLineItemTemplate(), objArray);
      }
      return empty;
    }

    private string getLineItemTemplate()
    {
      return "<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{13}\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"{2}\" unit=\"{3}\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{4}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{5}\"/><fstInd value=\"{6}\"/><pstInd value=\"{7}\"/><factorNumber value=\"{8}\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{9}\"{14}><qualifier code=\"{10}\"/></code><effectiveTime nullFlavor=\"NA\"/><performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{11}\"/><code code=\"{12}\"/></healthCareProvider></performer><location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component>";
    }

    private string getInjuries(IDataItem data)
    {
      string str1 = "";
      IList list = data.getList(LineItemType.InjuryLineItem);
      OCF18InjuryLineItemKeys injuryLineItemKeys = new OCF18InjuryLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        string str2 = dataItem.getValue(injuryLineItemKeys.OCF18_InjuriesAndSequelae_Injury_Code);
        str1 += string.Format("<Injury><Code>{0}</Code></Injury>", (object) str2);
      }
      return str1;
    }

    private string getOtherInsuranceCoverage(IDataItem data)
    {
      data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage);
      return this.getMOH(data) + this.getOtherInsurer1(data) + this.getOtherInsurer2(data);
    }

    private string getMOH(IDataItem data)
    {
      return string.Format("<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.1.1.1\" {0}/><code code=\"PUBLICPOL\"/><beneficiary><coveredPartyAsPatient><code code=\"NI\"/><coveredPartyAsPatientPerson><name><given>{1}</given><given>{2}</given><family>{3}</family></name><birthTime value=\"{4}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><author><carrierRole><id root=\"2.16.840.1.113883.3.30.1.1.1\"/></carrierRole></author></policyOrAccount></coverage>", (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_MOHAvailable)), (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_FirstName), (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_MiddleName), (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_LastName), (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_DateOfBirth));
    }

    private string getOtherInsurer1(IDataItem data)
    {
      return this.getOtherInsurer(new object[8]
      {
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID)),
        (object) this.getExtensionValue("assigningAuthorityName", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name)),
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber)),
        (object) data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName),
        (object) data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName),
        (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_FirstName),
        (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_LastName),
        (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_MiddleName)
      });
    }

    private string getOtherInsurer2(IDataItem data)
    {
      return this.getOtherInsurer(new object[8]
      {
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID)),
        (object) this.getExtensionValue("assigningAuthorityName", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name)),
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber)),
        (object) data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName),
        (object) data.getEncodedValue(this.m_ocf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName),
        (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_FirstName),
        (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_LastName),
        (object) data.getEncodedValue(this.m_ocf18Keys.Applicant_Name_MiddleName)
      });
    }

    private string getOtherInsurer(object[] args)
    {
      return string.Format("<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.1.1.2\" {2}/><code code=\"EHCPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.1.1.2\" {0}/><code nullFlavor=\"NI\"/><coveredPartyAsPatientPerson><name><given>{5}</given><given>{7}</given><family>{6}</family></name><birthTime nullFlavor=\"NI\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{3}</given><family>{4}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.1.1.2\" {1}/></carrierRole></author></policyOrAccount></coverage>", args);
    }

    private string getExtensionValue(string nodename, string value)
    {
      if (string.IsNullOrEmpty(value))
        return "";
      return nodename + "=\"" + value + "\"";
    }

    private string getEmptyComponent()
    {
      return "<component><invoiceElementDetail><id></id><code></code><unitQuantity><numerator></numerator><denominator></denominator></unitQuantity><unitPriceAmt><numerator></numerator><denominator></denominator></unitPriceAmt><netAmt></netAmt></invoiceElementDetail></component>";
    }

    private string populateDisplayNameAttribute(string strDescription)
    {
      if (!string.IsNullOrWhiteSpace(strDescription))
        return " displayName=\"" + strDescription + "\"";
      return "";
    }
  }
}
