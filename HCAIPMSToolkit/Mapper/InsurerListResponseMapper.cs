﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.InsurerListResponseMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Xml;

namespace PMSToolkit.Mapper
{
  internal class InsurerListResponseMapper : BaseXmlMapper
  {
    public override IDataItem transform(IDataItem data)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(xml);
      InsurerList insurerList = new InsurerList();
      insurerList.Xmn = (XmlNode) xmlDocument;
      return (IDataItem) insurerList;
    }
  }
}
