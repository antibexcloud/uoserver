﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.AddProviderMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Collections;

namespace PMSToolkit.Mapper
{
  internal class AddProviderMapper : BaseMapper
  {
    private ProviderAddKeys m_providerAddKeys = new ProviderAddKeys();
    private const string m_xmlTemplate = "<Provider xmlns=\"http://hcai.ca\"><FacilityRegistryID>{0}</FacilityRegistryID><FirstName>{1}</FirstName><LastName>{2}</LastName><EndDate>{3}</EndDate><HourlyRate>{4}</HourlyRate><Professions>{5}</Professions></Provider>";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("<Provider xmlns=\"http://hcai.ca\"><FacilityRegistryID>{0}</FacilityRegistryID><FirstName>{1}</FirstName><LastName>{2}</LastName><EndDate>{3}</EndDate><HourlyRate>{4}</HourlyRate><Professions>{5}</Professions></Provider>", (object[]) this.getProviderAddStringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private string[] getProviderAddStringArgs(IDataItem data)
    {
      return new string[6]
      {
        data.getEncodedValue(this.m_providerAddKeys.HCAI_Facility_Registry_ID),
        data.getEncodedValue(this.m_providerAddKeys.Provider_First_Name),
        data.getEncodedValue(this.m_providerAddKeys.Provider_Last_Name),
        data.getEncodedValue(this.m_providerAddKeys.Provider_End_Date),
        data.getEncodedValue(this.m_providerAddKeys.Provider_Hourly_Rate),
        this.getProfessionList(data)
      };
    }

    private string getProfessionList(IDataItem data)
    {
      string str = "";
      IList list = data.getList(LineItemType.ProviderProfessionLineItem);
      ProviderProfessionLineItemKeys professionLineItemKeys = new ProviderProfessionLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        string encodedValue1 = dataItem.getEncodedValue(professionLineItemKeys.Provider_Profession_Registration_Number);
        string encodedValue2 = dataItem.getEncodedValue(professionLineItemKeys.Provider_Profession_Profession_Code);
        str += string.Format("<Profession><RegistrationNumber>{0}</RegistrationNumber><ProfessionCode>{1}</ProfessionCode></Profession>", (object) encodedValue1, (object) encodedValue2);
      }
      return str;
    }
  }
}
