﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.SubmitOCF23Mapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Collections;

namespace PMSToolkit.Mapper
{
  internal class SubmitOCF23Mapper : BaseMapper
  {
    private OCF23Keys m_ocf23Keys = new OCF23Keys();
    private const string m_xmlTemplate = "{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" /><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF23><HealthPractitioner><FacilityRegistryID>{74}</FacilityRegistryID><ProviderRegistryID>{29}</ProviderRegistryID><Occupation>{30}</Occupation><ConflictOfInterest><ConflictExists>{31}</ConflictExists><ConflictDetails>{32}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{33}</IsSignatureOnFile><DateSigned>{34}</DateSigned><IsFirstInitiatingHealthPractitioner>{35}</IsFirstInitiatingHealthPractitioner></HealthPractitioner><InjuriesAndSequelae>{36}</InjuriesAndSequelae><OtherInsurance><IsThereOtherInsuranceCoverage>{17}</IsThereOtherInsuranceCoverage></OtherInsurance><PriorAndConcurrentConditions><EmployedAtTimeOfAccident>{37}</EmployedAtTimeOfAccident><PriorCondition><Response>{38}</Response><Explanation>{39}</Explanation><InvestigationOrTreatment><Response>{40}</Response><Explanation>{41}</Explanation></InvestigationOrTreatment></PriorCondition></PriorAndConcurrentConditions><BarriersToRecovery><Response>{42}</Response><Explanation>{43}</Explanation></BarriersToRecovery><DirectPaymentAssignment><Response>{80}</Response></DirectPaymentAssignment><PAFPreApproveServices><PAF><PAFType>{44}</PAFType><EstimatedFee>{45}</EstimatedFee></PAF><SupplementaryGoodsAndServices><Code>{77}</Code><Description>{46}</Description><EstimatedFee>{47}</EstimatedFee><PMSGSKey>{19}</PMSGSKey></SupplementaryGoodsAndServices><OtherPreApprovedServices>{48}</OtherPreApprovedServices><SubTotal>{49}</SubTotal></PAFPreApproveServices><OtherGoodsAndServices><GoodsAndServicesComments>{18}</GoodsAndServicesComments></OtherGoodsAndServices><ApplicantSignature><IsApplicantSignatureWaivedByInsurer>{50}</IsApplicantSignatureWaivedByInsurer><IsApplicantSignatureOnFile>{51}</IsApplicantSignatureOnFile><SigningApplicant><FirstName>{52}</FirstName><LastName>{53}</LastName></SigningApplicant><SigningDate>{54}</SigningDate></ApplicantSignature><AdditionalComments>{75}</AdditionalComments><AttachmentsBeingSent>{76}</AttachmentsBeingSent><OCFVersion>{71}</OCFVersion><PatientKey>{73}</PatientKey></OCF23></inlineData></value></healthDocumentAttachment></pertinentInformation1><coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {14}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {13}/><code {55}/><coveredPartyAsPatientPerson><name><given>{56}</given><given>{57}</given><family>{58}</family></name><administrativeGenderCode code=\"{59}\"/><birthTime value=\"{60}\"/><addr><streetAddressLine>{61}</streetAddressLine><streetAddressLine>{62}</streetAddressLine><city>{63}</city><state>{64}</state><postalCode>{65}</postalCode></addr><telecom value=\"tel:{15};ext={16}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{66}</given><family>{67}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{68}\"/></accident></pertinentInformation></policyOrAccount></coverage>{72}<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherGoodsAndServices\"/><code code=\"FININV\"/><netAmt value=\"{69}\"/>{70}</invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN300204UV02>";

    public override IDataItem transform(IDataItem data)
    {
      string val = string.Format("{0}{4}{5}<controlActProcess moodCode=\"EVN\"><subject><paymentRequest><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{2}\"/><amt value=\"{11}\"/>{6}<reasonOf><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" /><code code=\"CSPINV\"/><netAmt value=\"{11}\"/><pertinentInformation1><healthDocumentAttachment><id root=\"2.16.840.1.113883.3.30.3\"/><code/><value mediaType=\"text/xml\"><inlineData><OCF23><HealthPractitioner><FacilityRegistryID>{74}</FacilityRegistryID><ProviderRegistryID>{29}</ProviderRegistryID><Occupation>{30}</Occupation><ConflictOfInterest><ConflictExists>{31}</ConflictExists><ConflictDetails>{32}</ConflictDetails></ConflictOfInterest><IsSignatureOnFile>{33}</IsSignatureOnFile><DateSigned>{34}</DateSigned><IsFirstInitiatingHealthPractitioner>{35}</IsFirstInitiatingHealthPractitioner></HealthPractitioner><InjuriesAndSequelae>{36}</InjuriesAndSequelae><OtherInsurance><IsThereOtherInsuranceCoverage>{17}</IsThereOtherInsuranceCoverage></OtherInsurance><PriorAndConcurrentConditions><EmployedAtTimeOfAccident>{37}</EmployedAtTimeOfAccident><PriorCondition><Response>{38}</Response><Explanation>{39}</Explanation><InvestigationOrTreatment><Response>{40}</Response><Explanation>{41}</Explanation></InvestigationOrTreatment></PriorCondition></PriorAndConcurrentConditions><BarriersToRecovery><Response>{42}</Response><Explanation>{43}</Explanation></BarriersToRecovery><DirectPaymentAssignment><Response>{80}</Response></DirectPaymentAssignment><PAFPreApproveServices><PAF><PAFType>{44}</PAFType><EstimatedFee>{45}</EstimatedFee></PAF><SupplementaryGoodsAndServices><Code>{77}</Code><Description>{46}</Description><EstimatedFee>{47}</EstimatedFee><PMSGSKey>{19}</PMSGSKey></SupplementaryGoodsAndServices><OtherPreApprovedServices>{48}</OtherPreApprovedServices><SubTotal>{49}</SubTotal></PAFPreApproveServices><OtherGoodsAndServices><GoodsAndServicesComments>{18}</GoodsAndServicesComments></OtherGoodsAndServices><ApplicantSignature><IsApplicantSignatureWaivedByInsurer>{50}</IsApplicantSignatureWaivedByInsurer><IsApplicantSignatureOnFile>{51}</IsApplicantSignatureOnFile><SigningApplicant><FirstName>{52}</FirstName><LastName>{53}</LastName></SigningApplicant><SigningDate>{54}</SigningDate></ApplicantSignature><AdditionalComments>{75}</AdditionalComments><AttachmentsBeingSent>{76}</AttachmentsBeingSent><OCFVersion>{71}</OCFVersion><PatientKey>{73}</PatientKey></OCF23></inlineData></value></healthDocumentAttachment></pertinentInformation1><coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.3\" {14}/><code code=\"AUTOPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.3\" {13}/><code {55}/><coveredPartyAsPatientPerson><name><given>{56}</given><given>{57}</given><family>{58}</family></name><administrativeGenderCode code=\"{59}\"/><birthTime value=\"{60}\"/><addr><streetAddressLine>{61}</streetAddressLine><streetAddressLine>{62}</streetAddressLine><city>{63}</city><state>{64}</state><postalCode>{65}</postalCode></addr><telecom value=\"tel:{15};ext={16}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{66}</given><family>{67}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.2\"/></carrierRole></author><pertinentInformation><accident><code code=\"MVA\"/><effectiveTime value=\"{68}\"/></accident></pertinentInformation></policyOrAccount></coverage>{72}<component><invoiceElementGroup><id root=\"2.16.840.1.113883.3.30.3\" extension=\"OtherGoodsAndServices\"/><code code=\"FININV\"/><netAmt value=\"{69}\"/>{70}</invoiceElementGroup></component></invoiceElementGroup></reasonOf></paymentRequest></subject></controlActProcess></FICR_IN300204UV02>", (object[]) this.getOCF23StringArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      responseMessage.addValue("message", val);
      return (IDataItem) responseMessage;
    }

    private string[] getOCF23StringArgs(IDataItem data)
    {
      string[] strArray = new string[81];
      string encodedValue1 = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_FacilityRegistryID);
      string encodedValue2 = data.getEncodedValue(this.m_ocf23Keys.PMSFields_PMSDocumentKey);
      string encodedValue3 = data.getEncodedValue(this.m_ocf23Keys.Insurer_IBCInsurerID);
      strArray[0] = this.getSubmissionHeader((object) "FICR_IN300204UV02");
      strArray[1] = encodedValue1;
      strArray[2] = encodedValue2;
      strArray[3] = encodedValue3;
      strArray[4] = this.getReceiverElement((object) encodedValue3, (object) data.getEncodedValue(this.m_ocf23Keys.Insurer_IBCBranchID), (object) data.getEncodedValue(this.m_ocf23Keys.Insurer_Adjuster_TelephoneNumber), (object) data.getEncodedValue(this.m_ocf23Keys.Insurer_Adjuster_TelephoneExtension), (object) data.getEncodedValue(this.m_ocf23Keys.Insurer_Adjuster_FaxNumber), (object) data.getEncodedValue(this.m_ocf23Keys.Insurer_Adjuster_Name_FirstName), (object) data.getEncodedValue(this.m_ocf23Keys.Insurer_Adjuster_Name_LastName));
      strArray[5] = this.getSenderElement((object) data.getEncodedValue(this.m_ocf23Keys.PMSFields_PMSSoftware), (object) data.getEncodedValue(this.m_ocf23Keys.PMSFields_PMSVersion));
      strArray[6] = this.getCreditElement((object) encodedValue1, (object) null);
      strArray[11] = data.getEncodedValue(this.m_ocf23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Proposed);
      strArray[13] = this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.Header_ClaimNumber));
      strArray[14] = this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.Header_PolicyNumber));
      strArray[15] = data.getEncodedValue(this.m_ocf23Keys.Applicant_TelephoneNumber);
      strArray[16] = data.getEncodedValue(this.m_ocf23Keys.Applicant_TelephoneExtension);
      strArray[17] = data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage);
      strArray[18] = data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherGoodsAndServices_GoodsAndServicesComments);
      strArray[19] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey);
      strArray[20] = (string) null;
      strArray[21] = (string) null;
      strArray[22] = (string) null;
      strArray[23] = (string) null;
      strArray[24] = (string) null;
      strArray[25] = (string) null;
      strArray[26] = (string) null;
      strArray[27] = (string) null;
      strArray[28] = (string) null;
      strArray[29] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_ProviderRegistryID);
      strArray[30] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_Occupation);
      strArray[31] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists);
      strArray[32] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails);
      strArray[33] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_IsSignatureOnFile);
      strArray[34] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_DateSigned);
      strArray[35] = data.getEncodedValue(this.m_ocf23Keys.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner);
      strArray[36] = this.getInjuries(data);
      strArray[37] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident);
      strArray[38] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Response);
      strArray[39] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation);
      strArray[40] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response);
      strArray[41] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);
      strArray[42] = data.getEncodedValue(this.m_ocf23Keys.OCF23_BarriersToRecovery_Response);
      strArray[43] = data.getEncodedValue(this.m_ocf23Keys.OCF23_BarriersToRecovery_Explanation);
      strArray[44] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType);
      strArray[45] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_EstimatedFee);
      strArray[46] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description);
      strArray[47] = data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee);
      strArray[48] = this.getPAFItems(data);
      strArray[49] = data.getEncodedValue(this.m_ocf23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Proposed);
      strArray[50] = data.getEncodedValue(this.m_ocf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer);
      strArray[51] = data.getEncodedValue(this.m_ocf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureOnFile);
      strArray[52] = data.getEncodedValue(this.m_ocf23Keys.OCF23_ApplicantSignature_SigningApplicant_FirstName);
      strArray[53] = data.getEncodedValue(this.m_ocf23Keys.OCF23_ApplicantSignature_SigningApplicant_LastName);
      strArray[54] = data.getEncodedValue(this.m_ocf23Keys.OCF23_ApplicantSignature_SigningDate);
      strArray[55] = ConvertUtil.convertPolicyHolderSameAsToNeCST(data.getEncodedValue(this.m_ocf23Keys.Insurer_PolicyHolder_IsSameAsApplicant));
      strArray[56] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_FirstName);
      strArray[57] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_MiddleName);
      strArray[58] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_LastName);
      strArray[59] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Gender);
      strArray[60] = data.getEncodedValue(this.m_ocf23Keys.Applicant_DateOfBirth);
      strArray[61] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Address_StreetAddress1);
      strArray[62] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Address_StreetAddress2);
      strArray[63] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Address_City);
      strArray[64] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Address_Province);
      strArray[65] = data.getEncodedValue(this.m_ocf23Keys.Applicant_Address_PostalCode);
      strArray[66] = data.getEncodedValue(this.m_ocf23Keys.Insurer_PolicyHolder_Name_FirstName);
      strArray[67] = data.getEncodedValue(this.m_ocf23Keys.Insurer_PolicyHolder_Name_LastName);
      strArray[68] = data.getEncodedValue(this.m_ocf23Keys.Header_DateOfAccident);
      strArray[69] = data.getEncodedValue(this.m_ocf23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed);
      strArray[70] = this.getOtherGoodsAndServices(data);
      strArray[71] = data.getEncodedValue(this.m_ocf23Keys.OCFVersion);
      strArray[72] = this.getOtherInsuranceCoverage(data);
      strArray[73] = data.getEncodedValue(this.m_ocf23Keys.PMSFields_PMSPatientKey);
      strArray[74] = encodedValue1;
      strArray[75] = data.getEncodedValue(this.m_ocf23Keys.AdditionalComments);
      strArray[76] = data.getEncodedValue(this.m_ocf23Keys.AttachmentsBeingSent);
      strArray[77] = !(data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType) == "WAD1") ? (!(data.getEncodedValue(this.m_ocf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType) == "WAD2") ? "PWWSC" : "PW2SC") : "PW1SC";
      strArray[78] = "";
      strArray[79] = "";
      strArray[80] = data.getEncodedValue(this.m_ocf23Keys.OCF23_DirectPaymentAssignment_Response);
      return strArray;
    }

    private string getOtherGoodsAndServices(IDataItem data)
    {
      string str = "";
      OtherGSLineItemKeys otherGsLineItemKeys = new OtherGSLineItemKeys();
      IList list = data.getList(LineItemType.GS23OtherGSLineItem);
      if (list.Count == 0)
        return this.GetEmptyComponent();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        object[] objArray = new object[9]
        {
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_Quantity),
          (object) ConvertUtil.convertMeasureToNeCST(dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_Measure)),
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Estimated_LineCost),
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_Code),
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID),
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation),
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_Attribute),
          (object) dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey),
          (object) this.populateDisplayNameAttribute(dataItem.getEncodedValue(otherGsLineItemKeys.OCF23_OtherGoodsAndServices_Items_Item_Description))
        };
        str = objArray[0] == null || !string.IsNullOrEmpty(objArray[0].ToString()) || (objArray[1] == null || !string.IsNullOrEmpty(objArray[1].ToString())) || (objArray[2] == null || !string.IsNullOrEmpty(objArray[2].ToString()) || (objArray[3] == null || !string.IsNullOrEmpty(objArray[3].ToString()))) || (objArray[4] == null || !string.IsNullOrEmpty(objArray[4].ToString()) || (objArray[5] == null || !string.IsNullOrEmpty(objArray[5].ToString())) || (objArray[6] == null || !string.IsNullOrEmpty(objArray[6].ToString()))) ? str + string.Format("<component><invoiceElementDetail><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{7}\"/><code nullFlavor=\"NI\"/><unitQuantity><numerator value=\"{0}\" unit=\"{1}\"/><denominator value=\"1\"/></unitQuantity><unitPriceAmt><numerator value=\"{2}\"/><denominator value=\"1\"/></unitPriceAmt><netAmt value=\"{2}\"/><fstInd value=\"false\"/><pstInd value=\"false\"/><factorNumber value=\"1\"/><reasonOf><billableClinicalService moodCode=\"EVN\"><code code=\"{3}\"{8}><qualifier code=\"{6}\"/></code><effectiveTime nullFlavor=\"NA\"/><performer><healthCareProvider><id root=\"2.16.840.1.113883.3.30.3\" extension=\"{4}\"/><code code=\"{5}\"/></healthCareProvider></performer><location><serviceDeliveryLocation><id nullFlavor=\"NI\"/></serviceDeliveryLocation></location></billableClinicalService></reasonOf></invoiceElementDetail></component>", objArray) : str + this.GetEmptyComponent();
      }
      return str;
    }

    private string populateDisplayNameAttribute(string strDescription)
    {
      if (!string.IsNullOrWhiteSpace(strDescription))
        return " displayName=\"" + strDescription + "\"";
      return "";
    }

    private string getInjuries(IDataItem data)
    {
      string str1 = "";
      IList list = data.getList(LineItemType.InjuryLineItem);
      OCF23InjuryLineItemKeys injuryLineItemKeys = new OCF23InjuryLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) list)
      {
        string str2 = dataItem.getValue(injuryLineItemKeys.OCF23_InjuriesAndSequelae_Injury_Code);
        str1 += string.Format("<Injury><Code>{0}</Code></Injury>", (object) str2);
      }
      return str1;
    }

    private string getPAFItems(IDataItem data)
    {
      string str = "";
      PAFLineItemKeys pafLineItemKeys = new PAFLineItemKeys();
      foreach (IDataItem dataItem in (IEnumerable) data.getList(LineItemType.GS23PAFLineItem))
      {
        object[] objArray = new object[4]
        {
          (object) dataItem.getEncodedValue(pafLineItemKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code),
          (object) dataItem.getEncodedValue(pafLineItemKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute),
          (object) dataItem.getEncodedValue(pafLineItemKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee),
          (object) dataItem.getEncodedValue(pafLineItemKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey)
        };
        str += string.Format("<Item><Code>{0}</Code><Attribute>{1}</Attribute><EstimatedFee>{2}</EstimatedFee><PMSGSKey>{3}</PMSGSKey></Item>", objArray);
      }
      return str;
    }

    private string getOtherInsuranceCoverage(IDataItem data)
    {
      data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage);
      return this.getMOH(data) + this.getOtherInsurer1(data) + this.getOtherInsurer2(data);
    }

    private string getMOH(IDataItem data)
    {
      return string.Format("<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.1.1.1\" {0}/><code code=\"PUBLICPOL\"/><beneficiary><coveredPartyAsPatient><code code=\"NI\"/><coveredPartyAsPatientPerson><name><given>{1}</given><given>{2}</given><family>{3}</family></name><birthTime value=\"{4}\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><author><carrierRole><id root=\"2.16.840.1.113883.3.30.1.1.1\"/></carrierRole></author></policyOrAccount></coverage>", (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_MOHAvailable)), (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_FirstName), (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_MiddleName), (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_LastName), (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_DateOfBirth));
    }

    private string getOtherInsurer1(IDataItem data)
    {
      return this.getOtherInsurer(new object[8]
      {
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID)),
        (object) this.getExtensionValue("assigningAuthorityName", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name)),
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber)),
        (object) data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName),
        (object) data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName),
        (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_FirstName),
        (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_LastName),
        (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_MiddleName)
      });
    }

    private string getOtherInsurer2(IDataItem data)
    {
      return this.getOtherInsurer(new object[8]
      {
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID)),
        (object) this.getExtensionValue("assigningAuthorityName", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name)),
        (object) this.getExtensionValue("extension", data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber)),
        (object) data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName),
        (object) data.getEncodedValue(this.m_ocf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName),
        (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_FirstName),
        (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_LastName),
        (object) data.getEncodedValue(this.m_ocf23Keys.Applicant_Name_MiddleName)
      });
    }

    private string getOtherInsurer(object[] args)
    {
      return string.Format("<coverage><policyOrAccount><id root=\"2.16.840.1.113883.3.30.1.1.2\" {2}/><code code=\"EHCPOL\"/><beneficiary><coveredPartyAsPatient><id root=\"2.16.840.1.113883.3.30.1.1.2\" {0} /><code nullFlavor=\"NI\"/><coveredPartyAsPatientPerson><name><given>{5}</given><given>{7}</given><family>{6}</family></name><birthTime nullFlavor=\"NI\"/></coveredPartyAsPatientPerson></coveredPartyAsPatient></beneficiary><holder><policyHolder><policyHolderPerson><name><given>{3}</given><family>{4}</family></name><birthTime nullFlavor=\"NASK\"/></policyHolderPerson></policyHolder></holder><author><carrierRole><id root=\"2.16.840.1.113883.3.30.1.1.2\" {1} /></carrierRole></author></policyOrAccount></coverage>", args);
    }

    private string getExtensionValue(string nodename, string value)
    {
      if (string.IsNullOrEmpty(value))
        return "";
      return nodename + "=\"" + value + "\"";
    }

    private string GetEmptyComponent()
    {
      return "<component><invoiceElementDetail><id></id><code></code><unitQuantity><numerator></numerator><denominator></denominator></unitQuantity><unitPriceAmt><numerator></numerator><denominator></denominator></unitPriceAmt><netAmt></netAmt></invoiceElementDetail></component>";
    }
  }
}
