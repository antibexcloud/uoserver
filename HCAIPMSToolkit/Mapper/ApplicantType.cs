﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.ApplicantType
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Xml.Serialization;

namespace PMSToolkit.Mapper
{
  [XmlType(Namespace = "http://hcai.ca")]
  public class ApplicantType
  {
    public ApplicantTypeName Name { get; set; }

    public string DateOfBirth { get; set; }

    public string Gender { get; set; }

    public string Telephone { get; set; }

    public string Extension { get; set; }

    public AddressType Address { get; set; }
  }
}
