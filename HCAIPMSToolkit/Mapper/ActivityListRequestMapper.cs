﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.ActivityListRequestMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;

namespace PMSToolkit.Mapper
{
  internal class ActivityListRequestMapper : BaseMapper
  {
    private const string m_xmlTemplate = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><QUCR_IN820101UV01 xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 QUCR_IN820101UV01.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.2\" /><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30\" extension=\"QUCR_IN820101UV01\"/><processingCode code=\"P\"/><processingModeCode code=\"T\"/><acceptAckCode code=\"AL\"/><receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/></device></receiver><sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/></device></sender><controlActProcess moodCode=\"EVN\"><queryByParameter><statusCode code=\"new\"/><parameterList><id nullFlavor=\"NI\"/><adjudResultsGroup.Author.Time><value><low value=\"{1}\"/><high value=\"{2}\"/></value></adjudResultsGroup.Author.Time></parameterList></queryByParameter></controlActProcess></QUCR_IN820101UV01>";

    public override IDataItem transform(IDataItem data)
    {
      string str = string.Format("<?xml version=\"1.0\" encoding=\"UTF-8\"?><QUCR_IN820101UV01 xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 QUCR_IN820101UV01.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.2\" /><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30\" extension=\"QUCR_IN820101UV01\"/><processingCode code=\"P\"/><processingModeCode code=\"T\"/><acceptAckCode code=\"AL\"/><receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/></device></receiver><sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/></device></sender><controlActProcess moodCode=\"EVN\"><queryByParameter><statusCode code=\"new\"/><parameterList><id nullFlavor=\"NI\"/><adjudResultsGroup.Author.Time><value><low value=\"{1}\"/><high value=\"{2}\"/></value></adjudResultsGroup.Author.Time></parameterList></queryByParameter></controlActProcess></QUCR_IN820101UV01>", this.getArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      string key = "message";
      string val = str;
      responseMessage.addValue(key, val);
      return (IDataItem) responseMessage;
    }

    private object[] getArgs(IDataItem data)
    {
      ActivityListRequestKeys activityListRequestKeys = new ActivityListRequestKeys();
      return new object[3]
      {
        (object) DateTime.Now.ToString("yyyyMMddHHmmss"),
        (object) data.getValue(activityListRequestKeys.Start_Date),
        (object) data.getValue(activityListRequestKeys.End_Date)
      };
    }
  }
}
