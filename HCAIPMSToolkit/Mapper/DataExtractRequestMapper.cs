﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Mapper.DataExtractRequestMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\Temp\Restore.dll\HCAIPMSToolkit._3.18\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;

namespace PMSToolkit.Mapper
{
  internal class DataExtractRequestMapper : BaseMapper
  {
    private const string m_xmlTemplate = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><{2} xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 {2}.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.1.2\"/><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30\" extension=\"{2}\"/><processingCode code=\"P\"/><processingModeCode code=\"T\"/><acceptAckCode code=\"AL\"/><receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/></device></receiver><sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/></device></sender><controlActProcess moodCode=\"INT\"><queryByParameter><statusCode code=\"new\"/><parameterList><id root=\"2.16.840.1.113883.3.30.1.1\"/><invoiceElementGroup.Id><value root=\"2.16.840.1.113883.3.30.1.1\" extension=\"{1}\"/></invoiceElementGroup.Id></parameterList></queryByParameter></controlActProcess></{2}>";

    public override IDataItem transform(IDataItem data)
    {
      string str = string.Format("<?xml version=\"1.0\" encoding=\"UTF-8\"?><{2} xmlns=\"urn:hl7-org:v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 {2}.xsd\" ITSVersion=\"XML_1.0\"><id root=\"2.16.840.1.113883.3.30.1.1.2\"/><creationTime value=\"{0}\"/><interactionId root=\"2.16.840.1.113883.3.30\" extension=\"{2}\"/><processingCode code=\"P\"/><processingModeCode code=\"T\"/><acceptAckCode code=\"AL\"/><receiver><device><id root=\"2.16.840.1.113883.3.30.1.1\"/></device></receiver><sender><device><id root=\"2.16.840.1.113883.3.30.1.2\"/></device></sender><controlActProcess moodCode=\"INT\"><queryByParameter><statusCode code=\"new\"/><parameterList><id root=\"2.16.840.1.113883.3.30.1.1\"/><invoiceElementGroup.Id><value root=\"2.16.840.1.113883.3.30.1.1\" extension=\"{1}\"/></invoiceElementGroup.Id></parameterList></queryByParameter></controlActProcess></{2}>", this.getKeysArgs(data));
      ResponseMessage responseMessage = new ResponseMessage();
      string key = "message";
      string val = str;
      responseMessage.addValue(key, val);
      return (IDataItem) responseMessage;
    }

    private object[] getKeysArgs(IDataItem data)
    {
      DataExtractRequestKeys extractRequestKeys = new DataExtractRequestKeys();
      DocType result;
      string str;
      if (Enum.TryParse<DocType>(data.getValue(extractRequestKeys.Document_Type), out result))
      {
        switch (result)
        {
          case DocType.OCF18:
          case DocType.OCF22:
          case DocType.OCF23:
            str = "QUCR_IN340104UV02";
            break;
          default:
            str = "QUCR_IN640104UV02";
            break;
        }
      }
      else
        str = "QUCR_IN340104UV02";
      return new object[3]
      {
        (object) DateTime.Now.ToString("yyyyMMddHHmmss"),
        (object) data.getValue(extractRequestKeys.Document_Number),
        (object) str
      };
    }
  }
}
