﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitConstants
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit
{
  public class ToolkitConstants
  {
    internal const string NeCST_DateTime_Format = "yyyyMMddHHmmss";
    internal const string NeCST_Date_Format = "yyyyMMdd";
    internal const string Display_Date_Format = "yyyy-MM-dd";
    internal const string Display_DateTime_Format = "yyyy-MM-ddTHH:mm:ss";
    internal const string IBC_OID = "2.16.840.1.113883.3.30";
    internal const string ApplicationGroup_OID = "2.16.840.1.113883.3.30.1";
    internal const string HCAI_OID = "2.16.840.1.113883.3.30.1.1";
    internal const string Toolkit_OID = "2.16.840.1.113883.3.30.1.2";
    internal const string InsurerGroup_OID = "2.16.840.1.113883.3.30.2";
    internal const string FacilityGroup_OID = "2.16.840.1.113883.3.30.3";
    internal const string ClaimantPayee_OID = "2.16.840.1.113883.3.30.4";
    internal const string PAF_PreApproveServices_SupplementaryGSWAD1Code = "PW1SC";
    internal const string PAF_PreApproveServices_SupplementaryGSWAD2Code = "PW2SC";
    internal const string PAF_PreApproveServices_SupplementaryGSWAD1OR2Code = "PWWSC";
    internal const string HCAI_DEFAULT_NAMESPACE = "http://hcai.ca";
    internal const string NOT_ARCHIVED_TEXT = "Not Archived";
    internal const string NOT_ARCHIVED_VALUE = "1";
    internal const string ARCHIVED_TEXT = "Archived";
    internal const string ARCHIVED_VALUE = "2";
  }
}
