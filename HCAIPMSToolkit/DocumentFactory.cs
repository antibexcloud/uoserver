﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DocumentFactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;

namespace PMSToolkit
{
  internal class DocumentFactory
  {
    public static Document newDocument(DocType docType)
    {
      switch (docType)
      {
        case DocType.OCF18:
          return (Document) new OCF18();
        case DocType.OCF22:
          return (Document) new OCF22();
        case DocType.OCF23:
          return (Document) new OCF23();
        case DocType.OCF21B:
          return (Document) new OCF21B();
        case DocType.OCF21C:
          return (Document) new OCF21C();
        case DocType.OCF9:
          return (Document) new OCF9();
        default:
          return (Document) null;
      }
    }
  }
}
