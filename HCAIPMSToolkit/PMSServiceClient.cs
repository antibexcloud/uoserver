﻿// Decompiled with JetBrains decompiler
// Type: PMSServiceClient
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
public class PMSServiceClient : ClientBase<IService>, IService
{
  public PMSServiceClient()
  {
  }

  public PMSServiceClient(string endpointConfigurationName)
    : base(endpointConfigurationName)
  {
  }

  public PMSServiceClient(string endpointConfigurationName, string remoteAddress)
    : base(endpointConfigurationName, remoteAddress)
  {
  }

  public PMSServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress)
    : base(endpointConfigurationName, remoteAddress)
  {
  }

  public PMSServiceClient(Binding binding, EndpointAddress remoteAddress)
    : base(binding, remoteAddress)
  {
  }

  public XmlElement SendNeCSTMessage(XmlElement neCST_xml)
  {
    return this.Channel.SendNeCSTMessage(neCST_xml);
  }

  public XmlElement GetFacilityInfo()
  {
    return this.Channel.GetFacilityInfo();
  }

  public XmlElement GetInsurerList()
  {
    return this.Channel.GetInsurerList();
  }

  public XmlElement UpdateFacilityInfo(XmlElement pFacilityXml)
  {
    return this.Channel.UpdateFacilityInfo(pFacilityXml);
  }

  public XmlElement AddProviderInfo(XmlElement pProviderXml)
  {
    return this.Channel.AddProviderInfo(pProviderXml);
  }

  public XmlElement UpdateProviderInfo(XmlElement pProviderXml)
  {
    return this.Channel.UpdateProviderInfo(pProviderXml);
  }
}
