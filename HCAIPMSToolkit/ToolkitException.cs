﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitException
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;
using System.Runtime.Serialization;

namespace PMSToolkit
{
  internal class ToolkitException : ApplicationException
  {
    private ToolkitExceptionCode m_code;

    private ToolkitException()
    {
      throw new NotSupportedException();
    }

    public ToolkitException(ToolkitExceptionCode code, string message)
      : base(message)
    {
      this.m_code = code;
    }

    public ToolkitException(ToolkitExceptionCode code, string message, Exception inner)
      : base(message, inner)
    {
      this.m_code = code;
    }

    public ToolkitException(ToolkitExceptionCode code, SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      this.m_code = code;
    }

    public ToolkitExceptionCode getErrorCode()
    {
      return this.m_code;
    }
  }
}
