﻿// Decompiled with JetBrains decompiler
// Type: IBC.HCAI.SoapExceptionWrapper.FaultExceptionDetail
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace IBC.HCAI.SoapExceptionWrapper
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [DataContract(Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper")]
  public class FaultExceptionDetail : IExtensibleDataObject
  {
    private ExtensionDataObject extensionDataField;
    private string _errorCodeField;
    private string _errorMessageField;

    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember(IsRequired = true)]
    public string _errorCode
    {
      get
      {
        return this._errorCodeField;
      }
      set
      {
        this._errorCodeField = value;
      }
    }

    [DataMember(IsRequired = true)]
    public string _errorMessage
    {
      get
      {
        return this._errorMessageField;
      }
      set
      {
        this._errorMessageField = value;
      }
    }
  }
}
