﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.ErrorDescription
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.ToolkitError
{
  public class ErrorDescription
  {
    private string m_key;
    private string m_validationErrorCode;
    private string m_description;

    internal ErrorDescription(string key, string description)
    {
      this.m_key = key;
      this.m_description = description;
    }

    internal ErrorDescription(string key, string description, string validationMsgCode)
    {
      this.m_key = key;
      this.m_description = description;
      this.m_validationErrorCode = validationMsgCode;
    }

    public string getKey()
    {
      return this.m_key;
    }

    public string getDescription()
    {
      return this.m_description;
    }

    public string getValidationMessageErrorCode()
    {
      return this.m_validationErrorCode;
    }
  }
}
