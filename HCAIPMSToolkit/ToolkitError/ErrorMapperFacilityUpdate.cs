﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.ErrorMapperFacilityUpdate
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Xml;

namespace PMSToolkit.ToolkitError
{
  internal class ErrorMapperFacilityUpdate : IErrorMapper
  {
    public IError transform(IDataItem data, string documentType)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      int count = xmlDocument.SelectNodes("Response/Errors/Error", nsmgr).Count;
      Error error = new Error();
      string innerText1;
      try
      {
        innerText1 = xmlDocument.SelectSingleNode("Response/Errors/ErrorType", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The \"Error Type\" field in the XML response was not found where expected.", ex);
      }
      error.setErrorType(innerText1);
      if (innerText1 == "103")
      {
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          string innerText2;
          string innerText3;
          try
          {
            innerText2 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Location"), nsmgr).InnerText;
            innerText3 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Text"), nsmgr).InnerText;
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
          error.addError(innerText2, innerText3);
          code = 1;
          error.setErrorDescription(innerText3);
        }
        error.setValidationErrorCode(code);
      }
      else if (innerText1 == "108")
      {
        string key;
        string innerText2;
        try
        {
          key = "Authorization Error";
          innerText2 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(1, "Text"), nsmgr).InnerText;
        }
        catch (Exception ex)
        {
          throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
        }
        error.addError(key, innerText2);
        int code = 1;
        error.setErrorDescription(innerText2);
        error.setValidationErrorCode(code);
      }
      else if (innerText1 == "109")
      {
        string key;
        string innerText2;
        try
        {
          key = "Authorization Error";
          innerText2 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(1, "Text"), nsmgr).InnerText;
        }
        catch (Exception ex)
        {
          throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
        }
        error.addError(key, innerText2);
        int code = 1;
        error.setErrorDescription(innerText2);
        error.setValidationErrorCode(code);
      }
      else if (innerText1 == "115")
      {
        string s = string.Empty;
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          try
          {
            s = s + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Text"), nsmgr).InnerText + "\r\n";
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
        }
        error.setValidationErrorCode(code);
        error.setErrorDescription(s);
      }
      else if (innerText1 == "116")
      {
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          string innerText2;
          string innerText3;
          try
          {
            innerText2 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Location"), nsmgr).InnerText;
            innerText3 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Text"), nsmgr).InnerText;
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
          error.addError(innerText2, innerText3);
          code = 1;
          error.setErrorDescription(innerText3);
        }
        error.setValidationErrorCode(code);
      }
      else
        error.setValidationErrorCode(0);
      return (IError) error;
    }

    private string GetErrorNodeValue(int index, string nodeName)
    {
      return string.Format("Response/Errors/Error[{0}]/" + nodeName, (object) index);
    }
  }
}
