﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.ErrorMapperAACNSubmit
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Xml;

namespace PMSToolkit.ToolkitError
{
  internal class ErrorMapperAACNSubmit : IErrorMapper
  {
    public IError transform(IDataItem data, string documentType)
    {
      AACNSubmitErrorResponseKeys errorResponseKeys = new AACNSubmitErrorResponseKeys();
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      ErrorMapper errorMapper1 = new ErrorMapper();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      nsmgr.AddNamespace("x", "http://hcai.ca");
      string val = string.Empty;
      int count = xmlDocument.SelectNodes("/x:AACNResponse/x:Error", nsmgr).Count;
      string innerText1 = xmlDocument.SelectSingleNode("/x:AACNResponse/x:AACN/x:PMSFields/x:PMSDocumentKey", nsmgr).InnerText;
      string innerText2 = xmlDocument.SelectSingleNode("/x:AACNResponse/x:AACN/x:PMSFields/x:PMSPatientKey", nsmgr).InnerText;
      if (xmlDocument.SelectSingleNode("/x:AACNResponse/x:XPath", nsmgr) != null)
        val = xmlDocument.SelectSingleNode("/x:AACNResponse/x:XPath", nsmgr).InnerText;
      Error error = new Error();
      error.addValue(errorResponseKeys.PMSFields_PMSDocumentKey, innerText1);
      error.addValue(errorResponseKeys.PMSFields_PMSPatientKey, innerText2);
      error.addValue(errorResponseKeys.XPath, val);
      string innerText3;
      try
      {
        innerText3 = xmlDocument.SelectSingleNode("/x:AACNResponse/x:PMSErrorCode", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The \"Error Type\" field in the XML response was not found where expected.", ex);
      }
      error.setErrorType(innerText3);
      string s = string.Empty;
      if (xmlDocument.SelectSingleNode("/x:AACNResponse/x:Message", nsmgr) != null)
        s = xmlDocument.SelectSingleNode("/x:AACNResponse/x:Message", nsmgr).InnerText;
      error.setErrorDescription(s);
      if (innerText3 == "103")
      {
        int code = 0;
        Hashtable errorMapper2 = NeCSTXpathErrorMapper.GetErrorMapper(documentType);
        for (int index = 1; index <= count; ++index)
        {
          string innerText4;
          string innerText5;
          string innerText6;
          try
          {
            innerText4 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Code"), nsmgr).InnerText;
            innerText5 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "XPath"), nsmgr).InnerText;
            innerText6 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "Message"), nsmgr).InnerText;
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
          bool isLineItemError = innerText5.IndexOf("[") != -1;
          string pmsKey = this.MapNeCSTXpathsToPMSKey(innerText5.Replace("/x:*", documentType).Replace("/x:", "/"), errorMapper2, isLineItemError);
          if (isLineItemError)
          {
            BaseLineItemError lineItemError = LineItemErrorFactory.newLineItemError(innerText5);
            lineItemError.setKey = pmsKey;
            lineItemError.setDescription = innerText6;
            lineItemError.setValidationMessageErrorCode = innerText4;
            error.addLineItemError(lineItemError);
            code |= errorMapper1.GetValidationErrorValue(lineItemError.getLineItemType());
          }
          else
          {
            error.addError(pmsKey, innerText6, innerText4);
            code |= errorMapper1.GetValidationErrorValue(LineItemType.Document);
          }
        }
        error.setValidationErrorCode(code);
      }
      else
        error.setValidationErrorCode(0);
      return (IError) error;
    }

    private string GetErrorNodeValue(int index, string nodeName)
    {
      return string.Format("/x:AACNResponse/x:Error[{0}]/x:" + nodeName, (object) index);
    }

    private string MapNeCSTXpathsToPMSKey(string ocfXpath, Hashtable ocfXpathToPMSKeyMapper, bool isLineItemError)
    {
      string str = ocfXpath;
      if (isLineItemError)
        str = new Regex("\\[(.*?)]").Replace(ocfXpath, "");
      if (ocfXpathToPMSKeyMapper.ContainsKey((object) str))
        return (string) ocfXpathToPMSKeyMapper[(object) str];
      return string.Empty;
    }
  }
}
