﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.Error
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.ToolkitError
{
  internal class Error : IError
  {
    public Hashtable m_data = new Hashtable();
    private string m_errorType = "";
    private IList m_errors = (IList) new ArrayList();
    private IDictionary m_lineItemErrors = (IDictionary) new Hashtable();
    private string m_errorDescription = "";
    private int m_validationErrorCode;

    public string getErrorType()
    {
      return this.m_errorType;
    }

    public int getValidationErrorCode()
    {
      return this.m_validationErrorCode;
    }

    public IList getErrorList()
    {
      return this.m_errors;
    }

    public IList getLineItemErrorList(LineItemType lineItemType)
    {
      string str = lineItemType.ToString();
      IList list = (IList) this.m_lineItemErrors[(object) str];
      if (list == null)
      {
        list = (IList) new ArrayList();
        this.m_lineItemErrors[(object) str] = (object) list;
      }
      return list;
    }

    public string getErrorDescription()
    {
      return this.m_errorDescription;
    }

    internal void setErrorType(string errorType)
    {
      this.m_errorType = errorType;
    }

    internal void setValidationErrorCode(int code)
    {
      this.m_validationErrorCode = code;
    }

    internal void addError(string key, string description)
    {
      this.m_errors.Add((object) new ErrorDescription(key, description));
    }

    internal void addError(string key, string description, string validationMsgCode)
    {
      this.m_errors.Add((object) new ErrorDescription(key, description, validationMsgCode));
    }

    internal bool addLineItemError(BaseLineItemError lineItemError)
    {
      this.getLineItemErrorList(lineItemError.getLineItemType()).Add((object) lineItemError);
      return true;
    }

    internal BaseLineItemError newLineItemError(string xpath)
    {
      return LineItemErrorFactory.newLineItemError(xpath);
    }

    internal void setErrorDescription(string s)
    {
      this.m_errorDescription = s;
    }

    public string getValue(string key)
    {
      return (string) this.m_data[(object) key] ?? string.Empty;
    }

    public bool addValue(string key, string val)
    {
      try
      {
        if (this.m_data.ContainsKey((object) key))
          this.m_data.Remove((object) key);
        if (val != null)
          this.m_data.Add((object) key, (object) val);
        return true;
      }
      catch
      {
        return false;
      }
    }
  }
}
