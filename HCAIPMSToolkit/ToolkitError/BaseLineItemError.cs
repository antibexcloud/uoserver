﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.BaseLineItemError
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.ToolkitError
{
  internal abstract class BaseLineItemError : ILineItemError
  {
    private int m_lineItemIndex;
    private string m_key;
    private string m_description;
    private int m_sessionHeaderIndex;
    private string m_validationErrorCode;

    internal int setHeaderIndex
    {
      set
      {
        this.m_sessionHeaderIndex = value;
      }
    }

    internal int setIndex
    {
      set
      {
        this.m_lineItemIndex = value;
      }
    }

    internal string setKey
    {
      set
      {
        this.m_key = value;
      }
    }

    internal string setDescription
    {
      set
      {
        this.m_description = value;
      }
    }

    public int getSessionHeaderIndex()
    {
      return this.m_sessionHeaderIndex;
    }

    public int getLineItemIndex()
    {
      return this.m_lineItemIndex;
    }

    public string getKey()
    {
      return this.m_key;
    }

    public string getDescription()
    {
      return this.m_description;
    }

    internal string setValidationMessageErrorCode
    {
      set
      {
        this.m_validationErrorCode = value;
      }
    }

    public string getValidationMessageErrorCode()
    {
      return this.m_validationErrorCode;
    }

    public abstract LineItemType getLineItemType();
  }
}
