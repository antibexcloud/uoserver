﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.NeCSTXpathErrorMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using System;
using System.Collections;
using System.Configuration;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;

namespace PMSToolkit.ToolkitError
{
  internal class NeCSTXpathErrorMapper
  {
    private static Hashtable ocf18OCFXpathToPMSKeyMapping = new Hashtable();
    private static Hashtable ocf21BOCFXpathToPMSKeyMapping = new Hashtable();
    private static Hashtable ocf21COCFXpathToPMSKeyMapping = new Hashtable();
    private static Hashtable ocf22OCFXpathToPMSKeyMapping = new Hashtable();
    private static Hashtable ocf23OCFXpathToPMSKeyMapping = new Hashtable();
    private static Hashtable ocfAACNXpathToPMSKeyMapping = new Hashtable();

    static NeCSTXpathErrorMapper()
    {
      NeCSTXpathErrorMapper.PopulateOCFXpathToPMSKeyMapping(NeCSTXpathErrorMapper.ocf18OCFXpathToPMSKeyMapping, "OCF18");
      NeCSTXpathErrorMapper.PopulateOCFXpathToPMSKeyMapping(NeCSTXpathErrorMapper.ocf21BOCFXpathToPMSKeyMapping, "OCF21B");
      NeCSTXpathErrorMapper.PopulateOCFXpathToPMSKeyMapping(NeCSTXpathErrorMapper.ocf21COCFXpathToPMSKeyMapping, "OCF21C");
      NeCSTXpathErrorMapper.PopulateOCFXpathToPMSKeyMapping(NeCSTXpathErrorMapper.ocf22OCFXpathToPMSKeyMapping, "OCF22");
      NeCSTXpathErrorMapper.PopulateOCFXpathToPMSKeyMapping(NeCSTXpathErrorMapper.ocf23OCFXpathToPMSKeyMapping, "OCF23");
      NeCSTXpathErrorMapper.PopulateOCFXpathToPMSKeyMapping(NeCSTXpathErrorMapper.ocfAACNXpathToPMSKeyMapping, "AACN");
    }

    internal static Hashtable GetErrorMapper(string docType)
    {
      if (docType.Equals("OCF18"))
        return NeCSTXpathErrorMapper.ocf18OCFXpathToPMSKeyMapping;
      if (docType.Equals("OCF21C"))
        return NeCSTXpathErrorMapper.ocf21COCFXpathToPMSKeyMapping;
      if (docType.Equals("OCF21B"))
        return NeCSTXpathErrorMapper.ocf21BOCFXpathToPMSKeyMapping;
      if (docType.Equals("OCF22"))
        return NeCSTXpathErrorMapper.ocf22OCFXpathToPMSKeyMapping;
      if (docType.Equals("OCF23"))
        return NeCSTXpathErrorMapper.ocf23OCFXpathToPMSKeyMapping;
      if (docType.Equals("AACN"))
        return NeCSTXpathErrorMapper.ocfAACNXpathToPMSKeyMapping;
      return (Hashtable) null;
    }

    internal static string MapNeCSTXpathsToPMSKey(string ocfXpath, Hashtable ocfXpathToPMSKeyMapper, bool isLineItemError, string NeCSTXpath)
    {
      string str = ocfXpath;
      if (isLineItemError)
        str = new Regex("\\[\\d+]").Replace(ocfXpath, "");
      if (ocfXpathToPMSKeyMapper.ContainsKey((object) str))
        return (string) ocfXpathToPMSKeyMapper[(object) str];
      return NeCSTXpath;
    }

    //private static void PopulateOCFXpathToPMSKeyMapping(Hashtable hashTable, string OCFType)
    //{
    //  string filename = string.Format(ConfigurationManager.AppSettings["PMSkeysMappingXMLPath"], (object) OCFType);
    //  XmlDocument xmlDocument = new XmlDocument();
    //  xmlDocument.Load(filename);
    //  foreach (XmlNode selectNode in xmlDocument.SelectNodes("/OCFXPATHPMSKEYMAPPING/OCFXPATHPMSKEY"))
    //  {
    //    XmlNode childNode1 = selectNode.ChildNodes[0];
    //    XmlNode childNode2 = selectNode.ChildNodes[1];
    //    string innerText1 = childNode1.InnerText;
    //    string innerText2 = childNode2.InnerText;
    //    hashTable.Add((object) innerText1, (object) innerText2);
    //  }
    //}
      private static void PopulateOCFXpathToPMSKeyMapping(Hashtable hashTable, string OCFType)
        {
            try
            {
                //string filename = string.Format(ConfigurationManager.AppSettings["PMSkeysMappingXMLPath"], (object) OCFType);
                XmlDocument xmlDocument = new XmlDocument();
                switch (OCFType.ToUpper())
                {
                    case "AACN":
                        //xmlDocument.LoadXml(Properties);
                        xmlDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("PMSToolkit.Xml.AACNPMSkeysMapping.xml"));
                        break;
                    case "OCF18":
                        xmlDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("PMSToolkit.Xml.OCF18PMSkeysMapping.xml"));
                        break;
                    case "OCF21B":
                        xmlDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("PMSToolkit.Xml.OCF21BPMSkeysMapping.xml"));
                        break;
                    case "OCF21C":
                        xmlDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("PMSToolkit.Xml.OCF21CPMSkeysMapping.xml"));
                        break;
                    case "OCF22":
                        xmlDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("PMSToolkit.Xml.OCF22PMSkeysMapping.xml"));
                        break;
                    case "OCF23":
                        xmlDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("PMSToolkit.Xml.OCF23PMSkeysMapping.xml"));
                        break;

                }
                //xmlDocument.Load(filename);
                foreach (XmlNode selectNode in xmlDocument.SelectNodes("/OCFXPATHPMSKEYMAPPING/OCFXPATHPMSKEY"))
                {
                    XmlNode childNode1 = selectNode.ChildNodes[0];
                    XmlNode childNode2 = selectNode.ChildNodes[1];
                    string innerText1 = childNode1.InnerText;
                    string innerText2 = childNode2.InnerText;
                    hashTable.Add((object)innerText1, (object)innerText2);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception($" PMSToolkit.ToolkitError.PopulateOCFXpathToPMSKeyMapping(hashTable, {OCFType})\r\n", e);
            }
        }
  }
}
