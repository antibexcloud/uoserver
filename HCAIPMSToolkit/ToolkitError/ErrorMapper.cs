﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.ToolkitError.ErrorMapper
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System;
using System.Collections;
using System.Xml;

namespace PMSToolkit.ToolkitError
{
  internal class ErrorMapper : IErrorMapper
  {
    public IError transform(IDataItem data, string documentType)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      nsmgr.AddNamespace("hl7", "urn:hl7-org:v3");
      int count = xmlDocument.SelectNodes("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail", nsmgr).Count;
      Error error = new Error();
      string innerText1;
      try
      {
        innerText1 = xmlDocument.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:targetMessage/hl7:id/@extension", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The \"Error Type\" field in the XML response was not found where expected.", ex);
      }
      error.setErrorType(innerText1);
      if (innerText1 == "103")
      {
        int code = 0;
        Hashtable errorMapper = NeCSTXpathErrorMapper.GetErrorMapper(documentType);
        for (int index = 1; index <= count; ++index)
        {
          string innerText2;
          string innerText3;
          string innerText4;
          try
          {
            innerText2 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "location[1]"), nsmgr).InnerText;
            innerText3 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "location[2]"), nsmgr).InnerText;
            innerText4 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "text"), nsmgr).InnerText;
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
          bool isLineItemError = innerText2.IndexOf("[") != -1;
          string pmsKey = NeCSTXpathErrorMapper.MapNeCSTXpathsToPMSKey(innerText2.Replace("/x:*", documentType).Replace("/x:", "/"), errorMapper, isLineItemError, innerText3);
          if (isLineItemError)
          {
            BaseLineItemError lineItemError = LineItemErrorFactory.newLineItemError(innerText2);
            lineItemError.setKey = pmsKey;
            lineItemError.setDescription = innerText4 + " : The NeCST xpath is : " + innerText3;
            error.addLineItemError(lineItemError);
            code |= this.GetValidationErrorValue(lineItemError.getLineItemType());
          }
          else
          {
            error.addError(pmsKey, innerText4);
            code |= this.GetValidationErrorValue(LineItemType.Document);
          }
        }
        error.setValidationErrorCode(code);
      }
      else if (innerText1 == "115")
      {
        string s = string.Empty;
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          try
          {
            s = s + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "text"), nsmgr).InnerText + "\r\n";
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
        }
        error.setValidationErrorCode(code);
        error.setErrorDescription(s);
      }
      else if (innerText1 == "116")
      {
        string s = string.Empty;
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          try
          {
            s = s + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "text"), nsmgr).InnerText + " Location: " + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "location"), nsmgr).InnerText + "\r\n";
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
        }
        error.setErrorDescription(s);
        error.setValidationErrorCode(code);
      }
      else
        error.setValidationErrorCode(0);
      return (IError) error;
    }

    public IError transformAACNError(IDataItem data, string documentType)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = data.getValue(pmsResponseKeys.PMSResponseMessageKey);
      xmlDocument.LoadXml(xml);
      int count = xmlDocument.SelectNodes("/AACNResponse/Error").Count;
      Error error = new Error();
      string innerText1;
      try
      {
        innerText1 = xmlDocument.SelectSingleNode("/AACNResponse/PMSErrorCode").InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The \"Error Type\" field in the XML response was not found where expected.", ex);
      }
      error.setErrorType(innerText1);
      if (innerText1 == "103")
      {
        int code = 0;
        Hashtable errorMapper = NeCSTXpathErrorMapper.GetErrorMapper(documentType);
        for (int index = 1; index <= count; ++index)
        {
          string innerText2;
          string innerText3;
          string innerText4;
          try
          {
            innerText2 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "location[1]")).InnerText;
            innerText3 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "location[2]")).InnerText;
            innerText4 = xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "text")).InnerText;
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
          bool isLineItemError = innerText2.IndexOf("[") != -1;
          string pmsKey = NeCSTXpathErrorMapper.MapNeCSTXpathsToPMSKey(innerText2.Replace("/x:*", documentType).Replace("/x:", "/"), errorMapper, isLineItemError, innerText3);
          if (isLineItemError)
          {
            BaseLineItemError lineItemError = LineItemErrorFactory.newLineItemError(innerText2);
            lineItemError.setKey = pmsKey;
            lineItemError.setDescription = innerText4 + " : The NeCST xpath is : " + innerText3;
            error.addLineItemError(lineItemError);
            code |= this.GetValidationErrorValue(lineItemError.getLineItemType());
          }
          else
          {
            error.addError(pmsKey, innerText4);
            code |= this.GetValidationErrorValue(LineItemType.Document);
          }
        }
        error.setValidationErrorCode(code);
      }
      else if (innerText1 == "115")
      {
        string s = string.Empty;
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          try
          {
            s = s + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "text")).InnerText + "\r\n";
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
        }
        error.setValidationErrorCode(code);
        error.setErrorDescription(s);
      }
      else if (innerText1 == "116")
      {
        string s = string.Empty;
        int code = 0;
        for (int index = 1; index <= count; ++index)
        {
          try
          {
            s = s + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "text")).InnerText + " Location: " + xmlDocument.SelectSingleNode(this.GetErrorNodeValue(index, "location")).InnerText + "\r\n";
          }
          catch (Exception ex)
          {
            throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Information in the error object was not found where expected.", ex);
          }
        }
        error.setErrorDescription(s);
        error.setValidationErrorCode(code);
      }
      else
        error.setValidationErrorCode(0);
      return (IError) error;
    }

    private string GetErrorNodeValue(int index, string nodeName)
    {
      return string.Format("/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail[{0}]/hl7:" + nodeName, (object) index);
    }

    public int GetValidationErrorValue(LineItemType lineItemType)
    {
      switch (lineItemType)
      {
        case LineItemType.Document:
          return 1;
        case LineItemType.GS18LineItem:
          return 4;
        case LineItemType.GS18SessionHeaderLineItem:
          return 8;
        case LineItemType.GS18SessionLineItem:
          return 16;
        case LineItemType.GS21BLineItem:
          return 32;
        case LineItemType.GS21CRenderedGSLineItem:
          return 64;
        case LineItemType.GS21CPAFReimbursableLineItem:
          return 128;
        case LineItemType.GS21COtherReimbursableLineItem:
          return 256;
        case LineItemType.GS22LineItem:
          return 512;
        case LineItemType.GS23PAFLineItem:
          return 1028;
        case LineItemType.GS23OtherGSLineItem:
          return 2048;
        case LineItemType.InjuryLineItem:
          return 2;
        case LineItemType.AACNServicesLineItem:
          return 4096;
        case LineItemType.AACNAssessedPart:
          return 8192;
        default:
          return 0;
      }
    }
  }
}
