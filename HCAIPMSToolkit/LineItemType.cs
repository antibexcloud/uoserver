﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.LineItemType
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit
{
  public enum LineItemType
  {
    Document,
    GS18LineItem,
    GS18SessionHeaderLineItem,
    GS18NonSessionLineItem,
    GS18SessionLineItem,
    GS21BLineItem,
    GS21CRenderedGSLineItem,
    GS21CPAFReimbursableLineItem,
    GS21COtherReimbursableLineItem,
    GS22LineItem,
    GS23PAFLineItem,
    GS23OtherGSLineItem,
    GS9LineItem,
    GS9InterestLineItem,
    DE_GS18LineItem_Estimated,
    DE_GS18SessionHeaderLineItem_Estimated,
    DE_GS18SessionLineItem_Estimated,
    DE_GS18LineItem_Approved,
    DE_GS18SessionHeaderLineItem_Approved,
    DE_GS21BLineItem_Estimated,
    DE_GS21BLineItem_Approved,
    DE_GS21CRenderedGSLineItem,
    DE_GS21CPAFReimbursableLineItem_Estimated,
    DE_GS21COtherReimbursableLineItem_Estimated,
    DE_GS21CPAFReimbursableLineItem_Approved,
    DE_GS21COtherReimbursableLineItem_Approved,
    DE_GS22LineItem_Estimated,
    DE_GS22LineItem_Approved,
    DE_GS23PAFLineItem,
    DE_GS23OtherGSLineItem_Estimated,
    DE_GS23OtherGSLineItem_Approved,
    InjuryLineItem,
    ActivityLineItem,
    ProviderLineItem,
    RegistrationLineItem,
    InsurerLineItem,
    BranchLineItem,
    FacilityLineItem,
    AcknowledgementDetailLineItem,
    ProviderProfessionLineItem,
    PAFSecondaryProviderProfessionLineItem,
    FacilityLicenseLineItem,
    AACNPartHeader,
    AACNServicesLineItem,
    AACNAssessedPart,
    AACNApprovedPart,
  }
}
