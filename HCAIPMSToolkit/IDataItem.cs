﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.IDataItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit
{
  public interface IDataItem
  {
    bool addValue(string key, string val);

    bool addLineItem(IDataItem lineItem);

    IDataItem newLineItem(LineItemType lineItemType);

    string getValue(string key);

    string getEncodedValue(string key);

    IList getList(LineItemType lineItemType);

    IList getList(string key);

    bool isValidLineItemType(LineItemType lineItemType);
  }
}
