﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.LineItemFactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;

namespace PMSToolkit
{
  internal class LineItemFactory
  {
    public static IDataItem newLineItem(LineItemType lineItemType)
    {
      switch (lineItemType)
      {
        case LineItemType.Document:
          throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "You can't create a line item of type \"Document\".");
        case LineItemType.GS18LineItem:
          return (IDataItem) new GS18LineItem();
        case LineItemType.GS18SessionHeaderLineItem:
          return (IDataItem) new SessionHeaderLineItem();
        case LineItemType.GS18SessionLineItem:
          return (IDataItem) new SessionLineItem();
        case LineItemType.GS21BLineItem:
          return (IDataItem) new GS21BLineItem();
        case LineItemType.GS21CRenderedGSLineItem:
          return (IDataItem) new RenderedGSLineItem();
        case LineItemType.GS21CPAFReimbursableLineItem:
          return (IDataItem) new PAFReimbursableLineItem();
        case LineItemType.GS21COtherReimbursableLineItem:
          return (IDataItem) new OtherReimbursableLineItem();
        case LineItemType.GS22LineItem:
          return (IDataItem) new GS22LineItem();
        case LineItemType.GS23PAFLineItem:
          return (IDataItem) new PAFLineItem();
        case LineItemType.GS23OtherGSLineItem:
          return (IDataItem) new OtherGSLineItem();
        case LineItemType.GS9LineItem:
          return (IDataItem) new GS9LineItem();
        case LineItemType.GS9InterestLineItem:
          return (IDataItem) new GS9InterestLineItem();
        case LineItemType.InjuryLineItem:
          return (IDataItem) new InjuryLineItem();
        case LineItemType.ActivityLineItem:
          return (IDataItem) new ActivityLineItem();
        case LineItemType.ProviderLineItem:
          return (IDataItem) new ProviderLineItem();
        case LineItemType.RegistrationLineItem:
          return (IDataItem) new RegistrationLineItem();
        case LineItemType.InsurerLineItem:
          return (IDataItem) new InsurerLineItem();
        case LineItemType.BranchLineItem:
          return (IDataItem) new BranchLineItem();
        case LineItemType.AcknowledgementDetailLineItem:
          return (IDataItem) new AcknowledgementDetailLineItem();
        case LineItemType.ProviderProfessionLineItem:
          return (IDataItem) new ProviderProfessionLineItem();
        case LineItemType.PAFSecondaryProviderProfessionLineItem:
          return (IDataItem) new GS21CPAFSecondaryProviderProfessionLineItem();
        case LineItemType.AACNServicesLineItem:
          return (IDataItem) new AACNServicesLineItem();
        case LineItemType.AACNAssessedPart:
          return (IDataItem) new AACNAssessedPartLineItem();
        default:
          return (IDataItem) null;
      }
    }
  }
}
