﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Toolkit
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.Action;
using PMSToolkit.DataObjects;
using PMSToolkit.ToolkitError;
using System;
using System.Xml;

namespace PMSToolkit
{
  public class Toolkit
  {
    private IError m_error;

    public IError error
    {
      get
      {
        if (this.m_error == null)
          this.ResetError();
        return this.m_error;
      }
    }

    private void ResetError()
    {
      this.m_error = (IError) new Error();
    }

    public IDataItem createDoc(DocType docType)
    {
      return docType != DocType.AACN ? (IDataItem) DocumentFactory.newDocument(docType) : AACNFactory.newAACN(docType);
    }

    public IDataItem updateFacility()
    {
      return (IDataItem) UserFactory.UpdateFaciliy();
    }

    public IDataItem submit(string username, string password, IDataItem document)
    {
      if (DocType.AACN.ToString() == document.getValue("HCAI_Document_Type"))
        return this.ExecuteAACNAction(ActionType.SubmitAACN, username, password, document);
      return this.ExecuteAction(ActionType.SubmitDocument, username, password, document);
    }

    public IDataItem submitProviderAction(string username, string password, IDataItem document, PMSActionType action)
    {
      if (action == PMSActionType.AddProvider)
        return this.ExecuteAction(ActionType.AddProvider, username, password, document);
      if (action == PMSActionType.UpdateProvider)
        return this.ExecuteAction(ActionType.UpdateProvider, username, password, document);
      return (IDataItem) null;
    }

    public IDataItem submitFacilityUpdate(string username, string password, IDataItem document)
    {
      return this.ExecuteAction(ActionType.UpdateFacility, username, password, document);
    }

    public IDataItem activityList(string username, string password, DateTime startDate, DateTime endDate)
    {
      IDataItem inputData = (IDataItem) new ActivityListRequest();
      ActivityListRequestKeys activityListRequestKeys = new ActivityListRequestKeys();
      inputData.addValue(activityListRequestKeys.Start_Date, startDate.ToString("yyyyMMdd"));
      inputData.addValue(activityListRequestKeys.End_Date, endDate.ToString("yyyyMMdd"));
      return this.ExecuteAction(ActionType.GetActivityList, username, password, inputData);
    }

    public IDataItem adjusterResponse(string username, string password)
    {
      AdjusterResponseRequest adjusterResponseRequest = new AdjusterResponseRequest();
      return this.ExecuteAction(ActionType.GetAdjusterResponse, username, password, (IDataItem) adjusterResponseRequest);
    }

    public IDataItem dataExtract(string username, string password, string documentNumber, DocType docType)
    {
      IDataItem inputData = (IDataItem) new DataExtractRequest();
      DataExtractRequestKeys extractRequestKeys = new DataExtractRequestKeys();
      inputData.addValue(extractRequestKeys.Document_Number, ConvertUtil.encode(documentNumber));
      inputData.addValue(extractRequestKeys.Document_Type, docType.ToString());
      return this.ExecuteAction(ActionType.GetDataExtract, username, password, inputData);
    }

    public IDataItem dataExtract(string username, string password, string documentNumber)
    {
      IDataItem inputData = (IDataItem) new DataExtractRequest();
      DataExtractRequestKeys extractRequestKeys = new DataExtractRequestKeys();
      inputData.addValue(extractRequestKeys.Document_Number, ConvertUtil.encode(documentNumber));
      return this.ExecuteAction(ActionType.GetDataExtract, username, password, inputData);
    }

    public IDataItem facilityInfo(string username, string password)
    {
      return this.ExecuteAction(ActionType.GetFacilityInfo, username, password, (IDataItem) null);
    }

    public IDataItem insurerList(string username, string password)
    {
      return this.ExecuteAction(ActionType.GetInsurerList, username, password, (IDataItem) null);
    }

    public IError getError()
    {
      return this.m_error;
    }

    private IDataItem ExecuteAction(ActionType actionType, string username, string password, IDataItem inputData)
    {
      IAction action = ActionFactory.newAction(actionType);
      try
      {
        action.validate(username, password);
        username = username.Trim();
        password = password.Trim();
        IDataItem requestData = action.transformRequest(inputData);
        IDataItem responseData = action.sendMessage(username, password, requestData);
        PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
        string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(xml);
        xmlDocument.SelectSingleNode("/");
        if (action.isResponseSuccess(responseData))
          return action.transformResponse(responseData);
        string documentType = (string) null;
        if (actionType == ActionType.SubmitDocument)
          documentType = inputData.getValue(new DocumentKeys().HCAI_Document_Type);
        this.m_error = action.processError(responseData, documentType);
        return (IDataItem) null;
      }
      catch (ToolkitException ex)
      {
        Error error = (Error) this.error;
        ToolkitExceptionCode errorCode = ex.getErrorCode();
        if (errorCode != ToolkitExceptionCode.nHCAISystemError)
        {
          switch ((long) (errorCode - 10L))
          {
            case 0:
              error.setErrorType("201");
              error.setErrorDescription(this.GetExceptionMessages((Exception) ex));
              break;
            case 1:
              error.setErrorType("202");
              error.setErrorDescription(ex.Message);
              break;
            case 2:
              error.setErrorType("203");
              error.setErrorDescription(ex.Message);
              break;
            default:
              error.setErrorType("201");
              break;
          }
        }
        else
        {
          error.setErrorType("903");
          error.setErrorDescription(this.GetExceptionMessages((Exception) ex));
        }
        return (IDataItem) null;
      }
      catch (Exception ex)
      {
        Error error = (Error) this.error;
        error.setErrorType("201");
        error.setErrorDescription(this.GetExceptionMessages(ex));
        return (IDataItem) null;
      }
    }

    private IDataItem ExecuteAACNAction(ActionType actionType, string username, string password, IDataItem inputData)
    {
      IAction action = AACNActionFactory.newAction(actionType);
      try
      {
        action.validate(username, password);
        username = username.Trim();
        password = password.Trim();
        IDataItem requestData = action.transformRequest(inputData);
        IDataItem responseData = action.sendMessage(username, password, requestData);
        PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
        string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(xml);
        xmlDocument.SelectSingleNode("/");
        if (action.isResponseSuccess(responseData))
          return action.transformResponse(responseData);
        string documentType = (string) null;
        if (actionType == ActionType.SubmitAACN)
          documentType = inputData.getValue(new DocumentKeys().HCAI_Document_Type);
        this.m_error = action.processError(responseData, documentType);
        return (IDataItem) null;
      }
      catch (ToolkitException ex)
      {
        Error error = (Error) this.error;
        ToolkitExceptionCode errorCode = ex.getErrorCode();
        if (errorCode != ToolkitExceptionCode.nHCAISystemError)
        {
          switch ((long) (errorCode - 10L))
          {
            case 0:
              error.setErrorType("201");
              error.setErrorDescription(this.GetExceptionMessages((Exception) ex));
              break;
            case 1:
              error.setErrorType("202");
              error.setErrorDescription(ex.Message);
              break;
            case 2:
              error.setErrorType("203");
              error.setErrorDescription(ex.Message);
              break;
            default:
              error.setErrorType("201");
              break;
          }
        }
        else
        {
          error.setErrorType("903");
          error.setErrorDescription(this.GetExceptionMessages((Exception) ex));
        }
        return (IDataItem) null;
      }
      catch (Exception ex)
      {
        Error error = (Error) this.error;
        error.setErrorType("201");
        error.setErrorDescription(this.GetExceptionMessages(ex));
        return (IDataItem) null;
      }
    }

    private string GetExceptionMessages(Exception e)
    {
      string str = e.Message + "\r\n" + e.StackTrace;
      while (e.InnerException != null)
      {
        e = e.InnerException;
        str = str + "\r\n\r\nInnerException:\r\n" + e.Message + "\r\n" + e.StackTrace;
      }
      return str;
    }

    public IDataItem sendAdjusterResponseAck(string username, string password, string documentNumber)
    {
      IDataItem inputData = (IDataItem) new AdjusterResponseAckRequest();
      AdjusterResponseAckRequestKeys responseAckRequestKeys = new AdjusterResponseAckRequestKeys();
      inputData.addValue(responseAckRequestKeys.Document_Number, ConvertUtil.encode(documentNumber));
      return this.ExecuteAction(ActionType.SendAdjusterResponseAck, username, password, inputData);
    }

    public IDataItem CreateProviderAction(PMSActionType action)
    {
      if (action == PMSActionType.AddProvider)
        return (IDataItem) new ProviderAdd();
      if (action == PMSActionType.UpdateProvider)
        return (IDataItem) new ProviderUpdate();
      return (IDataItem) null;
    }
  }
}
