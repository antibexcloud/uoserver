﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DocType
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit
{
  public enum DocType
  {
    OCF18,
    OCF22,
    OCF23,
    OCF21B,
    OCF21C,
    OCF9,
    Provider,
    AACN,
  }
}
