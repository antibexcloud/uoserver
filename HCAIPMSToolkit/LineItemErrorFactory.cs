﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.LineItemErrorFactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.ToolkitError;
using System.Collections;

namespace PMSToolkit
{
  internal class LineItemErrorFactory
  {
    public static BaseLineItemError newLineItemError(string xpath)
    {
      IList indexList;
      string str = LineItemErrorFactory.RemoveIndicesFromXpath(xpath, out indexList);
      bool flag1 = false;
      bool flag2 = false;
      BaseLineItemError baseLineItemError;
      switch (str)
      {
        case "/AACN/AttendantCareServices/Item/ACSItemID":
        case "/AACN/AttendantCareServices/Item/Assessed/Minutes":
        case "/AACN/AttendantCareServices/Item/Assessed/TimesPerWeek":
        case "/AACN/AttendantCareServices/Item/PMSLineItemKey":
          baseLineItemError = (BaseLineItemError) new AACNLineItemError();
          break;
        case "/AACN/Costs/Assessed/Part/Benefit":
        case "/AACN/Costs/Assessed/Part/HourlyRate":
          baseLineItemError = (BaseLineItemError) new AACNPartLineItemError();
          break;
        case "/x:*/x:AccountActivity/x:OverdueAmount":
        case "/x:*/x:OtherInsuranceAmounts/x:Insurer1/x:Physiotherapy":
        case "/x:*/x:OtherInsuranceAmounts/x:Insurer1/x:Total/x:Proposed":
        case "/x:*/x:PreviouslyApprovedGoodsAndServices/x:PreviouslyBilled":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Attribute":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:DateOfService":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Description":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Estimated/x:GST":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Estimated/x:PST":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:ReimbursableGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new GS21BLineItemError();
          break;
        case "/x:*/x:InjuriesAndSequelae/x:Injury/x:Code":
          baseLineItemError = (BaseLineItemError) new InjuryLineItemError();
          break;
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:Attribute":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:Description":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:GoodsAndServicesComments":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:OtherGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new OtherGSLineItemError();
          break;
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Attribute":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:DateOfService":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Description":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Estimated/x:GST":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Estimated/x:PST":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:OtherReimbursableGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new OtherReimbursableLineItemError();
          break;
        case "/x:*/x:PAFPreApproveServices/x:OtherPreApprovedServices/x:Item/x:Attribute":
        case "/x:*/x:PAFPreApproveServices/x:OtherPreApprovedServices/x:Item/x:Code":
        case "/x:*/x:PAFPreApproveServices/x:OtherPreApprovedServices/x:Item/x:EstimatedFee":
        case "/x:*/x:PAFPreApproveServices/x:OtherPreApprovedServices/x:Item/x:PMSGSKey":
          baseLineItemError = (BaseLineItemError) new PAFLineItemError();
          break;
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:Attribute":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:Code":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:EstimatedLineCost":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:FirstDateOfService":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:SecondaryProviderData/x:Providers":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:SecondaryProviderData/x:Providers/x:Provider/x:ProviderReference/x:Occupation":
        case "/x:*/x:PAFReimbursableFees/x:Items/x:Item/x:SecondaryProviderData/x:Providers/x:Provider/x:ProviderReference/x:ProviderRegistryID":
          baseLineItemError = (BaseLineItemError) new PAFReimbursableLineItemError();
          break;
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Attribute":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Estimated/x:GST":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Estimated/x:PST":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:ProposedGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new GS22LineItemError();
          break;
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Attribute":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Description":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:Count":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:GST":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:PST":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:TotalLineCost":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:ProposedGoodsAndServices/x:NonSessionGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new GS18LineItemError();
          break;
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:Count":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Estimated/x:TotalLineCost":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Item/x:Code":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new SessionHeaderLineItemError();
          flag2 = true;
          break;
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Attribute":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Code":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Description":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Estimated/x:GST":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Estimated/x:LineCost":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Estimated/x:PST":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Measure":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:ProposedGoodsAndServices/x:SessionGoodsAndServices/x:Items/x:Item/x:SessionData/x:Items/x:Item/x:Quantity":
          baseLineItemError = (BaseLineItemError) new SessionLineItemError();
          flag1 = true;
          break;
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:Attribute":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:Code":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:DateOfService":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:Description":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:Measure":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:PMSGSKey":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:Occupation":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:ProviderReference/x:ProviderRegistryID":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:Quantity":
        case "/x:*/x:RenderedGoodsAndServices/x:Items/x:Item/x:ReferenceNumber":
          baseLineItemError = (BaseLineItemError) new RenderedGSLineItemError();
          break;
        default:
          throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "BaseLineItemError::newLineItemError(string) - unexpected value \"" + str + "\"found for the non-indexed path used to define the type of line item.");
      }
      if (baseLineItemError != null)
      {
        if (flag1)
        {
          baseLineItemError.setKey = str;
          baseLineItemError.setHeaderIndex = (int) indexList[0];
          baseLineItemError.setIndex = (int) indexList[1];
        }
        else if (flag2)
        {
          baseLineItemError.setKey = str;
          baseLineItemError.setHeaderIndex = (int) indexList[0];
        }
        else
        {
          baseLineItemError.setKey = str;
          baseLineItemError.setIndex = (int) indexList[0];
        }
      }
      return baseLineItemError;
    }

    private static string RemoveIndicesFromXpath(string xpath, out IList indexList)
    {
      string str = xpath;
      IList list = (IList) new ArrayList();
      for (int startIndex = str.IndexOf("["); startIndex > 0; startIndex = str.IndexOf("["))
      {
        int count = str.IndexOf("]", startIndex) - startIndex + 1;
        int num = int.Parse(str.Substring(startIndex + 1, count - 2));
        str = str.Remove(startIndex, count);
        list.Add((object) num);
      }
      indexList = list;
      return str;
    }
  }
}
