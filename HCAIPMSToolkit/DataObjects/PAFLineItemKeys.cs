﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.PAFLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class PAFLineItemKeys
  {
    public string OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey);
      }
    }

    public string OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code);
      }
    }

    public string OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute);
      }
    }

    public string OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee);
      }
    }
  }
}
