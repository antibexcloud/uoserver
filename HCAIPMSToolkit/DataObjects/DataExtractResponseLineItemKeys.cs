﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.DataExtractResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class DataExtractResponseLineItemKeys
  {
    internal string Items_Item_ReferenceNumber
    {
      get
      {
        return "hl7:sequenceNumber/@value";
      }
    }

    internal string Items_Item_Estimated_PMSGSKey
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:id/@extension";
      }
    }

    internal string Items_Item_Code
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/@code";
      }
    }

    internal string Items_Item_Description
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/@displayName";
      }
    }

    internal string Items_Item_Measure
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:unitQuantity/hl7:numerator/@unit";
      }
    }

    internal string Items_Item_Attribute
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/hl7:qualifier/@code";
      }
    }

    internal string Items_Item_ProviderReference_ProviderRegistryID
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:performer/hl7:healthCareProvider/hl7:id/@extension";
      }
    }

    internal string Items_Item_ProviderReference_Occupation
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:performer/hl7:healthCareProvider/hl7:code/@code";
      }
    }

    internal string Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID
    {
      get
      {
        return "hl7:healthCareProvider/hl7:id/@extension";
      }
    }

    internal string Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation
    {
      get
      {
        return "hl7:healthCareProvider/hl7:code/@code";
      }
    }

    internal string Items_Item_DateOfService
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:effectiveTime/@value";
      }
    }

    internal string Items_Item_FirstDateOfService
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:effectiveTime/@value";
      }
    }

    internal string Items_Item_Estimated_TotalLineCost
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:netAmt/@value";
      }
    }

    internal string Items_Item_Estimated_LineCost
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:unitPriceAmt/hl7:numerator/@value";
      }
    }

    internal string Items_Item_Estimated_Count
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:factorNumber/@value";
      }
    }

    internal string Items_Item_Estimated_GST
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:fstInd/@value";
      }
    }

    internal string Items_Item_Estimated_PST
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:pstInd/@value";
      }
    }

    internal string Items_Item_Quantity
    {
      get
      {
        return "hl7:invoiceElementDetail/hl7:unitQuantity/hl7:numerator/@value";
      }
    }

    internal string Items_Item_Approved_PMSGSKey
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:id/@extension";
      }
    }

    internal string Items_Item_Approved_TotalLineCost
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:netAmt/@value";
      }
    }

    internal string Items_Item_Approved_LineCost
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:unitPriceAmt/hl7:numerator/@value";
      }
    }

    internal string Items_Item_Approved_GST
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:fstInd/@value";
      }
    }

    internal string Items_Item_Approved_PST
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:pstInd/@value";
      }
    }

    internal string Items_Item_Approved_Count
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:factorNumber/@value";
      }
    }

    internal string Items_Item_Approved_ReasonCode
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    internal string Items_Item_Approved_ReasonCodeDesc
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    internal string Items_Item_Approved_OtherReasonCodeDesc
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    internal string InjuryLineItem_Code
    {
      get
      {
        return "hcai:Code";
      }
    }

    internal string InjuryLineItem_Description
    {
      get
      {
        return "hcai:Description";
      }
    }
  }
}
