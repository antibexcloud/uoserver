﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.Provider
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal abstract class Provider : BaseDataItem
  {
    public Provider()
    {
      this.m_data = new Hashtable();
      this.m_lists = new Hashtable();
    }

    internal bool addPrivateDatum(string key, string val)
    {
      try
      {
        if (this.m_data.ContainsKey((object) key))
          this.m_data.Remove((object) key);
        this.m_data.Add((object) key, (object) val);
        return true;
      }
      catch
      {
        return false;
      }
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      return lineItemType == LineItemType.ProviderProfessionLineItem;
    }
  }
}
