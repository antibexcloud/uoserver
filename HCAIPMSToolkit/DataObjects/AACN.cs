﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACN
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class AACN : AACNDocument
  {
    public AACN()
    {
      this.addPrivateDatum(new AACNKeys().HCAI_Document_Type, nameof (AACN));
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      if (lineItemType == LineItemType.AACNServicesLineItem || lineItemType == LineItemType.AACNAssessedPart)
        return true;
      return base.isValidLineItemType(lineItemType);
    }
  }
}
