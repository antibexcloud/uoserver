﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.SubmitResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class SubmitResponseKeys
  {
    public string HCAI_Document_Number
    {
      get
      {
        return nameof (HCAI_Document_Number);
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "PMS_Document_Key";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "PMS_Patient_Key";
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_Type
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_Type);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled);
      }
    }

    public string OCF21C_PreviouslyApprovedGoodsAndServices_Type
    {
      get
      {
        return nameof (OCF21C_PreviouslyApprovedGoodsAndServices_Type);
      }
    }

    public string OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate
    {
      get
      {
        return nameof (OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate);
      }
    }

    public string OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber
    {
      get
      {
        return nameof (OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber);
      }
    }

    public string OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved
    {
      get
      {
        return nameof (OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved);
      }
    }

    public string OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled
    {
      get
      {
        return nameof (OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled);
      }
    }
  }
}
