﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.BaseAACNLineItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal abstract class BaseAACNLineItem : BaseAACNDataItem
  {
    protected LineItemType m_lineItemType;

    public BaseAACNLineItem()
    {
      this.m_data = new Hashtable();
    }

    public LineItemType LineItemType
    {
      get
      {
        return this.m_lineItemType;
      }
    }
  }
}
