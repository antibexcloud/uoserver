﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.DocumentKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class DocumentKeys
  {
    public string OCFVersion
    {
      get
      {
        return nameof (OCFVersion);
      }
    }

    public string HCAI_Document_Type
    {
      get
      {
        return nameof (HCAI_Document_Type);
      }
    }

    public string HCAI_Document_Number
    {
      get
      {
        return nameof (HCAI_Document_Number);
      }
    }

    public string Document_Status
    {
      get
      {
        return nameof (Document_Status);
      }
    }

    public string PMSFields_PMSSoftware
    {
      get
      {
        return nameof (PMSFields_PMSSoftware);
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return nameof (PMSFields_PMSVersion);
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return nameof (PMSFields_PMSDocumentKey);
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return nameof (PMSFields_PMSPatientKey);
      }
    }

    public string Header_ClaimNumber
    {
      get
      {
        return nameof (Header_ClaimNumber);
      }
    }

    public string Header_PolicyNumber
    {
      get
      {
        return nameof (Header_PolicyNumber);
      }
    }

    public string Header_DateOfAccident
    {
      get
      {
        return nameof (Header_DateOfAccident);
      }
    }

    public string Applicant_Name_FirstName
    {
      get
      {
        return nameof (Applicant_Name_FirstName);
      }
    }

    public string Applicant_Name_MiddleName
    {
      get
      {
        return nameof (Applicant_Name_MiddleName);
      }
    }

    public string Applicant_Name_LastName
    {
      get
      {
        return nameof (Applicant_Name_LastName);
      }
    }

    public string Applicant_DateOfBirth
    {
      get
      {
        return nameof (Applicant_DateOfBirth);
      }
    }

    public string Applicant_Gender
    {
      get
      {
        return nameof (Applicant_Gender);
      }
    }

    public string Applicant_TelephoneNumber
    {
      get
      {
        return nameof (Applicant_TelephoneNumber);
      }
    }

    public string Applicant_TelephoneExtension
    {
      get
      {
        return nameof (Applicant_TelephoneExtension);
      }
    }

    public string Applicant_Address_StreetAddress1
    {
      get
      {
        return nameof (Applicant_Address_StreetAddress1);
      }
    }

    public string Applicant_Address_StreetAddress2
    {
      get
      {
        return nameof (Applicant_Address_StreetAddress2);
      }
    }

    public string Applicant_Address_City
    {
      get
      {
        return nameof (Applicant_Address_City);
      }
    }

    public string Applicant_Address_Province
    {
      get
      {
        return nameof (Applicant_Address_Province);
      }
    }

    public string Applicant_Address_PostalCode
    {
      get
      {
        return nameof (Applicant_Address_PostalCode);
      }
    }

    public string Insurer_IBCInsurerID
    {
      get
      {
        return nameof (Insurer_IBCInsurerID);
      }
    }

    public string Insurer_IBCBranchID
    {
      get
      {
        return nameof (Insurer_IBCBranchID);
      }
    }

    public string Insurer_Adjuster_Name_FirstName
    {
      get
      {
        return nameof (Insurer_Adjuster_Name_FirstName);
      }
    }

    public string Insurer_Adjuster_Name_LastName
    {
      get
      {
        return nameof (Insurer_Adjuster_Name_LastName);
      }
    }

    public string Insurer_Adjuster_TelephoneNumber
    {
      get
      {
        return nameof (Insurer_Adjuster_TelephoneNumber);
      }
    }

    public string Insurer_Adjuster_TelephoneExtension
    {
      get
      {
        return nameof (Insurer_Adjuster_TelephoneExtension);
      }
    }

    public string Insurer_Adjuster_FaxNumber
    {
      get
      {
        return nameof (Insurer_Adjuster_FaxNumber);
      }
    }

    public string Insurer_PolicyHolder_IsSameAsApplicant
    {
      get
      {
        return nameof (Insurer_PolicyHolder_IsSameAsApplicant);
      }
    }

    public string Insurer_PolicyHolder_Name_FirstName
    {
      get
      {
        return nameof (Insurer_PolicyHolder_Name_FirstName);
      }
    }

    public string Insurer_PolicyHolder_Name_LastName
    {
      get
      {
        return nameof (Insurer_PolicyHolder_Name_LastName);
      }
    }

    public string AdditionalComments
    {
      get
      {
        return nameof (AdditionalComments);
      }
    }

    public string AttachmentsBeingSent
    {
      get
      {
        return nameof (AttachmentsBeingSent);
      }
    }
  }
}
