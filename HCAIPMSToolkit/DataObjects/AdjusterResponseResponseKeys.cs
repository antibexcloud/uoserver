﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AdjusterResponseResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.21.6787.27110, Culture=neutral, PublicKeyToken=null
// MVID: 1E9D9237-0987-4F64-A03A-6992C1631BC9
// Assembly location: C:\uoemrmvc\Libs\HcaiPms\3.21.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AdjusterResponseResponseKeys
  {
    public string Status_Code
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:typeCode/@code";
      }
    }

    public string HCAI_Document_Number
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail[child::hl7:location='HCAI_Document_Number']/hl7:text";
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:targetMessage/hl7:id/@extension";
      }
    }

    public string Submission_Date
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:effectiveTime/@value";
      }
    }

    public string Submission_Source
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='Submission_Source']]/hl7:value/@code";
      }
    }

    public string Adjuster_Response_Date
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='Adjuster_Response_Date']]/hl7:value/@code";
      }
    }

    public string In_Dispute
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='In_Dispute']]/hl7:value/@code";
      }
    }

    public string Document_Type
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:id/@extension";
      }
    }

    public string Document_Status
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'Document_Status']]/@code";
      }
    }

    public string SigningAdjuster_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'SigningAdjuster_FirstName']]/@code";
      }
    }

    public string SigningAdjuster_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'SigningAdjuster_LastName']]/@code";
      }
    }

    public string NameOfAdjusterOnPDF
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:NameOfAdjusterOnPDF";
      }
    }

    public string ApprovedByOnPDF
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:ApprovedByOnPDF";
      }
    }

    public string Archival_Status
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'ArchivalStatus']]/@code";
      }
    }

    public string Attachments_Received_Date
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='Attachments_Received_Date']]/hl7:value/@code";
      }
    }

    public string Insurer_IBCInsurerID
    {
      get
      {
        return "/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:id/@extension";
      }
    }

    public string Insurer_IBCBranchID
    {
      get
      {
        return "/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:id/@extension";
      }
    }

    public string Claimant_First_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:given[1]";
      }
    }

    public string Claimant_Middle_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:given[2]";
      }
    }

    public string Claimant_Last_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:family";
      }
    }

    public string Claimant_Date_Of_Birth
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:birthTime/@value";
      }
    }

    public string Claimant_Gender
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:administrativeGenderCode/@code";
      }
    }

    public string Claimant_Telephone
    {
      get
      {
        return "substring(/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:telecom/@value,5,10)";
      }
    }

    public string Claimant_Extension
    {
      get
      {
        return "substring(/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:telecom/@value,20,10)";
      }
    }

    public string Claimant_Address1
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:streetAddressLine[1]";
      }
    }

    public string Claimant_Address2
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:streetAddressLine[2]";
      }
    }

    public string Claimant_City
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:city";
      }
    }

    public string Claimant_Province
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:state";
      }
    }

    public string Claimant_Postal_Code
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:postalCode";
      }
    }

    public string Messages
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail";
      }
    }

    internal string GS21BLineItem
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    internal string GS18LineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component";
      }
    }

    internal string GS22LineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component";
      }
    }

    internal string GS9LineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='GoodsAndServices']]/hl7:component";
      }
    }

    internal string OtherGSLineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:component";
      }
    }

    internal string OtherReimbursableLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    internal string PAFLineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PreApproved']]/hl7:component";
      }
    }

    internal string PAFReimbursableLineItem
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PAFReimbursableFees']]/hl7:component";
      }
    }

    internal string NonSessionHeaderLineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='NonSessionGoodsAndServices']]/hl7:component";
      }
    }

    internal string SessionHeaderLineItem
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component";
      }
    }

    internal string InjuryLineItem
    {
      get
      {
        return "InjuryLineItemNotYetImplemented";
      }
    }

    internal string RenderedGSLineItem
    {
      get
      {
        return "RenderedGSLineItemNotYetImplemented";
      }
    }

    internal string ActivityLineItem
    {
      get
      {
        return "/hl7:QUCR_IN830101UV01/hl7:controlActProcess/hl7:subject/hl7:adjudResultsGroup/hl7:reference";
      }
    }

    internal string ProviderLineItem
    {
      get
      {
        return "ProviderLineItemNotYetImplemented";
      }
    }

    internal string RegistrationLineItem
    {
      get
      {
        return "RegistrationLineItemNotYetImplemented";
      }
    }

    internal string InsurerLineItem
    {
      get
      {
        return "/hcai:InsurerList/hcai:Insurer";
      }
    }

    internal string BranchLineItem
    {
      get
      {
        return "hcai:Branch";
      }
    }
  }
}
