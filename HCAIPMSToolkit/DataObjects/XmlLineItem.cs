﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.XmlLineItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class XmlLineItem : XmlDataItem
  {
    private DataExtractResponseLineItemKeys OCFCommonKeys = new DataExtractResponseLineItemKeys();

    protected override string Translate(string key, string value)
    {
      if (key == this.OCFCommonKeys.Items_Item_Measure)
        return ConvertUtil.convertNeCSTMeasureToNative(value);
      if (key == this.OCFCommonKeys.Items_Item_DateOfService)
        return ConvertUtil.convertNeCSTDateToNative(value);
      if (key == this.OCFCommonKeys.Items_Item_Approved_GST || key == this.OCFCommonKeys.Items_Item_Approved_PST || (key == this.OCFCommonKeys.Items_Item_Estimated_PST || key == this.OCFCommonKeys.Items_Item_Estimated_GST))
        return ConvertUtil.convertNeCSTBooleanToYesNo(value);
      return base.Translate(key, value);
    }
  }
}
