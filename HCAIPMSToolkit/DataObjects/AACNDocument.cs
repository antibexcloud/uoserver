﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNDocument
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal abstract class AACNDocument : BaseAACNDataItem
  {
    public AACNDocument()
    {
      this.m_data = new Hashtable();
      this.m_lists = new Hashtable();
      AACNKeys aacnKeys = new AACNKeys();
    }

    internal bool addPrivateDatum(string key, string val)
    {
      try
      {
        if (this.m_data.ContainsKey((object) key))
          this.m_data.Remove((object) key);
        this.m_data.Add((object) key, (object) val);
        return true;
      }
      catch
      {
        return false;
      }
    }

    internal bool addPublicDatum(string key, string val)
    {
      AACNKeys aacnKeys = new AACNKeys();
      this.guardRestrictedKey(key, aacnKeys.HCAI_Document_Type);
      return this.addPrivateDatum(key, val);
    }

    private void guardRestrictedKey(string key, string restrictedKey)
    {
      if (key == restrictedKey)
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, string.Format("\"{0}\" is a reserved key that cannot be set by the client application.", (object) restrictedKey));
    }

    public override bool addValue(string key, string val)
    {
      if (val == null)
        return this.addPublicDatum(key, "");
      return this.addPublicDatum(key, val);
    }
  }
}
