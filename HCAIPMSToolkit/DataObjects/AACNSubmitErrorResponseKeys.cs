﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNSubmitErrorResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNSubmitErrorResponseKeys
  {
    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return nameof (PMSFields_PMSDocumentKey);
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return nameof (PMSFields_PMSPatientKey);
      }
    }

    public string XPath
    {
      get
      {
        return nameof (XPath);
      }
    }
  }
}
