﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.DataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class DataExtractResponseKeys
  {
    public string Header_ClaimNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string Header_PolicyNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:id/@extension";
      }
    }

    public string Header_DateOfAccident
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:pertinentInformation/hl7:accident/hl7:effectiveTime/@value";
      }
    }

    public string Applicant_Name_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:given[1]";
      }
    }

    public string Applicant_Name_MiddleName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:given[2]";
      }
    }

    public string Applicant_Name_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:family";
      }
    }

    public string Applicant_DateOfBirth
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:birthTime/@value";
      }
    }

    public string Applicant_Gender
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:administrativeGenderCode/@code";
      }
    }

    public string Applicant_TelephoneNumber
    {
      get
      {
        return "substring(/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:telecom/@value,5,10)";
      }
    }

    public string Applicant_TelephoneExtension
    {
      get
      {
        return "substring(/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:telecom/@value,20,10)";
      }
    }

    public string Applicant_Address_StreetAddress1
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:streetAddressLine[1]";
      }
    }

    public string Applicant_Address_StreetAddress2
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:streetAddressLine[2]";
      }
    }

    public string Applicant_Address_City
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:city";
      }
    }

    public string Applicant_Address_Province
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:state";
      }
    }

    public string Applicant_Address_PostalCode
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:postalCode";
      }
    }

    public string AdditionalComments
    {
      get
      {
        return "hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:AdditionalComments";
      }
    }

    public string OCFVersion
    {
      get
      {
        return "hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:OCFVersion";
      }
    }

    public string Insurer_PolicyHolder_IsSameAsApplicant
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:code";
      }
    }

    public string Insurer_PolicyHolder_Name_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string Insurer_PolicyHolder_Name_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='AUTOPOL']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string Insurer_IBCInsurerID
    {
      get
      {
        return "/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:id/@extension";
      }
    }

    public string Insurer_IBCBranchID
    {
      get
      {
        return "/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:id/@extension";
      }
    }

    public string Insurer_Adjuster_Name_FirstName
    {
      get
      {
        return "/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:contactParty/hl7:contactPerson/hl7:name/hl7:given";
      }
    }

    public string Insurer_Adjuster_Name_LastName
    {
      get
      {
        return "/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:contactParty/hl7:contactPerson/hl7:name/hl7:family";
      }
    }

    public string Insurer_Adjuster_TelephoneNumber
    {
      get
      {
        return "substring(/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:contactParty/hl7:telecom[1]/@value,5,10)";
      }
    }

    public string Insurer_Adjuster_TelephoneExtension
    {
      get
      {
        return "substring(/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:contactParty/hl7:telecom[1]/@value,20,10)";
      }
    }

    public string Insurer_Adjuster_FaxNumber
    {
      get
      {
        return "substring(/hl7:*/hl7:receiver/hl7:device/hl7:asAgent/hl7:representedOrganization/hl7:notificationParty/hl7:contactOrganization/hl7:contactParty/hl7:telecom[2]/@value,5,10)";
      }
    }

    public string Messages
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail";
      }
    }

    public string Status_Code
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:typeCode/@code";
      }
    }

    public string HCAI_Document_Number
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:acknowledgementDetail[child::hl7:location='HCAI_Document_Number']/hl7:text";
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "/hl7:*/hl7:acknowledgement/hl7:targetMessage/hl7:id/@extension";
      }
    }

    public string Submission_Date
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:effectiveTime/@value";
      }
    }

    public string Submission_Source
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='Submission_Source']]/hl7:value/@code";
      }
    }

    public string Adjuster_Response_Date
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:author/hl7:time/@value";
      }
    }

    public string In_Dispute
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='In_Dispute']]/hl7:value/@code";
      }
    }

    public string Document_Type
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:id/@extension";
      }
    }

    public string Document_Status
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'Document_Status']]/@code";
      }
    }

    public string SigningAdjuster_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:SigningAdjusterName/hcai:FirstName";
      }
    }

    public string SigningAdjuster_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:SigningAdjusterName/hcai:LastName";
      }
    }

    public string Archival_Status
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:ArchivalStatus";
      }
    }

    public string AttachmentsBeingSent
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:AttachmentsBeingSent";
      }
    }

    public string Attachments_Received_Date
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier[child::hl7:name[@code='Attachments_Received_Date']]/hl7:value/@code";
      }
    }

    public string Claimant_First_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:given[1]";
      }
    }

    public string Claimant_Middle_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:given[2]";
      }
    }

    public string Claimant_Last_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:name/hl7:family";
      }
    }

    public string Claimant_Date_Of_Birth
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:birthTime/@value";
      }
    }

    public string Claimant_Gender
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:administrativeGenderCode/@code";
      }
    }

    public string Claimant_Telephone
    {
      get
      {
        return "substring(/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:telecom/@value,5,10)";
      }
    }

    public string Claimant_Extension
    {
      get
      {
        return "substring(/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:telecom/@value,20)";
      }
    }

    public string Claimant_Address1
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:streetAddressLine[1]";
      }
    }

    public string Claimant_Address2
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:streetAddressLine[2]";
      }
    }

    public string Claimant_City
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:city";
      }
    }

    public string Claimant_Province
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:state";
      }
    }

    public string Claimant_Postal_Code
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:coverage/hl7:policyOrAccount/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:coveredPartyAsPatientPerson/hl7:addr/hl7:postalCode";
      }
    }
  }
}
