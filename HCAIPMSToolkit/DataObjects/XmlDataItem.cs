﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.XmlDataItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;
using System.Collections;
using System.Xml;
using System.Xml.XPath;

namespace PMSToolkit.DataObjects
{
  public class XmlDataItem : IDataItem
  {
    private XmlNode xmn;
    private XmlNamespaceManager _XmlNSMgr;

    public XmlDataItem()
    {
      this._XmlNSMgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      this._XmlNSMgr.AddNamespace("hl7", "urn:hl7-org:v3");
      this._XmlNSMgr.AddNamespace("hcai", "http://hcai.ca");
    }

    public XmlNamespaceManager XmlNsMgr
    {
      get
      {
        return this._XmlNSMgr;
      }
      set
      {
        this._XmlNSMgr = value;
      }
    }

    public XmlNode Xmn
    {
      get
      {
        return this.xmn;
      }
      set
      {
        this.xmn = value;
      }
    }

    public bool addValue(string key, string sValue)
    {
      throw new NotSupportedException();
    }

    public bool addLineItem(IDataItem lineItem)
    {
      throw new NotSupportedException();
    }

    public IDataItem newLineItem(LineItemType lineItemType)
    {
      throw new NotSupportedException();
    }

    public string getValue(string key)
    {
      XmlNode xmlNode = this.xmn.SelectSingleNode("/");
      try
      {
        xmlNode = this.xmn.SelectSingleNode(key, this._XmlNSMgr);
      }
      catch (Exception ex)
      {
      }
      if (key.StartsWith("sub"))
      {
        string str = key.Substring(10);
        string xpath1 = str.Remove(str.IndexOf(","));
        string empty;
        try
        {
          empty = this.xmn.SelectSingleNode(xpath1, this._XmlNSMgr).Value;
        }
        catch (Exception ex)
        {
          empty = string.Empty;
        }
        if (empty == string.Empty || empty == "tel:;ext=")
          return string.Empty;
        XPathNavigator navigator = this.xmn.CreateNavigator();
        string xpath2 = key;
        XPathExpression xpathExpression = navigator.Compile(xpath2);
        xpathExpression.SetContext(this._XmlNSMgr);
        XPathExpression expr = xpathExpression;
        return ConvertUtil.decode((string) navigator.Evaluate(expr));
      }
      string s;
      if (xmlNode != null)
      {
        string sValue = !(key == new DataExtractResponseKeys().Insurer_PolicyHolder_IsSameAsApplicant) ? (key == new AACNDataExtractResponseKeys().Document_Type || key == new AACNAdjusterResponseKeys().Document_Type ? xmlNode.Name : xmlNode.InnerText) : (xmlNode.Attributes.Count <= 0 ? string.Empty : xmlNode.Attributes[0].Value);
        s = this.Translate(key, sValue);
      }
      else
      {
        string empty = string.Empty;
        s = this.Translate(key, empty);
      }
      return ConvertUtil.decode(s);
    }

    public IList getList(LineItemType lineItemType)
    {
      ListKeys listKeys = new ListKeys();
      string xpath = (string) null;
      switch (lineItemType)
      {
        case LineItemType.GS18SessionHeaderLineItem:
          xpath = listKeys.AR_SessionHeaderLineItem;
          break;
        case LineItemType.GS18NonSessionLineItem:
          xpath = listKeys.AR_NonSessionLineItem;
          break;
        case LineItemType.GS21BLineItem:
          xpath = listKeys.AR_GS21BLineItem;
          break;
        case LineItemType.GS21CRenderedGSLineItem:
          xpath = listKeys.RenderedGSLineItem;
          break;
        case LineItemType.GS21CPAFReimbursableLineItem:
          xpath = listKeys.AR_PAFReimbursableLineItem;
          break;
        case LineItemType.GS21COtherReimbursableLineItem:
          xpath = listKeys.AR_OtherReimbursableLineItem;
          break;
        case LineItemType.GS22LineItem:
          xpath = listKeys.AR_GS22LineItem;
          break;
        case LineItemType.GS23PAFLineItem:
          xpath = listKeys.AR_PAFLineItem;
          break;
        case LineItemType.GS23OtherGSLineItem:
          xpath = listKeys.AR_OtherGSLineItem;
          break;
        case LineItemType.GS9LineItem:
          xpath = listKeys.AR_GS9LineItem;
          break;
        case LineItemType.DE_GS18LineItem_Estimated:
          xpath = listKeys.DE_OCF18_ProposedGoodsAndServices_NonSession_GS_Axis;
          break;
        case LineItemType.DE_GS18SessionHeaderLineItem_Estimated:
          xpath = listKeys.DE_OCF18_ProposedGoodsAndServices_Session_GS_Axis;
          break;
        case LineItemType.DE_GS18SessionLineItem_Estimated:
          xpath = listKeys.DE_OCF18_ProposedGoodsAndServices_Session_GS_Data_Axis;
          break;
        case LineItemType.DE_GS18LineItem_Approved:
          xpath = listKeys.DE_OCF18_Approved_NonSessionGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS18SessionHeaderLineItem_Approved:
          xpath = listKeys.DE_OCF18_Approved_SessionGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS21BLineItem_Estimated:
          xpath = listKeys.DE_OCF21B_ReimbursableGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS21BLineItem_Approved:
          xpath = listKeys.DE_OCF21B_Approved_ReimbursableGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS21CRenderedGSLineItem:
          xpath = listKeys.DE_OCF21C_RenderedGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS21CPAFReimbursableLineItem_Estimated:
          xpath = listKeys.DE_OCF21C_PAFReimbursableFees_Axis;
          break;
        case LineItemType.DE_GS21COtherReimbursableLineItem_Estimated:
          xpath = listKeys.DE_OCF21C_OtherReimbursableGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS21CPAFReimbursableLineItem_Approved:
          xpath = listKeys.DE_OCF21C_Approved_PAFReimbursableFees_Axis;
          break;
        case LineItemType.DE_GS21COtherReimbursableLineItem_Approved:
          xpath = listKeys.DE_OCF21C_Approved_OtherReimbursableGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS22LineItem_Estimated:
          xpath = listKeys.DE_OCF22_Estimated_ProposedGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS22LineItem_Approved:
          xpath = listKeys.DE_OCF22_Approved_ProposedGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS23PAFLineItem:
          xpath = listKeys.DE_OCF23_PAFServices_OtherPreApprovedServices_Axis;
          break;
        case LineItemType.DE_GS23OtherGSLineItem_Estimated:
          xpath = listKeys.DE_OCF23_Estimated_OtherGoodsAndServices_Axis;
          break;
        case LineItemType.DE_GS23OtherGSLineItem_Approved:
          xpath = listKeys.DE_OCF23_Approved_OtherGoodsAndServices_Axis;
          break;
        case LineItemType.InjuryLineItem:
          xpath = listKeys.InjuryLineItem;
          break;
        case LineItemType.ActivityLineItem:
          xpath = listKeys.ActivityLineItem;
          break;
        case LineItemType.ProviderLineItem:
          xpath = listKeys.ProviderLineItem;
          break;
        case LineItemType.RegistrationLineItem:
          xpath = listKeys.RegistrationLineItem;
          break;
        case LineItemType.InsurerLineItem:
          xpath = listKeys.InsurerLineItem;
          break;
        case LineItemType.BranchLineItem:
          xpath = listKeys.BranchLineItem;
          break;
        case LineItemType.FacilityLineItem:
          xpath = listKeys.FacilityLineItem;
          break;
        case LineItemType.FacilityLicenseLineItem:
          xpath = listKeys.FacilityLicenseLineItem;
          break;
        case LineItemType.AACNPartHeader:
          xpath = listKeys.AttendantCareServices_PartHeader;
          break;
        case LineItemType.AACNServicesLineItem:
          xpath = listKeys.AttendantCareServices_Item;
          break;
        case LineItemType.AACNAssessedPart:
          xpath = listKeys.Costs_Assessed_Part;
          break;
        case LineItemType.AACNApprovedPart:
          xpath = listKeys.Costs_Approved_Part;
          break;
      }
      XmlDataItemList xmlDataItemList = new XmlDataItemList();
      try
      {
        XmlNodeList xmlNodeList = this.xmn.SelectNodes(xpath, this._XmlNSMgr);
        xmlDataItemList.XmlNodeList = xmlNodeList;
      }
      catch (Exception ex)
      {
      }
      return (IList) xmlDataItemList;
    }

    public IList getList(string listXPath)
    {
      ListKeys listKeys = new ListKeys();
      XmlDataItemList xmlDataItemList = new XmlDataItemList();
      try
      {
        XmlNodeList xmlNodeList = this.xmn.SelectNodes(listXPath, this._XmlNSMgr);
        xmlDataItemList.XmlNodeList = xmlNodeList;
      }
      catch (Exception ex)
      {
      }
      return (IList) xmlDataItemList;
    }

    public virtual bool isValidLineItemType(LineItemType lineItemType)
    {
      return false;
    }

    protected virtual string Translate(string key, string sValue)
    {
      return sValue;
    }

    public string getEncodedValue(string key)
    {
      throw new NotSupportedException("Should not be calling this method from XMLDataItem class.");
    }
  }
}
