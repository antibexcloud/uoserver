﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF21B
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class OCF21B : Document
  {
    internal OCF21B()
    {
      this.addPrivateDatum(new OCF21BKeys().HCAI_Document_Type, nameof (OCF21B));
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      if (lineItemType == LineItemType.GS21BLineItem || lineItemType == LineItemType.InjuryLineItem)
        return true;
      return base.isValidLineItemType(lineItemType);
    }
  }
}
