﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.ProviderAddKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class ProviderAddKeys
  {
    public string HCAI_Facility_Registry_ID
    {
      get
      {
        return "hcai:FacilityRegistryID";
      }
    }

    public string Provider_First_Name
    {
      get
      {
        return "hcai:ProviderName/hcai:FirstName";
      }
    }

    public string Provider_Last_Name
    {
      get
      {
        return "hcai:ProviderName/hcai:LastName";
      }
    }

    public string Provider_End_Date
    {
      get
      {
        return "hcai:EndDate";
      }
    }

    public string Provider_Hourly_Rate
    {
      get
      {
        return "hcai:HourlyRate";
      }
    }
  }
}
