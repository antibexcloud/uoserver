﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.InsurerLineItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal class InsurerLineItem : BaseLineItem
  {
    public InsurerLineItem()
    {
      this.m_lineItemType = LineItemType.InsurerLineItem;
      this.m_lists = new Hashtable();
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      return LineItemType.BranchLineItem == lineItemType;
    }
  }
}
