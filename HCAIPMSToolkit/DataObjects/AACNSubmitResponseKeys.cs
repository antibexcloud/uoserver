﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNSubmitResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNSubmitResponseKeys
  {
    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "/hcai:AACNResponse/hcai:AACN/hcai:PMSFields/hcai:PMSDocumentKey";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "/hcai:AACNResponse/hcai:AACN/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string ResponseStatus
    {
      get
      {
        return "/hcai:AACNResponse/hcai:ResponseStatus";
      }
    }

    public string DocumentNumber
    {
      get
      {
        return "/hcai:AACNResponse/hcai:DocumentNumber";
      }
    }

    public string FacilityVersion
    {
      get
      {
        return "/hcai:AACNResponse/hcai:FacilityVersion";
      }
    }

    public string InsurerVersion
    {
      get
      {
        return "/hcai:AACNResponse/hcai:InsurerVersion";
      }
    }

    public string BranchVersion
    {
      get
      {
        return "/hcai:AACNResponse/hcai:BranchVersion";
      }
    }

    public string Costs_Assessed_Benefit
    {
      get
      {
        return "/hcai:AACNResponse/hcai:AACN/hcai:Costs/hcai:Assessed/hcai:Benefit";
      }
    }
  }
}
