﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.DataExtractRequestKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class DataExtractRequestKeys
  {
    public string Document_Number
    {
      get
      {
        return nameof (Document_Number);
      }
    }

    public string Document_Type
    {
      get
      {
        return nameof (Document_Type);
      }
    }
  }
}
