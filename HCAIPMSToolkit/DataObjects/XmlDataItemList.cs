﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.XmlDataItemList
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;
using System.Collections;
using System.Xml;

namespace PMSToolkit.DataObjects
{
  public class XmlDataItemList : IList, ICollection, IEnumerable
  {
    private XmlNodeList xmlNodeList;

    internal XmlNodeList XmlNodeList
    {
      get
      {
        return this.xmlNodeList;
      }
      set
      {
        this.xmlNodeList = value;
      }
    }

    public int Add(object value)
    {
      throw new NotImplementedException();
    }

    public bool Contains(object value)
    {
      throw new NotImplementedException();
    }

    public void Clear()
    {
      throw new NotImplementedException();
    }

    public int IndexOf(object value)
    {
      throw new NotImplementedException();
    }

    public void Insert(int index, object value)
    {
      throw new NotImplementedException();
    }

    public void Remove(object value)
    {
      throw new NotImplementedException();
    }

    public void RemoveAt(int index)
    {
      throw new NotImplementedException();
    }

    public bool IsReadOnly
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public bool IsFixedSize
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public object this[int index]
    {
      get
      {
        if (index < 0 || index >= this.Count)
          throw new ArgumentOutOfRangeException(nameof (index), (object) index, "Invalid index value.");
        XmlLineItem xmlLineItem = new XmlLineItem();
        XmlNode xmlNode = this.XmlNodeList[index];
        xmlLineItem.Xmn = xmlNode;
        return (object) xmlLineItem;
      }
      set
      {
        throw new NotImplementedException();
      }
    }

    public void CopyTo(Array array, int index)
    {
      throw new NotImplementedException();
    }

    public int Count
    {
      get
      {
        return this.XmlNodeList.Count;
      }
    }

    public object SyncRoot
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public bool IsSynchronized
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public IEnumerator GetEnumerator()
    {
      throw new NotImplementedException();
    }
  }
}
