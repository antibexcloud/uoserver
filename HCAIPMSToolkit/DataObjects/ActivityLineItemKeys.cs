﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.ActivityLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class ActivityLineItemKeys
  {
    public string HCAI_Document_Number
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:id/@extension";
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:outcomeOf/hl7:adjudicationResult/hl7:reference/hl7:invoiceElementGroup/hl7:id[1]/@extension";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:outcomeOf/hl7:adjudicationResult/hl7:reference/hl7:invoiceElementGroup/hl7:id[2]/@extension";
      }
    }

    public string Submission_Source
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:outcomeOf/hl7:adjudicationResult/hl7:reference/hl7:invoiceElementGroup/hl7:text/hl7:inlineData/hl7:submissionSource";
      }
    }

    public string Submission_Date
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:outcomeOf/hl7:adjudicationResult/hl7:reference/hl7:invoiceElementGroup/hl7:text/hl7:inlineData/hl7:submissionDate";
      }
    }

    public string Document_Status
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:outcomeOf/hl7:adjudicationResult/hl7:reference/hl7:invoiceElementGroup/hl7:text/hl7:inlineData/hl7:documentStatus";
      }
    }

    public string Document_Type
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementGroup/hl7:outcomeOf/hl7:adjudicationResult/hl7:reference/hl7:invoiceElementGroup/hl7:text/hl7:inlineData/hl7:documentType";
      }
    }
  }
}
