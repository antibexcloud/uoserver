﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF18DataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.21.6787.27110, Culture=neutral, PublicKeyToken=null
// MVID: 1E9D9237-0987-4F64-A03A-6992C1631BC9
// Assembly location: C:\uoemrmvc\Libs\HcaiPms\3.21.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF18DataExtractResponseKeys : DataExtractResponseKeys
  {
    public string PMSFields_PMSSoftware
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PMSFields/hcai:PMSSoftware";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PMSFields/hcai:PMSVersion";
      }
    }

    public string Insurer_IBCInsurerName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:InsurerName";
      }
    }

    public string Facility_FacilityName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:FacilityName";
      }
    }

    public string OCF18_Header_PlanNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:header";
      }
    }

    public string OCF18_HealthPractitioner_FacilityRegistryID
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:FacilityRegistryID";
      }
    }

    public string OCF18_HealthPractitioner_ProviderRegistryID
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:ProviderRegistryID";
      }
    }

    public string OCF18_HealthPractitioner_Occupation
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:Occupation";
      }
    }

    public string OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:ConflictOfInterest/hcai:ConflictExists";
      }
    }

    public string OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:ConflictOfInterest/hcai:ConflictDetails";
      }
    }

    public string OCF18_HealthPractitioner_IsSignatureOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:IsSignatureOnFile";
      }
    }

    public string OCF18_HealthPractitioner_DateSigned
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:HealthPractitioner/hcai:DateSigned";
      }
    }

    public string OCF18_IsOtherHealthPractitioner
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:IsOtherHealthPractitioner";
      }
    }

    public string OCF18_OtherHealthPractitioner_Name_FirstName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Name/hcai:FirstName";
      }
    }

    public string OCF18_OtherHealthPractitioner_Name_LastName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Name/hcai:LastName";
      }
    }

    public string OCF18_OtherHealthPractitioner_FacilityName
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:FacilityName";
      }
    }

    public string OCF18_OtherHealthPractitioner_AISIFacilityNumber
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:AISIFacilityNumber";
      }
    }

    public string OCF18_OtherHealthPractitioner_FacilityRegistryID
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:FacilityRegistryID";
      }
    }

    public string OCF18_OtherHealthPractitioner_LicenseNumber
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:LicenseNumber";
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_StreetAddress1
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Address/hcai:StreetAddress1";
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_StreetAddress2
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Address/hcai:StreetAddress2";
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_City
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Address/hcai:City";
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_Province
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Address/hcai:Province";
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_PostalCode
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Address/hcai:PostalCode";
      }
    }

    public string OCF18_OtherHealthPractitioner_TelephoneNumber
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:TelephoneNumber";
      }
    }

    public string OCF18_OtherHealthPractitioner_TelephoneExtension
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:TelephoneExtension";
      }
    }

    public string OCF18_OtherHealthPractitioner_FaxNumber
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:FaxNumber";
      }
    }

    public string OCF18_OtherHealthPractitioner_Email
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:EMail";
      }
    }

    public string OCF18_OtherHealthPractitioner_ProviderRegistryNumber
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:ProviderRegistryNumber";
      }
    }

    public string OCF18_OtherHealthPractitioner_Occupation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:Occupation";
      }
    }

    public string OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:ConflictOfInterest/hcai:ConflictExists";
      }
    }

    public string OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:ConflictOfInterest/hcai:ConflictDetails";
      }
    }

    public string OCF18_OtherHealthPractitioner_IsSignatureOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:IsSignatureOnFile";
      }
    }

    public string OCF18_OtherHealthPractitioner_DateSigned
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:OtherHealthPractitioner/hcai:DateSigned";
      }
    }

    public string OCF18_IsRHPSameAsHealthPractitioner
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:IsRHPSameAsHealthPractitioner";
      }
    }

    public string OCF18_RegulatedHealthProfessional_FacilityRegistryID
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:FacilityRegistryID";
      }
    }

    public string OCF18_RegulatedHealthProfessional_ProviderRegistryID
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:ProviderRegistryID";
      }
    }

    public string OCF18_RegulatedHealthProfessional_Occupation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:Occupation";
      }
    }

    public string OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:ConflictOfInterest/hcai:ConflictExists";
      }
    }

    public string OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:ConflictOfInterest/hcai:ConflictDetails";
      }
    }

    public string OCF18_RegulatedHealthProfessional_IsSignatureOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:IsSignatureOnFile";
      }
    }

    public string OCF18_RegulatedHealthProfessional_DateSigned
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:RegulatedHealthProfessional/hcai:DateSigned";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:Response";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:Explanation";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:InvestigationOrTreatment/hcai:Response";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:InvestigationOrTreatment/hcai:Explanation";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:ConcurrentCondition/hcai:Response";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:ConcurrentCondition/hcai:Explanation";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_IsPAF_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:IsPAF/hcai:Response";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:IsPAF/hcai:Circumstance";
      }
    }

    public string OCF18_PriorAndConcurrentConditions_IsPAF_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PriorAndConcurrentConditions/hcai:IsPAF/hcai:Explanation";
      }
    }

    public string OCF18_ActivityLimitations_ToEmployment_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ActivityLimitations/hcai:ToEmployment/hcai:Response";
      }
    }

    public string OCF18_ActivityLimitations_ToNormalLife_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ActivityLimitations/hcai:ToNormalLife/hcai:Response";
      }
    }

    public string OCF18_ActivityLimitations_ImpactOnAbilities
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ActivityLimitations/hcai:ImpactOnAbilities";
      }
    }

    public string OCF18_ActivityLimitations_ModifiedEmployment_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ActivityLimitations/hcai:ModifiedEmployment/hcai:Response";
      }
    }

    public string OCF18_ActivityLimitations_ModifiedEmployment_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ActivityLimitations/hcai:ModifiedEmployment/hcai:Explanation";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Goals/hcai:PainReduction";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Goals/hcai:IncreaseInStrength";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Goals/hcai:IncreasedRangeOfMotion";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_Other
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Goals/hcai:Other";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Goals/hcai:OtherDescription";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:FunctionalGoals/hcai:ReturnToNormalLiving";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:FunctionalGoals/hcai:ReturnToModifiedWorkActivities";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:FunctionalGoals/hcai:ReturnToPreAccidentWorkActivities";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:FunctionalGoals/hcai:Other";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:FunctionalGoals/hcai:OtherDescription";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Evaluation/hcai:ProgressEvaluation";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Evaluation/hcai:PreviousPlanImpact";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:BarriersToRecovery/hcai:Response";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:BarriersToRecovery/hcai:Explanation";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:BarriersToRecovery/hcai:Recommendations/hcai:Response";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:BarriersToRecovery/hcai:Recommendations/hcai:Explanation";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:ConcurrentTreatment/hcai:Response";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:ConcurrentTreatment/hcai:Explanation";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Consistency/hcai:Response";
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:PlanGoalsOutcomesAndMethods/hcai:Consistency/hcai:Explanation";
      }
    }

    public string OCF18_ProposedGoodsAndServices_TreatmentPlanDuration
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ProposedGoodsAndServices/hcai:TreatmentPlanDuration";
      }
    }

    public string OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ProposedGoodsAndServices/hcai:AlreadyProvidedTreatmentVisits";
      }
    }

    public string OCF18_ProposedGoodsAndServices_GoodsAndServicesComments
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ProposedGoodsAndServices/hcai:GoodsAndServicesComments";
      }
    }

    public string OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ProposedGoodsAndServices/hcai:ApplicantInitialsOnFile";
      }
    }

    public string OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ProposedGoodsAndServices/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF18_InsurerSignature_ApplicantSignatureWaived
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ApplicantSignatureWaived";
      }
    }

    public string OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ApplicantSignature/hcai:IsApplicantSignatureWaivedByInsurer";
      }
    }

    public string OCF18_ApplicantSignature_IsApplicantSignatureOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ApplicantSignature/hcai:IsApplicantSignatureOnFile";
      }
    }

    public string OCF18_ApplicantSignature_SigningApplicant_FirstName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ApplicantSignature/hcai:SigningApplicant/hcai:FirstName";
      }
    }

    public string OCF18_ApplicantSignature_SigningApplicant_LastName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ApplicantSignature/hcai:SigningApplicant/hcai:LastName";
      }
    }

    public string OCF18_ApplicantSignature_SigningDate
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF18/hcai:ApplicantSignature/hcai:SigningDate";
      }
    }

    public string OCF18_OtherInsurance_MOHAvailable
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='PUBLICPOL']]/hl7:id/@extension";
      }
    }

    public string OCF18_OtherInsurance_IsThereOtherInsuranceCoverage
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='PUBLICPOL']]";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_ID
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:author/hl7:carrierRole/hl7:id/@assigningAuthorityName";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:id/@extension";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_ID
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:author/hl7:carrierRole/hl7:id/@assigningAuthorityName";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:id/@extension";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string OCF18_InsurerTotals_MOH_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='MOH']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_MOH_Approved_LineCost
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_OtherInsurers_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_OtherInsurers_Approved_LineCost
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_SubTotal_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_SubTotal_Approved
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_TotalCount_Approved
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:factorNumber/@value";
      }
    }

    public string OCF18_InsurerTotals_GST_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_PST_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_GST_Approved_LineCost
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_PST_Approved_LineCost
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string NameOfAdjusterOnPDF
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:NameOfAdjusterOnPDF";
      }
    }
  }
}
