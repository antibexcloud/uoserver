﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNSubmitResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNSubmitResponseLineItemKeys
  {
    public string AttendantCareServices_PartHeader_ACSItemID
    {
      get
      {
        return "@ACSItemID";
      }
    }

    public string AttendantCareServices_PartHeader_AssessedSubtotal
    {
      get
      {
        return "@AssessedSubtotal";
      }
    }

    public string Costs_Assessed_Part_ACSItemID
    {
      get
      {
        return "@ACSItemID";
      }
    }

    public string Costs_Assessed_Part_WeeklyHours
    {
      get
      {
        return "hcai:WeeklyHours";
      }
    }

    public string Costs_Assessed_Part_MonthlyHours
    {
      get
      {
        return "hcai:MonthlyHours";
      }
    }

    public string Costs_Assessed_Part_Benefit
    {
      get
      {
        return "hcai:Benefit";
      }
    }
  }
}
