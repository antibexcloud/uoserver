﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF23DataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.21.6787.27110, Culture=neutral, PublicKeyToken=null
// MVID: 1E9D9237-0987-4F64-A03A-6992C1631BC9
// Assembly location: C:\uoemrmvc\Libs\HcaiPms\3.21.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF23DataExtractResponseKeys : DataExtractResponseKeys
  {
    public string PMSFields_PMSSoftware
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PMSFields/hcai:PMSSoftware";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PMSFields/hcai:PMSVersion";
      }
    }

    public string Insurer_IBCInsurerName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:InsurerName";
      }
    }

    public string Facility_FacilityName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:FacilityName";
      }
    }

    public string OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:ApplicantSignature/hcai:IsApplicantSignatureWaivedByInsurer";
      }
    }

    public string OCF23_ApplicantSignature_IsApplicantSignatureOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:ApplicantSignature/hcai:IsApplicantSignatureOnFile";
      }
    }

    public string OCF23_ApplicantSignature_SigningApplicant_FirstName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:ApplicantSignature/hcai:SigningApplicant/hcai:FirstName";
      }
    }

    public string OCF23_ApplicantSignature_SigningApplicant_LastName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:ApplicantSignature/hcai:SigningApplicant/hcai:LastName";
      }
    }

    public string OCF23_ApplicantSignature_SigningDate
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:ApplicantSignature/hcai:SigningDate";
      }
    }

    public string OCF23_InsurerSignature_ApplicantSignatureWaived
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ApplicantSignatureWaived";
      }
    }

    public string OCF23_InsurerSignature_ResponseForOtherGoodsAndServices
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ResponseForOtherGoodsAndServices";
      }
    }

    public string OCF23_InsurerSignature_PolicyInForceConfirmation
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:PolicyInForceConfirmation";
      }
    }

    public string OCF23_OtherGoodsAndServices_GoodsAndServicesComments
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:OtherGoodsAndServices/hcai:GoodsAndServicesComments";
      }
    }

    public string OCF23_OtherGoodsAndServices_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:OtherGoodsAndServices/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF23_PAFPreApproveServices_PAF_PAFType
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:PAF/hcai:PAFType";
      }
    }

    public string OCF23_PAFPreApproveServices_PAF_EstimatedFee
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:PAF/hcai:EstimatedFee";
      }
    }

    public string OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:SupplementaryGoodsAndServices/hcai:PMSGSKey";
      }
    }

    public string OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:SupplementaryGoodsAndServices/hcai:Description";
      }
    }

    public string OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:SupplementaryGoodsAndServices/hcai:EstimatedFee";
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:Response";
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:Explanation";
      }
    }

    public string OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PriorAndConcurrentConditions/hcai:EmployedAtTimeOfAccident";
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:InvestigationOrTreatment/hcai:Response";
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PriorAndConcurrentConditions/hcai:PriorCondition/hcai:InvestigationOrTreatment/hcai:Explanation";
      }
    }

    public string OCF23_BarriersToRecovery_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:BarriersToRecovery/hcai:Response";
      }
    }

    public string OCF23_BarriersToRecovery_Explanation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:BarriersToRecovery/hcai:Explanation";
      }
    }

    public string OCF23_DirectPaymentAssignment_Response
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:DirectPaymentAssignment/hcai:Response";
      }
    }

    public string OCF23_HealthPractitioner_ProviderFirstName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:ProviderFirstName";
      }
    }

    public string OCF23_HealthPractitioner_ProviderLastName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:ProviderLastName";
      }
    }

    public string OCF23_HealthPractitioner_FacilityName
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:FacilityName";
      }
    }

    public string OCF23_HealthPractitioner_AISIFacilityNumber
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:AISIFacilityNumber";
      }
    }

    public string OCF23_HealthPractitioner_Address1
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:Address1";
      }
    }

    public string OCF23_HealthPractitioner_Address2
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:Address2";
      }
    }

    public string OCF23_HealthPractitioner_City
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:City";
      }
    }

    public string OCF23_HealthPractitioner_Province
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:Province";
      }
    }

    public string OCF23_HealthPractitioner_PostalCode
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:PostalCode";
      }
    }

    public string OCF23_HealthPractitioner_TelephoneExtension
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:TelephoneExtension";
      }
    }

    public string OCF23_HealthPractitioner_TelephoneNumber
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:TelephoneNumber";
      }
    }

    public string OCF23_HealthPractitioner_FaxNumber
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:FaxNumber";
      }
    }

    public string OCF23_HealthPractitioner_Email
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:Email";
      }
    }

    public string OCF23_HealthPractitioner_FacilityRegistryID
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:FacilityRegistryID";
      }
    }

    public string OCF23_HealthPractitioner_ProviderRegistryID
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:ProviderRegistryID";
      }
    }

    public string OCF23_HealthPractitioner_Occupation
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:Occupation";
      }
    }

    public string OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:ConflictOfInterest/hcai:ConflictExists";
      }
    }

    public string OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:ConflictOfInterest/hcai:ConflictDetails";
      }
    }

    public string OCF23_HealthPractitioner_IsSignatureOnFile
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:IsSignatureOnFile";
      }
    }

    public string OCF23_HealthPractitioner_DateSigned
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:DateSigned";
      }
    }

    public string OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:HealthPractitioner/hcai:IsFirstInitiatingHealthPractitioner";
      }
    }

    public string OCF23_OtherInsurance_MOHAvailable
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='PUBLICPOL']]/hl7:id/@extension";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_ID
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:author/hl7:carrierRole/hl7:id/@assigningAuthorityName";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:id/@extension";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string OCF23_OtherInsurance_IsThereOtherInsuranceCoverage
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='PUBLICPOL']]";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_ID
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_Name
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:author/hl7:carrierRole/hl7:id/@assigningAuthorityName";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:id/@extension";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string OCF23_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return "/*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "/*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed
    {
      get
      {
        return "/*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved
    {
      get
      {
        return "/*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerTotals_SubTotalPreApproved_Proposed
    {
      get
      {
        return "/*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApprovedTotal";
      }
    }

    public string OCF23_InsurerTotals_SubTotalPreApproved_Approved
    {
      get
      {
        return "/*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PreApproved']]/hl7:netAmt/@value";
      }
    }

    public string NameOfAdjusterOnPDF
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:NameOfAdjusterOnPDF";
      }
    }
  }
}
