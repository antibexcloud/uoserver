﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF9DataExtractResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF9DataExtractResponseLineItemKeys
  {
    public string OCF9_GoodsAndServices_Items_Item_ReferenceNumber
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:id/@extension";
      }
    }

    public string OCF9_GoodsAndServices_Items_Item_InterestPayable
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:netAmt/@value";
      }
    }

    public string OCF9_GoodsAndServices_Items_Item_Code
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:code/@code";
      }
    }
  }
}
