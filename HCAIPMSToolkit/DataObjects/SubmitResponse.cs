﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.SubmitResponse
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal class SubmitResponse : BaseDataItem
  {
    public SubmitResponse()
    {
      this.m_data = new Hashtable();
    }

    public override bool addLineItem(IDataItem lineItem)
    {
      return false;
    }

    public override IDataItem newLineItem(LineItemType lineItemType)
    {
      return (IDataItem) null;
    }

    public override IList getList(LineItemType lineItemType)
    {
      return (IList) null;
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      return false;
    }
  }
}
