﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.GS21BLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class GS21BLineItemKeys
  {
    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Code
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Code);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost);
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Description
    {
      get
      {
        return nameof (OCF21B_ReimbursableGoodsAndServices_Items_Item_Description);
      }
    }
  }
}
