﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.DataExtract
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class DataExtract : XmlDataItem
  {
    protected override string Translate(string key, string value)
    {
      DataExtractResponseKeys extractResponseKeys = new DataExtractResponseKeys();
      if (key == new OCF21BDataExtractResponseKeys().OCF21B_Payee_FacilityIsPayee || key == new OCF21CDataExtractResponseKeys().OCF21C_Payee_FacilityIsPayee)
        return value == "PROV" ? "Yes" : "No";
      if (key == new OCF21CDataExtractResponseKeys().OCF21C_InsurerSignature_ClaimFormReceivedDate || key == new OCF21BDataExtractResponseKeys().OCF21B_InsurerSignature_ClaimFormReceivedDate || (key == extractResponseKeys.Attachments_Received_Date || key == extractResponseKeys.Claimant_Date_Of_Birth) || (key == extractResponseKeys.Applicant_DateOfBirth || key == extractResponseKeys.Header_DateOfAccident || (key == extractResponseKeys.Attachments_Received_Date || key == new OCF9DataExtractResponseKeys().OCF9_Header_DateRevised)))
        return ConvertUtil.convertNeCSTDateToNative(value);
      if (key == extractResponseKeys.Insurer_PolicyHolder_IsSameAsApplicant)
        return ConvertUtil.convertNeCSTPolicyHolderToNative(value);
      if (key == new OCF23DataExtractResponseKeys().OCF23_OtherInsurance_IsThereOtherInsuranceCoverage)
        return value != string.Empty ? "Yes" : "No";
      if (!(key == extractResponseKeys.Archival_Status))
        return base.Translate(key, value);
      return !string.IsNullOrEmpty(value) && value == "2" ? "Archived" : "Not Archived";
    }
  }
}
