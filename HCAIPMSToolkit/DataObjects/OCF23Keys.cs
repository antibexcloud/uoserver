﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF23Keys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF23Keys : DocumentKeys
  {
    public string OCF23_OtherInsurance_IsThereOtherInsuranceCoverage
    {
      get
      {
        return nameof (OCF23_OtherInsurance_IsThereOtherInsuranceCoverage);
      }
    }

    public string OCF23_OtherInsurance_MOHAvailable
    {
      get
      {
        return nameof (OCF23_OtherInsurance_MOHAvailable);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_ID
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer1_ID);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_Name
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer1_Name);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_ID
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer2_ID);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_Name
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer2_Name);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
      }
    }

    public string OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName
    {
      get
      {
        return nameof (OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
      }
    }

    public string OCF23_HealthPractitioner_ProviderFirstName
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_ProviderFirstName);
      }
    }

    public string OCF23_HealthPractitioner_ProviderLastName
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_ProviderLastName);
      }
    }

    public string OCF23_HealthPractitioner_FacilityName
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_FacilityName);
      }
    }

    public string OCF23_HealthPractitioner_AISIFacilityNumber
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_AISIFacilityNumber);
      }
    }

    public string OCF23_HealthPractitioner_Address1
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_Address1);
      }
    }

    public string OCF23_HealthPractitioner_Address2
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_Address2);
      }
    }

    public string OCF23_HealthPractitioner_City
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_City);
      }
    }

    public string OCF23_HealthPractitioner_Province
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_Province);
      }
    }

    public string OCF23_HealthPractitioner_PostalCode
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_PostalCode);
      }
    }

    public string OCF23_HealthPractitioner_TelephoneExtension
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_TelephoneExtension);
      }
    }

    public string OCF23_HealthPractitioner_TelephoneNumber
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_TelephoneNumber);
      }
    }

    public string OCF23_HealthPractitioner_FaxNumber
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_FaxNumber);
      }
    }

    public string OCF23_HealthPractitioner_Email
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_Email);
      }
    }

    public string OCF23_HealthPractitioner_FacilityRegistryID
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_FacilityRegistryID);
      }
    }

    public string OCF23_HealthPractitioner_ProviderRegistryID
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_ProviderRegistryID);
      }
    }

    public string OCF23_HealthPractitioner_Occupation
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_Occupation);
      }
    }

    public string OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists);
      }
    }

    public string OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails);
      }
    }

    public string OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner);
      }
    }

    public string OCF23_HealthPractitioner_IsSignatureOnFile
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_IsSignatureOnFile);
      }
    }

    public string OCF23_HealthPractitioner_DateSigned
    {
      get
      {
        return nameof (OCF23_HealthPractitioner_DateSigned);
      }
    }

    public string OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident
    {
      get
      {
        return nameof (OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident);
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_Response
    {
      get
      {
        return nameof (OCF23_PriorAndConcurrentConditions_PriorCondition_Response);
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation
    {
      get
      {
        return nameof (OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation);
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response
    {
      get
      {
        return nameof (OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response);
      }
    }

    public string OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation
    {
      get
      {
        return nameof (OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);
      }
    }

    public string OCF23_BarriersToRecovery_Response
    {
      get
      {
        return nameof (OCF23_BarriersToRecovery_Response);
      }
    }

    public string OCF23_BarriersToRecovery_Explanation
    {
      get
      {
        return nameof (OCF23_BarriersToRecovery_Explanation);
      }
    }

    public string OCF23_DirectPaymentAssignment_Response
    {
      get
      {
        return nameof (OCF23_DirectPaymentAssignment_Response);
      }
    }

    public string OCF23_PAFPreApproveServices_PAF_PAFType
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_PAF_PAFType);
      }
    }

    public string OCF23_PAFPreApproveServices_PAF_EstimatedFee
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_PAF_EstimatedFee);
      }
    }

    public string OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description);
      }
    }

    public string OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee);
      }
    }

    public string OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey
    {
      get
      {
        return nameof (OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey);
      }
    }

    public string OCF23_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return nameof (OCF23_InsurerTotals_AutoInsurerTotal_Proposed);
      }
    }

    public string OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed
    {
      get
      {
        return nameof (OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed);
      }
    }

    public string OCF23_InsurerTotals_SubTotalPreApproved_Proposed
    {
      get
      {
        return nameof (OCF23_InsurerTotals_SubTotalPreApproved_Proposed);
      }
    }

    public string OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer
    {
      get
      {
        return nameof (OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer);
      }
    }

    public string OCF23_ApplicantSignature_IsApplicantSignatureOnFile
    {
      get
      {
        return nameof (OCF23_ApplicantSignature_IsApplicantSignatureOnFile);
      }
    }

    public string OCF23_ApplicantSignature_SigningApplicant_FirstName
    {
      get
      {
        return nameof (OCF23_ApplicantSignature_SigningApplicant_FirstName);
      }
    }

    public string OCF23_ApplicantSignature_SigningApplicant_LastName
    {
      get
      {
        return nameof (OCF23_ApplicantSignature_SigningApplicant_LastName);
      }
    }

    public string OCF23_ApplicantSignature_SigningDate
    {
      get
      {
        return nameof (OCF23_ApplicantSignature_SigningDate);
      }
    }

    public string OCF23_OtherGoodsAndServices_GoodsAndServicesComments
    {
      get
      {
        return nameof (OCF23_OtherGoodsAndServices_GoodsAndServicesComments);
      }
    }
  }
}
