﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF21BDataExtractResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF21BDataExtractResponseLineItemKeys : DataExtractResponseLineItemKeys
  {
    public string OCF21B_InjuriesAndSequelae_Injury_Code
    {
      get
      {
        return this.InjuryLineItem_Code;
      }
    }

    public string OCF21B_InjuriesAndSequelae_Injury_Description
    {
      get
      {
        return this.InjuryLineItem_Description;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber
    {
      get
      {
        return this.Items_Item_ReferenceNumber;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Code
    {
      get
      {
        return this.Items_Item_Code;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Description
    {
      get
      {
        return this.Items_Item_Description;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute
    {
      get
      {
        return this.Items_Item_Attribute;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID
    {
      get
      {
        return this.Items_Item_ProviderReference_ProviderRegistryID;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation
    {
      get
      {
        return this.Items_Item_ProviderReference_Occupation;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity
    {
      get
      {
        return this.Items_Item_Quantity;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure
    {
      get
      {
        return this.Items_Item_Measure;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService
    {
      get
      {
        return this.Items_Item_DateOfService;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST
    {
      get
      {
        return this.Items_Item_Estimated_GST;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST
    {
      get
      {
        return this.Items_Item_Estimated_PST;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost
    {
      get
      {
        return this.Items_Item_Estimated_LineCost;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey
    {
      get
      {
        return this.Items_Item_Estimated_PMSGSKey;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey
    {
      get
      {
        return this.Items_Item_Approved_PMSGSKey;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST
    {
      get
      {
        return this.Items_Item_Approved_GST;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST
    {
      get
      {
        return this.Items_Item_Approved_PST;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost
    {
      get
      {
        return this.Items_Item_Approved_LineCost;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return this.Items_Item_Approved_ReasonCode;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return this.Items_Item_Approved_ReasonCodeDesc;
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return this.Items_Item_Approved_OtherReasonCodeDesc;
      }
    }
  }
}
