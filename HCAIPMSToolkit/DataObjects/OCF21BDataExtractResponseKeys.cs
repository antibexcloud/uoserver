﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF21BDataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.21.6787.27110, Culture=neutral, PublicKeyToken=null
// MVID: 1E9D9237-0987-4F64-A03A-6992C1631BC9
// Assembly location: C:\uoemrmvc\Libs\HcaiPms\3.21.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF21BDataExtractResponseKeys : DataExtractResponseKeys
  {
    public string PMSFields_PMSSoftware
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PMSFields/hcai:PMSSoftware";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PMSFields/hcai:PMSVersion";
      }
    }

    public string Insurer_IBCInsurerName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:InsurerName";
      }
    }

    public string Facility_FacilityName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:FacilityName";
      }
    }

    public string OCF21B_HCAI_Plan_Document_Number
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:acknowledgement/hl7:acknowledgementDetail[child::hl7:location='HCAI_Plan_Document_Number']/hl7:text";
      }
    }

    public string OCF21B_InvoiceInformation_InvoiceNumber
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:InvoiceInformation/hcai:InvoiceNumber";
      }
    }

    public string OCF21B_InvoiceInformation_FirstInvoice
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:InvoiceInformation/hcai:FirstInvoice";
      }
    }

    public string OCF21B_InvoiceInformation_LastInvoice
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:InvoiceInformation/hcai:LastInvoice";
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_Type
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PreviouslyApprovedGoodsAndServices/hcai:Type";
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PreviouslyApprovedGoodsAndServices/hcai:PlanDate";
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PreviouslyApprovedGoodsAndServices/hcai:PlanNumber";
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PreviouslyApprovedGoodsAndServices/hcai:AmountApproved";
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:PreviouslyApprovedGoodsAndServices/hcai:PreviouslyBilled";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:OtherInsuranceAmounts/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:ReimbursableGoodsAndServices/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:OtherInsuranceAmounts/hcai:OtherServiceTypeForDebits";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_OtherServiceType
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:OtherInsuranceAmounts/hcai:OtherServiceType";
      }
    }

    public string OCF21B_AccountActivity_PriorBalance
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:AccountActivity/hcai:PriorBalance";
      }
    }

    public string OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:AccountActivity/hcai:PaymentReceivedFromAutoInsurer";
      }
    }

    public string OCF21B_AccountActivity_OverdueAmount
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:AccountActivity/hcai:OverdueAmount";
      }
    }

    public string OCF21B_OtherInformation
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:OtherInformation";
      }
    }

    public string OCF21B_Payee_FacilityRegistryID
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:FacilityRegistryID";
      }
    }

    public string OCF21B_Payee_FacilityIsPayee
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:credit/hl7:account/hl7:holder/hl7:payeeRole/@classCode";
      }
    }

    public string OCF21B_InsurerSignature_ClaimFormReceived
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'ClaimFormReceived']]/@code";
      }
    }

    public string OCF21B_InsurerSignature_ClaimFormReceivedDate
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'ClaimFormReceivedDate']]/@code";
      }
    }

    public string OCF21B_Payee_MakeChequePayableTo
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:MakeChequePayableTo";
      }
    }

    public string OCF21B_Payee_ConflictOfInterests_ConflictExists
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ConflictOfInterest/hcai:ConflictExists";
      }
    }

    public string OCF21B_Payee_ConflictOfInterests_ConflictDetails
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ConflictOfInterest/hcai:ConflictDetails";
      }
    }

    public string OCF21B_Payee_Address1
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:Address1";
      }
    }

    public string OCF21B_Payee_Address2
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:Address2";
      }
    }

    public string OCF21B_Payee_City
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:City";
      }
    }

    public string OCF21B_Payee_Province
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:Province";
      }
    }

    public string OCF21B_Payee_PostalCode
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:PostalCode";
      }
    }

    public string OCF21B_Payee_SameServiceAddress
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:SameServiceAddress";
      }
    }

    public string OCF21B_Payee_ServiceAddress1
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ServiceAddress/hcai:Address1";
      }
    }

    public string OCF21B_Payee_ServiceAddress2
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ServiceAddress/hcai:Address2";
      }
    }

    public string OCF21B_Payee_ServiceCity
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ServiceAddress/hcai:City";
      }
    }

    public string OCF21B_Payee_ServiceProvince
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ServiceAddress/hcai:Province";
      }
    }

    public string OCF21B_Payee_ServicePostalCode
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:ServiceAddress/hcai:PostalCode";
      }
    }

    public string OCF21B_Payee_TelephoneNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:TelephoneNumber";
      }
    }

    public string OCF21B_Payee_FaxNumber
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:FaxNumber";
      }
    }

    public string OCF21B_Payee_Email
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:Email";
      }
    }

    public string OCF21B_Payee_FirstName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:FirstName";
      }
    }

    public string OCF21B_Payee_LastName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:LastName";
      }
    }

    public string OCF21B_Payee_Number
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:Payee/hcai:Number";
      }
    }

    public string OCF21B_OtherInsurance_MOHAvailable
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='PUBLICPOL']]/hl7:id/@extension";
      }
    }

    public string OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:code[@code='PUBLICPOL']]";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:author/hl7:carrierRole/hl7:id/@assigningAuthorityName";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:id/@extension";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:beneficiary/hl7:coveredPartyAsPatient/hl7:id/@extension";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:author/hl7:carrierRole/hl7:id/@assigningAuthorityName";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:id/@extension";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:given";
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:coverage/hl7:policyOrAccount[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:holder/hl7:policyHolder/hl7:policyHolderPerson/hl7:name/hl7:family";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Chiropractic']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Physiotherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='MassageTherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherService']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Chiropractic']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Physiotherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='MassageTherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherService']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Chiropractic']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Physiotherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='MassageTherapy']]/hl7:netAmt/@value ";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherService']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_IsAmountRefused
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:OtherInsuranceAmounts/hcai:IsAmountRefusedByOtherInsurance";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Chiropractic
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Chiropractic']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Physiotherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='MassageTherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_OtherService
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherService']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_MOH_Approved
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Chiropractic']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Physiotherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='MassageTherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_OtherService
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherService']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Chiropractic']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='Physiotherapy']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='MassageTherapy']]/hl7:netAmt/@value ";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_OtherService
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherService']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_MOH_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_OtherInsurers_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_OtherInsurers_Approved
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_SubTotal_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_SubTotal_Approved
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string ApprovedByOnPDF
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[1]/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:ApprovedByOnPDF";
      }
    }
  }
}
