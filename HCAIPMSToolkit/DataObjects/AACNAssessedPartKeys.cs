﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNAssessedPartKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNAssessedPartKeys
  {
    public string Costs_Assessed_Part_ACSItemID
    {
      get
      {
        return nameof (Costs_Assessed_Part_ACSItemID);
      }
    }

    public string Costs_Assessed_Part_HourlyRate
    {
      get
      {
        return nameof (Costs_Assessed_Part_HourlyRate);
      }
    }
  }
}
