﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF21BKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF21BKeys : DocumentKeys
  {
    public string OCF21B_InvoiceInformation_InvoiceNumber
    {
      get
      {
        return nameof (OCF21B_InvoiceInformation_InvoiceNumber);
      }
    }

    public string OCF21B_InvoiceInformation_FirstInvoice
    {
      get
      {
        return nameof (OCF21B_InvoiceInformation_FirstInvoice);
      }
    }

    public string OCF21B_InvoiceInformation_LastInvoice
    {
      get
      {
        return nameof (OCF21B_InvoiceInformation_LastInvoice);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_Type
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_Type);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved);
      }
    }

    public string OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled
    {
      get
      {
        return nameof (OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled);
      }
    }

    public string OCF21B_Payee_FacilityRegistryID
    {
      get
      {
        return nameof (OCF21B_Payee_FacilityRegistryID);
      }
    }

    public string OCF21B_Payee_MakeChequePayableTo
    {
      get
      {
        return nameof (OCF21B_Payee_MakeChequePayableTo);
      }
    }

    public string OCF21B_Payee_ConflictOfInterests_ConflictExists
    {
      get
      {
        return nameof (OCF21B_Payee_ConflictOfInterests_ConflictExists);
      }
    }

    public string OCF21B_Payee_ConflictOfInterests_ConflictDetails
    {
      get
      {
        return nameof (OCF21B_Payee_ConflictOfInterests_ConflictDetails);
      }
    }

    public string OCF21B_Payee_Address1
    {
      get
      {
        return nameof (OCF21B_Payee_Address1);
      }
    }

    public string OCF21B_Payee_Address2
    {
      get
      {
        return nameof (OCF21B_Payee_Address2);
      }
    }

    public string OCF21B_Payee_City
    {
      get
      {
        return nameof (OCF21B_Payee_City);
      }
    }

    public string OCF21B_Payee_Province
    {
      get
      {
        return nameof (OCF21B_Payee_Province);
      }
    }

    public string OCF21B_Payee_PostalCode
    {
      get
      {
        return nameof (OCF21B_Payee_PostalCode);
      }
    }

    public string OCF21B_Payee_TelephoneNumber
    {
      get
      {
        return nameof (OCF21B_Payee_TelephoneNumber);
      }
    }

    public string OCF21B_Payee_FaxNumber
    {
      get
      {
        return nameof (OCF21B_Payee_FaxNumber);
      }
    }

    public string OCF21B_Payee_Email
    {
      get
      {
        return nameof (OCF21B_Payee_Email);
      }
    }

    public string OCF21B_Payee_FirstName
    {
      get
      {
        return nameof (OCF21B_Payee_FirstName);
      }
    }

    public string OCF21B_Payee_LastName
    {
      get
      {
        return nameof (OCF21B_Payee_LastName);
      }
    }

    public string OCF21B_Payee_Number
    {
      get
      {
        return nameof (OCF21B_Payee_Number);
      }
    }

    public string OCF21B_Payee_FacilityIsPayee
    {
      get
      {
        return nameof (OCF21B_Payee_FacilityIsPayee);
      }
    }

    public string OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage);
      }
    }

    public string OCF21B_OtherInsurance_MOHAvailable
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_MOHAvailable);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
      }
    }

    public string OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName
    {
      get
      {
        return nameof (OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Chiropractic
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOH_Chiropractic);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_OtherService
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOH_OtherService);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_OtherService
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1_OtherService);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_OtherService
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2_OtherService);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_OtherServiceType
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_OtherServiceType);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType);
      }
    }

    public string OCF21B_OtherInsuranceAmounts_IsAmountRefused
    {
      get
      {
        return nameof (OCF21B_OtherInsuranceAmounts_IsAmountRefused);
      }
    }

    public string OCF21B_AccountActivity_PriorBalance
    {
      get
      {
        return nameof (OCF21B_AccountActivity_PriorBalance);
      }
    }

    public string OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer
    {
      get
      {
        return nameof (OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer);
      }
    }

    public string OCF21B_AccountActivity_OverdueAmount
    {
      get
      {
        return nameof (OCF21B_AccountActivity_OverdueAmount);
      }
    }

    public string OCF21B_InsurerTotals_MOH_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_MOH_Proposed);
      }
    }

    public string OCF21B_InsurerTotals_OtherInsurers_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_OtherInsurers_Proposed);
      }
    }

    public string OCF21B_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_AutoInsurerTotal_Proposed);
      }
    }

    public string OCF21B_InsurerTotals_GST_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_GST_Proposed);
      }
    }

    public string OCF21B_InsurerTotals_PST_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_PST_Proposed);
      }
    }

    public string OCF21B_InsurerTotals_SubTotal_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_SubTotal_Proposed);
      }
    }

    public string OCF21B_InsurerTotals_Interest_Proposed
    {
      get
      {
        return nameof (OCF21B_InsurerTotals_Interest_Proposed);
      }
    }

    public string OCF21B_OtherInformation
    {
      get
      {
        return nameof (OCF21B_OtherInformation);
      }
    }

    public string OCF21B_HCAI_Plan_Document_Number
    {
      get
      {
        return nameof (OCF21B_HCAI_Plan_Document_Number);
      }
    }
  }
}
