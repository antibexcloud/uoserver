﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.BranchLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class BranchLineItemKeys
  {
    public string Branch_ID
    {
      get
      {
        return "hcai:InsurerBranchID";
      }
    }

    public string Branch_Name
    {
      get
      {
        return "hcai:BranchName";
      }
    }

    public string Branch_Status
    {
      get
      {
        return "hcai:BranchStatus";
      }
    }
  }
}
