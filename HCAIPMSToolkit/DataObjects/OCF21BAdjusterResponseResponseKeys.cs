﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF21BAdjusterResponseResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF21BAdjusterResponseResponseKeys : AdjusterResponseResponseKeys
  {
    public string OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:OtherInsuranceAmounts/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21B/hcai:ReimbursableGoodsAndServices/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.4']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.5']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.6']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.2']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.3']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_MOH_Approved
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='MOHAmounts']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_OtherInsurers_Approved
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsuranceAmounts']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_SubTotal_Approved
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:netAmt/@value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='INTEREST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF21B_InsurerSignature_ClaimFormReceived
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'ClaimFormReceived']]/@code";
      }
    }

    public string OCF21B_InsurerSignature_ClaimFormReceivedDate
    {
      get
      {
        return "/hl7:FICR_IN610304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementDetail/hl7:code/hl7:qualifier/hl7:value[preceding-sibling::hl7:name[@code = 'ClaimFormReceivedDate']]/@code";
      }
    }
  }
}
