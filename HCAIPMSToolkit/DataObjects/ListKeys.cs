﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.ListKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class ListKeys
  {
    public string DE_OCF21B_ReimbursableGoodsAndServices_Axis
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF21B_Approved_ReimbursableGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF21C_RenderedGoodsAndServices_Axis
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='RenderedGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF21C_PAFReimbursableFees_Axis
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='PAFReimbursableFees']]/hl7:component";
      }
    }

    public string DE_OCF21C_OtherReimbursableGoodsAndServices_Axis
    {
      get
      {
        return "hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='OtherReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF21C_Approved_PAFReimbursableFees_Axis
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PAFReimbursableFees']]/hl7:component";
      }
    }

    public string DE_OCF21C_Approved_OtherReimbursableGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF21C_InjuriesAndSequelae_Axis
    {
      get
      {
        return "/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF21C/hcai:InjuriesAndSequelae/hcai:Injury";
      }
    }

    public string DE_OCF22_Estimated_ProposedGoodsAndServices_Axis
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF22_Approved_ProposedGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF23_Estimated_OtherGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF23_Approved_OtherGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF23_PAFServices_OtherPreApprovedServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:OtherPreApprovedServices/hcai:Item";
      }
    }

    public string DE_OCF18_ProposedGoodsAndServices_Session_GS_Axis
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF18_ProposedGoodsAndServices_Session_GS_Data_Axis
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='SessionData']]/hl7:component";
      }
    }

    public string DE_OCF18_ProposedGoodsAndServices_NonSession_GS_Axis
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='NonSessionGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF18_Approved_SessionGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component";
      }
    }

    public string DE_OCF18_Approved_NonSessionGoodsAndServices_Axis
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='NonSessionGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_GS21BLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_GS18LineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_GS22LineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_GS9LineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InterestPayable']]/hl7:component";
      }
    }

    public string AR_OtherGSLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_OtherReimbursableLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherReimbursableGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_PAFLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PreApproved']]/hl7:component";
      }
    }

    public string AR_PAFReimbursableLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PAFReimbursableFees']]/hl7:component";
      }
    }

    public string AR_NonSessionLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='NonSessionGoodsAndServices']]/hl7:component";
      }
    }

    public string AR_SessionHeaderLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component";
      }
    }

    public string InjuryLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:InjuriesAndSequelae/hcai:Injury";
      }
    }

    public string RenderedGSLineItem
    {
      get
      {
        return "RenderedGSLineItemNotYetImplemented";
      }
    }

    public string ActivityLineItem
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:adjudResultsGroup/hl7:reference";
      }
    }

    public string ProviderLineItem
    {
      get
      {
        return "hcai:Provider";
      }
    }

    public string RegistrationLineItem
    {
      get
      {
        return "hcai:Registration";
      }
    }

    public string InsurerLineItem
    {
      get
      {
        return "/hcai:InsurerList/hcai:Insurer";
      }
    }

    public string FacilityLineItem
    {
      get
      {
        return "hcai:Facility";
      }
    }

    public string BranchLineItem
    {
      get
      {
        return "hcai:Branch";
      }
    }

    public string FacilityLicenseLineItem
    {
      get
      {
        return "hcai:FacilityLicense";
      }
    }

    public string AttendantCareServices_PartHeader
    {
      get
      {
        return "/hcai:*/hcai:AACN/hcai:AttendantCareServices/hcai:PartHeader";
      }
    }

    public string AttendantCareServices_Item
    {
      get
      {
        return "/hcai:*/hcai:AACN/hcai:AttendantCareServices/hcai:Item";
      }
    }

    public string Costs_Assessed_Part
    {
      get
      {
        return "/hcai:*/hcai:AACN/hcai:Costs/hcai:Assessed/hcai:Part";
      }
    }

    public string Costs_Approved_Part
    {
      get
      {
        return "/hcai:*/hcai:AACN/hcai:Costs/hcai:Approved/hcai:Part";
      }
    }
  }
}
