﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.ProviderUpdateKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class ProviderUpdateKeys
  {
    public string HCAI_Facility_Registry_ID
    {
      get
      {
        return "hcai:FacilityRegistryID";
      }
    }

    public string HCAI_Provider_Registry_ID
    {
      get
      {
        return "hcai:ProviderRegistryID";
      }
    }

    public string Provider_End_Date
    {
      get
      {
        return "hcai:EndDate";
      }
    }

    public string Provider_Hourly_Rate
    {
      get
      {
        return "hcai:HourlyRate";
      }
    }
  }
}
