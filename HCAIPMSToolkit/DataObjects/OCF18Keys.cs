﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF18Keys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF18Keys : DocumentKeys
  {
    public string OCF18_Header_PlanNumber
    {
      get
      {
        return nameof (OCF18_Header_PlanNumber);
      }
    }

    public string OCF18_OtherInsurance_IsThereOtherInsuranceCoverage
    {
      get
      {
        return nameof (OCF18_OtherInsurance_IsThereOtherInsuranceCoverage);
      }
    }

    public string OCF18_OtherInsurance_MOHAvailable
    {
      get
      {
        return nameof (OCF18_OtherInsurance_MOHAvailable);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_ID
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer1_ID);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_Name
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer1_Name);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_ID
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer2_ID);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_Name
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer2_Name);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
      }
    }

    public string OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName
    {
      get
      {
        return nameof (OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);
      }
    }

    public string OCF18_HealthPractitioner_FacilityRegistryID
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_FacilityRegistryID);
      }
    }

    public string OCF18_HealthPractitioner_ProviderRegistryID
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_ProviderRegistryID);
      }
    }

    public string OCF18_HealthPractitioner_Occupation
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_Occupation);
      }
    }

    public string OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists);
      }
    }

    public string OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails);
      }
    }

    public string OCF18_HealthPractitioner_IsSignatureOnFile
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_IsSignatureOnFile);
      }
    }

    public string OCF18_HealthPractitioner_DateSigned
    {
      get
      {
        return nameof (OCF18_HealthPractitioner_DateSigned);
      }
    }

    public string OCF18_IsOtherHealthPractitioner
    {
      get
      {
        return nameof (OCF18_IsOtherHealthPractitioner);
      }
    }

    public string OCF18_OtherHealthPractitioner_Name_FirstName
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Name_FirstName);
      }
    }

    public string OCF18_OtherHealthPractitioner_Name_LastName
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Name_LastName);
      }
    }

    public string OCF18_OtherHealthPractitioner_FacilityName
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_FacilityName);
      }
    }

    public string OCF18_OtherHealthPractitioner_AISIFacilityNumber
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_AISIFacilityNumber);
      }
    }

    public string OCF18_OtherHealthPractitioner_FacilityRegistryID
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_FacilityRegistryID);
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_StreetAddress1
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Address_StreetAddress1);
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_StreetAddress2
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Address_StreetAddress2);
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_City
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Address_City);
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_Province
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Address_Province);
      }
    }

    public string OCF18_OtherHealthPractitioner_Address_PostalCode
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Address_PostalCode);
      }
    }

    public string OCF18_OtherHealthPractitioner_TelephoneNumber
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_TelephoneNumber);
      }
    }

    public string OCF18_OtherHealthPractitioner_TelephoneExtension
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_TelephoneExtension);
      }
    }

    public string OCF18_OtherHealthPractitioner_FaxNumber
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_FaxNumber);
      }
    }

    public string OCF18_OtherHealthPractitioner_Email
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Email);
      }
    }

    public string OCF18_OtherHealthPractitioner_ProviderRegistryNumber
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_ProviderRegistryNumber);
      }
    }

    public string OCF18_OtherHealthPractitioner_Occupation
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_Occupation);
      }
    }

    public string OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists);
      }
    }

    public string OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails);
      }
    }

    public string OCF18_OtherHealthPractitioner_IsSignatureOnFile
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_IsSignatureOnFile);
      }
    }

    public string OCF18_OtherHealthPractitioner_DateSigned
    {
      get
      {
        return nameof (OCF18_OtherHealthPractitioner_DateSigned);
      }
    }

    public string OCF18_IsRHPSameAsHealthPractitioner
    {
      get
      {
        return nameof (OCF18_IsRHPSameAsHealthPractitioner);
      }
    }

    public string OCF18_RegulatedHealthProfessional_FacilityRegistryID
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_FacilityRegistryID);
      }
    }

    public string OCF18_RegulatedHealthProfessional_ProviderRegistryID
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_ProviderRegistryID);
      }
    }

    public string OCF18_RegulatedHealthProfessional_Occupation
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_Occupation);
      }
    }

    public string OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists);
      }
    }

    public string OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails);
      }
    }

    public string OCF18_RegulatedHealthProfessional_IsSignatureOnFile
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_IsSignatureOnFile);
      }
    }

    public string OCF18_RegulatedHealthProfessional_DateSigned
    {
      get
      {
        return nameof (OCF18_RegulatedHealthProfessional_DateSigned);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_Response
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_PriorCondition_Response);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_IsPAF_Response
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_IsPAF_Response);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance);
      }
    }

    public string OCF18_PriorAndConcurrentConditions_IsPAF_Explanation
    {
      get
      {
        return nameof (OCF18_PriorAndConcurrentConditions_IsPAF_Explanation);
      }
    }

    public string OCF18_ActivityLimitations_ToEmployment_Response
    {
      get
      {
        return nameof (OCF18_ActivityLimitations_ToEmployment_Response);
      }
    }

    public string OCF18_ActivityLimitations_ToNormalLife_Response
    {
      get
      {
        return nameof (OCF18_ActivityLimitations_ToNormalLife_Response);
      }
    }

    public string OCF18_ActivityLimitations_ImpactOnAbilities
    {
      get
      {
        return nameof (OCF18_ActivityLimitations_ImpactOnAbilities);
      }
    }

    public string OCF18_ActivityLimitations_ModifiedEmployment_Response
    {
      get
      {
        return nameof (OCF18_ActivityLimitations_ModifiedEmployment_Response);
      }
    }

    public string OCF18_ActivityLimitations_ModifiedEmployment_Explanation
    {
      get
      {
        return nameof (OCF18_ActivityLimitations_ModifiedEmployment_Explanation);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_Other
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Goals_Other);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response);
      }
    }

    public string OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation
    {
      get
      {
        return nameof (OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation);
      }
    }

    public string OCF18_ProposedGoodsAndServices_TreatmentPlanDuration
    {
      get
      {
        return nameof (OCF18_ProposedGoodsAndServices_TreatmentPlanDuration);
      }
    }

    public string OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits
    {
      get
      {
        return nameof (OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits);
      }
    }

    public string OCF18_ProposedGoodsAndServices_GoodsAndServicesComments
    {
      get
      {
        return nameof (OCF18_ProposedGoodsAndServices_GoodsAndServicesComments);
      }
    }

    public string OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile
    {
      get
      {
        return nameof (OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile);
      }
    }

    public string OCF18_InsurerTotals_MOH_Proposed
    {
      get
      {
        return nameof (OCF18_InsurerTotals_MOH_Proposed);
      }
    }

    public string OCF18_InsurerTotals_OtherInsurers_Proposed
    {
      get
      {
        return nameof (OCF18_InsurerTotals_OtherInsurers_Proposed);
      }
    }

    public string OCF18_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return nameof (OCF18_InsurerTotals_AutoInsurerTotal_Proposed);
      }
    }

    public string OCF18_InsurerTotals_GST_Proposed
    {
      get
      {
        return nameof (OCF18_InsurerTotals_GST_Proposed);
      }
    }

    public string OCF18_InsurerTotals_PST_Proposed
    {
      get
      {
        return nameof (OCF18_InsurerTotals_PST_Proposed);
      }
    }

    public string OCF18_InsurerTotals_SubTotal_Proposed
    {
      get
      {
        return nameof (OCF18_InsurerTotals_SubTotal_Proposed);
      }
    }

    public string OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer
    {
      get
      {
        return nameof (OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer);
      }
    }

    public string OCF18_ApplicantSignature_IsApplicantSignatureOnFile
    {
      get
      {
        return nameof (OCF18_ApplicantSignature_IsApplicantSignatureOnFile);
      }
    }

    public string OCF18_ApplicantSignature_SigningApplicant_FirstName
    {
      get
      {
        return nameof (OCF18_ApplicantSignature_SigningApplicant_FirstName);
      }
    }

    public string OCF18_ApplicantSignature_SigningApplicant_LastName
    {
      get
      {
        return nameof (OCF18_ApplicantSignature_SigningApplicant_LastName);
      }
    }

    public string OCF18_ApplicantSignature_SigningDate
    {
      get
      {
        return nameof (OCF18_ApplicantSignature_SigningDate);
      }
    }
  }
}
