﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF22DataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF22DataExtractResponseKeys : DataExtractResponseKeys
  {
    public string PMSFields_PMSSoftware
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:PMSFields/hcai:PMSSoftware";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:PMSFields/hcai:PMSVersion";
      }
    }

    public string Insurer_IBCInsurerName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:InsurerName";
      }
    }

    public string Facility_FacilityName
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:FacilityName";
      }
    }

    public string OCF22_RegulatedHealthProfessional_ProviderFirstName
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:ProviderFirstName";
      }
    }

    public string OCF22_RegulatedHealthProfessional_ProviderLastName
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:ProviderLastName";
      }
    }

    public string OCF22_RegulatedHealthProfessional_FacilityName
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:FacilityName";
      }
    }

    public string OCF22_RegulatedHealthProfessional_AISIFacilityNumber
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:AISIFacilityNumber";
      }
    }

    public string OCF22_RegulatedHealthProfessional_Address1
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:Address1";
      }
    }

    public string OCF22_RegulatedHealthProfessional_Address2
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:Address2";
      }
    }

    public string OCF22_RegulatedHealthProfessional_City
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:City";
      }
    }

    public string OCF22_RegulatedHealthProfessional_Province
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:Province";
      }
    }

    public string OCF22_RegulatedHealthProfessional_PostalCode
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:PostalCode";
      }
    }

    public string OCF22_RegulatedHealthProfessional_TelephoneExtension
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:TelephoneExtension";
      }
    }

    public string OCF22_RegulatedHealthProfessional_TelephoneNumber
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:TelephoneNumber";
      }
    }

    public string OCF22_RegulatedHealthProfessional_FaxNumber
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:FaxNumber";
      }
    }

    public string OCF22_RegulatedHealthProfessional_FacilityRegistryID
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:FacilityRegistryID";
      }
    }

    public string OCF22_RegulatedHealthProfessional_ProviderRegistryID
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:ProviderRegistryID";
      }
    }

    public string OCF22_RegulatedHealthProfessional_Occupation
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:Occupation";
      }
    }

    public string OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:ConflictOfInterest/hcai:ConflictExists";
      }
    }

    public string OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:ConflictOfInterest/hcai:ConflictDetails";
      }
    }

    public string OCF22_RegulatedHealthProfessional_IsSignatureOnFile
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:IsSignatureOnFile";
      }
    }

    public string OCF22_RegulatedHealthProfessional_DateSigned
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:RegulatedHealthProfessional/hcai:DateSigned";
      }
    }

    public string OCF22_NatureOfAssessment_Response
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:NatureOfAssessment/hcai:Response";
      }
    }

    public string OCF22_ProvisionalClinicalInformation_ClinicalInformation_PresentComplaintsDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ProvisionalClinicalInformation/hcai:ClinicalInformation/hcai:PresentComplaintsDesc";
      }
    }

    public string OCF22_ProvisionalClinicalInformation_ClinicalInformation_AlreadyProvidedTreatment_Response
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ProvisionalClinicalInformation/hcai:ClinicalInformation/hcai:AlreadyProvidedTreatment/hcai:Response";
      }
    }

    public string OCF22_ProvisionalClinicalInformation_AssessmentInformation_Details
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ProvisionalClinicalInformation/hcai:AssessmentInformation/hcai:Details";
      }
    }

    public string OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_Response
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ProvisionalClinicalInformation/hcai:AssessmentInformation/hcai:PriorAssessment/hcai:Response";
      }
    }

    public string OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_AssessmentDate
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ProvisionalClinicalInformation/hcai:AssessmentInformation/hcai:PriorAssessment/hcai:AssessmentDate";
      }
    }

    public string OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ApplicantSignatureWaivedForOCF18";
      }
    }

    public string OCF22_ApplicantSignature_IsApplicantSignatureOnFile
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ApplicantSignature/hcai:IsApplicantSignatureOnFile";
      }
    }

    public string OCF22_ApplicantSignature_SigningApplicant_FirstName
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ApplicantSignature/hcai:SigningApplicant/hcai:FirstName";
      }
    }

    public string OCF22_ApplicantSignature_SigningApplicant_LastName
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ApplicantSignature/hcai:SigningApplicant/hcai:LastName";
      }
    }

    public string OCF22_ApplicantSignature_SigningDate
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF22/hcai:ApplicantSignature/hcai:SigningDate";
      }
    }

    public string OCF22_InsurerTotals_MOH_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='MOH']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:invoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_LineCost
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_GST_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_PST_Proposed
    {
      get
      {
        return "hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:invoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_SubTotal_Proposed
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_SubTotal_Approved
    {
      get
      {
        return "/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:netAmt/@value";
      }
    }
  }
}
