﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.ConvertUtil
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;

namespace PMSToolkit.DataObjects
{
  internal class ConvertUtil
  {
    private ConvertUtil()
    {
    }

    public static string convertNeCSTPolicyHolderToNative(string policyHolderSameAs)
    {
      return policyHolderSameAs.Equals("SELF") ? "Yes" : "No";
    }

    public static string convertNeCSTBooleanToYesNo(string flag)
    {
      if (flag == "true")
        return "Yes";
      if (flag == "false")
        return "No";
      return flag;
    }

    public static string convertNeCSTMeasureToNative(string measure)
    {
      if (measure == "{gd}")
        return "GD";
      if (measure == "h")
        return "HR";
      if (measure == "km")
        return "KM";
      if (measure == "{pg}")
        return "PG";
      if (measure == "{pr}")
        return "PR";
      if (measure == "{sn}")
        return "SN";
      return measure;
    }

    public static string convertNeCSTDateToNative(string text)
    {
      if (text == "")
        return "";
      return DateTime.ParseExact(text, "yyyyMMdd", (IFormatProvider) null).ToString("yyyy-MM-dd");
    }

    public static string convertNeCSTDateTimeToNative(string text)
    {
      if (text == "")
        return "";
      string s = text.Substring(0, 14);
      string str = text.Substring(14, text.Length - 14);
      string format = "yyyyMMddHHmmss";
      return DateTime.ParseExact(s, format, (IFormatProvider) null).ToString("yyyy-MM-ddTHH:mm:ss") + str.Substring(0, str.Length - 2) + ":" + str.Substring(str.Length - 2, 2);
    }

    public static string convertNeCSTTelephoneNumberToNative(string text)
    {
      if (text == "")
        return "";
      try
      {
        if (text.Length > 9)
          return text.Substring(4, 10);
        return "";
      }
      catch (Exception ex)
      {
        return "";
      }
    }

    public static string convertNeCSTTelephoneExtensionToNative(string text)
    {
      if (text == "")
        return "";
      try
      {
        if (text.Length > 19)
          return text.Substring(20, 4);
        return "";
      }
      catch (Exception ex)
      {
        return "";
      }
    }

    public static string encode(string s)
    {
      if (s == null)
        return (string) null;
      s = s.Replace("&", "&amp;");
      s = s.Replace("<", "&lt;");
      s = s.Replace(">", "&gt;");
      s = s.Replace("\"", "&quot;");
      s = s.Replace("'", "&apos;");
      return s;
    }

    public static string decode(string s)
    {
      if (s == null)
        return (string) null;
      s = s.Replace("&apos;", "'");
      s = s.Replace("&quot;", "\"");
      s = s.Replace("&gt;", ">");
      s = s.Replace("&lt;", "<");
      s = s.Replace("&amp;", "&");
      return s;
    }

    public static string convertGenderToNeCST(string gender)
    {
      if (gender == "Male")
        return "M";
      if (gender == "Female")
        return "F";
      return gender;
    }

    public static string convertPolicyHolderSameAsToNeCST(string policyHolderSameAs)
    {
      if (policyHolderSameAs == "Yes")
        return "code=\"SELF\"";
      if (policyHolderSameAs == "No")
        return "nullFlavor=\"NASK\"";
      return string.Empty;
    }

    public static string convertFlagToNeCSTBool(string flag)
    {
      if (flag == "Yes")
        return "true";
      if (flag == "No")
        return "false";
      return flag;
    }

    public static string convertMeasureToNeCST(string measure)
    {
      measure = measure.ToUpper();
      if (measure == "GD")
        return "{gd}";
      if (measure == "HR")
        return "h";
      if (measure == "KM")
        return "km";
      if (measure == "PG")
        return "{pg}";
      if (measure == "PR")
        return "{pr}";
      if (measure == "SN")
        return "{sn}";
      return measure;
    }
  }
}
