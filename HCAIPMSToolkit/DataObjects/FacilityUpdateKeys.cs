﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.FacilityUpdateKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class FacilityUpdateKeys
  {
    public string FacilityRegistryNumber
    {
      get
      {
        return nameof (FacilityRegistryNumber);
      }
    }

    public string Facility_Name
    {
      get
      {
        return "FacilityName";
      }
    }

    public string CorporationNumber
    {
      get
      {
        return nameof (CorporationNumber);
      }
    }

    public string BillingAddress
    {
      get
      {
        return nameof (BillingAddress);
      }
    }

    public string Billing_Street_Address_1
    {
      get
      {
        return "Billing_StreetAddress1";
      }
    }

    public string Billing_Street_Address_2
    {
      get
      {
        return "Billing_StreetAddress2";
      }
    }

    public string Billing_City
    {
      get
      {
        return nameof (Billing_City);
      }
    }

    public string Billing_Province
    {
      get
      {
        return nameof (Billing_Province);
      }
    }

    public string Billing_PostalCode
    {
      get
      {
        return nameof (Billing_PostalCode);
      }
    }

    public string SameServiceAddress
    {
      get
      {
        return nameof (SameServiceAddress);
      }
    }

    public string ServiceAddress
    {
      get
      {
        return nameof (ServiceAddress);
      }
    }

    public string Service_Street_Address_1
    {
      get
      {
        return "Service_StreetAddress1";
      }
    }

    public string Service_Street_Address_2
    {
      get
      {
        return "Service_StreetAddress2";
      }
    }

    public string Service_City
    {
      get
      {
        return nameof (Service_City);
      }
    }

    public string Service_Province
    {
      get
      {
        return nameof (Service_Province);
      }
    }

    public string Service_PostalCode
    {
      get
      {
        return nameof (Service_PostalCode);
      }
    }

    public string AISIFacilityNumber
    {
      get
      {
        return nameof (AISIFacilityNumber);
      }
    }

    public string Telephone
    {
      get
      {
        return nameof (Telephone);
      }
    }

    public string Extension
    {
      get
      {
        return nameof (Extension);
      }
    }

    public string Fax
    {
      get
      {
        return nameof (Fax);
      }
    }

    public string AuthorizingOfficerEmail
    {
      get
      {
        return nameof (AuthorizingOfficerEmail);
      }
    }

    public string AuthorizingOfficerConfirmEmail
    {
      get
      {
        return nameof (AuthorizingOfficerConfirmEmail);
      }
    }

    public string DefaultPerkmRate
    {
      get
      {
        return nameof (DefaultPerkmRate);
      }
    }

    public string ContactOne
    {
      get
      {
        return nameof (ContactOne);
      }
    }

    public string ContactOne_FirstName
    {
      get
      {
        return "ContactOneFirstName";
      }
    }

    public string ContactOne_LastName
    {
      get
      {
        return "ContactOneLastName";
      }
    }

    public string ContactOneTitle
    {
      get
      {
        return nameof (ContactOneTitle);
      }
    }

    public string ContactOneEmail
    {
      get
      {
        return nameof (ContactOneEmail);
      }
    }

    public string ContactOneConfirmEmail
    {
      get
      {
        return nameof (ContactOneConfirmEmail);
      }
    }

    public string ContactOneTelephone
    {
      get
      {
        return nameof (ContactOneTelephone);
      }
    }

    public string ContactOneTelephoneExtension
    {
      get
      {
        return nameof (ContactOneTelephoneExtension);
      }
    }

    public string ContactTwo
    {
      get
      {
        return nameof (ContactTwo);
      }
    }

    public string ContactTwo_FirstName
    {
      get
      {
        return "ContactTwoFirstName";
      }
    }

    public string ContactTwo_LastName
    {
      get
      {
        return "ContactTwoLastName";
      }
    }

    public string ContactTwoTitle
    {
      get
      {
        return nameof (ContactTwoTitle);
      }
    }

    public string ContactTwoEmail
    {
      get
      {
        return nameof (ContactTwoEmail);
      }
    }

    public string ContactTwoConfirmEmail
    {
      get
      {
        return nameof (ContactTwoConfirmEmail);
      }
    }

    public string ContactTwoTelephone
    {
      get
      {
        return nameof (ContactTwoTelephone);
      }
    }

    public string ContactTwoTelephoneExtension
    {
      get
      {
        return nameof (ContactTwoTelephoneExtension);
      }
    }

    public string MakeChequePayableTo
    {
      get
      {
        return nameof (MakeChequePayableTo);
      }
    }

    public string LockPayableFlag
    {
      get
      {
        return nameof (LockPayableFlag);
      }
    }

    public string PayeeNumber
    {
      get
      {
        return nameof (PayeeNumber);
      }
    }

    public string PayeeName
    {
      get
      {
        return nameof (PayeeName);
      }
    }

    public string PayeeName_FirstName
    {
      get
      {
        return "PayeeNameFirstName";
      }
    }

    public string PayeeName_LastName
    {
      get
      {
        return "PayeeNameLastName";
      }
    }

    public string PMSVendor
    {
      get
      {
        return nameof (PMSVendor);
      }
    }
  }
}
