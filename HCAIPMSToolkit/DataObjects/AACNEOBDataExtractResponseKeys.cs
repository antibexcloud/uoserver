﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNEOBDataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNEOBDataExtractResponseKeys
  {
    public string EOB_EOBDocumentNumber
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:EOB/hcai:EOBDocumentNumber";
      }
    }

    public string EOB_DateRevised
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:EOB/hcai:DateRevised";
      }
    }

    public string EOB_EOBAdditionalComments
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:EOB/hcai:EOBAdditionalComments";
      }
    }

    public string EOB_OCFVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:EOB/hcai:OCFVersion";
      }
    }
  }
}
