﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.GS21CPAFSecondaryProviderProfessionLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class GS21CPAFSecondaryProviderProfessionLineItemKeys
  {
    public string OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID);
      }
    }
  }
}
