﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF18InjuryLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF18InjuryLineItemKeys
  {
    public string OCF18_InjuriesAndSequelae_Injury_Code
    {
      get
      {
        return "/x:*/x:InjuriesAndSequelae/x:Injury/x:Code";
      }
    }
  }
}
