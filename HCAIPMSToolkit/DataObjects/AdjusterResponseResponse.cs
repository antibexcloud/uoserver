﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AdjusterResponseResponse
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class AdjusterResponseResponse : XmlDataItem
  {
    protected override string Translate(string key, string value)
    {
      AdjusterResponseResponseKeys responseResponseKeys = new AdjusterResponseResponseKeys();
      if (key == new AdjusterResponseResponseKeys().Attachments_Received_Date || key == responseResponseKeys.Claimant_Date_Of_Birth || (key == new OCF9AdjusterResponseResponseKeys().OCF9_Header_DateRevised || key == new OCF21BAdjusterResponseResponseKeys().OCF21B_InsurerSignature_ClaimFormReceivedDate) || key == new OCF21CAdjusterResponseResponseKeys().OCF21C_InsurerSignature_ClaimFormReceivedDate)
        return ConvertUtil.convertNeCSTDateToNative(value);
      if (!(key == responseResponseKeys.Archival_Status))
        return base.Translate(key, value);
      return !string.IsNullOrEmpty(value) && value == "2" ? "Archived" : "Not Archived";
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      return LineItemType.AcknowledgementDetailLineItem == lineItemType;
    }
  }
}
