﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNKeys
  {
    public string FormVersion
    {
      get
      {
        return nameof (FormVersion);
      }
    }

    public string HCAI_Document_Type
    {
      get
      {
        return nameof (HCAI_Document_Type);
      }
    }

    public string Header_ClaimNumber
    {
      get
      {
        return nameof (Header_ClaimNumber);
      }
    }

    public string Header_PolicyNumber
    {
      get
      {
        return nameof (Header_PolicyNumber);
      }
    }

    public string Header_DateOfAccident
    {
      get
      {
        return nameof (Header_DateOfAccident);
      }
    }

    public string Applicant_Name_FirstName
    {
      get
      {
        return nameof (Applicant_Name_FirstName);
      }
    }

    public string Applicant_Name_MiddleName
    {
      get
      {
        return nameof (Applicant_Name_MiddleName);
      }
    }

    public string Applicant_Name_LastName
    {
      get
      {
        return nameof (Applicant_Name_LastName);
      }
    }

    public string Applicant_DateOfBirth
    {
      get
      {
        return nameof (Applicant_DateOfBirth);
      }
    }

    public string Applicant_Gender
    {
      get
      {
        return nameof (Applicant_Gender);
      }
    }

    public string Applicant_Telephone
    {
      get
      {
        return nameof (Applicant_Telephone);
      }
    }

    public string Applicant_Extension
    {
      get
      {
        return nameof (Applicant_Extension);
      }
    }

    public string Applicant_Address_StreetAddress1
    {
      get
      {
        return nameof (Applicant_Address_StreetAddress1);
      }
    }

    public string Applicant_Address_StreetAddress2
    {
      get
      {
        return nameof (Applicant_Address_StreetAddress2);
      }
    }

    public string Applicant_Address_City
    {
      get
      {
        return nameof (Applicant_Address_City);
      }
    }

    public string Applicant_Address_Province
    {
      get
      {
        return nameof (Applicant_Address_Province);
      }
    }

    public string Applicant_Address_PostalCode
    {
      get
      {
        return nameof (Applicant_Address_PostalCode);
      }
    }

    public string AssessmentDate
    {
      get
      {
        return nameof (AssessmentDate);
      }
    }

    public string FirstAssessment
    {
      get
      {
        return nameof (FirstAssessment);
      }
    }

    public string LastAssessmentDate
    {
      get
      {
        return nameof (LastAssessmentDate);
      }
    }

    public string CurrentMonthlyAllowance
    {
      get
      {
        return nameof (CurrentMonthlyAllowance);
      }
    }

    public string InsurerExamination
    {
      get
      {
        return nameof (InsurerExamination);
      }
    }

    public string Assessor_FacilityRegistryID
    {
      get
      {
        return nameof (Assessor_FacilityRegistryID);
      }
    }

    public string Assessor_ProviderRegistryID
    {
      get
      {
        return nameof (Assessor_ProviderRegistryID);
      }
    }

    public string Assessor_Occupation
    {
      get
      {
        return nameof (Assessor_Occupation);
      }
    }

    public string Assessor_Email
    {
      get
      {
        return nameof (Assessor_Email);
      }
    }

    public string Assessor_IsSignatureOnFile
    {
      get
      {
        return nameof (Assessor_IsSignatureOnFile);
      }
    }

    public string Assessor_DateSigned
    {
      get
      {
        return nameof (Assessor_DateSigned);
      }
    }

    public string Insurer_IBCInsurerID
    {
      get
      {
        return nameof (Insurer_IBCInsurerID);
      }
    }

    public string Insurer_IBCBranchID
    {
      get
      {
        return nameof (Insurer_IBCBranchID);
      }
    }

    public string Insurer_PolicyHolder_IsSameAsApplicant
    {
      get
      {
        return nameof (Insurer_PolicyHolder_IsSameAsApplicant);
      }
    }

    public string Insurer_PolicyHolder_Name_FirstName
    {
      get
      {
        return nameof (Insurer_PolicyHolder_Name_FirstName);
      }
    }

    public string Insurer_PolicyHolder_Name_LastName
    {
      get
      {
        return nameof (Insurer_PolicyHolder_Name_LastName);
      }
    }

    public string AdditionalComments
    {
      get
      {
        return nameof (AdditionalComments);
      }
    }

    public string AttachmentsBeingSent
    {
      get
      {
        return nameof (AttachmentsBeingSent);
      }
    }

    public string AttachmentCount
    {
      get
      {
        return nameof (AttachmentCount);
      }
    }

    public string PMSFields_PMSSoftware
    {
      get
      {
        return nameof (PMSFields_PMSSoftware);
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return nameof (PMSFields_PMSVersion);
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return nameof (PMSFields_PMSDocumentKey);
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return nameof (PMSFields_PMSPatientKey);
      }
    }
  }
}
