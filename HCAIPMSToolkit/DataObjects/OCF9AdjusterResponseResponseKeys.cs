﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF9AdjusterResponseResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF9AdjusterResponseResponseKeys
  {
    public string HCAI_Document_Number
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OCF9']]/hl7:HCAI_Document_Number/@value";
      }
    }

    public string OCF9_Header_DateRevised
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OCF9']]/hl7:DateRevised/@value";
      }
    }

    public string AttachmentsBeingSent
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OCF9']]/hl7:AttachmentsBeingSent/@value";
      }
    }

    public string AdditionalComments
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OCF9']]/hl7:AdditionalComments/@value";
      }
    }

    public string OCFVersion
    {
      get
      {
        return "/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OCF9']]/hl7:OCFVersion/@value";
      }
    }
  }
}
