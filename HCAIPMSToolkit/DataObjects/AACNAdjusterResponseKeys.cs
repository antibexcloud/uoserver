﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNAdjusterResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.21.6787.27110, Culture=neutral, PublicKeyToken=null
// MVID: 1E9D9237-0987-4F64-A03A-6992C1631BC9
// Assembly location: C:\uoemrmvc\Libs\HcaiPms\3.21.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNAdjusterResponseKeys
  {
    public string Document_Type
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:PMSFields/hcai:PMSDocumentKey";
      }
    }

    public string Insurer_IBCInsurerID
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Insurer/hcai:IBCInsurerID";
      }
    }

    public string Insurer_IBCBranchID
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Insurer/hcai:IBCBranchID";
      }
    }

    public string Costs_Approved_CalculatedBenefit
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Costs/hcai:Approved/hcai:CalculatedBenefit";
      }
    }

    public string Costs_Approved_Benefit
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Costs/hcai:Approved/hcai:Benefit";
      }
    }

    public string Costs_Approved_ReasonCode
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Costs/hcai:Approved/hcai:ReasonCode";
      }
    }

    public string Costs_Approved_ReasonDescription
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Costs/hcai:Approved/hcai:ReasonDescription";
      }
    }

    public string Costs_Approved_AdjusterResponseExplanation
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AACN/hcai:Costs/hcai:Approved/hcai:AdjusterResponseExplanation";
      }
    }

    public string DocumentNumber
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:DocumentNumber";
      }
    }

    public string DocumentVersion
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:DocumentVersion";
      }
    }

    public string SubmissionTime
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:SubmissionTime";
      }
    }

    public string SubmissionSource
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:SubmissionSource";
      }
    }

    public string FacilityVersion
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:FacilityVersion";
      }
    }

    public string InsurerVersion
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:InsurerVersion";
      }
    }

    public string BranchVersion
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:BranchVersion";
      }
    }

    public string DocumentState
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:DocumentState";
      }
    }

    public string InDispute
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:InDispute";
      }
    }

    public string VersionDate
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:VersionDate";
      }
    }

    public string AttachmentsReceivedDate
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AttachmentsReceivedDate";
      }
    }

    public string SigningAdjusterName_FirstName
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:SigningAdjusterName/hcai:FirstName";
      }
    }

    public string SigningAdjusterName_LastName
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:SigningAdjusterName/hcai:LastName";
      }
    }

    public string NameOfAdjusterOnPDF
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:NameOfAdjusterOnPDF";
      }
    }

    public string AdjusterResponseTime
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:AdjusterResponseTime";
      }
    }

    public string ArchivalStatus
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:ArchivalStatus";
      }
    }

    public string Claimant_Name_FirstName
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Name/hcai:FirstName";
      }
    }

    public string Claimant_Name_MiddleName
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Name/hcai:MiddleName";
      }
    }

    public string Claimant_Name_LastName
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Name/hcai:LastName";
      }
    }

    public string Claimant_DateOfBirth
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:DateOfBirth";
      }
    }

    public string Claimant_Gender
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Gender";
      }
    }

    public string Claimant_Telephone
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Telephone";
      }
    }

    public string Claimant_Extension
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Extension";
      }
    }

    public string Claimant_Address_StreetAddress1
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Address/hcai:StreetAddress1";
      }
    }

    public string Claimant_Address_StreetAddress2
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Address/hcai:StreetAddress2";
      }
    }

    public string Claimant_Address_City
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Address/hcai:City";
      }
    }

    public string Claimant_Address_Province
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Address/hcai:Province";
      }
    }

    public string Claimant_Address_PostalCode
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:Claimant/hcai:Address/hcai:PostalCode";
      }
    }

    public string EOB_EOBAdditionalComments
    {
      get
      {
        return "/hcai:AACNAdjusterResponse/hcai:EOB/hcai:EOBAdditionalComments";
      }
    }
  }
}
