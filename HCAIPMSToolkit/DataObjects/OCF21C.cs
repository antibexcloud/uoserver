﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF21C
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class OCF21C : Document
  {
    public OCF21C()
    {
      this.addPrivateDatum(new OCF21CKeys().HCAI_Document_Type, nameof (OCF21C));
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      switch (lineItemType)
      {
        case LineItemType.GS21CRenderedGSLineItem:
        case LineItemType.GS21CPAFReimbursableLineItem:
        case LineItemType.GS21COtherReimbursableLineItem:
        case LineItemType.InjuryLineItem:
          return true;
        default:
          return base.isValidLineItemType(lineItemType);
      }
    }
  }
}
