﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.BaseDataItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal abstract class BaseDataItem : IDataItem
  {
    public Hashtable m_data;
    public Hashtable m_lists;

    public virtual bool addValue(string key, string val)
    {
      try
      {
        if (this.m_data.ContainsKey((object) key))
          this.m_data.Remove((object) key);
        if (val != null)
          this.m_data.Add((object) key, (object) val);
        return true;
      }
      catch
      {
        return false;
      }
    }

    public virtual string getValue(string key)
    {
      return (string) this.m_data[(object) key] ?? string.Empty;
    }

    public virtual bool addLineItem(IDataItem lineItem)
    {
      LineItemType lineItemType = ((BaseLineItem) lineItem).LineItemType;
      if (!this.isValidLineItemType(lineItemType))
        return false;
      this.getList(lineItemType).Add((object) lineItem);
      return true;
    }

    public virtual IDataItem newLineItem(LineItemType lineItemType)
    {
      if (this.isValidLineItemType(lineItemType))
        return LineItemFactory.newLineItem(lineItemType);
      return (IDataItem) null;
    }

    public virtual IList getList(LineItemType lineItemType)
    {
      IList list = (IList) null;
      if (this.isValidLineItemType(lineItemType))
      {
        string str = lineItemType.ToString();
        list = (IList) this.m_lists[(object) str];
        if (list == null)
        {
          list = (IList) new ArrayList();
          this.m_lists[(object) str] = (object) list;
        }
      }
      return list;
    }

    public IList getList(string listXPath)
    {
      return (IList) null;
    }

    public virtual bool isValidLineItemType(LineItemType lineItemType)
    {
      return false;
    }

    public string getEncodedValue(string key)
    {
      return ConvertUtil.encode((string) this.m_data[(object) key]);
    }
  }
}
