﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF23AdjusterResponseResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF23AdjusterResponseResponseKeys : AdjusterResponseResponseKeys
  {
    public string OCF23_OtherGoodsAndServices_AdjusterResponseExplanation
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:OtherGoodsAndServices/hcai:AdjusterResponseExplanation";
      }
    }

    public string OCF23_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerTotals_SubTotalPreApproved_Approved
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PreApproved']]/hl7:netAmt/@value";
      }
    }

    public string OCF23_InsurerSignature_ApplicantSignatureWaived
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ApplicantSignatureWaived";
      }
    }

    public string OCF23_InsurerSignature_ResponseForOtherGoodsAndServices
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ResponseForOtherGoodsAndServices";
      }
    }

    public string OCF23_InsurerSignature_PolicyInForceConfirmation
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:PolicyInForceConfirmation";
      }
    }
  }
}
