﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.PAFReimbursableLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class PAFReimbursableLineItemKeys
  {
    public string OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_Code
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_Code);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_Attribute
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_Attribute);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost);
      }
    }

    public string OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService
    {
      get
      {
        return nameof (OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService);
      }
    }
  }
}
