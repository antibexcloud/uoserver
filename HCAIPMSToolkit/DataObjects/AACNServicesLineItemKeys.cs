﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNServicesLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNServicesLineItemKeys
  {
    public string AttendantCareServices_Item_ACSItemID
    {
      get
      {
        return nameof (AttendantCareServices_Item_ACSItemID);
      }
    }

    public string AttendantCareServices_Item_PMSLineItemKey
    {
      get
      {
        return nameof (AttendantCareServices_Item_PMSLineItemKey);
      }
    }

    public string AttendantCareServices_Item_Assessed_Minutes
    {
      get
      {
        return nameof (AttendantCareServices_Item_Assessed_Minutes);
      }
    }

    public string AttendantCareServices_Item_Assessed_TimesPerWeek
    {
      get
      {
        return nameof (AttendantCareServices_Item_Assessed_TimesPerWeek);
      }
    }

    public string AttendantCareServices_Item_Assessed_TotalMinutes
    {
      get
      {
        return nameof (AttendantCareServices_Item_Assessed_TotalMinutes);
      }
    }
  }
}
