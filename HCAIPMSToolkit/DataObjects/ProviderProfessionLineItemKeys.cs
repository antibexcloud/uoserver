﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.ProviderProfessionLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class ProviderProfessionLineItemKeys
  {
    public string Provider_Profession_Registration_Number
    {
      get
      {
        return "hcai:ProfessionCode";
      }
    }

    public string Provider_Profession_Profession_Code
    {
      get
      {
        return "hcai:RegistrationNumber";
      }
    }
  }
}
