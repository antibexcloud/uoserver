﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF22Keys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF22Keys : DocumentKeys
  {
    public string OCF22_RegulatedHealthProfessional_ProviderFirstName
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_ProviderFirstName);
      }
    }

    public string OCF22_RegulatedHealthProfessional_ProviderLastName
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_ProviderLastName);
      }
    }

    public string OCF22_RegulatedHealthProfessional_FacilityName
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_FacilityName);
      }
    }

    public string OCF22_RegulatedHealthProfessional_AISIFacilityNumber
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_AISIFacilityNumber);
      }
    }

    public string OCF22_RegulatedHealthProfessional_Address1
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_Address1);
      }
    }

    public string OCF22_RegulatedHealthProfessional_Address2
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_Address2);
      }
    }

    public string OCF22_RegulatedHealthProfessional_City
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_City);
      }
    }

    public string OCF22_RegulatedHealthProfessional_Province
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_Province);
      }
    }

    public string OCF22_RegulatedHealthProfessional_PostalCode
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_PostalCode);
      }
    }

    public string OCF22_RegulatedHealthProfessional_TelephoneExtension
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_TelephoneExtension);
      }
    }

    public string OCF22_RegulatedHealthProfessional_TelephoneNumber
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_TelephoneNumber);
      }
    }

    public string OCF22_RegulatedHealthProfessional_FaxNumber
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_FaxNumber);
      }
    }

    public string OCF22_RegulatedHealthProfessional_FacilityRegistryID
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_FacilityRegistryID);
      }
    }

    public string OCF22_RegulatedHealthProfessional_ProviderRegistryID
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_ProviderRegistryID);
      }
    }

    public string OCF22_RegulatedHealthProfessional_Occupation
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_Occupation);
      }
    }

    public string OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists);
      }
    }

    public string OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails);
      }
    }

    public string OCF22_RegulatedHealthProfessional_IsSignatureOnFile
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_IsSignatureOnFile);
      }
    }

    public string OCF22_RegulatedHealthProfessional_DateSigned
    {
      get
      {
        return nameof (OCF22_RegulatedHealthProfessional_DateSigned);
      }
    }

    public string OCF22_NatureOfAssessment_Response
    {
      get
      {
        return nameof (OCF22_NatureOfAssessment_Response);
      }
    }

    public string OCF22_ProvisionalClinicalInformation_ClinicalInformation_PresentComplaintsDesc
    {
      get
      {
        return nameof (OCF22_ProvisionalClinicalInformation_ClinicalInformation_PresentComplaintsDesc);
      }
    }

    public string OCF22_ProvisionalClinicalInformation_ClinicalInformation_AlreadyProvidedTreatment_Response
    {
      get
      {
        return nameof (OCF22_ProvisionalClinicalInformation_ClinicalInformation_AlreadyProvidedTreatment_Response);
      }
    }

    public string OCF22_ProvisionalClinicalInformation_AssessmentInformation_Details
    {
      get
      {
        return nameof (OCF22_ProvisionalClinicalInformation_AssessmentInformation_Details);
      }
    }

    public string OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_Response
    {
      get
      {
        return nameof (OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_Response);
      }
    }

    public string OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_AssessmentDate
    {
      get
      {
        return nameof (OCF22_ProvisionalClinicalInformation_AssessmentInformation_PriorAssessment_AssessmentDate);
      }
    }

    public string OCF22_InsurerTotals_MOH_Proposed
    {
      get
      {
        return nameof (OCF22_InsurerTotals_MOH_Proposed);
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Proposed
    {
      get
      {
        return nameof (OCF22_InsurerTotals_OtherInsurers_Proposed);
      }
    }

    public string OCF22_InsurerTotals_AutoInsurerTotal_Proposed
    {
      get
      {
        return nameof (OCF22_InsurerTotals_AutoInsurerTotal_Proposed);
      }
    }

    public string OCF22_InsurerTotals_GST_Proposed
    {
      get
      {
        return nameof (OCF22_InsurerTotals_GST_Proposed);
      }
    }

    public string OCF22_InsurerTotals_PST_Proposed
    {
      get
      {
        return nameof (OCF22_InsurerTotals_PST_Proposed);
      }
    }

    public string OCF22_InsurerTotals_SubTotal_Proposed
    {
      get
      {
        return nameof (OCF22_InsurerTotals_SubTotal_Proposed);
      }
    }

    public string OCF22_ApplicantSignature_IsApplicantSignatureOnFile
    {
      get
      {
        return nameof (OCF22_ApplicantSignature_IsApplicantSignatureOnFile);
      }
    }

    public string OCF22_ApplicantSignature_SigningApplicant_FirstName
    {
      get
      {
        return nameof (OCF22_ApplicantSignature_SigningApplicant_FirstName);
      }
    }

    public string OCF22_ApplicantSignature_SigningApplicant_LastName
    {
      get
      {
        return nameof (OCF22_ApplicantSignature_SigningApplicant_LastName);
      }
    }

    public string OCF22_ApplicantSignature_SigningDate
    {
      get
      {
        return nameof (OCF22_ApplicantSignature_SigningDate);
      }
    }
  }
}
