﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.PAFReimbursableLineItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System.Collections;

namespace PMSToolkit.DataObjects
{
  internal class PAFReimbursableLineItem : BaseLineItem
  {
    public PAFReimbursableLineItem()
    {
      this.m_lineItemType = LineItemType.GS21CPAFReimbursableLineItem;
      this.m_lists = new Hashtable();
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      return LineItemType.PAFSecondaryProviderProfessionLineItem == lineItemType;
    }
  }
}
