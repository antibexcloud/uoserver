﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF22AdjusterResponseResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF22AdjusterResponseResponseKeys : AdjusterResponseResponseKeys
  {
    public string OCF22_InsurerSignature_ApplicantSignatureWaivedForOCF18
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hcai:InsurerSignature/hcai:ApplicantSignatureWaivedForOCF18";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@root='2.16.840.1.113883.3.30.1.1.1']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='InsurerTotals']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:id[@extension='OtherInsurers']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_AutoInsurerTotal_Approved
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='FST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_LineCost
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:netAmt/@value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[1]/hl7:adjudicationResultReason/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='TaxesAndInterest']]/hl7:component/hl7:adjudicatedInvoiceElementDetail[child::hl7:code[@code='PST']]/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    public string OCF22_InsurerTotals_SubTotal_Approved
    {
      get
      {
        return "/hl7:FICR_IN310304UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:netAmt/@value";
      }
    }
  }
}
