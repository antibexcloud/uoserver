﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.RenderedGSLineItem
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class RenderedGSLineItem : BaseLineItem
  {
    public RenderedGSLineItem()
    {
      this.m_lineItemType = LineItemType.GS21CRenderedGSLineItem;
    }
  }
}
