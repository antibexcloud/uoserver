﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF23AdjusterResponseResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class OCF23AdjusterResponseResponseLineItemKeys : AdjusterResponseResponseLineItemKeys
  {
    public string OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey
    {
      get
      {
        return this.Item_Approved_PMSGSKey;
      }
    }

    public string OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost
    {
      get
      {
        return this.Item_Approved_LineCost;
      }
    }

    public string OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return this.Item_Approved_ReasonCodeGroup_ReasonCode;
      }
    }

    public string OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return this.Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
      }
    }

    public string OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return this.Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
      }
    }
  }
}
