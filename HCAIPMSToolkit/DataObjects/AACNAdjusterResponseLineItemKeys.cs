﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNAdjusterResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNAdjusterResponseLineItemKeys
  {
    public string AttendantCareServices_Item_ACSItemID
    {
      get
      {
        return "@ACSItemID";
      }
    }

    public string AttendantCareServices_Item_Description
    {
      get
      {
        return "@Description";
      }
    }

    public string AttendantCareServices_Item_PMSLineItemKey
    {
      get
      {
        return "hcai:PMSLineItemKey";
      }
    }

    public string AttendantCareServices_Item_Approved_Minutes
    {
      get
      {
        return "hcai:Approved/hcai:Minutes";
      }
    }

    public string AttendantCareServices_Item_Approved_TimesPerWeek
    {
      get
      {
        return "hcai:Approved/hcai:TimesPerWeek";
      }
    }

    public string AttendantCareServices_Item_Approved_TotalMinutes
    {
      get
      {
        return "hcai:Approved/hcai:TotalMinutes";
      }
    }

    public string AttendantCareServices_Item_Approved_ReasonCode
    {
      get
      {
        return "hcai:Approved/hcai:ReasonCode";
      }
    }

    public string AttendantCareServices_Item_Approved_ReasonDescription
    {
      get
      {
        return "hcai:Approved/hcai:ReasonDescription";
      }
    }

    public string AttendantCareServices_Item_Approved_IsItemDeclined
    {
      get
      {
        return "hcai:Approved/hcai:IsItemDeclined";
      }
    }

    public string Costs_Approved_Part_ACSItemID
    {
      get
      {
        return "@ACSItemID";
      }
    }

    public string Costs_Approved_Part_WeeklyHours
    {
      get
      {
        return "hcai:WeeklyHours";
      }
    }

    public string Costs_Approved_Part_MonthlyHours
    {
      get
      {
        return "hcai:MonthlyHours";
      }
    }

    public string Costs_Approved_Part_HourlyRate
    {
      get
      {
        return "hcai:HourlyRate";
      }
    }

    public string Costs_Approved_Part_Benefit
    {
      get
      {
        return "hcai:Benefit";
      }
    }

    public string Costs_Approved_Part_ReasonCode
    {
      get
      {
        return "hcai:ReasonCode";
      }
    }

    public string Costs_Approved_Part_ReasonDescription
    {
      get
      {
        return "hcai:ReasonDescription";
      }
    }
  }
}
