﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.FacilityLicenseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class FacilityLicenseLineItemKeys
  {
    public string License_Number
    {
      get
      {
        return "hcai:LicenseNumber";
      }
    }

    public string License_Status
    {
      get
      {
        return "hcai:LicenseStatus";
      }
    }

    public string License_Last_Modified_Date
    {
      get
      {
        return "hcai:LicenseLastModifiedDate";
      }
    }

    public string License_End_Date
    {
      get
      {
        return "hcai:LicenseEndDate";
      }
    }
  }
}
