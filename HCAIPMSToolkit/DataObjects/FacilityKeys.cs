﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.FacilityKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class FacilityKeys
  {
    public string HCAI_Facility_Registry_ID
    {
      get
      {
        return "hcai:FacilityRegistryID";
      }
    }

    public string Facility_Name
    {
      get
      {
        return "hcai:FacilityName";
      }
    }

    public string Billing_Address_Line_1
    {
      get
      {
        return "hcai:BillingAddress/hcai:StreetAddress1";
      }
    }

    public string Billing_Address_Line_2
    {
      get
      {
        return "hcai:BillingAddress/hcai:StreetAddress2";
      }
    }

    public string Billing_City
    {
      get
      {
        return "hcai:BillingAddress/hcai:City";
      }
    }

    public string Billing_Province
    {
      get
      {
        return "hcai:BillingAddress/hcai:Province";
      }
    }

    public string Billing_Postal_Code
    {
      get
      {
        return "hcai:BillingAddress/hcai:PostalCode";
      }
    }

    public string SameServiceAddress
    {
      get
      {
        return "hcai:SameServiceAddress";
      }
    }

    public string Service_Address_Line_1
    {
      get
      {
        return "hcai:ServiceAddress/hcai:StreetAddress1";
      }
    }

    public string Service_Address_Line_2
    {
      get
      {
        return "hcai:ServiceAddress/hcai:StreetAddress2";
      }
    }

    public string Service_City
    {
      get
      {
        return "hcai:ServiceAddress/hcai:City";
      }
    }

    public string Service_Province
    {
      get
      {
        return "hcai:ServiceAddress/hcai:Province";
      }
    }

    public string Service_Postal_Code
    {
      get
      {
        return "hcai:ServiceAddress/hcai:PostalCode";
      }
    }

    public string AISI_Facility_Number
    {
      get
      {
        return "hcai:AISIFacilityNumber";
      }
    }

    public string Telephone_Number
    {
      get
      {
        return "hcai:Telephone";
      }
    }

    public string Fax_Number
    {
      get
      {
        return "hcai:Fax";
      }
    }

    public string Authorizing_Officer_First_Name
    {
      get
      {
        return "hcai:AuthorizingOfficerName/hcai:FirstName";
      }
    }

    public string Authorizing_Officer_Last_Name
    {
      get
      {
        return "hcai:AuthorizingOfficerName/hcai:LastName";
      }
    }

    public string Authorizing_Officer_Title
    {
      get
      {
        return "hcai:AuthorizingOfficerName/hcai:AuthorizingOfficerTitle";
      }
    }

    public string Lock_Payable_Flag
    {
      get
      {
        return "hcai:LockPayableFlag";
      }
    }

    public string Facility_Start_Date
    {
      get
      {
        return "hcai:StartDate";
      }
    }

    public string Facility_End_Date
    {
      get
      {
        return "hcai:EndDate";
      }
    }
  }
}
