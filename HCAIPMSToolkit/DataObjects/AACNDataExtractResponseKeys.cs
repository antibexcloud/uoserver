﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AACNDataExtractResponseKeys
// Assembly: HCAIPMSToolkit, Version=3.21.6787.27110, Culture=neutral, PublicKeyToken=null
// MVID: 1E9D9237-0987-4F64-A03A-6992C1631BC9
// Assembly location: C:\uoemrmvc\Libs\HcaiPms\3.21.orig\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AACNDataExtractResponseKeys
  {
    public string Document_Type
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN";
      }
    }

    public string PMSFields_PMSSoftware
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:PMSFields/hcai:PMSSoftware";
      }
    }

    public string PMSFields_PMSPatientKey
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:PMSFields/hcai:PMSPatientKey";
      }
    }

    public string PMSFields_PMSVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:PMSFields/hcai:PMSVersion";
      }
    }

    public string PMSFields_PMSDocumentKey
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:PMSFields/hcai:PMSDocumentKey";
      }
    }

    public string AttachmentsBeingSent
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:AttachmentsBeingSent";
      }
    }

    public string AttachmentCount
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:AttachmentCount";
      }
    }

    public string AdditionalComments
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:AdditionalComments";
      }
    }

    public string Header_ClaimNumber
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Header/hcai:ClaimNumber";
      }
    }

    public string FormVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:FormVersion";
      }
    }

    public string Header_PolicyNumber
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Header/hcai:PolicyNumber";
      }
    }

    public string Header_DateOfAccident
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Header/hcai:DateOfAccident";
      }
    }

    public string Applicant_Name_FirstName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Name/hcai:FirstName";
      }
    }

    public string Applicant_Name_MiddleName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Name/hcai:MiddleName";
      }
    }

    public string Applicant_Name_LastName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Name/hcai:LastName";
      }
    }

    public string Applicant_DateOfBirth
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:DateOfBirth";
      }
    }

    public string Applicant_Gender
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Gender";
      }
    }

    public string Applicant_Telephone
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Telephone";
      }
    }

    public string Applicant_Extension
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Extension";
      }
    }

    public string Applicant_Address_StreetAddress1
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Address/hcai:StreetAddress1";
      }
    }

    public string Applicant_Address_StreetAddress2
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Address/hcai:StreetAddress2";
      }
    }

    public string Applicant_Address_City
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Address/hcai:City";
      }
    }

    public string Applicant_Address_Province
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Address/hcai:Province";
      }
    }

    public string Applicant_Address_PostalCode
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Applicant/hcai:Address/hcai:PostalCode";
      }
    }

    public string AssessmentDate
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:AssessmentDate";
      }
    }

    public string FirstAssessment
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:FirstAssessment";
      }
    }

    public string LastAssessmentDate
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:LastAssessmentDate";
      }
    }

    public string CurrentMonthlyAllowance
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:CurrentMonthlyAllowance";
      }
    }

    public string InsurerExamination
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:InsurerExamination";
      }
    }

    public string Assessor_FacilityRegistryID
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:FacilityRegistryID";
      }
    }

    public string Assessor_ProviderRegistryID
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:ProviderRegistryID";
      }
    }

    public string Assessor_Occupation
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:Occupation";
      }
    }

    public string Assessor_Email
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:Email";
      }
    }

    public string Assessor_IsSignatureOnFile
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:IsSignatureOnFile";
      }
    }

    public string Assessor_DateSigned
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:DateSigned";
      }
    }

    public string Insurer_IBCInsurerID
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Insurer/hcai:IBCInsurerID";
      }
    }

    public string Insurer_IBCBranchID
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Insurer/hcai:IBCBranchID";
      }
    }

    public string Insurer_PolicyHolder_IsSameAsApplicant
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Insurer/hcai:PolicyHolder/hcai:IsSameAsApplicant";
      }
    }

    public string Insurer_PolicyHolder_Name_FirstName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Insurer/hcai:PolicyHolder/hcai:Name/hcai:FirstName";
      }
    }

    public string Insurer_PolicyHolder_Name_LastName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Insurer/hcai:PolicyHolder/hcai:Name/hcai:LastName";
      }
    }

    public string Costs_Assessed_Benefit
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Costs/hcai:Assessed/hcai:Benefit";
      }
    }

    public string Costs_Approved_CalculatedBenefit
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Costs/hcai:Approved/hcai:CalculatedBenefit";
      }
    }

    public string Costs_Approved_Benefit
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Costs/hcai:Approved/hcai:Benefit";
      }
    }

    public string Costs_Approved_ReasonCode
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Costs/hcai:Approved/hcai:ReasonCode";
      }
    }

    public string Costs_Approved_ReasonDescription
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Costs/hcai:Approved/hcai:ReasonDescription";
      }
    }

    public string Costs_Approved_AdjusterResponseExplanation
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Costs/hcai:Approved/hcai:AdjusterResponseExplanation";
      }
    }

    public string DocumentNumber
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:DocumentNumber";
      }
    }

    public string DocumentVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:DocumentVersion";
      }
    }

    public string SubmissionTime
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:SubmissionTime";
      }
    }

    public string SubmissionSource
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:SubmissionSource";
      }
    }

    public string FacilityVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:FacilityVersion";
      }
    }

    public string InsurerVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:InsurerVersion";
      }
    }

    public string BranchVersion
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:BranchVersion";
      }
    }

    public string DocumentState
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:DocumentState";
      }
    }

    public string InDispute
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:InDispute";
      }
    }

    public string VersionDate
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:VersionDate";
      }
    }

    public string Insurer_Name
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Insurer/hcai:Name";
      }
    }

    public string Assessor_Facility_Name
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AACN/hcai:Assessor/hcai:Facility/hcai:Name";
      }
    }

    public string AttachmentsReceivedDate
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AttachmentsReceivedDate";
      }
    }

    public string SigningAdjusterName_FirstName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:SigningAdjusterName/hcai:FirstName";
      }
    }

    public string SigningAdjusterName_LastName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:SigningAdjusterName/hcai:LastName";
      }
    }

    public string NameOfAdjusterOnPDF
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:NameOfAdjusterOnPDF";
      }
    }

    public string AdjusterResponseTime
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:AdjusterResponseTime";
      }
    }

    public string ArchivalStatus
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:ArchivalStatus";
      }
    }

    public string Claimant_Name_FirstName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Name/hcai:FirstName";
      }
    }

    public string Claimant_Name_MiddleName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Name/hcai:MiddleName";
      }
    }

    public string Claimant_Name_LastName
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Name/hcai:LastName";
      }
    }

    public string Claimant_DateOfBirth
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:DateOfBirth";
      }
    }

    public string Claimant_Gender
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Gender";
      }
    }

    public string Claimant_Telephone
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Telephone";
      }
    }

    public string Claimant_Extension
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Extension";
      }
    }

    public string Claimant_Address_StreetAddress1
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Address/hcai:StreetAddress1";
      }
    }

    public string Claimant_Address_StreetAddress2
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Address/hcai:StreetAddress2";
      }
    }

    public string Claimant_Address_City
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Address/hcai:City";
      }
    }

    public string Claimant_Address_Province
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Address/hcai:Province";
      }
    }

    public string Claimant_Address_PostalCode
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:Claimant/hcai:Address/hcai:PostalCode";
      }
    }

    public string EOB_EOBAdditionalComments
    {
      get
      {
        return "/hcai:AACNDataExtract/hcai:EOB/hcai:EOBAdditionalComments";
      }
    }
  }
}
