﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF18
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class OCF18 : Document
  {
    public OCF18()
    {
      this.addPrivateDatum(new OCF18Keys().HCAI_Document_Type, nameof (OCF18));
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      if (lineItemType == LineItemType.GS18LineItem || lineItemType == LineItemType.GS18SessionHeaderLineItem || lineItemType == LineItemType.InjuryLineItem)
        return true;
      return base.isValidLineItemType(lineItemType);
    }
  }
}
