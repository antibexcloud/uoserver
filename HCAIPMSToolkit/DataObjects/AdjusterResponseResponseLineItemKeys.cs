﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.AdjusterResponseResponseLineItemKeys
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  public class AdjusterResponseResponseLineItemKeys
  {
    protected string Item_Approved_PMSGSKey
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:id/@extension";
      }
    }

    protected string Item_Approved_GST
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:fstInd/@value";
      }
    }

    protected string Item_Approved_PST
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:pstInd/@value";
      }
    }

    protected string Item_Approved_LineCost
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:unitPriceAmt/hl7:numerator/@value";
      }
    }

    protected string Item_Approved_TotalLineCost
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:netAmt/@value";
      }
    }

    protected string Item_Approved_Count
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:factorNumber/@value";
      }
    }

    protected string Item_Approved_ReasonCodeGroup_ReasonCode
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value";
      }
    }

    protected string Item_Approved_ReasonCodeGroup_ReasonCodeDesc
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    protected string Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultInformation/hl7:value";
      }
    }

    protected string Item_ReferenceNumber
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:id/@extension";
      }
    }

    protected string Item_InterestPayable
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:netAmt/@value";
      }
    }

    protected string Item_Item_Code
    {
      get
      {
        return "hl7:adjudicatedInvoiceElementDetail/hl7:code/@code";
      }
    }
  }
}
