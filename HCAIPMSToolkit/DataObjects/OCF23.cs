﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.DataObjects.OCF23
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.DataObjects
{
  internal class OCF23 : Document
  {
    public OCF23()
    {
      this.addPrivateDatum(new OCF23Keys().HCAI_Document_Type, nameof (OCF23));
    }

    public override bool isValidLineItemType(LineItemType lineItemType)
    {
      if (lineItemType == LineItemType.GS23PAFLineItem || lineItemType == LineItemType.GS23OtherGSLineItem || lineItemType == LineItemType.InjuryLineItem)
        return true;
      return base.isValidLineItemType(lineItemType);
    }
  }
}
