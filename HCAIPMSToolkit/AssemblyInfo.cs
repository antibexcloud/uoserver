﻿using System;
using System.Reflection;

[assembly: AssemblyTitle("HCAI Practice Management System Integration Toolkit")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
[assembly: AssemblyCopyright("Copyright © Sapient Corporation 2016")]
[assembly: AssemblyCompany("Insurance Bureau of Canada")]
[assembly: AssemblyProduct("HCAI")]
[assembly: AssemblyConfiguration("Debug")]
[assembly: CLSCompliant(false)]
[assembly: AssemblyVersion("3.17.6326.27877")]
