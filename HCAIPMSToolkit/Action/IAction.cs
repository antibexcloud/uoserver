﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.IAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

namespace PMSToolkit.Action
{
  internal interface IAction
  {
    void validate(string username, string password);

    IDataItem transformRequest(IDataItem data);

    IDataItem sendMessage(string username, string password, IDataItem requestData);

    bool isResponseSuccess(IDataItem responseData);

    IDataItem transformResponse(IDataItem responseData);

    IError processError(IDataItem responseData, string documentType);
  }
}
