﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.ProviderAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using IBC.HCAI.SoapExceptionWrapper;
using PMSToolkit.DataObjects;
using PMSToolkit.ToolkitError;
using System;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.Xml;

namespace PMSToolkit.Action
{
  internal class ProviderAction : BaseAction
  {
    private ActionType m_action;

    public ProviderAction(ActionType action)
    {
      if (action != ActionType.UpdateProvider && action != ActionType.AddProvider)
        throw new NotSupportedException("ActionType (" + action.ToString() + ") not supported.");
      this.m_action = action;
    }

    public override IDataItem sendMessage(string username, string password, IDataItem requestData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = requestData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      try
      {
        xmlDocument.LoadXml(xml);
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The string composed by the toolkit is not valid XML.  Have XML meta-characters been input as one of the key values?", ex);
      }
      PMSServiceClient facilityInfo = new PMSServiceClient();
      this.AddCustomHeaderForPMS(username, password, facilityInfo);
      XmlElement xmlElement = (XmlElement) null;
      string appSetting = ConfigurationManager.AppSettings["NeCSTCapture"];
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\ProviderRequest.xml", false);
          string str = xml;
          streamWriter.Write(str);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      try
      {
        if (this.m_action == ActionType.AddProvider)
          xmlElement = facilityInfo.AddProviderInfo(xmlDocument.DocumentElement);
        else if (this.m_action == ActionType.UpdateProvider)
          xmlElement = facilityInfo.UpdateProviderInfo(xmlDocument.DocumentElement);
      }
      catch (FaultException<FaultExceptionDetail> ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nHCAISystemError, "Error while updating information for the Provider.", (Exception) ex);
      }
      finally
      {
        facilityInfo.Close();
      }
      ResponseMessage responseMessage = new ResponseMessage();
      string outerXml = xmlElement.OuterXml;
      responseMessage.addValue(pmsResponseKeys.PMSResponseMessageKey, outerXml);
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\ProviderResponse.xml", false);
          string str = outerXml;
          streamWriter.Write(str);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      return (IDataItem) responseMessage;
    }

    public override IError processError(IDataItem responseData, string documentType)
    {
      return new ProviderErrorMapper().transform(responseData, documentType);
    }

    public override bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      if ("" == xml)
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The toolkit expected an XML document to be returned in the response, but an empty string was returned, instead.");
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      string innerText;
      try
      {
        innerText = xmlDocument.SelectSingleNode("Response/ResponseStatus", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Code for response status not found where expected.", ex);
      }
      return innerText.Contains("Success");
    }
  }
}
