﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.SubmitAACNAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using IBC.HCAI.SoapExceptionWrapper;
using PMSToolkit.DataObjects;
using PMSToolkit.Mapper;
using PMSToolkit.ToolkitError;
using System;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;

namespace PMSToolkit.Action
{
  internal class SubmitAACNAction : IAction
  {
    private OperationContextScope objOperationContextScope;

    protected void AddCustomHeaderForPMS(string username, string password, PMSServiceClient submitAACN)
    {
      this.objOperationContextScope = new OperationContextScope((IContextChannel) submitAACN.InnerChannel);
      MessageHeader header1 = MessageHeader.CreateHeader("Username", "http://hcai.ca", (object) username);
      MessageHeader header2 = MessageHeader.CreateHeader("Password", "http://hcai.ca", (object) password);
      OperationContext.Current.OutgoingMessageHeaders.Add(header1);
      OperationContext.Current.OutgoingMessageHeaders.Add(header2);
    }

    public virtual void validate(string username, string password)
    {
      if (ToolkitUtility.IsNullOrEmpty(username) || username.Trim() == string.Empty)
        throw new ToolkitException(ToolkitExceptionCode.nBlankUserName, "User Name has not been specified.");
      if (ToolkitUtility.IsNullOrEmpty(password) || password.Trim() == string.Empty)
        throw new ToolkitException(ToolkitExceptionCode.nBlankPassword, "Password has not been specified.");
    }

    public virtual IDataItem transformRequest(IDataItem data)
    {
      return this.doTransform(data);
    }

    public virtual IDataItem sendMessage(string username, string password, IDataItem requestData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = requestData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      try
      {
        xmlDocument.LoadXml(xml);
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The string composed by the toolkit is not valid XML.  Have XML meta-characters been input as one of the key values?", ex);
      }
      PMSServiceClient submitAACN = new PMSServiceClient();
      this.AddCustomHeaderForPMS(username, password, submitAACN);
      XmlElement xmlElement = (XmlElement) null;
      string appSetting = ConfigurationManager.AppSettings["NeCSTCapture"];
      if (appSetting == "true")
      {
        try
        {
                    //StreamWriter streamWriter = new StreamWriter("HCAIRequest.xml", false);
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIRequest_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml", false);
          streamWriter.Write(xml);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      try
      {
        xmlElement = submitAACN.SendNeCSTMessage(xmlDocument.DocumentElement);
      }
      catch (FaultException<FaultExceptionDetail> ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nHCAISystemError, "The call to the web method 'SendNeCSTMessage()' message threw an exception.", (Exception) ex);
      }
      finally
      {
        submitAACN.Close();
      }
      AACNResponseMessage aacnResponseMessage = new AACNResponseMessage();
      string outerXml = xmlElement.OuterXml;
      aacnResponseMessage.addValue(pmsResponseKeys.PMSResponseMessageKey, outerXml);
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIResponse_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".xml", false);//StreamWriter streamWriter = new StreamWriter("HCAIResponse.xml", false);
          streamWriter.Write(outerXml);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      return (IDataItem) aacnResponseMessage;
    }

    private IDataItem doTransform(IDataItem data)
    {
      return AACNMapperfactory.newMapper(data, (IAction) this).transform(data);
    }

    public virtual IDataItem transformResponse(IDataItem responseData)
    {
      return this.doTransform(responseData);
    }

    public virtual IError processError(IDataItem responseData, string documentType)
    {
      return new ErrorMapperAACNSubmit().transform(responseData, documentType);
    }

    private void SetEncoding(XmlDocument xmlOCFDoc)
    {
      if (xmlOCFDoc.FirstChild.NodeType != XmlNodeType.XmlDeclaration)
        return;
      ((XmlDeclaration) xmlOCFDoc.FirstChild).Encoding = "ISO-8859-1";
    }

    public virtual bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      if ("" == xml)
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The toolkit expected an XML document to be returned in the response, but an empty string was returned, instead.");
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      nsmgr.AddNamespace("x", "http://hcai.ca");
      string empty = string.Empty;
      string innerText;
      try
      {
        innerText = xmlDocument.SelectSingleNode("/x:AACNResponse/x:ResponseStatus", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Code for response status not found where expected.", ex);
      }
      return innerText == "Success";
    }
  }
}
