﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.FacilityInfoAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using IBC.HCAI.SoapExceptionWrapper;
using PMSToolkit.DataObjects;
using System;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;

namespace PMSToolkit.Action
{
  internal class FacilityInfoAction : BaseAction
  {
    private OperationContextScope objOperationContextScope;

    private new void AddCustomHeaderForPMS(string username, string password, PMSServiceClient facilityInfo)
    {
      this.objOperationContextScope = new OperationContextScope((IContextChannel) facilityInfo.InnerChannel);
      MessageHeader header1 = MessageHeader.CreateHeader("Username", "http://hcai.ca", (object) username);
      MessageHeader header2 = MessageHeader.CreateHeader("Password", "http://hcai.ca", (object) password);
      OperationContext.Current.OutgoingMessageHeaders.Add(header1);
      OperationContext.Current.OutgoingMessageHeaders.Add(header2);
    }

    public override IDataItem transformRequest(IDataItem data)
    {
      return (IDataItem) null;
    }

    public override IDataItem sendMessage(string username, string password, IDataItem requestData)
    {
      PMSServiceClient facilityInfo = new PMSServiceClient();
      this.AddCustomHeaderForPMS(username, password, facilityInfo);
      string outerXml;
      try
      {
        outerXml = facilityInfo.GetFacilityInfo().OuterXml;
      }
      catch (FaultException<FaultExceptionDetail> ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nHCAISystemError, "The call to the web method 'GetFacilityInfo()' message threw an exception.", (Exception) ex);
      }
      finally
      {
        facilityInfo.Close();
      }
      if (ConfigurationManager.AppSettings["NeCSTCapture"] == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIResponse.xml", false);
          string str = outerXml;
          streamWriter.Write(str);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      ResponseMessage responseMessage = new ResponseMessage();
      string responseMessageKey = pmsResponseKeys.PMSResponseMessageKey;
      string val = outerXml;
      responseMessage.addValue(responseMessageKey, val);
      return (IDataItem) responseMessage;
    }

    public override bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      string str = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument = new XmlDocument();
      string xml = str;
      xmlDocument.LoadXml(xml);
      string xpath = "/";
      if (xmlDocument.SelectSingleNode(xpath).FirstChild.Name == "Facility")
        return str.Length > 0;
      return base.isResponseSuccess(responseData);
    }
  }
}
