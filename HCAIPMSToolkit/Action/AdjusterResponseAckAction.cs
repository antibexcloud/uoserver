﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.AdjusterResponseAckAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Xml;

namespace PMSToolkit.Action
{
  internal class AdjusterResponseAckAction : BaseAction
  {
    public override bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(xml);
      if (xmlDocument.SelectSingleNode("/").FirstChild.Name.ToString() != "MCCI_IN000002UV01")
        return base.isResponseSuccess(responseData);
      return true;
    }
  }
}
