﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.DataExtractAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using PMSToolkit.DataObjects;
using System.Xml;

namespace PMSToolkit.Action
{
  internal class DataExtractAction : BaseAction
  {
    public override bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      string str = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument = new XmlDocument();
      string xml = str;
      xmlDocument.LoadXml(xml);
      string xpath = "/";
      XmlNode xmlNode = xmlDocument.SelectSingleNode(xpath);
      if (xmlNode.FirstChild != null && xmlNode.FirstChild.Name.ToString() == "AACNDataExtract")
        return this.isResponseSuccessAACN(responseData);
      if (xmlNode.FirstChild.Name.ToString() != "QUCR_IN350104UV02")
        return base.isResponseSuccess(responseData);
      return str.Length > 0;
    }
  }
}
