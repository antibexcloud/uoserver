﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.ActionFactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;

namespace PMSToolkit.Action
{
  internal class ActionFactory
  {
    public static IAction newAction(ActionType action)
    {
      switch (action)
      {
        case ActionType.SubmitDocument:
          return (IAction) new SubmitAction();
        case ActionType.GetAdjusterResponse:
          return (IAction) new GetAdjusterResponseAction();
        case ActionType.GetActivityList:
          return (IAction) new ActivityListAction();
        case ActionType.GetDataExtract:
          return (IAction) new DataExtractAction();
        case ActionType.GetInsurerList:
          return (IAction) new InsurerListAction();
        case ActionType.GetFacilityInfo:
          return (IAction) new FacilityInfoAction();
        case ActionType.SendAdjusterResponseAck:
          return (IAction) new AdjusterResponseAckAction();
        case ActionType.UpdateProvider:
        case ActionType.AddProvider:
          return (IAction) new ProviderAction(action);
        case ActionType.UpdateFacility:
          return (IAction) new UpdateFacilityAction();
        default:
          throw new NotSupportedException("ActionType (" + action.ToString() + ") not supported.");
      }
    }
  }
}
