﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.UpdateFacilityAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using IBC.HCAI.SoapExceptionWrapper;
using PMSToolkit.DataObjects;
using PMSToolkit.ToolkitError;
using System;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.Xml;

namespace PMSToolkit.Action
{
  internal class UpdateFacilityAction : BaseAction
  {
    public override IDataItem sendMessage(string username, string password, IDataItem requestData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = requestData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      try
      {
        xmlDocument.LoadXml(xml);
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The string composed by the toolkit is not valid XML.  Have XML meta-characters been input as one of the key values?", ex);
      }
      PMSServiceClient facilityInfo = new PMSServiceClient();
      this.AddCustomHeaderForPMS(username, password, facilityInfo);
      XmlElement xmlElement = (XmlElement) null;
      string appSetting = ConfigurationManager.AppSettings["NeCSTCapture"];
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIRequest_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".xml", false);//StreamWriter streamWriter = new StreamWriter("HCAIRequest.xml", false);
          streamWriter.Write(xml);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      try
      {
        xmlElement = facilityInfo.UpdateFacilityInfo(xmlDocument.DocumentElement);
      }
      catch (FaultException<FaultExceptionDetail> ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nHCAISystemError, "The call to the web method 'updateFacilityInfo()' message threw an exception.", (Exception) ex);
      }
      finally
      {
        facilityInfo.Close();
      }
      ResponseMessage responseMessage = new ResponseMessage();
      string outerXml = xmlElement.OuterXml;
      responseMessage.addValue(pmsResponseKeys.PMSResponseMessageKey, outerXml);
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIResponse_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".xml", false);//StreamWriter streamWriter = new StreamWriter("HCAIResponse.xml", false);
          streamWriter.Write(outerXml);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      return (IDataItem) responseMessage;
    }

    public override IError processError(IDataItem responseData, string documentType)
    {
      return new ErrorMapperFacilityUpdate().transform(responseData, documentType);
    }

    public override bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      if (string.IsNullOrEmpty(xml))
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The toolkit expected an XML document to be returned in the response, but an empty string was returned, instead.");
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      string innerText;
      try
      {
        innerText = xmlDocument.SelectSingleNode("Response/ResponseStatus", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Code for response status not found where expected.", ex);
      }
      return innerText == "Success";
    }
  }
}
