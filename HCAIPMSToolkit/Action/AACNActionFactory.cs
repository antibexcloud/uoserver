﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.AACNActionFactory
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using System;

namespace PMSToolkit.Action
{
  internal class AACNActionFactory
  {
    public static IAction newAction(ActionType action)
    {
      if (action == ActionType.SubmitAACN)
        return (IAction) new SubmitAACNAction();
      throw new NotSupportedException("AACNActionType (" + (object) action + ") not supported.");
    }
  }
}
