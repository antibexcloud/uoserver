﻿// Decompiled with JetBrains decompiler
// Type: PMSToolkit.Action.BaseAction
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: 71EE743D-7156-4214-8C45-7A0EADD18289
// Assembly location: D:\uoemrmvc\Libs\HcaiPms\3.18.orig\HCAIPMSToolkit.dll

using IBC.HCAI.SoapExceptionWrapper;
using PMSToolkit.DataObjects;
using PMSToolkit.Mapper;
using PMSToolkit.ToolkitError;
using System;
using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace PMSToolkit.Action
{
  internal class BaseAction : IAction
  {
    private string xsltTemplate = "<?xml version=\"1.0\" encoding=\"utf-8\"?><xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:oc=\"urn:hl7-org:v3\" exclude-result-prefixes=\"oc\"><xsl:template match=\"@* | node()\"><xsl:copy><xsl:apply-templates select=\"@* | node()\"/></xsl:copy></xsl:template><xsl:template match=\"oc:fstInd[@value='No']\"><oc:fstInd value=\"false\"/></xsl:template><xsl:template match=\"oc:fstInd[@value='Yes']\"><oc:fstInd value=\"true\"/></xsl:template><xsl:template match=\"oc:pstInd[@value='No']\"><oc:pstInd value=\"false\"/></xsl:template><xsl:template match=\"oc:pstInd[@value='Yes']\">  <oc:pstInd value=\"true\"/> </xsl:template></xsl:stylesheet>";
    private OperationContextScope objOperationContextScope;

    protected void AddCustomHeaderForPMS(string username, string password, PMSServiceClient facilityInfo)
    {
      this.objOperationContextScope = new OperationContextScope((IContextChannel) facilityInfo.InnerChannel);
      MessageHeader header1 = MessageHeader.CreateHeader("Username", "http://hcai.ca", (object) username);
      MessageHeader header2 = MessageHeader.CreateHeader("Password", "http://hcai.ca", (object) password);
      OperationContext.Current.OutgoingMessageHeaders.Add(header1);
      OperationContext.Current.OutgoingMessageHeaders.Add(header2);
    }

    public virtual void validate(string username, string password)
    {
      if (ToolkitUtility.IsNullOrEmpty(username) || username.Trim() == string.Empty)
        throw new ToolkitException(ToolkitExceptionCode.nBlankUserName, "User Name has not been specified.");
      if (ToolkitUtility.IsNullOrEmpty(password) || password.Trim() == string.Empty)
        throw new ToolkitException(ToolkitExceptionCode.nBlankPassword, "Password has not been specified.");
    }

    public virtual IDataItem transformRequest(IDataItem data)
    {
      return this.doTransform(data);
    }

    public virtual IDataItem sendMessage(string username, string password, IDataItem requestData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string Xml = requestData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      string xml;
      try
      {
        xml = this.ApplyXslt(Xml, this.xsltTemplate);
        xmlDocument.LoadXml(xml);
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The string composed by the toolkit is not valid XML.  Have XML meta-characters been input as one of the key values?", ex);
      }
      PMSServiceClient facilityInfo = new PMSServiceClient();
      this.AddCustomHeaderForPMS(username, password, facilityInfo);
      XmlElement xmlElement = (XmlElement) null;
      string appSetting = ConfigurationManager.AppSettings["NeCSTCapture"];
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIRequest_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".xml", false);
          streamWriter.Write(xml);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      try
      {
        xmlElement = facilityInfo.SendNeCSTMessage(xmlDocument.DocumentElement);
      }
      catch (FaultException<FaultExceptionDetail> ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nHCAISystemError, "The call to the web method 'SendNeCSTMessage()' message threw an exception.", (Exception) ex);
      }
      finally
      {
        facilityInfo.Close();
      }
      ResponseMessage responseMessage = new ResponseMessage();
      string outerXml = xmlElement.OuterXml;
      responseMessage.addValue(pmsResponseKeys.PMSResponseMessageKey, outerXml);
      if (appSetting == "true")
      {
        try
        {
          StreamWriter streamWriter = new StreamWriter("c:\\Temp\\HCAIResponse_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".xml", false);
          streamWriter.Write(outerXml);
          streamWriter.Close();
        }
        catch
        {
        }
      }
      return (IDataItem) responseMessage;
    }

    public virtual bool isResponseSuccessAACN(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument = new XmlDocument();
      string xml = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      if ("" == xml)
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The toolkit expected an XML document to be returned in the response, but an empty string was returned, instead.");
      xmlDocument.LoadXml(xml);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      nsmgr.AddNamespace("hcai", "http://hcai.ca");
      string empty = string.Empty;
      string innerText;
      try
      {
        innerText = xmlDocument.SelectSingleNode("/hcai:AACNDataExtract/hcai:DocumentNumber", nsmgr).InnerText;
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Code for response status not found where expected.", ex);
      }
      return !string.IsNullOrEmpty(innerText);
    }

    public virtual bool isResponseSuccess(IDataItem responseData)
    {
      PMSResponseKeys pmsResponseKeys = new PMSResponseKeys();
      XmlDocument xmlDocument1 = new XmlDocument();
      string xml1 = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      if ("" == xml1)
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "The toolkit expected an XML document to be returned in the response, but an empty string was returned, instead.");
      xmlDocument1.LoadXml(xml1);
      XmlNamespaceManager nsmgr = new XmlNamespaceManager((XmlNameTable) new NameTable());
      string xml2 = responseData.getValue(pmsResponseKeys.PMSResponseMessageKey);
      XmlDocument xmlDocument2 = new XmlDocument();
      xmlDocument2.LoadXml(xml2);
      XmlNode xmlNode = xmlDocument2.SelectSingleNode("/");
      bool flag = false;
      string innerText;
      try
      {
        if (xmlNode.FirstChild != null && xmlNode.FirstChild.Name.ToString() == "AACNAdjusterResponse")
        {
          nsmgr.AddNamespace("hcai", "http://hcai.ca");
          flag = true;
          innerText = xmlDocument1.SelectSingleNode("/hcai:AACNAdjusterResponse/hcai:DocumentNumber", nsmgr).InnerText;
        }
        else
        {
          nsmgr.AddNamespace("hl7", "urn:hl7-org:v3");
          innerText = xmlDocument1.SelectSingleNode("/hl7:*/hl7:acknowledgement/hl7:typeCode/@code", nsmgr).InnerText;
        }
      }
      catch (Exception ex)
      {
        throw new ToolkitException(ToolkitExceptionCode.nInternalToolkitError, "Code for response status not found where expected.", ex);
      }
      if (!flag)
        return innerText == "AA";
      return !string.IsNullOrEmpty(innerText);
    }

    public virtual IDataItem transformResponse(IDataItem responseData)
    {
      return this.doTransform(responseData);
    }

    public virtual IError processError(IDataItem responseData, string documentType)
    {
      return new ErrorMapper().transform(responseData, documentType);
    }

    private IDataItem doTransform(IDataItem data)
    {
      return MapperFactory.newMapper(data, (IAction) this).transform(data);
    }

    public string ApplyXslt(string Xml, string xslt)
    {
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(Xml);
      XslCompiledTransform compiledTransform = new XslCompiledTransform();
      compiledTransform.Load(XmlReader.Create((TextReader) new StringReader(xslt)));
      StringBuilder output = new StringBuilder();
      XmlWriter results = XmlWriter.Create(output, new XmlWriterSettings()
      {
        Encoding = Encoding.UTF8
      });
      compiledTransform.Transform((IXPathNavigable) xmlDocument, (XsltArgumentList) null, results);
      XmlDocument xmlOCFDoc = new XmlDocument();
      var sRes = output.ToString();
      xmlOCFDoc.LoadXml(sRes);
      this.SetEncoding(xmlOCFDoc);
      return xmlOCFDoc.OuterXml;
    }

    private void SetEncoding(XmlDocument xmlOCFDoc)
    {
      if (xmlOCFDoc.FirstChild.NodeType != XmlNodeType.XmlDeclaration)
        return;
      ((XmlDeclaration) xmlOCFDoc.FirstChild).Encoding = "ISO-8859-1";
    }
  }
}
