﻿// Decompiled with JetBrains decompiler
// Type: IService
// Assembly: HCAIPMSToolkit, Version=3.17.6326.27877, Culture=neutral, PublicKeyToken=null
// MVID: A13581D2-2DC5-4D0C-92DA-5116CDA5E6E2
// Assembly location: D:\UniOffice.libs\HCAIPMSToolkit.dll

using IBC.HCAI.SoapExceptionWrapper;
using System.CodeDom.Compiler;
using System.Net.Security;
using System.ServiceModel;
using System.Xml;

[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[ServiceContract(ConfigurationName = "IService")]
public interface IService
{
  [OperationContract(Action = "http://tempuri.org/IService/SendNeCSTMessage", ProtectionLevel = ProtectionLevel.Sign, ReplyAction = "http://tempuri.org/IService/SendNeCSTMessageResponse")]
  [FaultContract(typeof (FaultExceptionDetail), Action = "http://tempuri.org/IService/SendNeCSTMessageFaultExceptionDetailFault", Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper", ProtectionLevel = ProtectionLevel.Sign)]
  XmlElement SendNeCSTMessage(XmlElement neCST_xml);

  [OperationContract(Action = "http://tempuri.org/IService/GetFacilityInfo", ProtectionLevel = ProtectionLevel.Sign, ReplyAction = "http://tempuri.org/IService/GetFacilityInfoResponse")]
  [FaultContract(typeof (FaultExceptionDetail), Action = "http://tempuri.org/IService/GetFacilityInfoFaultExceptionDetailFault", Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper", ProtectionLevel = ProtectionLevel.Sign)]
  XmlElement GetFacilityInfo();

  [OperationContract(Action = "http://tempuri.org/IService/GetInsurerList", ProtectionLevel = ProtectionLevel.Sign, ReplyAction = "http://tempuri.org/IService/GetInsurerListResponse")]
  [FaultContract(typeof (FaultExceptionDetail), Action = "http://tempuri.org/IService/GetInsurerListFaultExceptionDetailFault", Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper", ProtectionLevel = ProtectionLevel.Sign)]
  XmlElement GetInsurerList();

  [OperationContract(Action = "http://tempuri.org/IService/UpdateFacilityInfo", ReplyAction = "http://tempuri.org/IService/UpdateFacilityInfoResponse")]
  [FaultContract(typeof (FaultExceptionDetail), Action = "http://tempuri.org/IService/UpdateFacilityInfoFaultExceptionDetailFault", Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper")]
  XmlElement UpdateFacilityInfo(XmlElement pFacilityXml);

  [OperationContract(Action = "http://tempuri.org/IService/AddProviderInfo", ReplyAction = "http://tempuri.org/IService/AddProviderInfoResponse")]
  [FaultContract(typeof (FaultExceptionDetail), Action = "http://tempuri.org/IService/AddProviderInfoFaultExceptionDetailFault", Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper")]
  XmlElement AddProviderInfo(XmlElement pProviderXml);

  [OperationContract(Action = "http://tempuri.org/IService/UpdateProviderInfo", ReplyAction = "http://tempuri.org/IService/UpdateProviderInfoResponse")]
  [FaultContract(typeof (FaultExceptionDetail), Action = "http://tempuri.org/IService/UpdateProviderInfoFaultExceptionDetailFault", Name = "FaultExceptionDetail", Namespace = "http://schemas.datacontract.org/2004/07/IBC.HCAI.SoapExceptionWrapper")]
  XmlElement UpdateProviderInfo(XmlElement pProviderXml);
}
