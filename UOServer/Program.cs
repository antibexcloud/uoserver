﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Diagnostics;

namespace UOServer
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        private static extern Boolean AllocConsole();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            Trace.AutoFlush = true;
            Trace.WriteLine("Main of UOServer");

            if (Environment.UserInteractive)
            {
                AllocConsole(); // console mode

                var servicesToRun = new[] { new UOService() };

                var type = typeof(ServiceBase);
                const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
                var method = type.GetMethod("OnStart", flags);
                foreach (var service in servicesToRun)
                {
                    method?.Invoke(service, new object[] { args });
                }

                Console.WriteLine(@"Press any key to exit");
                Console.ReadKey();

                var methodStop = type.GetMethod("OnStop", flags);
                foreach (var service in servicesToRun)
                    methodStop?.Invoke(service, null);
            }
            else
            {
                ServiceBase.Run(new ServiceBase[] { new UOService() });
            }
        }
    }
}
