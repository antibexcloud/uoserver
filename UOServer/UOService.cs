﻿using System;
using System.ServiceProcess;
using System.Threading;
using UOServerLib;

namespace UOServer
{
    public partial class UOService : ServiceBase
    {
        private readonly UOServiceLib _server;

        public UOService()
        {
            InitializeComponent();
            _server = new UOServiceLib(mEventLog);
        }

        protected override void OnStart(string[] args)
        {
            Debugger.LogInfo("Service started");

            if (Environment.UserInteractive)
                InitServer();
            else
                ThreadPool.QueueUserWorkItem(obj => InitServer());
        }

        private void InitServer()
        {
            try
            {
                if (_server.Start()) return;

                Debugger.WriteToFile("bad init!", 10);
                OnStop();
            }
            catch (Exception ex)
            {
                Debugger.LogError("start service exception", ex);
            }

            //exit
            if (Environment.UserInteractive)
                Environment.Exit(0);
            else
                Stop();
        }

        protected override void OnStop()
        {
            Debugger.WriteToFile("Service Stop function is called.", 10);//Eugene-Added
            _server.Stop();
        }

        private void mEventLog_EntryWritten(object sender, System.Diagnostics.EntryWrittenEventArgs e)
        {

        }
    }
}
