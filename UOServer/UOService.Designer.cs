﻿namespace UOServer
{
    partial class UOService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mEventLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.mEventLog)).BeginInit();
            // 
            // mEventLog
            // 
            this.mEventLog.Log = "UONewLog";
            this.mEventLog.Source = "UOLogSource";
            this.mEventLog.EntryWritten += new System.Diagnostics.EntryWrittenEventHandler(this.mEventLog_EntryWritten);
            // 
            // UOService
            // 
            this.ServiceName = "UOService";
            ((System.ComponentModel.ISupportInitialize)(this.mEventLog)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog mEventLog;
    }
}
